VERSION 5.00
Begin VB.Form SFrmParam 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Param�tres"
   ClientHeight    =   8430
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3465
   ControlBox      =   0   'False
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8430
   ScaleWidth      =   3465
   ShowInTaskbar   =   0   'False
   Tag             =   "1"
   Begin VB.CommandButton Btn_Stop 
      BackColor       =   &H000000FF&
      Caption         =   "Stop"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   37
      Tag             =   "73"
      ToolTipText     =   "0"
      Top             =   8040
      Visible         =   0   'False
      Width           =   3435
   End
   Begin VB.CommandButton Btn_Decoupe 
      BackColor       =   &H0080C0FF&
      Caption         =   "D�couper"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   36
      Tag             =   "72"
      ToolTipText     =   "0"
      Top             =   7680
      Width           =   3435
   End
   Begin VB.CommandButton Btn_Calcul 
      BackColor       =   &H0000FF00&
      Caption         =   "Calculer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   32
      Tag             =   "71"
      ToolTipText     =   "0"
      Top             =   7320
      Width           =   3435
   End
   Begin VB.Frame Frame4 
      Caption         =   "Chauffe et vitesse"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1755
      Left            =   0
      TabIndex        =   20
      Tag             =   "61"
      ToolTipText     =   "0"
      Top             =   5520
      Width           =   3435
      Begin VB.ComboBox Combo_Mat 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   120
         TabIndex        =   24
         ToolTipText     =   "0"
         Top             =   720
         Width           =   3195
      End
      Begin VB.OptionButton Option_Chauffe 
         Caption         =   "Manuelle"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   23
         Tag             =   "62"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1275
      End
      Begin VB.OptionButton Option_Chauffe 
         Caption         =   "Asservie"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   22
         Tag             =   "63"
         ToolTipText     =   "0"
         Top             =   240
         Value           =   -1  'True
         Width           =   1275
      End
      Begin VB.TextBox TB_VDecoupe 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1380
         TabIndex        =   21
         Text            =   "3"
         ToolTipText     =   "0"
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Chauffe :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   38
         Tag             =   "68"
         ToolTipText     =   "0"
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label16 
         Caption         =   "Mati�re :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   28
         Tag             =   "64"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label17 
         Caption         =   "Vitesse :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   27
         Tag             =   "66"
         ToolTipText     =   "0"
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label18 
         Caption         =   "mm / s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2220
         TabIndex        =   26
         Tag             =   "67"
         ToolTipText     =   "0"
         Top             =   1080
         Width           =   675
      End
      Begin VB.Label lblChauffe 
         Alignment       =   2  'Center
         Caption         =   "0 %"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1440
         TabIndex        =   25
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1440
         Width           =   1875
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Rayonnement"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   0
      TabIndex        =   19
      Tag             =   "51"
      Top             =   4680
      Width           =   3435
      Begin VB.OptionButton Option_Rayonnement 
         Caption         =   "invers� (int�rieur)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   35
         Tag             =   "53"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   3255
      End
      Begin VB.OptionButton Option_Rayonnement 
         Caption         =   "compens�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   34
         Tag             =   "52"
         ToolTipText     =   "0"
         Top             =   240
         Value           =   -1  'True
         Width           =   3255
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Position du bloc"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3435
      Left            =   0
      TabIndex        =   5
      Tag             =   "20"
      ToolTipText     =   "0"
      Top             =   1200
      Width           =   3435
      Begin VB.Frame Frame7 
         Caption         =   "Suivant X"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   120
         TabIndex        =   29
         Tag             =   "21"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   3195
         Begin VB.TextBox TB_Offset_X 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1800
            TabIndex        =   30
            Text            =   "20"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   735
         End
         Begin VB.Label Label3 
            Caption         =   "Distance de :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   39
            Tag             =   "23"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   2700
            TabIndex        =   33
            Tag             =   "34"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   375
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Suivant Z"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1635
         Left            =   120
         TabIndex        =   9
         Tag             =   "41"
         ToolTipText     =   "0"
         Top             =   1680
         Width           =   3195
         Begin VB.OptionButton Option_Z 
            Caption         =   "Au milieu"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   120
            TabIndex        =   16
            Tag             =   "42"
            ToolTipText     =   "0"
            Top             =   240
            Value           =   -1  'True
            Width           =   2715
         End
         Begin VB.OptionButton Option_Z 
            Caption         =   "Au bord gauche de la table"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   120
            TabIndex        =   15
            Tag             =   "43"
            ToolTipText     =   "0"
            Top             =   480
            Width           =   2715
         End
         Begin VB.OptionButton Option_Z 
            Caption         =   "Au bord droit de la table"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   120
            TabIndex        =   14
            Tag             =   "44"
            ToolTipText     =   "0"
            Top             =   720
            Width           =   2715
         End
         Begin VB.OptionButton Option_Z 
            Caption         =   "A gauche :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   120
            TabIndex        =   13
            Tag             =   "45"
            ToolTipText     =   "0"
            Top             =   960
            Width           =   1455
         End
         Begin VB.OptionButton Option_Z 
            Caption         =   "A droite :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   120
            TabIndex        =   12
            Tag             =   "46"
            ToolTipText     =   "0"
            Top             =   1200
            Width           =   1455
         End
         Begin VB.TextBox TB_Offset_Z_Gauche 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1800
            TabIndex        =   11
            Text            =   "0"
            ToolTipText     =   "0"
            Top             =   960
            Width           =   735
         End
         Begin VB.TextBox TB_Offset_Z_Droit 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1800
            TabIndex        =   10
            Text            =   "0"
            ToolTipText     =   "0"
            Top             =   1200
            Width           =   735
         End
         Begin VB.Label Label14 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   2580
            TabIndex        =   18
            Tag             =   "47"
            ToolTipText     =   "0"
            Top             =   960
            Width           =   375
         End
         Begin VB.Label Label14 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   2580
            TabIndex        =   17
            Tag             =   "47"
            ToolTipText     =   "0"
            Top             =   1200
            Width           =   375
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Suivant Y"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   120
         TabIndex        =   6
         Tag             =   "31"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   3195
         Begin VB.TextBox TB_Offset_Y 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1800
            TabIndex        =   7
            Text            =   "0"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   735
         End
         Begin VB.Label Label4 
            Caption         =   "Sur cale de :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   40
            Tag             =   "33"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label Label15 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   2700
            TabIndex        =   8
            Tag             =   "34"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   375
         End
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "D�coupe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   0
      TabIndex        =   0
      Tag             =   "10"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   3435
      Begin VB.CommandButton Command1 
         Caption         =   "Liste"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2580
         TabIndex        =   31
         Tag             =   "12"
         ToolTipText     =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.ComboBox Combo2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   120
         TabIndex        =   3
         Text            =   "Combo2"
         ToolTipText     =   "0"
         Top             =   480
         Visible         =   0   'False
         Width           =   2355
      End
      Begin VB.OptionButton Option_GD 
         Caption         =   "C�t� gauche"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Tag             =   "13"
         ToolTipText     =   "0"
         Top             =   840
         Value           =   -1  'True
         Width           =   1575
      End
      Begin VB.OptionButton Option_GD 
         Caption         =   "C�t� droit"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1680
         TabIndex        =   1
         Tag             =   "14"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   1635
      End
      Begin VB.Label Label20 
         Caption         =   "Choix :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   180
         TabIndex        =   4
         Tag             =   "11"
         ToolTipText     =   "0"
         Top             =   240
         Visible         =   0   'False
         Width           =   855
      End
   End
End
Attribute VB_Name = "SFrmParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Largeur(EMPLANTURE To SAUMON) As Long
Dim Hauteur(EMPLANTURE To SAUMON) As Long

Dim bPoints As Boolean
Dim bNum As Boolean

Dim minX As Single
Dim maxX As Single
Dim minY As Single
Dim maxY As Single

Public Sub Btn_Calcul_Click()
    Dim i As Integer, Index As Integer
    Dim k As Integer
    
    For i = Option_Z.LBound To Option_Z.UBound
        If Option_Z(i) Then
            Index = i
        End If
    Next i
    Call MDIFrmDecoupe.Calcul(minX, maxX, minY, maxY, CDbl(TB_VDecoupe), False)
    For k = EMPLANTURE To SAUMON
        Call MDIFrmDecoupe.PublicDessin(k)
    Next k
    Call MDIFrmDecoupe.PublicDessinDessus
    Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub Btn_Decoupe_Click()
    If Not maClasseHID.IsAvailable Then
        MsgBox ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 256)  'Interface non connect�e
        Exit Sub
    End If
    
    Call maClasseHID.DemandeInformation
    If Not maClasseHID.PWM_ON Or maClasseHID.PWM_Manual Then
        If MsgBox(ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 255), vbQuestion + vbOKCancel) = vbCancel Then
            Exit Sub
        End If
    End If
    
    Btn_Stop.Visible = True
    Call MDIFrmDecoupe.FrameEnable(False, Me)
    Call MDIFrmDecoupe.Decoupe
    Btn_Stop.Visible = False
End Sub

Private Sub Btn_Stop_Click()
    gPause = True
    Btn_Stop.Visible = False
    Call MDIFrmDecoupe.FrameEnable(True, Me)
End Sub

Private Sub Combo_Mat_Click()
    gstrMat = Combo_Mat.Text
    wVitesse = Min(Calcul_Fil_VitMax(gstrMat), CSng(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0)))
    wVitesse = Round(wVitesse, 1)
    TB_VDecoupe = wVitesse

    Call Affiche_Chauffe
    Call TB_VDecoupe_Validate(False)
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    gPause = True
End Sub

Private Sub Form_Load()
    Dim Section() As String
    Dim i As Integer
    Dim maTable As String

    Set gFenetreHID = MDIFrmDecoupe
    ChargeTrad Me
    
    'Chargement ListBox des mat�riaux
    Call Chargt_Materiau(Combo_Mat)
    
    On Error Resume Next        'Lancement sans .ini
    
    ReDim ListeDecalage(EMPLANTURE To SAUMON) As ClsListeChainee
    ListeDecalage(EMPLANTURE) = New ClsListeChainee
    ListeDecalage(SAUMON) = New ClsListeChainee
    Btn_Decoupe.Enabled = False
    MousePointer = vbHourglass
    TB_Offset_Z_Gauche = wOffset_Gauche - wDelta_Gauche
    TB_Offset_Z_Droit = wOffset_Droit - wDelta_Droit
    Option_GD(1).value = (wOption_GD = DROITE)
    TB_VDecoupe = wVitesse
    Call Option_Z_Click(0)  'Centrage au milieu en Z et Calcul
    '       ---> Il faut faire le calcul pour calculer les offset X et Y
    bProfilCharge = False 'pour emp�cher le calcul automatique
    
    If minX < 0# Then
        wOffset_X = Round(-minX + 0.5)
    End If
    If minY < 0# Then
        wOffset_Y = Round(-minY + 0.5)
    End If
    TB_Offset_X = wOffset_X
    TB_Offset_Y = wOffset_Y
    'Faire les d�calages
    Call MDIFrmDecoupe.DecalXY(wOffset_X, wOffset_Y)
    
    bProfilCharge = True 'pour forcer le calcul automatique
    MousePointer = vbDefault
    Call TB_VDecoupe_Validate(False)
End Sub

Private Sub Option_GD_Click(Index As Integer)
    wOption_GD = Index
    'Call MDIFrmDecoupe.Calcul(minX, maxX, minY, maxY, CDbl(TB_VDecoupe), False)
    Call MDIFrmDecoupe.PublicDessinDessus
    Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub Option_Rayonnement_Click(Index As Integer)
    wRayonCompense = (Option_Rayonnement(0).value = True)
    'Call Btn_Calcul_Click
End Sub

Private Sub Option_Z_Click(Index As Integer)
    Dim bSav As Boolean
    Dim k As Integer
    
    TB_Offset_Z_Gauche.Enabled = False
    TB_Offset_Z_Droit.Enabled = False
    
    'Les Offset se comprennent par rapport au porte fil et non � la table
    Select Case Index
        Case 0  'Au milieu
            wOffset_Gauche = (wLargeurTable - wEnvergure) / 2
            wOffset_Droit = wOffset_Gauche
        Case 1  'Au bord gauche
            wOffset_Gauche = wDelta_Gauche
            wOffset_Droit = wLargeurTable - wEnvergure - wOffset_Gauche
        Case 2  'Au bord droit
            wOffset_Droit = wDelta_Droit
            wOffset_Gauche = wLargeurTable - wEnvergure - wOffset_Droit
        Case 3  'A gauche de x mm
            TB_Offset_Z_Gauche.Enabled = True
            wOffset_Droit = wLargeurTable - wEnvergure - wOffset_Gauche
        Case 4  'A droite de x mm
            TB_Offset_Z_Droit.Enabled = True
            wOffset_Gauche = wLargeurTable - wEnvergure - wOffset_Droit
    End Select
    bSav = bProfilCharge
    bProfilCharge = False
    TB_Offset_Z_Gauche = wOffset_Gauche - wDelta_Gauche
    TB_Offset_Z_Droit = wOffset_Droit - wDelta_Droit
    bProfilCharge = bSav
    'Call MDIFrmDecoupe.Calcul(minX, maxX, minY, maxY, CDbl(TB_VDecoupe), False)
    'For k = EMPLANTURE To SAUMON
    '    Call MDIFrmDecoupe.PublicDessin(k)
    'Next k
    'Call MDIFrmDecoupe.PublicDessinDessus
    'Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub TB_Offset_X_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Offset_X_Validate(False)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Offset_X_Validate(Cancel As Boolean)
    If Not bProfilCharge Then Exit Sub
    If TB_Offset_X = "" Then Exit Sub
    If Not IsNumeric(TB_Offset_X) Then Exit Sub
    
    Dim xTemp As Single
    Dim yTemp As Single
    
    xTemp = CSng(TB_Offset_X.Text) - wOffset_X
    yTemp = CSng(TB_Offset_Y.Text) - wOffset_Y
    
    Call MDIFrmDecoupe.DecalXY(xTemp, yTemp)

    wOffset_X = CSng(TB_Offset_X.Text)
    minX = minX + xTemp
    maxX = maxX + xTemp

'    Call MDIFrmDecoupe.Calcul(minX, maxX, minY, maxY, CDbl(TB_VDecoupe), False)
'    Call MDIFrmDecoupe.PublicDessin(EMPLANTURE)
'    Call MDIFrmDecoupe.PublicDessin(SAUMON)
'    Call MDIFrmDecoupe.PublicDessinDessus
'    Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub TB_Offset_Y_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Offset_Y_Validate(False)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Offset_Y_Validate(Cancel As Boolean)
    If Not bProfilCharge Then Exit Sub
    If TB_Offset_Y = "" Then Exit Sub
    If Not IsNumeric(TB_Offset_Y) Then Exit Sub
    
    Dim xTemp As Single
    Dim yTemp As Single
    
    xTemp = CSng(TB_Offset_X.Text) - wOffset_X
    yTemp = CSng(TB_Offset_Y.Text) - wOffset_Y

    Call MDIFrmDecoupe.DecalXY(xTemp, yTemp)
    wOffset_Y = CSng(TB_Offset_Y.Text)
    minY = minY + yTemp
    maxY = maxY + yTemp

    'Call MDIFrmDecoupe.Calcul(minX, maxX, minY, maxY, CDbl(TB_VDecoupe), False)
    Call MDIFrmDecoupe.PublicDessin(EMPLANTURE)
    Call MDIFrmDecoupe.PublicDessin(SAUMON)
    Call MDIFrmDecoupe.PublicDessinDessus
    Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub TB_Offset_Z_Gauche_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Offset_Z_Gauche_Validate(False)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Offset_Z_Gauche_Validate(Cancel As Boolean)
    If Not bProfilCharge Then Exit Sub
    If TB_Offset_Z_Gauche = "" Then Exit Sub
    If Not IsNumeric(TB_Offset_Z_Gauche) Then Exit Sub
    wOffset_Gauche = CDbl(TB_Offset_Z_Gauche.Text) + wDelta_Gauche
    wOffset_Droit = wLargeurTable - wEnvergure - wOffset_Gauche
    TB_Offset_Z_Droit = wOffset_Droit - wDelta_Droit
    
    'Call MDIFrmDecoupe.Calcul(minX, maxX, minY, maxY, CDbl(TB_VDecoupe), False)
    Call MDIFrmDecoupe.PublicDessinDessus
    Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub TB_Offset_Z_droit_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Offset_Z_Droit_Validate(False)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Offset_Z_Droit_Validate(Cancel As Boolean)
    If Not bProfilCharge Then Exit Sub
    If TB_Offset_Z_Droit = "" Then Exit Sub
    If Not IsNumeric(TB_Offset_Z_Droit) Then Exit Sub
    wOffset_Droit = CDbl(TB_Offset_Z_Droit.Text) + wDelta_Droit
    wOffset_Gauche = wLargeurTable - wEnvergure - wOffset_Droit
    TB_Offset_Z_Gauche = wOffset_Gauche - wDelta_Gauche
    
    'Call MDIFrmDecoupe.Calcul(minX, maxX, minY, maxY, CDbl(TB_VDecoupe), False)
    Call MDIFrmDecoupe.PublicDessinDessus
    Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub TB_VDecoupe_Validate(Cancel As Boolean)
    Dim CH As Double
    Dim CB As Double
    Dim VH As Double
    Dim VB As Double
    Dim SH1 As Single
    Dim SH2 As Single
    If TB_VDecoupe.Text = "" Then Exit Sub
    If IsNumeric(TB_VDecoupe) Then
        If CDbl(TB_VDecoupe) > ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0) Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 204), vbCritical
            Exit Sub
        End If
        If Calcul_Fil(gstrMat, CDbl(TB_VDecoupe), CH, CB, VH, VB, SH1, SH2) > 100 Then
            wVitesse = Round(100 * (VH - VB) / (CH - CB) + (VB * CH - VH * CB) / (CH - CB), 1)
            TB_VDecoupe = wVitesse
        Else
            wVitesse = CDbl(TB_VDecoupe)
        End If
        Call Affiche_Chauffe
    End If
End Sub

Private Sub TB_VDecoupe_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_VDecoupe_Validate(False)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub Affiche_Chauffe()
    Dim CH As Double
    Dim CB As Double
    Dim VH As Double
    Dim VB As Double
    Dim SH1 As Single
    Dim SH2 As Single

    'Affichage de la chauffe en %
'    MDIFrmDecoupe.wChauffe = Calcul_Fil(gstrMat, CDbl(TB_VDecoupe), CH, CB, VH, VB, SH1, SH2)
    MDIFrmDecoupe.wChauffe = Calcul_Fil(gstrMat, wVitesse, CH, CB, VH, VB, SH1, SH2)
    lblChauffe = Round(MDIFrmDecoupe.wChauffe + 0.05, 0) & " %"
End Sub
