VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MDIFrmDecoupe 
   BackColor       =   &H8000000C&
   Caption         =   "D�coupe"
   ClientHeight    =   8610
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   14010
   LinkTopic       =   "MDIForm1"
   ScrollBars      =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   8055
      Visible         =   0   'False
      Width           =   14010
      _ExtentX        =   24712
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList IL1 
      Left            =   1440
      Top             =   1800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIfrmDecoupe.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIfrmDecoupe.frx":08DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIfrmDecoupe.frx":0D1A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIfrmDecoupe.frx":112A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIfrmDecoupe.frx":152E
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIfrmDecoupe.frx":1BF0
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIfrmDecoupe.frx":22B2
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIfrmDecoupe.frx":2974
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar SB 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   1
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   8310
      Width           =   14010
      _ExtentX        =   24712
      _ExtentY        =   529
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar TB 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "0"
      Top             =   0
      Width           =   14010
      _ExtentX        =   24712
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "IL1"
      DisabledImageList=   "IL1"
      HotImageList    =   "IL1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "mnu_Reset"
            Object.ToolTipText     =   "tt2"
            Object.Tag             =   "0"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "mnu_Points"
            ImageIndex      =   2
            Style           =   1
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "mnu_Num"
            ImageIndex      =   4
            Style           =   1
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "mnu_ZoomMoins"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "mnu_ZoomPlus"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "mnu_ZoomSelection"
            ImageIndex      =   7
            Style           =   1
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "mnu_Zoom1"
            ImageIndex      =   8
         EndProperty
      EndProperty
      Begin VB.Image ImageMain 
         Height          =   480
         Left            =   0
         Picture         =   "MDIfrmDecoupe.frx":3036
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   0
         Visible         =   0   'False
         Width           =   480
      End
   End
End
Attribute VB_Name = "MDIFrmDecoupe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim FrmEmplanture As Form
Dim FrmSaumon As Form
Dim FrmDessus As Form
Dim FrmFace As Form
Dim FrmParam As Form
Dim FrmErreurs As Form

Public wChauffe As Integer  'Valeur de la chauffe calcul�e en fonction de la vitesse de consigne
Public Min_Chariot_X As Single
Public Max_Chariot_X As Single
Public Min_Chariot_Y As Single
Public Max_Chariot_Y As Single
Public Min_Decal_X As Single
Public Max_Decal_X As Single
Public Min_Decal_Y As Single
Public Max_Decal_Y As Single

Public bPoints As Boolean
Public bNum As Boolean

Private defProgBarHwnd  As Long 'Pour la ProgressBar

Dim xMin As Single, xMax As Single

Private Sub MDIForm_Load()
    'Appel au nettoyage synchro :
    Dim k As Integer

    Dim nb As Long
    nb = NettoyageSynchro(monTableau1(), Resolution(gstrTable))
    If nb <> 0 Then
        ReDim Preserve monTableau1(1 To 2, 0 To nb - 1)
    End If
    
    'Il faut recr�er une liste
    Call CreerListeDecoupe
    
    Erase Tab_Chariot
    
    ChargeTrad Me
    
    Call maClasseHID.TableVerif
    
    bDessinDEcoupe = False
    
    On Error Resume Next
    '8790=Hauteur de SFRMParam
    Me.Move ReadINI_Num(Me.Name & "_Left", 0), _
            ReadINI_Num(Me.Name & "_Top", 0), _
            ReadINI_Num(Me.Name & "_Width", 13000), _
            Max(ReadINI_Num(Me.Name & "_Height", 9600), 8790 + TB.Height + SB.Height + ProgressBar1.Height + 500)
    On Error GoTo 0
'    FrmErreurs.Hide
    Set FrmEmplanture = New SFrmEmplanture
    Set FrmSaumon = New SFrmSaumon
    Set FrmDessus = New SFrmDessus
    Set FrmFace = New SFrmFace
    Set FrmParam = New SFrmParam
    FrmParam.Show
    bDessinDEcoupe = True
    Call Reset_Forms
    Call FrmParam.Btn_Calcul_Click
    Call MDIFrmDecoupe.DecalXY(wOffset_X, wOffset_Y)
    For k = EMPLANTURE To SAUMON
        Call MDIFrmDecoupe.PublicDessin(k)
    Next k
    Call MDIFrmDecoupe.PublicDessinDessus
    Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If DeplEnCours Then
        Beep
        Cancel = DeplEnCours
        Exit Sub
    End If
    Unload FrmParam
    Call WriteINI(Me.Name & "_Offset_Gauche", CStr(wOffset_Gauche))
    Call WriteINI(Me.Name & "_Offset_Droit", CStr(wOffset_Droit))
    Call WriteINI(Me.Name & "_Offset_X", CStr(wOffset_X))
    Call WriteINI(Me.Name & "_Offset_Y", CStr(wOffset_Y))
    Call WriteINI(Me.Name & "_Epaisseur", CStr(wEpaisseur))
    Call WriteINI(Me.Name & "_Vitesse", CStr(wVitesse))
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
        Call WriteINI(Me.Name & "_Width", Me.Width)
        Call WriteINI(Me.Name & "_Height", Me.Height)
    End If
    frmMain.Timer_Refresh.Enabled = True
End Sub

Private Sub TB_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim bReDessin As Boolean
    Dim indicef As Integer
    
    Select Case ActiveForm.Name
        Case "SFrmEmplanture"
            indicef = E
        Case "SFrmSaumon"
            indicef = s
        Case "SFrmFace"
            indicef = f
        Case "SFrmDessus"
            indicef = D
        Case Else
            indicef = 0
    End Select
    
    bReDessin = False
    
    Select Case Button.Key
        Case "mnu_Reset"
            Call Reset_Forms
        Case "mnu_Num", "mnu_Points"
            bNum = (TB.buttons(3).value = tbrPressed)
            bPoints = (TB.buttons(2).value = tbrPressed)
            Call FrmSaumon.Form_Resize
            Call FrmEmplanture.Form_Resize
        Case "mnu_ZoomMoins"
            If indicef <> 0 Then ActiveForm.CoeffInit = ActiveForm.CoeffInit * 0.9
            bReDessin = True
        Case "mnu_ZoomPlus"
            If indicef <> 0 Then ActiveForm.CoeffInit = ActiveForm.CoeffInit / 0.9
            bReDessin = True
        Case "mnu_Zoom1"
            If indicef <> 0 Then
                Dim wCoeffInit As Single
                wCoeffInit = ActiveForm.MemoCoeffInit
                Call ActiveForm.InitVariable
                ActiveForm.CoeffInit = wCoeffInit
                ActiveForm.MemoCoeffInit = wCoeffInit
                bReDessin = True
            End If
        Case "mnu_ZoomSelection"
            If Not Button.value = tbrPressed Then
                Button.value = tbrUnpressed
            Else
                Button.value = tbrPressed
            End If
    End Select
    Select Case ActiveForm.Name
        Case "SFrmEmplanture", "SFrmSaumon"
            Call Dessin(ActiveForm, IIf(ActiveForm.Name = "SFrmEmplanture", EMPLANTURE, SAUMON))
        Case "SFrmFace"
            Call DessinFace(ActiveForm)
        Case "SFrmDessus"
            Call DessinDessus(ActiveForm)
    End Select
End Sub

Private Sub Reset_Forms()
    Dim maHauteur As Integer
    Dim maLargeur1 As Integer
    Dim maLargeur2 As Integer
    Dim LargeurParam As Integer
    LargeurParam = 3555
    
    If Me.ScaleHeight = 0# Then Exit Sub
    
    maHauteur = (Me.ScaleHeight - ProgressBar1.Height) / 2
    If maHauteur = 0 Then Exit Sub
    maLargeur1 = (Me.ScaleWidth - LargeurParam) * 2 / 3
    maLargeur2 = (Me.ScaleWidth - LargeurParam) * 1 / 3
    
    FrmEmplanture.Move LargeurParam, 0, maLargeur1, maHauteur
    FrmSaumon.Move LargeurParam, maHauteur, maLargeur1, maHauteur
    FrmDessus.Move LargeurParam + maLargeur1, 0, maLargeur2, maHauteur
    FrmFace.Move FrmDessus.Left, maHauteur, maLargeur2, maHauteur
    FrmParam.Move 0, 0
End Sub

Public Sub PublicDessin(k As Integer)
    If k = EMPLANTURE Then
        Call Dessin(FrmEmplanture, k)
    Else
        Call Dessin(FrmSaumon, k)
    End If
End Sub

Private Sub Dessin(f As Form, k As Integer)
    'Dessin pour l'emplanture et le saumon,
    '    pour le profil initial et le profil d�cal�
    Dim i As Long
    Dim P1 As Point
    Dim P2 As Point
    Dim x1 As Single, y1 As Single, x2 As Single, y2 As Single
    Dim Rapport(EMPLANTURE To SAUMON) As Double
    Dim DeltaEpsilon As Double
    Dim DeltaX As Single
    Dim DeltaY As Single
    'Pour les fl�ches
    Dim PDepart As Point
    Dim pFleche As Point
    Dim bDecalage As Boolean    'D�calage calcul� ?
    Const HAUTEUR_TABLE = 10
    
    DeltaEpsilon = 0.05
    
    With f.PB
        .AutoRedraw = True
        .Cls
        '.ScaleLeft = -.ScaleWidth
    End With
    
    x1 = Min(ListeDecoupe(EMPLANTURE).minX, ListeDecoupe(SAUMON).minX)
    x2 = Max(ListeDecoupe(EMPLANTURE).maxX, ListeDecoupe(SAUMON).maxX)
    y1 = Min(ListeDecoupe(EMPLANTURE).minY, ListeDecoupe(SAUMON).minY)
    y2 = Max(ListeDecoupe(EMPLANTURE).maxY, ListeDecoupe(SAUMON).maxY)
    x1 = Min(x1, Min_Decal_X)
    x2 = Max(x2, Max_Decal_X)
    y1 = Min(y1, Min_Decal_Y)
    y2 = Max(y2, Max_Decal_Y)

    'Dimensions du bloc
    For i = 0 To UBound(tabBlocInv, 2)
        x1 = Min(x1, Min(tabBlocInv(EMPLANTURE, i).x, tabBlocInv(SAUMON, i).x))
        x2 = Max(x2, Max(tabBlocInv(EMPLANTURE, i).x, tabBlocInv(SAUMON, i).x))
        y1 = Min(y1, Min(tabBlocInv(EMPLANTURE, i).y, tabBlocInv(SAUMON, i).y))
        y2 = Max(y2, Max(tabBlocInv(EMPLANTURE, i).y, tabBlocInv(SAUMON, i).y))
    Next i
    
    y1 = y1 - HAUTEUR_TABLE
    
    P1.x = x1
    x1 = x1 - DeltaEpsilon * (x2 - x1)
    y1 = y1 - DeltaEpsilon * (y2 - y1)
    P2.x = x2
    x2 = x2 + DeltaEpsilon * (x2 - x1)
    y2 = y2 + DeltaEpsilon * (y2 - y1)
    
    'Dessin table
    P1.x = 0#
    P1.y = 0#
    P2.y = -HAUTEUR_TABLE
    P2.x = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_16", 800)    'D�battement X
    f.PB.FillStyle = vbDiagonalCross
    f.PB.FillColor = vbBlack
    Call PB_Rectangle(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbBlack, False)
    
    'Dessin du bloc
    P1 = PLTtoAPI(tabBlocInv(k, 0), f.PB)
    P2 = PLTtoAPI(tabBlocInv(k, 2), f.PB)
    f.PB.FillStyle = vbTransparent
    f.PB.FillColor = vbWhite
    f.PB.Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k), B
    
    'Dessin profil initial
    If ListeDecoupe(k).count <> 0 Then
        ListeDecoupe(k).MoveFirst
        P1 = PLTtoAPI(ListeDecoupe(k).CurrentPoint, f.PB)
        For i = 1 To ListeDecoupe(k).count - 1
            ListeDecoupe(k).MoveNext
            P2 = PLTtoAPI(ListeDecoupe(k).CurrentPoint, f.PB)
            f.PB.Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k)
            P1 = P2
        Next i
    End If
   
    'Dessin profil d�cal�
    If Not IsArrayNullChariot(Tab_Chariot) Then
        P1 = PLTtoAPI(Tab_Chariot(1).P_Decal(k), f.PB)
        'Dessin du trajet du fil avant entr�e dans le bloc
        P2.x = 0#
        P2.y = 0#
        P2 = PLTtoAPI(P2, f.PB)
        f.PB.Line (P1.x, P1.y)-(P2.x, P2.y), vbBlack
        Call PB_Fleche(f.PB, P2, P1, CouleurFleche)
        PDepart = P1
        pFleche = P2
        For i = 2 To UBound(Tab_Chariot)
            P2 = PLTtoAPI(Tab_Chariot(i).P_Decal(k), f.PB)
            
    '        If i = 2 Then
            If i = 8 Then
                PDepart = P1
                pFleche = P2
            End If
            If PDepart.x = pFleche.x And _
                PDepart.y = pFleche.y Then
                pFleche = P1
            Else
                'Trac� des fl�ches de sens
                Call PB_Fleche(f.PB, PDepart, pFleche, CouleurFleche)
                f.PB.DrawWidth = 1
            End If
            If i = Round(UBound(Tab_Chariot) / 2, 0) Then
                PDepart = P1
                pFleche = P2
            End If
            f.PB.Line (P1.x, P1.y)-(P2.x, P2.y), vbBlack
            P1 = P2
        Next i
        'Dessin du trajet du fil apr�s sortie dans le bloc
        P2.x = 0#
        P2.y = 0#
        P2 = PLTtoAPI(P2, f.PB)
        f.PB.Line (P1.x, P1.y)-(P2.x, P2.y), vbBlack
        f.PB.FillStyle = vbFSTransparent
    
        'Dessin des points ou des num�ros
        If bPoints Or bNum Then
            For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
                P1 = Tab_Chariot(i).P_Decal(k)
                If bNum Then
                    Call PB_Cercle(f.PB, PLTtoAPI(P1, f.PB), Couleur(k), Couleur(3 - k), _
                        False, False, False, bNum, IIf(bNum, i, 0))
                Else
                    Call PB_Carre(f.PB, PLTtoAPI(P1, f.PB), Couleur(k))
                End If
            Next i
        End If
    End If
End Sub

Public Sub PublicDessinDessus()
    Call DessinDessus(FrmDessus)
End Sub
Private Sub DessinDessus(f As Form)
    'Dessin (vue du dessus)
    Dim i As Long
    Dim k As Integer
    Dim P1 As Point
    Dim P2 As Point
    Dim x1 As Single, y1 As Single, x2 As Single, y2 As Single
    Dim bDecalage As Boolean
    Dim DeltaEpsilon As Double
    
    DeltaEpsilon = 0.1
    
    f.PB.AutoRedraw = True
    f.PB.Cls
    
    'Dimensions de la table
    x1 = 0#
    y1 = 0#
    x2 = wLargeurTable
    y2 = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_16", 800)    'D�battement X
    
    P1.x = x1 - DeltaEpsilon * (x2 - x1)
    P1.y = y1 - DeltaEpsilon * (y2 - y1)
    P2.x = x2 + DeltaEpsilon * (x2 - x1)
    P2.y = y2 + DeltaEpsilon * (y2 - y1)
    
    'Dessin table
    f.PB.DrawWidth = 1
    f.PB.FillStyle = vbFSSolid
    f.PB.FillColor = RGB(255, 255, 200) 'Gris p�le
    P1.x = x1 + wDelta_Gauche
    P1.y = y1
    P2.x = x2 - wDelta_Droit
    P2.y = y2
    Call PB_Rectangle(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbBlack)
    
    'Dessin parcours des chariots
    If Not IsArrayNullChariot(Tab_Chariot) Then
        P1.x = IIf(wOption_GD = GAUCHE, 0#, wLargeurTable)
        P2.x = IIf(wOption_GD = GAUCHE, wLargeurTable, 0#)
        For i = 1 To UBound(Tab_Chariot)
            P1.y = Tab_Chariot(i).P_abs(EMPLANTURE).x
            P2.y = Tab_Chariot(i).P_abs(SAUMON).x
            Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbBlack)
        Next i
    End If
    
    'Dessin du bloc
    'On part du bas � gauche, on tourne sens horloge
    
    P1.x = wOffset_Gauche
    P1.y = tabBlocInv(IIf(wOption_GD = GAUCHE, EMPLANTURE, SAUMON), 1).x
    P2.x = P1.x
    P2.y = tabBlocInv(IIf(wOption_GD = GAUCHE, EMPLANTURE, SAUMON), 0).x
    Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), Couleur(IIf(wOption_GD = GAUCHE, EMPLANTURE, SAUMON)))
    P1.x = wOffset_Gauche + wEnvergure
    P1.y = tabBlocInv(IIf(wOption_GD = GAUCHE, SAUMON, EMPLANTURE), 0).x
    Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbGreen)
    P2.x = P1.x
    P2.y = tabBlocInv(IIf(wOption_GD = GAUCHE, SAUMON, EMPLANTURE), 1).x
    Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), Couleur(IIf(wOption_GD = GAUCHE, SAUMON, EMPLANTURE)))
    P1.x = wOffset_Gauche
    P1.y = tabBlocInv(IIf(wOption_GD = GAUCHE, EMPLANTURE, SAUMON), 1).x
    Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbGreen)
End Sub

Public Sub PublicDessinFace()
    Call DessinFace(FrmFace)
End Sub
Private Sub DessinFace(f As Form)
    'Dessin (vue de face)
    Dim i As Long
    Dim P1 As Point
    Dim P2 As Point
    Dim x1 As Single, y1 As Single, x2 As Single, y2 As Single
    Dim bDecalage As Boolean
    Dim DeltaEpsilon As Double
    
    DeltaEpsilon = 0.1
    
    On Error GoTo DecalageVide
    On Error GoTo 0
DecalageVide:

    f.PB.AutoRedraw = True
    f.PB.Cls
    
    'Dimensions du dessin
    x1 = 0
    y1 = -10
    x2 = wLargeurTable 'Largeur de la table
    y2 = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_17", 400)    'Course en Y
    
    P1.y = y1 - DeltaEpsilon * (y2 - y1)
    P2.y = y2 + DeltaEpsilon * (y2 - y1)
    P1.x = x1 - DeltaEpsilon * (x2 - x1)
    P2.x = x2 + DeltaEpsilon * (x2 - x1)
    
    'Dessin du bloc
    'On part du bas � gauche, on tourne sens horloge
    P1.x = wOffset_Gauche
    P1.y = wOffset_Y
    P2.x = P1.x
    P2.y = wEpaisseur + wOffset_Y
    Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), Couleur(IIf(wOption_GD = GAUCHE, EMPLANTURE, SAUMON)))
    P1.x = wOffset_Gauche + wEnvergure
    P1.y = P2.y
    Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbGreen)
    P2.x = P1.x
    P2.y = wOffset_Y
    Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), Couleur(IIf(wOption_GD = GAUCHE, SAUMON, EMPLANTURE)))
    P1.x = wOffset_Gauche
    P1.y = P2.y
    Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbGreen)
   
    If Not IsArrayNullChariot(Tab_Chariot) Then
        'Dessin parcours des chariots
        P1.x = IIf(wOption_GD = GAUCHE, 0#, wLargeurTable)
        P2.x = IIf(wOption_GD = GAUCHE, wLargeurTable, 0#)
        For i = 1 To UBound(Tab_Chariot)
            P1.y = Tab_Chariot(i).P_abs(EMPLANTURE).y
            P2.y = Tab_Chariot(i).P_abs(SAUMON).y
            Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbBlack)
        Next i
    
        'Dessin d�calage
        P1.x = wOffset_Gauche
        P2.x = wOffset_Gauche + wEnvergure
        For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
            P1.y = Tab_Chariot(i).P_Decal(EMPLANTURE).y
            P2.y = Tab_Chariot(i).P_Decal(SAUMON).y
            Call PB_Line(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbBlack)
        Next i
    End If
    
    'Dessin table
    P1.x = wDelta_Gauche
    P1.y = 0
    P2.x = wLargeurTable - wDelta_Droit 'Largeur de la table
    P2.y = -10
    f.PB.DrawWidth = 1
    f.PB.FillStyle = vbFSSolid
    f.PB.FillColor = vbBlack
    f.PB.FillStyle = vbDiagonalCross
    Call PB_Rectangle(f.PB, PLTtoAPI(P1, f.PB), PLTtoAPI(P2, f.PB), vbBlack, False)
End Sub

Public Sub Calcul(ByRef minX As Single, _
                  ByRef maxX As Single, _
                  ByRef minY As Single, _
                  ByRef maxY As Single, _
                  ByVal Vitesse As Single, _
                  ByVal bCalculPos As Boolean)     'bCalculPos : calcul pour le positionnement
                  
    Dim wCourse_X As Single
    Dim wCourse_Y As Single
    Dim i As Long, j As Long
    Dim k As Integer
    Dim P1 As Point, P2 As Point
    Dim VitMax As Single
    Dim maVitesse As Single
    Dim wErreur As String
    Dim Temp As Single
    
    'On calcule les temps en fonction de la vitesse de consigne
    ' pour chaque segment (Emplanture et Saumon) et on
    '       les place dans ListeDecoupe
    Call Calcul_Vitesse_Decalage(ListeDecoupe, Vitesse)
    
Debut_Calcul:

    If UCase$(ReadINI("DebugV4", "Faux")) = "VRAI" Then
        Dim Tableau() As TypeDecal
        Dim DVH As Single
        Dim SH1 As Single, SH2 As Single
        Dim nbPoints As Long

        DVH = ReadINI_Machine_Num("mat_" & FrmParam.Combo_Mat, "VH", 20)
        SH1 = ReadINI_Machine_Num("mat_" & FrmParam.Combo_Mat, "SH1", 0)
        SH2 = ReadINI_Machine_Num("mat_" & FrmParam.Combo_Mat, "SH2", 0)
        'Remplissage de Tableau
        ReDim Tableau(0 To ListeDecoupe(EMPLANTURE).count)
        For k = EMPLANTURE To SAUMON
            i = 0
            ListeDecoupe(k).MoveFirst
            Do
                Tableau(i).XOrigEmplanture = ListeDecoupe(k).CurrentPoint.x
                Tableau(i).YOrigEmplanture = ListeDecoupe(k).CurrentPoint.y
                Tableau(i).Vitesse_Temps = ListeDecoupe(k).CurrentVitesse
                i = i + 1
            Loop Until Not ListeDecoupe(k).MoveNext
        Next k

        nbPoints = DecalageV4(Tableau, -1, Vitesse, DVH, SH1, SH2)
        'Recr�ation de Tab_Chariot
        ReDim Tab_Chariot(0 To nbPoints - 1)
        For i = 0 To nbPoints - 1
            Tab_Chariot(i).P_abs(EMPLANTURE).x = Tableau(i).XOrigEmplanture
            Tab_Chariot(i).P_abs(EMPLANTURE).y = Tableau(i).YOrigEmplanture
            Tab_Chariot(i).P_abs(SAUMON).x = Tableau(i).XOrigSaumon
            Tab_Chariot(i).P_abs(SAUMON).y = Tableau(i).YOrigSaumon
            Tab_Chariot(i).P_Decal(EMPLANTURE).x = Tableau(i).XDecalEmplanture
            Tab_Chariot(i).P_Decal(EMPLANTURE).y = Tableau(i).YDecalEmplanture
            Tab_Chariot(i).P_Decal(SAUMON).x = Tableau(i).XDecalSaumon
            Tab_Chariot(i).P_Decal(SAUMON).y = Tableau(i).YDecalSaumon
            Tab_Chariot(i).Temps = Tableau(i).Vitesse_Temps
        Next i
    Else
        'Calcul de la valeur du d�calage pour chaque segment
        Call Calcul_Valeur_Decalage(ListeDecoupe, FrmParam.Combo_Mat, _
            wChauffe)
        
        'Calcul d�calage : cr�ation du tableau Tab_Chariot
        Tab_Chariot() = Calcul_Decalage(ListeDecoupe, _
            wRayonCompense)
    End If
            
'    Debug.Print "Avant Suppression"
'    For k = EMPLANTURE To SAUMON
'        Debug.Print IIf(k = EMPLANTURE, "EMPLANTURE", "SAUMON")
'        For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'            Debug.Print i, Tab_Chariot(i).P_Decal(k).X, Tab_Chariot(i).P_Decal(k).Y, Tab_Chariot(i).Temps, Tab_Chariot(i).iOrigine
'        Next i
'    Next k
    
    'Suppression des points en double dans Tab_Chariot
    'a) On marque les points en double
    For i = LBound(Tab_Chariot) + 1 To UBound(Tab_Chariot)
        If Calcul_Distance(Tab_Chariot(i).P_Decal(EMPLANTURE), Tab_Chariot(i - 1).P_Decal(EMPLANTURE)) < 0.001 _
            And Calcul_Distance(Tab_Chariot(i).P_Decal(SAUMON), Tab_Chariot(i - 1).P_Decal(SAUMON)) < 0.001 _
            Then
            Tab_Chariot(i).iOrigine = -1
        End If
    Next i
    'b) On supprime les points marqu�s
    j = LBound(Tab_Chariot) + 1
    For i = LBound(Tab_Chariot) + 1 To UBound(Tab_Chariot)
        If Tab_Chariot(i).iOrigine <> -1 Then
            Tab_Chariot(j) = Tab_Chariot(i)
            j = j + 1
        End If
    Next i
    ReDim Preserve Tab_Chariot(LBound(Tab_Chariot) To j - 1)

'    Debug.Print "Apr�s Suppression"
'    For k = EMPLANTURE To SAUMON
'        Debug.Print IIf(k = EMPLANTURE, "EMPLANTURE", "SAUMON")
'        For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'            Debug.Print i, Tab_Chariot(i).P_Decal(k).X, Tab_Chariot(i).P_Decal(k).Y, Tab_Chariot(i).Temps, Tab_Chariot(i).iOrigine
'        Next i
'    Next k
    
    ReDim Preserve Tab_Chariot(LBound(Tab_Chariot) To j - 1)

    'Calcul de la trajectoire des chariots
    minX = Min(tabBlocInv(EMPLANTURE, 0).x, tabBlocInv(EMPLANTURE, 1).x)
    minX = Min(minX, Min(tabBlocInv(SAUMON, 0).x, tabBlocInv(SAUMON, 1).x))
    maxX = Max(tabBlocInv(EMPLANTURE, 0).x, tabBlocInv(EMPLANTURE, 1).x)
    maxX = Max(minX, Max(tabBlocInv(SAUMON, 0).x, tabBlocInv(SAUMON, 1).x))
    minY = tabBlocInv(EMPLANTURE, 2).y
    maxY = tabBlocInv(EMPLANTURE, 1).y
    Call Calcul_Trajectoire(Tab_Chariot, wOffset_Gauche, _
        wOffset_Droit, wEnvergure, wOption_GD, _
        minX, maxX, minY, maxY)
        
'    Debug.Print "Tab_Chariot apr�s calcul"
'    For k = EMPLANTURE To SAUMON
'        Debug.Print IIf(k = EMPLANTURE, "EMPLANTURE", "SAUMON")
'        For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'            Temp = 0#
'            If i > LBound(Tab_Chariot) Then Temp = Calcul_Distance(Tab_Chariot(i - 1).P_abs(k), Tab_Chariot(i).P_abs(k)) / Tab_Chariot(i).Temps
'            Debug.Print i, Tab_Chariot(i).P_abs(k).X, Tab_Chariot(i).P_abs(k).Y, Tab_Chariot(i).Temps, Tab_Chariot(i).iOrigine, Temp
'        Next i
'    Next k
        
    If bCalculPos Then Exit Sub
    
'    'Sortie fichier des coordonn�es
'    Dim toto As Integer
'    toto = toto + 1
'    Open "RP-FC_Dessin" & toto & ".txt" For Output As #1
'    Print #1, "Emplanture"
'    k = EMPLANTURE
'    Print #1, "P_Decal(k).x", "P_Decal(k).y", "P_abs(k).x", "P_abs(k).y", "Temps", "Vitesse"
'    For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'        Temp = 0#
'        If i > LBound(Tab_Chariot) Then Temp = Calcul_Distance(Tab_Chariot(i - 1).P_abs(k), Tab_Chariot(i).P_abs(k)) / Tab_Chariot(i).Temps
'        Print #1, Tab_Chariot(i).P_Decal(k).x, Tab_Chariot(i).P_Decal(k).y, Tab_Chariot(i).P_abs(k).x, Tab_Chariot(i).P_abs(k).y, Tab_Chariot(i).Temps, Temp
'    Next i
'    Print #1, "Saumon"
'    k = SAUMON
'    Print #1, "P_Decal(k).x", "P_Decal(k).y", "P_abs(k).x", "P_abs(k).y", "Temps", "Vitesse"
'    For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'        Temp = 0#
'        If i > LBound(Tab_Chariot) Then Temp = Calcul_Distance(Tab_Chariot(i - 1).P_abs(k), Tab_Chariot(i).P_abs(k)) / Tab_Chariot(i).Temps
'        Print #1, Tab_Chariot(i).P_Decal(k).x, Tab_Chariot(i).P_Decal(k).y, Tab_Chariot(i).P_abs(k).x, Tab_Chariot(i).P_abs(k).y, Tab_Chariot(i).Temps, Temp
'    Next i
'    Close #1
   
    'Diminution des vitesses pour les segments trop rapides :
    ' On parcourt Tab_Chariot et on change le temps dans ListeDecoupe
    If Ajustement_Vitesse_Traj(Tab_Chariot, gstrTable) Then
        GoTo Debut_Calcul
    End If
    
    'Contr�le des vitesses maximum des chariots
    If Not Ctrl_Vitesse_Traj(Tab_Chariot, gstrTable) Then
        MsgBox "Vitesse max d�pass�e"
    End If
    
    'Contr�le de la trajectoire des chariots
    wCourse_X = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_16", 0)
    wCourse_Y = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_17", 0)
    wErreur = Ctrl_Trajectoire(Tab_Chariot, wCourse_X, wCourse_Y, _
        Vitesse)
'    If wErreur <> "" Then
'            FrmErreurs.Show
'            FrmErreurs.WindowState = vbNormal
'            FrmParam.Btn_Decoupe.Enabled = False
'    Else
'            FrmErreurs.WindowState = vbMinimized
'            FrmParam.Btn_Decoupe.Enabled = True
'    End If
    If wErreur <> "" Then
'            Me.SB.SimpleText = wErreur
            Me.SB.Panels(5).Text = wErreur
            FrmParam.Btn_Decoupe.Enabled = False
    Else
'            Me.SB.SimpleText = ""
            Me.SB.Panels(5).Text = ""
            FrmParam.Btn_Decoupe.Enabled = True
    End If

'    'Sortie fichier des coordonn�es
'    Open "RP-FC_Ctrl.txt" For Output As #1
'    Print #1, "Emplanture"
'    k = EMPLANTURE
'    Print #1, "P_Decal(k).x", "P_Decal(k).y", "P_abs(k).x", "P_abs(k).y", "Temps", "Vitesse"
'    For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'        Temp = 0#
'        If i > LBound(Tab_Chariot) Then Temp = Calcul_Distance(Tab_Chariot(i - 1).P_abs(k), Tab_Chariot(i).P_abs(k)) / Tab_Chariot(i).Temps
'        Print #1, Tab_Chariot(i).P_Decal(k).x, Tab_Chariot(i).P_Decal(k).y, Tab_Chariot(i).P_abs(k).x, Tab_Chariot(i).P_abs(k).y, Tab_Chariot(i).Temps, Temp
'    Next i
'    Print #1, "Saumon"
'    k = SAUMON
'    Print #1, "P_Decal(k).x", "P_Decal(k).y", "P_abs(k).x", "P_abs(k).y", "Temps", "Vitesse"
'    For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'        Temp = 0#
'        If i > LBound(Tab_Chariot) Then Temp = Calcul_Distance(Tab_Chariot(i - 1).P_abs(k), Tab_Chariot(i).P_abs(k)) / Tab_Chariot(i).Temps
'        Print #1, Tab_Chariot(i).P_Decal(k).x, Tab_Chariot(i).P_Decal(k).y, Tab_Chariot(i).P_abs(k).x, Tab_Chariot(i).P_abs(k).y, Tab_Chariot(i).Temps, Temp
'    Next i
'    Close #1

End Sub

Public Sub Decoupe()
    'Lancement d�coupe
    Dim i As Long, k As Integer
    Dim minX As Single
    Dim maxX As Single
    Dim minY As Single
    Dim maxY As Single
    Dim Vmax As Single
    Dim DeltaX(EMPLANTURE To SAUMON) As Single
    Dim DeltaY(EMPLANTURE To SAUMON) As Single
    Dim Dist(EMPLANTURE To SAUMON) As Single
    Dim P1 As Point, P2 As Point
    
    Call maClasseHID.TableVerif
    
    SB.Panels(5).Text = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 253)
    
    Call Calcul(minX, maxX, minY, maxY, CDbl(FrmParam.TB_VDecoupe), False)
    
    Vmax = Min(CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 0)), _
        CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0)))

    FrmParam.Btn_Decoupe.Enabled = False
    'Chauffe
    SB.Panels(5).Text = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 251)
    Call maClasseHID.ChauffeEcrirePC(Val(FrmParam.lblChauffe))
    DeplEnCours = True
    gPause = False
    
    'On passe de coordonn�es absolues en coordonn�es relatives
    For k = EMPLANTURE To SAUMON
        DeltaX(k) = 0#
        DeltaY(k) = 0#

        For i = UBound(Tab_Chariot) To LBound(Tab_Chariot) + 1 Step -1
            Tab_Chariot(i).P_abs(k).x = Tab_Chariot(i).P_abs(k).x - Tab_Chariot(i - 1).P_abs(k).x
            Tab_Chariot(i).P_abs(k).y = Tab_Chariot(i).P_abs(k).y - Tab_Chariot(i - 1).P_abs(k).y
            If i > LBound(Tab_Chariot) Then
                DeltaX(k) = DeltaX(k) + Tab_Chariot(i).P_abs(k).x
                DeltaY(k) = DeltaY(k) + Tab_Chariot(i).P_abs(k).y
            End If
        Next i
        
        'Approche depuis le 0 jusqu'au bloc
        Tab_Chariot(LBound(Tab_Chariot)).P_abs(k).x = Tab_Chariot(LBound(Tab_Chariot)).P_abs(k).x
        DeltaX(k) = DeltaX(k) + Tab_Chariot(LBound(Tab_Chariot)).P_abs(k).x
        DeltaY(k) = DeltaY(k) + Tab_Chariot(LBound(Tab_Chariot)).P_abs(k).y
    Next k
        
    'Calcul de la vitesse du premier segment
    For k = EMPLANTURE To SAUMON
        P1.x = 0#
        P1.y = 0#
        P2.x = Tab_Chariot(LBound(Tab_Chariot)).P_abs(k).x
        P2.y = Tab_Chariot(LBound(Tab_Chariot)).P_abs(k).y
        Dist(k) = Calcul_Distance(P1, P2)
    Next k
    Tab_Chariot(LBound(Tab_Chariot)).Temps = Calcul_Temps_Distance(Max(Dist(EMPLANTURE), Dist(SAUMON)), Vmax)

    ReDim Preserve Tab_Chariot(LBound(Tab_Chariot) To UBound(Tab_Chariot) + 1)
    
    'Retour au z�ro
    For k = EMPLANTURE To SAUMON
        Tab_Chariot(UBound(Tab_Chariot)).P_abs(k).x = -DeltaX(k)
        Tab_Chariot(UBound(Tab_Chariot)).P_abs(k).y = -DeltaY(k)
        P1.x = 0#
        P1.y = 0#
        P2.x = DeltaX(k)
        P2.y = DeltaY(k)
        Dist(k) = Calcul_Distance(P1, P2)
    Next k
    Tab_Chariot(UBound(Tab_Chariot)).Temps = Calcul_Temps_Distance(Max(Dist(EMPLANTURE), Dist(SAUMON)), Vmax)
    
    If gbOrientationInv Then
        'La table est invers�e, il faut inverser le sens des X
        For k = EMPLANTURE To SAUMON
            For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
                Tab_Chariot(i).P_abs(k).x = -Tab_Chariot(i).P_abs(k).x
            Next i
        Next k
    End If
'    Dim TotX As Single, TotY As Single
'    For k = EMPLANTURE To SAUMON
'        TotX = 0#
'        TotY = 0#
'        For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'            Debug.Print IIf(k = EMPLANTURE, "EMPLANTURE", "SAUMON"), Tab_Chariot(i).P_abs(k).x, Tab_Chariot(i).P_abs(k).y, Tab_Chariot(i).Temps
'            TotX = TotX + Tab_Chariot(i).P_abs(k).x
'            TotY = TotY + Tab_Chariot(i).P_abs(k).y
'        Next i
'        Debug.Print "Totx : " & TotX & "   TotY : " & TotY
'    Next k
    
    Call Envoi_Decoupe(Tab_Chariot, Me, FrmEmplanture.PB, FrmSaumon.PB, FrmParam.Option_GD(0), True, UCase$(ReadINI("Debug", "Faux")) = "VRAI")
    
    FrmParam.Btn_Decoupe.Enabled = True
    Call FrameEnable(True, FrmParam)
    SB.Panels(5).Text = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 251)

    'S'il existe une tempo chauffe APRES
    Call maClasseHID.ChauffeEcrire(Round(Val(FrmParam.lblChauffe) * 255 / 100, 0), False)
    Call maClasseHID.ChauffeEcrire(0, True) 'L'arr�t de la chauffe est temporis�
    
    SB.Panels(5).Text = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 252)
    DeplEnCours = False
End Sub

Public Sub FrameEnable(bEnable As Boolean, f As Form)
    Dim Ctrl As Control
    For Each Ctrl In FrmParam.Controls
        If UCase$(Left(Ctrl.Name, 5)) = "FRAME" Then
            Ctrl.Enabled = bEnable
        End If
    Next
End Sub

Public Sub CreerListeDecoupe()
    Dim monElem As TypeNoeud
    Dim k As Integer
    Dim i As Long

    'Il faut cr�er ou recr�er une liste
    monElem.bSynchro = False
    monElem.bTemp = False
    For k = EMPLANTURE To SAUMON
        Set ListeDecoupe(k) = New ClsListeChainee
        'Ajout trajet dans le bloc (entr�e)
        monElem.x = tabBloc(k, 1).x
        If monElem.x <> monTableau1(k, LBound(monTableau1, 2)).x Then
            'Cas particulier quand pas de marge pour le bloc
            monElem.y = monTableau1(k, LBound(monTableau1, 2)).y
            ListeDecoupe(k).Insert monElem
            ListeDecoupe(k).MoveNext
        End If
        For i = LBound(monTableau1, 2) To UBound(monTableau1, 2)
            monElem.x = monTableau1(k, i).x
            monElem.y = monTableau1(k, i).y
            ListeDecoupe(k).Insert monElem
            ListeDecoupe(k).MoveNext
        Next i
        'Ajout trajet dans le bloc (sortie)
        monElem.x = tabBloc(k, 1).x
        If monElem.x <> monTableau1(k, UBound(monTableau1, 2)).x Then
            'Cas particulier quand pas de marge pour le bloc
            monElem.y = monTableau1(k, UBound(monTableau1, 2)).y
            ListeDecoupe(k).Insert monElem
            ListeDecoupe(k).MoveFirst
        End If
    Next k
    
    'Miroir horizontal de la liste de d�coupe
    xMin = Min(ListeDecoupe(EMPLANTURE).minX, ListeDecoupe(SAUMON).minX)
    xMax = Max(ListeDecoupe(EMPLANTURE).maxX, ListeDecoupe(SAUMON).maxX)
    For k = EMPLANTURE To SAUMON
        Call ListeDecoupe(k).MiroirH(xMin, xMax)
        Call ListeDecoupe(k).Decal(-xMin, 0#)
    Next k
    
    'Constitution de tabBlocInv
    For k = EMPLANTURE To SAUMON
        'Il ne faut pas retrancher wMargeBF, puisqu'on en a tenu compte pour le trajet de sortie du bloc
        tabBlocInv(k, 0).x = ListeDecoupe(k).minX
        tabBlocInv(k, 3).x = tabBlocInv(k, 0).x
        tabBlocInv(k, 4).x = tabBlocInv(k, 0).x
        tabBlocInv(k, 1).x = ListeDecoupe(k).maxX + wMargeBA(k)
        tabBlocInv(k, 2).x = tabBlocInv(k, 1).x
        For i = LBound(tabBloc, 2) To UBound(tabBloc, 2)
            tabBlocInv(k, i).y = tabBloc(k, i).y
        Next i
    Next k
End Sub

Public Sub DecalXY(x As Single, y As Single)
    Dim i As Integer, k As Integer
        
    If IsArrayNullChariot(Tab_Chariot) Then Exit Sub

    For k = EMPLANTURE To SAUMON
        'Liste de d�coupe
        Call ListeDecoupe(k).Decal(x, y)
        'Table du bloc
        For i = LBound(tabBlocInv, 2) To UBound(tabBlocInv, 2)
            tabBlocInv(k, i).x = tabBlocInv(k, i).x + x
            tabBlocInv(k, i).y = tabBlocInv(k, i).y + y
        Next i
        'D�placements des chariots
        For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
            Tab_Chariot(i).P_abs(k).x = Tab_Chariot(i).P_abs(k).x + x
            Tab_Chariot(i).P_abs(k).y = Tab_Chariot(i).P_abs(k).y + y
            Tab_Chariot(i).P_Decal(k).x = Tab_Chariot(i).P_Decal(k).x + x
            Tab_Chariot(i).P_Decal(k).y = Tab_Chariot(i).P_Decal(k).y + y
        Next i
    Next k
End Sub

