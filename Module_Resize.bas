Attribute VB_Name = "Module_Resize"
Option Explicit
DefBool B

Type POINTAPI
    X As Long
    Y As Long
End Type

Type MINMAXINFO
    ptReserved As POINTAPI
    ptMaxSize As POINTAPI
    ptMaxPosition As POINTAPI
    ptMinTrackSize As POINTAPI
    ptMaxTrackSize As POINTAPI
End Type

Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (pDest As Any, pSrc As Any, ByVal cbSrc As Long)

Public Const WM_GETMINMAXINFO = &H24
Public Const GWL_WNDPROC = (-4)

Dim lpPrevWndProc As Long

Public bLimitMaxSize, bLimitMinSize, bUseMaximizedPos, bUseMaximizedSize
Public ptLimitMaxSize As POINTAPI, ptLimitMinSize As POINTAPI
Public ptMaximizedPos As POINTAPI, ptMaximizedSize As POINTAPI

Function GetPOINTAPI(ByVal X As Long, ByVal Y As Long) As POINTAPI
    GetPOINTAPI.X = X
    GetPOINTAPI.Y = Y
End Function

Function WindowProc(ByVal hWnd As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim tMinMaxInfo As MINMAXINFO
    
    If uMsg = WM_GETMINMAXINFO Then
        Call CopyMemory(tMinMaxInfo, ByVal lParam, Len(tMinMaxInfo))
        
        If bLimitMaxSize Then tMinMaxInfo.ptMaxTrackSize = ptLimitMaxSize
        If bLimitMinSize Then tMinMaxInfo.ptMinTrackSize = ptLimitMinSize
        If bUseMaximizedPos Then tMinMaxInfo.ptMaxPosition = ptMaximizedPos
        If bUseMaximizedSize Then tMinMaxInfo.ptMaxSize = ptMaximizedSize
        
        Call CopyMemory(ByVal lParam, tMinMaxInfo, Len(tMinMaxInfo))
        Exit Function
    End If
    
    WindowProc = CallWindowProc(lpPrevWndProc, hWnd, uMsg, wParam, lParam)
End Function

Sub SubClass(ByVal hWnd As Long)
    lpPrevWndProc = SetWindowLong(hWnd, GWL_WNDPROC, AddressOf WindowProc)
End Sub

Sub UnSubClass(ByVal hWnd As Long)
    Call SetWindowLong(hWnd, GWL_WNDPROC, lpPrevWndProc)
End Sub


