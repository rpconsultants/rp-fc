VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmPilManClavier 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Pilotage clavier"
   ClientHeight    =   7440
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3945
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   18
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   FontTransparent =   0   'False
   ForeColor       =   &H00FFFFFF&
   Icon            =   "FrmPilManClavier.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "FrmPilManClavier.frx":058A
   ScaleHeight     =   7440
   ScaleWidth      =   3945
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.Frame FrameValeur 
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   0
      TabIndex        =   20
      Top             =   2880
      Visible         =   0   'False
      Width           =   3855
      Begin VB.TextBox Text2 
         Alignment       =   2  'Center
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   0
         TabIndex        =   25
         Tag             =   "0"
         Text            =   "Y"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   0
         TabIndex        =   24
         Tag             =   "0"
         Text            =   "X"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   855
      End
      Begin VB.TextBox TB_ValeurY 
         Alignment       =   2  'Center
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         TabIndex        =   23
         Tag             =   "0"
         Text            =   "20"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   2055
      End
      Begin VB.TextBox TB_ValeurX 
         Alignment       =   2  'Center
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         TabIndex        =   22
         Tag             =   "0"
         Text            =   "20"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   2055
      End
      Begin VB.CommandButton Btn_OK 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFF80&
         Default         =   -1  'True
         Height          =   735
         Left            =   3120
         Picture         =   "FrmPilManClavier.frx":5105
         Style           =   1  'Graphical
         TabIndex        =   21
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   735
      End
   End
   Begin VB.Timer TimerFleche 
      Enabled         =   0   'False
      Interval        =   150
      Left            =   120
      Top             =   4440
   End
   Begin MSComctlLib.StatusBar SB 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   19
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   7185
      Width           =   3945
      _ExtentX        =   6959
      _ExtentY        =   450
      Style           =   1
      SimpleText      =   "..."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Picture         =   "FrmPilManClavier.frx":599E
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Timer TimerFlash 
      Enabled         =   0   'False
      Interval        =   250
      Left            =   120
      Top             =   3960
   End
   Begin VB.Timer TimerCarre 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   120
      Top             =   3480
   End
   Begin VB.Frame FrameAide 
      Caption         =   "Aide (F1 =retour)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4095
      Left            =   3480
      TabIndex        =   1
      Tag             =   "10"
      ToolTipText     =   "0"
      Top             =   4440
      Visible         =   0   'False
      Width           =   2655
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   16
         Tag             =   "114"
         ToolTipText     =   "0"
         Top             =   3840
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   15
         Tag             =   "100"
         ToolTipText     =   "0"
         Top             =   3600
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   14
         Tag             =   "113"
         ToolTipText     =   "0"
         Top             =   3360
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   13
         Tag             =   "112"
         ToolTipText     =   "0"
         Top             =   3120
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   12
         Tag             =   "111"
         ToolTipText     =   "0"
         Top             =   2880
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   11
         Tag             =   "110"
         ToolTipText     =   "0"
         Top             =   2640
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   10
         Tag             =   "109"
         ToolTipText     =   "0"
         Top             =   2400
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   9
         Tag             =   "108"
         ToolTipText     =   "0"
         Top             =   2160
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   8
         Tag             =   "107"
         ToolTipText     =   "0"
         Top             =   1920
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   7
         Tag             =   "106"
         ToolTipText     =   "0"
         Top             =   1680
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   6
         Tag             =   "105"
         ToolTipText     =   "0"
         Top             =   1440
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   5
         Tag             =   "104"
         ToolTipText     =   "0"
         Top             =   1200
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   4
         Tag             =   "103"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   3
         Tag             =   "102"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   3495
      End
      Begin VB.Label LabelAide 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Tag             =   "101"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   3495
      End
   End
   Begin VB.TextBox TB_Vitesse_Manuel 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   540
      Left            =   840
      TabIndex        =   0
      Tag             =   "0"
      Text            =   "4"
      ToolTipText     =   "0"
      Top             =   240
      Width           =   2295
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   19
      Left            =   3360
      Picture         =   "FrmPilManClavier.frx":5D5A
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   960
      Width           =   555
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   9
      Left            =   3360
      Picture         =   "FrmPilManClavier.frx":60CA
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   960
      Width           =   555
   End
   Begin VB.Label Lbl_Nettoyage 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Nettoyage fil"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Tag             =   "200"
      ToolTipText     =   "0"
      Top             =   6600
      Width           =   1695
   End
   Begin VB.Image Image1 
      Height          =   795
      Index           =   0
      Left            =   0
      Picture         =   "FrmPilManClavier.frx":6490
      Stretch         =   -1  'True
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   6360
      Width           =   1965
   End
   Begin VB.Image Img_Fleche 
      Height          =   735
      Index           =   1
      Left            =   3120
      Picture         =   "FrmPilManClavier.frx":68A6
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   120
      Width           =   855
   End
   Begin VB.Image Img_Fleche 
      Height          =   735
      Index           =   0
      Left            =   0
      Picture         =   "FrmPilManClavier.frx":6D7E
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   120
      Width           =   855
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   14
      Left            =   2520
      Picture         =   "FrmPilManClavier.frx":724A
      ToolTipText     =   "0"
      Top             =   960
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   4
      Left            =   2520
      Picture         =   "FrmPilManClavier.frx":76E2
      ToolTipText     =   "0"
      Top             =   960
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   13
      Left            =   1680
      Picture         =   "FrmPilManClavier.frx":7C90
      ToolTipText     =   "0"
      Top             =   960
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   3
      Left            =   1680
      Picture         =   "FrmPilManClavier.frx":8023
      ToolTipText     =   "0"
      Top             =   960
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   12
      Left            =   840
      Picture         =   "FrmPilManClavier.frx":8423
      ToolTipText     =   "0"
      Top             =   960
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   2
      Left            =   840
      Picture         =   "FrmPilManClavier.frx":884F
      ToolTipText     =   "0"
      Top             =   960
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   11
      Left            =   0
      Picture         =   "FrmPilManClavier.frx":8D61
      ToolTipText     =   "0"
      Top             =   960
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   1
      Left            =   0
      Picture         =   "FrmPilManClavier.frx":916A
      ToolTipText     =   "0"
      Top             =   960
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   15
      Left            =   120
      Picture         =   "FrmPilManClavier.frx":9631
      ToolTipText     =   "0"
      Top             =   2040
      Width           =   900
   End
   Begin VB.Image Carre 
      Appearance      =   0  'Flat
      Height          =   795
      Index           =   17
      Left            =   3000
      Picture         =   "FrmPilManClavier.frx":9B47
      ToolTipText     =   "0"
      Top             =   2040
      Width           =   870
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   16
      Left            =   1080
      Picture         =   "FrmPilManClavier.frx":A02A
      ToolTipText     =   "0"
      Top             =   2040
      Width           =   1845
   End
   Begin VB.Label Lbl_Stop 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "STOP"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   1200
      TabIndex        =   18
      Tag             =   "201"
      ToolTipText     =   "0"
      Top             =   3960
      Width           =   1695
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   6
      Left            =   1080
      Picture         =   "FrmPilManClavier.frx":A751
      ToolTipText     =   "0"
      Top             =   1800
      Width           =   1845
   End
   Begin VB.Image Carre 
      Appearance      =   0  'Flat
      Height          =   795
      Index           =   7
      Left            =   3000
      Picture         =   "FrmPilManClavier.frx":AFB0
      ToolTipText     =   "0"
      Top             =   1800
      Width           =   885
   End
   Begin VB.Image Carre 
      Height          =   795
      Index           =   5
      Left            =   120
      Picture         =   "FrmPilManClavier.frx":B53A
      ToolTipText     =   "0"
      Top             =   1800
      Width           =   885
   End
End
Attribute VB_Name = "FrmPilManClavier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim iGD As Integer
Dim AncieniGD As Integer
Dim DistX As Double
Dim DistY As Double
Dim Pas As Integer
Dim AncienPas As Integer
Dim CarreHeight() As Single

Private Type PtRem
    Pt As Point
    KeyCode As Integer
End Type
Dim TabPtRem() As PtRem
Dim maValeurX As Single
Dim maValeurY As Single
Dim Vitesse As Single
Dim bControl As Boolean
Dim bShift As Boolean
Dim bAdd As Boolean             'Touche fl�che '+'

Private Sub Btn_OK_Click()
    maValeurX = CSng(TB_ValeurX)
    maValeurY = CSng(TB_ValeurY)
    Pas = 0
    Call AffichagePas
    FrameValeur.Visible = False
End Sub

Private Sub Carre_Click(Index As Integer)
    Select Case Index
        'Pas
        Case 11
            Call Form_KeyDown(vbKeyF5, 0)
        Case 12
            Call Form_KeyDown(vbKeyF6, 0)
        Case 13
            Call Form_KeyDown(vbKeyF7, 0)
        Case 14
            Call Form_KeyDown(vbKeyF8, 0)
        Case 9, 19  'Saisie valeur
            Call Form_KeyDown(vbKeyV, 0)
        'G, GD, D
        Case 15
            Call Form_KeyDown(vbKeyG, 0)
        Case 16
            Call Form_KeyDown(vbKeySpace, 0)
        Case 17
            Call Form_KeyDown(vbKeyD, 0)
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    Dim Depl As Single
    Dim TpsReel As Single
    Dim VitesseReelle As Single
    Dim XD As Double
    Dim YD As Double
    Dim XG As Double
    Dim YG As Double
    Dim P1 As Point, P2 As Point
    
'    MsgBox KeyCode
    
    Select Case KeyCode
        Case vbKeyEscape
            gPause = True
            Call Form_KeyUp(vbKeyEscape, 0)
            Exit Sub
        Case vbKeyShift         'Vitesse / 20
            TB_Vitesse_Manuel.BackColor = vbGreen
            bShift = True
            Call AffichagePas
            Exit Sub
        Case vbKeyControl       'Vitesse *2
            TB_Vitesse_Manuel.BackColor = vbRed
            bControl = True
            Call AffichagePas
            Exit Sub
        Case vbKeyF1            'Aide
            FrameAide.Visible = Not FrameAide.Visible
            Exit Sub
        Case vbKeySubtract       'Vitesse - 0.5
            Vitesse = Max(0.5, Vitesse - 0.5)
            Call AffichagePas
            Exit Sub
        Case vbKeyAdd           'Vitesse + 0.5
            Vitesse = Min(Min(CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 1)), CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 1))), Vitesse + 0.5)
            Call AffichagePas
            Exit Sub
        Case vbKeyF5            'Continu
            Pas = 1
            CarreHeight(1) = 0
            Call AffichagePas
            Exit Sub
        Case vbKeyF6            '10
            Pas = 2
            Call AffichagePas
            Exit Sub
        Case vbKeyF7            '1
            Pas = 3
            Call AffichagePas
            Exit Sub
        Case vbKeyF8            '0.1
            Pas = 4
            Call AffichagePas
            Exit Sub
        Case vbKeyG             'Gauche
            iGD = 0
            Call AffichagePas
            Exit Sub
        Case vbKeyD             'Droit
            iGD = 2
            Call AffichagePas
            Exit Sub
        Case vbKeySpace         'Droit et Gauche
            iGD = 1
            Call AffichagePas
            Exit Sub
        Case vbKeyN             'Nettoyage fil
            If Not maClasseHID.IsAvailable Then Exit Sub
            Call NettoyageFil
            Exit Sub
        Case vbKeyV             'Valeur
            FrameValeur.Visible = True
            Pas = 0
            CarreHeight(9) = 0
            Call AffichagePas
            Exit Sub
    End Select
    
    'On va "illuminer" le point de l'�cran qui correspond � la touche
    'Parcours de TabPtRem
    For i = LBound(TabPtRem) To UBound(TabPtRem)
        If TabPtRem(i).KeyCode = KeyCode Then
            Circle (TabPtRem(i).Pt.x, TabPtRem(i).Pt.y), 200, vbWhite
            TimerFlash.Enabled = True
            Exit For
        End If
    Next i
    
    If DeplEnCours Then Exit Sub
    If Not maClasseHID.TableVerif Then Exit Sub
    DeplEnCours = True
    
    'Valeur de Dist
    Select Case Pas
        Case 0      'Valeur
            Call TB_ValeurX_Validate(False)
            Call TB_ValeurY_Validate(False)
            DistX = maValeurX
            DistY = maValeurY
        Case 1      'Continu
            DistX = 2000
            DistY = 2000
        Case 2      '10
            DistX = 10
            DistY = 10
        Case 3      '1
            DistX = 1
            DistY = 1
        Case 4      '0,1
            DistX = 0.1
            DistY = 0.1
    End Select
    
    Select Case KeyCode
        Case vbKeyNumpad7, vbKeyNumpad8, vbKeyNumpad9, vbKeyHome, vbKeyUp, vbKeyPageUp
            If iGD <= 1 Then YG = DistY
            If iGD >= 1 Then YD = DistY
        Case vbKeyNumpad4, vbKeyNumpad6, vbKeyLeft, vbKeyRight
            If iGD <= 1 Then YG = 0#
            If iGD >= 1 Then YD = 0#
        Case vbKeyNumpad1, vbKeyNumpad2, vbKeyNumpad3, vbKeyEnd, vbKeyDown, vbKeyPageDown
            If iGD <= 1 Then YG = -DistY
            If iGD >= 1 Then YD = -DistY
        Case Else
            Exit Sub
    End Select
    
    Select Case KeyCode
        Case vbKeyNumpad7, vbKeyNumpad4, vbKeyNumpad1, vbKeyHome, vbKeyLeft, vbKeyEnd
            If iGD <= 1 Then XG = -DistX
            If iGD >= 1 Then XD = -DistX
        Case vbKeyNumpad8, vbKeyNumpad2, vbKeyUp, vbKeyDown
            If iGD <= 1 Then XG = 0#
            If iGD >= 1 Then XD = 0#
        Case vbKeyNumpad9, vbKeyNumpad6, vbKeyNumpad3, vbKeyPageUp, vbKeyRight, vbKeyPageDown
            If iGD <= 1 Then XG = DistX
            If iGD >= 1 Then XD = DistX
    End Select
    P1.x = 0#
    P1.y = 0#
    P2.x = Max(Abs(XD), Abs(XG))
    P2.y = Max(Abs(YD), Abs(YG))
    
'    If gbOrientationInv Then        'Inversion table
'        XD = -XD
'        XG = -XG
'    End If
    
    gPause = False
    
    VitesseReelle = Vitesse
    If bShift Then VitesseReelle = Vitesse / 20#
    If bControl Then VitesseReelle = Min(Min(CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 1)), CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 1))), Vitesse * 2)

    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 208)
    
    ReDim Tab_Chariot(1 To 1)
    Tab_Chariot(1).P_abs(EMPLANTURE).x = XD
    Tab_Chariot(1).P_abs(EMPLANTURE).y = YD
    Tab_Chariot(1).P_abs(SAUMON).x = XG
    Tab_Chariot(1).P_abs(SAUMON).y = YG
    Tab_Chariot(1).Temps = Calcul_Temps_Points(P1, P2, VitesseReelle)
    Call Envoi_Decoupe(Tab_Chariot, Me)
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyShift Or KeyCode = vbKeyControl Then        'Vitesse
        If KeyCode = vbKeyShift Then bShift = False
        If KeyCode = vbKeyControl Then bControl = False
        Call AffichagePas
        DoEvents
        Exit Sub
    End If
    
    TimerFlash.Enabled = True
    
    If Pas <> 1 And Not gPause Then Exit Sub 'D�placement infini
    If KeyCode = vbKeyEscape Then Lbl_Stop.BackStyle = vbBSSolid
    gPause = True
    DeplEnCours = False
    If Not maClasseHID.IsAvailable Then Exit Sub
    Call maClasseHID.StopResume(FastStop, 0)
    Call maClasseHID.DemandeInformation
    While maClasseHID.Step_Processed
        DoEvents
        Call maClasseHID.DemandeInformation
        DoEvents
    Wend
    DeplEnCours = False
    gPause = False
End Sub

Private Sub NettoyageFil()
    Dim Tempo As Single
    Dim y As Single
    Dim P As Point
    Dim P1 As Point
    
    maClasseHID.DemandeInformation
    If maClasseHID.Step_Processed Then Exit Sub
    
    Lbl_Nettoyage.BackStyle = vbBSSolid
    DoEvents
    P1.x = 0#
    P1.y = 0#
    P.x = 0#
    y = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_34", 50)
    P.y = y
    Tempo = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_35", 3)
    gPause = False
    
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrirePC(100)
    
    If Tempo <> 0 Then
        ReDim Tab_Chariot(1 To 4)
        Tab_Chariot(1).P_abs(EMPLANTURE) = P
        Tab_Chariot(1).P_abs(SAUMON) = P
        Tab_Chariot(1).Temps = Calcul_Temps_Points(P, P1, Vitesse)
        
        P.y = 0.1
        Tab_Chariot(2).P_abs(EMPLANTURE) = P
        Tab_Chariot(2).P_abs(SAUMON) = P
        Tab_Chariot(2).Temps = Tempo / 2#
        
        P.y = -0.1
        Tab_Chariot(3).P_abs(EMPLANTURE) = P
        Tab_Chariot(3).P_abs(SAUMON) = P
        Tab_Chariot(3).Temps = Tempo / 2#
        
        P.x = 0#
        P.y = -y
        Tab_Chariot(4).P_abs(EMPLANTURE) = P
        Tab_Chariot(4).P_abs(SAUMON) = P
        Tab_Chariot(4).Temps = Tab_Chariot(1).Temps
    
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 303)
        Call Envoi_Decoupe(Tab_Chariot, Me)
    
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
        Call maClasseHID.ChauffeEcrirePC(0)
    Else
        ReDim Tab_Chariot(1 To 1)
        P.x = 0#
        P.y = y
        Tab_Chariot(1).P_abs(EMPLANTURE) = P
        Tab_Chariot(1).P_abs(SAUMON) = P
        Tab_Chariot(1).Temps = Calcul_Temps_Points(P, P1, Vitesse)
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 303)
        Call Envoi_Decoupe(Tab_Chariot, Me)
        
        MsgBox ReadINI_Trad(gLangue, "erreurs", 76), vbQuestion + vbOKOnly
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
        Call maClasseHID.ChauffeEcrirePC(0)

        P.y = -y
        Tab_Chariot(1).P_abs(EMPLANTURE) = P
        Tab_Chariot(1).P_abs(SAUMON) = P
        Call Envoi_Decoupe(Tab_Chariot, Me)
    End If
    SB.SimpleText = "..."
    Lbl_Nettoyage.BackStyle = vbTransparent
End Sub

Private Sub Form_Activate()
    Dim r As Long
    Const HWND_TOPMOST = -1
    Const HWND_NOTOPMOST = -2
    Const SWP_NOMOVE = &H2
    Const SWP_NOSIZE = &H1

    r = SetWindowPos(Me.hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    ChargeTrad Me
    Set gFenetreHID = FrmPilManClavier
    Call maClasseHID.TableVerif

    
    With Me
        Left = ReadINI_Num(Me.Name & "_Left", Me.Left)
        Top = ReadINI_Num(Me.Name & "_Top", Me.Top)
    End With
    On Error Resume Next
    ReDim CarreHeight(Carre.LBound To Carre.UBound)
    
    Carre(15).Top = Carre(5).Top
    Carre(15).Left = Carre(5).Left
    Carre(16).Top = Carre(6).Top
    Carre(16).Left = Carre(6).Left
    Carre(17).Top = Carre(7).Top
    Carre(17).Left = Carre(7).Left

    Vitesse = ReadINI_Num(Me.Name & "_Vitesse", 4)
    FrameValeur.Left = 0
    FrameValeur.Top = Carre(1).Top
    DistX = ReadINI_Num(Me.Name & "_DistX", 1)
    DistY = ReadINI_Num(Me.Name & "_DistY", 1)
    Pas = ReadINI_Num(Me.Name & "_Pas", 1)
    iGD = 1
    AncienPas = -1
    maValeurX = ReadINI_Num(Me.Name & "_ValeurX", 20)
    maValeurY = ReadINI_Num(Me.Name & "_ValeurY", 20)
    TB_ValeurX = maValeurX
    TB_ValeurY = maValeurY
    Call AffichagePas
    FrameAide.Left = 0
    FrameAide.Width = Me.Width
    FrameAide.Top = 3120
    
    ReDim TabPtRem(1 To 17)
    'Vitesse - et +
    TabPtRem(1).KeyCode = vbKeySubtract
    TabPtRem(1).Pt.x = 720
    TabPtRem(1).Pt.y = 480
    TabPtRem(2).KeyCode = vbKeyAdd
    TabPtRem(2).Pt.x = 3315
    TabPtRem(2).Pt.y = 480
    
    'Clavier num�rique
    TabPtRem(3).KeyCode = vbKeyNumpad8
    TabPtRem(3).Pt.x = 2025
    TabPtRem(3).Pt.y = 2985
    TabPtRem(4).KeyCode = vbKeyNumpad9
    TabPtRem(4).Pt.x = 2835
    TabPtRem(4).Pt.y = 3315
    TabPtRem(5).KeyCode = vbKeyNumpad6
    TabPtRem(5).Pt.x = 3120
    TabPtRem(5).Pt.y = 4065
    TabPtRem(6).KeyCode = vbKeyNumpad3
    TabPtRem(6).Pt.x = 2835
    TabPtRem(6).Pt.y = 4830
    TabPtRem(7).KeyCode = vbKeyNumpad2
    TabPtRem(7).Pt.x = 2025
    TabPtRem(7).Pt.y = 5175
    TabPtRem(8).KeyCode = vbKeyNumpad1
    TabPtRem(8).Pt.x = 1200
    TabPtRem(8).Pt.y = 4830
    TabPtRem(9).KeyCode = vbKeyNumpad4
    TabPtRem(9).Pt.x = 900
    TabPtRem(9).Pt.y = 4065
    TabPtRem(10).KeyCode = vbKeyNumpad7
    TabPtRem(10).Pt.x = 1200
    TabPtRem(10).Pt.y = 3345
    
    'Carr�s de pas
    TabPtRem(11).KeyCode = vbKeyF5
    TabPtRem(11).Pt.x = 660
    TabPtRem(11).Pt.y = 1335
    TabPtRem(12).KeyCode = vbKeyF6
    TabPtRem(12).Pt.x = 1590
    TabPtRem(12).Pt.y = 1335
    TabPtRem(13).KeyCode = vbKeyF7
    TabPtRem(13).Pt.x = 2505
    TabPtRem(13).Pt.y = 1335
    TabPtRem(14).KeyCode = vbKeyF8
    TabPtRem(14).Pt.x = 3405
    TabPtRem(14).Pt.y = 1335
    
    'Carr�s GD
    TabPtRem(15).KeyCode = vbKeyG
    TabPtRem(15).Pt.x = 510
    TabPtRem(15).Pt.y = 6750
    TabPtRem(16).KeyCode = vbKeySpace
    TabPtRem(16).Pt.x = 2025
    TabPtRem(16).Pt.y = 6750
    TabPtRem(17).KeyCode = vbKeyD
    TabPtRem(17).Pt.x = 3495
    TabPtRem(17).Pt.y = 6750
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    'MsgBox "X = " & x & "              Y = " & y
    Dim i As Integer
    Dim minI As Integer
    Dim MinDist As Double
    Dim TempDist As Double
    Dim P1 As Point
    
    minI = LBound(TabPtRem)
    P1.x = x
    P1.y = y
    MinDist = Calcul_Distance(P1, TabPtRem(LBound(TabPtRem)).Pt)
    
    For i = LBound(TabPtRem) + 1 To UBound(TabPtRem)
        TempDist = Calcul_Distance(P1, TabPtRem(i).Pt)
        If TempDist < MinDist Then
            MinDist = TempDist
            minI = i
        End If
    Next i
    
    If MinDist < 400# Then
        Circle (TabPtRem(minI).Pt.x, TabPtRem(minI).Pt.y), 100, vbWhite
        TimerFlash.Enabled = True
        Call Form_KeyDown(TabPtRem(minI).KeyCode, Shift)
    End If
End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Pas = 1 Then Call Form_KeyUp(1, 0)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
    End If
    Call WriteINI(Me.Name & "_Vitesse", Str(Vitesse))
    Call WriteINI(Me.Name & "_ValeurX", Str(maValeurX))
    Call WriteINI(Me.Name & "_ValeurY", Str(maValeurY))
    Call WriteINI(Me.Name & "_DistX", Str(DistX))
    Call WriteINI(Me.Name & "_DistY", Str(DistY))
    Call WriteINI(Me.Name & "_Pas", Str(Pas))
End Sub

Private Sub Img_Fleche_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim P As POINTAPI
    Static Mult As Boolean

    'Ci-dessous pour autoriser des clics rapides
    Call GetCursorPos(P)
    If Mult Then
        Call SetCursorPos(P.x + 3, P.y + 3)
    Else
        Call SetCursorPos(P.x - 3, P.y - 3)
    End If
    Mult = Not Mult
    
    Call Form_KeyDown(IIf(Index = 0, vbKeySubtract, vbKeyAdd), 0)
    bAdd = (Index = 1)
    TimerFleche.Enabled = True
End Sub

Private Sub Lbl_Valeur_Click()
    Call Form_KeyDown(vbKeyV, 0)
End Sub

Private Sub Img_Fleche_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    TimerFleche.Enabled = False
End Sub

Private Sub Lbl_Nettoyage_Click()
    Call Form_KeyDown(vbKeyN, 0)
End Sub

Private Sub Lbl_Stop_Click()
    Call Form_KeyDown(vbKeyEscape, 0)
End Sub

Private Sub TB_ValeurX_GotFocus()
    TB_ValeurX.SelStart = 0
    TB_ValeurX.SelLength = Len(TB_ValeurX.Text)
End Sub

Private Sub TB_Valeury_GotFocus()
    TB_ValeurY.SelStart = 0
    TB_ValeurY.SelLength = Len(TB_ValeurY.Text)
End Sub

Private Sub TB_ValeurX_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) = "-" Then
        KeyAscii = 0
        Exit Sub
    End If
    If KeyAscii = vbKeyReturn Then TB_ValeurX_Validate (True)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_ValeurY_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) = "-" Then
        KeyAscii = 0
        Exit Sub
    End If
    If KeyAscii = vbKeyReturn Then TB_ValeurY_Validate (True)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_ValeurX_Validate(Cancel As Boolean)
    maValeurX = CSng(TB_ValeurX)
    Pas = 0
    Call AffichagePas
End Sub

Private Sub TB_ValeurY_Validate(Cancel As Boolean)
    maValeurY = CSng(TB_ValeurY)
    Pas = 0
    Call AffichagePas
End Sub

Private Sub AffichagePas()
    Dim i As Integer
    
    TB_Vitesse_Manuel = Vitesse
    TB_Vitesse_Manuel.BackColor = &HC0FFC0
    If bShift Then
        TB_Vitesse_Manuel = Vitesse / 10
        TB_Vitesse_Manuel.BackColor = vbGreen
    End If
    If bControl Then
        TB_Vitesse_Manuel = Min(Min(CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 1)), CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 1))), Vitesse * 2)
        TB_Vitesse_Manuel.BackColor = vbRed
    End If
    TB_Vitesse_Manuel = TB_Vitesse_Manuel & " mm/s"
    
    'Boutons des pas (Inf, 10, 1, 0.1)
    If Pas <> AncienPas Then
        For i = 1 To 4
            CarreHeight(i) = IIf(Pas = i, 795, 0)   'Hauteur � obtenir (voir TimerCarre)
            CarreHeight(i + 10) = IIf(Pas <> i, 795, 0)
            Carre(i).ZOrder (IIf(Pas = i, 0, 1))
            Carre(i).Height = 795 - CarreHeight(i)  'Hauteur actuelle
        Next i
        CarreHeight(9) = IIf(Pas = 0, 795, 0)   'Hauteur � obtenir (voir TimerCarre)
        CarreHeight(9 + 10) = IIf(Pas <> 0, 795, 0)
        Carre(9).ZOrder (IIf(Pas = 0, 0, 1))
        Carre(9).Height = 795 - CarreHeight(9)  'Hauteur actuelle
        AncienPas = Pas
    End If
    
    'Boutons Gauche-droite
    If iGD <> AncieniGD Then
        CarreHeight(5) = IIf(iGD = 0, 795, 0)   'Hauteur � obtenir (voir TimerCarre)
        CarreHeight(6) = IIf(iGD = 1, 795, 0)   'Hauteur � obtenir (voir TimerCarre)
        CarreHeight(7) = IIf(iGD = 2, 795, 0)   'Hauteur � obtenir (voir TimerCarre)
        Carre(5).ZOrder (IIf(iGD = 0, 0, 1))
        Carre(6).ZOrder (IIf(iGD = 1, 0, 1))
        Carre(7).ZOrder (IIf(iGD = 2, 0, 1))
        Carre(5).Height = 795 - CarreHeight(5)  'Hauteur actuelle
        Carre(6).Height = 795 - CarreHeight(6)  'Hauteur actuelle
        Carre(7).Height = 795 - CarreHeight(7)  'Hauteur actuelle
        CarreHeight(15) = IIf(iGD <> 0, 795, 0)   'Hauteur � obtenir (voir TimerCarre)
        CarreHeight(16) = IIf(iGD <> 1, 795, 0)   'Hauteur � obtenir (voir TimerCarre)
        CarreHeight(17) = IIf(iGD <> 2, 795, 0)   'Hauteur � obtenir (voir TimerCarre)
        AncieniGD = iGD
    End If
    TimerCarre.Enabled = True
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 303)
    Select Case Pas
        Case 0      'Valeur
            SB.SimpleText = SB.SimpleText & " X=" & maValeurX & " mm" & " Y=" & maValeurY & " mm"
        Case 1      'Infini
            SB.SimpleText = SB.SimpleText & "..."
        Case 2      '10
            SB.SimpleText = SB.SimpleText & " = 10 mm"
        Case 3      '1
            SB.SimpleText = SB.SimpleText & " = 1 mm"
        Case 4      '0.1
            SB.SimpleText = SB.SimpleText & " = 0.1 mm"
    End Select
End Sub

Private Sub TimerCarre_Timer()
    Dim i As Integer
    
    On Error Resume Next
    
    For i = Carre.LBound To Carre.UBound
        If Carre(i).Height <> CarreHeight(i) Then
            If CarreHeight(i) <> 0 Then
                Carre(i).Height = Min(795, Carre(i).Height + 20)
            End If
        End If
    Next i
End Sub

Private Sub TimerFlash_Timer()
    TimerFlash.Enabled = False
    Lbl_Stop.BackStyle = vbTransparent
    Me.Refresh
End Sub

Private Sub TimerFleche_Timer()
    'Utilis� pour la r�p�tition des fl�ches de vitesse
    Call Form_KeyDown(IIf(bAdd, vbKeyAdd, vbKeySubtract), 0)
End Sub
