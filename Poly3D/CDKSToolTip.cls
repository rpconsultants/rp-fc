VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDKSToolTip"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'====================================================================================================
'Nom du programme : DKS_ToolTip
'Version : 1.0.0
'Auteurs : Nicolas Teillet
'Environnement de d�veloppement : Visual basic 6.0
'R�sum� : objet permettant d'attribuer une tooltip personnalisable � un contr�le
'====================================================================================================
'====================================================================================================
'Nom du fichier : CDKSToolTip.cls
'Cr�e le        : 30/12/2004
'R�le           : Module de classe contenant l'objet de tooltip personnalisable
'====================================================================================================

Option Explicit

'd�clarations des fonctions de l'API Windows priv�es � l'objet
'fonction permettant de cr�er une fen�tre
Private Declare Function CreateWindowEx Lib "user32" Alias "CreateWindowExA" ( _
ByVal dwExStyle As Long, _
ByVal lpClassName As String, _
ByVal lpWindowName As String, _
ByVal dwStyle As Long, _
ByVal X As Long, _
ByVal Y As Long, _
ByVal nWidth As Long, _
ByVal nHeight As Long, _
ByVal hWndParent As Long, _
ByVal hMenu As Long, _
ByVal hInstance As Long, _
lpParam As Any) As Long

'fonction permettant de d�truire une fen�tre
Private Declare Function DestroyWindow Lib "user32" ( _
ByVal hwnd As Long) As Long

'fonction permettant d'envoyer un message � une fen�tre
Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" ( _
ByVal hwnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
ByVal lParam As Long) As Long

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" ( _
ByVal hwnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
lParam As Any) As Long

'fonction permettant d'initialiser les contr�les communs de Windows
Private Declare Sub InitCommonControls Lib "comctl32.dll" ()

'fonction permettant d'appliquer une transparence � la fen�tre
Private Declare Function SetLayeredWindowAttributes Lib "user32.dll" ( _
ByVal hwnd As Long, _
ByVal crKey As Long, _
ByVal bAlpha As Byte, _
ByVal dwFlags As Long) As Long

Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" ( _
ByVal hwnd As Long, _
ByVal nIndex As Long) As Long

Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" ( _
ByVal hwnd As Long, _
ByVal nIndex As Long, _
ByVal dwNewLong As Long) As Long

'd�claration des constantes priv�es � l'objet
Private Const CW_USEDEFAULT = &H80000000
Private Const TTS_NOPREFIX = 2
Private Const TTF_CENTERTIP = 2
Private Const TTM_ADDTOOLA = 1028
Private Const TTM_UPDATETIPTEXTA = 1036
Private Const TTM_SETTIPBKCOLOR = 1043
Private Const TTM_SETTIPTEXTCOLOR = 1044
Private Const TTM_SETTITLE = 1056
Private Const TTS_BALLOON = 64
Private Const TTS_ALWAYSTIP = 1
Private Const TTF_SUBCLASS = 16
Private Const TTF_IDISHWND = 1
Private Const TTM_SETDELAYTIME = 1027
Private Const TTDT_AUTOPOP = 2
Private Const TTDT_INITIAL = 3
Private Const TOOLTIPS_CLASSA = "tooltips_class32"
Private Const LWA_ALPHA = 2
Private Const GWL_STYLE = -16
Private Const GWL_EXSTYLE = -20
Private Const WS_EX_LAYERED = &H80000

'd�claration des types priv�es � l'objet
Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type TOOLINFO
    lSize As Long
    lFlags As Long
    hwnd As Long
    lId As Long
    lpRect As RECT
    hInstance As Long
    lpStr As String
    lParam As Long
End Type

'd�claration des �num�rations publiques de l'objet
Public Enum enmIconStyle
    [ToolTipNoIcon] = 0
    [ToolTipIconInfo] = 1
    [ToolTipIconWarning] = 2
    [ToolTipIconError] = 3
End Enum

Public Enum enmStyle
    [ToolTipStandard] = 0
    [ToolTipBalloon] = 1
End Enum

'd�claration des variables membres priv�es de l'objet
Private m_lBackColor As Long 'stocke la couleur de fond du tool tip
Private m_lControlHwnd As Long 'stocke le handle de la fen�tre du contr�le rattach� au tool tip
Private m_lDisplayTime As Long 'stocke le temps de visibilit� du tool tip (en ms)
Private m_lForeColor As Long 'stocke la couleur du texte du tool tip
Private m_lHwnd As Long 'stocke le handle de la fen�tre permettant l'affichage du tool tip
Private m_pIconStyle As enmIconStyle 'stocke le type d'ic�ne du tool tip
Private m_bIsCentered As Boolean 'stocke l'�tat de centrage du tool tip
Private m_pStyle As enmStyle 'stocke le style du tool tip
Private m_sTitle As String 'stocke le titre du tool tip
Private m_sTipText As String 'stocke le texte du tool tip
Private m_pToolTipInfo As TOOLINFO 'stocke les informations concernant le tool tip
Private m_byTransparenceFactor As Byte 'stocke le facteur de transparence du tool tip
Private m_lWaitTime As Long 'stocke le temps d'attente avant l'affichage du tool tip (en ms)

Public Property Let BackColor(ByVal lBackColor As Long)
   Let m_lBackColor = lBackColor
End Property

Public Property Get BackColor() As Long
   Let BackColor = m_lBackColor
End Property

Public Property Get DisplayTime() As Long
   Let DisplayTime = m_lDisplayTime
End Property

Public Property Let DisplayTime(ByVal lDisplayTime As Long)
   Let m_lDisplayTime = lDisplayTime
End Property

Public Property Let ForeColor(ByVal lForeColor As OLE_COLOR)
   Let m_lForeColor = lForeColor
End Property

Public Property Get ForeColor() As OLE_COLOR
   Let ForeColor = m_lForeColor
End Property

Public Property Let IconStyle(ByVal pIconStyle As enmIconStyle)
   Let m_pIconStyle = pIconStyle
End Property

Public Property Get IconStyle() As enmIconStyle
   Let IconStyle = m_pIconStyle
End Property

Public Property Let IsCentered(ByVal bIsCentered As Boolean)
   Let m_bIsCentered = bIsCentered
End Property

Public Property Get IsCentered() As Boolean
   Let IsCentered = m_bIsCentered
End Property

Public Property Let Style(ByVal pStyle As enmStyle)
   Let m_pStyle = pStyle
End Property

Public Property Get Style() As enmStyle
   Let Style = m_pStyle
End Property

Public Property Let Title(ByVal sTitle As String)
   Let m_sTitle = sTitle
End Property

Public Property Get Title() As String
   Let Title = m_sTitle
End Property

Public Property Let TipText(ByVal sTipText As String)
   Let m_sTipText = sTipText
End Property

Public Property Get TipText() As String
   Let TipText = m_sTipText
End Property

Public Property Get TransparenceFactor() As Byte
    Let TransparenceFactor = m_byTransparenceFactor
End Property

Public Property Let TransparenceFactor(ByVal byTransparenceFactor As Byte)
    Let m_byTransparenceFactor = byTransparenceFactor
End Property

Public Property Get WaitTime() As Long
   Let WaitTime = m_lWaitTime
End Property

Public Property Let WaitTime(ByVal lWaitTime As Long)
   Let m_lWaitTime = lWaitTime
End Property

Public Sub CreateToolTip(ByVal lControlHwnd As Long)
   
'======================================================================
'Proc�dure : CreateToolTip
'Date : 30/12/2004
'Auteur : DarK Sidious
'But : Permet de cr�er le tool tip et de l'affecter au contr�le
'======================================================================
   
'on active la routine de traitement d'erreur
On Error Resume Next
'l'erreur etant minime, on continue l'�x�cution normalement

'd�claration des variables priv�es
Dim lStyle As Long

    'on d�truit la fen�tre si elle est d�j� d�finie
    If m_lHwnd <> 0 Then Call DestroyWindow(m_lHwnd)

    'on cr�e la fen�tre du tooltip
    'Let m_lHwnd = CreateWindowEx(0&, TOOLTIPS_CLASSA, vbNullString, IIf(m_pStyle = ToolTipBalloon, TTS_ALWAYSTIP Or TTS_NOPREFIX Or TTS_BALLOON, TTS_ALWAYSTIP Or TTS_NOPREFIX), CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 0&, 0&, App.hInstance, 0&)
    Let m_lHwnd = CreateWindowEx(0&, TOOLTIPS_CLASSA, vbNullString, IIf(m_pStyle = 1, TTS_ALWAYSTIP Or TTS_NOPREFIX Or TTS_BALLOON, TTS_ALWAYSTIP Or TTS_NOPREFIX), CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 0&, 0&, App.hInstance, 0&)
    
    'on d�finit les informations du tooltip
    With m_pToolTipInfo
        Let .lFlags = TTF_SUBCLASS Or TTF_IDISHWND
        If m_bIsCentered Then Let .lFlags = .lFlags Or TTF_CENTERTIP
        Let .hwnd = lControlHwnd
        Let .lId = lControlHwnd
        Let .hInstance = App.hInstance
        Let .lpStr = m_sTipText
        Let .lSize = Len(m_pToolTipInfo)
    End With
    Call SendMessage(m_lHwnd, TTM_ADDTOOLA, 0&, m_pToolTipInfo)
    
    'on ajoute le titre et l'ic�ne du tooltip (on n'affiche pas l'ic�ne si on ne veut pas de titre)
    'If m_sTitle <> vbNullString Or m_pIconStyle <> ToolTipNoIcon Then Call SendMessage(m_lHwnd, TTM_SETTITLE, CLng(m_pIconStyle), ByVal m_sTitle)
    If m_sTitle <> vbNullString Or m_pIconStyle <> 0 Then Call SendMessage(m_lHwnd, TTM_SETTITLE, CLng(m_pIconStyle), ByVal m_sTitle)
    
    'on d�finit la couleur du texte
    If m_lForeColor <> Empty Then Call PostMessage(m_lHwnd, TTM_SETTIPTEXTCOLOR, m_lForeColor, 0&)
    
    'on d�finit la couleur de fond
    If m_lBackColor <> Empty Then Call PostMessage(m_lHwnd, TTM_SETTIPBKCOLOR, m_lBackColor, 0&)

    'on d�finit le d�lais d'attente et le temps d'affichage
    Call PostMessage(m_lHwnd, TTM_SETDELAYTIME, TTDT_AUTOPOP, m_lDisplayTime)
    Call PostMessage(m_lHwnd, TTM_SETDELAYTIME, TTDT_INITIAL, m_lWaitTime)
        
    'on applique la transparence � la fen�tre
    If m_byTransparenceFactor <> 255 Then
    
        Call SetWindowLong(m_lHwnd, GWL_EXSTYLE, GetWindowLong(m_lHwnd, GWL_EXSTYLE) Or WS_EX_LAYERED)
        Call SetLayeredWindowAttributes(m_lHwnd, 0, m_byTransparenceFactor, LWA_ALPHA)
    
    End If
    
End Sub

Public Sub DestroyToolTip()

'======================================================================
'Sub : DestroyToolTip
'Date : 30/12/2004
'Auteur : DarK Sidious
'But : Permet de d�truire la fen�tre du tooltip
'======================================================================

   If m_lHwnd <> 0 Then Call DestroyWindow(m_lHwnd)
   
End Sub

Private Sub Class_Initialize()
   
    'on initialise les propri�t�s
    Call InitCommonControls
    Let m_lWaitTime = 500
    Let m_lDisplayTime = 10000
    Let m_byTransparenceFactor = 255
   
End Sub

Private Sub Class_Terminate()

   Call DestroyToolTip
   
End Sub
