Attribute VB_Name = "Module_Trajectoires"
Option Explicit
Private Type DROITE
    A As Double
    B As Double
    c As Double
End Type
Private Type ligne
    P1 As Point
    P2 As Point
    E As Single
    Dist As Single
    DEC_P1 As Point
    DEC_P2 As Point
    Vitesse As Single
End Type

Sub Calcul_Trajectoire(monTab() As ElemDecoupe, Offset_Gauche As Single, _
        Offset_Droit As Single, Largeur_Bloc As Single, _
        Option_GD, _
        Optional minX As Single = 0#, Optional maxX As Single = 0#, _
        Optional minY As Single = 0#, Optional maxY As Single = 0#)
    'Calcul des trajectoires des chariots en fonction des coordonn�es des profils
    Dim k As Integer
    Dim i As Long
    Dim B5 As Double
    Dim C5 As Double
    Dim D5 As Double
    Dim E5 As Double
    Dim F5 As Double
    Dim H5 As Double
    Dim I5 As Double
    Dim L5 As Double
    Dim M5 As Double
    Dim A As Double, B As Double
    Dim P1(EMPLANTURE To SAUMON) As Point
    Dim DistChariot As Double
    Dim Offset As Double
    
    DistChariot = Offset_Gauche + Offset_Droit + Largeur_Bloc
    Offset = IIf(Option_GD = GAUCHE, Offset_Gauche, Offset_Droit)
        
    For i = LBound(monTab) To UBound(monTab)
        'Procedure RP
        'Coefficient directeur (a)
        A = (monTab(i).P_Decal(EMPLANTURE).x - monTab(i).P_Decal(SAUMON).x) / Largeur_Bloc
        B = monTab(i).P_Decal(EMPLANTURE).x + A * Offset
        monTab(i).P_abs(EMPLANTURE).x = B
        monTab(i).P_abs(SAUMON).x = -A * DistChariot + B
        
        A = (monTab(i).P_Decal(EMPLANTURE).y - monTab(i).P_Decal(SAUMON).y) / Largeur_Bloc
        B = monTab(i).P_Decal(EMPLANTURE).y + A * Offset
        monTab(i).P_abs(EMPLANTURE).y = B
        monTab(i).P_abs(SAUMON).y = -A * DistChariot + B
        
        MDIFrmDecoupe.Min_Decal_X = Min(MDIFrmDecoupe.Min_Decal_X, Min(monTab(i).P_Decal(EMPLANTURE).x, monTab(i).P_Decal(SAUMON).x))
        MDIFrmDecoupe.Max_Decal_X = Max(MDIFrmDecoupe.Max_Decal_X, Max(monTab(i).P_Decal(EMPLANTURE).x, monTab(i).P_Decal(SAUMON).x))
        MDIFrmDecoupe.Min_Decal_Y = Min(MDIFrmDecoupe.Min_Decal_Y, Min(monTab(i).P_Decal(EMPLANTURE).y, monTab(i).P_Decal(SAUMON).y))
        MDIFrmDecoupe.Max_Decal_Y = Max(MDIFrmDecoupe.Max_Decal_Y, Max(monTab(i).P_Decal(EMPLANTURE).y, monTab(i).P_Decal(SAUMON).y))
        MDIFrmDecoupe.Min_Chariot_X = Min(MDIFrmDecoupe.Min_Chariot_X, Min(monTab(i).P_abs(EMPLANTURE).x, monTab(i).P_abs(SAUMON).x))
        MDIFrmDecoupe.Max_Chariot_X = Max(MDIFrmDecoupe.Max_Chariot_X, Max(monTab(i).P_abs(EMPLANTURE).x, monTab(i).P_abs(SAUMON).x))
        MDIFrmDecoupe.Min_Chariot_Y = Min(MDIFrmDecoupe.Min_Chariot_Y, Min(monTab(i).P_abs(EMPLANTURE).y, monTab(i).P_abs(SAUMON).y))
        MDIFrmDecoupe.Max_Chariot_Y = Max(MDIFrmDecoupe.Max_Chariot_Y, Max(monTab(i).P_abs(EMPLANTURE).y, monTab(i).P_abs(SAUMON).y))
    Next i
End Sub

Function Ctrl_Vitesse_Traj(tab_traj() As ElemDecoupe, Table As String) As Boolean
    'Contr�le que la vitesse maximum des chariots n'est pas d�pass�e
    Dim k As Integer
    Dim i As Long
    Dim XVitMaxi(EMPLANTURE To SAUMON) As Single
    Dim YVitMaxi(EMPLANTURE To SAUMON) As Single
    Dim iXVitMaxi(EMPLANTURE To SAUMON) As Long
    Dim iYVitMaxi(EMPLANTURE To SAUMON) As Long
    Dim VitMaxX As Double
    Dim VitMaxY As Double
    Dim Temp As Single
    
    Ctrl_Vitesse_Traj = True
    For k = EMPLANTURE To SAUMON
        XVitMaxi(k) = 0#
        YVitMaxi(k) = 0#
        iXVitMaxi(k) = LBound(tab_traj)
        iYVitMaxi(k) = iXVitMaxi(k)
    Next k
    
    For i = 2 To UBound(tab_traj)
        For k = EMPLANTURE To SAUMON
            Temp = Abs(tab_traj(i - 1).P_abs(k).x - tab_traj(i).P_abs(k).x) / tab_traj(i).Temps
            If Temp > XVitMaxi(k) Then
                XVitMaxi(k) = Temp
                iXVitMaxi(k) = i
            End If
            Temp = Abs(tab_traj(i - 1).P_abs(k).y - tab_traj(i).P_abs(k).y) / tab_traj(i).Temps
            If Temp > YVitMaxi(k) Then
                YVitMaxi(k) = Temp
                iYVitMaxi(k) = i
            End If
        Next k
    Next i
    VitMaxX = ReadINI_Machine_Num("table_" & Table, "TB_SAV_14", 0)
    VitMaxY = ReadINI_Machine_Num("table_" & Table, "TB_SAV_15", 0)
    
    If XVitMaxi(EMPLANTURE) > VitMaxX Or XVitMaxi(SAUMON) > VitMaxX Or _
        YVitMaxi(EMPLANTURE) > VitMaxY Or YVitMaxi(SAUMON) > VitMaxY Then
        Ctrl_Vitesse_Traj = False
    End If
    MDIFrmDecoupe.SB.Panels(1) = "Xmax E : " & Round(XVitMaxi(EMPLANTURE), 2) & " mm/s - Pts " & _
        iXVitMaxi(EMPLANTURE) - 1 & "/" & iXVitMaxi(EMPLANTURE)
    MDIFrmDecoupe.SB.Panels(2) = "Ymax E : " & Round(YVitMaxi(EMPLANTURE), 2) & " mm/s - Pts " & _
        iYVitMaxi(EMPLANTURE) - 1 & "/" & iYVitMaxi(EMPLANTURE)
    MDIFrmDecoupe.SB.Panels(3) = "Xmax S : " & Round(XVitMaxi(SAUMON), 2) & " mm/s - Pts " & _
        iXVitMaxi(SAUMON) - 1 & "/" & iXVitMaxi(SAUMON)
    MDIFrmDecoupe.SB.Panels(4) = "Ymax S : " & Round(YVitMaxi(SAUMON), 2) & " mm/s - Pts " & _
        iYVitMaxi(SAUMON) - 1 & "/" & iYVitMaxi(SAUMON)
End Function

Function Ajustement_Vitesse_Traj(monTab() As ElemDecoupe, Table As String) As Boolean
    'Ajustement des vitesses de ListeDecoupe pour que la vitesse maximum des chariots
        'ne soit pas d�pass�e
        
    Dim k As Integer
    Dim i As Long
    Dim VitMaxX As Single
    Dim VitMaxY As Single
    Dim Vit As Single
    Dim RapportMax As Single
    Dim TempX(EMPLANTURE To SAUMON) As Single
    Dim TempY(EMPLANTURE To SAUMON) As Single
    
    VitMaxX = ReadINI_Machine_Num("table_" & Table, "TB_SAV_14", 0)
    VitMaxY = ReadINI_Machine_Num("table_" & Table, "TB_SAV_15", 0)

    For k = EMPLANTURE To SAUMON
        TempX(k) = 0#
        TempY(k) = 0#
    Next k
    Ajustement_Vitesse_Traj = False
    For i = LBound(monTab) To UBound(monTab)
        RapportMax = 0#
        For k = EMPLANTURE To SAUMON
            Vit = Abs((monTab(i).P_abs(k).x - TempX(k)) / monTab(i).Temps)
            TempX(k) = monTab(i).P_abs(k).x
            RapportMax = Max(RapportMax, Vit / VitMaxX)
            Vit = Abs((monTab(i).P_abs(k).y - TempY(k)) / monTab(i).Temps)
            TempY(k) = monTab(i).P_abs(k).y
            RapportMax = Max(RapportMax, Vit / VitMaxY)
        Next k
        If RapportMax > 1# Then
            For k = EMPLANTURE To SAUMON
                ListeDecoupe(k).MoveIndex monTab(i).iOrigine + 1
                ListeDecoupe(k).Vitesse ListeDecoupe(k).CurrentVitesse / (RapportMax * 1.01)
            Next k
            Ajustement_Vitesse_Traj = True
        End If
    Next i
End Function

Function Ctrl_Trajectoire(tab_traj() As ElemDecoupe, Debat_X As Single, Debat_Y As Single, _
    VitesseMatiere As Single) As String
    'Contr�le des trajectoires des chariots
    Dim iMinX(EMPLANTURE To SAUMON) As Long
    Dim iMaxX(EMPLANTURE To SAUMON) As Long
    Dim iMinY(EMPLANTURE To SAUMON) As Long
    Dim iMaxY(EMPLANTURE To SAUMON) As Long
    Dim TempNum As Single
    Dim i As Long
    Dim k As Integer
    
    Ctrl_Trajectoire = True
    
    For k = EMPLANTURE To SAUMON
        iMinX(k) = 1
        iMaxX(k) = 1
        iMinY(k) = 1
        iMaxY(k) = 1
    Next k
    
    For i = 2 To UBound(tab_traj)
        For k = EMPLANTURE To SAUMON
            If tab_traj(i).P_abs(k).x < tab_traj(iMinX(k)).P_abs(k).x Then
                iMinX(k) = i
            ElseIf tab_traj(i).P_abs(k).x > tab_traj(iMaxX(k)).P_abs(k).x Then
                iMaxX(k) = i
            End If
            If tab_traj(i).P_abs(k).y < tab_traj(iMinY(k)).P_abs(k).y Then
                iMinY(k) = i
            ElseIf tab_traj(i).P_abs(k).y > tab_traj(iMaxY(k)).P_abs(k).y Then
                iMaxY(k) = i
            End If
            
        Next k
        
    Next i
    
    Ctrl_Trajectoire = ""
    
    TempNum = tab_traj(iMinX(EMPLANTURE)).P_abs(EMPLANTURE).x
    If TempNum < -0.01 Then
        Ctrl_Trajectoire = Ctrl_Trajectoire & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 10) & _
        " " & ReadINI_Trad(gLangue, "erreurs", 30) & " " & -Round(TempNum, 1) & " mm"
    End If
    TempNum = tab_traj(iMinX(SAUMON)).P_abs(SAUMON).x
    If TempNum < -0.01 Then
        Ctrl_Trajectoire = Ctrl_Trajectoire & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 11) & _
        " " & ReadINI_Trad(gLangue, "erreurs", 30) & " " & -Round(TempNum, 1) & " mm"
    End If
    TempNum = tab_traj(iMinY(EMPLANTURE)).P_abs(EMPLANTURE).y
    If TempNum < -0.01 Then
        Ctrl_Trajectoire = Ctrl_Trajectoire & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 12) & _
        " " & ReadINI_Trad(gLangue, "erreurs", 30) & " " & -Round(TempNum, 1) & " mm"
    End If
    TempNum = tab_traj(iMinY(SAUMON)).P_abs(SAUMON).y
    If TempNum < -0.01 Then
        Ctrl_Trajectoire = Ctrl_Trajectoire & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 13) & _
        " " & ReadINI_Trad(gLangue, "erreurs", 30) & " " & -Round(TempNum, 1) & " mm"
    End If
    
    TempNum = tab_traj(iMaxX(EMPLANTURE)).P_abs(EMPLANTURE).x
    If TempNum > Debat_X + 0.01 Then
        Ctrl_Trajectoire = Ctrl_Trajectoire & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 14) & _
        " " & ReadINI_Trad(gLangue, "erreurs", 30) & " " & Round(TempNum - Debat_X, 1) & " mm"
    End If
    TempNum = tab_traj(iMaxX(SAUMON)).P_abs(SAUMON).x
    If TempNum > Debat_X + 0.01 Then
        Ctrl_Trajectoire = Ctrl_Trajectoire & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 15) & _
        " " & ReadINI_Trad(gLangue, "erreurs", 30) & " " & Round(TempNum - Debat_X, 1) & " mm"
    End If
    TempNum = tab_traj(iMaxY(EMPLANTURE)).P_abs(EMPLANTURE).y
    If TempNum > Debat_Y + 0.01 Then
        Ctrl_Trajectoire = Ctrl_Trajectoire & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 16) & _
        " " & ReadINI_Trad(gLangue, "erreurs", 30) & " " & Round(TempNum - Debat_Y, 1) & " mm"
    End If
    TempNum = tab_traj(iMaxY(SAUMON)).P_abs(SAUMON).y
    If TempNum > Debat_Y + 0.01 Then
        Ctrl_Trajectoire = Ctrl_Trajectoire & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 17) & _
        " " & ReadINI_Trad(gLangue, "erreurs", 30) & " " & Round(TempNum - Debat_Y, 1) & " mm"
    End If
End Function

Function Calcul_Valeur_Decalage(ByRef maListe() As ClsListeChainee, _
    ByVal Materiau As String, ByVal chauffe As Single)
    'Calcul du d�calage en fonction des vitesses calcul�es pour l'emplanture et le saumon
    ' et de la valeur de chauffe (chauffe)
    
    Dim k As Integer
    Dim CH As Double, CB As Double, VH As Double, VB As Double
    Dim SH1 As Single, SH2 As Single
'    Dim RH1 As Single, RH2 As Single
    Dim VIdeal As Single
    Dim Temp1 As Single
    Dim Temp2 As Single
    
    'D�termination de la droite de chauffe pour le mat�riau choisi
    CH = ReadINI_Machine_Num("mat_" & Materiau, "CH", 2)
    CB = ReadINI_Machine_Num("mat_" & Materiau, "CB", 1)
    VH = ReadINI_Machine_Num("mat_" & Materiau, "VH", 20)
    VB = ReadINI_Machine_Num("mat_" & Materiau, "VB", 2)
    
    SH1 = ReadINI_Machine_Num("mat_" & Materiau, "SH1", 0)
    SH2 = ReadINI_Machine_Num("mat_" & Materiau, "SH2", 0)
'    RH1 = SH1 / 2
'    RH2 = SH2 / 2
    VIdeal = chauffe * (VH - VB) / (CH - CB) + (VB * CH - VH * CB) / (CH - CB)
    Temp1 = (SH1 - SH2) / VIdeal    'Optimisation des calculs
    Temp2 = SH2 - SH1 / 2#
    
    For k = EMPLANTURE To SAUMON
        maListe(k).MoveFirst
        Do
            maListe(k).Decalage Round(maListe(k).CurrentVitesse * Temp1 + Temp2, 4)
        Loop While maListe(k).MoveNext
    Next k
End Function

Function Calcul_Vitesse_Decalage(ByRef maListe() As ClsListeChainee, ByVal Vitesse_Consigne As Single)
    Dim k As Integer
    Dim toto As Integer
    Dim Distance(EMPLANTURE To SAUMON) As Double
    Dim Vitesse(EMPLANTURE To SAUMON) As Single
    
    For k = EMPLANTURE To SAUMON
        maListe(k).MoveFirst
        maListe(k).MoveNext
    Next k
    Do
    toto = toto + 1
        'Quel est le trajet le plus long ?
        For k = EMPLANTURE To SAUMON
            Distance(k) = Calcul_Distance(maListe(k).PreviousPoint, maListe(k).CurrentPoint)
        Next k
'Debug.Print "E/S : " & Round(Distance(EMPLANTURE) / Distance(SAUMON), 2)
        If Distance(SAUMON) <= Distance(EMPLANTURE) + 0.02 Then
            'L'emplanture est plus grande que le saumon
            '    c'est donc l'emplanture qui sera parcourue � la vitesse de consigne
            Vitesse(EMPLANTURE) = Vitesse_Consigne
            Vitesse(SAUMON) = Vitesse(EMPLANTURE) / Distance(EMPLANTURE) * Distance(SAUMON)
        Else
            'Le saumon est plus grand que l'emplanture
            '    c'est donc le saumon qui sera parcouru � la vitesse de consigne
            Vitesse(SAUMON) = Vitesse_Consigne
            Vitesse(EMPLANTURE) = Vitesse(SAUMON) / Distance(SAUMON) * Distance(EMPLANTURE)
        End If
        For k = EMPLANTURE To SAUMON
            maListe(k).Vitesse Vitesse(k)
        Next k
    Loop While maListe(SAUMON).MoveNext And maListe(EMPLANTURE).MoveNext
End Function

Function Calcul_Decalage(Tab_Coord() As ClsListeChainee, Compense As Boolean) As ElemDecoupe()
    'Calcul du d�calage V3
    Dim TabDecalV3() As ligne
    
    Dim i As Long
    Dim j As Long
    Dim k As Integer
    Dim Eq_Parall As DROITE
    Dim Eq_B As DROITE
    Dim Interb As Point
    Dim InterbM1 As Point
    Dim Dist As Single
    Dim Temp As Single
    Dim Perp As Point
    Dim P1 As Point
    Dim signe As Integer
    
    signe = IIf(Compense, 1, -1)
'    MsgBox "Signe : " & signe
    
    For k = EMPLANTURE To SAUMON
        Tab_Coord(k).DecalEpsilon
'        While Tab_Coord(k).MoveNext
'            If Tab_Coord(k).CurrentPoint.x = Tab_Coord(k).PreviousPoint.x Then
'                Tab_Coord(k).CurrentX
'                Tab_Coord(k).CurrentX = Tab_Coord(k).CurrentPoint.x + 0.01
'            End If
'            If Tab_Coord(k).CurrentPoint.y = Tab_Coord(k).PreviousPoint.y Then
'                Tab_Coord(k).CurrentY = Tab_Coord(k).CurrentPoint.y + 0.01
'            End If
'        Wend
    Next k
    For k = EMPLANTURE To SAUMON
        'Constitution du tableau de travail (coordonn�es doubl�es)
        ReDim TabDecalV3(1 To Tab_Coord(EMPLANTURE).count - 1)
        Tab_Coord(k).MoveFirst
        TabDecalV3(1).P1 = Tab_Coord(k).CurrentPoint
        Tab_Coord(k).MoveNext
        TabDecalV3(1).P2 = Tab_Coord(k).CurrentPoint
        TabDecalV3(1).E = signe * Tab_Coord(k).CurrentDecalage
        TabDecalV3(1).Vitesse = Tab_Coord(k).CurrentVitesse
        i = 1
        P1 = Tab_Coord(k).CurrentPoint
        Do
            i = i + 1
            TabDecalV3(i).P1 = Tab_Coord(k).CurrentPoint
            Tab_Coord(k).MoveNext
            TabDecalV3(i).P2 = Tab_Coord(k).CurrentPoint
            TabDecalV3(i).E = signe * Tab_Coord(k).CurrentDecalage
            TabDecalV3(i).Vitesse = Tab_Coord(k).CurrentVitesse
        Loop While i < UBound(TabDecalV3)
    
        'Call Test(TabDecalV3)
        
        'Calcul des distances
        For i = 1 To UBound(TabDecalV3)
            TabDecalV3(i).Dist = Calcul_Distance(TabDecalV3(i).P1, TabDecalV3(i).P2)
        Next i
    
        'Calcul de la trajectoire d�cal�e
        'X1, Y1     X2, Y2     E1, E2     Dist     X3, Y3
        i = 1
        Do
            Eq_Parall.A = TabDecalV3(i).P2.y - TabDecalV3(i).P1.y
            Eq_Parall.B = TabDecalV3(i).P1.x - TabDecalV3(i).P2.x
            Eq_Parall.c = TabDecalV3(i).P2.x * TabDecalV3(i).P1.y - _
                TabDecalV3(i).P1.x * TabDecalV3(i).P2.y + _
                TabDecalV3(i).E * TabDecalV3(i).Dist
                
            If i < UBound(TabDecalV3) Then
                Eq_B.A = (TabDecalV3(i + 1).P2.x - TabDecalV3(i).P2.x) * TabDecalV3(i).Dist - _
                    (TabDecalV3(i).P1.x - TabDecalV3(i).P2.x) * TabDecalV3(i + 1).Dist
                Eq_B.B = (TabDecalV3(i + 1).P2.y - TabDecalV3(i).P2.y) * TabDecalV3(i).Dist _
                    - (TabDecalV3(i).P1.y - TabDecalV3(i).P2.y) * TabDecalV3(i + 1).Dist
                Eq_B.c = -Eq_B.A * TabDecalV3(i).P2.x - Eq_B.B * TabDecalV3(i).P2.y
                Temp = (Eq_B.B * Eq_Parall.A - Eq_B.A * Eq_Parall.B)
                Interb.x = (Eq_B.c * Eq_Parall.B - Eq_Parall.c * Eq_B.B) / Temp
                Interb.y = (Eq_B.A * Eq_Parall.c - Eq_Parall.A * Eq_B.c) / Temp
            End If
            
            If Eq_Parall.B = 0# Then
                Perp.x = -Eq_Parall.c / Eq_Parall.A
                Perp.y = TabDecalV3(i).P1.y
            Else
                Perp.x = (TabDecalV3(i).P1.x - (Eq_Parall.A / Eq_Parall.B) * (TabDecalV3(i).P1.y + (Eq_Parall.c / Eq_Parall.B))) / _
                ((Eq_Parall.A / Eq_Parall.B) * (Eq_Parall.A / Eq_Parall.B) + 1)
                Perp.y = -(Eq_Parall.A / Eq_Parall.B) * Perp.x - Eq_Parall.c / Eq_Parall.B
            End If
            
            If i = 1 Then
                TabDecalV3(i).DEC_P1 = Perp
            Else
                If (TabDecalV3(i).E = TabDecalV3(i - 1).E) Then
                    TabDecalV3(i).DEC_P1 = TabDecalV3(i - 1).DEC_P2
                Else
                    If Calcul_Distance(Perp, TabDecalV3(i).P2) < Calcul_Distance(InterbM1, TabDecalV3(i).P2) Then
                        TabDecalV3(i).DEC_P1 = Perp
                    Else
                        If Eq_Parall.B = 0 Then
                            TabDecalV3(i).DEC_P1 = Perp
                        Else
                            TabDecalV3(i).DEC_P1.x = (InterbM1.x - (Eq_Parall.A / Eq_Parall.B) * (InterbM1.y + (Eq_Parall.c / Eq_Parall.B))) / ((Eq_Parall.A / Eq_Parall.B) * (Eq_Parall.A / Eq_Parall.B) + 1)
                            TabDecalV3(i).DEC_P1.y = -(Eq_Parall.A / Eq_Parall.B) * TabDecalV3(i).DEC_P1.x - Eq_Parall.c / Eq_Parall.B
                        End If
                    End If
                End If
            End If
            
            If i < UBound(TabDecalV3) Then
                TabDecalV3(i).DEC_P2 = Interb
            Else
                If Eq_Parall.B = 0 Then
                    TabDecalV3(i).DEC_P2.x = -Eq_Parall.c / Eq_Parall.A
                    TabDecalV3(i).DEC_P2.y = TabDecalV3(i).P2.y
                Else
                    TabDecalV3(i).DEC_P2.x = (TabDecalV3(i).P2.x - (Eq_Parall.A / Eq_Parall.B) * (TabDecalV3(i).P2.y + (Eq_Parall.c / Eq_Parall.B))) / _
                        ((Eq_Parall.A / Eq_Parall.B) * (Eq_Parall.A / Eq_Parall.B) + 1)
                    TabDecalV3(i).DEC_P2.y = -(Eq_Parall.A / Eq_Parall.B) * TabDecalV3(i).DEC_P2.x - Eq_Parall.c / Eq_Parall.B
                End If
            End If
            
'           Debug.Print i & vbTab & TabDecalV3(i).P1.x & vbTab & TabDecalV3(i).P1.y & vbTab & _
'                TabDecalV3(i).P2.x & vbTab & TabDecalV3(i).P2.y & vbTab & _
'                TabDecalV3(i).E & vbTab & _
'                TabDecalV3(i).Dist & vbTab & _
'                Eq_Parall.A & vbTab & Eq_Parall.B & vbTab & Eq_Parall.c & vbTab & _
'                Eq_B.A & vbTab & Eq_B.B & vbTab & Eq_B.c & vbTab & _
'                Interb.x & vbTab & Interb.y & vbTab & _
'                Perp.x & vbTab & Perp.y & vbTab & _
'                TabDecalV3(i).DEC_P1.x & vbTab & TabDecalV3(i).DEC_P1.y & vbTab & _
'                TabDecalV3(i).DEC_P2.x & vbTab & TabDecalV3(i).DEC_P2.y & _
'                TabDecalV3(i).Vitesse
'Debug.Print TabDecalV3(i).Vitesse
            
            i = i + 1
            InterbM1 = Interb
        Loop While i <= UBound(TabDecalV3)
        
        'On recopie dans Tab_Chariot
        If k = EMPLANTURE Then
            ReDim Tab_Chariot(1 To UBound(TabDecalV3) * 2)
        End If
        j = 0
        P1.x = 0#
        P1.y = 0#
        For i = 1 To UBound(TabDecalV3)
            j = j + 1
            Tab_Chariot(j).P_Decal(k) = TabDecalV3(i).DEC_P1
            Tab_Chariot(j).iOrigine = i
            If k = EMPLANTURE Then
                Tab_Chariot(j).Temps = Calcul_Temps_Points(P1, TabDecalV3(i).DEC_P1, TabDecalV3(i).Vitesse)
            Else
                Tab_Chariot(j).Temps = Max(Tab_Chariot(j).Temps, Calcul_Temps_Points(P1, TabDecalV3(i).DEC_P1, TabDecalV3(i).Vitesse))
            End If
            P1 = TabDecalV3(i).DEC_P1
            
            j = j + 1
            Tab_Chariot(j).P_Decal(k) = TabDecalV3(i).DEC_P2
            Tab_Chariot(j).iOrigine = i
            Tab_Chariot(j).Temps = Calcul_Temps_Points(P1, TabDecalV3(i).DEC_P2, TabDecalV3(i).Vitesse)
            P1 = TabDecalV3(i).DEC_P2
        Next i
    Next k
    Calcul_Decalage = Tab_Chariot
'    Debug.Print "Apr�s d�calage"
'    For k = EMPLANTURE To SAUMON
'        Debug.Print IIf(k = EMPLANTURE, "EMPLANTURE", "SAUMON")
'        For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
'            Debug.Print i, Tab_Chariot(i).P_Decal(k).X, Tab_Chariot(i).P_Decal(k).Y, Tab_Chariot(i).Temps, Tab_Chariot(i).iOrigine
'        Next i
'    Next k
End Function

Function Envoi_Decoupe(Tab_Chariot() As ElemDecoupe, _
        maForm1 As Form, Optional PB1 As PictureBox, Optional PB2 As PictureBox, _
        Optional bInv As Boolean = False, Optional bAffich As Boolean = False, Optional bLog As Boolean = False) _
            As Boolean
    'Envoi des commandes de d�coupe
    Dim TpsReel As Single
    Dim TpsTotal As Single
    Dim wTempsRestant As Single
    Dim monTempsDepart As Long
    Dim Pulse_Rap As Double
    Dim i As Long, k As Integer
    Dim PB(EMPLANTURE To SAUMON) As PictureBox
    
    'Variables pour le calcul des pas
    Dim X_pas_tot(EMPLANTURE To SAUMON) As Long
    Dim Y_pas_Tot(EMPLANTURE To SAUMON) As Long
    Dim X_mm_Tot(EMPLANTURE To SAUMON) As Single
    Dim Y_mm_Tot(EMPLANTURE To SAUMON) As Single

    'Variables pour traiter la reprise
    Dim NbPasX(EMPLANTURE To SAUMON) As Long      'Nombre de pas effectu�s
    Dim NbPasY(EMPLANTURE To SAUMON) As Long      'Nombre de pas effectu�s
    Dim sensX(EMPLANTURE To SAUMON) As Integer    'Sens du d�placement
    Dim sensY(EMPLANTURE To SAUMON) As Integer    'Sens du d�placement
    Dim posX(EMPLANTURE To SAUMON)   As Double    'Position apr�s arr�t
    Dim posY(EMPLANTURE To SAUMON)   As Double    'Position apr�s arr�t

    Dim wSegment As Long
    Dim FDEC As Byte
    Dim monDiff As Single
    Dim X_VitMax_SansAcc As Single
    Dim Y_VitMax_SansAcc As Single
    Dim X_VitMax_AvecAcc As Single
    Dim Y_VitMax_AvecAcc As Single

    'En mode debug, on va cr�er un fichier log
    Dim cumX As Single, cumY As Single
    Dim cumPasX As Single, cumPasY As Single
    Dim monEnreg As String
    Dim monLib As String
    
    If Not (PB1 Is Nothing) Then Set PB(1) = PB1
    If Not (PB2 Is Nothing) Then Set PB(2) = PB2
    For k = EMPLANTURE To SAUMON
        sensX(k) = 1
        sensY(k) = 1
    Next k

    DeplEnCours = True
    If gPause Then Exit Function
    
    Call maClasseHID.TableVerif
    Call maClasseHID.MotorOnOff(True)

    'Calcul de la vitesse maxi de d�coupe
    X_VitMax_SansAcc = Val(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_12", 5))
    Y_VitMax_SansAcc = Val(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_13", 5))
    
    X_VitMax_AvecAcc = Val(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 5))
    Y_VitMax_AvecAcc = Val(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 5))
    
    'Calcul en nombre de pas
    If bLog Then
        Open "RP-FC.log" For Output As #1
        monEnreg = "Version " & App.Major & "." & App.Minor & "." & App.Revision
        Print #1, monEnreg
    End If
    For k = EMPLANTURE To SAUMON
        X_pas_tot(k) = 0#
        Y_pas_Tot(k) = 0#
        X_mm_Tot(k) = 0#
        Y_mm_Tot(k) = 0#
        If bLog Then
            monEnreg = IIf(k = EMPLANTURE, "Emplanture", "Saumon")
            Print #1, monEnreg
        End If
        For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
            X_mm_Tot(k) = X_mm_Tot(k) + Tab_Chariot(i).P_abs(k).x
            Tab_Chariot(i).X_pas(k) = Round(X_mm_Tot(k) / maClasseHID.X_Demult) - X_pas_tot(k)
            X_pas_tot(k) = X_pas_tot(k) + Tab_Chariot(i).X_pas(k)
            Y_mm_Tot(k) = Y_mm_Tot(k) + Tab_Chariot(i).P_abs(k).y
            Tab_Chariot(i).Y_pas(k) = Round(Y_mm_Tot(k) / maClasseHID.Y_Demult) - Y_pas_Tot(k)
            Y_pas_Tot(k) = Y_pas_Tot(k) + Tab_Chariot(i).Y_pas(k)
            If bLog Then
                monEnreg = "Temps : " & Format$(Format$(Tab_Chariot(i).Temps, "0.00"), "@@@@@@@") & Space$(2)
                monEnreg = monEnreg & "X/Y     " & Format$(Format$(Tab_Chariot(i).P_abs(k).x, "0.00"), "@@@@@@@@") & " / " & _
                    Format$(Format$(Tab_Chariot(i).P_abs(k).y, "0.00"), "@@@@@@@@")
                cumX = cumX + Tab_Chariot(i).P_abs(k).x
                cumY = cumY + Tab_Chariot(i).P_abs(k).y
                monEnreg = monEnreg & "    " & Format$(Tab_Chariot(i).X_pas(k), "@@@@@@@@") & " / " & _
                    Format$(Tab_Chariot(i).Y_pas(k), "@@@@@@@@")
                cumPasX = cumPasX + Tab_Chariot(i).X_pas(k)
                cumPasY = cumPasY + Tab_Chariot(i).Y_pas(k)
                Print #1, monEnreg
            End If
        Next i
        
        If bLog Then
            monEnreg = "Contr�le mm  : " & Format$(cumX, "0.00") & "  /  " & Format$(cumY, "0.00")
            Print #1, monEnreg
            monEnreg = "Contr�le pas : " & Format$(cumPasX, "@@@@@@@@") & "  /  " & Format$(cumPasY, "@@@@@@@@")
            Print #1, monEnreg
        End If
    Next k
    
    If bLog Then
        Close #1
        Exit Function
    End If
    
    TpsTotal = Temps_Decoupe(Tab_Chariot)
    If bAffich Then
        maForm1.ProgressBar1.Min = 0
        maForm1.ProgressBar1.Max = TpsTotal
        maForm1.ProgressBar1.value = 0
        maForm1.ProgressBar1.Visible = True
    End If

    monTempsDepart = Hour(Now) * 3600 + Minute(Now) * 60 + Second(Now)
    iDessin = 2
    
    Call maClasseHID.EnvoiUSB
    Call maClasseHID.DemandeInformation
    On Error Resume Next
    maForm1.SB.Style = sbrSimple
    On Error GoTo 0

    i = LBound(Tab_Chariot)
    wSegment = 0
    monLib = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 254)

    Do While i <= UBound(Tab_Chariot)
        'Boucle qui tourne pour tout le tableau et tant que l'on n'a pas fait "pause"
        If gPause Then Exit Do
        While Not maClasseHID.CommandeDATA(Tab_Chariot(i).X_pas(IIf(bInv, SAUMON, EMPLANTURE)), _
                                           Tab_Chariot(i).Y_pas(IIf(bInv, SAUMON, EMPLANTURE)), _
                                           Tab_Chariot(i).X_pas(IIf(bInv, EMPLANTURE, SAUMON)), _
                                           Tab_Chariot(i).Y_pas(IIf(bInv, EMPLANTURE, SAUMON)), _
                                           Tab_Chariot(i).Temps, _
                                           TpsReel, False) _
                        And Not gPause
            wTempsRestant = maClasseHID.TempsRestant

            If maClasseHID.Num_Segment <= UBound(Tab_Chariot) Then
                If wSegment <> maClasseHID.Num_Segment And _
                    wTempsRestant > 0.1 Then
                        wSegment = maClasseHID.Num_Segment
                        If bAffich Then
                            Call Dessin_Avancement(Tab_Chariot, TpsTotal, monTempsDepart, maClasseHID.Num_Segment, PB, monLib)
                        End If
                End If
            End If
            DoEvents
            If gPause Then Exit Do
        Wend
        Tab_Chariot(i).Nbr_Pulse = maClasseHID.Nbr_Pulse
        i = i + 1
    Loop

    If Not gPause Then
        While Not maClasseHID.CommandeDATAend And (Not gPause)
            DoEvents
            If wSegment <> maClasseHID.Num_Segment And _
                maClasseHID.Num_Segment <= UBound(Tab_Chariot) Then
                If bAffich Then
                    Call Dessin_Avancement(Tab_Chariot, TpsTotal, monTempsDepart, maClasseHID.Num_Segment, PB, monLib)
                End If
            End If '
        Wend
    End If
    Call maClasseHID.DemandeInformation
    Do While maClasseHID.Step_Processed And (Not gPause)
        If wSegment <> maClasseHID.Num_Segment And _
            maClasseHID.Num_Segment <= UBound(Tab_Chariot) Then
            If bAffich Then
                Call Dessin_Avancement(Tab_Chariot, TpsTotal, monTempsDepart, maClasseHID.Num_Segment, PB, monLib)
            End If
        End If
        Call maClasseHID.DemandeInformation
        DoEvents
    Loop

    If gPause Then
        'Calcul de FDEC : Quel est l'axe qui a le plus grand diff�rentiel entre la vitesse max avec acc�l�ration
        '                 et la vitesse max sans acc�l�ration
        monDiff = Max(X_VitMax_AvecAcc / X_VitMax_SansAcc, Y_VitMax_AvecAcc / Y_VitMax_SansAcc)
            
        FDEC = Int(255 / monDiff)
        Call maClasseHID.StopResume(FastStop, FDEC)
        'Calcul du nombre de pas effectu�s dans le cas d'un SLOW STOP
        'Rapport du nombre pulses
        wSegment = maClasseHID.Num_Segment
        Pulse_Rap = (Tab_Chariot(maClasseHID.Num_Segment).Nbr_Pulse - _
                    maClasseHID.Nbr_Pulse_Restant - 1) / _
                    Tab_Chariot(maClasseHID.Num_Segment).Nbr_Pulse
        'Calcul du nombre de pas effectu�s
        For k = EMPLANTURE To SAUMON
            NbPasX(k) = Abs(Tab_Chariot(maClasseHID.Num_Segment).X_pas(k)) * Pulse_Rap
            NbPasY(k) = Abs(Tab_Chariot(maClasseHID.Num_Segment).Y_pas(k)) * Pulse_Rap
            'Calcul de la position actuelle
            sensX(k) = IIf(Tab_Chariot(maClasseHID.Num_Segment).X_pas(k) > 0, 1, -1)
            sensY(k) = IIf(Tab_Chariot(maClasseHID.Num_Segment).Y_pas(k) > 0, 1, -1)
            If maClasseHID.Num_Segment = 1 Then
                posX(k) = 0 + maClasseHID.Pas_mm_X(NbPasX(k))
                posY(k) = 0 + maClasseHID.Pas_mm_Y(NbPasY(k))
            Else
                posX(k) = Tab_Chariot(maClasseHID.Num_Segment - 1).P_abs(k).x + maClasseHID.Pas_mm_X(NbPasX(k) * sensX(k))
                posY(k) = Tab_Chariot(maClasseHID.Num_Segment - 1).P_abs(k).y + maClasseHID.Pas_mm_Y(NbPasY(k) * sensY(k))
            End If
        Next k

        'On boucle pour attendre la fin du d�placement
        Call maClasseHID.DemandeInformation
        While maClasseHID.Step_Processed
            Call maClasseHID.DemandeInformation
            DoEvents
        Wend
        'On r�initialise le buffer
        Call maClasseHID.EnvoiUSB
        
        'On raccourcit le tableau en vue d'une �ventuelle reprise
        If wSegment > 1 Then
            For i = wSegment To UBound(Tab_Chariot)
                Tab_Chariot(i - wSegment + 1) = Tab_Chariot(i)
            Next i
            ReDim Preserve Tab_Chariot(1 To UBound(Tab_Chariot) - wSegment + 1)
        End If
        
        For k = EMPLANTURE To SAUMON
            Tab_Chariot(1).X_pas(k) = Tab_Chariot(1).X_pas(k) - (NbPasX(k) * sensX(k))
            Tab_Chariot(1).Y_pas(k) = Tab_Chariot(1).Y_pas(k) - (NbPasY(k) * sensY(k))
        Next k
        
        If Tab_Chariot(1).X_pas(EMPLANTURE) = 0 And Tab_Chariot(1).Y_pas(EMPLANTURE) = 0 And _
            Tab_Chariot(1).X_pas(SAUMON) = 0 And Tab_Chariot(1).Y_pas(SAUMON) = 0 Then
            'On est arriv� au bout du segment
            For i = 2 To UBound(Tab_Chariot)
                Tab_Chariot(i - 1) = Tab_Chariot(i)
            Next i
            If UBound(Tab_Chariot) > 1 Then
                ReDim Preserve Tab_Chariot(1 To UBound(Tab_Chariot) - 1)
            End If
        End If
        
            
        DeplEnCours = False
        Exit Function
    End If
    
    'C'est fini, on remet le bouton dans sa config initiale :
'    F_Representation.Form_Resize
    If bAffich Then
        MDIFrmDecoupe.ProgressBar1.Visible = False
    '   TB_TempsRestant.Visible = False
    End If

    DeplEnCours = False
    On Error Resume Next
    maForm1.SB.SimpleText = "..."
    On Error GoTo 0
End Function

Private Sub Dessin_Avancement(TabDecoupe() As ElemDecoupe, TempsTotal As Single, _
        TempsDepart As Long, monSegment As Long, PB() As PictureBox, monLib As String)
    Dim k As Integer
    Dim monTempsActuel As Long
    Dim hh As Integer, mm As Integer, ss As Integer, sss As Long
    Static P1(EMPLANTURE To SAUMON) As PointPLT
    Dim P2(EMPLANTURE To SAUMON) As PointPLT
    
    On Error Resume Next    'Pour ne pas planter lors des tests
    
    For k = EMPLANTURE To SAUMON
        With PB(k)
            .DrawWidth = 3
            .DrawStyle = vbSolid
            .ForeColor = vbBlue
        End With
'        ListeDecalage(k).MoveIndex iDessin
        If iDessin = 2 Then         'Pour initialiser
'            P1(k).x = ListeDecalage(k).CurrentPoint.x
'            P1(k).y = ListeDecalage(k).CurrentPoint.y - MDIFrmDecoupe.wOffset_Y
        End If
    Next k
'    While iDessin <= monSegment And iDessin <= ListeDecalage(EMPLANTURE).count
'        For k = EMPLANTURE To SAUMON
'            P2(k).x = ListeDecalage(k).CurrentPoint.x
'            P2(k).y = ListeDecalage(k).CurrentPoint.y - MDIFrmDecoupe.wOffset_Y
'            If P1(k).x <> 0# Or P1(k).y <> 0# Then
'                PB(k).Line (P1(k).x, P1(k).y)-(P2(k).x, P2(k).y)
'            End If
'            DoEvents
'            P1(k) = P2(k)
'            ListeDecalage(k).MoveNext
'        Next k
'        iDessin = iDessin + 1
'    Wend
    
    monTempsActuel = Hour(Now) * 3600 + Minute(Now) * 60 + Second(Now)
    sss = Round(TempsTotal - (monTempsActuel - TempsDepart))
    hh = Int(sss / 3600)
    mm = Int((sss - hh * 3600) / 60)
    ss = sss - hh * 3600 - mm * 60
    MDIFrmDecoupe.SB.SimpleText = monLib & Space$(1) & IIf(hh > 0, hh & ":", "") & IIf(mm > 0, Format(mm, "00") & ":", "") & Format(ss, "00")
    MDIFrmDecoupe.ProgressBar1.value = Min(monTempsActuel - TempsDepart, MDIFrmDecoupe.ProgressBar1.Max)
End Sub

Sub Test(TabDecalV2() As ligne)
    ReDim Preserve TabDecalV2(1 To 7)
    TabDecalV2(1).P1.x = 100#
    TabDecalV2(1).P1.y = 5#
    TabDecalV2(1).P2.x = 100#
    TabDecalV2(1).P2.y = 30#
    TabDecalV2(1).E = 0#
    TabDecalV2(2).P1.x = 100#
    TabDecalV2(2).P1.y = 30#
    TabDecalV2(2).P2.x = 0#
    TabDecalV2(2).P2.y = 30#
    TabDecalV2(2).E = 0#
    TabDecalV2(3).P1.x = 0#
    TabDecalV2(3).P1.y = 30#
    TabDecalV2(3).P2.x = 0#
    TabDecalV2(3).P2.y = 5#
    TabDecalV2(3).E = 0#
    TabDecalV2(4).P1.x = 0
    TabDecalV2(4).P1.y = 5#
    TabDecalV2(4).P2.x = 100#
    TabDecalV2(4).P2.y = 5#
    TabDecalV2(4).E = 0#
    TabDecalV2(5).P1.x = 45
    TabDecalV2(5).P1.y = 11.113
    TabDecalV2(5).P2.x = 48.148
    TabDecalV2(5).P2.y = 25.93
    TabDecalV2(5).E = 5#
    TabDecalV2(6).P1.x = 48.148
    TabDecalV2(6).P1.y = 25.93
    TabDecalV2(6).P2.x = 85.185
    TabDecalV2(6).P2.y = 25.93
    TabDecalV2(6).E = 2#
    TabDecalV2(7).P1.x = 85.185
    TabDecalV2(7).P1.y = 25.93
    TabDecalV2(7).P2.x = 100
    TabDecalV2(7).P2.y = 11.113
    TabDecalV2(7).E = 2#

    ReDim Preserve TabDecalV2(1 To 4)
    TabDecalV2(1).P1.x = 0#
    TabDecalV2(1).P1.y = 0#
    TabDecalV2(1).P2.x = 20#
    TabDecalV2(1).P2.y = 0#
    TabDecalV2(1).E = -1#
    TabDecalV2(2).P1.x = 20#
    TabDecalV2(2).P1.y = 0#
    TabDecalV2(2).P2.x = 20#
    TabDecalV2(2).P2.y = 20#
    TabDecalV2(2).E = -2#
    TabDecalV2(3).P1.x = 20#
    TabDecalV2(3).P1.y = 20#
    TabDecalV2(3).P2.x = 0#
    TabDecalV2(3).P2.y = 20#
    TabDecalV2(3).E = -3#
    TabDecalV2(4).P1.x = 0
    TabDecalV2(4).P1.y = 20#
    TabDecalV2(4).P2.x = 0#
    TabDecalV2(4).P2.y = 0#
    TabDecalV2(4).E = -2#
End Sub

Function Temps_Decoupe(TabDecoupe() As ElemDecoupe) As Double
    'Calcul du temps de d�coupe : appels � CommandeDATA avec bCalculSimple � True
    Dim i As Long
    Dim TpsReel As Single
    
    Temps_Decoupe = 0#
    Call maClasseHID.EnvoiUSB       'Pour initialiser les d�multiplications
    
    For i = LBound(TabDecoupe) + 1 To UBound(TabDecoupe)
        Call maClasseHID.CommandeDATA(TabDecoupe(i).X_pas(EMPLANTURE), _
                                      TabDecoupe(i).Y_pas(EMPLANTURE), _
                                      TabDecoupe(i).X_pas(SAUMON), _
                                      TabDecoupe(i).Y_pas(SAUMON), _
                                      TabDecoupe(i).Temps, TpsReel, True)
        Temps_Decoupe = Temps_Decoupe + TpsReel
    Next i
End Function

Sub RetourFDC(x As Boolean, Origin As Boolean)
    'Retour aux origines avec les fins de course
    Dim Dist As Single
    Dim VmaxAvecAcc As Single
    Dim bDG As Boolean
    Dim NbPas As Long
    Dim TpsReel As Single
    
    If Not maClasseHID.TableVerif Then Exit Sub
    
    If x Then
        Dist = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_16", 0)
        VmaxAvecAcc = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 0)
    Else
        Dist = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_17", 0)
        VmaxAvecAcc = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0)
    End If
    
    'FDC On
    Call maClasseHID.FDC(FdcOn)
    'D�placement � Vmax avec acc�l�ration de Dist sur les deux axes
    If Origin Then Dist = -Dist
    If x And gbOrientationInv Then Dist = -Dist
    Call maClasseHID.EnvoiUSB
    Call maClasseHID.CommandeDATAmm(IIf(x, Dist, 0), _
                                    IIf(x, 0, Dist), _
                                    IIf(x, Dist, 0), _
                                    IIf(x, 0, Dist), _
                                    Calcul_Temps_Distance(Dist, VmaxAvecAcc), _
                                    TpsReel, False)
    Call maClasseHID.CommandeDATAend
        
    'Attendre que IPL5X soit arr�t�
    Call maClasseHID.DemandeInformation
    Do While maClasseHID.Step_Processed
        Call maClasseHID.DemandeInformation
        DoEvents
    Loop
                
    'Retour pas � pas alternativement pour G puis D jusqu'� ce que les contacts se referment
    'FDC Off
    Call maClasseHID.FDC(FdcOff)
    bDG = False
    NbPas = IIf(Origin, -1, 1)
    If x And gbOrientationInv Then NbPas = -NbPas
    While maClasseHID.FDC(FdcRead)
'Debug.Print "FDC : " & maClasseHID.FDC(FdcRead)
            'XD, YD, XG, YG
        Call maClasseHID.EnvoiUSB
        Call maClasseHID.CommandeDATA(IIf(x, NbPas, 0) * IIf(bDG, 1, 0), _
                                      IIf(x, 0, NbPas) * IIf(bDG, 1, 0), _
                                      IIf(x, NbPas, 0) * IIf(bDG, 0, 1), _
                                      IIf(x, 0, NbPas) * IIf(bDG, 0, 1), _
                                      0.01, _
                                      TpsReel, False)
        Call maClasseHID.CommandeDATAend
        'Attendre que IPL5X soit arr�t�
        While maClasseHID.Step_Processed
            Call maClasseHID.DemandeInformation
            DoEvents
        Wend
        bDG = Not bDG
    Wend
    'L'axe "bDG" est au max, on s'occupe de l'autre
    'FDC On
    Call maClasseHID.FDC(FdcOn)
    'Remont�e � Vmax avec acc�l�ration de Dist sur l'axe qui n'est pas encore au max
    Call maClasseHID.EnvoiUSB
    Call maClasseHID.CommandeDATAmm(IIf(x, Dist, 0) * IIf(bDG, 1, 0), _
                                    IIf(x, 0, Dist) * IIf(bDG, 1, 0), _
                                    IIf(x, Dist, 0) * IIf(bDG, 0, 1), _
                                    IIf(x, 0, Dist) * IIf(bDG, 0, 1), _
                                    Calcul_Temps_Distance(Dist, VmaxAvecAcc), _
                                    TpsReel, False)
    Call maClasseHID.CommandeDATAend
        
    'Attendre que IPL5X soit arr�t�
    While maClasseHID.Step_Processed
        Call maClasseHID.DemandeInformation
        DoEvents
    Wend
        
    'Descente pas � pas jusqu'� ce que les contacts se referment
    'FDC Off
    
    Call maClasseHID.FDC(FdcOff)
    NbPas = IIf(Origin, 1, -1)
    If x And gbOrientationInv Then NbPas = -NbPas
    While maClasseHID.FDC(FdcRead)
            'XD, YD, XG, YG
        Call maClasseHID.EnvoiUSB
        Call maClasseHID.CommandeDATA(IIf(x, NbPas, 0) * IIf(bDG, 1, 0), _
                                      IIf(x, 0, NbPas) * IIf(bDG, 1, 0), _
                                      IIf(x, NbPas, 0) * IIf(bDG, 0, 1), _
                                      IIf(x, 0, NbPas) * IIf(bDG, 0, 1), _
                                      0.01, _
                                      TpsReel, False)
        Call maClasseHID.CommandeDATAend
    Wend
    'Attendre que IPL5X soit arr�t�
    While maClasseHID.Step_Processed
        Call maClasseHID.DemandeInformation
        DoEvents
    Wend
    'FDC On
    Call maClasseHID.FDC(FdcOn)
End Sub
