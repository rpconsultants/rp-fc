Attribute VB_Name = "Module_HEX"
Option Explicit

Function str2Byte(s As String) As Byte
    'Re�oit 2 caract�res ascii, renvoie un byte
    If Len(s) <> 2 Then MsgBox "Pb str2Byte"
    str2Byte = str2Quartet(left(s, 1)) * 2 ^ 4 + _
              str2Quartet(Mid(s, 2, 1)) * 2 ^ 0
End Function

Function str2Int(s As String) As Long
    'Re�oit 4 caract�res ascii, renvoie un entier non sign� de 0 � 65535
    If Len(s) <> 4 Then MsgBox "Pb str2Int"
    str2Int = str2Quartet(left(s, 1)) * 2 ^ 4 + _
              str2Quartet(Mid(s, 2, 1)) * 2 ^ 0 + _
              str2Quartet(Mid(s, 3, 1)) * 2 ^ 12 + _
              str2Quartet(Mid(s, 4, 1)) * 2 ^ 8
End Function

Function str2Offset(s As String) As Long
    'Re�oit 4 caract�res ascii, renvoie un entier non sign� de 0 � 65535
    If Len(s) <> 4 Then MsgBox "Pb str2Offset"
    str2Offset = str2Quartet(left(s, 1)) * 2 ^ 12 + _
              str2Quartet(Mid(s, 2, 1)) * 2 ^ 8 + _
              str2Quartet(Mid(s, 3, 1)) * 2 ^ 4 + _
              str2Quartet(Mid(s, 4, 1)) * 2 ^ 0
End Function

Function str2Mot(s As String) As Long
    'Re�oit 4 caract�res ascii, renvoie un mot PIC (entier � l'envers)
    If Len(s) <> 4 Then MsgBox "Pb str2Mot"
    str2Mot = str2Quartet(left(s, 1)) * 2 ^ 4 + _
              str2Quartet(Mid(s, 2, 1)) * 2 ^ 0 + _
              str2Quartet(Mid(s, 3, 1)) * 2 ^ 12 + _
              str2Quartet(Mid(s, 4, 1)) * 2 ^ 8
End Function

Function str2Quartet(s As String) As Byte
    'Re�oit un caract�re ASCII, renvoie un quartet
    If Len(s) <> 1 Then MsgBox "Pb str2Quartet"
    
    Select Case s
        Case "0"
            str2Quartet = &H0
        Case "1"
            str2Quartet = &H1
        Case "2"
            str2Quartet = &H2
        Case "3"
            str2Quartet = &H3
        Case "4"
            str2Quartet = &H4
        Case "5"
            str2Quartet = &H5
        Case "6"
            str2Quartet = &H6
        Case "7"
            str2Quartet = &H7
        Case "8"
            str2Quartet = &H8
        Case "9"
            str2Quartet = &H9
        Case "A"
            str2Quartet = &HA
        Case "B"
            str2Quartet = &HB
        Case "C"
            str2Quartet = &HC
        Case "D"
            str2Quartet = &HD
        Case "E"
            str2Quartet = &HE
        Case "F"
            str2Quartet = &HF
        End Select
End Function


