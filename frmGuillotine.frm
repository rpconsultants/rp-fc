VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmGuillotine 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Guillotines"
   ClientHeight    =   8265
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   11700
   FillStyle       =   0  'Solid
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8265
   ScaleWidth      =   11700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.CheckBox chkOrientation 
      Caption         =   "Orientation table"
      Height          =   345
      Left            =   5640
      TabIndex        =   29
      Tag             =   "210"
      Top             =   7440
      Visible         =   0   'False
      Width           =   1515
   End
   Begin MSComctlLib.StatusBar SB 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   99
      Top             =   8010
      Width           =   11700
      _ExtentX        =   20638
      _ExtentY        =   450
      Style           =   1
      SimpleText      =   "..."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame frame_Degagement 
      Caption         =   "D�gagement"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   3480
      TabIndex        =   83
      Tag             =   "135"
      ToolTipText     =   "0"
      Top             =   6360
      Width           =   2085
      Begin VB.CheckBox Check_Degagement 
         Caption         =   "Activer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   120
         TabIndex        =   93
         Tag             =   "145"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   1860
      End
      Begin VB.CommandButton cmdD 
         Height          =   300
         Index           =   0
         Left            =   1215
         Picture         =   "frmGuillotine.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   89
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   765
         Width           =   280
      End
      Begin VB.CommandButton cmdD 
         Height          =   270
         Index           =   1
         Left            =   1500
         Picture         =   "frmGuillotine.frx":0353
         Style           =   1  'Graphical
         TabIndex        =   88
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   795
         Width           =   280
      End
      Begin VB.CommandButton cmdD 
         Height          =   300
         Index           =   3
         Left            =   1215
         Picture         =   "frmGuillotine.frx":06A0
         Style           =   1  'Graphical
         TabIndex        =   87
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1065
         Width           =   280
      End
      Begin VB.CommandButton cmdD 
         Height          =   270
         Index           =   2
         Left            =   1500
         Picture         =   "frmGuillotine.frx":09F6
         Style           =   1  'Graphical
         TabIndex        =   86
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1065
         Width           =   280
      End
      Begin VB.TextBox TB_Degagement 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   255
         Locked          =   -1  'True
         TabIndex        =   85
         ToolTipText     =   "0"
         Top             =   900
         Width           =   900
      End
      Begin VB.CheckBox Check_Degagement 
         Caption         =   "Tranche n�1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   120
         TabIndex        =   84
         Tag             =   "140"
         ToolTipText     =   "0"
         Top             =   270
         Width           =   1860
      End
   End
   Begin VB.Frame Frame_MainsLibres 
      Caption         =   "Mains libres"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2070
      Left            =   9360
      TabIndex        =   74
      Tag             =   "185"
      ToolTipText     =   "0"
      Top             =   4080
      Width           =   2295
      Begin VB.Timer Timer_Decoupe 
         Enabled         =   0   'False
         Left            =   0
         Top             =   1560
      End
      Begin VB.Timer Timer_Armement 
         Enabled         =   0   'False
         Left            =   120
         Top             =   600
      End
      Begin VB.TextBox TB_DelayDecoupe 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   540
         TabIndex        =   78
         Text            =   "5"
         ToolTipText     =   "0"
         Top             =   1545
         Width           =   480
      End
      Begin VB.CheckBox Check_DecoupeAuto 
         Caption         =   "D�coupe auto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   345
         Left            =   120
         TabIndex        =   77
         Tag             =   "200"
         ToolTipText     =   "0"
         Top             =   1110
         Width           =   2100
      End
      Begin VB.TextBox TB_DelayArmement 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   540
         TabIndex        =   76
         Text            =   "5"
         ToolTipText     =   "0"
         Top             =   645
         Width           =   480
      End
      Begin VB.CheckBox Check_ArmementAuto 
         Caption         =   "R�armement auto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   345
         Left            =   120
         TabIndex        =   75
         Tag             =   "190"
         ToolTipText     =   "0"
         Top             =   300
         Width           =   2100
      End
      Begin VB.Label Label2 
         Caption         =   "secondes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   270
         Left            =   1095
         TabIndex        =   80
         Tag             =   "205"
         ToolTipText     =   "0"
         Top             =   1575
         Width           =   1050
      End
      Begin VB.Label Label1 
         Caption         =   "secondes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   270
         Left            =   1095
         TabIndex        =   79
         Tag             =   "195"
         ToolTipText     =   "0"
         Top             =   675
         Width           =   1050
      End
   End
   Begin VB.Frame frmBloc 
      Caption         =   "Visualisation du bloc"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   5880
      TabIndex        =   53
      Tag             =   "150"
      ToolTipText     =   "0"
      Top             =   6195
      Width           =   5820
      Begin VB.CommandButton cmdOffset 
         Height          =   300
         Index           =   1
         Left            =   5025
         Picture         =   "frmGuillotine.frx":0D43
         Style           =   1  'Graphical
         TabIndex        =   67
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1305
         Width           =   280
      End
      Begin VB.CommandButton cmdOffset 
         Height          =   300
         Index           =   0
         Left            =   5025
         Picture         =   "frmGuillotine.frx":1099
         Style           =   1  'Graphical
         TabIndex        =   66
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1005
         Width           =   280
      End
      Begin VB.TextBox TB_BlocOffset 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4095
         Locked          =   -1  'True
         TabIndex        =   65
         Text            =   "5"
         ToolTipText     =   "0"
         Top             =   1155
         Width           =   900
      End
      Begin VB.CommandButton cmdH 
         Height          =   300
         Index           =   2
         Left            =   3615
         Picture         =   "frmGuillotine.frx":13EC
         Style           =   1  'Graphical
         TabIndex        =   64
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   615
         Width           =   280
      End
      Begin VB.CommandButton cmdH 
         Height          =   300
         Index           =   1
         Left            =   3615
         Picture         =   "frmGuillotine.frx":1742
         Style           =   1  'Graphical
         TabIndex        =   63
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   315
         Width           =   280
      End
      Begin VB.CommandButton cmdH 
         Height          =   330
         Index           =   3
         Left            =   3330
         Picture         =   "frmGuillotine.frx":1A95
         Style           =   1  'Graphical
         TabIndex        =   62
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   615
         Width           =   280
      End
      Begin VB.CommandButton cmdH 
         Height          =   330
         Index           =   0
         Left            =   3330
         Picture         =   "frmGuillotine.frx":1DEF
         Style           =   1  'Graphical
         TabIndex        =   61
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   285
         Width           =   280
      End
      Begin VB.TextBox TB_BlocHauteur 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   60
         Text            =   "50"
         ToolTipText     =   "0"
         Top             =   525
         Width           =   900
      End
      Begin VB.TextBox TB_BlocLargeur 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4095
         Locked          =   -1  'True
         TabIndex        =   59
         Text            =   "100"
         ToolTipText     =   "0"
         Top             =   525
         Width           =   900
      End
      Begin VB.CommandButton cmdL 
         Height          =   330
         Index           =   0
         Left            =   5025
         Picture         =   "frmGuillotine.frx":214A
         Style           =   1  'Graphical
         TabIndex        =   58
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   285
         Width           =   280
      End
      Begin VB.CommandButton cmdL 
         Height          =   330
         Index           =   3
         Left            =   5025
         Picture         =   "frmGuillotine.frx":24A5
         Style           =   1  'Graphical
         TabIndex        =   57
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   615
         Width           =   280
      End
      Begin VB.CommandButton cmdL 
         Height          =   300
         Index           =   1
         Left            =   5310
         Picture         =   "frmGuillotine.frx":27FF
         Style           =   1  'Graphical
         TabIndex        =   56
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   315
         Width           =   280
      End
      Begin VB.CommandButton cmdL 
         Height          =   300
         Index           =   2
         Left            =   5310
         Picture         =   "frmGuillotine.frx":2B52
         Style           =   1  'Graphical
         TabIndex        =   55
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   615
         Width           =   280
      End
      Begin VB.CheckBox Check_Bloc 
         Height          =   585
         Left            =   495
         Picture         =   "frmGuillotine.frx":2EA8
         Style           =   1  'Graphical
         TabIndex        =   54
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   390
         Width           =   960
      End
      Begin VB.Label lblMemoBloc 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Dimensions bloc"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   360
         TabIndex        =   103
         Tag             =   "220"
         ToolTipText     =   "tt220"
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label lblOffset 
         Alignment       =   1  'Right Justify
         Caption         =   "D�calage suivant X (mm)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1485
         TabIndex        =   70
         Tag             =   "165"
         ToolTipText     =   "0"
         Top             =   1215
         Width           =   2475
      End
      Begin VB.Label lblH 
         Caption         =   "H (mm)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2505
         TabIndex        =   69
         Tag             =   "155"
         ToolTipText     =   "0"
         Top             =   225
         Width           =   780
      End
      Begin VB.Label lblL 
         Caption         =   "L (mm)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4185
         TabIndex        =   68
         Tag             =   "160"
         ToolTipText     =   "0"
         Top             =   225
         Width           =   720
      End
   End
   Begin VB.Frame frame_Tranches 
      Caption         =   "Tranches"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   39
      Tag             =   "120"
      ToolTipText     =   "0"
      Top             =   6195
      Width           =   5715
      Begin VB.CommandButton cmdn 
         Height          =   300
         Index           =   0
         Left            =   2790
         Picture         =   "frmGuillotine.frx":329B
         Style           =   1  'Graphical
         TabIndex        =   49
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   585
         Width           =   280
      End
      Begin VB.CommandButton cmdn 
         Height          =   300
         Index           =   3
         Left            =   2790
         Picture         =   "frmGuillotine.frx":35EE
         Style           =   1  'Graphical
         TabIndex        =   48
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   885
         Width           =   280
      End
      Begin VB.TextBox TB_Tranchen 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   46
         Text            =   "1"
         ToolTipText     =   "0"
         Top             =   750
         Width           =   705
      End
      Begin VB.TextBox TB_Tranchee 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   330
         Left            =   315
         Locked          =   -1  'True
         TabIndex        =   44
         Text            =   "15"
         ToolTipText     =   "0"
         Top             =   750
         Width           =   900
      End
      Begin VB.CommandButton cmdE 
         Height          =   270
         Index           =   2
         Left            =   1545
         Picture         =   "frmGuillotine.frx":3944
         Style           =   1  'Graphical
         TabIndex        =   43
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   870
         Width           =   280
      End
      Begin VB.CommandButton cmdE 
         Height          =   300
         Index           =   3
         Left            =   1260
         Picture         =   "frmGuillotine.frx":3C91
         Style           =   1  'Graphical
         TabIndex        =   42
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   870
         Width           =   280
      End
      Begin VB.CommandButton cmdE 
         Height          =   270
         Index           =   1
         Left            =   1545
         Picture         =   "frmGuillotine.frx":3FE7
         Style           =   1  'Graphical
         TabIndex        =   41
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   280
      End
      Begin VB.CommandButton cmdE 
         Height          =   300
         Index           =   0
         Left            =   1260
         Picture         =   "frmGuillotine.frx":4334
         Style           =   1  'Graphical
         TabIndex        =   40
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   570
         Width           =   280
      End
      Begin VB.Label lblN 
         Caption         =   "n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2325
         TabIndex        =   47
         Tag             =   "130"
         ToolTipText     =   "0"
         Top             =   435
         Width           =   225
      End
      Begin VB.Label lblE 
         Caption         =   "e (mm)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   405
         TabIndex        =   45
         Tag             =   "125"
         ToolTipText     =   "0"
         Top             =   435
         Width           =   765
      End
   End
   Begin VB.Frame Frame_Fil 
      Caption         =   "Fil"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1485
      Left            =   8520
      TabIndex        =   35
      Tag             =   "55"
      ToolTipText     =   "0"
      Top             =   30
      Width           =   3120
      Begin VB.TextBox txtSaignee 
         Alignment       =   2  'Center
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1815
         Locked          =   -1  'True
         TabIndex        =   90
         Text            =   "1 mm"
         ToolTipText     =   "0"
         Top             =   870
         Width           =   1200
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Chauffe :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   9
         Left            =   120
         TabIndex        =   38
         Tag             =   "60"
         ToolTipText     =   "0"
         Top             =   390
         Width           =   1530
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Saign�e (mm) :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   7
         Left            =   120
         TabIndex        =   37
         Tag             =   "65"
         ToolTipText     =   "0"
         Top             =   870
         Width           =   1530
      End
      Begin VB.Label lblChauffe 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Caption         =   "X %"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1815
         TabIndex        =   36
         ToolTipText     =   "0"
         Top             =   405
         Width           =   1200
      End
   End
   Begin VB.Frame Frame_Mouvements 
      Caption         =   "Mouvements"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2460
      Left            =   9360
      TabIndex        =   33
      Tag             =   "170"
      ToolTipText     =   "0"
      Top             =   1545
      Width           =   2295
      Begin VB.CheckBox Check_Chauffe 
         BackColor       =   &H0000FF00&
         Caption         =   "Chauffe/armement"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   102
         Tag             =   "182"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   2010
      End
      Begin VB.CommandButton Cmd_Armement 
         BackColor       =   &H0000FF00&
         Caption         =   "Armement"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   101
         Tag             =   "175"
         ToolTipText     =   "0"
         Top             =   345
         Width           =   2010
      End
      Begin VB.CommandButton Cmd_Stop 
         BackColor       =   &H000000FF&
         Caption         =   "Stop"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   100
         Tag             =   "181"
         ToolTipText     =   "0"
         Top             =   1800
         Visible         =   0   'False
         Width           =   2010
      End
      Begin VB.CommandButton Cmd_Decoupe 
         BackColor       =   &H0080C0FF&
         Caption         =   "D�coupe"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   34
         Tag             =   "180"
         ToolTipText     =   "0"
         Top             =   1200
         Width           =   2010
      End
   End
   Begin VB.Frame Frame_Vitesse 
      Caption         =   "Vitesses et mati�re"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1485
      Left            =   120
      TabIndex        =   1
      Tag             =   "5"
      ToolTipText     =   "0"
      Top             =   30
      Width           =   8280
      Begin VB.ComboBox Combo_Mat 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4560
         Style           =   2  'Dropdown List
         TabIndex        =   98
         Tag             =   "1"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   3465
      End
      Begin VB.OptionButton Option_Vitesse 
         Caption         =   "Vitesse maxi sans acc�l�ration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   1
         Left            =   4080
         TabIndex        =   97
         Tag             =   "25"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1935
      End
      Begin VB.OptionButton Option_Vitesse 
         Caption         =   "Vitesse maxi avec acc�l�ration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   0
         Left            =   1560
         TabIndex        =   96
         Tag             =   "15"
         ToolTipText     =   "0"
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.TextBox TB_VDecoupe 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1380
         TabIndex        =   72
         Text            =   "4"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   705
      End
      Begin VB.TextBox TB_VArmement 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6600
         TabIndex        =   71
         ToolTipText     =   "0"
         Top             =   300
         Width           =   645
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Mati�re : "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   4
         Left            =   3000
         TabIndex        =   7
         Tag             =   "50"
         ToolTipText     =   "0"
         Top             =   1065
         Width           =   1290
      End
      Begin VB.Line Line1 
         X1              =   165
         X2              =   8040
         Y1              =   870
         Y2              =   870
      End
      Begin VB.Label Label3 
         Caption         =   "D�coupe :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   3
         Left            =   135
         TabIndex        =   6
         Tag             =   "40"
         ToolTipText     =   "0"
         Top             =   1065
         Width           =   1170
      End
      Begin VB.Label Label3 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   2145
         TabIndex        =   5
         Tag             =   "45"
         ToolTipText     =   "0"
         Top             =   1095
         Width           =   630
      End
      Begin VB.Label Label3 
         Caption         =   "Armement :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   135
         TabIndex        =   4
         Tag             =   "10"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   1245
      End
      Begin VB.Label Label3 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   7320
         TabIndex        =   3
         Tag             =   "35"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   645
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "ou"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   6
         Left            =   3465
         TabIndex        =   2
         Tag             =   "20"
         ToolTipText     =   "0"
         Top             =   345
         Width           =   450
      End
   End
   Begin VB.Frame Frame_Directions 
      Caption         =   "Directions et dimensions"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4605
      Left            =   120
      TabIndex        =   0
      Tag             =   "70"
      ToolTipText     =   "0"
      Top             =   1545
      Width           =   9240
      Begin VB.CommandButton cmdRAZ 
         Caption         =   "RAZ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   150
         TabIndex        =   52
         Tag             =   "95"
         ToolTipText     =   "0"
         Top             =   2460
         Width           =   825
      End
      Begin VB.OptionButton Option_Direction 
         Caption         =   "Horizontale"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   160
         TabIndex        =   10
         Tag             =   "85"
         ToolTipText     =   "0"
         Top             =   1360
         Width           =   1545
      End
      Begin VB.OptionButton Option_Direction 
         Caption         =   "Oblique"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   160
         TabIndex        =   9
         Tag             =   "80"
         ToolTipText     =   "0"
         Top             =   860
         Width           =   1545
      End
      Begin VB.OptionButton Option_Direction 
         Caption         =   "Verticale"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   160
         TabIndex        =   8
         Tag             =   "75"
         ToolTipText     =   "0"
         Top             =   360
         Value           =   -1  'True
         Width           =   1545
      End
      Begin VB.OptionButton Option_Direction 
         Caption         =   "Tranches"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   160
         TabIndex        =   31
         Tag             =   "90"
         ToolTipText     =   "0"
         Top             =   1860
         Width           =   1545
      End
      Begin VB.PictureBox pctG 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         FillStyle       =   0  'Solid
         Height          =   4290
         Left            =   1620
         ScaleHeight     =   4290
         ScaleWidth      =   7575
         TabIndex        =   11
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   255
         Width           =   7575
         Begin VB.CommandButton cmdX 
            Height          =   300
            Index           =   4
            Left            =   3690
            Picture         =   "frmGuillotine.frx":4687
            Style           =   1  'Graphical
            TabIndex        =   95
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3900
            Width           =   280
         End
         Begin VB.CommandButton cmdX 
            Height          =   330
            Index           =   5
            Left            =   3405
            Picture         =   "frmGuillotine.frx":49DD
            Style           =   1  'Graphical
            TabIndex        =   94
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3900
            Width           =   280
         End
         Begin VB.CommandButton cmdy 
            Height          =   270
            Index           =   2
            Left            =   7350
            Picture         =   "frmGuillotine.frx":4D37
            Style           =   1  'Graphical
            TabIndex        =   92
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   2775
            Width           =   280
         End
         Begin VB.CommandButton cmdy 
            Height          =   270
            Index           =   3
            Left            =   7350
            Picture         =   "frmGuillotine.frx":5084
            Style           =   1  'Graphical
            TabIndex        =   91
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3045
            Width           =   280
         End
         Begin VB.TextBox TB_YReel 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5850
            Locked          =   -1  'True
            TabIndex        =   81
            ToolTipText     =   "0"
            Top             =   3510
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.CommandButton cmdy 
            Height          =   300
            Index           =   4
            Left            =   7065
            Picture         =   "frmGuillotine.frx":53D1
            Style           =   1  'Graphical
            TabIndex        =   26
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3045
            Width           =   280
         End
         Begin VB.CommandButton cmdy 
            Height          =   300
            Index           =   1
            Left            =   7065
            Picture         =   "frmGuillotine.frx":5727
            Style           =   1  'Graphical
            TabIndex        =   25
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   2745
            Width           =   280
         End
         Begin VB.CommandButton cmdy 
            Height          =   330
            Index           =   5
            Left            =   6780
            Picture         =   "frmGuillotine.frx":5A7A
            Style           =   1  'Graphical
            TabIndex        =   24
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3045
            Width           =   280
         End
         Begin VB.CommandButton cmdy 
            Height          =   330
            Index           =   0
            Left            =   6780
            Picture         =   "frmGuillotine.frx":5DD4
            Style           =   1  'Graphical
            TabIndex        =   23
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   2715
            Width           =   280
         End
         Begin VB.CommandButton cmdX 
            Height          =   270
            Index           =   2
            Left            =   3975
            Picture         =   "frmGuillotine.frx":612F
            Style           =   1  'Graphical
            TabIndex        =   22
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3630
            Width           =   280
         End
         Begin VB.CommandButton cmdX 
            Height          =   300
            Index           =   1
            Left            =   3690
            Picture         =   "frmGuillotine.frx":647C
            Style           =   1  'Graphical
            TabIndex        =   21
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3600
            Width           =   280
         End
         Begin VB.CommandButton cmdX 
            Height          =   270
            Index           =   3
            Left            =   3975
            Picture         =   "frmGuillotine.frx":67CF
            Style           =   1  'Graphical
            TabIndex        =   20
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3900
            Width           =   280
         End
         Begin VB.CommandButton cmdX 
            Height          =   330
            Index           =   0
            Left            =   3405
            Picture         =   "frmGuillotine.frx":6B1C
            Style           =   1  'Graphical
            TabIndex        =   19
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3570
            Width           =   280
         End
         Begin VB.CommandButton cmdB 
            Height          =   300
            Index           =   0
            Left            =   2445
            Picture         =   "frmGuillotine.frx":6E77
            Style           =   1  'Graphical
            TabIndex        =   18
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   60
            Width           =   280
         End
         Begin VB.CommandButton cmdB 
            Height          =   270
            Index           =   1
            Left            =   2730
            Picture         =   "frmGuillotine.frx":71CA
            Style           =   1  'Graphical
            TabIndex        =   17
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   90
            Width           =   280
         End
         Begin VB.CommandButton cmdB 
            Height          =   300
            Index           =   3
            Left            =   2445
            Picture         =   "frmGuillotine.frx":7517
            Style           =   1  'Graphical
            TabIndex        =   16
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   280
         End
         Begin VB.CommandButton cmdB 
            Height          =   270
            Index           =   2
            Left            =   2730
            Picture         =   "frmGuillotine.frx":786D
            Style           =   1  'Graphical
            TabIndex        =   15
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   280
         End
         Begin VB.TextBox TB_Y 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5850
            Locked          =   -1  'True
            TabIndex        =   14
            Text            =   "60"
            ToolTipText     =   "0"
            Top             =   2880
            Width           =   900
         End
         Begin VB.TextBox TB_X 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2460
            Locked          =   -1  'True
            TabIndex        =   13
            ToolTipText     =   "0"
            Top             =   3720
            Width           =   900
         End
         Begin VB.TextBox txtBeta 
            Alignment       =   2  'Center
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1500
            Locked          =   -1  'True
            TabIndex        =   12
            ToolTipText     =   "0"
            Top             =   165
            Width           =   900
         End
         Begin VB.Label lblYReel 
            Caption         =   "Y r�el"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   5985
            TabIndex        =   82
            Tag             =   "115"
            ToolTipText     =   "0"
            Top             =   3255
            Visible         =   0   'False
            Width           =   765
         End
         Begin VB.Label lblDim 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "LblDim"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Index           =   1
            Left            =   1680
            TabIndex        =   73
            ToolTipText     =   "0"
            Top             =   2145
            Width           =   945
         End
         Begin VB.Label lblDim 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "LblDim"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Index           =   0
            Left            =   1620
            TabIndex        =   51
            ToolTipText     =   "0"
            Top             =   1665
            Width           =   945
         End
         Begin VB.Label lblDim 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "LblDim"
            ForeColor       =   &H00FF0000&
            Height          =   225
            Index           =   2
            Left            =   1680
            TabIndex        =   50
            ToolTipText     =   "0"
            Top             =   2865
            Width           =   945
         End
         Begin VB.Image imgB 
            Height          =   300
            Left            =   1050
            Picture         =   "frmGuillotine.frx":7BBA
            Top             =   210
            Width           =   135
         End
         Begin VB.Label lblZoom 
            Alignment       =   1  'Right Justify
            Caption         =   "Label7"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   5985
            TabIndex        =   32
            ToolTipText     =   "0"
            Top             =   3975
            Width           =   1455
         End
         Begin VB.Label lblB 
            Caption         =   "(�)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            TabIndex        =   30
            Tag             =   "100"
            ToolTipText     =   "0"
            Top             =   195
            Width           =   225
         End
         Begin VB.Label lblY 
            Caption         =   "Y (mm)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   5985
            TabIndex        =   28
            Tag             =   "110"
            ToolTipText     =   "0"
            Top             =   2580
            Width           =   765
         End
         Begin VB.Label lblX 
            Caption         =   "X (mm)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1695
            TabIndex        =   27
            Tag             =   "105"
            ToolTipText     =   "0"
            Top             =   3735
            Width           =   720
         End
      End
   End
End
Attribute VB_Name = "frmGuillotine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim HauteurApproche As Single
Dim CoupeHauteur As Single
Dim CoupeLargeur As Single
Dim beta As Double
Dim sngTrajet() As Single
Dim TypeGuillotine As String

Dim CourseY As Single
Dim CourseX As Single
Dim OffsetOrigineX As Single

Dim HauteurZone, LargeurZone As Single
Dim IncrementZoom_mm As Integer
Dim Rapport As Double
Dim FacteurZoom_V As Integer
Dim FacteurZoom_H As Integer
Dim FacteurZoom As Integer

Dim TrancheEpaisseur As Single
Dim TrancheDegagement As Single
Dim TrancheNombre As Integer
Dim Saignee As Single
Dim BlocHauteur As Single
Dim BlocLargeur As Single
Dim BlocOffset As Single
Dim Objet As Control
Dim Dim_Bas As Single
Dim Dim_Haut As Single
Dim Dim_Haut_2 As Single
Dim i As Integer
Dim A As Single
Dim B As Single
Dim c As Single
Dim n As Single
Dim TpsReel As Double

Const VERTICALE = 0
Const OBLIQUE = 1
Const HORIZONTALE = 2
Const TRANCHES = 3

Private Sub Cmd_Armement_Click()
    Dim P As Point
    Dim P1 As Point
    Dim k As Integer
    
    If Not maClasseHID.IsAvailable Then Exit Sub
    
    Call maClasseHID.DemandeInformation
    If Not maClasseHID.PWM_ON Or maClasseHID.PWM_Manual Then
        If MsgBox(ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 255), vbQuestion + vbOKCancel) = vbCancel Then
            Exit Sub
        End If
    End If
    
    Cmd_Armement.Enabled = False
    Cmd_Decoupe.Enabled = False
    DeplEnCours = True
    gPause = False
    ReDim Tab_Chariot(1 To 1)
    Frame_Directions.Enabled = False
    Cmd_Stop.Visible = True
    
    If Check_Chauffe Then
        'Envoi consigne de chauffe
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 207)
        Call maClasseHID.ChauffeEcrire(Round(Val(lblChauffe) * 255 / 100, 0), True)
    End If
    
    If Option_Direction(VERTICALE) Then  'verticale
        P.x = 0#
        P.y = TB_Y
    End If
    If Option_Direction(OBLIQUE) Then  'oblique
        P.x = TB_X
        P.y = TB_Y
    End If
    If Option_Direction(HORIZONTALE) Then  'horizontale
        P.x = 0#
        P.y = TB_Y + IIf(Check_Degagement(1).value = vbChecked, CDbl(TB_Degagement), 0#)
    End If
    If Option_Direction(TRANCHES) Then  'tranches
        P.x = 0#
        P.y = TB_Y
    End If
    
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 208)
    
    Tab_Chariot(1).P_abs(EMPLANTURE) = P
    Tab_Chariot(1).P_abs(SAUMON) = P
    P1.x = 0#
    P1.y = 0#
    Tab_Chariot(1).Temps = Calcul_Temps_Points(P, P1, IIf(Check_Chauffe, CDbl(TB_VDecoupe), CDbl(TB_VArmement)))
    P1 = P
    
    If Option_Direction(2) Then  'horizontale
        P.x = TB_X
        P.y = 0#
        ReDim Preserve Tab_Chariot(1 To 3)
        Tab_Chariot(2).P_abs(EMPLANTURE) = P
        Tab_Chariot(2).P_abs(SAUMON) = P
        Tab_Chariot(2).Temps = Calcul_Temps_Points(P, P1, IIf(Check_Chauffe, CDbl(TB_VDecoupe), CDbl(TB_VArmement)))
        P1 = P
        P.x = 0#
        P.y = -IIf(Check_Degagement(1).value = vbChecked, CDbl(TB_Degagement), 0#)
        Tab_Chariot(3).P_abs(EMPLANTURE) = P
        Tab_Chariot(3).P_abs(SAUMON) = P
        Tab_Chariot(3).Temps = Calcul_Temps_Points(P, P1, IIf(Check_Chauffe, CDbl(TB_VDecoupe), CDbl(TB_VArmement)))
    End If
    
    If gbOrientationInv Then
        'La table est invers�e, il faut inverser le sens des X
        For k = EMPLANTURE To SAUMON
            For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
                Tab_Chariot(i).P_abs(k).x = -Tab_Chariot(i).P_abs(k).x
            Next i
        Next k
    End If

    Call Envoi_Decoupe(Tab_Chariot, Me)

    'Si la chauffe est demand�e et qu'il existe une tempo chauffe APRES
    If Check_Chauffe Then
        'Envoi consigne de chauffe
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 207)
        Call maClasseHID.ChauffeEcrire(Round(Val(lblChauffe) * 255 / 100, 0), False)
        Call maClasseHID.ChauffeEcrire(0, True) 'L'arr�t de la chauffe est temporis�
    End If
    
    Cmd_Stop.Visible = False
    Cmd_Decoupe.Enabled = True
    Cmd_Decoupe.SetFocus
    If Check_DecoupeAuto Then
        Timer_Decoupe.Interval = CDbl(TB_DelayDecoupe) * 1000
        Timer_Decoupe.Enabled = True
    Else
        Cmd_Decoupe.SetFocus
    End If
    If Check_Chauffe Then
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 207)
        Call maClasseHID.ChauffeEcrire(0, True)
    End If
    DeplEnCours = False
    SB.SimpleText = "..."
End Sub

Private Sub Cmd_Decoupe_Click()
    Dim P As Point
    Dim P1 As Point
    Dim n As Integer
    Dim k As Integer
    Dim bSens As Boolean
    Dim maVitMaxY As Single
    
    If Not maClasseHID.IsAvailable Then Exit Sub
    Call maClasseHID.DemandeInformation
    If Not maClasseHID.PWM_ON Or maClasseHID.PWM_Manual Then
        If MsgBox(ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 255), vbQuestion + vbOKCancel) = vbCancel Then
            Exit Sub
        End If
    End If
    
    Cmd_Decoupe.Enabled = False
    Cmd_Armement.Enabled = False
    DeplEnCours = True
    gPause = False
    maVitMaxY = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 3)

    Cmd_Stop.Visible = True
    'Envoi consigne de chauffe
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 207)
    Call maClasseHID.ChauffeEcrirePC(Round(Val(lblChauffe)))
    
    ReDim Tab_Chariot(1 To 1)
    
    If Option_Direction(VERTICALE) Then  'verticale
        P.x = 0#
        P.y = -TB_Y
    End If
    If Option_Direction(OBLIQUE) Then  'oblique
        P.x = -TB_X
        P.y = -TB_Y
    End If
    If Option_Direction(HORIZONTALE) Then  'horizontale
        P.x = -TB_X
        P.y = 0#
    End If
    If Option_Direction(TRANCHES) Then  'tranches
        P.x = TB_X
        P.y = 0#
    End If
    P1.x = 0#
    P1.y = 0#
    Tab_Chariot(1).P_abs(EMPLANTURE) = P
    Tab_Chariot(1).P_abs(SAUMON) = P
    Tab_Chariot(1).Temps = Calcul_Temps_Points(P1, P, CDbl(TB_VDecoupe))
    P1 = P
    
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 208)
    If Option_Direction(2) Then  'horizontale
        P1.x = 0#
        P1.y = 0#
        P.x = 0#
        P.y = -TB_Y
        ReDim Preserve Tab_Chariot(1 To 2)
        Tab_Chariot(2).P_abs(EMPLANTURE) = P
        Tab_Chariot(2).P_abs(SAUMON) = P
        Tab_Chariot(2).Temps = Calcul_Temps_Points(P1, P, maVitMaxY)
    End If
    If Option_Direction(3) Then  'tranches
        bSens = True
        For n = 1 To Val(TB_Tranchen)
            ReDim Preserve Tab_Chariot(1 To UBound(Tab_Chariot) + 2)
            'Descente de l'�paisseur d'une tranche
            P1.x = 0#
            P1.y = 0#
            P.x = 0#
            P.y = -TB_Tranchee
            Tab_Chariot(UBound(Tab_Chariot) - 1).P_abs(EMPLANTURE) = P
            Tab_Chariot(UBound(Tab_Chariot) - 1).P_abs(SAUMON) = P
            Tab_Chariot(UBound(Tab_Chariot) - 1).Temps = Calcul_Temps_Points(P1, P, maVitMaxY)
            P1 = P
            'D�coupe d'un tranche dans un sens ou dans l'autre
            P.x = IIf(bSens, -TB_X, TB_X)
            bSens = Not bSens
            P.y = 0
            Tab_Chariot(UBound(Tab_Chariot)).P_abs(EMPLANTURE) = P
            Tab_Chariot(UBound(Tab_Chariot)).P_abs(SAUMON) = P
            Tab_Chariot(UBound(Tab_Chariot)).Temps = Calcul_Temps_Points(P1, P, CDbl(TB_VDecoupe))
            P1 = P
        Next n
        P.x = 0
        P.y = -(TB_Y - Val(TB_Tranchen) * TB_Tranchee)
        ReDim Preserve Tab_Chariot(1 To UBound(Tab_Chariot) + 1)
        Tab_Chariot(UBound(Tab_Chariot)).P_abs(EMPLANTURE) = P
        Tab_Chariot(UBound(Tab_Chariot)).P_abs(SAUMON) = P
        Tab_Chariot(UBound(Tab_Chariot)).Temps = Calcul_Temps_Points(P1, P, maVitMaxY)
    End If
    
    Cmd_Armement.Enabled = True
    Frame_Directions.Enabled = True
    
    If gbOrientationInv Then
        'La table est invers�e, il faut inverser le sens des X
        For k = EMPLANTURE To SAUMON
            For i = LBound(Tab_Chariot) To UBound(Tab_Chariot)
                Tab_Chariot(i).P_abs(k).x = -Tab_Chariot(i).P_abs(k).x
            Next i
        Next k
    End If
    
    Cmd_Stop.Visible = True
    Call Envoi_Decoupe(Tab_Chariot, Me)
    Cmd_Stop.Visible = False
    
    'Si la chauffe est demand�e et qu'il existe une tempo chauffe APRES
'    If Check_Chauffe Then
        'Envoi consigne de chauffe
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 207)
        Call maClasseHID.ChauffeEcrire(Round(Val(lblChauffe) * 255 / 100, 0), False)
        Call maClasseHID.ChauffeEcrire(0, True) 'L'arr�t de la chauffe est temporis�
'    End If

    DeplEnCours = False
    SB.SimpleText = "..."
    If Check_ArmementAuto Then
        Timer_Armement.Interval = CDbl(TB_DelayArmement) * 1000
        Timer_Armement.Enabled = True
    Else
        Cmd_Armement.Visible = True
        Cmd_Armement.SetFocus
    End If
End Sub

Private Sub Cmd_Stop_Click()
    gPause = True
End Sub

Private Sub cmdH_Click(Index As Integer)
    Dim maValeur As Integer
    Select Case Index
        Case 0  'P10
            maValeur = 10
        Case 1  'P1
            maValeur = 1
        Case 2  'M1
            maValeur = -1
        Case 3  'M10
            maValeur = -10
    End Select
    BlocHauteur = BlocHauteur + maValeur
    Call CalculsGuillotine
End Sub

Private Sub cmdn_Click(Index As Integer)
    Dim AncienNombre As Integer
    AncienNombre = TrancheNombre
    Select Case Index
        Case 0  'Plus 2
            TrancheNombre = TrancheNombre + 2
        Case 3  'Moins 2
            TrancheNombre = TrancheNombre - 2
    End Select
    TestHauteurTranches
    If CoupeHauteur > CourseY Then
        CoupeHauteur = CourseY
        MsgBox ReadINI_Trad(gLangue, "erreurs", 201), vbCritical
        TrancheNombre = AncienNombre
    End If
    Call CalculsGuillotine
End Sub

Private Sub cmdY_Click(Index As Integer)
    Select Case Index
        Case 0  'P10
            CoupeHauteur = CoupeHauteur + 10
        Case 1  'P1
            CoupeHauteur = CoupeHauteur + 1
        Case 2  'P0
            CoupeHauteur = CoupeHauteur + 0.1
        Case 3  'M0
            CoupeHauteur = CoupeHauteur - 0.1
        Case 4  'M1
            CoupeHauteur = CoupeHauteur - 1
        Case 5  'M10
            CoupeHauteur = CoupeHauteur - 10
    End Select
    If CoupeHauteur > CourseY Then
        CoupeHauteur = CourseY
        MsgBox ReadINI_Trad(gLangue, "erreurs", 200), vbCritical
    End If
    If CoupeHauteur <= 0 Then CoupeHauteur = 0.1
    If TypeGuillotine = "Tranches" Then TestHauteurTranches 'v�rifier que les tranches rentrent dans la hauteur
    Call CalculsGuillotine
End Sub

Private Sub cmdE_Click(Index As Integer)
    Select Case Index
        Case 0  'P1
            TrancheEpaisseur = TrancheEpaisseur + 1
        Case 1  'P0
            TrancheEpaisseur = TrancheEpaisseur + 0.1
        Case 2  'M0
            TrancheEpaisseur = TrancheEpaisseur - 0.1
        Case 3  'M1
            TrancheEpaisseur = TrancheEpaisseur - 1
    End Select
    TestHauteurTranches
    If CoupeHauteur > CourseY Then
        CoupeHauteur = CourseY
        MsgBox ReadINI_Trad(gLangue, "erreurs", 200), vbCritical
        If Index = 0 Then
            TrancheEpaisseur = TrancheEpaisseur - 1
        Else
            TrancheEpaisseur = TrancheEpaisseur - 0.1
        End If
    End If
    If TrancheEpaisseur < 0 Then TrancheEpaisseur = 0
    TrancheDegagement = TrancheEpaisseur
    Call CalculsGuillotine
End Sub

Private Sub cmdD_Click(Index As Integer)
    Dim maValeur As Integer
    Select Case Index
        Case 0  'P10
            maValeur = 10
        Case 1  'P1
            maValeur = 1
        Case 2  'M1
            maValeur = -1
        Case 3  'M10
            maValeur = -10
    End Select
    If TypeGuillotine = "Horizontale" Then
        TrancheDegagement = TrancheDegagement + maValeur
        If CoupeHauteur + TrancheDegagement > CourseY Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 200), vbCritical
            TrancheDegagement = TrancheDegagement - maValeur
        End If
        If TrancheDegagement < 0 Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 201), vbCritical
            TrancheDegagement = TrancheDegagement - maValeur
        End If
    End If
    If TypeGuillotine = "Tranches" Then
        TrancheDegagement = TrancheDegagement + maValeur
        CoupeHauteur = CoupeHauteur + maValeur
        If CoupeHauteur + TrancheDegagement > CourseY Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 200), vbCritical
            CoupeHauteur = CoupeHauteur - maValeur
            TrancheDegagement = TrancheDegagement - maValeur
        End If
        If TrancheDegagement < 0 Or CoupeHauteur < 0 Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 201), vbCritical
            TrancheDegagement = TrancheDegagement - maValeur
            CoupeHauteur = CoupeHauteur - maValeur
        End If
        TestHauteurTranches
    End If
    
    Call CalculsGuillotine
End Sub
Private Sub cmdX_Click(Index As Integer)
    Dim bErr As Boolean
    
    bErr = False
    Select Case Index
        Case 0   'P10
            CoupeLargeur = CoupeLargeur + 10
        Case 1   'P1
            CoupeLargeur = CoupeLargeur + 1
        Case 2   'P0
            CoupeLargeur = CoupeLargeur + 0.1
        Case 3   'M0
            CoupeLargeur = CoupeLargeur - 0.1
        Case 4   'M1
            CoupeLargeur = CoupeLargeur - 1
        Case 5     'M10
            CoupeLargeur = CoupeLargeur - 10
    End Select
    If CoupeLargeur > CourseX - OffsetOrigineX Then
        CoupeLargeur = CourseX - OffsetOrigineX
        bErr = True
    End If
    If CoupeLargeur < -OffsetOrigineX Then
        CoupeLargeur = -OffsetOrigineX
        bErr = True
    End If
    If bErr Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 202), vbCritical
    End If
    Call CalculsGuillotine
End Sub

Private Sub cmdB_Click(Index As Integer)
    Select Case Index
        Case 0  'P1
            beta = beta + PI / 180
        Case 1  'P0
            beta = beta + PI / 1800
        Case 2  'M0
            beta = beta - PI / 1800
        Case 3  'M1
            beta = beta - PI / 180
    End Select
    If beta > PI / 2 - PI / 180 Then beta = beta - PI / 180
    CoupeLargeur = CoupeHauteur * Tan(beta)
  Call CalculsGuillotine
End Sub

Private Sub cmdl_Click(Index As Integer)
    Dim maValeur As Integer
    Select Case Index
        Case 0  'P10
            maValeur = 10
        Case 1  'P1
            maValeur = 1
        Case 2  'M1
            maValeur = -1
        Case 3  'M10
            maValeur = -10
    End Select
    BlocLargeur = BlocLargeur + maValeur
    Call CalculsGuillotine
End Sub

Private Sub cmdOffset_Click(Index As Integer)
    If Index = 0 Then
        BlocOffset = BlocOffset + 1
    Else
        BlocOffset = BlocOffset - 1
    End If
    Call CalculsGuillotine
End Sub

Private Sub CmdRAZ_Click()
    Select Case TypeGuillotine
        Case "Verticale"
            CoupeHauteur = 60
            CoupeLargeur = 5
        Case "Oblique"
            CoupeHauteur = 60
            CoupeLargeur = 5
        Case "Horizontale"
            CoupeHauteur = 40
            CoupeLargeur = 120
            TrancheDegagement = 15
            TrancheNombre = 1
        Case "Tranches"
            CoupeHauteur = 60
            CoupeLargeur = 120
            TrancheNombre = 3
            TrancheEpaisseur = 15
            TrancheDegagement = 15
            Check_Degagement(1).value = 0
    End Select
    Call CalculsGuillotine
End Sub

Private Sub check_Bloc_Click()
    For i = lblDim.LBound To lblDim.UBound
        lblDim(i).Visible = False
    Next i

    Call CalculsGuillotine
End Sub
Private Sub Check_Degagement_Click(Index As Integer)
    Dim i As Integer
    If TypeGuillotine = "Horizontale" Then
        If Check_Degagement(1).value = vbUnchecked Then
            TrancheDegagement = 10
        End If
        For i = cmdD.LBound To cmdD.UBound
            cmdD(i).Enabled = (Check_Degagement(1).value = vbChecked)
        Next i
        TB_Degagement.Enabled = (Check_Degagement(1).value = vbChecked)
    End If
        If TypeGuillotine = "Tranches" Then
        If Check_Degagement(0).value = vbUnchecked Then
            CoupeHauteur = CoupeHauteur - (TrancheDegagement - TrancheEpaisseur)
            TrancheDegagement = TrancheEpaisseur
        End If
        For i = cmdD.LBound To cmdD.UBound
            cmdD(i).Enabled = (Check_Degagement(0).value = vbChecked)
        Next i
        TB_Degagement.Enabled = (Check_Degagement(0).value = vbChecked)
    End If

    Call CalculsGuillotine
End Sub

Private Sub Combo_Mat_Click()
    gstrMat = Combo_Mat.Text
    Call Affiche_Fil
End Sub

Private Sub Form_Activate()
    Cmd_Armement.SetFocus
End Sub

Private Sub Form_Load()
    ChargeTrad Me
    Call Chargt_Materiau(Combo_Mat)

    Set gFenetreHID = frmGuillotine
    Call maClasseHID.TableVerif
    
    lblMemoBloc.Visible = bMemoBloc
    
    IncrementZoom_mm = 150 'le zoom changera tous les 150mm
    FacteurZoom_V = 1
    FacteurZoom_H = 1
    FacteurZoom = 1
     
    CourseX = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_16", 0)
    CourseY = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_17", 0)
    
    TB_DelayArmement = ReadINI_Num(Me.Name & "_DelayArmement", 5)
    TB_DelayDecoupe = ReadINI_Num(Me.Name & "_DelayDecoupe", 5)
    Check_Chauffe.value = ReadINI_Num(Me.Name & "_ChauffeArmement", 0)
    Check_ArmementAuto.value = ReadINI_Num(Me.Name & "_ArmementAuto", 0)
    Check_DecoupeAuto.value = ReadINI_Num(Me.Name & "_DecoupeAuto", 0)
    BlocHauteur = ReadINI_Num(Me.Name & "_BlocHauteur", 50)
    BlocLargeur = ReadINI_Num(Me.Name & "_BlocLargeur", 100)
    BlocOffset = ReadINI_Num(Me.Name & "_BlocOffset", 5)
    
    CoupeHauteur = ReadINI_Num(Me.Name & "_CoupeHauteur", 60)
    CoupeLargeur = ReadINI_Num(Me.Name & "_CoupeLargeur", 120)
    beta = ReadINI_Num(Me.Name & "_Beta", 0)
    TB_VDecoupe = ReadINI_Num(Me.Name & "_VDecoupe", CDbl(ReadINI_Machine_Num("mat_" & gstrMat, "VH", 6)))
    TB_VDecoupe = Round(Min(TB_VDecoupe, Calcul_Fil_VitMax(gstrMat)), 2)
    TrancheEpaisseur = ReadINI_Num(Me.Name & "_Tranchee", 15)
    TrancheNombre = ReadINI_Num(Me.Name & "_Tranchen", 1)
'    Call WriteINI(Me.Name & "_TrancheDegagement", TrancheDegagement)


    OffsetOrigineX = 150
    OffsetOrigineX = 0#
    TrancheDegagement = TrancheEpaisseur  'on initialise le d�gagement � l'�paisseur d'une tranche
    Saignee = 1
    TypeGuillotine = "Verticale"
    Check_Bloc.value = vbChecked
  
    TB_Tranchee.Text = Str(TrancheEpaisseur)  '�criture des valeurs de tranches par d�faut
    TB_Tranchen.Text = Str(TrancheNombre)
    TB_Degagement.Text = Str(TrancheDegagement)
    TB_BlocHauteur.Text = Str(BlocHauteur)
    TB_BlocLargeur.Text = Str(BlocLargeur)
    TB_BlocOffset.Text = Str(BlocOffset)
    
    'placement de la checkbox du d�gagement pour la guillotine horizontale, et mise invisible :
    Check_Degagement(1).Top = 270
    Check_Degagement(1).Visible = False
          
    For i = cmdD.LBound To cmdD.UBound
        'd�sactivation de la saisie du d�gagement
        cmdD(i).Enabled = False
    Next i
    TB_Degagement.Enabled = False
      
    frame_Tranches.Visible = False
    frame_Degagement.Visible = False
  
    lblZoom.Caption = "Zoom : 1/" + Str(FacteurZoom)
            
    lblX.Visible = False
    TB_X.Visible = False
    For i = cmdX.LBound To cmdX.UBound
        cmdX(i).Visible = False
    Next i
    lblB.Visible = False
    txtBeta.Visible = False
    imgB.Visible = False
    For i = cmdB.LBound To cmdB.UBound
        cmdB(i).Visible = False
    Next i
    For i = lblDim.LBound To lblDim.UBound
        lblDim(i).Visible = False
    Next i
    
    Call Placement
  
    Call Option_Vitesse_Click(0)    'Par d�faut : vitesse armement : vitesse max AVEC acc�l�ration
End Sub
Private Sub Placement()
    If gbOrientationInv Then 'placement des textes de X et Y
        TB_Y.Left = pctG.Width - 1780
        TB_YReel.Left = pctG.Width - 1780
        TB_X.Left = pctG.Width * 1 / 3
    Else
        TB_Y.Left = 0
        TB_YReel.Left = 0
        TB_X.Left = pctG.Width * 1 / 2
    End If
    cmdy(0).Left = TB_Y.Left + 930
    cmdy(5).Left = TB_Y.Left + 930
    cmdy(1).Left = TB_Y.Left + 1215
    cmdy(4).Left = TB_Y.Left + 1215
    cmdy(2).Left = TB_Y.Left + 1500
    cmdy(3).Left = TB_Y.Left + 1500
    lblY.Left = TB_Y.Left + 100
    lblYReel.Left = TB_Y.Left + 100
    cmdX(0).Left = TB_X.Left + 930
    cmdX(5).Left = TB_X.Left + 930
    cmdX(1).Left = TB_X.Left + 1215
    cmdX(4).Left = TB_X.Left + 1215
    cmdX(2).Left = TB_X.Left + 1500
    cmdX(3).Left = TB_X.Left + 1500
    lblX.Left = TB_X.Left - lblX.Width - 20
    If gbOrientationInv Then  'placement du texte de b�ta
        txtBeta.Left = pctG.Width / 5
    Else
        txtBeta.Left = pctG.Width / 2
    End If
    cmdB(0).Left = txtBeta.Left + 930
    cmdB(3).Left = txtBeta.Left + 930
    cmdB(1).Left = txtBeta.Left + 1215
    cmdB(2).Left = txtBeta.Left + 1215
    lblB.Left = txtBeta.Left - 250
    imgB.Left = lblB.Left - 200
      
    Call CalculsGuillotine
End Sub
Private Sub CalculsGuillotine()
    Select Case TypeGuillotine
        Case "Verticale"
            ReDim sngTrajet(1 To 3, 1 To 2)
            FacteurZoom = Int(CoupeHauteur / IncrementZoom_mm) + 1
            HauteurZone = TB_X.Top - 250 - 350
            Rapport = HauteurZone / IncrementZoom_mm / FacteurZoom
            sngTrajet(1, 1) = 3800
            sngTrajet(1, 2) = TB_X.Top - 250
            sngTrajet(2, 1) = sngTrajet(1, 1)
            sngTrajet(2, 2) = sngTrajet(1, 2)
            sngTrajet(3, 1) = sngTrajet(2, 1)
            sngTrajet(3, 2) = sngTrajet(2, 2) - CoupeHauteur * Rapport
        Case "Oblique"
            ReDim sngTrajet(1 To 3, 1 To 2)
            HauteurZone = TB_X.Top - 250 - cmdB(3).Top
            LargeurZone = pctG.Width / 3
            FacteurZoom_V = Int(CoupeHauteur / IncrementZoom_mm) + 1
            FacteurZoom_H = Abs(Fix(CoupeLargeur / IncrementZoom_mm)) + 1
            If FacteurZoom_H >= FacteurZoom_V Then
                FacteurZoom = FacteurZoom_H
            Else
                FacteurZoom = FacteurZoom_V
            End If
            If HauteurZone / IncrementZoom_mm / FacteurZoom > LargeurZone / IncrementZoom_mm / FacteurZoom Then
                Rapport = LargeurZone / IncrementZoom_mm / FacteurZoom
            Else
                Rapport = HauteurZone / IncrementZoom_mm / FacteurZoom
            End If
            sngTrajet(1, 1) = 3800
            sngTrajet(1, 2) = TB_X.Top - 250
            sngTrajet(2, 1) = sngTrajet(1, 1)
            sngTrajet(2, 2) = sngTrajet(1, 2)
            If gbOrientationInv Then
                sngTrajet(3, 1) = sngTrajet(1, 1) - CoupeLargeur * Rapport
            Else
                sngTrajet(3, 1) = sngTrajet(1, 1) + CoupeLargeur * Rapport
            End If
    
            sngTrajet(3, 2) = sngTrajet(2, 2) - CoupeHauteur * Rapport
            beta = Atn(CoupeLargeur / CoupeHauteur)
    Case "Horizontale"
        If Check_Degagement(1).value = vbChecked Then ReDim sngTrajet(1 To 5, 1 To 2)
        If Check_Degagement(1).value = vbUnchecked Then ReDim sngTrajet(1 To 3, 1 To 2)
        HauteurZone = TB_X.Top - 250 - cmdB(3).Top
        LargeurZone = pctG.Width / 3
        If Check_Degagement(1).value = vbChecked Then FacteurZoom_V = Int((CoupeHauteur + TrancheDegagement) / IncrementZoom_mm) + 1
        If Check_Degagement(1).value = vbUnchecked Then FacteurZoom_V = Int((CoupeHauteur) / IncrementZoom_mm) + 1
        FacteurZoom_H = Abs(Fix(CoupeLargeur / IncrementZoom_mm)) + 1
        If FacteurZoom_H >= FacteurZoom_V Then
            FacteurZoom = FacteurZoom_H
        Else
            FacteurZoom = FacteurZoom_V
        End If
        If HauteurZone / IncrementZoom_mm / FacteurZoom > LargeurZone / IncrementZoom_mm / FacteurZoom Then
            Rapport = LargeurZone / IncrementZoom_mm / FacteurZoom
        Else
            Rapport = HauteurZone / IncrementZoom_mm / FacteurZoom
        End If
        If gbOrientationInv Then
            sngTrajet(1, 1) = 4500
            'Contr�le du d�passement de l'axe des X � droite sur la figure
            If sngTrajet(1, 1) - CoupeLargeur * Rapport >= TB_Y.Left - 100 Then
                FacteurZoom = FacteurZoom + 1
            End If
            If HauteurZone / IncrementZoom_mm / FacteurZoom > LargeurZone / IncrementZoom_mm / FacteurZoom Then
                Rapport = LargeurZone / IncrementZoom_mm / FacteurZoom
            Else
                Rapport = HauteurZone / IncrementZoom_mm / FacteurZoom
            End If
        Else
            sngTrajet(1, 1) = 3000
            'Contr�le du d�passement de l'axe des X � gauche sur la figure
            If sngTrajet(1, 1) + CoupeLargeur * Rapport <= TB_Y.Left + TB_Y.Width + 700 Then
                FacteurZoom = FacteurZoom + 1
            End If
            If HauteurZone / IncrementZoom_mm / FacteurZoom > LargeurZone / IncrementZoom_mm / FacteurZoom Then
                Rapport = LargeurZone / IncrementZoom_mm / FacteurZoom
            Else
                Rapport = HauteurZone / IncrementZoom_mm / FacteurZoom
            End If
        End If
        sngTrajet(1, 2) = TB_X.Top - 250
        sngTrajet(2, 1) = sngTrajet(1, 1)
        If Check_Degagement(1).value = vbChecked Then sngTrajet(2, 2) = sngTrajet(1, 2) - (CoupeHauteur + TrancheDegagement) * Rapport
        If Check_Degagement(1).value = vbUnchecked Then sngTrajet(2, 2) = sngTrajet(1, 2) - (CoupeHauteur) * Rapport
        If gbOrientationInv Then
        sngTrajet(3, 1) = sngTrajet(1, 1) - CoupeLargeur * Rapport
        Else
        sngTrajet(3, 1) = sngTrajet(1, 1) + CoupeLargeur * Rapport
        End If
        sngTrajet(3, 2) = sngTrajet(2, 2)
        If Check_Degagement(1).value = vbChecked Then
            sngTrajet(4, 1) = sngTrajet(3, 1)
            sngTrajet(4, 2) = sngTrajet(1, 2) - CoupeHauteur * Rapport
            sngTrajet(5, 1) = sngTrajet(2, 1)
            sngTrajet(5, 2) = sngTrajet(4, 2)
        End If
    Case "Tranches"
        If TrancheNombre = 1 Then
            ReDim sngTrajet(1 To 5, 1 To 2)
        Else
            ReDim sngTrajet(1 To (1 + (TrancheNombre - 1) * 4), 1 To 2)
        End If
        HauteurZone = TB_X.Top - 250 - cmdB(3).Top
        LargeurZone = pctG.Width / 3
        
        FacteurZoom_V = Int(CoupeHauteur / IncrementZoom_mm) + 1
        FacteurZoom_H = Abs(Fix(CoupeLargeur / IncrementZoom_mm)) + 1
        
        If FacteurZoom_H >= FacteurZoom_V Then
            FacteurZoom = FacteurZoom_H
        Else
            FacteurZoom = FacteurZoom_V
        End If
        If HauteurZone / IncrementZoom_mm / FacteurZoom > LargeurZone / IncrementZoom_mm / FacteurZoom Then
            Rapport = LargeurZone / IncrementZoom_mm / FacteurZoom
        Else
            Rapport = HauteurZone / IncrementZoom_mm / FacteurZoom
        End If
        If gbOrientationInv Then
            sngTrajet(1, 1) = 4500
            'Contr�le du d�passement de l'axe des X � droite sur la figure
            If sngTrajet(1, 1) - CoupeLargeur * Rapport >= TB_Y.Left - 100 Then
                FacteurZoom = FacteurZoom + 1
            End If
            If HauteurZone / IncrementZoom_mm / FacteurZoom > LargeurZone / IncrementZoom_mm / FacteurZoom Then
                Rapport = LargeurZone / IncrementZoom_mm / FacteurZoom
            Else
                Rapport = HauteurZone / IncrementZoom_mm / FacteurZoom
            End If
        Else
            sngTrajet(1, 1) = 3000
            'Contr�le du d�passement de l'axe des X � gauche sur la figure
            If sngTrajet(1, 1) + CoupeLargeur * Rapport <= TB_Y.Left + TB_Y.Width + 700 Then
                FacteurZoom = FacteurZoom + 1
            End If
            If HauteurZone / IncrementZoom_mm / FacteurZoom > LargeurZone / IncrementZoom_mm / FacteurZoom Then
                Rapport = LargeurZone / IncrementZoom_mm / FacteurZoom
            Else
                Rapport = HauteurZone / IncrementZoom_mm / FacteurZoom
            End If
        End If
        sngTrajet(1, 2) = TB_X.Top - 250
        sngTrajet(2, 1) = sngTrajet(1, 1)
        sngTrajet(2, 2) = sngTrajet(1, 2) - CoupeHauteur * Rapport
        If gbOrientationInv Then
            sngTrajet(3, 1) = sngTrajet(1, 1) - CoupeLargeur * Rapport
        Else
            sngTrajet(3, 1) = sngTrajet(1, 1) + CoupeLargeur * Rapport
        End If    ' � ce stade, les deux premiers traits sont trac�s
        sngTrajet(3, 2) = sngTrajet(2, 2)
        sngTrajet(4, 1) = sngTrajet(3, 1)
        If Check_Degagement(0).value = 0 Then
            sngTrajet(4, 2) = sngTrajet(3, 2) + (TrancheEpaisseur + Saignee) * Rapport
        End If
        If Check_Degagement(0).value = 1 Then
            sngTrajet(4, 2) = sngTrajet(3, 2) + (TrancheDegagement + Saignee) * Rapport
        End If
        sngTrajet(5, 1) = sngTrajet(1, 1)
        sngTrajet(5, 2) = sngTrajet(4, 2) 'la premi�re tranche est boucl�e
    
        If TrancheNombre > 1 Then  'on regarde s'il y en a d'autres
            For i = 6 To (5 + 2 * (TrancheNombre - 1)) Step 4
                sngTrajet(i, 1) = sngTrajet(1, 1)
                sngTrajet(i, 2) = sngTrajet(i - 1, 2) + (TrancheEpaisseur + Saignee) * Rapport
                sngTrajet(i + 1, 1) = sngTrajet(3, 1)
                sngTrajet(i + 1, 2) = sngTrajet(i, 2)
                sngTrajet(i + 2, 1) = sngTrajet(3, 1)
                sngTrajet(i + 2, 2) = sngTrajet(i, 2) + (TrancheEpaisseur + Saignee) * Rapport
                sngTrajet(i + 3, 1) = sngTrajet(1, 1)
                sngTrajet(i + 3, 2) = sngTrajet(i + 2, 2)
            Next i
        End If
    End Select
    'calcul des dimensions du bloc
    A = BlocHauteur * Rapport
    B = BlocLargeur * Rapport
    c = BlocOffset * Rapport
    If CoupeHauteur = 0# Then
        beta = 0
    Else
        beta = Atn(CoupeLargeur / CoupeHauteur)
    End If          'on r�initialise toujours beta au cas o� on change de guillotine

    Call SchemaG
End Sub
Private Sub SchemaG()
    TB_Y.Text = Format(CoupeHauteur, "###0.0") 'on remplit les cases des valeurs
    TB_X.Text = Format(CoupeLargeur, "###0.0")
    txtBeta.Text = Format(beta * 180 / PI, "###0.0")
    TB_Tranchee.Text = Format(TrancheEpaisseur, "###0.0")
    TB_Tranchen.Text = Format(TrancheNombre, "###0")
    TB_Degagement.Text = Format(TrancheDegagement, "###0.0")
    TB_BlocHauteur.Text = Format(BlocHauteur, "###0.0")
    TB_BlocLargeur.Text = Format(BlocLargeur, "###0.0")
    TB_BlocOffset.Text = Format(BlocOffset, "###0.0")
    If Dim_Haut_2 = 0 Then lblDim(1).Visible = False
    
    pctG.Cls
    pctG.DrawStyle = vbSolid  ' lignes pleines
    pctG.DrawWidth = 1        ' �paisseur 1 pixel
    pctG.ForeColor = vbBlack   'couleur
    
    If Check_Bloc.value = vbChecked Then
        For i = lblDim.LBound To lblDim.UBound
            lblDim(i).Visible = False
        Next i
        Call AfficheBloc
    End If
    If gbOrientationInv Then
        pctG.Line (TB_Y.Left - 100, TB_X.Top - 250)-(200, TB_X.Top - 250)  'axe X
        pctG.Line -Step(150, 70) 'fl�che X
        pctG.Line -Step(0, -140)
        pctG.Line -Step(-150, 70)
        pctG.Line (TB_Y.Left - 100, TB_X.Top - 250)-(TB_Y.Left - 100, 200)  'axe Y
        pctG.Line -Step(70, 150) 'fl�che Y
        pctG.Line -Step(-140, 0)
        pctG.Line -Step(70, -150)
    Else
        pctG.Line (TB_Y.Left + TB_Y.Width + 920, TB_X.Top - 250)-(pctG.Width - 200, TB_X.Top - 250) 'axe X
        pctG.Line -Step(-150, 70) 'fl�che X
        pctG.Line -Step(0, -140)
        pctG.Line -Step(150, 70)
        pctG.Line (TB_Y.Left + TB_Y.Width + 920, TB_X.Top - 250)-(TB_Y.Left + TB_Y.Width + 920, 250)  'axe Y
        pctG.Line -Step(70, 150) 'fl�che Y
        pctG.Line -Step(-140, 0)
        pctG.Line -Step(70, -150)
    End If
    Select Case TypeGuillotine
    Case "Verticale"
        pctG.DrawWidth = 3
        pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2) - 50)-(sngTrajet(3, 1), sngTrajet(3, 2) - 50), vbRed 'le -50
        pctG.DrawWidth = 1                                                  'remonte le cercle de chauffe
        pctG.FillStyle = 0
        pctG.FillColor = RGB(255, 152, 17)
        pctG.Circle (sngTrajet(2, 1) - 5, sngTrajet(2, 2) - 50), 54, vbRed 'le -5 sert � axer les cercles
        pctG.FillColor = RGB(0, 207, 11)
        pctG.Circle (sngTrajet(3, 1) - 5, sngTrajet(3, 2) - 50), 54, RGB(0, 112, 6) 'le -5 sert � axer les cercles
        pctG.DrawStyle = vbDot  'lignes pointill�es Y
        If gbOrientationInv Then
            pctG.Line (TB_Y.Left - 100, TB_X.Top - 250)-(TB_Y.Left + 10, TB_X.Top - 250) 'dessous Y
            pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2) - 50)-(TB_Y.Left, sngTrajet(3, 2) - 50) ' fil
        Else
            pctG.Line (TB_Y.Left + TB_Y.Width + 920, TB_X.Top - 250)-(TB_Y.Left + TB_Y.Width + 800, TB_X.Top - 250)  'dessous Y
            pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2) - 50)-(TB_Y.Left + TB_Y.Width + 800, sngTrajet(3, 2) - 50) ' fil
        End If
        pctG.DrawStyle = vbSolid
    Case "Oblique"
        'angle devant b�ta
        If gbOrientationInv Then
            pctG.Line (cmdB(2).Left + cmdB(2).Width + 600, 600)-(cmdB(2).Left + cmdB(2).Width + 600, 100)
            pctG.Line (cmdB(2).Left + cmdB(2).Width + 600, 600)-(cmdB(2).Left + cmdB(2).Width + 600 - 500 * Sin(beta), 600 - 500 * Cos(beta))
            If beta > 0 Then
                pctG.Circle (cmdB(2).Left + cmdB(2).Width + 600, 600), 400, vbMagenta, (PI / 2), (PI / 2 + beta)
            End If
            If beta < 0 Then
                pctG.Circle (cmdB(2).Left + cmdB(2).Width + 600, 600), 400, vbMagenta, (PI / 2 + beta), (PI / 2)
            End If
        Else
            pctG.Line (cmdB(2).Left + cmdB(2).Width + 600, 600)-(cmdB(2).Left + cmdB(2).Width + 600, 100)
            pctG.Line (cmdB(2).Left + cmdB(2).Width + 600, 600)-(cmdB(2).Left + cmdB(2).Width + 600 + 500 * Sin(beta), 600 - 500 * Cos(beta))
            If beta > 0 Then
                pctG.Circle (cmdB(2).Left + cmdB(2).Width + 600, 600), 400, vbMagenta, (PI / 2 - beta), (PI / 2)
            End If
            If beta < 0 Then
                pctG.Circle (cmdB(2).Left + cmdB(2).Width + 600, 600), 400, vbMagenta, (PI / 2), (PI / 2 - beta)
            End If
        End If
        pctG.DrawWidth = 3
        pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2) - 50)-(sngTrajet(3, 1), sngTrajet(3, 2) - 50), vbRed 'le -50
        pctG.DrawWidth = 1                                                          'remonte le cercle de chauffe
        pctG.FillStyle = 0
        pctG.FillColor = RGB(255, 152, 17)
        pctG.Circle (sngTrajet(2, 1) - 5, sngTrajet(2, 2) - 50), 54, vbRed 'le -5 sert � axer les cercles
        pctG.FillColor = RGB(0, 207, 11)
        pctG.Circle (sngTrajet(3, 1) - 5, sngTrajet(3, 2) - 50), 54, RGB(0, 112, 6) 'le -5 sert � axer les cercles
        pctG.DrawStyle = vbDot  'lignes pointill�es Y
        If gbOrientationInv Then
            pctG.Line (TB_Y.Left - 100, TB_X.Top - 250)-(TB_Y.Left + 10, TB_X.Top - 250) 'dessous Y
            pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2) - 50)-(TB_Y.Left, sngTrajet(3, 2) - 50) ' fil
        Else
            pctG.Line (TB_Y.Left + TB_Y.Width + 920, TB_X.Top - 250)-(TB_Y.Left + TB_Y.Width + 800, TB_X.Top - 250)  'dessous Y
            pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2))-(TB_Y.Left + TB_Y.Width + 800, sngTrajet(3, 2)) ' fil
        End If
        ' lignes pointill�es X
        pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2))-(sngTrajet(2, 1), sngTrajet(2, 2) + 100) 'd�part fil
        pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2))-(sngTrajet(3, 1), sngTrajet(2, 2) + 100) 'arriv�e fil
        pctG.DrawStyle = vbSolid
    Case "Horizontale"
        pctG.DrawWidth = 1
        pctG.Line (sngTrajet(1, 1), sngTrajet(1, 2))-(sngTrajet(2, 1), sngTrajet(2, 2)), vbRed
        If Check_Degagement(1).value = vbUnchecked Then
            pctG.DrawWidth = 3
            pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2))-(sngTrajet(3, 1), sngTrajet(3, 2)), vbRed
            pctG.DrawWidth = 1
            pctG.FillStyle = 0
            pctG.FillColor = RGB(255, 152, 17)
            pctG.Circle (sngTrajet(2, 1), sngTrajet(2, 2) + 4), 54, vbRed 'le + 4 sert � axer les cercles
            pctG.FillColor = RGB(0, 207, 11)
            pctG.Circle (sngTrajet(3, 1), sngTrajet(3, 2) + 4), 54, RGB(0, 112, 6) 'le + 4 sert � axer les cercles
            pctG.DrawStyle = vbDot  'lignes pointill�es Y
        Else
            pctG.DrawWidth = 1
            pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2))-(sngTrajet(3, 1), sngTrajet(3, 2)), vbRed
            pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2))-(sngTrajet(4, 1), sngTrajet(4, 2)), vbRed
            pctG.DrawWidth = 3
            pctG.Line (sngTrajet(4, 1), sngTrajet(4, 2))-(sngTrajet(5, 1), sngTrajet(5, 2)), vbRed
            pctG.DrawWidth = 1
            pctG.FillStyle = 0
            pctG.FillColor = RGB(0, 207, 11)
            pctG.Circle (sngTrajet(4, 1), sngTrajet(4, 2) + 4), 54, RGB(0, 112, 6) 'le + 4 sert � axer les cercles
            pctG.FillColor = RGB(255, 152, 17)
            pctG.Circle (sngTrajet(5, 1), sngTrajet(5, 2) + 4), 54, vbRed 'le + 4 sert � axer les cercles
            pctG.DrawStyle = vbDot  'lignes pointill�es Y
        End If
        If gbOrientationInv Then
            pctG.Line (TB_Y.Left - 100, TB_X.Top - 250)-(TB_Y.Left + 10, TB_X.Top - 250) 'dessous Y
            If Check_Degagement(1).value = vbUnchecked Then
                pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2))-(TB_Y.Left, sngTrajet(2, 2)) ' fil
            End If
            If Check_Degagement(1).value = vbChecked Then
                pctG.Line (sngTrajet(5, 1), sngTrajet(5, 2))-(TB_Y.Left, sngTrajet(5, 2)) ' fil
            End If
        Else
            pctG.Line (TB_Y.Left + TB_Y.Width + 920, TB_X.Top - 250)-(TB_Y.Left + TB_Y.Width + 800, TB_X.Top - 250)  'dessous Y
            If Check_Degagement(1).value = vbUnchecked Then
                pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2))-(TB_Y.Left + TB_Y.Width + 800, sngTrajet(2, 2)) ' fil
            End If
            If Check_Degagement(1).value = vbChecked Then
                pctG.Line (sngTrajet(5, 1), sngTrajet(5, 2))-(TB_Y.Left + TB_Y.Width + 800, sngTrajet(5, 2)) ' fil
            End If
        End If
        ' lignes pointill�es X
        pctG.Line (sngTrajet(1, 1), sngTrajet(1, 2))-(sngTrajet(1, 1), sngTrajet(1, 2) + 100) 'd�part fil
        pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2))-(sngTrajet(3, 1), sngTrajet(1, 2) + 100) 'arriv�e fil
        pctG.DrawStyle = vbSolid
    Case "Tranches"
        pctG.DrawWidth = 1
        'Trajet du fil
        pctG.Line (sngTrajet(1, 1), sngTrajet(1, 2))-(sngTrajet(2, 1), sngTrajet(2, 2)), vbRed
        pctG.DrawWidth = 2
        pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2))-(sngTrajet(3, 1), sngTrajet(3, 2)), vbRed
        pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2))-(sngTrajet(4, 1), sngTrajet(4, 2)), vbRed
        pctG.Line (sngTrajet(4, 1), sngTrajet(4, 2))-(sngTrajet(5, 1), sngTrajet(5, 2)), vbRed
        pctG.DrawWidth = 1
        pctG.FillStyle = 0
        pctG.FillColor = RGB(0, 207, 11)
        pctG.Circle (sngTrajet(2, 1), sngTrajet(2, 2) + 4), 40, RGB(0, 112, 6) 'le + 4 sert � axer les cercles
        pctG.FillColor = RGB(255, 152, 17)
        If TrancheNombre = 1 Then
            pctG.Circle (sngTrajet(5, 1), sngTrajet(5, 2) + 4), 40, vbRed 'le + 4 sert � axer les cercles
        End If
        'l� on a trac� une tranche, on regarde s'il y en a d'autres
        If TrancheNombre > 1 Then
            pctG.DrawWidth = 2
            For i = 6 To (5 + 2 * (TrancheNombre - 1)) Step 4
                pctG.Line (sngTrajet(i - 1, 1), sngTrajet(i - 1, 2))-(sngTrajet(i, 1), sngTrajet(i, 2)), vbRed
                pctG.Line (sngTrajet(i, 1), sngTrajet(i, 2))-(sngTrajet(i + 1, 1), sngTrajet(i + 1, 2)), vbRed
                pctG.Line (sngTrajet(i + 1, 1), sngTrajet(i + 1, 2))-(sngTrajet(i + 2, 1), sngTrajet(i + 2, 2)), vbRed
                pctG.Line (sngTrajet(i + 2, 1), sngTrajet(i + 2, 2))-(sngTrajet(i + 3, 1), sngTrajet(i + 3, 2)), vbRed
            Next i
            pctG.DrawWidth = 1
            pctG.Circle (sngTrajet(1, 1), sngTrajet(5 + 2 * (TrancheNombre - 1), 2) + 4), 40, vbRed 'le + 4 sert � axer les cercles
        End If
        pctG.DrawStyle = vbDot  'lignes pointill�es Y
        If gbOrientationInv Then
            pctG.Line (TB_Y.Left - 100, TB_X.Top - 250)-(TB_Y.Left + 10, TB_X.Top - 250) 'dessous Y
            pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2))-(TB_Y.Left, sngTrajet(2, 2)) ' fil
        Else
            pctG.Line (TB_Y.Left + TB_Y.Width + 920, TB_X.Top - 250)-(TB_Y.Left + TB_Y.Width + 800, TB_X.Top - 250)  'dessous Y
            pctG.Line (sngTrajet(2, 1), sngTrajet(2, 2))-(TB_Y.Left + TB_Y.Width + 800, sngTrajet(2, 2)) ' fil
        End If
        ' lignes pointill�es X
        pctG.Line (sngTrajet(1, 1), sngTrajet(1, 2))-(sngTrajet(1, 1), sngTrajet(1, 2) + 100) 'd�part fil
        pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2))-(sngTrajet(3, 1), sngTrajet(1, 2) + 100) 'arriv�e fil
        pctG.DrawStyle = vbSolid
    End Select
    lblZoom.Caption = "Zoom : 1/" + Str(FacteurZoom)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    gPause = True
    If DeplEnCours Then Beep
    Cancel = DeplEnCours
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If maClasseHID.IsAvailable Then
        maClasseHID.EnvoiUSB    'RAZ du buffer
    End If
    frmMain.Timer_Refresh.Enabled = True
    'M�morisation des valeurs param�tr�es
    Call WriteINI(Me.Name & "_DelayArmement", TB_DelayArmement)
    Call WriteINI(Me.Name & "_DelayDecoupe", TB_DelayDecoupe)
    Call WriteINI(Me.Name & "_ChauffeArmement", IIf(Check_Chauffe, 1, 0))
    Call WriteINI(Me.Name & "_ArmementAuto", IIf(Check_ArmementAuto, 1, 0))
    Call WriteINI(Me.Name & "_DecoupeAuto", IIf(Check_DecoupeAuto, 1, 0))
    Call WriteINI(Me.Name & "_BlocHauteur", Str(BlocHauteur))
    Call WriteINI(Me.Name & "_BlocLargeur", Str(BlocLargeur))
    Call WriteINI(Me.Name & "_BlocOffset", Str(BlocOffset))
    Call WriteINI(Me.Name & "_CoupeHauteur", Str(CoupeHauteur))
    Call WriteINI(Me.Name & "_CoupeLargeur", Str(CoupeLargeur))
    Call WriteINI(Me.Name & "_Beta", Str(beta))
    Call WriteINI(Me.Name & "_VDecoupe", TB_VDecoupe)
    Call WriteINI(Me.Name & "_Tranchee", Str(TrancheEpaisseur))
    Call WriteINI(Me.Name & "_Tranchen", Str(TrancheNombre))
    Call WriteINI(Me.Name & "_TrancheDegagement", Str(TrancheDegagement))
End Sub

Private Sub lblMemoBloc_Click()
    'Affichage des dimensions du bloc
    Dim Machaine As String
    
    Machaine = ReadINI_Trad(gLangue, Me.Name, "221") & ":" & gwLargeurEmplanture & " mm" & vbCrLf
    Machaine = Machaine & ReadINI_Trad(gLangue, Me.Name, "222") & ":" & gwLargeurSaumon & " mm" & vbCrLf
    Machaine = Machaine & ReadINI_Trad(gLangue, Me.Name, "223") & ":" & gwFlecheBA & " mm" & vbCrLf
    Machaine = Machaine & ReadINI_Trad(gLangue, Me.Name, "224") & ":" & gwFlecheBF & " mm" & vbCrLf
    Machaine = Machaine & ReadINI_Trad(gLangue, Me.Name, "225") & ":" & gwEpaisseur & " mm" & vbCrLf
    Machaine = Machaine & ReadINI_Trad(gLangue, Me.Name, "226") & ":" & gwLongueur & " mm"
    MsgBox Machaine, vbInformation, ReadINI_Trad(gLangue, Me.Name, "220")
End Sub

Private Sub Option_Vitesse_Click(Index As Integer)
    If Index = 0 Then 'Avec acc�l�ration
        TB_VArmement = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 1)
    Else
        TB_VArmement = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_13", 1)
    End If
End Sub

Private Sub Option_Direction_Click(Index As Integer)
    For i = cmdB.LBound To cmdB.UBound
        cmdB(i).Visible = False
    Next i
    For i = cmdX.LBound To cmdX.UBound
        cmdX(i).Visible = True
    Next i
    lblX.Visible = True
    TB_X.Visible = True
    lblB.Visible = False
    txtBeta.Visible = False
    imgB.Visible = False
    lblYReel.Visible = False
    TB_YReel.Visible = False
    frame_Degagement.Visible = False

    Select Case Index
        Case 0
'            CoupeLargeur = 0
'            beta = 0
            TypeGuillotine = "Verticale"
            lblDim(0).Visible = False
            lblDim(1).Visible = False
            lblDim(2).Visible = False
            lblX.Visible = False
            TB_X.Visible = False
            For i = cmdX.LBound To cmdX.UBound
                cmdX(i).Visible = False
            Next i
            frame_Tranches.Visible = False
        Case 1
            TypeGuillotine = "Oblique"
            lblDim(0).Visible = False
            lblDim(1).Visible = False
            lblDim(2).Visible = False
            lblB.Visible = True
            txtBeta.Visible = True
            imgB.Visible = True
            For i = cmdB.LBound To cmdB.UBound
                cmdB(i).Visible = True
            Next i
            frame_Tranches.Visible = False
        Case 2
            lblDim(0).Visible = False
            lblDim(1).Visible = False
            lblDim(2).Visible = False
            Check_Degagement(0).Visible = False
            Check_Degagement(1).Visible = True
            TypeGuillotine = "Horizontale"
            frame_Tranches.Visible = False
            frame_Degagement.Visible = True
            lblYReel.Visible = True
            TB_YReel.Visible = True
        Case 3
            lblDim(0).Visible = False
            lblDim(1).Visible = False
            lblDim(2).Visible = False
            TypeGuillotine = "Tranches"
            Check_Degagement(1).Visible = False
            Check_Degagement(0).Visible = True
            frame_Tranches.Visible = True
            frame_Degagement.Visible = True
    End Select
'    CmdRAZ_Click
    Call CalculsGuillotine
End Sub
Private Sub TestHauteurTranches()
    If Check_Degagement(0).value = vbUnchecked Then
        If TrancheNombre * (TrancheEpaisseur + Saignee) + 0.5 * Saignee > CoupeHauteur Then
            CoupeHauteur = TrancheNombre * (TrancheEpaisseur + Saignee) + 0.5 * Saignee
            MsgBox ReadINI_Trad(gLangue, "erreurs", 203), vbCritical
        End If
    Else
        If TrancheDegagement + (TrancheNombre - 1) * TrancheEpaisseur + TrancheNombre * Saignee + 0.5 * Saignee > CoupeHauteur Then
            CoupeHauteur = TrancheDegagement + (TrancheNombre - 1) * TrancheEpaisseur + TrancheNombre * Saignee + 0.5 * Saignee
            MsgBox ReadINI_Trad(gLangue, "erreurs", 203), vbCritical
        End If
    End If
End Sub
Private Sub AfficheBloc()
    'Calcul des cotes :
    Select Case TypeGuillotine
        'Pour les guillotines Verticale et Oblique, on affiche juste la distance entre le haut de la trajectoire
        'du fil et le dessus du bloc, sans tenir compte du rayonnement
    Case "Verticale", "Oblique"
        Dim_Haut = Abs(CoupeHauteur - BlocHauteur - Saignee / 2) 'la guillotine doit d�passer du bloc
        'Pour la guillotine Horizontale, on affiche la distance entre le haut du bloc et le fil en tenant
        'compte de la saign�e
    Case "Horizontale"
        Dim_Haut = Abs(BlocHauteur - CoupeHauteur - Saignee / 2)
        'Pour la guillotine Tranche, on distingue le cas o� le fil passe au-dessus du bloc ou dans le bloc
        ' et on tient compte de la saign�e
    Case "Tranches"
        If BlocHauteur > CoupeHauteur + Saignee / 2 Then
            Dim_Haut = Abs(BlocHauteur - CoupeHauteur - Saignee / 2)
            Dim_Haut_2 = 0 'on met la valeur � 0 pour qu'elle ne s'affiche pas
        End If
        If BlocHauteur < CoupeHauteur - Saignee / 2 Then
            Dim_Haut = Abs(CoupeHauteur - Saignee / 2 - BlocHauteur)
            Dim_Haut_2 = Abs(BlocHauteur - (CoupeHauteur - TrancheDegagement - Saignee / 2))
        End If
        If BlocHauteur < CoupeHauteur + Saignee / 2 And BlocHauteur > CoupeHauteur - Saignee / 2 Then
            Dim_Haut = 0
            Dim_Haut_2 = 0
        End If
        If Check_Degagement(0).value = 0 Then
            Dim_Bas = CoupeHauteur - TrancheNombre * TrancheEpaisseur - (TrancheNombre + 0.5) * Saignee
        Else
            Dim_Bas = CoupeHauteur - (TrancheNombre - 1) * TrancheEpaisseur - TrancheDegagement - (TrancheNombre + 0.5) * Saignee
        End If

    End Select
    'Affichage du bloc et de la cote communes � toutes les guillotines
    If gbOrientationInv Then  'orientation de la table avec origine � droite
        'trac� du bloc
        pctG.Line (sngTrajet(1, 1) - c, sngTrajet(1, 2))-Step(0, -A), vbBlue
        pctG.Line -Step(-B, 0), vbBlue
        pctG.Line -Step(0, A), vbBlue
        'affichage de la dimension entre le point le plus haut de la guillotine et le dessus du bloc
        pctG.DrawStyle = vbDot
        If TypeGuillotine = "Horizontale" And Check_Degagement(1).value = 1 Then 'cas de la guillotine hor. avec d�gagement
            pctG.Line (sngTrajet(4, 1), sngTrajet(4, 2))-Step(-300, 0), vbBlue
        Else
            pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2))-Step(-300, 0), vbBlue
        End If
        pctG.Line (sngTrajet(1, 1) - c - B, sngTrajet(1, 2) - A)-(sngTrajet(3, 1) - 300, sngTrajet(1, 2) - A), vbBlue
        pctG.DrawStyle = vbSolid
        If TypeGuillotine = "Horizontale" And Check_Degagement(1).value = 1 Then
            pctG.Line (sngTrajet(4, 1) - 255, sngTrajet(4, 2))-(sngTrajet(4, 1) - 255, sngTrajet(1, 2) - A), vbBlue
        Else
            pctG.Line (sngTrajet(3, 1) - 255, sngTrajet(3, 2))-(sngTrajet(3, 1) - 255, sngTrajet(1, 2) - A), vbBlue
        End If
        'Le sens des fl�ches d�pend du type de guillotine
        Select Case TypeGuillotine
            Case "Verticale", "Oblique"
            Call Fleche(sngTrajet(3, 1) - 255, sngTrajet(3, 2), "H")
            Call Fleche(sngTrajet(3, 1) - 255, sngTrajet(1, 2) - A, "B")
            Case "Horizontale"
            If Check_Degagement(1).value = 1 Then
                Call Fleche(sngTrajet(4, 1) - 255, sngTrajet(4, 2), "B")
                Call Fleche(sngTrajet(4, 1) - 255, sngTrajet(1, 2) - A, "H")
            Else
                Call Fleche(sngTrajet(3, 1) - 255, sngTrajet(3, 2), "B")
                Call Fleche(sngTrajet(3, 1) - 255, sngTrajet(1, 2) - A, "H")
            End If
            Case "Tranches"
            Call Fleche(sngTrajet(3, 1) - 255, sngTrajet(3, 2), "B")
            Call Fleche(sngTrajet(3, 1) - 255, sngTrajet(1, 2) - A, "H")
        End Select
    Else        'orientation de la table avec origine � gauche
        'trac� du bloc
        pctG.Line (sngTrajet(1, 1) + c, sngTrajet(1, 2))-Step(0, -A), vbBlue
        pctG.Line -Step(B, 0), vbBlue
        pctG.Line -Step(0, A), vbBlue
        'affichage de la dimension entre le point le plus haut de la guillotine et le dessus du bloc
        pctG.DrawStyle = vbDot
        pctG.Line (sngTrajet(3, 1), sngTrajet(3, 2))-Step(300, 0), vbBlue
        pctG.Line (sngTrajet(1, 1) + c + B, sngTrajet(1, 2) - A)-(sngTrajet(3, 1) + 300, sngTrajet(1, 2) - A), vbBlue
        pctG.DrawStyle = vbSolid
        pctG.Line (sngTrajet(3, 1) + 255, sngTrajet(3, 2))-(sngTrajet(3, 1) + 255, sngTrajet(1, 2) - A), vbBlue
        'Le sens des fl�ches d�pend du type de guillotine
        Select Case TypeGuillotine
            Case "Verticale", "Oblique"
            Call Fleche(sngTrajet(3, 1) + 255, sngTrajet(3, 2), "H")
            Call Fleche(sngTrajet(3, 1) + 255, sngTrajet(1, 2) - A, "B")
            Case "Horizontale", "Tranches"
            Call Fleche(sngTrajet(3, 1) + 255, sngTrajet(3, 2), "B")
            Call Fleche(sngTrajet(3, 1) + 255, sngTrajet(1, 2) - A, "H")
        End Select
    End If
  
    'Affichage des dimensions suppl�mentaires
    If TypeGuillotine = "Tranches" Then
        n = 5 + 2 * (TrancheNombre - 1) - 1 'num�ro de l'avant dernier point du trajet
        'Affichage de la peau inf�rieure
        If gbOrientationInv Then  'Dimension du bas pour axe des X � droite
            pctG.DrawStyle = vbDot
            pctG.Line (sngTrajet(n, 1) - 30, sngTrajet(n, 2))-Step(-300, 0), vbBlue  'trait horizontal
            pctG.DrawStyle = vbSolid
            pctG.Line (sngTrajet(n, 1) - 255, sngTrajet(n, 2))-(sngTrajet(n, 1) - 255, sngTrajet(1, 2)), vbBlue 'cote
            'pointes fl�ches
            Call Fleche(sngTrajet(n, 1) - 255, sngTrajet(1, 2), "B")
            Call Fleche(sngTrajet(n, 1) - 255, sngTrajet(n, 2), "H")
            lblDim(2).Alignment = 1 'alignement � droite
            lblDim(2).Left = sngTrajet(n, 1) - 255 - lblDim(2).Width - 100
            lblDim(2).Top = (sngTrajet(1, 2) + sngTrajet(n, 2)) / 2 - 170
            lblDim(2).Visible = True
        Else    'Dimension du bas pour axe des X � gauche
            pctG.Line (sngTrajet(n, 1) + 30, sngTrajet(n, 2))-Step(300, 0), vbBlue  'trait horizontal
            pctG.Line (sngTrajet(n, 1) + 255, sngTrajet(n, 2))-(sngTrajet(n, 1) + 255, sngTrajet(1, 2)), vbBlue 'cote
            'pointes fl�ches
            Call Fleche(sngTrajet(n, 1) + 255, sngTrajet(1, 2), "B")
            Call Fleche(sngTrajet(n, 1) + 255, sngTrajet(n, 2), "H")
            lblDim(2).Alignment = 0 'alignement � gauche
            lblDim(2).Left = sngTrajet(n, 1) + 255 + 100
            lblDim(2).Top = (sngTrajet(1, 2) + sngTrajet(n, 2)) / 2 - 170
            lblDim(2).Visible = True
        End If
        lblDim(2).Caption = Format(Dim_Bas, "###0.0") + " mm"
        'Affichage de la peau sup�rieure dans le cas d'une guillotine avec d�gagement
        If BlocHauteur < CoupeHauteur - Saignee / 2 Then
            If gbOrientationInv Then  'Dimension peau pour origine � droite
                pctG.DrawStyle = vbDot
                pctG.Line (sngTrajet(4, 1) - 30, sngTrajet(4, 2))-Step(-300, 0), vbBlue  'trait horizontal
                pctG.DrawStyle = vbSolid
                pctG.Line (sngTrajet(4, 1) - 255, sngTrajet(4, 2))-(sngTrajet(4, 1) - 255, sngTrajet(1, 2) - A), vbBlue 'cote
                'pointe fl�che suppl�mentaire
                Call Fleche(sngTrajet(4, 1) - 255, sngTrajet(4, 2), "B")
                lblDim(1).Alignment = 1 'alignement � droite
                lblDim(1).Left = sngTrajet(3, 1) - 255 - lblDim(2).Width - 100
            Else    'Dimension peau pour origine � gauche
                pctG.DrawStyle = vbDot
                pctG.Line (sngTrajet(4, 1) + 30, sngTrajet(4, 2))-Step(300, 0), vbBlue  'trait horizontal
                pctG.DrawStyle = vbSolid
                pctG.Line (sngTrajet(4, 1) + 255, sngTrajet(4, 2))-(sngTrajet(4, 1) + 255, sngTrajet(1, 2) - A), vbBlue 'cote
                'pointe fl�che suppl�mentaire
                Call Fleche(sngTrajet(4, 1) + 255, sngTrajet(4, 2), "B")
                lblDim(1).Alignment = 0 'alignement � gauche
                lblDim(1).Left = sngTrajet(n, 1) + 255 + 100
            End If
            lblDim(1).Top = (sngTrajet(4, 2) + sngTrajet(1, 2) - A) / 2 - 140
            lblDim(1).Visible = True
            lblDim(1).Caption = Format(Dim_Haut_2, "###0.0") + " mm"
        End If
    End If
    'affichage de la mesure
    If TypeGuillotine = "Horizontale" And Check_Degagement(1).value = 1 Then
        lblDim(0).Top = (sngTrajet(4, 2) + sngTrajet(1, 2) - A) / 2 - 170
    Else
        lblDim(0).Top = (sngTrajet(3, 2) + sngTrajet(1, 2) - A) / 2 - 170
    End If
    If gbOrientationInv Then
        lblDim(0).Alignment = 1 'alignement � droite
        lblDim(0).Left = sngTrajet(3, 1) - 255 - lblDim(2).Width - 100
    Else
        lblDim(0).Alignment = 0 'alignement � gauche
        lblDim(0).Left = sngTrajet(3, 1) + 255 + 100
    End If
    lblDim(0).Caption = Format(Dim_Haut, "###0.0") + " mm"
    lblDim(0).Visible = True
End Sub
Private Sub Fleche(ByVal m As Single, ByVal n As Single, ByVal Sens As String)
    Dim y As Integer
    
    y = IIf(Sens = "B", 100, -100)
    pctG.Line (m, n)-Step(-50, y), vbBlue
    pctG.Line -Step(100, 0), vbBlue
    pctG.Line -Step(-50, -y), vbBlue
End Sub

Private Sub TB_DelayArmement_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_DelayDecoupe_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Tranchee_Change()
    If TB_Tranchee.Text = "" Then Exit Sub
    If Not IsNumeric(TB_Tranchee.Text) Then Exit Sub
    TrancheEpaisseur = CDbl(TB_Tranchee.Text)
    Call CalculsGuillotine
End Sub

Private Sub TB_Tranchee_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Tranchen_Change()
    If TB_Tranchen.Text = "" Then Exit Sub
    If Not IsNumeric(TB_Tranchen.Text) Then Exit Sub
    TrancheNombre = CDbl(TB_Tranchen.Text)
    Call CalculsGuillotine
End Sub

Private Sub TB_Tranchen_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_VArmement_Change()
    If TB_VArmement.Text = "" Then Exit Sub
    If Not IsNumeric(TB_VArmement.Text) Then Exit Sub
    If CDbl(TB_VArmement) > ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0) Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 204), vbCritical
        TB_VArmement = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0)
    End If
End Sub

Private Sub TB_VArmement_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_VDecoupe_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_VDecoupe_Change()
    If TB_VDecoupe.Text = "" Then Exit Sub
    If IsNumeric(TB_VDecoupe) Then
        If CDbl(TB_VDecoupe) > ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0) Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 204), vbCritical
            TB_VDecoupe = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0)
        End If
        If CDbl(TB_VDecoupe) > CDbl(ReadINI_Machine_Num("mat_" & gstrMat, "VH", 6)) Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 209), vbCritical
            TB_VDecoupe = ReadINI_Machine_Num("mat_" & gstrMat, "VH", 6)
        End If
        
        Call Affiche_Fil
    End If
End Sub

Private Sub TB_X_Change()
    If TB_X.Text = "" Then Exit Sub
    If Not IsNumeric(TB_X.Text) Then Exit Sub
    CoupeLargeur = CDbl(TB_X.Text)
    Call CalculsGuillotine
End Sub

Private Sub TB_X_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub tb_Y_Change()
    If TB_Y.Text = "" Then Exit Sub
    If Not IsNumeric(TB_Y.Text) Then Exit Sub
    CoupeHauteur = CDbl(TB_Y.Text)
    If CoupeHauteur = 0# Then Exit Sub
    TB_YReel = Format(CDbl(TB_Y.Text) - CDbl(Left(txtSaignee.Text, Len(txtSaignee.Text) - 3)) / 2#, "#0.00")
    Call CalculsGuillotine
End Sub

Private Sub TB_Y_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub Timer_Armement_Timer()
    If gPause Then Exit Sub
    Call Cmd_Armement_Click
    Timer_Armement.Enabled = False
End Sub

Private Sub Timer_Decoupe_Timer()
    If gPause Then Exit Sub
    Call Cmd_Decoupe_Click
    Timer_Decoupe.Enabled = False
End Sub

Private Sub Affiche_Fil()
    Dim chauffe As Double
    Dim CH As Double
    Dim CB As Double
    Dim VH As Double
    Dim VB As Double
    Dim SH1 As Single
    Dim SH2 As Single

    'Affichage de la chauffe en % et de la valeur de la saign�e (rayonnement x 2)
    chauffe = Min(Calcul_Fil(gstrMat, CDbl(TB_VDecoupe), CH, CB, VH, VB, SH1, SH2), 100)
    lblChauffe = Round(chauffe) & " %"
    txtSaignee = SH1 & " mm"
End Sub

Private Sub txtBeta_Change()
    If txtBeta.Text = "" Then Exit Sub
    If Not IsNumeric(txtBeta.Text) Then Exit Sub
    Call CalculsGuillotine
End Sub

Private Sub TB_Degagement_Change()
    If TB_Degagement.Text = "" Then Exit Sub
    If Not IsNumeric(TB_Degagement.Text) Then Exit Sub
    TrancheDegagement = CDbl(TB_Degagement.Text)
    Call CalculsGuillotine
End Sub

Private Sub TB_Degagement_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_BlocHauteur_Change()
    If TB_BlocHauteur.Text = "" Then Exit Sub
    If Not IsNumeric(TB_BlocHauteur.Text) Then Exit Sub
    BlocHauteur = CDbl(TB_BlocHauteur.Text)
    Call CalculsGuillotine
End Sub

Private Sub TB_BlocHauteur_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_BlocLargeur_Change()
    If TB_BlocLargeur.Text = "" Then Exit Sub
    If Not IsNumeric(TB_BlocLargeur.Text) Then Exit Sub
    BlocLargeur = CDbl(TB_BlocLargeur.Text)
    Call CalculsGuillotine
End Sub

Private Sub TB_BlocLargeur_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_BlocOffset_Change()
    If TB_BlocOffset.Text = "" Then Exit Sub
    If Not IsNumeric(TB_BlocOffset.Text) Then Exit Sub
    BlocOffset = CDbl(TB_BlocOffset.Text)
    Call CalculsGuillotine
End Sub

Private Sub TB_BlocOffset_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub
