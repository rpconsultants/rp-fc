#include "stdafx.h"

// Global Variables:
//---------------------------------------
// DIRECT 3D
//---------------------------------------
LPDIRECT3D9             g_pD3D = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE9       g_pd3dDevice = NULL; // Our rendering device
LPDIRECT3DVERTEXBUFFER9 g_pVB = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_TABLE = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_PROFILA = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_PROFILB = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_POLY = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_POLYOUT = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_POLYCUT = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_POLYCUTOUT = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_XG = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_XD = NULL; // Buffer to hold vertices
LPDIRECT3DVERTEXBUFFER9 g_pVB_STRUCT = NULL; // Buffer to hold vertices

struct FIL {
	float XG;
	float YG;
	float XD;
	float YD;
} extern Fil;

struct CUT {
	float XG;
	float YG;
	float XD;
	float YD;
	float Z;
	float TIME;
	int	PWM;
}  extern *Data_Cut;
int extern Data_Cut_Nbr,Poly_Cut_Nbr,AngleX,AngleY,Zoom,MoveX,MoveY;

struct POLY {
	float SizeX;
	float SizeY;
	float SizeZ;
	float PosX;
	float PosY;
	float PosZ;
} extern Poly;

struct {
	char NAME[100];
	float SIZE_X,SIZE_Y,SIZE_Z;
	struct {
		unsigned int PAS_TOUR,PAS_PERDU,STEP,DIR,LINK;
		float MM_TOUR,VIT_SS_ACCEL,VIT_AV_ACCEL;
		BOOL SENS;
		char NAME[10];
	} M[5];
} extern Meca;

RECT rcClient3D;

// A structure for our custom vertex type
struct CUSTOMVERTEX
{
    FLOAT x, y, z;      // The untransformed, 3D position for the vertex
    DWORD color;        // The vertex color
};

// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)

// Forward declarations for 3D:
HRESULT			InitD3D(HWND);
HRESULT			InitGeometry();
VOID			Cleanup();
VOID			SetupMatrices();
VOID			Render();
VOID			SetupLights();
VOID			Cube(float,float,float,float,float,float,DWORD,DWORD,CUSTOMVERTEX *);
VOID			CubeWire(float,float,float,float,float,float,DWORD,CUSTOMVERTEX *);

//-----------------------------------------------------------------------------
// Name: InitD3D()
// Desc: Initializes Direct3D
//-----------------------------------------------------------------------------
HRESULT InitD3D( HWND hWnd )
{
	Cleanup();
	if( g_pd3dDevice != NULL )
	{
		g_pd3dDevice->Release();
        g_pd3dDevice=NULL;
	}

    if( g_pD3D != NULL )
	{
        g_pD3D->Release();
        g_pD3D=NULL;
	}
	GetClientRect(hWnd, &rcClient3D);
	if(rcClient3D.right==0||rcClient3D.bottom==0)
		return E_FAIL;
	// Create the D3D object.
    if( NULL == ( g_pD3D = Direct3DCreate9( D3D_SDK_VERSION ) ) )
        return E_FAIL;

    // Set up the structure used to create the D3DDevice
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof( d3dpp ) );
    d3dpp.Windowed = TRUE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
    d3dpp.EnableAutoDepthStencil = TRUE;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

    // Create the D3DDevice
    if( FAILED( g_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                                      &d3dpp, &g_pd3dDevice ) ) )
    {
        return E_FAIL;
    }

    // Turn off culling, so we see the front and back of the triangle
    g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

    // Turn off D3D lighting, since we are providing our own vertex colors
    g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

	g_pd3dDevice->SetRenderState(D3DRS_ZENABLE,  TRUE ); // Z-Buffer (Depth Buffer)

	g_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
	g_pd3dDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS ); 
	g_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	g_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA); //SRCCOLOR);

	return S_OK;
}


//-----------------------------------------------------------------------------
// Name: InitGeometry()
// Desc: Creates the scene geometry
//-----------------------------------------------------------------------------
HRESULT InitGeometry()
{
    VOID* pVertices;
	CUSTOMVERTEX* profilVertices;
	int i;

	Cleanup();
    //****************************************
	// Table
    //****************************************
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 19*sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_TABLE, NULL ) ) )
        return E_FAIL;
    if( FAILED( g_pVB_TABLE->Lock( 0, 19*sizeof(CUSTOMVERTEX), ( void** )&pVertices, 0 ) ) )
        return E_FAIL;
	Cube(0.0f,0.0f,0.0f,Meca.SIZE_X,-10.0f,Meca.SIZE_Z,0xffdfdfdf,0xff7f3f00,(CUSTOMVERTEX *)pVertices);
    g_pVB_TABLE->Unlock();

    //****************************************
	// Struct
    //****************************************
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 7*sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_STRUCT, NULL ) ) )
        return E_FAIL;
    if( FAILED( g_pVB_STRUCT->Lock( 0, 7*sizeof(CUSTOMVERTEX), ( void** )&profilVertices, 0 ) ) )
        return E_FAIL;
	profilVertices[0].color=0x80FFFFFF;
	profilVertices[0].x=Fil.XG;
	profilVertices[0].y=0.0;
	profilVertices[0].z=0.0f;
	profilVertices[1].color=0x80FFFFFF;
	profilVertices[1].x=Fil.XG;
	profilVertices[1].y=Meca.SIZE_Y;
	profilVertices[1].z=0.0f;
	profilVertices[2].color=0x80FFFFFF;
	profilVertices[2].x=Fil.XD;
	profilVertices[2].y=0.0;
	profilVertices[2].z=Meca.SIZE_Z;
	profilVertices[3].color=0x80FFFFFF;
	profilVertices[3].x=Fil.XD;
	profilVertices[3].y=Meca.SIZE_Y;
	profilVertices[3].z=Meca.SIZE_Z;
	profilVertices[4].color=0xFFFF0000;
	profilVertices[4].x=Fil.XG;
	profilVertices[4].y=Fil.YG;
	profilVertices[4].z=0.0f;
	profilVertices[5].color=0xFFFF0000;
	profilVertices[5].x=Fil.XD;
	profilVertices[5].y=Fil.YD;
	profilVertices[5].z=Meca.SIZE_Z;
    g_pVB_STRUCT->Unlock();

    //****************************************
	// XG
    //****************************************
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 19*sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_XG, NULL ) ) )
        return E_FAIL;
    if( FAILED( g_pVB_XG->Lock( 0, 19*sizeof(CUSTOMVERTEX), ( void** )&pVertices, 0 ) ) )
        return E_FAIL;
	Cube(Fil.XG-15.0f,0.0f,0.0f,30.0f,-15.0f,-30.0f,0xff7f7f7f,0xffffffff,(CUSTOMVERTEX *)pVertices);
    g_pVB_XG->Unlock();

    //****************************************
	// XD
    //****************************************
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 19*sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_XD, NULL ) ) )
        return E_FAIL;
    if( FAILED( g_pVB_XD->Lock( 0, 19*sizeof(CUSTOMVERTEX), ( void** )&pVertices, 0 ) ) )
        return E_FAIL;
	Cube(Fil.XD-15.0f,0.0f,Meca.SIZE_Z,30.0f,-15.0f,30.0f,0xffffffff,0xff7f7f7f,(CUSTOMVERTEX *)pVertices);
    g_pVB_XD->Unlock();

    //****************************************
	// Poly
    //****************************************
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 19*sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_POLY, NULL ) ) )
        return E_FAIL;
    if( FAILED( g_pVB_POLY->Lock( 0, 19*sizeof(CUSTOMVERTEX), ( void** )&pVertices, 0 ) ) )
        return E_FAIL;
	Cube(Poly.PosX,Poly.PosY+0.3f,Poly.PosZ,Poly.SizeX,Poly.SizeY,Poly.SizeZ,0x20ffffff,0x20ffff5f,(CUSTOMVERTEX *)pVertices);
    g_pVB_POLY->Unlock();
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 17*sizeof(CUSTOMVERTEX),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_POLYOUT, NULL ) ) )
        return E_FAIL;
    if( FAILED( g_pVB_POLYOUT->Lock( 0, 17*sizeof(CUSTOMVERTEX), ( void** )&pVertices, 0 ) ) )
        return E_FAIL;
	CubeWire(Poly.PosX,Poly.PosY+0.3f,Poly.PosZ,Poly.SizeX,Poly.SizeY,Poly.SizeZ,0xffffff5f,(CUSTOMVERTEX *)pVertices);
    g_pVB_POLYOUT->Unlock();

    //****************************************
	// Rep�re
    //****************************************
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 21* sizeof( CUSTOMVERTEX ),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB, NULL ) ) )
        return E_FAIL;
    if( FAILED( g_pVB->Lock( 0, 21*sizeof( CUSTOMVERTEX ), ( void** )&profilVertices, 0 ) ) )
        return E_FAIL;
	for (i=0;i!=20;i++)
	{
		profilVertices[i].color=0xffffffff;
		profilVertices[i].x=-50.0f;
		profilVertices[i].y=-50.0f;
		profilVertices[i].z=-50.0f;
	}
	profilVertices[1].x=0.0f;
	profilVertices[3].y=0.0f;
	profilVertices[5].z=0.0f;
	//X
	profilVertices[6].x+=65.0f;
	profilVertices[6].y-=5.0f;
	profilVertices[7].x+=55.0f;
	profilVertices[7].y+=5.0f;
	profilVertices[8].x+=65.0f;
	profilVertices[8].y+=5.0f;
	profilVertices[9].x+=55.0f;
	profilVertices[9].y-=5.0f;
	//Y
	profilVertices[10].x-=5.0f;
	profilVertices[10].y+=65.0f;
	profilVertices[11].x+=5.0f;
	profilVertices[11].y+=55.0f;
	profilVertices[12].x+=5.0f;
	profilVertices[12].y+=65.0f;
	profilVertices[13].y+=60.0f;
	//Z
	profilVertices[14].z+=55.0f;
	profilVertices[14].y+=5.0f;
	profilVertices[15].z+=65.0f;
	profilVertices[15].y+=5.0f;
	profilVertices[16].z+=65.0f;
	profilVertices[16].y+=5.0f;
	profilVertices[17].z+=55.0f;
	profilVertices[17].y-=5.0f;
	profilVertices[18].z+=55.0f;
	profilVertices[18].y-=5.0f;
	profilVertices[19].z+=65.0f;
	profilVertices[19].y-=5.0f;

    g_pVB->Unlock();

    //****************************************
	// Profils
    //****************************************
	if(Data_Cut_Nbr!=0)
	{
		// PROFILA
		if( FAILED( g_pd3dDevice->CreateVertexBuffer( (Data_Cut_Nbr+1) * sizeof( CUSTOMVERTEX ),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_PROFILA, NULL ) ) )
	        return E_FAIL;
		if( FAILED( g_pVB_PROFILA->Lock( 0, (Data_Cut_Nbr+1) * sizeof( CUSTOMVERTEX ), ( void** )&profilVertices, 0 ) ) )
	        return E_FAIL;
		for (i=0;i!=Data_Cut_Nbr;i++)
		{
			profilVertices[i].color=0xff0000ff;
			profilVertices[i].x=Data_Cut[i].XG;
			profilVertices[i].y=Data_Cut[i].YG;
			profilVertices[i].z=0.0f;
		}
	    g_pVB_PROFILA->Unlock();
		// PROFILB
		if( FAILED( g_pd3dDevice->CreateVertexBuffer( (Data_Cut_Nbr+1) * sizeof( CUSTOMVERTEX ),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_PROFILB, NULL ) ) )
	        return E_FAIL;
		if( FAILED( g_pVB_PROFILB->Lock( 0, (Data_Cut_Nbr+1) * sizeof( CUSTOMVERTEX ), ( void** )&profilVertices, 0 ) ) )
	        return E_FAIL;
		for (i=0;i!=Data_Cut_Nbr;i++)
		{
			profilVertices[i].color=0xff00ff00;
			profilVertices[i].x=Data_Cut[i].XD;
			profilVertices[i].y=Data_Cut[i].YD;
			profilVertices[i].z=Meca.SIZE_Z;
		}
	    g_pVB_PROFILB->Unlock();
		// POLY CUT
		if( FAILED( g_pd3dDevice->CreateVertexBuffer( 2*(Poly_Cut_Nbr+1) * sizeof( CUSTOMVERTEX ),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_POLYCUT, NULL ) ) )
	        return E_FAIL;
		if( FAILED( g_pVB_POLYCUT->Lock( 0, 2*(Poly_Cut_Nbr+1) * sizeof( CUSTOMVERTEX ), ( void** )&profilVertices, 0 ) ) )
	        return E_FAIL;
		for (i=0;i!=Poly_Cut_Nbr;i++)
		{
			profilVertices[i*2].color=0x50ff0000;
			profilVertices[i*2].x=((Data_Cut[i].XD-Data_Cut[i].XG)*Poly.PosZ)/Meca.SIZE_Z+Data_Cut[i].XG;
			profilVertices[i*2].y=((Data_Cut[i].YD-Data_Cut[i].YG)*Poly.PosZ)/Meca.SIZE_Z+Data_Cut[i].YG;
			profilVertices[i*2].z=Poly.PosZ;
			profilVertices[i*2+1].color=0x50ff0000;
			profilVertices[i*2+1].x=((Data_Cut[i].XD-Data_Cut[i].XG)*(Poly.PosZ+Poly.SizeZ))/Meca.SIZE_Z+Data_Cut[i].XG;
			profilVertices[i*2+1].y=((Data_Cut[i].YD-Data_Cut[i].YG)*(Poly.PosZ+Poly.SizeZ))/Meca.SIZE_Z+Data_Cut[i].YG;
			profilVertices[i*2+1].z=Poly.PosZ+Poly.SizeZ;
		}
	    g_pVB_POLYCUT->Unlock();
		if( FAILED( g_pd3dDevice->CreateVertexBuffer( 4*(Poly_Cut_Nbr+1) * sizeof( CUSTOMVERTEX ),0, D3DFVF_CUSTOMVERTEX,D3DPOOL_DEFAULT, &g_pVB_POLYCUTOUT, NULL ) ) )
	        return E_FAIL;
		if( FAILED( g_pVB_POLYCUTOUT->Lock( 0, 4*(Poly_Cut_Nbr+1) * sizeof( CUSTOMVERTEX ), ( void** )&profilVertices, 0 ) ) )
	        return E_FAIL;
		for (i=0;i!=Poly_Cut_Nbr-1;i++)
		{
			profilVertices[i*4].color=0xffff0000;
			profilVertices[i*4].x=((Data_Cut[i].XD-Data_Cut[i].XG)*Poly.PosZ)/Meca.SIZE_Z+Data_Cut[i].XG;
			profilVertices[i*4].y=((Data_Cut[i].YD-Data_Cut[i].YG)*Poly.PosZ)/Meca.SIZE_Z+Data_Cut[i].YG;
			profilVertices[i*4].z=Poly.PosZ;
			profilVertices[i*4+1].color=0xffff0000;
			profilVertices[i*4+1].x=((Data_Cut[i+1].XD-Data_Cut[i+1].XG)*Poly.PosZ)/Meca.SIZE_Z+Data_Cut[i+1].XG;
			profilVertices[i*4+1].y=((Data_Cut[i+1].YD-Data_Cut[i+1].YG)*Poly.PosZ)/Meca.SIZE_Z+Data_Cut[i+1].YG;
			profilVertices[i*4+1].z=Poly.PosZ;
			profilVertices[i*4+2].color=0xffff0000;
			profilVertices[i*4+2].x=((Data_Cut[i].XD-Data_Cut[i].XG)*(Poly.PosZ+Poly.SizeZ))/Meca.SIZE_Z+Data_Cut[i].XG;
			profilVertices[i*4+2].y=((Data_Cut[i].YD-Data_Cut[i].YG)*(Poly.PosZ+Poly.SizeZ))/Meca.SIZE_Z+Data_Cut[i].YG;
			profilVertices[i*4+2].z=Poly.PosZ+Poly.SizeZ;
			profilVertices[i*4+3].color=0xffff0000;
			profilVertices[i*4+3].x=((Data_Cut[i+1].XD-Data_Cut[i+1].XG)*(Poly.PosZ+Poly.SizeZ))/Meca.SIZE_Z+Data_Cut[i+1].XG;
			profilVertices[i*4+3].y=((Data_Cut[i+1].YD-Data_Cut[i+1].YG)*(Poly.PosZ+Poly.SizeZ))/Meca.SIZE_Z+Data_Cut[i+1].YG;
			profilVertices[i*4+3].z=Poly.PosZ+Poly.SizeZ;
		}
		g_pVB_POLYCUTOUT->Unlock();
	}
    return S_OK;
}


//-----------------------------------------------------------------------------
// Name: SetupMatrices()
// Desc: Sets up the world, view, and projection transform Matrices.
//-----------------------------------------------------------------------------
VOID SetupMatrices()
{
	D3DXMATRIXA16 matRotX,matRotY,matRotZ,matTrans1,matTrans2,matWorld,matScale;
	float Zoom_mat;

	// Set up world matrix
	//----------------------
	// Calculate rotation matrix
	D3DXMatrixRotationX( &matRotX, ( (AngleX)* 2 * D3DX_PI ) / 1024.0f  );
	D3DXMatrixRotationY( &matRotY, ( (AngleY)* 2 * D3DX_PI ) / 1024.0f );
	// Calculate a translation matrix
//	D3DXMatrixTranslation(&matTrans1,171/2,143/2,-Meca.SIZE_Z/2);
//	D3DXMatrixTranslation(&matTrans2,-171/2-MoveX,-143/2,0.0f);
	D3DXMatrixTranslation(&matTrans1,0.0f,0.0f,-Meca.SIZE_Z/2);
	D3DXMatrixTranslation(&matTrans2,(float)-MoveX,(float)-MoveY,0.0f);
	// Calculate a scaling matrix
	Zoom_mat=(float)Zoom/100;
	D3DXMatrixScaling(&matScale, Zoom_mat,Zoom_mat,Zoom_mat);
	// Calculate our world matrix by multiplying the above (in the correct order)
	matWorld=matTrans1*(matRotX*matRotY)*matTrans2*matScale; //
	g_pd3dDevice->SetTransform( D3DTS_WORLD, &matWorld );

    // Set up view matrix
	//----------------------
//    D3DXVECTOR3 vEyePt( 171/2, 143/2,Meca.SIZE_Z/2);
//    D3DXVECTOR3 vLookatPt( 171/2, 143/2, Meca.SIZE_Z);
    D3DXVECTOR3 vEyePt( 0.0f, 0.0f,Meca.SIZE_Z/2);
    D3DXVECTOR3 vLookatPt( 0.0f, 0.0f, Meca.SIZE_Z);
    D3DXVECTOR3 vUpVec( 0.0f, 1.0f, 0.0f );
    D3DXMATRIXA16 matView;
    D3DXMatrixLookAtRH( &matView, &vEyePt, &vLookatPt, &vUpVec );
    g_pd3dDevice->SetTransform( D3DTS_VIEW, &matView );

    // Set up projection matrix
	//--------------------------
    D3DXMATRIXA16 matProj;
	D3DXMatrixOrthoRH( &matProj, (float)rcClient3D.right, (float)rcClient3D.bottom, -10000.0f, 10000.0f );
    g_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
}

//-----------------------------------------------------------------------------
// Name: Render()
// Desc: Draws the scene
//-----------------------------------------------------------------------------
VOID Render()
{
    // Clear the backbuffer to a black color
    g_pd3dDevice->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB( 0, 0, 0 ), 1.0f, 0 );

    // Begin the scene
    if( SUCCEEDED( g_pd3dDevice->BeginScene() ) )
    {
		// Setup the world, view, and projection Matrices
        SetupMatrices();

        // Render the vertex buffer contents
		// TABLE
		g_pd3dDevice->SetStreamSource( 0, g_pVB_TABLE, 0, sizeof( CUSTOMVERTEX ) );
        g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 8 );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 10, 2 );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 14, 2 );
		// POLY
		g_pd3dDevice->SetRenderState(D3DRS_ZENABLE,  FALSE ); // Z-Buffer (Depth Buffer)
		g_pd3dDevice->SetStreamSource( 0, g_pVB_POLY, 0, sizeof( CUSTOMVERTEX ) );
        g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 8 );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 10, 2 );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 14, 2 );
		g_pd3dDevice->SetRenderState(D3DRS_ZENABLE,  TRUE ); // Z-Buffer (Depth Buffer)
		// POLYOUT
		g_pd3dDevice->SetStreamSource( 0, g_pVB_POLYOUT, 0, sizeof( CUSTOMVERTEX ) );
        g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
        g_pd3dDevice->DrawPrimitive( D3DPT_LINESTRIP, 0, 15 );
		if(Data_Cut_Nbr!=0)
		{
			// POLYCUT
			g_pd3dDevice->SetRenderState(D3DRS_ZENABLE,  FALSE ); // Z-Buffer (Depth Buffer)
			g_pd3dDevice->SetStreamSource( 0, g_pVB_POLYCUT, 0, sizeof( CUSTOMVERTEX ) );
			g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
			g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 2*(Poly_Cut_Nbr-1) );
			g_pd3dDevice->SetRenderState(D3DRS_ZENABLE,  TRUE ); // Z-Buffer (Depth Buffer)
			// POLYCUTOUT
			g_pd3dDevice->SetStreamSource( 0, g_pVB_POLYCUTOUT, 0, sizeof( CUSTOMVERTEX ) );
			g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
			g_pd3dDevice->DrawPrimitive( D3DPT_LINELIST, 0, 2*(Poly_Cut_Nbr-1) );
			// PROFILA
			g_pd3dDevice->SetStreamSource( 0, g_pVB_PROFILA, 0, sizeof( CUSTOMVERTEX ) );
			g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
			g_pd3dDevice->DrawPrimitive( D3DPT_LINESTRIP, 0, Data_Cut_Nbr-1 );
			// PROFILB
			g_pd3dDevice->SetStreamSource( 0, g_pVB_PROFILB, 0, sizeof( CUSTOMVERTEX ) );
			g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
			g_pd3dDevice->DrawPrimitive( D3DPT_LINESTRIP, 0, Data_Cut_Nbr-1 );
		}
		// STRUCT
        g_pd3dDevice->SetStreamSource( 0, g_pVB_STRUCT, 0, sizeof( CUSTOMVERTEX ) );
        g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
        g_pd3dDevice->DrawPrimitive( D3DPT_LINELIST, 0, 3 );
		// XG
        g_pd3dDevice->SetStreamSource( 0, g_pVB_XG, 0, sizeof( CUSTOMVERTEX ) );
        g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 8 );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 10, 2 );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 14, 2 );
		// XD
        g_pd3dDevice->SetStreamSource( 0, g_pVB_XD, 0, sizeof( CUSTOMVERTEX ) );
        g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 8 );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 10, 2 );
        g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 14, 2 );
		// REPERE
        g_pd3dDevice->SetStreamSource( 0, g_pVB, 0, sizeof( CUSTOMVERTEX ) );
        g_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
        g_pd3dDevice->DrawPrimitive( D3DPT_LINELIST, 0, 10 );

        // End the scene
        g_pd3dDevice->EndScene();
    }

    // Present the backbuffer contents to the display
    g_pd3dDevice->Present( NULL, NULL, NULL, NULL );
}


//-----------------------------------------------------------------------------
// Name: Cleanup()
// Desc: Releases all previously initialized objects
//-----------------------------------------------------------------------------
VOID Cleanup()
{
	if( g_pVB != NULL )
	{
		g_pVB->Release();
		g_pVB=NULL;
	}
    if( g_pVB_TABLE != NULL )
	{
		g_pVB_TABLE->Release();
		g_pVB_TABLE=NULL;
	}
    if( g_pVB_STRUCT != NULL )
	{
		g_pVB_STRUCT->Release();
		g_pVB_STRUCT=NULL;
	}
    if( g_pVB_XG != NULL )
	{
		g_pVB_XG->Release();
		g_pVB_XG=NULL;
	}
    if( g_pVB_XD != NULL )
	{
		g_pVB_XD->Release();
		g_pVB_XD=NULL;
	}
    if( g_pVB_POLY != NULL )
	{
		g_pVB_POLY->Release();
		g_pVB_POLY=NULL;
	}
    if( g_pVB_POLYOUT != NULL )
	{
		g_pVB_POLYOUT->Release();
		g_pVB_POLYOUT=NULL;
	}
    if( g_pVB_POLYCUT != NULL )
	{
		g_pVB_POLYCUT->Release();
		g_pVB_POLYCUT=NULL;
	}
    if( g_pVB_POLYCUTOUT != NULL )
	{
		g_pVB_POLYCUTOUT->Release();
		g_pVB_POLYCUTOUT=NULL;
	}
    if( g_pVB_PROFILA != NULL )
	{
        g_pVB_PROFILA->Release();
		g_pVB_PROFILA=NULL;
	}
    if( g_pVB_PROFILB != NULL )
	{
        g_pVB_PROFILB->Release();
		g_pVB_PROFILB=NULL;
	}

/*	if( g_pd3dDevice != NULL )
	{
		g_pd3dDevice->Release();
        g_pd3dDevice=NULL;
	}

    if( g_pD3D != NULL )
	{
        g_pD3D->Release();
        g_pD3D=NULL;
	}
*/
}
void Cube(float posx,float posy,float posz,float sx,float sy,float sz,DWORD color1,DWORD color2,CUSTOMVERTEX *Vertices)
{
	int i;

	for(i=0;i!=18;i++)
	{
		Vertices[i].color=color1;
		Vertices[i].x=posx;
		Vertices[i].y=posy;
		Vertices[i].z=posz;
	}

	// Front side
	Vertices[0].y+=sy;
	Vertices[2].y+=sy;
	Vertices[2].z+=sz;
	Vertices[2].color=color2;
	Vertices[3].z+=sz;
	Vertices[3].color=color2;
	//Right side
	Vertices[4].x+=sx;
	Vertices[4].y+=sy;
	Vertices[4].z+=sz;
	Vertices[4].color=color2;
	Vertices[5].x+=sx;
	Vertices[5].z+=sz;
	Vertices[5].color=color2;
	// Back side
	Vertices[6].x+=sx;
	Vertices[6].y+=sy;
	Vertices[6].color=color2;
	Vertices[7].x+=sx;
    //Left side
	Vertices[8].y+=sy;
	Vertices[8].color=color2;
	//Top
	Vertices[11].x+=sx;
	Vertices[12].z+=sz;
	Vertices[12].color=color2;
	Vertices[13].x+=sx;
	Vertices[13].z+=sz;
	Vertices[13].color=color2;
	//Bottom
	Vertices[14].y+=sy;
	Vertices[15].x+=sx;
	Vertices[15].y+=sy;
	Vertices[16].y+=sy;
	Vertices[16].z+=sz;
	Vertices[16].color=color2;
	Vertices[17].x+=sx;
	Vertices[17].y+=sy;
	Vertices[17].z+=sz;
	Vertices[17].color=color2;
}

void CubeWire(float posx,float posy,float posz,float sx,float sy,float sz,DWORD color1,CUSTOMVERTEX *Vertices)
{
	int i;

	for(i=0;i!=16;i++)
	{
		Vertices[i].color=color1;
		Vertices[i].x=posx;
		Vertices[i].y=posy;
		Vertices[i].z=posz;
	}

	Vertices[1].y+=sy;
	Vertices[2].y+=sy;
	Vertices[2].z+=sz;
	Vertices[3].x+=sx;
	Vertices[3].y+=sy;
	Vertices[3].z+=sz;
	Vertices[4].x+=sx;
	Vertices[4].z+=sz;
	Vertices[5].x+=sx;
	Vertices[6].x+=sx;
	Vertices[6].y+=sy;
	Vertices[7].x+=sx;
	Vertices[7].y+=sy;
	Vertices[7].z+=sz;
	Vertices[8].x+=sx;
	Vertices[8].z+=sz;
	Vertices[9].z+=sz;
	Vertices[11].x+=sx;
	Vertices[12].x+=sx;
	Vertices[12].y+=sy;
	Vertices[13].y+=sy;
	Vertices[14].y+=sy;
	Vertices[14].z+=sz;
	Vertices[15].z+=sz;
}
