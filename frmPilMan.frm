VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPilMan 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Pilotage manuel"
   ClientHeight    =   6900
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3585
   ControlBox      =   0   'False
   Icon            =   "frmPilMan.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6900
   ScaleWidth      =   3585
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      TabIndex        =   38
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   6360
      Width           =   3615
      Begin VB.CommandButton Cmd_Nettoyage 
         BackColor       =   &H000000FF&
         Caption         =   "Nettoyage fil"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   39
         Tag             =   "72"
         ToolTipText     =   "0"
         Top             =   120
         Width           =   3525
      End
   End
   Begin MSComctlLib.StatusBar SB 
      Align           =   2  'Align Bottom
      Height          =   30
      Left            =   0
      TabIndex        =   37
      Top             =   6870
      Width           =   3585
      _ExtentX        =   6324
      _ExtentY        =   53
      SimpleText      =   "..."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Cmd_GOandSTOP 
      Caption         =   "GO and STOP"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   35
      Tag             =   "71"
      ToolTipText     =   "0"
      Top             =   6000
      Width           =   3525
   End
   Begin VB.CommandButton Cmd_Stop_Manuel 
      Caption         =   "STOP"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   34
      Tag             =   "69"
      ToolTipText     =   "0"
      Top             =   4080
      Width           =   3600
   End
   Begin VB.Frame FramePil 
      Caption         =   "Incr�ments"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   885
      Index           =   1
      Left            =   0
      TabIndex        =   19
      Tag             =   "56"
      ToolTipText     =   "0"
      Top             =   720
      Width           =   3585
      Begin VB.OptionButton Option_Unite 
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   1920
         TabIndex        =   26
         Tag             =   "57"
         ToolTipText     =   "0"
         Top             =   240
         Value           =   -1  'True
         Width           =   645
      End
      Begin VB.OptionButton Option_Unite 
         Caption         =   "pas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   2760
         TabIndex        =   25
         Tag             =   "58"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   645
      End
      Begin VB.OptionButton Option_Unite 
         Caption         =   "Continu"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   2
         Left            =   1920
         TabIndex        =   24
         Tag             =   "60"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   1485
      End
      Begin VB.Frame Frame_Nb 
         Height          =   510
         Left            =   0
         TabIndex        =   20
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1860
         Begin VB.OptionButton Option_Increment 
            BackColor       =   &H8000000B&
            Caption         =   "1"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   60
            TabIndex        =   23
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   135
            Width           =   360
         End
         Begin VB.OptionButton Option_Increment 
            BackColor       =   &H8000000B&
            Caption         =   "10"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   495
            TabIndex        =   22
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   135
            Value           =   -1  'True
            Width           =   570
         End
         Begin VB.OptionButton Option_Increment 
            BackColor       =   &H8000000B&
            Caption         =   "100"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   1095
            TabIndex        =   21
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   135
            Width           =   690
         End
      End
   End
   Begin VB.Frame FramePil 
      Caption         =   "Valeurs (+/-)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1485
      Index           =   2
      Left            =   0
      TabIndex        =   8
      Tag             =   "70"
      ToolTipText     =   "0"
      Top             =   4440
      Width           =   3585
      Begin VB.TextBox TB_D 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   2670
         TabIndex        =   14
         Text            =   "0"
         ToolTipText     =   "0"
         Top             =   1065
         Width           =   750
      End
      Begin VB.TextBox TB_D 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   510
         TabIndex        =   13
         Text            =   "0"
         ToolTipText     =   "0"
         Top             =   1065
         Width           =   750
      End
      Begin VB.TextBox TB_D 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   2670
         TabIndex        =   12
         Text            =   "0"
         ToolTipText     =   "0"
         Top             =   645
         Width           =   750
      End
      Begin VB.TextBox TB_D 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   510
         TabIndex        =   11
         Text            =   "0"
         ToolTipText     =   "0"
         Top             =   645
         Width           =   750
      End
      Begin VB.CommandButton CmdPlusMoins 
         Caption         =   "+/-"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1920
         TabIndex        =   10
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   495
      End
      Begin VB.CommandButton CmdRAZ 
         Caption         =   "RAZ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         TabIndex        =   9
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "YD :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   3
         Left            =   2250
         TabIndex        =   18
         Tag             =   "29"
         ToolTipText     =   "0"
         Top             =   1140
         Width           =   450
      End
      Begin VB.Label Label3 
         Caption         =   "XD :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   2
         Left            =   2250
         TabIndex        =   17
         Tag             =   "27"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   450
      End
      Begin VB.Label Label3 
         Caption         =   "YG :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   90
         TabIndex        =   16
         Tag             =   "28"
         ToolTipText     =   "0"
         Top             =   1140
         Width           =   450
      End
      Begin VB.Label Label3 
         Caption         =   "XG :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   4
         Left            =   90
         TabIndex        =   15
         Tag             =   "26"
         Top             =   720
         Width           =   450
      End
   End
   Begin VB.Frame FramePil 
      Caption         =   "D�placements"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2325
      Index           =   3
      Left            =   0
      TabIndex        =   3
      Tag             =   "61"
      ToolTipText     =   "0"
      Top             =   1680
      Width           =   3585
      Begin VB.CommandButton Cmd_Depl_Manu 
         BackColor       =   &H000000FF&
         Height          =   495
         Index           =   8
         Left            =   1080
         Picture         =   "frmPilMan.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   33
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1800
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Depl_Manu 
         BackColor       =   &H000000FF&
         Height          =   495
         Index           =   7
         Left            =   120
         Picture         =   "frmPilMan.frx":09CC
         Style           =   1  'Graphical
         TabIndex        =   32
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1800
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Depl_Manu 
         BackColor       =   &H000000FF&
         Height          =   495
         Index           =   6
         Left            =   1080
         Picture         =   "frmPilMan.frx":0E0E
         Style           =   1  'Graphical
         TabIndex        =   31
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Depl_Manu 
         BackColor       =   &H000000FF&
         Height          =   495
         Index           =   5
         Left            =   120
         Picture         =   "frmPilMan.frx":1250
         Style           =   1  'Graphical
         TabIndex        =   30
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   495
      End
      Begin VB.CommandButton Cmd_GD 
         BackColor       =   &H0000FF00&
         Caption         =   "D"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   2
         Left            =   3000
         Style           =   1  'Graphical
         TabIndex        =   29
         Tag             =   "172"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   495
      End
      Begin VB.CommandButton Cmd_GD 
         BackColor       =   &H00C000C0&
         Caption         =   "G=D"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   1
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   28
         Tag             =   "64"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton Cmd_GD 
         BackColor       =   &H000000FF&
         Caption         =   "G"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   0
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   27
         Tag             =   "171"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Depl_Manu 
         BackColor       =   &H000000FF&
         Height          =   495
         Index           =   2
         Left            =   120
         Picture         =   "frmPilMan.frx":1692
         Style           =   1  'Graphical
         TabIndex        =   7
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1320
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Depl_Manu 
         BackColor       =   &H000000FF&
         Height          =   495
         Index           =   3
         Left            =   1080
         Picture         =   "frmPilMan.frx":1AD4
         Style           =   1  'Graphical
         TabIndex        =   6
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1320
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Depl_Manu 
         BackColor       =   &H000000FF&
         Height          =   495
         Index           =   4
         Left            =   600
         Picture         =   "frmPilMan.frx":1F16
         Style           =   1  'Graphical
         TabIndex        =   5
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1800
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Depl_Manu 
         BackColor       =   &H000000FF&
         Height          =   495
         Index           =   1
         Left            =   600
         Picture         =   "frmPilMan.frx":2358
         Style           =   1  'Graphical
         TabIndex        =   4
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   495
      End
   End
   Begin VB.Frame FramePil 
      Caption         =   "Vitesse"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Tag             =   "52"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   3585
      Begin VB.Timer Timer_Touche 
         Interval        =   100
         Left            =   1200
         Top             =   240
      End
      Begin VB.CheckBox CB_VitMax 
         Caption         =   "Vitesse maxi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   36
         Tag             =   "55"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   1695
      End
      Begin VB.TextBox TB_Vitesse_Manuel 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   120
         TabIndex        =   1
         Tag             =   "0"
         Text            =   "4"
         ToolTipText     =   "0"
         Top             =   270
         Width           =   750
      End
      Begin VB.Label Label3 
         Caption         =   "mm / s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   960
         TabIndex        =   2
         Tag             =   "53"
         ToolTipText     =   "0"
         Top             =   315
         Width           =   735
      End
   End
End
Attribute VB_Name = "frmPilMan"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim AncienXD As String
Dim AncienYD As String
Dim bTouche As Boolean
Dim i As Long
Dim iGD As Integer

Private Sub CB_VitMax_Click()
    TB_Vitesse_Manuel.Enabled = (CB_VitMax.value = 0)
End Sub

Private Sub Cmd_Depl_Manu_Click(Index As Integer)
    Dim Vitesse As Single
    Dim Dist As Double
    Dim TpsReel As Single
    Dim XD As Double
    Dim YD As Double
    Dim XG As Double
    Dim YG As Double
    Dim P1 As Point, P2 As Point
    
    If DeplEnCours Then Exit Sub
    If Option_Unite(2) Then Exit Sub  'D�placement infini, on passe par "MouseDown"
    If Not maClasseHID.IsAvailable Then Exit Sub
    Call maClasseHID.TableVerif
    
    'Pour emp�cher des appuis successifs
    DeplEnCours = True
    Call FrameEnable(False)
    Cmd_GOandSTOP.Enabled = False
    Cmd_Stop_Manuel.Enabled = True
    Cmd_Stop_Manuel.BackColor = RGB(255, 0, 0)

    gPause = False
    
    If Option_Increment(0).value Then Dist = 1
    If Option_Increment(1).value Then Dist = 10
    If Option_Increment(2).value Then Dist = 100
        
    Select Case Index
        Case 1, 5, 6
            If iGD <= 1 Then YG = Dist
            If iGD >= 1 Then YD = Dist
        Case 2, 3
            If iGD <= 1 Then YG = 0#
            If iGD >= 1 Then YD = 0#
        Case 4, 7, 8
            If iGD <= 1 Then YG = -Dist
            If iGD >= 1 Then YD = -Dist
    End Select
    Select Case Index
        Case 2, 5, 7
            If iGD <= 1 Then XG = -Dist
            If iGD >= 1 Then XD = -Dist
        Case 1, 4
            If iGD <= 1 Then XG = 0#
            If iGD >= 1 Then XD = 0#
        Case 3, 6, 8
            If iGD <= 1 Then XG = Dist
            If iGD >= 1 Then XD = Dist
    End Select
    Vitesse = CDbl(TB_Vitesse_Manuel)
    P1.x = 0#
    P1.y = 0#
    P2.x = Max(Abs(XD), Abs(XG))
    P2.y = Max(Abs(YD), Abs(YG))
    If CB_VitMax.value = vbChecked Then
        Vitesse = Calcul_Vitesse(P1, P2, CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 1)), _
            CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 1)))
    End If
    
    If ReadINI_Machine("table_" & gstrTable, "CB_SAV_13", 0) = vbChecked Then        'Inversion table
    'NB : Il faut inverser � ce niveau (et aussi au niveau de "MouseDown")
    '     car il y a une inversion au niveau de la classe IPL5X
        XD = -XD
        XG = -XG
    End If

    Call maClasseHID.MotorOnOff(True)
    Call maClasseHID.EnvoiUSB
    If Option_Unite(0).value Then       'mm
        Call maClasseHID.CommandeDATAmm(XD, YD, XG, YG, Calcul_Temps_Points(P1, P2, Vitesse), TpsReel)
    Else                                'Pas
        Call maClasseHID.CommandeDATA(XD, YD, XG, YG, Max(P2.x * maClasseHID.X_Demult / Vitesse, P2.y * maClasseHID.Y_Demult / Vitesse), TpsReel)
    End If
    Call maClasseHID.CommandeDATAend
    Call maClasseHID.DemandeInformation
    While maClasseHID.Step_Processed And Not gPause
        DoEvents
        Call maClasseHID.DemandeInformation
    Wend
    If gPause Then
        Call maClasseHID.StopResume(FastStop, 0)
        Call maClasseHID.DemandeInformation
        While maClasseHID.Step_Processed
            DoEvents
            Call maClasseHID.DemandeInformation
            DoEvents
        Wend
    End If
    
    Call FrameEnable(True)

    Cmd_GOandSTOP.Enabled = True
    Cmd_Stop_Manuel.Enabled = False
    Cmd_Stop_Manuel.BackColor = &H8000000F
    Cmd_GOandSTOP.BackColor = &H8000000F
    DeplEnCours = False
End Sub

Private Sub Cmd_Depl_Manu_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim Depl As Single
    Dim Vitesse As Single
    Dim TpsReel As Single
    Dim Dist As Single
    Dim XD As Double
    Dim YD As Double
    Dim XG As Double
    Dim YG As Double
    Dim P1 As Point, P2 As Point

    If Not Option_Unite(2) Then Exit Sub   'D�placement infini
    If Not maClasseHID.IsAvailable Then Exit Sub
    If Not maClasseHID.TableVerif Then Exit Sub
    Dist = 2000
    DeplEnCours = True
    
    Select Case Index
        Case 1, 5, 6
            If iGD <= 1 Then YG = Dist
            If iGD >= 1 Then YD = Dist
        Case 2, 3
            If iGD <= 1 Then YG = 0#
            If iGD >= 1 Then YD = 0#
        Case 4, 7, 8
            If iGD <= 1 Then YG = -Dist
            If iGD >= 1 Then YD = -Dist
    End Select
    Select Case Index
        Case 2, 5, 7
            If iGD <= 1 Then XG = -Dist
            If iGD >= 1 Then XD = -Dist
        Case 1, 4
            If iGD <= 1 Then XG = 0#
            If iGD >= 1 Then XD = 0#
        Case 3, 6, 8
            If iGD <= 1 Then XG = Dist
            If iGD >= 1 Then XD = Dist
    End Select
    Vitesse = CDbl(TB_Vitesse_Manuel)
    P1.x = 0#
    P1.y = 0#
    P2.x = Max(Abs(XD), Abs(XG))
    P2.y = Max(Abs(YD), Abs(YG))
    If CB_VitMax.value = vbChecked Then
        Vitesse = Calcul_Vitesse(P1, P2, CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 1)), _
            CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 1)))
    End If
    
    If ReadINI_Machine("table_" & gstrTable, "CB_SAV_13", 0) = vbChecked Then        'Inversion table
        XD = -XD
        XG = -XG
    End If
    
    gPause = False
    
    If (Shift And vbShiftMask) = vbShiftMask Then Vitesse = Vitesse / 10#
    If (Shift And vbCtrlMask) = vbCtrlMask Then Vitesse = Vitesse / 100#
    If (Shift And vbCtrlMask) = vbCtrlMask And _
        (Shift And vbShiftMask) = vbShiftMask Then _
            Vitesse = Vitesse * 2#
    Call maClasseHID.MotorOnOff(True)
    Call maClasseHID.EnvoiUSB
    Call maClasseHID.CommandeDATAmm(XD, YD, XG, YG, Calcul_Temps_Points(P1, P2, Vitesse), TpsReel)
    Call maClasseHID.CommandeDATAend
    Call maClasseHID.GoBuffer
End Sub

Private Sub Cmd_Depl_Manu_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Not Option_Unite(2) Then Exit Sub    'D�placement infini
    gPause = True
    If Not maClasseHID.IsAvailable Then Exit Sub
    Call maClasseHID.StopResume(FastStop, 0)
    Call maClasseHID.DemandeInformation
    While maClasseHID.Step_Processed
        DoEvents
        Call maClasseHID.DemandeInformation
        DoEvents
    Wend
    DeplEnCours = False
End Sub

Private Sub Cmd_GD_Click(Index As Integer)
    iGD = Index
    TB_D(2).Visible = (Index <> 1)
    TB_D(4).Visible = TB_D(2).Visible
    Label3(2).Visible = TB_D(2).Visible
    Label3(3).Visible = TB_D(2).Visible
    If Index = 1 Then   'G=D
        Label3(4) = "X"
        Label3(1) = "Y"
    Else
        Label3(4) = AncienXD    '"XD"
        Label3(1) = AncienYD    '"YD"
    End If
    For i = Cmd_Depl_Manu.LBound To Cmd_Depl_Manu.UBound
        Cmd_Depl_Manu(i).BackColor = Cmd_GD(Index).BackColor
    Next i
    Cmd_Depl_Manu(1).Left = FramePil(3).Width / 2 - Cmd_Depl_Manu(1).Width / 2
    Select Case Index
        Case 0  'Gauche
            Cmd_Depl_Manu(1).Left = Cmd_Depl_Manu(1).Left - Cmd_Depl_Manu(1).Width * 2
        Case 2  'Droit
            Cmd_Depl_Manu(1).Left = Cmd_Depl_Manu(1).Left + Cmd_Depl_Manu(1).Width * 2
    End Select
    Cmd_Depl_Manu(4).Left = Cmd_Depl_Manu(1).Left
    Cmd_Depl_Manu(5).Left = Cmd_Depl_Manu(1).Left - Cmd_Depl_Manu(1).Width
    Cmd_Depl_Manu(2).Left = Cmd_Depl_Manu(5).Left
    Cmd_Depl_Manu(7).Left = Cmd_Depl_Manu(5).Left
    Cmd_Depl_Manu(6).Left = Cmd_Depl_Manu(1).Left + Cmd_Depl_Manu(1).Width
    Cmd_Depl_Manu(3).Left = Cmd_Depl_Manu(6).Left
    Cmd_Depl_Manu(8).Left = Cmd_Depl_Manu(6).Left
End Sub

Private Sub Cmd_GOandSTOP_Click()
    Dim PDepart As Point
    Dim PG As Point
    Dim PD As Point
    Dim Dist As Double
    Dim Vitesse As Single
    Dim TpsReel As Single
    Dim P1 As Point, P2 As Point
    
    If DeplEnCours Then
        gPause = True
        Exit Sub
    End If
    
    If Not maClasseHID.IsAvailable Then Exit Sub
    If Not maClasseHID.TableVerif Then Exit Sub
    
    If Option_Unite(2).value Then Exit Sub      'Continu
   
    If iGD = 1 Then
        TB_D(2) = TB_D(1)
        TB_D(4) = TB_D(3)
    End If
    
    If TB_D(1) = "" And TB_D(2) = "" And TB_D(3) = "" And TB_D(4) = "" Then
        Exit Sub
    End If
    If CDbl(TB_D(1)) = 0 And CDbl(TB_D(2)) = 0 And CDbl(TB_D(3)) = 0 And CDbl(TB_D(4)) = 0 Then
        Exit Sub
    End If
    
    gPause = False
    'Pour emp�cher des appuis successifs
    Call FrameEnable(False)
    Cmd_GOandSTOP.Enabled = True
    Cmd_Stop_Manuel.Enabled = True
    Cmd_GOandSTOP.BackColor = RGB(255, 0, 0)
    
    PDepart.x = 0
    PDepart.y = 0
    
    PG.x = CDbl(TB_D(1))
    PG.y = CDbl(TB_D(3))
    
    PD.x = CDbl(TB_D(2))
    PD.y = CDbl(TB_D(4))
    
    DeplEnCours = True
    
    Vitesse = CDbl(TB_Vitesse_Manuel)
    P1.x = 0#
    P1.y = 0#
    P2.x = Max(Abs(PD.x), Abs(PG.x))
    P2.y = Max(Abs(PD.y), Abs(PG.y))
    If CB_VitMax.value = vbChecked Then
        Vitesse = Calcul_Vitesse(P1, P2, CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 1)), _
            CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 1)))
    End If
    maClasseHID.MotorOnOff (True)
    Call maClasseHID.EnvoiUSB
    If Option_Unite(0).value Then       'mm
        Call maClasseHID.CommandeDATAmm(0# + PD.x, 0# + PD.y, 0# + PG.x, 0# + PG.y, Calcul_Temps_Points(P1, P2, Vitesse), TpsReel)
    Else                                'Pas
        Call maClasseHID.CommandeDATA(0# + PD.x, 0# + PD.y, 0# + PG.x, 0# + PG.y, Max(P2.x * maClasseHID.X_Demult / Vitesse, P2.y * maClasseHID.Y_Demult / Vitesse), TpsReel)
    End If

    Call maClasseHID.CommandeDATAend
    Call maClasseHID.DemandeInformation
    While maClasseHID.Step_Processed And Not gPause
        DoEvents
        Call maClasseHID.DemandeInformation
    Wend
    If gPause Then
        Call maClasseHID.StopResume(FastStop, 0)
        Call maClasseHID.DemandeInformation
        While maClasseHID.Step_Processed
            DoEvents
            Call maClasseHID.DemandeInformation
            DoEvents
        Wend
    End If
    Cmd_Stop_Manuel.Enabled = False
    Cmd_Stop_Manuel.BackColor = &H8000000F
    
    Cmd_GOandSTOP.Enabled = True
    Cmd_Stop_Manuel.Enabled = False
    Cmd_Stop_Manuel.BackColor = &H8000000F
    Cmd_GOandSTOP.BackColor = &H8000000F
    DeplEnCours = False
    Call FrameEnable(True)
End Sub

Private Sub Cmd_Nettoyage_Click()
    Dim Tempo As Single
    Dim Vitesse As Single
    Dim y As Single
    Dim P As Point
    Dim P1 As Point
    
    Call FrameEnable(False)
    
    P1.x = 0#
    P1.y = 0#
    P.x = 0#
    y = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_34", 50)
    P.y = y
    Tempo = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_35", 60)
    Vitesse = CSng(TB_Vitesse_Manuel)
    If CB_VitMax.value = vbChecked Then
        Vitesse = CSng(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 1))
    End If
    
    Call maClasseHID.ChauffeEcrirePC(100)
    
    If Tempo <> 0 Then
        ReDim Tab_Chariot(1 To 4)
        Tab_Chariot(1).P_abs(EMPLANTURE) = P
        Tab_Chariot(1).P_abs(SAUMON) = P
        Tab_Chariot(1).Temps = Calcul_Temps_Points(P, P1, Vitesse)
        
        P.y = 0.01
        Tab_Chariot(2).P_abs(EMPLANTURE) = P
        Tab_Chariot(2).P_abs(SAUMON) = P
        Tab_Chariot(2).Temps = Tempo / 2#
        
        P.y = -0.01
        Tab_Chariot(3).P_abs(EMPLANTURE) = P
        Tab_Chariot(3).P_abs(SAUMON) = P
        Tab_Chariot(3).Temps = Tempo / 2#
        
        P.x = 0#
        P.y = -y
        Tab_Chariot(4).P_abs(EMPLANTURE) = P
        Tab_Chariot(4).P_abs(SAUMON) = P
        Tab_Chariot(4).Temps = Tab_Chariot(1).Temps
    
        Call Envoi_Decoupe(Tab_Chariot, Me)
    
        Call maClasseHID.ChauffeEcrirePC(0)
    Else
        ReDim Tab_Chariot(1 To 1)
        P.x = 0#
        P.y = y
        Tab_Chariot(1).P_abs(EMPLANTURE) = P
        Tab_Chariot(1).P_abs(SAUMON) = P
        Tab_Chariot(1).Temps = Calcul_Temps_Points(P, P1, Vitesse)
        Call Envoi_Decoupe(Tab_Chariot, Me)
        
        MsgBox ReadINI_Trad(gLangue, "erreurs", 76), vbQuestion + vbOKOnly
        Call maClasseHID.ChauffeEcrirePC(0)

        P.y = -y
        Tab_Chariot(1).P_abs(EMPLANTURE) = P
        Call Envoi_Decoupe(Tab_Chariot, Me)
    End If
    Call FrameEnable(True)
End Sub

Private Sub Cmd_Stop_Manuel_Click()
    gPause = True
End Sub

Private Sub Cmd_VitMax_Click()
    TB_Vitesse_Manuel = Min(CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 0)), _
        CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0)))
End Sub

Private Sub CmdPlusMoins_Click()
    For i = TB_D.LBound To TB_D.UBound
        If TB_D(i) <> "" Then TB_D(i) = -Val(TB_D(i))
    Next i
End Sub

Private Sub CmdRAZ_Click()
    For i = TB_D.LBound To TB_D.UBound
        TB_D(i) = "0"
    Next i
End Sub

Private Sub Form_Activate()
    Dim r As Long
    Const HWND_TOPMOST = -1
    Const HWND_NOTOPMOST = -2
    Const SWP_NOMOVE = &H2
    Const SWP_NOSIZE = &H1

    r = SetWindowPos(Me.hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
    bTouche = False
End Sub

Private Sub Form_Load()
    ChargeTrad Me
    Set gFenetreHID = frmPilMan
    Call maClasseHID.TableVerif
    
    Dim Rouge(0 To 2) As Byte
    Dim Vert(0 To 2) As Byte
    Dim Bleu(0 To 2) As Byte
    
    Rouge(1) = (Int(Cmd_GD(0).BackColor And &HFF) + Int(Cmd_GD(2).BackColor And &HFF)) / 2
    Vert(1) = (Int((Cmd_GD(0).BackColor And &H100FF00) / &H100) + Int((Cmd_GD(2).BackColor And &H100FF00) / &H100)) / 2
    Bleu(1) = (Int((Cmd_GD(0).BackColor And &HFF0000) / &H10000) + Int((Cmd_GD(2).BackColor And &HFF0000) / &H10000)) / 2
'    .Rouge = Int(Couleur And &HFF)
'    .Vert = Int((Couleur And &H100FF00) / &H100)
'    .Bleu = Int((Couleur And &HFF0000) / &H10000)

    Cmd_GD(1).BackColor = RGB(Rouge(1), Vert(1), Bleu(1))
    
    With Me
        Left = ReadINI_Num(Me.Name & "_Left", Me.Left)
        Top = ReadINI_Num(Me.Name & "_Top", Me.Top)
    End With
    TB_Vitesse_Manuel = ReadINI_Num(Me.Name & "_Vitesse", TB_Vitesse_Manuel)
    Option_Increment(ReadINI_Num(Me.Name & "_Increment", 1)) = True
    Call FrameEnable(CDbl(TB_Vitesse_Manuel) <> 0)
    Option_Unite(ReadINI_Num(Me.Name & "_Unite", 0)) = True
    
    AncienXD = Label3(4)
    AncienYD = Label3(1)
    Call Cmd_GD_Click(1)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
    End If
    Call WriteINI(Me.Name & "_Vitesse", TB_Vitesse_Manuel)
    For i = Option_Unite.LBound To Option_Unite.UBound
        If Option_Unite(i) Then Call WriteINI(Me.Name & "_Unite", Str(i))
    Next i
    For i = Option_Increment.LBound To Option_Increment.UBound
        If Option_Increment(i) Then Call WriteINI(Me.Name & "_Increment", Str(i))
    Next i
End Sub

Private Sub Option_Unite_Click(Index As Integer)
    Frame_Nb.Visible = Index <> 2
    FramePil(2).Enabled = Index <> 2
    Cmd_GOandSTOP.Enabled = Index <> 2
End Sub

Private Sub TB_D_Change(Index As Integer)
    Cmd_GOandSTOP.Enabled = False
    If TB_D(Index) = "" Then Exit Sub
    If Not IsNumeric(TB_D(Index)) Then Exit Sub
    If TB_Vitesse_Manuel = "" Then Exit Sub
    If (CDbl(TB_D(1)) <> 0 Or CDbl(TB_D(2)) <> 0 Or CDbl(TB_D(3)) <> 0 Or CDbl(TB_D(4)) <> 0) _
        And CDbl(TB_Vitesse_Manuel) <> 0 Then
        Cmd_GOandSTOP.Enabled = True
    End If
End Sub

Private Sub TB_D_DblClick(Index As Integer)
    TB_D(Index).SelStart = 0
    TB_D(Index).SelLength = Len(TB_D(Index))
End Sub

Private Sub TB_D_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Vitesse_Manuel_Change()
'    Call FrameEnable(False)
    Cmd_GOandSTOP.Enabled = False
    If IsNull(TB_Vitesse_Manuel()) Then Exit Sub
    If TB_Vitesse_Manuel() = "" Then Exit Sub
    If CDbl(TB_Vitesse_Manuel()) = 0 Then Exit Sub
'    Call FrameEnable(True)
    Cmd_GOandSTOP.Enabled = True
End Sub

Private Sub TB_Vitesse_Manuel_DblClick()    'todo : solution provisoire
    Me.Visible = False
    FrmPilManClavier.Visible = True
End Sub

Private Sub TB_Vitesse_Manuel_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub FrameEnable(bEnable As Boolean)
    For i = FramePil.LBound To FramePil.UBound
        FramePil(i).Enabled = bEnable
    Next i
End Sub
