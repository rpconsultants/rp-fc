VERSION 5.00
Begin VB.Form frmPasPerdus 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Outil de test des pas perdus"
   ClientHeight    =   3255
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6180
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3255
   ScaleWidth      =   6180
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.CommandButton Cmd_Depl 
      Caption         =   "- 1 pas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   1
      Left            =   1560
      TabIndex        =   15
      Tag             =   "31"
      ToolTipText     =   "0"
      Top             =   2280
      Width           =   1365
   End
   Begin VB.CommandButton Cmd_RAZ 
      Caption         =   "R.A.Z."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   5280
      TabIndex        =   7
      Tag             =   "33"
      ToolTipText     =   "0"
      Top             =   2280
      Width           =   870
   End
   Begin VB.CommandButton CMD_Memoriser 
      Caption         =   "M�moriser cet axe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   3120
      TabIndex        =   8
      Tag             =   "40"
      ToolTipText     =   "0"
      Top             =   2775
      Width           =   2160
   End
   Begin VB.CommandButton Cmd_Depl 
      Caption         =   "+ 10 mm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   0
      Left            =   0
      TabIndex        =   6
      Tag             =   "30"
      ToolTipText     =   "0"
      Top             =   2280
      Width           =   1365
   End
   Begin VB.Frame Frame_Axe 
      Caption         =   "Axe test�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   810
      Left            =   0
      TabIndex        =   0
      Tag             =   "20"
      Top             =   1410
      Width           =   6150
      Begin VB.OptionButton Option_Axe 
         Caption         =   "Z"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   4
         Left            =   3000
         TabIndex        =   14
         Tag             =   "27"
         ToolTipText     =   "0"
         Top             =   315
         Width           =   690
      End
      Begin VB.TextBox TB_Vitesse 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4605
         TabIndex        =   5
         Text            =   "4"
         ToolTipText     =   "0"
         Top             =   330
         Width           =   705
      End
      Begin VB.OptionButton Option_Axe 
         Caption         =   "YD"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   3
         Left            =   2280
         TabIndex        =   4
         Tag             =   "24"
         ToolTipText     =   "0"
         Top             =   315
         Width           =   690
      End
      Begin VB.OptionButton Option_Axe 
         Caption         =   "YG"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   2
         Left            =   1560
         TabIndex        =   3
         Tag             =   "23"
         ToolTipText     =   "0"
         Top             =   315
         Width           =   690
      End
      Begin VB.OptionButton Option_Axe 
         Caption         =   "XD"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   840
         TabIndex        =   2
         Tag             =   "22"
         ToolTipText     =   "0"
         Top             =   315
         Width           =   690
      End
      Begin VB.OptionButton Option_Axe 
         Caption         =   "XG"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Tag             =   "21"
         ToolTipText     =   "0"
         Top             =   315
         Value           =   -1  'True
         Width           =   690
      End
      Begin VB.Label Label2 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5445
         TabIndex        =   11
         Tag             =   "26"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   645
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Vitesse :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3585
         TabIndex        =   10
         Tag             =   "25"
         ToolTipText     =   "0"
         Top             =   345
         Width           =   930
      End
   End
   Begin VB.Label Label_Compteur 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   345
      Left            =   4320
      TabIndex        =   13
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   2280
      Width           =   660
   End
   Begin VB.Label Label_Aide 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Aide"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1260
      Left            =   75
      TabIndex        =   12
      Tag             =   "10"
      Top             =   105
      Width           =   5970
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Compteur :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3000
      TabIndex        =   9
      Tag             =   "32"
      ToolTipText     =   "0"
      Top             =   2400
      Width           =   1185
   End
End
Attribute VB_Name = "frmPasPerdus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Cmd_Depl_Click(Index As Integer)
    Dim P1 As Point, P2 As Point
    
    If Index = 0 Then Label_Compteur = 0
    If Index = 1 Then Label_Compteur = Label_Compteur + 1
    
    Dim iIncrement As Integer
    
    Dim Vitesse As Single
    Dim Dist As Double
    Dim TpsReel As Single
    Dim XD As Double
    Dim YD As Double
    Dim XG As Double
    Dim YG As Double
    
    If frmParamMachine.TB_SAV(12) = "" Or frmParamMachine.TB_SAV(13) = "" Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 3), vbQuestion + vbYesNo, App.Title
        Exit Sub
    End If
    
    If Index = 1 Then                   '- 1 pas
        Dist = -1 * frmParamMachine.TB_SAV(0) / frmParamMachine.TB_SAV(3)
    Else                                '10 mm
        Dist = 10
    End If
        
    XD = 0#
    YD = 0#
    XG = 0#
    YG = 0#
    If Option_Axe(0).value Then XG = Dist
    If Option_Axe(1).value Then XD = Dist
    If Option_Axe(2).value Then YG = Dist
    If Option_Axe(3).value Then YD = Dist
    
    Vitesse = TB_Vitesse
    P1.x = 0#
    P1.y = 0#
    P2.x = IIf(XG <> 0#, XG, XD)
    P2.y = IIf(YG <> 0#, YG, YD)
    Call maClasseHID.MotorOnOff(True)
    Call maClasseHID.EnvoiUSB
    Call maClasseHID.CommandeDATAmm(XD, YD, XG, YG, Calcul_Temps_Points(P1, P2, Vitesse), TpsReel)
    Call maClasseHID.CommandeDATAend
End Sub

Private Sub CMD_Memoriser_Click()
    Dim i As Integer
    Dim Index As Integer
    
    For i = Option_Axe.LBound To Option_Axe.UBound
        If Option_Axe(i).value = True Then
            Index = i
        End If
    Next i
    Select Case Index
        Case 0      'XG
            frmParamMachine.TB_SAV(9) = Label_Compteur - 1
        Case 1      'XD
            frmParamMachine.TB_SAV(31) = Label_Compteur - 1
        Case 2      'YG
            frmParamMachine.TB_SAV(10) = Label_Compteur - 1
        Case 3      'YD
            frmParamMachine.TB_SAV(32) = Label_Compteur - 1
        Case 4      'Z
            frmParamMachine.TB_SAV(11) = Label_Compteur - 1
    End Select
End Sub

Private Sub Cmd_RAZ_Click()
    Label_Compteur = 0
End Sub

Private Sub Form_Load()
    Call ChargeTrad(Me)
    Set gFenetreHID = frmParamMachine
    Call maClasseHID.TableVerif
    Me.Top = frmParamMachine.Top
    Me.Left = frmParamMachine.Left
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call maClasseHID.MotorOnOff(False)
End Sub

Private Sub TB_Vitesse_Change()
    If TB_Vitesse = "" Then Exit Sub
    TB_Vitesse = Round(TB_Vitesse, 2)
End Sub

Private Sub TB_Vitesse_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub
