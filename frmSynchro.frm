VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSynchro 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "frmSynchro"
   ClientHeight    =   7530
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   11775
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   11775
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.CommandButton CancelButton 
      Caption         =   "Abandon"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8880
      TabIndex        =   18
      Tag             =   "73"
      ToolTipText     =   "tt73"
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton CB_SupprPtsInutiles 
      Caption         =   "Suppression points inutiles"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   16
      Tag             =   "102"
      ToolTipText     =   "tt102"
      Top             =   0
      Width           =   2610
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8235
      TabIndex        =   15
      Tag             =   "70"
      ToolTipText     =   "tt70"
      Top             =   0
      Width           =   615
   End
   Begin VB.CommandButton RAZButton 
      Caption         =   "RAZ"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7140
      TabIndex        =   14
      Tag             =   "69"
      ToolTipText     =   "tt69"
      Top             =   0
      Width           =   1095
   End
   Begin VB.CommandButton SynchroButton 
      Caption         =   "Synchronisation"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5220
      TabIndex        =   13
      Tag             =   "68"
      ToolTipText     =   "tt68"
      Top             =   0
      Width           =   1920
   End
   Begin VB.PictureBox PB_Help 
      Height          =   330
      Left            =   11040
      Picture         =   "frmSynchro.frx":0000
      ScaleHeight     =   270
      ScaleWidth      =   255
      TabIndex        =   7
      ToolTipText     =   "tt60"
      Top             =   0
      Width           =   315
   End
   Begin VB.Frame Frame_Profil 
      Caption         =   "Saumon"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3165
      Index           =   2
      Left            =   45
      TabIndex        =   4
      Tag             =   "17"
      ToolTipText     =   "0"
      Top             =   4050
      Width           =   10215
      Begin VB.HScrollBar HScroll 
         Height          =   255
         Index           =   2
         Left            =   3240
         TabIndex        =   10
         Top             =   1200
         Width           =   2295
      End
      Begin VB.VScrollBar VScroll 
         Height          =   2655
         Index           =   2
         Left            =   2655
         TabIndex        =   9
         Top             =   240
         Width           =   255
      End
      Begin VB.PictureBox PB_Synchro 
         AutoRedraw      =   -1  'True
         Height          =   2775
         Index           =   2
         Left            =   2910
         ScaleHeight     =   2715
         ScaleWidth      =   6990
         TabIndex        =   5
         Tag             =   "0"
         ToolTipText     =   "tt67"
         Top             =   240
         Width           =   7050
      End
      Begin MSComctlLib.ListView LV_Synchro 
         Height          =   2775
         Index           =   2
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   4895
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "61"
            Text            =   "__"
            Object.Width           =   441
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Object.Tag             =   "62"
            Text            =   "Pt"
            Object.Width           =   794
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "63"
            Text            =   "X"
            Object.Width           =   1341
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "64"
            Text            =   "Y"
            Object.Width           =   1341
         EndProperty
      End
   End
   Begin VB.Frame Frame_Profil 
      Caption         =   "Emplanture"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3165
      Index           =   1
      Left            =   360
      TabIndex        =   1
      Tag             =   "13"
      ToolTipText     =   "0"
      Top             =   840
      Width           =   10215
      Begin VB.HScrollBar HScroll 
         Height          =   255
         Index           =   1
         Left            =   3240
         TabIndex        =   12
         Top             =   2280
         Width           =   2295
      End
      Begin VB.VScrollBar VScroll 
         Height          =   2655
         Index           =   1
         Left            =   2655
         TabIndex        =   11
         Top             =   240
         Width           =   255
      End
      Begin VB.PictureBox PB_Synchro 
         AutoRedraw      =   -1  'True
         Height          =   2775
         Index           =   1
         Left            =   2910
         ScaleHeight     =   2715
         ScaleWidth      =   7140
         TabIndex        =   3
         Tag             =   "0"
         ToolTipText     =   "tt67"
         Top             =   240
         Width           =   7200
      End
      Begin MSComctlLib.ListView LV_Synchro 
         Height          =   2775
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   4895
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   0
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "61"
            Text            =   "__"
            Object.Width           =   441
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Object.Tag             =   "62"
            Text            =   "Pt"
            Object.Width           =   794
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "63"
            Text            =   "X"
            Object.Width           =   1341
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "64"
            Text            =   "Y"
            Object.Width           =   1341
         EndProperty
      End
   End
   Begin VB.CommandButton CB_Synchro 
      Caption         =   "Points de synchro auto."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2610
      TabIndex        =   0
      Tag             =   "65"
      ToolTipText     =   "tt65"
      Top             =   0
      Width           =   2610
   End
   Begin MSComctlLib.Slider SliderZoom 
      Height          =   375
      Left            =   10440
      TabIndex        =   8
      Tag             =   "71"
      ToolTipText     =   "tt71"
      Top             =   0
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      MousePointer    =   99
      MouseIcon       =   "frmSynchro.frx":014A
      LargeChange     =   1
      Min             =   1
      Max             =   5
      SelStart        =   1
      TickStyle       =   1
      Value           =   1
   End
   Begin MSComctlLib.StatusBar SB1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   19
      Top             =   7275
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   9
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel9 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label_Zoom 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Zoom :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9600
      TabIndex        =   17
      Tag             =   "66"
      ToolTipText     =   "0"
      Top             =   30
      Width           =   855
   End
   Begin VB.Menu Mnu_Ctx 
      Caption         =   "1001"
      Visible         =   0   'False
      Begin VB.Menu mnuCtx_PtCle 
         Caption         =   "1002"
      End
      Begin VB.Menu mnuCtx_Origine 
         Caption         =   "1003"
      End
   End
End
Attribute VB_Name = "frmSynchro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Largeur(EMPLANTURE To SAUMON) As Integer
Dim Hauteur(EMPLANTURE To SAUMON) As Integer
Dim Gauche(EMPLANTURE To SAUMON) As Integer
Dim Haut(EMPLANTURE To SAUMON) As Integer

Dim monIndex As Integer
Dim Zoom As Integer
Dim bModif As Boolean

Dim frmParent As String

Public Property Let CallingForm(nom As String)
    'Nom du formulaire appelant : sert pour autoriser ou non le bouton "Abandon"
    frmParent = nom
End Property

Private Sub CancelButton_Click()
    Unload Me
    ReDim gTabCoord(0 To 0)
    frmMain.Form_Resize
End Sub

Private Sub CB_SupprPtsInutiles_Click()
    Dim k As Integer
    'Suppression des points en double et des points align�s
    For k = EMPLANTURE To SAUMON
        gListePointSynchro(k).SupprPtsInutiles
    Next k
    
    RAZButton_Click
End Sub

Private Sub Form_Activate()
    Dim r As Long
    Const HWND_TOPMOST = -1
    Const HWND_NOTOPMOST = -2
    Const SWP_NOMOVE = &H2
    Const SWP_NOSIZE = &H1

    r = SetWindowPos(Me.hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
End Sub

Private Sub Form_Load()
    Dim k As Integer
    Dim i As Long
        
    Zoom = 1
    
    ChargeTrad Me
    
    CancelButton.Visible = (frmParent = "frmImporter")
    
    OKButton.Enabled = False
    
    bModif = True
    
    'Lecture de la valeur "CouleurEmplanture" : Couleur du trac� de l'emplanture
    Couleur(EMPLANTURE) = Val(ReadINI_Num("CouleurEmplanture", RGB(255, 0, 0)))
    'Lecture de la valeur "CouleurSaumon" : Couleur du trac� du saumon
    Couleur(SAUMON) = Val(ReadINI_Num("CouleurSaumon", RGB(0, 0, 255)))
    'Lecture de la valeur "CouleurFleche" : Couleur de la fl�che du sens
    CouleurFleche = Val(ReadINI_Num("CouleurFleche", RGB(127, 0, 127)))
    
    For k = EMPLANTURE To SAUMON
        LV_Synchro(k).ForeColor = Couleur(k)
        Frame_Profil(k).ForeColor = Couleur(k)
        VScroll(k).value = 0
        HScroll(k).value = 0
    Next k
    
    If gListePointSynchro(EMPLANTURE).count = gListePointSynchro(SAUMON).count Then
        OKButton.Enabled = True
    End If
    
    Call RemplLV
    
    With Me
        Left = ReadINI_Num(Me.Name & "_Left", Me.Left)
        Top = ReadINI_Num(Me.Name & "_Top", Me.Top)
        Width = ReadINI_Num(Me.Name & "_Width", Me.Width)
        Height = ReadINI_Num(Me.Name & "_Height", Me.Height)
    End With
End Sub

Private Sub CB_Synchro_Click()
    frmSynchroOptions.Show vbModal
    SynchroButton.Enabled = True
End Sub

Public Sub Form_Resize()
    Dim P1 As Point
    Dim P2 As Point
    Dim PDepart As Point
    Dim pFleche As Point
    Dim i As Long
    Dim k As Integer
    Dim x1 As Double, y1 As Double, x2 As Double, y2 As Double
    Dim DeltaEpsilon As Double
    Dim Re As Double, Rd As Double
    Dim CentrageX As Double
    Dim CentrageY As Double
    Dim bSynchro As Boolean
    Dim bTemp As Boolean
    Dim NumSynchro As Long
    
    If Not bProfilCharge Then Exit Sub
    
    DeltaEpsilon = 0.1
    
    PB_Help.Left = Me.ScaleWidth - PB_Help.Width
    ' GetSystemMetrics% (SM_CYCAPTION)=4
    'CYFRAME=33
    'PB_Help.Top = GetSystemMetrics(4) + GetSystemMetrics(33)
    PB_Help.Top = GetSystemMetrics(4)
    SliderZoom.Left = PB_Help.Left - SliderZoom.Width
    Label_Zoom.Left = SliderZoom.Left - Label_Zoom.Width
    
    Frame_Profil(EMPLANTURE).Top = CB_Synchro.Top + CB_Synchro.Height
    'CYFULLSCREEN=17
    'CYFRAME=33
    Frame_Profil(SAUMON).Top = (Me.ScaleHeight - CB_Synchro.Height) / 2 + 250
    
    Dim x As Double
    Dim y As Double
    Dim X1Zoom As Double
    Dim X2Zoom As Double
    Dim Y1Zoom As Double
    Dim Y2Zoom As Double
    Dim PC As Double
    
    For k = EMPLANTURE To SAUMON
        PB_Synchro(k).Picture = LoadPicture   'Effacement de la PictureBox
    
        Frame_Profil(k).Left = 0
        Frame_Profil(k).Height = (Me.ScaleHeight - SB1.Height - CB_Synchro.Height) / 2
        Frame_Profil(k).Width = Me.ScaleWidth
        LV_Synchro(k).Height = Frame_Profil(k).Height - LV_Synchro(k).Top - 100
        Largeur(k) = Frame_Profil(k).Width - LV_Synchro(k).Width - VScroll(k).Width - 200
        If Largeur(k) < 0 Then Exit Sub
        Hauteur(k) = Frame_Profil(k).Height - HScroll(k).Height - 350
        If Hauteur(k) < 0 Then Exit Sub
        PB_Synchro(k).Width = Largeur(k)
        PB_Synchro(k).Height = Hauteur(k)
        VScroll(k).Height = LV_Synchro(k).Height - HScroll(k).Height
        HScroll(k).Top = VScroll(k).Top + VScroll(k).Height
        HScroll(k).Left = VScroll(k).Left + VScroll(k).Width
        HScroll(k).Width = PB_Synchro(k).Width
        
        x1 = gListePointSynchro(k).minX
        x2 = gListePointSynchro(k).maxX
        y1 = gListePointSynchro(k).minY
        y2 = gListePointSynchro(k).maxY
        x1 = x1 - DeltaEpsilon * (x2 - x1)
        y1 = y1 - DeltaEpsilon * (y2 - y1)
        x2 = x2 + DeltaEpsilon * (x2 - x1)
        y2 = y2 + DeltaEpsilon * (y2 - y1)
        
        'Calcul du zoom
        X2Zoom = ((x2 - x1) / Zoom + x2 + x1) / 2
        X1Zoom = x2 + x1 - X2Zoom
        Y2Zoom = ((y2 - y1) / Zoom + y2 + y1) / 2
        Y1Zoom = y2 + y1 - Y2Zoom
        
        'Dimensionnement et placement des dessins

        'Calcul du rapport largeur/hauteur des dessins :
        Rd = (X2Zoom - X1Zoom) / (Y2Zoom - Y1Zoom)
        'Calcul du rapport largeur/hauteur de la PictureBox :
        Re = PB_Synchro(k).Width / PB_Synchro(k).Height
                
        'Calcul de la Scale
        If Rd > Re Then
            PB_Synchro(k).ScaleLeft = X1Zoom
            PB_Synchro(k).ScaleWidth = X2Zoom - X1Zoom
            PB_Synchro(k).ScaleHeight = -(Y2Zoom - Y1Zoom) * (Rd / Re)
            PB_Synchro(k).ScaleTop = Y2Zoom
        Else
            PB_Synchro(k).ScaleLeft = X1Zoom
            PB_Synchro(k).ScaleWidth = (X2Zoom - X1Zoom) / (Rd / Re)
            PB_Synchro(k).ScaleHeight = -(Y2Zoom - Y1Zoom)
            PB_Synchro(k).ScaleTop = Y2Zoom
        End If
        'Ascenseurs
        bModif = False
        PC = HScroll(k).value / (HScroll(k).Max - HScroll(k).Min)
        HScroll(k).Min = -(gListePointSynchro(k).maxX - gListePointSynchro(k).minX) * Zoom
        HScroll(k).Max = gListePointSynchro(k).maxX - gListePointSynchro(k).minX * Zoom
        HScroll(k).value = PC * (HScroll(k).Max - HScroll(k).Min)
        PC = VScroll(k).value / (VScroll(k).Max - VScroll(k).Min)
        VScroll(k).Min = -(gListePointSynchro(k).maxY - gListePointSynchro(k).minY) * Zoom
        VScroll(k).Max = gListePointSynchro(k).maxY - gListePointSynchro(k).minY * Zoom
        VScroll(k).value = PC * (VScroll(k).Max - VScroll(k).Min)
        bModif = True

        CentrageX = PB_Synchro(k).ScaleWidth / 2 - (X2Zoom - X1Zoom) / 2
        CentrageY = PB_Synchro(k).ScaleHeight / 2 + (Y2Zoom - Y1Zoom) / 2
        PB_Synchro(k).ScaleLeft = PB_Synchro(k).ScaleLeft - CentrageX + HScroll(k).value
        PB_Synchro(k).ScaleTop = PB_Synchro(k).ScaleTop - CentrageY - VScroll(k).value
    Next k
    
    For k = EMPLANTURE To SAUMON
        gListePointSynchro(k).MoveFirst
        P1 = gListePointSynchro(k).CurrentPoint
        PDepart = P1
        i = 1
        While gListePointSynchro(k).MoveNext
            i = i + 1
            bTemp = gListePointSynchro(k).CurrentbTemp
            P2 = gListePointSynchro(k).CurrentPoint
            If i = 1 Then
                pFleche = P2
            End If
            If PDepart.x = pFleche.x And _
                PDepart.y = pFleche.y Then
                pFleche = P1
            Else
                'Trac� des fl�ches de sens
                Call PB_Fleche(PB_Synchro(k), PDepart, pFleche, CouleurFleche)
            End If
            If i = Round(gListePointSynchro(k).count / 2, 0) Then
                PDepart = P1
                pFleche = P1
            End If
            Call PB_Cercle(PB_Synchro(k), _
                P1, _
                Couleur(k), _
                Couleur(3 - k), _
                False, _
                LV_Synchro(k).ListItems(i - 1).Selected, _
                bTemp, _
                bSynchro, _
                0)
            
            PB_Synchro(k).Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k)
            P1 = P2
        Wend
    Next k
    'Dessin des points de synchro
    For k = EMPLANTURE To SAUMON
        gListePointSynchro(k).MoveFirst
        P1 = gListePointSynchro(k).CurrentPoint
        NumSynchro = 0
        Do
            If gListePointSynchro(k).CurrentbSynchro Then
                NumSynchro = NumSynchro + 1
                Call PB_Cercle(PB_Synchro(k), _
                    gListePointSynchro(k).CurrentPoint, _
                    Couleur(k), _
                    Couleur(3 - k), _
                    True, _
                    False, _
                    bTemp, _
                    True, _
                    NumSynchro)
            End If
        Loop Until Not gListePointSynchro(k).MoveNext
    Next k
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
        Call WriteINI(Me.Name & "_Width", Me.Width)
        Call WriteINI(Me.Name & "_Height", Me.Height)
    End If
End Sub

Private Sub HScroll_Change(Index As Integer)
    If bModif Then
        Call Form_Resize
    End If
End Sub

Private Sub HScroll_Scroll(Index As Integer)
    Call Form_Resize
End Sub

Private Sub LV_Synchro_ItemCheck(Index As Integer, ByVal Item As MSComctlLib.ListItem)
    gListePointSynchro(Index).Synchro Item.Index, Item.Checked
    Call Form_Resize
End Sub

Private Sub LV_Synchro_ItemClick(Index As Integer, ByVal Item As MSComctlLib.ListItem)
    Dim kBis As Integer
    'Synchro avec l'autre ListView
    kBis = 3 - Index
    On Error Resume Next    'Si les listes n'ont pas le m�me nombre de points
    LV_Synchro(kBis).ListItems(Item.Index).EnsureVisible
    Set LV_Synchro(kBis).SelectedItem = LV_Synchro(kBis).ListItems(Item.Index)
    
    'On clique sur un point, on affiche une croix sur le graphique
    Call Form_Resize
End Sub

Private Sub LV_Synchro_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton Then
        monIndex = Index
        PopupMenu mnu_ctx
    End If
End Sub

Private Sub mnuCtx_Origine_Click()
    gListePointSynchro(monIndex).ChangeOrigine LV_Synchro(monIndex).SelectedItem.Index
    Call RemplLV
    Call Form_Resize
End Sub

Private Sub mnuCtx_PtCle_Click()
    Dim i As Long
    
    'Mise � jour de gListePointSynchro
    'Trouver quel point est s�lectionn� dans la ListView
    i = LV_Synchro(monIndex).SelectedItem.Index
    LV_Synchro(monIndex).ListItems(i).Checked = Not LV_Synchro(monIndex).ListItems(i).Checked
    gListePointSynchro(monIndex).Synchro i, LV_Synchro(monIndex).SelectedItem.Checked
    Call Form_Resize
End Sub

Private Sub OKButton_Click()
    'On va recopier la liste chain�e dans gTabCoord
    Dim lTaille As Long
    Dim i As Long   'Parcours de gTabCoord
    Dim k As Integer
    
    lTaille = Max(gListePointSynchro(EMPLANTURE).count, gListePointSynchro(SAUMON).count)
    ReDim gTabCoord(0 To lTaille - 1)
    
    For k = EMPLANTURE To SAUMON
        i = 0
        gListePointSynchro(k).MoveFirst
        Do
            gTabCoord(i).P(k).x = gListePointSynchro(k).CurrentX
            gTabCoord(i).P(k).y = gListePointSynchro(k).CurrentY
            i = i + 1
        Loop Until Not gListePointSynchro(k).MoveNext
    Next k
    
    Unload Me
    frmMain.mnuFileCut.Enabled = True
    frmMain.Form_Resize
End Sub


Private Sub PB_Synchro_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    'Recherche si on a cliqu� sur un point
    Dim i As Long
    Dim MinDist As Double
    Dim maDist As Double
    Dim maDistMini As Double
    Dim P As Point
    Dim minI As Long
    
    maDistMini = Sqr((PB_Synchro(Index).ScaleWidth * PB_Synchro(Index).ScaleWidth) + _
        (PB_Synchro(Index).ScaleHeight + PB_Synchro(Index).ScaleHeight)) / 150
    
    P.x = x
    P.y = y
    gListePointSynchro(Index).MoveFirst
    minI = 1
    i = 1
    MinDist = Calcul_Distance(P, gListePointSynchro(Index).CurrentPoint)
    
    While gListePointSynchro(Index).MoveNext
        i = i + 1
        maDist = Calcul_Distance(P, gListePointSynchro(Index).CurrentPoint)
        If maDist < MinDist Then
            MinDist = maDist
            minI = i
        End If
    Wend
    
    If MinDist > maDistMini Then Exit Sub
    If Button = vbRightButton Then
        LV_Synchro(Index).ListItems(minI).Checked = Not LV_Synchro(Index).ListItems(minI).Checked
        gListePointSynchro(Index).Synchro minI, LV_Synchro(Index).ListItems(minI).Checked
    End If
    Set LV_Synchro(Index).SelectedItem = LV_Synchro(Index).ListItems(minI)
    LV_Synchro(Index).ListItems(minI).EnsureVisible
    OKButton.Enabled = False
    Call Form_Resize
End Sub

Private Sub PB_Synchro_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    'Recherche si on est pr�s d'un point pour afficher son n�
    Dim i As Long
    Dim MinDist As Double
    Dim maDist As Double
    Dim maDistMini As Double
    Dim P As Point
    Dim minI As Long
    
    maDistMini = Sqr((PB_Synchro(Index).ScaleWidth * PB_Synchro(Index).ScaleWidth) + (PB_Synchro(Index).ScaleHeight * PB_Synchro(Index).ScaleHeight)) / 150
    
    P.x = x
    P.y = y
    gListePointSynchro(Index).MoveFirst
    MinDist = Calcul_Distance(P, gListePointSynchro(Index).CurrentPoint)
    minI = 1
    i = 1
    
    While gListePointSynchro(Index).MoveNext
        i = i + 1
        maDist = Calcul_Distance(P, gListePointSynchro(Index).CurrentPoint)
        If maDist < MinDist Then
            MinDist = maDist
            minI = i
        End If
    Wend
    If MinDist <= maDistMini Then
        PB_Synchro(Index).ToolTipText = minI
    Else
        PB_Synchro(Index).ToolTipText = ""
    End If
End Sub

Private Sub RAZButton_Click()
    Dim k As Integer
    
    'RAZ des synchro
    For k = EMPLANTURE To SAUMON
        gListePointSynchro(k).RAZPtCles
    Next k
    
    OKButton.Enabled = False
    Call RemplLV
    Call Form_Resize
    SynchroButton.Enabled = True
End Sub

Private Sub SliderZoom_Change()
    Zoom = SliderZoom.value
    Call Form_Resize
End Sub

Private Sub SynchroButton_Click()
    Dim i As Long
    Dim j As Long
    Dim iDeb(EMPLANTURE To SAUMON) As Long
    Dim iFin(EMPLANTURE To SAUMON) As Long
    Dim iSegment As Long
    Dim k As Integer
    Dim monK As Integer
    Dim NbPtCles(EMPLANTURE To SAUMON) As Long
    Dim P As Point
    Dim P1 As Point
    Dim P2 As Point
    Dim PRet1 As Point
    Dim PRet2 As Point
    Dim maDist As Double
    Dim monRayon As Double
    Dim Rapport(EMPLANTURE To SAUMON) As Double
    Dim EPSILON(EMPLANTURE To SAUMON) As Double
    Dim monNoeud As TypeNoeud
    Dim InterDist As Single
    Dim Dist(EMPLANTURE To SAUMON) As Double
    Dim Point1(EMPLANTURE To SAUMON) As Point
    Dim Point2(EMPLANTURE To SAUMON) As Point
    
    'Recherche des points cl�s dans les listes cha�n�es
    'On doit avoir le m�me nombre (>=2) de points cl�s sur chaque profil
    For k = EMPLANTURE To SAUMON
        NbPtCles(k) = 0
        gListePointSynchro(k).MoveFirst
        Do
            NbPtCles(k) = NbPtCles(k) + IIf(gListePointSynchro(k).CurrentbSynchro, 1, 0)
        Loop While gListePointSynchro(k).MoveNext
        
        'Calcul de l'epsilon
        EPSILON(k) = (gListePointSynchro(k).maxX - gListePointSynchro(k).minX) / 1000#
'        EPSILON(k) = 0#
        iFin(k) = 1
    Next k
    
    If NbPtCles(EMPLANTURE) <> NbPtCles(SAUMON) Then    'Nbres de points cl�s diff�rents
        MsgBox ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 5), vbCritical
        Exit Sub
    End If
    If NbPtCles(EMPLANTURE) < 2 Then                    'Nbre de points cl�s < 2
        MsgBox ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 6), vbCritical
        Exit Sub
    End If
    
    For iSegment = 1 To NbPtCles(EMPLANTURE) - 1 'Boucle des segments
        'On calcule les distances
        For k = EMPLANTURE To SAUMON    'Boucle des profils
            iDeb(k) = iFin(k)
            InterDist = 0#
            gListePointSynchro(k).MoveIndex iDeb(k)
            gListePointSynchro(k).Dist 0#
            P1 = gListePointSynchro(k).CurrentPoint
            Do
                gListePointSynchro(k).MoveNext
                iFin(k) = iFin(k) + 1
                P2 = gListePointSynchro(k).CurrentPoint
                InterDist = Calcul_Distance(P1, P2) + InterDist
                gListePointSynchro(k).Dist InterDist
                P1 = P2
            Loop While Not gListePointSynchro(k).CurrentbSynchro
            Dist(k) = InterDist
        Next k
        Rapport(EMPLANTURE) = Dist(EMPLANTURE) / Dist(SAUMON)
        Rapport(SAUMON) = 1# / Rapport(EMPLANTURE)
        
        'Parcours des deux segments pour l'emplanture et le saumon
        For k = EMPLANTURE To SAUMON    'Boucle des profils
            gListePointSynchro(k).MoveIndex iDeb(k) + 1
        Next k
        While Not (gListePointSynchro(EMPLANTURE).CurrentbSynchro And _
                   gListePointSynchro(SAUMON).CurrentbSynchro)
            Dist(EMPLANTURE) = gListePointSynchro(EMPLANTURE).CurrentDist
            Dist(SAUMON) = gListePointSynchro(SAUMON).CurrentDist
            If Abs(Dist(EMPLANTURE) / Rapport(EMPLANTURE) - Dist(SAUMON)) > EPSILON(SAUMON) Then
                If Dist(EMPLANTURE) / Rapport(EMPLANTURE) < Dist(SAUMON) Then
                    monK = SAUMON       'On cr�e dans le saumon
                    monRayon = Dist(EMPLANTURE) / Rapport(EMPLANTURE) - Dist(SAUMON)
                Else
                    monK = EMPLANTURE   'On cr�e dans l'emplanture
                    monRayon = Dist(SAUMON) / Rapport(SAUMON) - Dist(EMPLANTURE)
                End If
                
                P1 = gListePointSynchro(monK).PreviousPoint
                P2 = gListePointSynchro(monK).CurrentPoint
                'Le point P est situ� entre les points P1 et P2
                
                Call Calcul_Inter_Droite_Cercle(P1, P2, P2, monRayon, PRet1, PRet2)
                'Il faut ensuite trouver le point compris entre p1 et p2
                If Calcul_Distance(P1, PRet1) + Calcul_Distance(P2, PRet1) < _
                    Calcul_Distance(P1, PRet2) + Calcul_Distance(P2, PRet2) Then
                    P = PRet1
                Else
                    P = PRet2
                End If
                monNoeud.x = P.x
                monNoeud.y = P.y
                monNoeud.bSynchro = False
                monNoeud.bTemp = True
                gListePointSynchro(monK).Insert monNoeud
                iFin(monK) = iFin(monK) + 1
            End If      ' < EPSILON
            For k = EMPLANTURE To SAUMON    'Boucle des profils
                If Not gListePointSynchro(k).CurrentbSynchro Then
                    gListePointSynchro(k).MoveNext
                End If
            Next k
        Wend
    Next iSegment
    
    OKButton.Enabled = True
    Call RemplLV
    Call Form_Resize
    SynchroButton.Enabled = False
End Sub

Private Sub VScroll_Change(Index As Integer)
    If bModif Then
        Call Form_Resize
    End If
End Sub

Private Sub VScroll_Scroll(Index As Integer)
    Call Form_Resize
End Sub

Public Sub RemplLV()
    Dim i As Integer
    Dim k As Integer
    Dim mItem As Variant
    
    For k = EMPLANTURE To SAUMON
        LV_Synchro(k).ListItems.Clear
        gListePointSynchro(k).MoveFirst
        For i = 1 To gListePointSynchro(k).count
            LV_Synchro(k).ListItems.Add
            LV_Synchro(k).ListItems(i).SubItems(1) = Format$(i, "###")
            LV_Synchro(k).ListItems(i).SubItems(2) = Format$(gListePointSynchro(k).CurrentX, "0.000")
            LV_Synchro(k).ListItems(i).SubItems(3) = Format$(gListePointSynchro(k).CurrentY, "0.000")
            LV_Synchro(k).ListItems(i).Checked = gListePointSynchro(k).CurrentbSynchro
            If gListePointSynchro(k).CurrentbTemp Then
                Set mItem = LV_Synchro(k).ListItems(i)
                mItem.Bold = True
                mItem.ListSubItems(1).ForeColor = &H0
                mItem.ListSubItems(2).ForeColor = &H0
                mItem.ListSubItems(3).ForeColor = &H0
            End If

            gListePointSynchro(k).MoveNext
        Next i
    Next k
End Sub
