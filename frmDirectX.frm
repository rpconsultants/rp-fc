VERSION 5.00
Begin VB.Form frmDirectX 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "-X"
      Height          =   315
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "+X"
      Height          =   315
      Left            =   105
      TabIndex        =   0
      Top             =   105
      Width           =   1215
   End
End
Attribute VB_Name = "frmDirectX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const FVF_TLVERTEX = (D3DFVF_XYZRHW Or D3DFVF_TEX1 Or D3DFVF_DIFFUSE Or D3DFVF_SPECULAR)
Const FVF_LVERTEX = (D3DFVF_XYZ Or D3DFVF_DIFFUSE Or D3DFVF_SPECULAR Or D3DFVF_TEX1)
Const FVF_VERTEX = (D3DFVF_XYZ Or D3DFVF_NORMAL Or D3DFVF_TEX1)

Dim Dx As DirectX8                'The master Object, everything comes from here
Dim D3D As Direct3D8              'This controls all things 3D
Dim D3DDevice As Direct3DDevice8  'This actually represents the hardware doing the rendering
Dim bRunning As Boolean           'Controls whether the program is running or not...

Private Type TLVERTEX
  X As Single
  Y As Single
  Z As Single
  rhw As Single
  color As Long
  Specular As Long
  tu As Single
  tv As Single
End Type

Private Type LITVERTEX
  X As Single
  Y As Single
  Z As Single
  color As Long
  Specular As Long
  tu As Single
  tv As Single
End Type

Private Type VERTEX
  X As Single
  Y As Single
  Z As Single
  nx As Single
  ny As Single
  nz As Single
  tu As Single
  tv As Single
End Type




Public Function Initialize() As Boolean
On Error GoTo ErrHandler:


Initialize = True '//We succeeded
Exit Function

ErrHandler:
Debug.Print "Error Number Returned: " & Err.Number, Err.Description
Initialize = False
End Function

Private Sub InitializeGeometry()

    Dim TriVert(0 To 2) As TLVERTEX '//we require 3 vertices to make a triangle...
    D3DDevice.SetVertexShader FVF_TLVERTEX
    D3DDevice.SetRenderState D3DRS_LIGHTING, 0

    TriVert(0) = CreateTLVertex(0, 0, 0, 1, &HFF0000, 0, 0, 0)
    TriVert(1) = CreateTLVertex(175, 0, 0, 1, &HFF00&, 0, 0, 0)
    TriVert(2) = CreateTLVertex(0, 175, 0, 1, &HFF&, 0, 0, 0)
End Sub

Private Function CreateTLVertex(X As Single, Y As Single, Z As Single, _
                                rhw As Single, color As Long, _
                                Specular As Long, tu As Single, _
                                tv As Single) As TLVERTEX
  CreateTLVertex.X = X
  CreateTLVertex.Y = Y
  CreateTLVertex.Z = Z
  CreateTLVertex.rhw = rhw
  CreateTLVertex.color = color
  CreateTLVertex.Specular = Specular
  CreateTLVertex.tu = tu
  CreateTLVertex.tv = tv
End Function

Public Sub Render()
    '//1. We need to clear the render device before we can draw anything
    '     This must always happen before you start rendering stuff...
    D3DDevice.Clear 0, ByVal 0, D3DCLEAR_TARGET, &HCCCCFF, 1#, 0
    'the hexidecimal value in the middle is the same as when you're using
    ' colours in HTML - if you're familiar with that.
    
    '//2. Next we would render everything. This example doesn't do this,
    '     but if it did it'd look something like this:
    
    D3DDevice.BeginScene
        'All rendering calls go between these two lines
    D3DDevice.EndScene
    
    '//3. Update the frame to the screen...
    '     This is the same as the Primary.Flip method as used in DirectX 7
    '     These values below should work for almost all cases...
    D3DDevice.Present ByVal 0, ByVal 0, 0, ByVal 0
End Sub



Private Sub Form_Load()
Me.Show '//Make sure our window is visible

bRunning = Initialize()
'So you can see what happens...
Debug.Print "Device Creation Return Code : ", bRunning

Do While bRunning
  Render    '//Update the frame...
  DoEvents  '//Allow windows time to think; otherwise you'll
            '//get into a really tight (and bad) loop...

Loop   '//Begin the next frame...

'//If we've gotten to this point the loop must have been terminated
'  So we need to clean up after ourselves. This isn't essential, but it's
'  good coding practise.

On Error Resume Next 'If the objects were never created;
'                     (the initialization failed) we might get an
'                     error when freeing them... which we need to
'                     handle, but as we're closing anyway...
Set D3DDevice = Nothing
Set D3D = Nothing
Set Dx = Nothing
Debug.Print "All Objects Destroyed"

'//Final termination:
Unload Me

End Sub
