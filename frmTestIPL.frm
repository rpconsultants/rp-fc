VERSION 5.00
Begin VB.Form frmTestIPL 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "test IPL5X"
   ClientHeight    =   7275
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4545
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7275
   ScaleWidth      =   4545
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.Frame Frame_KB 
      Caption         =   "Test Clavier"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   30
      Tag             =   "55"
      ToolTipText     =   "0"
      Top             =   5880
      Width           =   4410
      Begin VB.CommandButton Cmd_Touche_OK 
         Enabled         =   0   'False
         Height          =   495
         Left            =   3000
         Style           =   1  'Graphical
         TabIndex        =   36
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Touche_ESC 
         Enabled         =   0   'False
         Height          =   495
         Left            =   3000
         Style           =   1  'Graphical
         TabIndex        =   35
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Touche_RIGHT 
         Enabled         =   0   'False
         Height          =   495
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   34
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Touche_LEFT 
         Enabled         =   0   'False
         Height          =   495
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   33
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Touche_DOWN 
         Enabled         =   0   'False
         Height          =   495
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   32
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Touche_UP 
         Enabled         =   0   'False
         Height          =   495
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   31
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.Timer Timer_Affichage 
      Enabled         =   0   'False
      Interval        =   300
      Left            =   1440
      Top             =   1200
   End
   Begin VB.Timer Timer_Refresh 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   360
      Top             =   3360
   End
   Begin VB.Frame Frame3 
      Caption         =   "Switches"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   1440
      TabIndex        =   23
      Tag             =   "50"
      ToolTipText     =   "0"
      Top             =   4440
      Width           =   3090
      Begin VB.CheckBox CB_Fin 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   2760
         TabIndex        =   28
         ToolTipText     =   "0"
         Top             =   840
         Width           =   255
      End
      Begin VB.CheckBox CB_Origine 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   2760
         TabIndex        =   26
         ToolTipText     =   "0"
         Top             =   600
         Width           =   255
      End
      Begin VB.CheckBox CB_UtilSwitch 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   2760
         TabIndex        =   24
         ToolTipText     =   "0"
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label_Fin 
         Caption         =   "SW fin d�clench� ?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   29
         Tag             =   "53"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   2535
      End
      Begin VB.Label Label_Origine 
         Caption         =   "SW Origine d�clench� ?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   27
         Tag             =   "52"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   2535
      End
      Begin VB.Label Label5 
         Caption         =   "Utiliser les fins de course ?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   25
         Tag             =   "51"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   2535
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Test afficheur"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Left            =   120
      TabIndex        =   20
      Tag             =   "15"
      ToolTipText     =   "0"
      Top             =   960
      Width           =   4410
      Begin VB.CommandButton Cmd_TestAfficheur 
         Caption         =   "- - - - - - - - "
         Height          =   375
         Left            =   120
         TabIndex        =   21
         Tag             =   "0"
         ToolTipText     =   "tt15"
         Top             =   240
         Width           =   4095
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Tables"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   120
      TabIndex        =   8
      Tag             =   "40"
      ToolTipText     =   "0"
      Top             =   4440
      Width           =   1290
      Begin VB.Label Label_Table 
         Alignment       =   2  'Center
         Caption         =   "Table"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   12
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label_Table 
         Alignment       =   2  'Center
         Caption         =   "Table"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   11
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label_Table 
         Alignment       =   2  'Center
         Caption         =   "Table"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   10
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label_Table 
         Alignment       =   2  'Center
         Caption         =   "Table"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame Frame_Chauffe 
      Caption         =   "Chauffe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   120
      TabIndex        =   7
      Tag             =   "30"
      ToolTipText     =   "0"
      Top             =   3120
      Width           =   4410
      Begin VB.HScrollBar HScroll_Chauffe 
         Height          =   255
         LargeChange     =   10
         Left            =   120
         Max             =   255
         TabIndex        =   19
         Top             =   840
         Width           =   4170
      End
      Begin VB.Label Label_Chauffe 
         Alignment       =   2  'Center
         Caption         =   "Label5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   22
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   4215
      End
      Begin VB.Label Label_PWM_AUTO 
         Alignment       =   2  'Center
         Caption         =   "Auto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   15
         Tag             =   "32"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label_PWM_MANUAL 
         Alignment       =   2  'Center
         Caption         =   "Manuel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Tag             =   "33"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Version Firmware"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Left            =   120
      TabIndex        =   1
      Tag             =   "10"
      ToolTipText     =   "0"
      Top             =   120
      Width           =   4410
      Begin VB.Label Label_Version 
         Alignment       =   2  'Center
         Caption         =   "Version"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   13
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   4095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Flash"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1365
      Left            =   120
      TabIndex        =   0
      Tag             =   "20"
      ToolTipText     =   "0"
      Top             =   1680
      Width           =   4410
      Begin VB.CheckBox CB_Protection 
         Caption         =   "Check1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   3360
         TabIndex        =   6
         ToolTipText     =   "0"
         Top             =   960
         Width           =   255
      End
      Begin VB.Label Label_Capacite 
         Alignment       =   2  'Center
         Caption         =   "Capacit�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   18
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label_TYPE_MEMOIRE 
         Alignment       =   2  'Center
         Caption         =   "Type m�moire"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   17
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label_ID 
         Alignment       =   2  'Center
         Caption         =   "Manufacturer ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   16
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Protection �criture"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Tag             =   "24"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Capacit�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Tag             =   "23"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Type m�moire"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Tag             =   "22"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Manufacturer ID"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Tag             =   "21"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1815
      End
   End
End
Attribute VB_Name = "frmTestIPL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim maLigne As Integer
Dim maPos As Integer
Dim maLigneSav As Integer
Dim maPosSav As Integer
Dim bStop As Boolean
Dim Fichier As String

Private Sub Cmd_TestAfficheur_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    maLigne = 0
    maPos = 0
    maLigneSav = -1
    Timer_Refresh.Enabled = False
    Timer_Affichage.Enabled = True
    Call maClasseHID.DisplayEnable(False)
    Call maClasseHID.DisplayClear
End Sub

Private Sub Cmd_TestAfficheur_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Timer_Refresh.Enabled = True
    Timer_Affichage.Enabled = False
    Call maClasseHID.DisplayClear
    Call maClasseHID.DisplayEnable(True)
    Call maClasseHID.DisplaySend(maClasseHID.DemandeVersion, 0, 0)
    Call maClasseHID.DisplaySend("MODE USB", 1, 0)
End Sub

Private Sub Form_Load()
    Dim chauffe As String

    Me.Left = ReadINI_Num(Me.Name & "_Left", 0)
    Me.Top = ReadINI_Num(Me.Name & "_Top", 0)
    Call ChargeTrad(Me)
    
    Set gFenetreHID = frmTestIPL
    
    Fichier = ""
    maLigne = 0
    maPos = 0
    Timer_Affichage.Enabled = False
    
    'Version
    Label_Version = maClasseHID.DemandeVersion
    
    'Mod�le de Flash
    Dim maFlash As String
    maFlash = maClasseHID.FlashModel
    Label_ID = Hex$(Mid$(maFlash, 1, 2)) & "h"
    Label_TYPE_MEMOIRE = Hex$(Mid$(maFlash, 3, 2)) & "h"
    Label_Capacite = Hex$(Mid$(maFlash, 5, 2)) & "h"
    CB_Protection = IIf(Mid$(maFlash, 7, 2) <> "00", vbChecked, vbUnchecked)
    
    'Chauffe
    chauffe = maClasseHID.ChauffeLire
    Me.HScroll_Chauffe.value = Asc(Mid$(chauffe, 3, 1))
    
    'Tables
    'N� de la table par d�faut
    Dim iDefaut As Byte
    
    iDefaut = maClasseHID.TableDefautLire()
    
    Dim Index As Byte
    For Index = 0 To 3
        Label_Table(Index) = maClasseHID.TableLectureNom(Index)
        Label_Table(Index).FontBold = (Index = iDefaut)
    Next Index
    Timer_Refresh.Enabled = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Timer_Affichage.Enabled = False
    Timer_Refresh.Enabled = False
    bStop = True
    Call maClasseHID.DisplayEnable(True)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
    End If
End Sub

Private Sub HScroll_Chauffe_Change()
    Call maClasseHID.EnvoiUSB
    Call maClasseHID.GoBuffer
    Call maClasseHID.ChauffeEcrire(HScroll_Chauffe, False)
    Call maClasseHID.GoBuffer
End Sub

Private Sub Timer_Affichage_Timer()
    Dim ligne As Byte
    Dim Pos As Byte
    
    ligne = maLigne
    Pos = maPos
    Call maClasseHID.DisplaySend(Chr(255), ligne, Pos)
    If maLigneSav <> -1 Then
        ligne = maLigneSav
        Pos = maPosSav
        Call maClasseHID.DisplaySend(" ", ligne, Pos)
    End If
    maLigneSav = maLigne
    maPosSav = maPos
    If maLigne = 0 Then
        maPos = maPos + 1
    Else
        maPos = maPos - 1
    End If
    If maPos > 19 Then
        maPos = 19
        maLigne = 1
    End If
    If maPos < 0 Then
        maPos = 0
        maLigne = 0
    End If
End Sub

Private Sub Timer_Refresh_Timer()
    Call maClasseHID.DemandeInformation
    Label_PWM_AUTO.BackColor = IIf(maClasseHID.PWM_ON, RGB(255, 0, 0), &H8000000F)
    Label_PWM_MANUAL.BackColor = IIf(maClasseHID.PWM_ON, RGB(255, 0, 0), &H8000000F)
    Label_Chauffe.BackColor = IIf(maClasseHID.PWM_ON, RGB(255, 0, 0), &H8000000F)
    Frame_Chauffe.BackColor = IIf(maClasseHID.PWM_ON, RGB(255, 0, 0), &H8000000F)
    
    Label_PWM_MANUAL.Visible = maClasseHID.PWM_Manual
    Label_PWM_AUTO.Visible = Not maClasseHID.PWM_Manual
    
    If maClasseHID.PWM_Manual Then
        Label_Chauffe = maClasseHID.PWMM
        Me.HScroll_Chauffe.Visible = False
    Else
        Label_Chauffe = maClasseHID.PWMA
        Me.HScroll_Chauffe.Visible = True
    End If
    
    If maClasseHID.SW_Override Then
        'Sw override
        CB_UtilSwitch.value = vbUnchecked
    Else
        CB_UtilSwitch.value = vbChecked
    End If
    CB_Origine.value = IIf(maClasseHID.SW_Origin, vbChecked, vbUnchecked)
    CB_Fin.value = IIf(maClasseHID.SW_End, vbChecked, vbUnchecked)
    Cmd_Touche_ESC.BackColor = (IIf(maClasseHID.Touche_ESC, RGB(255, 0, 0), RGB(0, 0, 0)))
    Cmd_Touche_OK.BackColor = (IIf(maClasseHID.Touche_OK, RGB(255, 0, 0), RGB(0, 0, 0)))
    Cmd_Touche_LEFT.BackColor = (IIf(maClasseHID.Touche_LEFT, RGB(255, 0, 0), RGB(0, 0, 0)))
    Cmd_Touche_DOWN.BackColor = (IIf(maClasseHID.Touche_DOWN, RGB(255, 0, 0), RGB(0, 0, 0)))
    Cmd_Touche_UP.BackColor = (IIf(maClasseHID.Touche_UP, RGB(255, 0, 0), RGB(0, 0, 0)))
    Cmd_Touche_RIGHT.BackColor = (IIf(maClasseHID.Touche_RIGHT, RGB(255, 0, 0), RGB(0, 0, 0)))
End Sub
