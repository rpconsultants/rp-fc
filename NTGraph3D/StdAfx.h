// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__CE2CD6E0_49FB_49A0_B15A_5D833A403BD2__INCLUDED_)
#define AFX_STDAFX_H__CE2CD6E0_49FB_49A0_B15A_5D833A403BD2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0400
#endif
#define _ATL_APARTMENT_THREADED

#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>
#include <atlctl.h>

//Add support for OpenGL headers. 
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>
//Add support for OpenGL libs
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "glaux.lib")

// STL Interfaces
#include <algorithm>
#include <vector>

#include <math.h>
#include <ocidl.h>	// Added by ClassView

// Add native C++ compiler COM support - BSTR, VARIANT wrappers header
#include <comutil.h> 
#pragma comment(lib, "comsupp.lib")

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__CE2CD6E0_49FB_49A0_B15A_5D833A403BD2__INCLUDED)
