VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmParamMachine 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   8760
   ClientLeft      =   2835
   ClientTop       =   1470
   ClientWidth     =   10620
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   584
   ScaleMode       =   0  'User
   ScaleWidth      =   964.359
   ShowInTaskbar   =   0   'False
   Tag             =   "1"
   Begin VB.Frame Frame_0 
      Caption         =   "Gestion des tables"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   720
      Left            =   0
      TabIndex        =   14
      Tag             =   "10"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   10410
      Begin VB.CommandButton cmdGestion 
         Caption         =   "Copier"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   1
         Left            =   5940
         TabIndex        =   48
         Tag             =   "16"
         ToolTipText     =   "tt16"
         Top             =   220
         Width           =   1300
      End
      Begin VB.CommandButton cmdGestion 
         Caption         =   "Supprimer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   3
         Left            =   8550
         TabIndex        =   18
         Tag             =   "18"
         ToolTipText     =   "tt18"
         Top             =   220
         Width           =   1300
      End
      Begin VB.CommandButton cmdGestion 
         Caption         =   "Renommer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   2
         Left            =   7245
         TabIndex        =   17
         Tag             =   "17"
         ToolTipText     =   "tt17"
         Top             =   220
         Width           =   1300
      End
      Begin VB.ComboBox Combo_Table 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2100
         Sorted          =   -1  'True
         TabIndex        =   16
         ToolTipText     =   "0"
         Top             =   270
         Width           =   2880
      End
      Begin VB.Label LabelErrNom 
         Caption         =   "LabelErrNom"
         Height          =   255
         Left            =   0
         TabIndex        =   67
         Tag             =   "19_3"
         ToolTipText     =   "0"
         Top             =   360
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label LabelConfirmSuppress 
         Caption         =   "LabelConfirmSuppress"
         Height          =   375
         Left            =   1080
         TabIndex        =   62
         Tag             =   "19_2"
         ToolTipText     =   "0"
         Top             =   240
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label Label_01 
         Alignment       =   2  'Center
         Caption         =   "Table courante :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   195
         TabIndex        =   15
         Tag             =   "11"
         ToolTipText     =   "0"
         Top             =   315
         Width           =   1800
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7965
      Left            =   0
      TabIndex        =   84
      Tag             =   "20"
      ToolTipText     =   "0"
      Top             =   720
      Width           =   10410
      _ExtentX        =   18362
      _ExtentY        =   14049
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   714
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Interfaces"
      TabPicture(0)   =   "frmParamMachine.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label_23(3)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label_23(2)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label_23(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Image1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label_14"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label_12"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label2(1)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label_22(3)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label_24(9)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label_23(8)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label_11"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Line1(28)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Line1(29)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Line1(30)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Line1(31)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Line1(32)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Line1(33)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Line1(34)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Line1(36)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Line1(37)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Line1(38)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Line1(39)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "Line1(40)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Label_26(0)"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "Label_25(0)"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "LabelErrSignal"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "Label_23(0)"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "Line1(0)"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "Line1(3)"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "Line1(8)"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "Line1(9)"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "Label1(0)"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "Line1(7)"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "Line1(15)"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "Label_OUT(10)"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "Label_OUT(9)"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "Label_OUT(8)"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "Label_OUT(7)"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "Label_OUT(6)"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "Label_OUT(5)"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).Control(40)=   "Label_OUT(4)"
      Tab(0).Control(40).Enabled=   0   'False
      Tab(0).Control(41)=   "Label_OUT(3)"
      Tab(0).Control(41).Enabled=   0   'False
      Tab(0).Control(42)=   "Label_OUT(2)"
      Tab(0).Control(42).Enabled=   0   'False
      Tab(0).Control(43)=   "Line1(14)"
      Tab(0).Control(43).Enabled=   0   'False
      Tab(0).Control(44)=   "Line1(16)"
      Tab(0).Control(44).Enabled=   0   'False
      Tab(0).Control(45)=   "Line1(17)"
      Tab(0).Control(45).Enabled=   0   'False
      Tab(0).Control(46)=   "Line1(18)"
      Tab(0).Control(46).Enabled=   0   'False
      Tab(0).Control(47)=   "Line1(19)"
      Tab(0).Control(47).Enabled=   0   'False
      Tab(0).Control(48)=   "Line1(20)"
      Tab(0).Control(48).Enabled=   0   'False
      Tab(0).Control(49)=   "Line1(24)"
      Tab(0).Control(49).Enabled=   0   'False
      Tab(0).Control(50)=   "Line1(23)"
      Tab(0).Control(50).Enabled=   0   'False
      Tab(0).Control(51)=   "Line1(22)"
      Tab(0).Control(51).Enabled=   0   'False
      Tab(0).Control(52)=   "Line1(21)"
      Tab(0).Control(52).Enabled=   0   'False
      Tab(0).Control(53)=   "Line1(25)"
      Tab(0).Control(53).Enabled=   0   'False
      Tab(0).Control(54)=   "Line1(61)"
      Tab(0).Control(54).Enabled=   0   'False
      Tab(0).Control(55)=   "Line1(62)"
      Tab(0).Control(55).Enabled=   0   'False
      Tab(0).Control(56)=   "Label_OUT(1)"
      Tab(0).Control(56).Enabled=   0   'False
      Tab(0).Control(57)=   "Line1(63)"
      Tab(0).Control(57).Enabled=   0   'False
      Tab(0).Control(58)=   "Line1(64)"
      Tab(0).Control(58).Enabled=   0   'False
      Tab(0).Control(59)=   "Label1(9)"
      Tab(0).Control(59).Enabled=   0   'False
      Tab(0).Control(60)=   "Line1(27)"
      Tab(0).Control(60).Enabled=   0   'False
      Tab(0).Control(61)=   "Line1(44)"
      Tab(0).Control(61).Enabled=   0   'False
      Tab(0).Control(62)=   "Label1(10)"
      Tab(0).Control(62).Enabled=   0   'False
      Tab(0).Control(63)=   "Line1(48)"
      Tab(0).Control(63).Enabled=   0   'False
      Tab(0).Control(64)=   "Combo_SAV(0)"
      Tab(0).Control(64).Enabled=   0   'False
      Tab(0).Control(65)=   "Combo_SAV(11)"
      Tab(0).Control(65).Enabled=   0   'False
      Tab(0).Control(66)=   "Combo_SAV(12)"
      Tab(0).Control(66).Enabled=   0   'False
      Tab(0).Control(67)=   "Combo_SAV(13)"
      Tab(0).Control(67).Enabled=   0   'False
      Tab(0).Control(68)=   "TB_SAV(28)"
      Tab(0).Control(68).Enabled=   0   'False
      Tab(0).Control(69)=   "CB_SAV(110)"
      Tab(0).Control(69).Enabled=   0   'False
      Tab(0).Control(70)=   "CB_SAV(109)"
      Tab(0).Control(70).Enabled=   0   'False
      Tab(0).Control(71)=   "CB_SAV(108)"
      Tab(0).Control(71).Enabled=   0   'False
      Tab(0).Control(72)=   "CB_SAV(107)"
      Tab(0).Control(72).Enabled=   0   'False
      Tab(0).Control(73)=   "CB_SAV(106)"
      Tab(0).Control(73).Enabled=   0   'False
      Tab(0).Control(74)=   "CB_SAV(105)"
      Tab(0).Control(74).Enabled=   0   'False
      Tab(0).Control(75)=   "CB_SAV(104)"
      Tab(0).Control(75).Enabled=   0   'False
      Tab(0).Control(76)=   "CB_SAV(103)"
      Tab(0).Control(76).Enabled=   0   'False
      Tab(0).Control(77)=   "CB_SAV(102)"
      Tab(0).Control(77).Enabled=   0   'False
      Tab(0).Control(78)=   "CB_SAV(101)"
      Tab(0).Control(78).Enabled=   0   'False
      Tab(0).Control(79)=   "Combo_SAV(10)"
      Tab(0).Control(79).Enabled=   0   'False
      Tab(0).Control(80)=   "Combo_SAV(9)"
      Tab(0).Control(80).Enabled=   0   'False
      Tab(0).Control(81)=   "Combo_SAV(8)"
      Tab(0).Control(81).Enabled=   0   'False
      Tab(0).Control(82)=   "Combo_SAV(7)"
      Tab(0).Control(82).Enabled=   0   'False
      Tab(0).Control(83)=   "Combo_SAV(6)"
      Tab(0).Control(83).Enabled=   0   'False
      Tab(0).Control(84)=   "Combo_SAV(5)"
      Tab(0).Control(84).Enabled=   0   'False
      Tab(0).Control(85)=   "Combo_SAV(4)"
      Tab(0).Control(85).Enabled=   0   'False
      Tab(0).Control(86)=   "Combo_SAV(3)"
      Tab(0).Control(86).Enabled=   0   'False
      Tab(0).Control(87)=   "Combo_SAV(2)"
      Tab(0).Control(87).Enabled=   0   'False
      Tab(0).Control(88)=   "Combo_SAV(1)"
      Tab(0).Control(88).Enabled=   0   'False
      Tab(0).Control(89)=   "TB_SAV(33)"
      Tab(0).Control(89).Enabled=   0   'False
      Tab(0).Control(90)=   "TB_SAV(36)"
      Tab(0).Control(90).Enabled=   0   'False
      Tab(0).Control(91)=   "CB_SAV(16)"
      Tab(0).Control(91).Enabled=   0   'False
      Tab(0).ControlCount=   92
      TabCaption(1)   =   "Moteurs / Transmissions"
      TabPicture(1)   =   "frmParamMachine.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Image2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame3"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame5"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Frame17"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Frame4"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      TabCaption(2)   =   "Vitesses / Acc�leration"
      TabPicture(2)   =   "frmParamMachine.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame1"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Frame2"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "Frame16"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Dimensions"
      TabPicture(3)   =   "frmParamMachine.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Image_Table(0)"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "Image_Table(1)"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "Label3"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "Frame10"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).Control(4)=   "Frame11"
      Tab(3).Control(4).Enabled=   0   'False
      Tab(3).Control(5)=   "Frame12"
      Tab(3).Control(5).Enabled=   0   'False
      Tab(3).Control(6)=   "Frame13"
      Tab(3).Control(6).Enabled=   0   'False
      Tab(3).Control(7)=   "Frame14"
      Tab(3).Control(7).Enabled=   0   'False
      Tab(3).Control(8)=   "Frame15"
      Tab(3).Control(8).Enabled=   0   'False
      Tab(3).Control(9)=   "Command19"
      Tab(3).Control(9).Enabled=   0   'False
      Tab(3).Control(10)=   "CB_SAV(13)"
      Tab(3).Control(10).Enabled=   0   'False
      Tab(3).Control(11)=   "Frame20"
      Tab(3).Control(11).Enabled=   0   'False
      Tab(3).ControlCount=   12
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   16
         Left            =   9240
         TabIndex        =   203
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4320
         Width           =   255
      End
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   36
         Left            =   8955
         TabIndex        =   195
         Tag             =   "0"
         Text            =   "3"
         ToolTipText     =   "0"
         Top             =   3780
         Width           =   750
      End
      Begin VB.Frame Frame20 
         Caption         =   "Nettoyage fil"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   -67320
         TabIndex        =   184
         Tag             =   "180"
         ToolTipText     =   "0"
         Top             =   5400
         Width           =   2610
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   35
            Left            =   1230
            TabIndex        =   186
            Tag             =   "0"
            ToolTipText     =   "tt182"
            Top             =   735
            Width           =   750
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   34
            Left            =   1230
            TabIndex        =   185
            ToolTipText     =   "0"
            Top             =   345
            Width           =   750
         End
         Begin VB.Label Label25 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            Caption         =   "sec"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   190
            Tag             =   "184"
            ToolTipText     =   "0"
            Top             =   765
            Width           =   450
         End
         Begin VB.Label Label24 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            Caption         =   "Chauffe :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   189
            Tag             =   "182"
            ToolTipText     =   "0"
            Top             =   765
            Width           =   1095
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   188
            Tag             =   "183"
            ToolTipText     =   "0"
            Top             =   375
            Width           =   450
         End
         Begin VB.Label Label4 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            Caption         =   "Mont�e :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   187
            Tag             =   "181"
            ToolTipText     =   "0"
            Top             =   375
            Width           =   1095
         End
      End
      Begin VB.CheckBox CB_SAV 
         Caption         =   "<-    ->"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   13
         Left            =   -65760
         TabIndex        =   182
         Tag             =   "173"
         ToolTipText     =   "0"
         Top             =   2253
         Width           =   825
      End
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   33
         Left            =   8955
         TabIndex        =   179
         Tag             =   "0"
         Text            =   "3"
         ToolTipText     =   "0"
         Top             =   3270
         Width           =   750
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         ItemData        =   "frmParamMachine.frx":0070
         Left            =   3240
         List            =   "frmParamMachine.frx":0072
         Style           =   2  'Dropdown List
         TabIndex        =   153
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   903
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         ItemData        =   "frmParamMachine.frx":0074
         Left            =   3240
         List            =   "frmParamMachine.frx":0076
         Style           =   2  'Dropdown List
         TabIndex        =   152
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   1383
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         ItemData        =   "frmParamMachine.frx":0078
         Left            =   3240
         List            =   "frmParamMachine.frx":007A
         Style           =   2  'Dropdown List
         TabIndex        =   151
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   1863
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         ItemData        =   "frmParamMachine.frx":007C
         Left            =   3240
         List            =   "frmParamMachine.frx":007E
         Style           =   2  'Dropdown List
         TabIndex        =   150
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   2343
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         ItemData        =   "frmParamMachine.frx":0080
         Left            =   3240
         List            =   "frmParamMachine.frx":0082
         Style           =   2  'Dropdown List
         TabIndex        =   149
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   2823
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         ItemData        =   "frmParamMachine.frx":0084
         Left            =   3240
         List            =   "frmParamMachine.frx":0086
         Style           =   2  'Dropdown List
         TabIndex        =   148
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   3303
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         ItemData        =   "frmParamMachine.frx":0088
         Left            =   3240
         List            =   "frmParamMachine.frx":008A
         Style           =   2  'Dropdown List
         TabIndex        =   147
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   3783
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         ItemData        =   "frmParamMachine.frx":008C
         Left            =   3240
         List            =   "frmParamMachine.frx":008E
         Style           =   2  'Dropdown List
         TabIndex        =   146
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   4263
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         ItemData        =   "frmParamMachine.frx":0090
         Left            =   3240
         List            =   "frmParamMachine.frx":0092
         Style           =   2  'Dropdown List
         TabIndex        =   145
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   4743
         Width           =   1680
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         ItemData        =   "frmParamMachine.frx":0094
         Left            =   3240
         List            =   "frmParamMachine.frx":0096
         Style           =   2  'Dropdown List
         TabIndex        =   144
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   5223
         Width           =   1680
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   101
         Left            =   5640
         TabIndex        =   143
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   933
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   102
         Left            =   5640
         TabIndex        =   142
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1413
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   103
         Left            =   5640
         TabIndex        =   141
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1893
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   104
         Left            =   5640
         TabIndex        =   140
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   2373
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   105
         Left            =   5640
         TabIndex        =   139
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   2853
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   106
         Left            =   5640
         TabIndex        =   138
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3333
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   107
         Left            =   5640
         TabIndex        =   137
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3813
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   108
         Left            =   5640
         TabIndex        =   136
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4293
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   109
         Left            =   5640
         TabIndex        =   135
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4773
         Width           =   255
      End
      Begin VB.CheckBox CB_SAV 
         Height          =   255
         Index           =   110
         Left            =   5640
         TabIndex        =   134
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   5253
         Width           =   255
      End
      Begin VB.Frame Frame4 
         Caption         =   "Moteurs"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2130
         Left            =   -68640
         TabIndex        =   125
         Tag             =   "85b"
         ToolTipText     =   "0"
         Top             =   693
         Width           =   3405
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   3
            Left            =   915
            TabIndex        =   129
            Text            =   "100"
            ToolTipText     =   "0"
            Top             =   885
            Width           =   850
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   4
            Left            =   915
            TabIndex        =   128
            Text            =   "100"
            ToolTipText     =   "0"
            Top             =   1440
            Width           =   850
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   6
            Left            =   2175
            TabIndex        =   127
            Text            =   "3,6"
            ToolTipText     =   "0"
            Top             =   870
            Width           =   850
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   7
            Left            =   2175
            TabIndex        =   126
            Text            =   "3,6"
            ToolTipText     =   "0"
            Top             =   1425
            Width           =   850
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   11
            X1              =   180
            X2              =   3240
            Y1              =   285
            Y2              =   285
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   12
            X1              =   195
            X2              =   3240
            Y1              =   1365
            Y2              =   1365
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   13
            X1              =   195
            X2              =   3240
            Y1              =   1920
            Y2              =   1920
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   41
            X1              =   195
            X2              =   195
            Y1              =   285
            Y2              =   1920
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   42
            X1              =   840
            X2              =   840
            Y1              =   285
            Y2              =   1920
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   43
            X1              =   1995
            X2              =   1995
            Y1              =   750
            Y2              =   1920
         End
         Begin VB.Label Label36 
            Alignment       =   2  'Center
            Caption         =   "Y"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   210
            TabIndex        =   131
            Tag             =   "84"
            ToolTipText     =   "0"
            Top             =   1485
            Width           =   510
         End
         Begin VB.Label Label35 
            Alignment       =   2  'Center
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   210
            TabIndex        =   130
            Tag             =   "83"
            ToolTipText     =   "0"
            Top             =   945
            Width           =   510
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   45
            X1              =   195
            X2              =   3240
            Y1              =   765
            Y2              =   765
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   10
            X1              =   3240
            X2              =   3240
            Y1              =   285
            Y2              =   1920
         End
         Begin VB.Label Label88 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0FF&
            Caption         =   "pas/tour ou deg./pas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Index           =   11
            Left            =   840
            TabIndex        =   133
            Tag             =   "87"
            ToolTipText     =   "0"
            Top             =   285
            Width           =   2355
         End
         Begin VB.Label Label88 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0FF&
            Caption         =   "Axes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Index           =   10
            Left            =   210
            TabIndex        =   132
            Tag             =   "86"
            ToolTipText     =   "0"
            Top             =   285
            Width           =   630
         End
      End
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   28
         Left            =   8970
         TabIndex        =   99
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   2805
         Width           =   750
      End
      Begin VB.Frame Frame17 
         Caption         =   "Gestion moteurs"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1725
         Left            =   -74640
         TabIndex        =   66
         Tag             =   "75"
         ToolTipText     =   "0"
         Top             =   5973
         Width           =   2205
         Begin VB.CheckBox CB_SAV 
            Height          =   255
            Index           =   15
            Left            =   1560
            TabIndex        =   193
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   1080
            Width           =   255
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   30
            Left            =   1440
            TabIndex        =   74
            Text            =   "30"
            ToolTipText     =   "0"
            Top             =   480
            Width           =   495
         End
         Begin VB.Label Label35 
            Alignment       =   2  'Center
            Caption         =   "Inv. signal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   194
            Tag             =   "77"
            ToolTipText     =   "0"
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   35
            X1              =   120
            X2              =   2040
            Y1              =   1440
            Y2              =   1440
         End
         Begin VB.Label Label89 
            Alignment       =   2  'Center
            Caption         =   "TimeOut"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   192
            Tag             =   "76"
            ToolTipText     =   "0"
            Top             =   480
            Width           =   1215
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   5
            X1              =   120
            X2              =   2040
            Y1              =   960
            Y2              =   960
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   4
            X1              =   120
            X2              =   2040
            Y1              =   360
            Y2              =   360
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   2
            X1              =   2040
            X2              =   2040
            Y1              =   360
            Y2              =   1440
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   1
            X1              =   120
            X2              =   120
            Y1              =   360
            Y2              =   1440
         End
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   13
         ItemData        =   "frmParamMachine.frx":0098
         Left            =   8460
         List            =   "frmParamMachine.frx":009A
         Style           =   2  'Dropdown List
         TabIndex        =   71
         Tag             =   "45"
         ToolTipText     =   "0"
         Top             =   2160
         Width           =   1860
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   12
         ItemData        =   "frmParamMachine.frx":009C
         Left            =   8460
         List            =   "frmParamMachine.frx":009E
         Style           =   2  'Dropdown List
         TabIndex        =   70
         Tag             =   "45"
         ToolTipText     =   "0"
         Top             =   1695
         Width           =   1860
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   11
         ItemData        =   "frmParamMachine.frx":00A0
         Left            =   7080
         List            =   "frmParamMachine.frx":00A2
         Style           =   2  'Dropdown List
         TabIndex        =   69
         Tag             =   "46"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   2805
      End
      Begin VB.Frame Frame16 
         BorderStyle     =   0  'None
         Caption         =   "Outil de test des vitesses"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2625
         Left            =   -74880
         TabIndex        =   45
         Tag             =   "100"
         ToolTipText     =   "0"
         Top             =   453
         Width           =   6900
         Begin VB.Timer Timer_Test 
            Enabled         =   0   'False
            Interval        =   50
            Left            =   3120
            Top             =   1080
         End
         Begin VB.Frame Frame19 
            Caption         =   "Param�tres"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1575
            Left            =   3240
            TabIndex        =   109
            Tag             =   "104"
            ToolTipText     =   "0"
            Top             =   960
            Width           =   3615
            Begin VB.CheckBox CB_SAV 
               Alignment       =   1  'Right Justify
               Caption         =   "Pauses interm�diaires"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   195
               TabIndex        =   124
               Tag             =   "105_5"
               ToolTipText     =   "0"
               Top             =   1260
               Width           =   2430
            End
            Begin VB.TextBox TB_Param 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   4
               Left            =   2085
               TabIndex        =   123
               Text            =   "20"
               ToolTipText     =   "0"
               Top             =   960
               Width           =   705
            End
            Begin VB.TextBox TB_Param 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   3
               Left            =   2085
               TabIndex        =   122
               Text            =   "1"
               ToolTipText     =   "0"
               Top             =   720
               Width           =   705
            End
            Begin VB.TextBox TB_Param 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   2
               Left            =   2085
               TabIndex        =   121
               Text            =   "20"
               ToolTipText     =   "0"
               Top             =   480
               Width           =   705
            End
            Begin VB.TextBox TB_Param 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   1
               Left            =   2085
               TabIndex        =   120
               Text            =   "4"
               ToolTipText     =   "0"
               Top             =   210
               Width           =   705
            End
            Begin VB.Label Label22 
               Caption         =   "mm/s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   2925
               TabIndex        =   117
               Tag             =   "106_1"
               ToolTipText     =   "0"
               Top             =   240
               Width           =   645
            End
            Begin VB.Label Label21 
               Alignment       =   1  'Right Justify
               Caption         =   "Vitesse de d�part"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   116
               Tag             =   "105_1"
               ToolTipText     =   "0"
               Top             =   240
               Width           =   1890
            End
            Begin VB.Label Label20 
               Caption         =   "mm/s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   2925
               TabIndex        =   115
               Tag             =   "106_1"
               ToolTipText     =   "0"
               Top             =   510
               Width           =   645
            End
            Begin VB.Label Label19 
               Alignment       =   1  'Right Justify
               Caption         =   "Vitesse d'arriv�e"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   114
               Tag             =   "105_2"
               ToolTipText     =   "0"
               Top             =   510
               Width           =   1890
            End
            Begin VB.Label Label18 
               Caption         =   "mm/s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   2925
               TabIndex        =   113
               Tag             =   "106_1"
               ToolTipText     =   "0"
               Top             =   750
               Width           =   645
            End
            Begin VB.Label Label17 
               Alignment       =   1  'Right Justify
               Caption         =   "Incr�ment"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   112
               Tag             =   "105_3"
               ToolTipText     =   "0"
               Top             =   750
               Width           =   1890
            End
            Begin VB.Label Label6 
               Caption         =   "mm"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   2925
               TabIndex        =   111
               Tag             =   "106_2"
               ToolTipText     =   "0"
               Top             =   990
               Width           =   645
            End
            Begin VB.Label Label5 
               Alignment       =   1  'Right Justify
               Caption         =   "Distance � parcourir"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   110
               Tag             =   "105_4"
               ToolTipText     =   "0"
               Top             =   990
               Width           =   1890
            End
         End
         Begin VB.Frame Frame_Axe 
            Caption         =   "Axe(s) test�(s)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   104
            Tag             =   "102"
            ToolTipText     =   "0"
            Top             =   960
            Width           =   3015
            Begin VB.CheckBox CB_Axe 
               Caption         =   "XG"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   108
               Tag             =   "103_1"
               ToolTipText     =   "0"
               Top             =   240
               Width           =   615
            End
            Begin VB.CheckBox CB_Axe 
               Caption         =   "XD"
               Height          =   255
               Index           =   1
               Left            =   840
               TabIndex        =   107
               Tag             =   "103_2"
               ToolTipText     =   "0"
               Top             =   240
               Width           =   615
            End
            Begin VB.CheckBox CB_Axe 
               Caption         =   "YG"
               Height          =   255
               Index           =   2
               Left            =   1560
               TabIndex        =   106
               Tag             =   "103_3"
               ToolTipText     =   "0"
               Top             =   240
               Width           =   615
            End
            Begin VB.CheckBox CB_Axe 
               Caption         =   "YD"
               Height          =   255
               Index           =   3
               Left            =   2280
               TabIndex        =   105
               Tag             =   "103_4"
               ToolTipText     =   "0"
               Top             =   240
               Width           =   615
            End
         End
         Begin VB.CommandButton Cmd_Stop 
            Caption         =   "STOP !"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Left            =   120
            TabIndex        =   79
            Tag             =   "109"
            ToolTipText     =   "0"
            Top             =   2040
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton Cmd_GO 
            Caption         =   "GO !"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Left            =   2400
            TabIndex        =   78
            Tag             =   "108"
            ToolTipText     =   "0"
            Top             =   2040
            Width           =   765
         End
         Begin VB.Label Label_Vitesse2 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   1200
            TabIndex        =   191
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   2160
            Width           =   1140
         End
         Begin VB.Label Label_VitesseActuelle 
            Alignment       =   2  'Center
            Caption         =   "Vitesse actuelle"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   960
            TabIndex        =   119
            Tag             =   "107"
            ToolTipText     =   "0"
            Top             =   1680
            Width           =   1545
         End
         Begin VB.Label Label_Vitesse 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   1200
            TabIndex        =   118
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   1920
            Width           =   1140
         End
         Begin VB.Label Label81 
            BackColor       =   &H00E0E0E0&
            Caption         =   $"frmParamMachine.frx":00A4
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   720
            Left            =   150
            TabIndex        =   46
            Tag             =   "101"
            ToolTipText     =   "0"
            Top             =   255
            Width           =   6585
         End
      End
      Begin VB.CommandButton Command19 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   -70680
         Picture         =   "frmParamMachine.frx":0145
         Style           =   1  'Graphical
         TabIndex        =   44
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   543
         Width           =   585
      End
      Begin VB.Frame Frame15 
         Caption         =   "Longueur maxi du fil"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   -74910
         TabIndex        =   42
         Tag             =   "135"
         ToolTipText     =   "0"
         Top             =   1680
         Width           =   2325
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   18
            Left            =   645
            TabIndex        =   87
            ToolTipText     =   "0"
            Top             =   315
            Width           =   750
         End
         Begin VB.Label Label85 
            Alignment       =   2  'Center
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1455
            TabIndex        =   43
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   345
            Width           =   450
         End
      End
      Begin VB.Frame Frame14 
         Caption         =   "Fins de courses"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1140
         Left            =   -69600
         TabIndex        =   41
         Tag             =   "140"
         ToolTipText     =   "0"
         Top             =   513
         Width           =   2040
         Begin VB.CommandButton Cmd_RetourOrigine 
            Caption         =   "Retour origine"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   165
            TabIndex        =   89
            Tag             =   "142"
            ToolTipText     =   "0"
            Top             =   645
            Width           =   1695
         End
         Begin VB.CheckBox CB_SAV 
            Caption         =   "Activ�es"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   12
            Left            =   150
            TabIndex        =   88
            Tag             =   "141"
            ToolTipText     =   "0"
            Top             =   300
            Width           =   1845
         End
      End
      Begin VB.Frame Frame13 
         Caption         =   "Ecartement"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1560
         Left            =   -74880
         TabIndex        =   34
         Tag             =   "150"
         ToolTipText     =   "0"
         Top             =   6240
         Width           =   3090
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   25
            Left            =   1725
            TabIndex        =   94
            ToolTipText     =   "0"
            Top             =   1065
            Width           =   750
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   24
            Left            =   1725
            TabIndex        =   93
            ToolTipText     =   "0"
            Top             =   690
            Width           =   750
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   23
            Left            =   1725
            TabIndex        =   92
            ToolTipText     =   "0"
            Top             =   300
            Width           =   750
         End
         Begin VB.Label Label80 
            Alignment       =   2  'Center
            BackColor       =   &H008080FF&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2535
            TabIndex        =   40
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   1095
            Width           =   450
         End
         Begin VB.Label Label79 
            Alignment       =   2  'Center
            BackColor       =   &H008080FF&
            Caption         =   "table <--> YD :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   39
            Tag             =   "153"
            ToolTipText     =   "0"
            Top             =   1095
            Width           =   1590
         End
         Begin VB.Label Label78 
            Alignment       =   2  'Center
            BackColor       =   &H008080FF&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2535
            TabIndex        =   38
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   720
            Width           =   450
         End
         Begin VB.Label Label77 
            Alignment       =   2  'Center
            BackColor       =   &H008080FF&
            Caption         =   "YG <--> table :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   37
            Tag             =   "152"
            ToolTipText     =   "0"
            Top             =   720
            Width           =   1590
         End
         Begin VB.Label Label76 
            Alignment       =   2  'Center
            BackColor       =   &H00FF00FF&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2535
            TabIndex        =   36
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   330
            Width           =   450
         End
         Begin VB.Label Label75 
            Alignment       =   2  'Center
            BackColor       =   &H00FF00FF&
            Caption         =   "YG <--> YD :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   35
            Tag             =   "151"
            ToolTipText     =   "0"
            Top             =   330
            Width           =   1590
         End
      End
      Begin VB.Frame Frame12 
         Caption         =   "D�calages origine"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1245
         Left            =   -67440
         TabIndex        =   29
         Tag             =   "145"
         ToolTipText     =   "0"
         Top             =   513
         Width           =   2775
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   37
            Left            =   1605
            TabIndex        =   199
            ToolTipText     =   "0"
            Top             =   360
            Width           =   630
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   38
            Left            =   1605
            TabIndex        =   198
            ToolTipText     =   "0"
            Top             =   765
            Width           =   630
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   21
            Left            =   510
            TabIndex        =   91
            ToolTipText     =   "0"
            Top             =   765
            Width           =   630
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   19
            Left            =   510
            TabIndex        =   90
            ToolTipText     =   "0"
            Top             =   360
            Width           =   630
         End
         Begin VB.Label Label27 
            Alignment       =   2  'Center
            BackColor       =   &H0000FF00&
            Caption         =   "XD"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            TabIndex        =   201
            Tag             =   "147"
            ToolTipText     =   "0"
            Top             =   390
            Width           =   375
         End
         Begin VB.Label Label26 
            Alignment       =   2  'Center
            BackColor       =   &H0000C000&
            Caption         =   "YD"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            TabIndex        =   200
            Tag             =   "149"
            ToolTipText     =   "0"
            Top             =   795
            Width           =   375
         End
         Begin VB.Label Label72 
            Alignment       =   2  'Center
            BackColor       =   &H0000C000&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2280
            TabIndex        =   33
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   795
            Width           =   450
         End
         Begin VB.Label Label71 
            Alignment       =   2  'Center
            BackColor       =   &H0000C000&
            Caption         =   "YG"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   32
            Tag             =   "148"
            ToolTipText     =   "0"
            Top             =   795
            Width           =   375
         End
         Begin VB.Label Label16 
            Alignment       =   2  'Center
            BackColor       =   &H0000FF00&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2280
            TabIndex        =   31
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   390
            Width           =   450
         End
         Begin VB.Label Label15 
            Alignment       =   2  'Center
            BackColor       =   &H0000FF00&
            Caption         =   "XG"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   30
            Tag             =   "146"
            ToolTipText     =   "0"
            Top             =   390
            Width           =   375
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "Rangement chariots"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   -67320
         TabIndex        =   24
         Tag             =   "160"
         ToolTipText     =   "0"
         Top             =   6600
         Width           =   2610
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   26
            Left            =   1230
            TabIndex        =   95
            ToolTipText     =   "0"
            Top             =   345
            Width           =   750
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   27
            Left            =   1230
            TabIndex        =   96
            ToolTipText     =   "0"
            Top             =   735
            Width           =   750
         End
         Begin VB.Label Label14 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFF80&
            Caption         =   "Suivant X :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   28
            Tag             =   "161"
            ToolTipText     =   "0"
            Top             =   375
            Width           =   1095
         End
         Begin VB.Label Label13 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFF80&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   27
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   375
            Width           =   450
         End
         Begin VB.Label Label12 
            Alignment       =   2  'Center
            BackColor       =   &H00C0C000&
            Caption         =   "Suivant Y :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   26
            Tag             =   "162"
            ToolTipText     =   "0"
            Top             =   765
            Width           =   1095
         End
         Begin VB.Label Label11 
            Alignment       =   2  'Center
            BackColor       =   &H00C0C000&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   25
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   765
            Width           =   450
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "Courses maxi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   -74910
         TabIndex        =   19
         Tag             =   "130"
         ToolTipText     =   "0"
         Top             =   453
         Width           =   2175
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   17
            Left            =   825
            TabIndex        =   86
            ToolTipText     =   "0"
            Top             =   720
            Width           =   750
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   16
            Left            =   825
            TabIndex        =   85
            ToolTipText     =   "0"
            Top             =   345
            Width           =   750
         End
         Begin VB.Label Label10 
            Alignment       =   2  'Center
            BackColor       =   &H000080FF&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1635
            TabIndex        =   23
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   765
            Width           =   450
         End
         Begin VB.Label Label9 
            Alignment       =   2  'Center
            BackColor       =   &H000080FF&
            Caption         =   "En Y :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   22
            Tag             =   "132"
            ToolTipText     =   "0"
            Top             =   765
            Width           =   660
         End
         Begin VB.Label Label8 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1635
            TabIndex        =   21
            Tag             =   "133"
            ToolTipText     =   "0"
            Top             =   375
            Width           =   450
         End
         Begin VB.Label Label7 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            Caption         =   "En X :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   105
            TabIndex        =   20
            Tag             =   "131"
            ToolTipText     =   "0"
            Top             =   375
            Width           =   660
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Rapport de transmission"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2085
         Left            =   -74670
         TabIndex        =   13
         Tag             =   "80"
         ToolTipText     =   "0"
         Top             =   453
         Width           =   3285
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   2070
            TabIndex        =   73
            ToolTipText     =   "0"
            Top             =   1515
            Width           =   850
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   0
            Left            =   2055
            TabIndex        =   72
            ToolTipText     =   "0"
            Top             =   960
            Width           =   850
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   6
            X1              =   105
            X2              =   3120
            Y1              =   360
            Y2              =   360
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   49
            X1              =   120
            X2              =   120
            Y1              =   360
            Y2              =   1920
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   51
            X1              =   3120
            X2              =   3120
            Y1              =   360
            Y2              =   1920
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   50
            X1              =   1800
            X2              =   1800
            Y1              =   360
            Y2              =   1920
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   52
            X1              =   120
            X2              =   3120
            Y1              =   840
            Y2              =   840
         End
         Begin VB.Label Label35 
            Alignment       =   2  'Center
            Caption         =   "XG et XD"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   360
            TabIndex        =   57
            Tag             =   "83"
            ToolTipText     =   "0"
            Top             =   1020
            Width           =   1125
         End
         Begin VB.Label Label36 
            Alignment       =   2  'Center
            Caption         =   "YG et YD"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   360
            TabIndex        =   56
            Tag             =   "84"
            ToolTipText     =   "0"
            Top             =   1560
            Width           =   1125
         End
         Begin VB.Label Label88 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0FF&
            Caption         =   "mm/tour"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Index           =   13
            Left            =   1800
            TabIndex        =   55
            Tag             =   "82"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   1320
         End
         Begin VB.Label Label88 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0FF&
            Caption         =   "Axe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Index           =   2
            Left            =   120
            TabIndex        =   54
            Tag             =   "81"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   1695
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   47
            X1              =   120
            X2              =   3120
            Y1              =   1920
            Y2              =   1920
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   46
            X1              =   120
            X2              =   3120
            Y1              =   1440
            Y2              =   1440
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Pas perdus � l'inversion de sens"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2265
         Left            =   -71760
         TabIndex        =   12
         Tag             =   "89"
         ToolTipText     =   "0"
         Top             =   5493
         Width           =   7065
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   31
            Left            =   6000
            TabIndex        =   102
            ToolTipText     =   "0"
            Top             =   675
            Width           =   735
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   32
            Left            =   6000
            TabIndex        =   101
            ToolTipText     =   "0"
            Top             =   1140
            Width           =   735
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   10
            Left            =   5040
            TabIndex        =   77
            ToolTipText     =   "0"
            Top             =   1140
            Width           =   735
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   9
            Left            =   5040
            TabIndex        =   76
            ToolTipText     =   "0"
            Top             =   675
            Width           =   735
         End
         Begin VB.CommandButton Cmd_TestPasPerdus 
            Caption         =   "Outil Test"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1200
            TabIndex        =   75
            Tag             =   "91"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   1215
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   60
            X1              =   3960
            X2              =   6840
            Y1              =   615
            Y2              =   615
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   59
            X1              =   6840
            X2              =   6840
            Y1              =   255
            Y2              =   1560
         End
         Begin VB.Label Label35 
            Alignment       =   2  'Center
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   4230
            TabIndex        =   61
            Tag             =   "95"
            ToolTipText     =   "0"
            Top             =   735
            Width           =   405
         End
         Begin VB.Label Label36 
            Alignment       =   2  'Center
            Caption         =   "Y"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   4200
            TabIndex        =   60
            Tag             =   "96"
            ToolTipText     =   "0"
            Top             =   1215
            Width           =   405
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   58
            X1              =   4920
            X2              =   4920
            Y1              =   255
            Y2              =   1560
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   57
            X1              =   3960
            X2              =   3960
            Y1              =   255
            Y2              =   1560
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   55
            X1              =   3960
            X2              =   6840
            Y1              =   1575
            Y2              =   1575
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   54
            X1              =   3960
            X2              =   6840
            Y1              =   1095
            Y2              =   1095
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00C00000&
            BorderWidth     =   2
            Index           =   53
            X1              =   3945
            X2              =   6840
            Y1              =   255
            Y2              =   255
         End
         Begin VB.Label Label64 
            BackColor       =   &H00E0E0E0&
            Caption         =   $"frmParamMachine.frx":028F
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1260
            Left            =   120
            TabIndex        =   47
            Tag             =   "92"
            ToolTipText     =   "0"
            Top             =   810
            Width           =   3630
         End
         Begin VB.Label Label88 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0FF&
            Caption         =   "Axe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   14
            Left            =   3960
            TabIndex        =   58
            Tag             =   "93"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label88 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0FF&
            Caption         =   "Gauche"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   15
            Left            =   4920
            TabIndex        =   59
            Tag             =   "94"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   960
         End
         Begin VB.Label Label88 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0FF&
            Caption         =   "Droite"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   0
            Left            =   5880
            TabIndex        =   100
            Tag             =   "97"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   960
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Vitesses sans acc�l�ration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2625
         Left            =   -67920
         TabIndex        =   6
         Tag             =   "110"
         ToolTipText     =   "0"
         Top             =   453
         Width           =   3225
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   13
            Left            =   1920
            TabIndex        =   81
            ToolTipText     =   "0"
            Top             =   1245
            Width           =   600
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   12
            Left            =   1920
            TabIndex        =   80
            ToolTipText     =   "0"
            Top             =   810
            Width           =   600
         End
         Begin VB.Label Label58 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Vitesses maxi sans perte de pas."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   120
            TabIndex        =   11
            Tag             =   "111"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   2955
         End
         Begin VB.Label Label57 
            Alignment       =   2  'Center
            Caption         =   "Vitesse maxi en Y :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   120
            TabIndex        =   10
            Tag             =   "113"
            ToolTipText     =   "0"
            Top             =   1275
            Width           =   1800
         End
         Begin VB.Label Label56 
            Caption         =   "mm/s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   2520
            TabIndex        =   9
            Tag             =   "114"
            ToolTipText     =   "0"
            Top             =   1290
            Width           =   615
         End
         Begin VB.Label Label55 
            Alignment       =   2  'Center
            Caption         =   "Vitesse maxi en X :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   120
            TabIndex        =   8
            Tag             =   "112"
            ToolTipText     =   "0"
            Top             =   870
            Width           =   1755
         End
         Begin VB.Label Label54 
            Caption         =   "mm/s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   2520
            TabIndex        =   7
            Tag             =   "114"
            ToolTipText     =   "0"
            Top             =   855
            Width           =   615
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Acc�l�ration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4695
         Left            =   -74895
         TabIndex        =   0
         Tag             =   "120"
         ToolTipText     =   "0"
         Top             =   3093
         Width           =   10185
         Begin VB.TextBox TB_Pente 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   6000
            TabIndex        =   167
            Text            =   "5"
            Top             =   2760
            Width           =   495
         End
         Begin MSComctlLib.Slider Sld_SAV 
            Height          =   525
            Index           =   0
            Left            =   5400
            TabIndex        =   178
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   3360
            Width           =   1665
            _ExtentX        =   2937
            _ExtentY        =   926
            _Version        =   393216
            LargeChange     =   1
            Min             =   1
            Max             =   9
            SelStart        =   2
            Value           =   2
         End
         Begin VB.PictureBox PB_Accel 
            AutoRedraw      =   -1  'True
            BorderStyle     =   0  'None
            Height          =   3765
            Left            =   2280
            ScaleHeight     =   3765
            ScaleWidth      =   7830
            TabIndex        =   172
            ToolTipText     =   "0"
            Top             =   840
            Width           =   7830
            Begin VB.Label Label_Accel 
               Alignment       =   2  'Center
               Caption         =   "On double la vitesse en :"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FF0000&
               Height          =   255
               Left            =   1080
               TabIndex        =   177
               Tag             =   "0"
               ToolTipText     =   "0"
               Top             =   3120
               Width           =   5805
            End
            Begin VB.Label Label1 
               BackStyle       =   0  'Transparent
               Caption         =   "Temps"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   8
               Left            =   6735
               TabIndex        =   176
               Tag             =   "118"
               ToolTipText     =   "0"
               Top             =   3525
               Width           =   930
            End
            Begin VB.Label Label1 
               BackStyle       =   0  'Transparent
               Caption         =   "Vitesse"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   7
               Left            =   450
               TabIndex        =   175
               Tag             =   "115"
               ToolTipText     =   "0"
               Top             =   150
               Width           =   990
            End
            Begin VB.Label Label1 
               BackStyle       =   0  'Transparent
               Caption         =   "2 x V"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   6
               Left            =   3630
               TabIndex        =   174
               Tag             =   "116"
               ToolTipText     =   "0"
               Top             =   705
               Width           =   555
            End
            Begin VB.Label Label1 
               BackStyle       =   0  'Transparent
               Caption         =   "V"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   525
               TabIndex        =   173
               Tag             =   "117"
               ToolTipText     =   "0"
               Top             =   2715
               Width           =   345
            End
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   15
            Left            =   555
            TabIndex        =   83
            ToolTipText     =   "0"
            Top             =   2580
            Width           =   660
         End
         Begin VB.TextBox TB_SAV 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   14
            Left            =   555
            TabIndex        =   82
            ToolTipText     =   "0"
            Top             =   1590
            Width           =   660
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Temps"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   8925
            TabIndex        =   171
            Tag             =   "118"
            ToolTipText     =   "0"
            Top             =   4335
            Width           =   930
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Vitesse"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   2640
            TabIndex        =   170
            Tag             =   "115"
            ToolTipText     =   "0"
            Top             =   960
            Width           =   990
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "2 x V"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   5820
            TabIndex        =   169
            Tag             =   "116"
            ToolTipText     =   "0"
            Top             =   1515
            Width           =   555
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "V"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   2715
            TabIndex        =   168
            Tag             =   "117"
            ToolTipText     =   "0"
            Top             =   3525
            Width           =   345
         End
         Begin VB.Label Label37 
            BackColor       =   &H00E0E0E0&
            Caption         =   $"frmParamMachine.frx":032B
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   570
            Index           =   1
            Left            =   120
            TabIndex        =   5
            Tag             =   "122"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   9900
         End
         Begin VB.Label Label53 
            Alignment       =   2  'Center
            Caption         =   "Vitesse maxi en Y :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   255
            TabIndex        =   4
            Tag             =   "113"
            ToolTipText     =   "0"
            Top             =   2280
            Width           =   1995
         End
         Begin VB.Label Label52 
            Caption         =   "mm/s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   1305
            TabIndex        =   3
            Tag             =   "114"
            ToolTipText     =   "0"
            Top             =   2655
            Width           =   615
         End
         Begin VB.Label Label39 
            Alignment       =   2  'Center
            Caption         =   "Vitesse maxi en X :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   240
            TabIndex        =   2
            Tag             =   "112"
            ToolTipText     =   "0"
            Top             =   1260
            Width           =   1995
         End
         Begin VB.Label Label40 
            Caption         =   "mm/s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   1275
            TabIndex        =   1
            Tag             =   "114"
            ToolTipText     =   "0"
            Top             =   1635
            Width           =   615
         End
      End
      Begin VB.ComboBox Combo_SAV 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         ItemData        =   "frmParamMachine.frx":040B
         Left            =   135
         List            =   "frmParamMachine.frx":040D
         Style           =   2  'Dropdown List
         TabIndex        =   68
         Tag             =   "43"
         ToolTipText     =   "0"
         Top             =   2469
         Width           =   1800
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   48
         X1              =   6480
         X2              =   10320
         Y1              =   4680
         Y2              =   4680
      End
      Begin VB.Label Label1 
         Caption         =   "s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         Left            =   9840
         TabIndex        =   197
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3720
         Width           =   255
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   44
         X1              =   6480
         X2              =   10320
         Y1              =   4200
         Y2              =   4200
      End
      Begin VB.Label Label3 
         Caption         =   "Orientation"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -65880
         TabIndex        =   183
         Tag             =   "144"
         Top             =   1893
         Width           =   975
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   27
         X1              =   6480
         X2              =   10320
         Y1              =   3210
         Y2              =   3210
      End
      Begin VB.Label Label1 
         Caption         =   "s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   9825
         TabIndex        =   180
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3270
         Width           =   255
      End
      Begin VB.Image Image_Table 
         Height          =   6885
         Index           =   1
         Left            =   -74880
         Picture         =   "frmParamMachine.frx":040F
         Top             =   1008
         Visible         =   0   'False
         Width           =   10395
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   64
         X1              =   2040
         X2              =   6360
         Y1              =   810
         Y2              =   810
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   63
         X1              =   2040
         X2              =   6360
         Y1              =   5610
         Y2              =   5610
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2160
         TabIndex        =   165
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   933
         Width           =   885
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   62
         X1              =   3120
         X2              =   3120
         Y1              =   453
         Y2              =   5613
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   61
         X1              =   5040
         X2              =   5040
         Y1              =   453
         Y2              =   5613
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   26
         X1              =   0
         X2              =   4560
         Y1              =   -27
         Y2              =   -27
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   25
         X1              =   2040
         X2              =   6360
         Y1              =   450
         Y2              =   450
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   21
         X1              =   2040
         X2              =   6360
         Y1              =   1770
         Y2              =   1770
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   22
         X1              =   2040
         X2              =   6360
         Y1              =   2730
         Y2              =   2730
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   23
         X1              =   2040
         X2              =   6360
         Y1              =   3690
         Y2              =   3690
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   24
         X1              =   2040
         X2              =   6360
         Y1              =   4650
         Y2              =   4650
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   20
         X1              =   2040
         X2              =   2040
         Y1              =   453
         Y2              =   5613
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   19
         X1              =   2040
         X2              =   6360
         Y1              =   1290
         Y2              =   1290
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   18
         X1              =   2040
         X2              =   6360
         Y1              =   2250
         Y2              =   2250
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   17
         X1              =   2040
         X2              =   6360
         Y1              =   3210
         Y2              =   3210
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   16
         X1              =   2040
         X2              =   6360
         Y1              =   4170
         Y2              =   4170
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   14
         X1              =   2040
         X2              =   6360
         Y1              =   5130
         Y2              =   5130
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   2160
         TabIndex        =   162
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1413
         Width           =   885
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   2160
         TabIndex        =   161
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1893
         Width           =   885
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   2160
         TabIndex        =   160
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   2373
         Width           =   885
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   2160
         TabIndex        =   159
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   2853
         Width           =   885
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   2160
         TabIndex        =   158
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3333
         Width           =   885
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   2160
         TabIndex        =   157
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3813
         Width           =   885
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   2160
         TabIndex        =   156
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4293
         Width           =   885
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   2160
         TabIndex        =   155
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4773
         Width           =   885
      End
      Begin VB.Label Label_OUT 
         Alignment       =   2  'Center
         Caption         =   "OUT10"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   2160
         TabIndex        =   154
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   5253
         Width           =   885
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   15
         X1              =   6360
         X2              =   6360
         Y1              =   450
         Y2              =   5610
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   7
         X1              =   6480
         X2              =   10320
         Y1              =   3690
         Y2              =   3690
      End
      Begin VB.Label Label1 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   9840
         TabIndex        =   103
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   2805
         Width           =   255
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   9
         X1              =   10320
         X2              =   10320
         Y1              =   2730
         Y2              =   4680
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   8
         X1              =   8400
         X2              =   8400
         Y1              =   2730
         Y2              =   4680
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   3
         X1              =   6480
         X2              =   10320
         Y1              =   2730
         Y2              =   2730
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   0
         X1              =   6480
         X2              =   6480
         Y1              =   2730
         Y2              =   4680
      End
      Begin VB.Label Label_23 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Chauffe Maximum"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   6495
         TabIndex        =   98
         Tag             =   "42"
         ToolTipText     =   "0"
         Top             =   2745
         Width           =   1935
      End
      Begin VB.Label LabelErrSignal 
         Caption         =   "LabelErrSignal"
         Height          =   255
         Left            =   420
         TabIndex        =   65
         Tag             =   "err34"
         Top             =   1053
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label Label_25 
         Alignment       =   2  'Center
         Caption         =   "XG et XD"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   6600
         TabIndex        =   53
         Tag             =   "39"
         ToolTipText     =   "0"
         Top             =   1770
         Width           =   1725
      End
      Begin VB.Label Label_26 
         Alignment       =   2  'Center
         Caption         =   "YG et YD"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   6600
         TabIndex        =   52
         Tag             =   "40"
         ToolTipText     =   "0"
         Top             =   2205
         Width           =   1725
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   40
         X1              =   10335
         X2              =   10335
         Y1              =   435
         Y2              =   1290
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   39
         X1              =   6480
         X2              =   6480
         Y1              =   435
         Y2              =   1290
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   38
         X1              =   10335
         X2              =   10320
         Y1              =   1395
         Y2              =   2640
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   37
         X1              =   8415
         X2              =   8400
         Y1              =   1395
         Y2              =   2640
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   36
         X1              =   6480
         X2              =   6480
         Y1              =   1395
         Y2              =   2640
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   34
         X1              =   6480
         X2              =   10335
         Y1              =   2640
         Y2              =   2640
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   33
         X1              =   6480
         X2              =   10335
         Y1              =   2160
         Y2              =   2160
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   32
         X1              =   6480
         X2              =   10320
         Y1              =   1680
         Y2              =   1680
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   31
         X1              =   6480
         X2              =   10335
         Y1              =   1395
         Y2              =   1395
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   30
         X1              =   6480
         X2              =   10335
         Y1              =   1290
         Y2              =   1290
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   29
         X1              =   6480
         X2              =   10320
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00C00000&
         BorderWidth     =   2
         Index           =   28
         X1              =   6480
         X2              =   10335
         Y1              =   435
         Y2              =   435
      End
      Begin VB.Image Image_Table 
         Height          =   6885
         Index           =   0
         Left            =   -74880
         Picture         =   "frmParamMachine.frx":113EB
         Top             =   1008
         Width           =   10395
      End
      Begin VB.Label Label_11 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Interface USB :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   97
         Tag             =   "21"
         ToolTipText     =   "0"
         Top             =   2088
         Width           =   1815
      End
      Begin VB.Image Image2 
         Height          =   4410
         Left            =   -74880
         Picture         =   "frmParamMachine.frx":22414
         ToolTipText     =   "0"
         Top             =   1143
         Width           =   10275
      End
      Begin VB.Label Label_23 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Axes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   6495
         TabIndex        =   50
         Tag             =   "37"
         ToolTipText     =   "0"
         Top             =   1395
         Width           =   1935
      End
      Begin VB.Label Label_24 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "R�solution"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   8400
         TabIndex        =   51
         Tag             =   "38"
         ToolTipText     =   "0"
         Top             =   1410
         Width           =   1935
      End
      Begin VB.Label Label_22 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Fr�quence maxi accept�e"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   6495
         TabIndex        =   49
         Tag             =   "36"
         ToolTipText     =   "0"
         Top             =   435
         Width           =   3855
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Inv. Sign."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   5040
         TabIndex        =   166
         Tag             =   "25"
         ToolTipText     =   "0"
         Top             =   450
         Width           =   1335
      End
      Begin VB.Label Label_12 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Sortie"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2040
         TabIndex        =   164
         Tag             =   "23"
         ToolTipText     =   "0"
         Top             =   453
         Width           =   1095
      End
      Begin VB.Label Label_14 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Affectation"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3120
         TabIndex        =   163
         Tag             =   "24"
         ToolTipText     =   "0"
         Top             =   453
         Width           =   1935
      End
      Begin VB.Image Image1 
         Height          =   3660
         Left            =   360
         Picture         =   "frmParamMachine.frx":28806
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4440
         Width           =   9930
      End
      Begin VB.Label Label_23 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Tempo avant"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   6480
         TabIndex        =   181
         Tag             =   "43"
         ToolTipText     =   "0"
         Top             =   3210
         Width           =   1935
      End
      Begin VB.Label Label_23 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Tempo apr�s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   6480
         TabIndex        =   196
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   3720
         Width           =   1935
      End
      Begin VB.Label Label_23 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "PWM -> IO1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   6480
         TabIndex        =   202
         Tag             =   "45"
         ToolTipText     =   "0"
         Top             =   4200
         Width           =   1935
      End
   End
   Begin VB.Label LabelErreurCombo 
      Caption         =   "LabelConfirmSuppress"
      Height          =   375
      Left            =   0
      TabIndex        =   64
      Tag             =   "19_3"
      ToolTipText     =   "0"
      Top             =   240
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LabelNouveauNom 
      Caption         =   "LabelNouveauNom"
      Height          =   375
      Left            =   240
      TabIndex        =   63
      Tag             =   "19"
      ToolTipText     =   "0"
      Top             =   360
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "frmParamMachine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim AncienneTable As String
Dim bDepl As Boolean
Dim bChange As Boolean
Dim maVitesse As Single
'Pour le dessin de l'acc�l�ration
Dim Temps(1 To 9) As Single
Dim t As Single

Private Sub CB_Axe_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Call TestGo
    If Index > 1 Then
        CB_Axe(0).value = vbUnchecked
        CB_Axe(1).value = vbUnchecked
    Else
        CB_Axe(2).value = vbUnchecked
        CB_Axe(3).value = vbUnchecked
    End If
End Sub

Private Sub CB_SAV_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    
    If Index = 12 Then  'Activation FDC
        Cmd_RetourOrigine.Enabled = (CB_SAV(12).value = vbChecked)
    End If
    If Index = 13 Then  'Inversion table
        Image_Table(0).Visible = (CB_SAV(13).value = vbUnchecked)
        Image_Table(1).Visible = Not Image_Table(0).Visible
        Call WriteINI_Machine("table_" & gstrTable, "CB_SAV_13", CB_SAV(13).value)
    End If
    Call Sav_Table(Combo_Table)
    End Sub

Private Sub Cmd_Go_Click()
    Call Sav_Table(Combo_Table)
    Call maClasseHID.TableVerif

    Cmd_Stop.Visible = True
    
    Call maClasseHID.MotorOnOff(True)
    maVitesse = TB_Param(1)                 'Vitesse de d�part
    Label_Vitesse = maVitesse & " mm/s"
    Label_VitesseActuelle.Visible = True
    Label_Vitesse.Visible = True
    Label_Vitesse2.Visible = True
    Timer_Test.Enabled = True
End Sub

Private Sub Cmd_RetourOrigine_Click()
    Dim XG As Single, XD As Single, YG As Single, YD As Single
    Dim VmaxAvecAcc As Single
    Dim P1 As Point, P2 As Point
    
    'Retour � l'origine avec les fins de course
    If MsgBox(ReadINI_Trad(gLangue, "erreurs", 56), vbQuestion + vbYesNo) = vbYes Then
        Call RetourFDC(False, False)
    End If
    Call RetourFDC(True, True)
    Call RetourFDC(False, True)
    'On est cal� sur les fins de course, on d�place maintenant les chariots � la v�ritable origine
    XG = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_19", 0)
    XD = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_37", 0)
    YG = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_21", 0)
    YD = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_38", 0)
    VmaxAvecAcc = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_14", 0)
    P1.x = 0#
    P1.y = 0#
    If XD * XD + YD * YD > XG * XG + YG * YD Then
        P2.x = XD
        P2.y = YD
    Else
        P2.x = XG
        P2.y = YG
    End If
    
    ReDim Tab_Chariot(1 To 1)
    Tab_Chariot(1).P_abs(EMPLANTURE).x = XD
    Tab_Chariot(1).P_abs(EMPLANTURE).y = YD
    Tab_Chariot(1).P_abs(SAUMON).x = XG
    Tab_Chariot(1).P_abs(SAUMON).y = YG
    Tab_Chariot(1).Temps = Calcul_Temps_Points(P1, P2, VmaxAvecAcc)
    Call Envoi_Decoupe(Tab_Chariot, frmMain)
End Sub

Private Sub Cmd_Stop_Click()
    Timer_Test.Enabled = False
    Call maClasseHID.EnvoiUSB
    Call maClasseHID.MotorOnOff(False)
    Cmd_Stop.Visible = False
End Sub

Private Sub Cmd_TestPasPerdus_Click()
    Call maClasseHID.TableVerif
    frmPasPerdus.Show vbModal
End Sub

Private Sub cmdGestion_Click(Index As Integer)
    Dim NouveauNom As String
    Dim NomINI As String
    Dim Section() As String
    
    If Index = 1 Or Index = 2 Then      'Copier ou renommer
        NouveauNom = SaisieNom
        If NouveauNom = "" Then Exit Sub
        If NouveauNom = Combo_Table.Text Then Exit Sub
    End If
    
    NomINI = App.Path & "\" & App.Title & ".ini"
    Select Case Index
        Case 1  'Copier
            Sav_Table (Combo_Table.Text)
            Call CopySectionIni("table_" & Combo_Table.Text, "table_" & NouveauNom, NomINI)
            Call WriteINI("TableDefaut", NouveauNom)
        Case 2  'Renommer
            Sav_Table (Combo_Table.Text)
            Call RenameSectionIni("table_" & Combo_Table.Text, "table_" & NouveauNom, NomINI)
            Call WriteINI("TableDefaut", NouveauNom)
        Case 3  'Supprimer
            If MsgBox(ReadINI_Trad(gLangue, "erreurs", 51) & " " & Combo_Table.Text & " ?", vbCritical + vbQuestion + vbOKCancel, App.Title) = vbOK Then
                Call DeleteSectionIni("table_" & Combo_Table.Text, NomINI)
                AncienneTable = ""
                Combo_Table.ListIndex = 0
            End If
    End Select
    
    Call Chargt_Tables(Me)
End Sub

Private Sub Combo_SAV_Click(Index As Integer)
    Dim i As Integer
    If Not bChange Then Exit Sub
    If Index <> 0 Then Exit Sub
    
    Label2(1).Visible = False
    For i = 101 To 110
        CB_SAV(i).Visible = False
    Next i
    Label88(1).Visible = True
    For i = 1 To 3
        CB_SAV(i).Visible = True
    Next i
    
    'Combo de choix de l'interface
    Select Case Combo_SAV(0).ListIndex
        Case 0      'IPL5X
            gOptionLPT = 20
            Label2(1).Visible = True
            For i = 101 To 110
                CB_SAV(i).Visible = True
            Next i
            Label88(1).Visible = False
            For i = 1 To 3
                CB_SAV(i).Visible = False
            Next i
        Case 1      'LPT1
            gOptionLPT = 0
        Case 2      'LPT2
            gOptionLPT = 1
        Case 3      'LPT3
            gOptionLPT = 2
        Case 4      'XAVTRONIC
            gOptionLPT = 10
    End Select
    Call Sav_Table(Combo_Table)
End Sub

Private Sub Combo_Table_Click()
    Dim Index As Integer

    'Sauvegarde de l'ancienne table
    If AncienneTable <> "" Then
        If Not Sav_Table(AncienneTable) Then Exit Sub
    End If
    'Chargement de la nouvelle
    Call Chargt_Table
    Me.Caption = Combo_Table.Text
    AncienneTable = Combo_Table.Text
    gstrTable = Combo_Table.Text
    'Ecriture table par d�faut
    Call WriteINI("TableDefaut", gstrTable)
    'Charger la table de l'interface
    If Not maClasseHID.IsAvailable Then Exit Sub
    
    Index = maClasseHID.TableRecherche(gstrTable)
    If Index <> -1 Then Call maClasseHID.TableDefautEcrire(Index)
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    Call ChargeTrad(Me)
    Set gFenetreHID = frmParamMachine
    'Lecture table par d�faut
    gstrTable = ReadINI("TableDefaut", "MM2001")

    bDepl = False
    
    AncienneTable = ""
    
    For i = 1 To 10
        Combo_SAV(i).Clear
        Combo_SAV(i).AddItem ("Dir XG")
        Combo_SAV(i).AddItem ("Step XG")
        Combo_SAV(i).AddItem ("Dir YG")
        Combo_SAV(i).AddItem ("Step YG")
        Combo_SAV(i).AddItem ("Dir XD")
        Combo_SAV(i).AddItem ("Step XD")
        Combo_SAV(i).AddItem ("Dir YD")
        Combo_SAV(i).AddItem ("Step YD")
        Combo_SAV(i).AddItem ("Non connect�")
    Next i
    
    Call Chargt_Tables(Me)
    Call Chargt_Table
    
    TB_Param(1) = ReadINI_Num(Me.Name & "_VitDep", 5)
    TB_Param(2) = ReadINI_Num(Me.Name & "_VitArr", 15)
    TB_Param(3) = ReadINI_Num(Me.Name & "_Incr", 1)
    TB_Param(4) = ReadINI_Num(Me.Name & "_Depl", 50)
    
    For i = CB_Axe.LBound To CB_Axe.UBound
        CB_Axe(i) = ReadINI(Me.Name & "_Axe" & i, vbUnchecked)
    Next i

    Call TestGo
    Label_VitesseActuelle.Visible = False
    Label_Vitesse.Visible = False
    Label_Vitesse2.Visible = False
    
    Me.SSTab1.Tab = 0
    
    'Acc�l�ration
    Temps(1) = 0.021875
    For i = 2 To 9
        Temps(i) = Temps(i - 1) * 2
    Next i

    Call Sld_SAV_Change(0)
    
    Cmd_RetourOrigine.Enabled = (CB_SAV(12).value = vbChecked)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim i As Integer
    
    'Test des vitesses
    Call WriteINI(Me.Name & "_VitDep", TB_Param(1))
    Call WriteINI(Me.Name & "_VitArr", TB_Param(2))
    Call WriteINI(Me.Name & "_Incr", TB_Param(3))
    Call WriteINI(Me.Name & "_Depl", TB_Param(4))
        
    For i = CB_Axe.LBound To CB_Axe.UBound
        Call WriteINI(Me.Name & "_Axe" & i, CB_Axe(i).value)
    Next i

    Cancel = Not Sav_Table(AncienneTable)
    Call maClasseHID.TableVerif
End Sub

Private Function Sav_Table(gstrTable As String) As Boolean
    Dim ctl As Control
    
    On Error Resume Next
   
    For Each ctl In Me.Controls
        If ctl.Name = "TB_SAV" Then   'TextBox
            Call WriteINI_Machine("table_" & gstrTable, ctl.Name & "_" & ctl.Index, ctl.Text)
        End If
        If ctl.Name = "Combo_SAV" Then 'ComboBox
            Call WriteINI_Machine("table_" & gstrTable, ctl.Name & "_" & ctl.Index, ctl.ListIndex)
        End If
        If ctl.Name = "CB_SAV" Then   'CheckButton
            Call WriteINI_Machine("table_" & gstrTable, ctl.Name & "_" & ctl.Index, ctl.value)
        End If
        If ctl.Name = "Option_SAV" Then   'Boutons radio
            Call WriteINI_Machine("table_" & gstrTable, ctl.Name & "_" & ctl.Index, IIf(ctl.value, "1", "0"))
        End If
        If ctl.Name = "Sld_SAV" Then   'Sliders
            Call WriteINI_Machine("table_" & gstrTable, ctl.Name & "_" & ctl.Index, ctl.value)
        End If
    Next ctl
    'Nom de la table
    Call WriteINI_Machine("table_" & gstrTable, "COMBO_TABLE", gstrTable)
    ' Ecriture de la valeur "OptionLPT" : Choix du port parall�le
    Call WriteINI("OptionLPT", Str(gOptionLPT))
    gbOrientationInv = (CB_SAV(13) = vbChecked)
    
    wLargeurTable = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_23", 600)
    wDelta_Gauche = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_24", 0#)
    wDelta_Droit = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_25", 0#)

    Sav_Table = True
End Function

Private Sub Chargt_Table()
    Dim ctl As Control
    Dim i As Integer
    Dim Inter As Integer
    
    For Each ctl In Me.Controls
        If ctl.Name = "TB_SAV" Then   'TextBox
            ctl.Text = ReadINI_Machine_Num("table_" & Combo_Table, ctl.Name & "_" & ctl.Index, ctl.Text)
        End If
        If ctl.Name = "Combo_SAV" Then 'ComboBox
            Inter = ReadINI_Machine("table_" & Combo_Table, ctl.Name & "_" & ctl.Index, 0)
            If Inter > ctl.ListCount - 1 Then
                ctl.ListIndex = ctl.ListCount - 1
            Else
                ctl.ListIndex = Inter
            End If
        End If
        If ctl.Name = "CB_SAV" Then   'CheckButton
            ctl.value = ReadINI_Machine("table_" & Combo_Table, ctl.Name & "_" & ctl.Index, ctl.value)
            If ctl.Index = 13 Then  'Inversion table
                Image_Table(0).Visible = (CB_SAV(13).value = vbUnchecked)
                Image_Table(1).Visible = Not Image_Table(0).Visible
            End If
        End If
        If ctl.Name = "Option_SAV" Then   'Boutons radio
            ctl.value = (ReadINI_Machine("table_" & Combo_Table, ctl.Name & "_" & ctl.Index, 0) = "1")
        End If
    Next ctl
    For Each ctl In Me.Controls 'Il faut faire �a apr�s pour �viter de sauvegarder n'importe quoi
        If ctl.Name = "Sld_SAV" Then   'Sliders
            ctl.value = ReadINI_Machine("table_" & Combo_Table, ctl.Name & "_" & ctl.Index, ctl.value)
        End If
    Next ctl
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmMain.Timer_Refresh.Enabled = True
End Sub

Private Sub Sld_SAV_Change(Index As Integer)
    If Index <> 0 Then Exit Sub
    'Slider de l'acc�l�ration
    t = Temps(Sld_SAV(0).value)
    Label_Accel.Caption = "On double la vitesse en " & Format(t, "###0.00") & " secondes"
    TB_Pente = Sld_SAV(0)
    Call Trace_Accel
    Call Sav_Table(Combo_Table)
End Sub

Sub Chargt_Tables(f As Form)
    Dim Section() As String
    Dim i As Integer
    Dim maTable As String
    
    'Lecture table par d�faut
    gstrTable = ReadINI("TableDefaut", "MM2001")
    
    f.Combo_Table.Clear
    Call ListeSectionIni(App.Title & ".ini", Section)  '-- Param�tre Section pass� ByRef
    For i = LBound(Section) To UBound(Section)
        maTable = Section(i)
        If Left$(maTable, 6) = "table_" Then
            maTable = Mid$(maTable, 7, Len(maTable) - 6)
            f.Combo_Table.AddItem maTable
        End If
    Next
    On Error Resume Next        'Lancement sans .ini
    For i = 0 To f.Combo_Table.ListCount
        If f.Combo_Table.List(i) = gstrTable Then
            f.Combo_Table.ListIndex = i
        End If
    Next i
End Sub

Private Sub TB_Param_Change(Index As Integer)
    If TB_Param(Index) = "" Then Exit Sub
    If Not IsNumeric(TB_Param(Index)) Then Exit Sub
    Call TestGo
End Sub

Private Sub TB_Param_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Pente_Change()
    If Val(TB_Pente.Text) > Sld_SAV(0).Min And Val(TB_Pente.Text) < Sld_SAV(0).Max Then
        Sld_SAV(0).value = Val(TB_Pente.Text)
    End If
    Call maClasseHID.TableVerif
End Sub

Private Sub TB_SAV_Validate(Index As Integer, Cancel As Boolean)
    Dim Temp As Single
    If TB_SAV(Index) = "" Then Exit Sub
    Select Case Index
        Case 3          'Nb pas/tour X
            Temp = CDbl(Replace(TB_SAV(3), ".", SepDecimal))
            If Temp <> 0 Then TB_SAV(6) = Round(360# / Temp, 2)
        Case 4          'Nb pas/tour Y
            Temp = CDbl(Replace(TB_SAV(4), ".", SepDecimal))
            If Temp <> 0 Then TB_SAV(7) = Round(360# / Temp, 2)
        Case 5          'Nb pas/tour Z
            Temp = CDbl(Replace(TB_SAV(5), ".", SepDecimal))
            If Temp <> 0 Then TB_SAV(8) = Round(360# / Temp, 2)
        Case 6          'Deg/pas X
            Temp = CDbl(Replace(TB_SAV(6), ".", SepDecimal))
            If Temp <> 0 Then TB_SAV(3) = Round(360# / Temp, 2)
        Case 7          'Deg/pas Y
            Temp = CDbl(Replace(TB_SAV(7), ".", SepDecimal))
            If Temp <> 0 Then TB_SAV(4) = Round(360# / Temp, 2)
        Case 6          'Deg/pas Z
            Temp = CDbl(Replace(TB_SAV(8), ".", SepDecimal))
            If Temp <> 0 Then TB_SAV(5) = Round(360# / Temp, 2)
        Case 30         'TimeOut
            TB_SAV(30) = Min(127, Val(TB_SAV(30)))
    End Select

    If Index = 30 Then      'Timeout
        If TB_SAV(30) > 127 Then
            TB_SAV(30) = 127
            Beep
        End If
    End If
    Select Case Index
        Case 0 To 2
            TB_SAV(Index) = Format$(Round(CDbl(TB_SAV(Index)), 4), "#0.0000")
        Case 6 To 8
            TB_SAV(Index) = Format$(Round(CDbl(TB_SAV(Index)), 2), "#0.00")
        Case 12 To 27
            TB_SAV(Index) = Format$(Round(CDbl(TB_SAV(Index)), 1), "#0.0")
        Case 3 To 5, 9 To 11, 28, 31 To 32
            TB_SAV(Index) = Format$(Round(CDbl(TB_SAV(Index)), 0), "#0")
    End Select
    Call Sav_Table(Combo_Table)
End Sub

Private Sub TB_SAV_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_SAV_Validate(Index, False)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Function SaisieNom() As String
    SaisieNom = "NewTable-"
    While Len(SaisieNom) > 8
        If SaisieNom <> "NewTable-" Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 50), vbCritical
        End If
        SaisieNom = InputBox(ReadINI_Trad(gLangue, "erreurs", 52), Combo_Table.Text, SaisieNom)
    Wend
    SaisieNom = UCase$(SaisieNom)
End Function

Private Sub Timer_Test_Timer()
    Dim XG As Double
    Dim XD As Double
    Dim YG As Double
    Dim YD As Double
    Dim TpsReel As Single
    Dim mmTour As Double
    Dim P1 As Point, P2 As Point
    
    Call maClasseHID.DemandeInformation
    If maClasseHID.Step_Processed Then
        'D�placement en cours
        Exit Sub
    End If
    
    If maVitesse > TB_Param(2) Then           'Vitesse d'arriv�e
        Timer_Test.Enabled = False
        Cmd_Stop.Visible = False
        Exit Sub
    End If
    
    mmTour = CDbl(Replace(TB_SAV(IIf(CB_Axe(0).value Or CB_Axe(0).value, 0, 1)), ".", SepDecimal))
    
    If CB_SAV(14).value = vbChecked Then 'Pause interm�diaire
        If MsgBox(ReadINI_Trad(gLangue, "erreurs", 4), vbQuestion + vbYesNo) = vbNo Then
            Timer_Test.Enabled = False
            Cmd_Stop.Visible = False
            Label_VitesseActuelle.Visible = False
            Label_Vitesse.Visible = False
            Exit Sub
        End If
    End If
    
    Label_Vitesse = maVitesse & " mm/s"
    'On a une vitesse en mm/seconde, il faut la calculer en t/mn
    Label_Vitesse2 = maVitesse / mmTour * 60# & " t/mn"
    
    'Parcours
    XG = 0#
    XD = 0#
    YG = 0#
    YD = 0#
    If CB_Axe(0).value Then XG = TB_Param(4)
    If CB_Axe(1).value Then XD = TB_Param(4)
    If CB_Axe(2).value Then YG = TB_Param(4)
    If CB_Axe(3).value Then YD = TB_Param(4)
    
    Call maClasseHID.EnvoiUSB
    P1.x = 0#
    P1.y = 0#
    P2.x = Max(XG, XD)
    P2.y = Max(YG, YD)
    Call maClasseHID.CommandeDATAmm(XD, YD, XG, YG, Calcul_Temps_Points(P1, P2, maVitesse), TpsReel)
    Call maClasseHID.CommandeDATAmm(-XD, -YD, -XG, -YG, Calcul_Temps_Points(P1, P2, maVitesse), TpsReel)
    Call maClasseHID.CommandeDATAend

    maVitesse = Round(maVitesse + TB_Param(3), 2) 'Incr�ment
End Sub

Private Sub TestGo()
    Cmd_GO.Enabled = False
    If TB_Param(4) = 0 Then Exit Sub
        
    Dim i As Integer
    
    For i = CB_Axe.LBound To CB_Axe.UBound
        If CB_Axe(i).value = vbChecked Then
            Cmd_GO.Enabled = True
            Exit Sub
        End If
    Next i
End Sub

Private Sub Trace_Accel()
    Dim i As Integer

    'Trac� du rep�re
    With PB_Accel
        .Cls
        .DrawStyle = vbSolid  ' lignes pleines
        .DrawWidth = 1        ' �paisseur 1 pixel
        .ForeColor = vbBlack   'couleur
    
        PB_Accel.Line (300, 3500)-(7800, 3500)
        PB_Accel.Line -Step(-200, -60)
        PB_Accel.Line -Step(0, 120)
        PB_Accel.Line -Step(200, -60)
    
        PB_Accel.Line (300, 3500)-(300, 30)
        PB_Accel.Line -Step(-60, 200)
        PB_Accel.Line -Step(120, 0)
        PB_Accel.Line -Step(-60, -200)
    
        'Trac� de toutes les rampes en pointill�s
        .DrawStyle = vbDot  ' lignes pointill�es
        .DrawWidth = 1        ' �paisseur 1 pixel
        .ForeColor = vbBlack   'couleur
        'ligne horizontale
        PB_Accel.Line (900 + Temps(2) * 450, 1000)-(900 + Temps(2) * 450 + (13 - 2 * Temps(2)) * 450, 1000)
        'pentes
        For i = 9 To 1 Step -1
            PB_Accel.Line (900, 3000)-(900 + Temps(i) * 450, 1000)
            PB_Accel.Line (900 + Temps(i) * 450 + (13 - 2 * Temps(i)) * 450, 1000)-(900 + Temps(i) * 450 + (13 - 2 * Temps(i)) * 450 + Temps(i) * 450, 3000)
        Next i
    
        'Trac� de l'acc�l�ration s�lectionn�e
        .DrawStyle = vbSolid  ' lignes pleines
        .DrawWidth = 2        ' �paisseur 2 pixels
        .ForeColor = vbRed   'couleur
    
        PB_Accel.Line (300, 3000)-(900, 3000)
        PB_Accel.Line (900, 3000)-Step(t * 450, -2000)
        PB_Accel.Line -Step((13 - 2 * t) * 450, 0)
        PB_Accel.Line -Step(t * 450, 2000)
        PB_Accel.Line -Step(600, 0)
    End With
End Sub
