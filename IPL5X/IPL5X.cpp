// IPL5X.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "IPL5X.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
HWND hWnd3D=NULL;								// 3D dialog box
HWND hWndMain=NULL;								// Main window
HWND hStatus=NULL;								// Status bar
HWND hWndTab=NULL;								// Tab control
HWND hWndAbout=NULL;							// About control
HWND hWndCut=NULL;								// Cut control
HWND hWndManuel=NULL;							// Manuel control
HWND hWndCurrent=NULL;							// Current window

TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
char szCutFileName[MAX_PATH] = "gmfc.cut";				// Current opened cut file

int	Data_Cut_Nbr=0,Poly_Cut_Nbr=1,Factor=100;
char Data_Cut_Name[256];
int AngleX=-15,AngleY=-10,Zoom=150,MoveX=150,MoveY=0;
int MousePosX=0,MousePosY=0,MouseFlag=0;
int Simul=0,PWMmax;
float Xmax,Ymax;
long int SDONE[5];

struct POLY {
	float SizeX;
	float SizeY;
	float SizeZ;
	float PosX;
	float PosY;
	float PosZ;
} Poly;

struct FIL {
	float XG;
	float YG;
	float XD;
	float YD;
} Fil;


struct CUT {
	float XG;
	float YG;
	float XD;
	float YD;
	float Z;
	float TIME;
	int	PWM;
} *Data_Cut;

struct CUT_INT {
	long int XG;
	long int YG;
	long int XD;
	long int YD;
	long int Z;
	long int TIME;
	long int TIME_DEC;
	int PWM;
	int FREQ_DEC;
} *Data_Cut_Int;

struct {
	unsigned int M_ON,M_OFF,ACCEL_TYPE,FREQ;
} Interface;

struct {
	char NAME[100];
	float SIZE_X,SIZE_Y,SIZE_Z;
	struct {
		unsigned int PAS_TOUR,PAS_PERDU,STEP,DIR,LINK;
		float MM_TOUR,VIT_SS_ACCEL,VIT_AV_ACCEL;
		BOOL SENS;
		char NAME[10];
	} M[5];
} Meca;

struct POINTD {
    float x, y;
};

DWORD HID_vendorID=0x04D8, HID_productID=0x00AA;
int HID_input_len, HID_output_len;


//---------------------------------------
// DIRECT 3D
//---------------------------------------
LPDIRECT3D9             extern g_pD3D; // Used to create the D3DDevice
LPDIRECT3DDEVICE9       extern g_pd3dDevice; // Our rendering device
LPDIRECT3DVERTEXBUFFER9 extern g_pVB; // Buffer to hold vertices

// A structure for our custom vertex type
struct CUSTOMVERTEX
{
    FLOAT x, y, z;      // The untransformed, 3D position for the vertex
    DWORD color;        // The vertex color
};

//---------------------------------------
// HID
//---------------------------------------
HANDLE extern HID_handle;
HANDLE extern HID_EventObject;
OVERLAPPED extern HIDOverlapped;
char extern HIDDevicePathName[256];
GUID extern hidGUID;

// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)
//---------------------------------------

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	Dlg_3D(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	Dlg_About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	Dlg_Manuel(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	Dlg_Cut(HWND, UINT, WPARAM, LPARAM);
VOID				LoadCutFile(CUT *);
int					ConvertCutInt();
void				SendCutInt();
void				SendInt(int);
int					CountStrLines(LPCTSTR);
void				ReadIni();
void Move_Step(DWORD NBR,DWORD *S,BOOL FLAG_ACC);
void Move_relative(float T,float D0,float D1,float D2,float D3,float D4,BOOL FLAG_ACC);

// Forward declarations for 3D:
HRESULT	extern		InitD3D(HWND);
HRESULT	extern		InitGeometry();
VOID	extern		Cleanup();
VOID	extern		SetupMatrices();
VOID	extern		Render();
// Forward declarations for HID:
LRESULT	extern		Main_OnDeviceChange(WPARAM wParam, LPARAM lParam);
BOOL	extern		DeviceNameMatch(LPARAM lParam);
void	extern		RegisterForDeviceNotifications(HWND);
VOID	extern		DisconnectHID();
BOOL	extern		ConnectHID(DWORD, DWORD, DWORD);
BOOL	extern		WriteHID(const BYTE *bytes, int nBuffLen);
int		extern		ReadHID(BYTE *bytes, int nBuffLen);


int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_IPL5X, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_IPL5X));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (hWndCurrent==NULL||!IsDialogMessage(hWndCurrent, &msg))
        {
			if (hWndManuel==NULL||!IsDialogMessage(hWndManuel, &msg))
		    {
				if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_IPL5X));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW);
	wcex.lpszMenuName	= NULL; //MAKEINTRESOURCE(IDC_IPL5X);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int	statwidths[] = {10, -1};
	int i;
	LPNMHDR lpnmhdr;
//	PAINTSTRUCT ps;
//	HDC hdc;
	TCITEM tie; 
	RECT rcClient;
	POINT	Mouse;
	MINMAXINFO *lpMMI;

	switch (message)
	{
/*	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
			case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, Dlg_About);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			case ID_FILE_OPEN:
				break;
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
*/
	case WM_CREATE:
		ReadIni();
		Fil.XD=0.0f;
		Fil.YD=0.0f;
		Fil.XG=0.0f;
		Fil.YG=0.0f;
		for(i=0;i!=5;i++)
			SDONE[i]=0;
		InitCommonControls();
		hStatus = CreateWindowEx(0, STATUSCLASSNAME, NULL,
	        WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0,
		    hWnd, (HMENU)IDC_MAIN_STATUS, hInst, NULL);
        SendMessage(hStatus, SB_SETPARTS, 2, (LPARAM)statwidths);
		SendMessage(hStatus, SB_SETTEXT, 1, (LPARAM)"USB: Getting status");

   		HidD_GetHidGuid (&hidGUID);
		RegisterForDeviceNotifications(hWnd);
		ConnectHID(HID_vendorID, HID_productID,0);

		hWnd3D = CreateDialog(hInst, MAKEINTRESOURCE(IDD_3D), hWnd, Dlg_3D);
		hWndAbout = CreateDialog(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, Dlg_About);
		hWndManuel = CreateDialog(hInst, MAKEINTRESOURCE(IDD_MANUEL), hWnd, Dlg_Manuel);
		hWndTab = CreateWindow(WC_TABCONTROL, "",WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE | WS_TABSTOP, 100, 0, 100, 100, hWnd, NULL, hInst, NULL); 
		hWndCurrent = hWndCut = CreateDialog(hInst, MAKEINTRESOURCE(IDD_CUT), hWnd, Dlg_Cut);
	    tie.mask = TCIF_TEXT | TCIF_IMAGE; 
	    tie.iImage = -1; 
	    tie.pszText = "D�coupe"; 
	    TabCtrl_InsertItem(hWndTab, 0, &tie); 
	    tie.pszText = "Origine"; 
	    TabCtrl_InsertItem(hWndTab, 1, &tie); 
	    tie.pszText = "Moteurs"; 
	    TabCtrl_InsertItem(hWndTab, 2, &tie); 
	    tie.pszText = "M�canique"; 
	    TabCtrl_InsertItem(hWndTab, 3, &tie); 
	    tie.pszText = "A propos..."; 
	    TabCtrl_InsertItem(hWndTab, 4, &tie); 
		LoadCutFile(Data_Cut);
		break;
//	case WM_PAINT:
//		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
//		EndPaint(hWnd, &ps);
//		break;
	case WM_GETMINMAXINFO:
		lpMMI=(MINMAXINFO*)lParam;
		lpMMI->ptMinTrackSize.x=1000;
		lpMMI->ptMinTrackSize.y=700;
		break;
	case WM_SIZE:
		GetClientRect(hWnd, &rcClient);
        statwidths[0]=rcClient.right-rcClient.left-123;
		SendMessage(hStatus, SB_SETPARTS, 2, (LPARAM)statwidths);
		SendMessage(hStatus, WM_SIZE, 0, 0);
		SetWindowPos(hWnd3D,HWND_TOP,0,2,rcClient.right-400,rcClient.bottom-20,SWP_SHOWWINDOW);
		SetWindowPos(hWndTab,HWND_TOP,rcClient.right-400+10,2,380,400,SWP_SHOWWINDOW);
		SetWindowPos(hWndAbout,HWND_TOP,rcClient.right-400+15,28,380-15,372,NULL);
		SetWindowPos(hWndCut,HWND_TOP,rcClient.right-400+15,28,380-15,372,NULL);
		SetWindowPos(hWndManuel,HWND_TOP,rcClient.right-401,406,400,245,SWP_SHOWWINDOW);
		break;
	case WM_NOTIFY:
		lpnmhdr = (LPNMHDR)lParam;
		if (lpnmhdr->code == TCN_SELCHANGE)
		{
			switch(TabCtrl_GetCurSel(lpnmhdr->hwndFrom))
			{
				case 0:
					ShowWindow(hWndAbout,SW_HIDE);
					//ShowWindow(hWndManuel,SW_HIDE);
					ShowWindow(hWndCut,SW_SHOW);
					hWndCurrent=hWndCut;
					break;
				case 1:
					ShowWindow(hWndCut,SW_HIDE);
					ShowWindow(hWndAbout,SW_HIDE);
					//ShowWindow(hWndManuel,SW_SHOW);
					//hWndCurrent=hWndManuel;
					break;
				case 2:
					ShowWindow(hWndCut,SW_HIDE);
					ShowWindow(hWndAbout,SW_HIDE);
					//ShowWindow(hWndManuel,SW_HIDE);
					break;
				case 3:
					ShowWindow(hWndCut,SW_HIDE);
					ShowWindow(hWndAbout,SW_HIDE);
					//ShowWindow(hWndManuel,SW_HIDE);
					break;
				case 4:
					ShowWindow(hWndCut,SW_HIDE);
					//ShowWindow(hWndManuel,SW_HIDE);
					ShowWindow(hWndAbout,SW_SHOW);
					hWndCurrent=hWndAbout;
					break;
			}
		}
		break;
	case WM_DESTROY:
		DisconnectHID();
		if(hWnd3D!=NULL)
			DestroyWindow(hWnd3D);
		if(hWndAbout!=NULL)
			DestroyWindow(hWndAbout);
		if(hWndCut!=NULL)
			DestroyWindow(hWndCut);
		if(Data_Cut!=NULL)
			GlobalFree(Data_Cut);
		if(Data_Cut_Int!=NULL)
			GlobalFree(Data_Cut_Int);
		PostQuitMessage(0);
		break;
	case WM_MOUSEWHEEL:
		GetCursorPos(&Mouse);
		ScreenToClient(hWnd3D,&Mouse);
		GetClientRect(hWnd3D, &rcClient);
		if(Mouse.x<0||Mouse.y<0||Mouse.x>rcClient.right||Mouse.y>rcClient.bottom)
			break;
		Zoom+=GET_WHEEL_DELTA_WPARAM(wParam)/10;
		if(Zoom<=50)
			Zoom=50;
		Render();
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK Dlg_About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK)
		{
			ShellExecute(hDlg, "open", "http://www.aeropassion.net/aeromicro",NULL,NULL, SW_SHOWNORMAL) ;
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

// Message handler for Manuel box.
INT_PTR CALLBACK Dlg_Manuel(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	char Text[100];
	float T=0,D1=0,D2=0,D3=0,D4=0,D5=0,V=0;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		CheckDlgButton(hDlg,IDC_MAN1_1,BST_CHECKED);
		CheckDlgButton(hDlg,IDC_MAN1_10,BST_UNCHECKED);
		CheckDlgButton(hDlg,IDC_MAN1_100,BST_UNCHECKED);
		SendDlgItemMessage(hDlg,IDC_MAN1_MM,BM_SETCHECK,TRUE,0L);
		SendDlgItemMessage(hDlg,IDC_MAN2_MM,BM_SETCHECK,TRUE,0L);
		SetWindowText(GetDlgItem(hDlg,IDC_MAN_SPEED),"2.00");
		SetWindowText(GetDlgItem(hDlg,IDC_MAN2_X1),"0.00");
		SetWindowText(GetDlgItem(hDlg,IDC_MAN2_X2),"0.00");
		SetWindowText(GetDlgItem(hDlg,IDC_MAN2_Y1),"0.00");
		SetWindowText(GetDlgItem(hDlg,IDC_MAN2_Y2),"0.00");
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
			case IDC_MAN_FAST:
				if(IsDlgButtonChecked(hDlg,IDC_MAN_FAST)==BST_CHECKED)
					SendDlgItemMessage(hDlg,IDC_MAN_SPEED,WM_ENABLE,FALSE,0L);
				else
					SendDlgItemMessage(hDlg,IDC_MAN_SPEED,WM_ENABLE,TRUE,0L);
				return (INT_PTR)TRUE;
			case IDC_MAN1_EQU:
				if(IsDlgButtonChecked(hDlg,IDC_MAN1_EQU)==BST_CHECKED)
				{
					EnableWindow(GetDlgItem(hDlg,IDC_MAN1_X2P),FALSE);
					EnableWindow(GetDlgItem(hDlg,IDC_MAN1_Y2P),FALSE);
					EnableWindow(GetDlgItem(hDlg,IDC_MAN1_X2M),FALSE);
					EnableWindow(GetDlgItem(hDlg,IDC_MAN1_Y2M),FALSE);
				}
				else
				{
					EnableWindow(GetDlgItem(hDlg,IDC_MAN1_X2P),TRUE);
					EnableWindow(GetDlgItem(hDlg,IDC_MAN1_X2M),TRUE);
					EnableWindow(GetDlgItem(hDlg,IDC_MAN1_Y2P),TRUE);
					EnableWindow(GetDlgItem(hDlg,IDC_MAN1_Y2M),TRUE);
				}
				return (INT_PTR)TRUE;
			case IDC_MAN2_EQU:
				if(IsDlgButtonChecked(hDlg,IDC_MAN2_EQU)==BST_CHECKED)
				{
					SendDlgItemMessage(hDlg,IDC_MAN2_X2,WM_ENABLE,FALSE,0L);
					SendDlgItemMessage(hDlg,IDC_MAN2_Y2,WM_ENABLE,FALSE,0L);
				}
				else
				{
					SendDlgItemMessage(hDlg,IDC_MAN2_X2,WM_ENABLE,TRUE,0L);
					SendDlgItemMessage(hDlg,IDC_MAN2_Y2,WM_ENABLE,TRUE,0L);
				}
				return (INT_PTR)TRUE;
			case IDC_MAN1_1:
				CheckDlgButton(hDlg,IDC_MAN1_1,BST_CHECKED);
				CheckDlgButton(hDlg,IDC_MAN1_10,BST_UNCHECKED);
				CheckDlgButton(hDlg,IDC_MAN1_100,BST_UNCHECKED);
				return (INT_PTR)TRUE;
			case IDC_MAN1_10:
				CheckDlgButton(hDlg,IDC_MAN1_1,BST_UNCHECKED);
				CheckDlgButton(hDlg,IDC_MAN1_10,BST_CHECKED);
				CheckDlgButton(hDlg,IDC_MAN1_100,BST_UNCHECKED);
				return (INT_PTR)TRUE;
			case IDC_MAN1_100:
				CheckDlgButton(hDlg,IDC_MAN1_1,BST_UNCHECKED);
				CheckDlgButton(hDlg,IDC_MAN1_10,BST_UNCHECKED);
				CheckDlgButton(hDlg,IDC_MAN1_100,BST_CHECKED);
				return (INT_PTR)TRUE;
			case IDC_MAN1_X1P:
				SendMessage(hDlg,DM_SETDEFID,IDC_MAN1_STOP,0L);
				SendDlgItemMessage(hDlg,IDC_MAN2_GOSTOP,BM_SETSTYLE,NULL,TRUE);
				return (INT_PTR)TRUE;
			case IDC_MAN1_X1M:
				SendMessage(hDlg,DM_SETDEFID,IDC_MAN1_STOP,0L);
				SendDlgItemMessage(hDlg,IDC_MAN2_GOSTOP,BM_SETSTYLE,NULL,TRUE);
				return (INT_PTR)TRUE;
			case IDC_MAN1_Y1P:
				SendMessage(hDlg,DM_SETDEFID,IDC_MAN1_STOP,0L);
				SendDlgItemMessage(hDlg,IDC_MAN2_GOSTOP,BM_SETSTYLE,NULL,TRUE);
				return (INT_PTR)TRUE;
			case IDC_MAN1_Y1M:
				SendMessage(hDlg,DM_SETDEFID,IDC_MAN1_STOP,0L);
				SendDlgItemMessage(hDlg,IDC_MAN2_GOSTOP,BM_SETSTYLE,NULL,TRUE);
				return (INT_PTR)TRUE;
			case IDC_MAN1_X2P:
				SendMessage(hDlg,DM_SETDEFID,IDC_MAN1_STOP,0L);
				SendDlgItemMessage(hDlg,IDC_MAN2_GOSTOP,BM_SETSTYLE,NULL,TRUE);
				return (INT_PTR)TRUE;
			case IDC_MAN1_X2M:
				SendMessage(hDlg,DM_SETDEFID,IDC_MAN1_STOP,0L);
				SendDlgItemMessage(hDlg,IDC_MAN2_GOSTOP,BM_SETSTYLE,NULL,TRUE);
				return (INT_PTR)TRUE;
			case IDC_MAN1_Y2P:
				SendMessage(hDlg,DM_SETDEFID,IDC_MAN1_STOP,0L);
				SendDlgItemMessage(hDlg,IDC_MAN2_GOSTOP,BM_SETSTYLE,NULL,TRUE);
				return (INT_PTR)TRUE;
			case IDC_MAN1_Y2M:
				SendMessage(hDlg,DM_SETDEFID,IDC_MAN1_STOP,0L);
				SendDlgItemMessage(hDlg,IDC_MAN2_GOSTOP,BM_SETSTYLE,NULL,TRUE);
				return (INT_PTR)TRUE;
			case IDC_MAN1_STOP:
				return (INT_PTR)TRUE;
			case IDC_MAN2_GOSTOP:
				GetWindowText(GetDlgItem(hDlg,IDC_MAN_SPEED),Text,20);
				V=(float)atof(Text);
				GetWindowText(GetDlgItem(hDlg,IDC_MAN2_X1),Text,20);
				D1=(float)atof(Text);
				T=D1/V;
				Move_relative(T,D1,D2,D3,D4,D5,FALSE);
				return (INT_PTR)TRUE;
			case IDC_MAN2_X1:
			case IDC_MAN2_Y1:
			case IDC_MAN2_X2:
			case IDC_MAN2_Y2:
				if(HIWORD(wParam)==EN_CHANGE)
				{
					SendMessage(hDlg,DM_SETDEFID,IDC_MAN2_GOSTOP,0L);
					SendDlgItemMessage(hDlg,IDC_MAN1_STOP,BM_SETSTYLE,NULL,TRUE);
				}
				if(HIWORD(wParam)==EN_KILLFOCUS)
				{
					GetWindowText((HWND)lParam, Text,20);
					sprintf_s(Text,10,"%.2f",(float)atof(Text));
					SetWindowText((HWND)lParam,Text);
				}
				return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}


/* T  in sec
   D1,D2,D3,D4,D5 absolute distance in mm
   FLAG_ACC=TRUE -> enable acceleration   */
void Move_relative(float T,float D0,float D1,float D2,float D3,float D4,BOOL FLAG_ACC)
{
/*	float D[5],TMP;
	DWORD S[5],NBR;
	int i;

	D[0]=D0;D[1]=D1;D[2]=D2;D[3]=D3;D[4]=D4;

	// Translate from mm to step
	for(i=0;i!=5;i++)
		S[i]=(DWORD)((D[i]*Meca.M[i].PAS_TOUR)/Meca.M[i].MM_TOUR);
	TMP=abs(T)*(float)Interface.FREQ+0.5f;
	NBR=(DWORD)TMP;

	Move_Step(NBR,S,FLAG_ACC);
*/
}

void Move_Step(DWORD NBR,DWORD *S,BOOL FLAG_ACC)
{
/*	BOOL flag, flag_acc;
	int i;
	float	TMP;

	flag_acc=FALSE;
	// Verify speed
	for(i=0;i!=5;i++)
	{
		TMP=0.5f+((float)Interface.FREQ*Meca.M[i].MM_TOUR*abs((float)S[i]))/(Meca.M[i].PAS_TOUR*Meca.M[i].VIT_AV_ACCEL);
		NBR_AV_ACC=(DWORD)TMP;
		TMP=0.5f+((float)Interface.FREQ*Meca.M[i].MM_TOUR*abs((float)S[i]))/(Meca.M[i].PAS_TOUR*Meca.M[i].VIT_SS_ACCEL);
		NBR_SS_ACC=(DWORD)TMP;
		
		if(NBR>NBR_AV_ACC)
			NBR=NBR_AV_ACC;
		if(NBR>NBR_SS_ACC & FLAG_ACC==FALSE)
			NBR=NBR_SS_ACC;
	}
	// Verify acceleration
	ACC=12000000/Interface.FREQ;
	for(i=0;i!=5;i++)
	{
		TMP=0.5f+((float)Interface.FREQ*Meca.M[i].MM_TOUR*abs((float)S[i]))/(Meca.M[i].PAS_TOUR*Meca.M[i].VIT_SS_ACCEL);
		NBR_SS_ACC=(DWORD)TMP;
		
		if(NBR>NBR_SS_ACC & FLAG_ACC==TRUE)
		{
			flag_acc=TRUE;
		}

	}	 

*/
}



/* T  in sec
   D1,D2,D3,D4,D5 absolute distance in mm
   FLAG_ACC=TRUE -> enable acceleration   
void Move_absolute(float T,float D1,float D2,float D3,float D4,float D5,BOOL FLAG_ACC)
{
	BOOL flag, flag_acc,i;
	float V[5],D[5];
	float TC,TMP;
	unsigned int ACC[5],ACCEL;
	long int S[5],S1[5],NBR,NBR1;

	D[0]=D1;D[1]=D2;D[2]=D3;D[3]=D4;D[4]=D5;
	// Verify speed and see if acceleration is needed
	TC=T;
	do
	{
		flag=FALSE;
		flag_acc=FALSE;
		for(i=0;i!=5;i++)
		{
			ACC[i]=12000000/Interface.FREQ;
			V[i]=D[i]/TC;
			if(V[i]>Meca.M[i].VIT_AV_ACCEL && FLAG_ACC==TRUE)
			{
				flag=TRUE;
				TC=D[i]/Meca.M[i].VIT_AV_ACCEL;
			}
			if(V[i]>Meca.M[i].VIT_SS_ACCEL && FLAG_ACC==FALSE)
			{
				flag=TRUE;
				TC=D[i]/Meca.M[i].VIT_SS_ACCEL;
			}
			if(V[i]>Meca.M[i].VIT_SS_ACCEL)
			{
				flag_acc=TRUE;
				ACC[i]=(unsigned int)((V[i]*12000000)/(Meca.M[i].VIT_SS_ACCEL*Interface.FREQ));
			}
			TMP=TC*(float)Interface.FREQ;
			NBR=(long int)TMP+1;
			TC=(float)NBR/(float)Interface.FREQ;
		}
	}
	while(flag==TRUE);
	
	// Find the bigger ACC value
	if(flag_acc==TRUE)
	{
		ACCEL=0;
		for(i=0;i!=5;i++)
			if(ACC[i]>ACCEL)
				ACCEL=ACC[i];
	}

	// Translate from mm to step
	for(i=0;i!=5;i++)
	{
		S[i]=(long int)((D[i]*Meca.M[i].PAS_TOUR)/Meca.M[i].MM_TOUR)-SDONE[i];
		SDONE[i]+=S[i];
	}

	NBR1=NBR;
	do
	{
		if(NBR-0x3FFFFF<0)
			NBR1=0x3FFFFF;
		else
			NBR1=NBR;
		for(i=0;i!=5;i++)
			S1[i]=(NBR1*S[i])/NBR;
		
		Send_Small(NBR1,S1[0],S1[1],S1[2],S1[3],S1[4]);

		for(i=0;i!=5;i++)
			S[i]-=S1[i];
		NBR-=NBR1;
	}
	while(NBR>0);
}
*/


// Message handler for Cut box.
INT_PTR CALLBACK Dlg_Cut(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	OPENFILENAME ofn;
	UNREFERENCED_PARAMETER(lParam);
	char Text[100];
	int i;

	#define IDT_TIMER1 101

	switch (message)
	{
	case WM_INITDIALOG:
		sprintf_s(Text,10,"%.2f",Poly.SizeX);
		SetWindowText(GetDlgItem(hDlg,IDC_CUT_SIZEX),Text);
		sprintf_s(Text,10,"%.2f",Poly.SizeY);
		SetWindowText(GetDlgItem(hDlg,IDC_CUT_SIZEY),Text);
		sprintf_s(Text,10,"%.2f",Poly.SizeZ);
		SetWindowText(GetDlgItem(hDlg,IDC_CUT_SIZEZ),Text);
		sprintf_s(Text,10,"%.2f",Poly.PosX);
		SetWindowText(GetDlgItem(hDlg,IDC_CUT_POSX),Text);
		sprintf_s(Text,10,"%.2f",Poly.PosY);
		SetWindowText(GetDlgItem(hDlg,IDC_CUT_POSY),Text);
		sprintf_s(Text,10,"%.2f",Poly.PosZ);
		SetWindowText(GetDlgItem(hDlg,IDC_CUT_POSZ),Text);
		SendDlgItemMessage(hDlg,IDC_CUT_TIME, TBM_SETRANGE, (WPARAM)FALSE, (LPARAM)MAKELONG(1,200));
		SendDlgItemMessage(hDlg,IDC_CUT_TIME, TBM_SETPOS, (WPARAM)TRUE, (LPARAM)100);
		SetFocus(GetDlgItem(hDlg,IDC_CUT_OPEN));
		//delete all strings: SendMessage(GetDlgItem(hDlg,IDC_CUT_SLOT),CB_RESETCONTENT,0,0L);
		//get selected entry: Index=(int)SendMessage(GetDlgItem(hDlg,IDC_CUT_SLOT),CB_GETCURSEL,0,0L);
		for(i=1;i!=8;i++)
		{
			sprintf_s(Text,10,"%d: -",i);
			SendDlgItemMessage(hDlg,IDC_CUT_SLOT,CB_ADDSTRING,0,(LPARAM)Text);
		}
		SendDlgItemMessage(hDlg,IDC_CUT_SLOT, CB_SETCURSEL, 0, 0L);
		return (INT_PTR)TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
			case IDC_CUT_SIZEX:
				if(HIWORD(wParam)==EN_CHANGE)
				{
					GetWindowText((HWND)lParam, Text,20);
					Poly.SizeX=(float)atof(Text);
					if(Poly.SizeX<1)
						Poly.SizeX=1;
					if(Poly.SizeX+Poly.PosX>Meca.SIZE_X)
						Poly.SizeX=Meca.SIZE_X-Poly.PosX;
					if( SUCCEEDED( InitGeometry() ) )
						Render();
				}
				if(HIWORD(wParam)==EN_KILLFOCUS)
				{
					sprintf_s(Text,10,"%.2f",Poly.SizeX);
					SetWindowText(GetDlgItem(hDlg,IDC_CUT_SIZEX),Text);
					if(GetFocus()==GetDlgItem(hDlg,IDC_CUT_TIME))
					{
						SetFocus(GetDlgItem(hDlg,IDC_CUT_POSZ));
						SendDlgItemMessage(hDlg,IDC_CUT_POSZ,EM_SETSEL,(WPARAM)(INT) 0,(LPARAM)(INT) -1);
					}
				}
				return (INT_PTR)TRUE;
			case IDC_CUT_SIZEY:
				if(HIWORD(wParam)==EN_CHANGE)
				{
					GetWindowText((HWND)lParam, Text,20);
					Poly.SizeY=(float)atof(Text);
					if(Poly.SizeY<1)
						Poly.SizeY=1;
					if(Poly.SizeY+Poly.PosY>Meca.SIZE_Y)
						Poly.SizeY=Meca.SIZE_Y-Poly.PosY;
					if( SUCCEEDED( InitGeometry() ) )
						Render();
				}
				if(HIWORD(wParam)==EN_KILLFOCUS)
				{
					sprintf_s(Text,10,"%.2f",Poly.SizeY);
					SetWindowText(GetDlgItem(hDlg,IDC_CUT_SIZEY),Text);
				}
				return (INT_PTR)TRUE;
			case IDC_CUT_SIZEZ:
				if(HIWORD(wParam)==EN_CHANGE)
				{
					GetWindowText((HWND)lParam, Text,20);
					Poly.SizeZ=(float)atof(Text);
					if(Poly.SizeZ<1)
						Poly.SizeZ=1;
					if(Poly.SizeZ+Poly.PosZ>Meca.SIZE_Z)
						Poly.SizeZ=Meca.SIZE_Z-Poly.PosZ;
					if( SUCCEEDED( InitGeometry() ) )
						Render();
				}
				if(HIWORD(wParam)==EN_KILLFOCUS)
				{
					sprintf_s(Text,10,"%.2f",Poly.SizeZ);
					SetWindowText(GetDlgItem(hDlg,IDC_CUT_SIZEZ),Text);
				}
				return (INT_PTR)TRUE;
			case IDC_CUT_POSX:
				if(HIWORD(wParam)==EN_CHANGE)
				{
					GetWindowText((HWND)lParam, Text,20);
					Poly.PosX=(float)atof(Text);
					if(Poly.PosX<0)
						Poly.PosX=0;
					if(Poly.SizeX+Poly.PosX>Meca.SIZE_X)
						Poly.PosX=Meca.SIZE_X-Poly.SizeX;
					if( SUCCEEDED( InitGeometry() ) )
						Render();
				}
				if(HIWORD(wParam)==EN_KILLFOCUS)
				{
					sprintf_s(Text,10,"%.2f",Poly.PosX);
					SetWindowText(GetDlgItem(hDlg,IDC_CUT_POSX),Text);
				}
				return (INT_PTR)TRUE;
			case IDC_CUT_POSY:
				if(HIWORD(wParam)==EN_CHANGE)
				{
					GetWindowText((HWND)lParam, Text,20);
					Poly.PosY=(float)atof(Text);
					if(Poly.PosY<0)
						Poly.PosY=0;
					if(Poly.SizeY+Poly.PosY>Meca.SIZE_Y)
						Poly.PosY=Meca.SIZE_Y-Poly.SizeY;
					if( SUCCEEDED( InitGeometry() ) )
						Render();
				}
				if(HIWORD(wParam)==EN_KILLFOCUS)
				{
					sprintf_s(Text,10,"%.2f",Poly.PosY);
					SetWindowText(GetDlgItem(hDlg,IDC_CUT_POSY),Text);
				}
				return (INT_PTR)TRUE;
			case IDC_CUT_POSZ:
				if(HIWORD(wParam)==EN_CHANGE)
				{
					GetWindowText((HWND)lParam, Text,20);
					Poly.PosZ=(float)atof(Text);
					if(Poly.PosZ<0)
						Poly.PosZ=0;
					if(Poly.SizeZ+Poly.PosZ>Meca.SIZE_Z)
						Poly.PosZ=Meca.SIZE_Z-Poly.SizeZ;
					if( SUCCEEDED( InitGeometry() ) )
						Render();
				}
				if(HIWORD(wParam)==EN_KILLFOCUS)
				{
					sprintf_s(Text,10,"%.2f",Poly.PosZ);
					SetWindowText(GetDlgItem(hDlg,IDC_CUT_POSZ),Text);
					if(GetFocus()==GetDlgItem(hDlg,IDC_CUT_POSZ1))
					{
						SetFocus(GetDlgItem(hDlg,IDC_CUT_SIZEX));
						SendDlgItemMessage(hDlg,IDC_CUT_SIZEX,EM_SETSEL,(WPARAM)(INT) 0,(LPARAM)(INT) -1);
					}
				}
				return (INT_PTR)TRUE;
			case IDC_CUT_OPEN:
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = hDlg;
				ofn.lpstrFilter = "Cut Files (*.cut)\0*.cut\0All Files (*.*)\0*.*\0";
				ofn.lpstrFile = szCutFileName;
				ofn.nMaxFile = MAX_PATH;
				ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
				ofn.lpstrDefExt = "cut";
				if(GetOpenFileName(&ofn))
				{
					LoadCutFile(Data_Cut);
					if( SUCCEEDED( InitGeometry() ) )
						Render();
					else
						MessageBox(hDlg,"Can't init Geometry...","3D View",MB_OK);
				}
				InvalidateRect(hWndMain,NULL,FALSE);
				UpdateWindow(hWndMain);
				return (INT_PTR)TRUE;
			case IDC_CUT_SIMUL:
				if(Simul==0)
				{
					SetTimer(hDlg,IDT_TIMER1,(unsigned int)((Data_Cut[Simul].TIME*1000)/SendDlgItemMessage(hWndCut,IDC_CUT_TIME, TBM_GETPOS, (WPARAM)0, (LPARAM)0L)),(TIMERPROC) NULL);
					SetWindowText(GetDlgItem(hWndCut,IDC_CUT_SIMUL),"Stop Simul");
					Poly_Cut_Nbr=Simul+1;
					ShowWindow(GetDlgItem(hWndCut,IDC_CUT_SEGMENT1),SW_SHOW);
					sprintf_s(Text,20,"%d/%d",Simul+1,Data_Cut_Nbr);
					SetWindowText(GetDlgItem(hWndCut,IDC_CUT_SEGMENT2),Text);
					ShowWindow(GetDlgItem(hWndCut,IDC_CUT_SEGMENT2),SW_SHOW);
				}
				else
				{
					KillTimer(hDlg, IDT_TIMER1);
					Simul=0;
					Fil.XG=0.0f;
					Fil.XD=0.0f;
					Fil.YG=0.0f;
					Fil.YD=0.0f;
					Poly_Cut_Nbr=Data_Cut_Nbr;
					SetWindowText(GetDlgItem(hWndCut,IDC_CUT_SIMUL),"Start/Stop Simul");
					ShowWindow(GetDlgItem(hWndCut,IDC_CUT_SEGMENT1),SW_HIDE);
					ShowWindow(GetDlgItem(hWndCut,IDC_CUT_SEGMENT2),SW_HIDE);
					if( SUCCEEDED( InitGeometry() ) )
						Render();
					else
						MessageBox(hDlg,"Can't init Geometry...","3D View",MB_OK);
				}
				break;
			case IDC_CUT_RUN:
				if(ConvertCutInt()==1)
					SendCutInt();
				return (INT_PTR)TRUE;
		}
		break;
	case WM_TIMER:
		switch (wParam) 
		{ 
			case IDT_TIMER1:
				Simul++;
				SetTimer(hDlg,IDT_TIMER1,(unsigned int)((Data_Cut[Simul].TIME*1000)/SendDlgItemMessage(hWndCut,IDC_CUT_TIME, TBM_GETPOS, (WPARAM)0, (LPARAM)0L)),(TIMERPROC) NULL);
				sprintf_s(Text,20,"%d/%d",Simul+1,Data_Cut_Nbr);
				SetWindowText(GetDlgItem(hWndCut,IDC_CUT_SEGMENT2),Text);
				Poly_Cut_Nbr=Simul+1;
				Fil.XG=Data_Cut[Simul].XG;
				Fil.XD=Data_Cut[Simul].XD;
				Fil.YG=Data_Cut[Simul].YG;
				Fil.YD=Data_Cut[Simul].YD;
				if( SUCCEEDED( InitGeometry() ) )
					Render();
				else
					MessageBox(hDlg,"Can't init Geometry...","3D View",MB_OK);
				if(Simul>=Data_Cut_Nbr)
				{
					KillTimer(hDlg, IDT_TIMER1);
					Simul=0;
					Poly_Cut_Nbr=Data_Cut_Nbr;
					Fil.XG=0.0f;
					Fil.XD=0.0f;
					Fil.YG=0.0f;
					Fil.YD=0.0f;
					SetWindowText(GetDlgItem(hWndCut,IDC_CUT_SIMUL),"Start/Stop Simul");
					ShowWindow(GetDlgItem(hWndCut,IDC_CUT_SEGMENT1),SW_HIDE);
					ShowWindow(GetDlgItem(hWndCut,IDC_CUT_SEGMENT2),SW_HIDE);
				}
				return 0; 
		}
	}
	return (INT_PTR)FALSE;
}


// Message handler for 3D box.
INT_PTR CALLBACK Dlg_3D(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	RECT rcClient3D;
	POINT	Mouse;

	switch (message)
	{
		case WM_INITDIALOG:
			if( SUCCEEDED( InitD3D( hDlg ) ) )
			{
			// Create the scene geometry
				if( SUCCEEDED( InitGeometry() ) )
				    ShowWindow( hDlg, SW_SHOWDEFAULT );
				else
					MessageBox(hDlg,"Can't init Geometry...","3D View",MB_OK);
			}
			else
				MessageBox(hDlg,"Can't init 3D...","3D View",MB_OK);
			return (INT_PTR)TRUE;
		case WM_MOUSEMOVE:
			switch(wParam)
			{
				case MK_LBUTTON:
					if(MouseFlag==0||MouseFlag==2)
					{
						MousePosX=LOWORD(lParam);
						MousePosY=HIWORD(lParam);
						MouseFlag=1;
					}
					else
					{
						AngleY=(AngleY+LOWORD(lParam)-MousePosX)%1024;
						AngleX=(AngleX+MousePosY-HIWORD(lParam))%1024;
						MousePosX=LOWORD(lParam);
						MousePosY=HIWORD(lParam);
						Render();
					}
					break;
				case MK_RBUTTON:
					if(MouseFlag==0||MouseFlag==1)
					{
						MousePosX=LOWORD(lParam);
						MousePosY=HIWORD(lParam);
						MouseFlag=2;
					}
					else
					{
						MoveX+=LOWORD(lParam)-MousePosX;
						MoveY+=HIWORD(lParam)-MousePosY;
						MousePosX=LOWORD(lParam);
						MousePosY=HIWORD(lParam);
						Render();
					}
					break;
				default:
					MouseFlag=0;
					break;
			}
			break;
		case WM_MOUSEWHEEL:
			GetCursorPos(&Mouse);
			ScreenToClient(hDlg,&Mouse);
			GetClientRect(hDlg, &rcClient3D);
			if(Mouse.x<0||Mouse.y<0||Mouse.x>rcClient3D.right||Mouse.y>rcClient3D.bottom)
				return (INT_PTR)TRUE;
			Zoom+=GET_WHEEL_DELTA_WPARAM(wParam)/10;
			if(Zoom<=50)
				Zoom=50;
			Render();
			return (INT_PTR)TRUE;
		case WM_SIZE:
			GetClientRect(hDlg, &rcClient3D);
			if(rcClient3D.right==0||rcClient3D.bottom==0)
				break;
			if( SUCCEEDED( InitD3D( hDlg ) ) )
			{
			// Create the scene geometry
				if( SUCCEEDED( InitGeometry() ) )
				    ShowWindow( hDlg, SW_SHOWDEFAULT );
				else
					MessageBox(hDlg,"Can't init Geometry...","3D View",MB_OK);
			}
			else
				MessageBox(hDlg,"Can't init 3D...","3D View",MB_OK);
			break;
		case WM_PAINT:
			hdc = BeginPaint(hDlg, &ps);
			Render();
			EndPaint(hDlg, &ps);
			break;
		case WM_CLOSE:
		case WM_DESTROY:
			hWnd3D=NULL;
			DestroyWindow(hDlg);
			break;
	}
	return (INT_PTR)FALSE;
}

VOID SendCutInt()
{
	int i=0, old_seg=0;
	BYTE Buffer[65],j;
	long int seg_nbr=0;
	char Text[100];

	Buffer[0]=0;
	Buffer[1]=0x4C;
	Buffer[2]=0x01;
	Buffer[3]=0x00;
	Buffer[4]=11;
	sprintf_s(Text,50,"%5ld/%5ld",seg_nbr,Data_Cut_Nbr);
	for(i=0;i!=11;i++)
		Buffer[5+i]=Text[i];
	WriteHID(Buffer, HID_output_len);
	ReadHID(Buffer, HID_input_len);

	Buffer[0]=0;
	Buffer[1]=0x4D;
	Buffer[2]=0x01;
	WriteHID(Buffer, HID_output_len);
	ReadHID(Buffer, HID_input_len);
	Buffer[0]=0;
	Buffer[1]=0x42;
	Buffer[2]=0x00;
	WriteHID(Buffer, HID_output_len);
	ReadHID(Buffer, HID_input_len);
	Buffer[0]=0;
	Buffer[1]=0x42;
	Buffer[2]=0x01;
	WriteHID(Buffer, HID_output_len);
	ReadHID(Buffer, HID_input_len);
	i=0;
	do {
		SendInt(i);
		ReadHID(Buffer, HID_input_len);
		i++;
	} while(Buffer[2]==1 && i!=Data_Cut_Nbr);
	i--;


	Buffer[0]=0;
	Buffer[1]=0x42;
	Buffer[2]=0x80;
	WriteHID(Buffer, HID_output_len);
	ReadHID(Buffer, HID_input_len);

	old_seg=-1;
	while(i!=Data_Cut_Nbr)
	{
		do {
			SendInt(i);
			ReadHID(Buffer, HID_input_len);
			j=Buffer[2];
			if(Buffer[3]==0x07 && old_seg!=Buffer[4])
			{
				old_seg=Buffer[4];
				seg_nbr=Buffer[4]+Buffer[5]*0x100+Buffer[6]*0x10000;
				sprintf_s(Text,50,"%5ld",seg_nbr);
				Buffer[0]=0;
				Buffer[1]=0x4C;
				Buffer[2]=0x01;
				Buffer[3]=0x00;
				Buffer[4]=0x05;
				Buffer[5]=Text[0];
				Buffer[6]=Text[1];
				Buffer[7]=Text[2];
				Buffer[8]=Text[3];
				Buffer[9]=Text[4];
				WriteHID(Buffer, HID_output_len);
				ReadHID(Buffer, HID_input_len);
			}
		} while(j!=1);
		i++;
	}
	Buffer[0]=0;
	Buffer[1]=0x44;
	Buffer[2]=0x80;
	WriteHID(Buffer, HID_output_len);
	ReadHID(Buffer, HID_input_len);

	do {
		Buffer[0]=0;
		Buffer[1]=0x49;
		WriteHID(Buffer, HID_output_len);
		ReadHID(Buffer, HID_input_len);
		j=Buffer[7]&0x01;
		if(Buffer[3]==0x07 && old_seg!=Buffer[4])
		{
			old_seg=Buffer[4];
			seg_nbr=Buffer[4]+Buffer[5]*0x100+Buffer[6]*0x10000;
			sprintf_s(Text,50,"%5ld",seg_nbr);
			Buffer[0]=0;
			Buffer[1]=0x4C;
			Buffer[2]=0x01;
			Buffer[3]=0x00;
			Buffer[4]=0x05;
			Buffer[5]=Text[0];
			Buffer[6]=Text[1];
			Buffer[7]=Text[2];
			Buffer[8]=Text[3];
			Buffer[9]=Text[4];
			WriteHID(Buffer, HID_output_len);
			ReadHID(Buffer, HID_input_len);
		}
	} while(j==1);

	Buffer[0]=0;
	Buffer[1]=0x4D;
	Buffer[2]=0x00;
	WriteHID(Buffer, HID_output_len);
	ReadHID(Buffer, HID_input_len);

}

void SendInt(int toto)
{
	BYTE Buffer[65];
	int i;
	long int Temp;

	for (i=0;i!=65;i++)
		Buffer[i]=0;
	Buffer[0]=0;
	// CMD
	Buffer[1]=0x44;

	// DIR
	Buffer[2]=0;
	if(Data_Cut_Int[toto].XD>0)
		Buffer[2]|=0x01;
	if(Data_Cut_Int[toto].YD>0)
		Buffer[2]|=0x02;
	if(Data_Cut_Int[toto].XG>0)
		Buffer[2]|=0x04;
	if(Data_Cut_Int[toto].YG>0)
		Buffer[2]|=0x08;
	if(Data_Cut_Int[toto].Z>0)
		Buffer[2]|=0x10;
	if(Data_Cut_Int[toto].PWM!=-1)
		Buffer[2]|=0x40;

	i=3;
	// NBR INT
	Temp=Data_Cut_Int[toto].TIME;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	i++;

	// S XD
	Temp=abs(Data_Cut_Int[toto].XD)*2;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	i++;

	// S YD
	Temp=abs(Data_Cut_Int[toto].YD)*2;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	i++;

	//S XG
	Temp=abs(Data_Cut_Int[toto].XG)*2;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	i++;

	// S YG
	Temp=abs(Data_Cut_Int[toto].YG)*2;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	i++;

	// S Z
	Temp=abs(Data_Cut_Int[toto].Z)*2;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	Temp/=256;
	i++;
	Buffer[i]=Temp&0xFF;
	i++;

	if(Data_Cut_Int[toto].PWM!=-1)
		Buffer[i]=Data_Cut_Int[toto].PWM&0xFF;

	i=WriteHID(Buffer, HID_output_len);
}

int ConvertCutInt()
{
	char Text[100];
	int i,ret=1,OLD_PWM=-1;
	long int VXG,VXD,VYG,VYD,VZ,OLD_XG=0,OLD_XD=0,OLD_YG=0,OLD_YD=0,OLD_Z=0;

	if(Data_Cut_Int != NULL)
		GlobalFree(Data_Cut_Int);
	Data_Cut_Int=(CUT_INT*)GlobalAlloc(GPTR, (Data_Cut_Nbr)*sizeof(CUT_INT));
	if(Data_Cut_Int != NULL)
	{
		VXG=(long int)(((Interface.FREQ*10*Meca.M[0].MM_TOUR)/(Meca.M[0].VIT_SS_ACCEL*Meca.M[0].PAS_TOUR)));
		VYG=(long int)(((Interface.FREQ*10*Meca.M[1].MM_TOUR)/(Meca.M[1].VIT_SS_ACCEL*Meca.M[1].PAS_TOUR)));
		VXD=(long int)(((Interface.FREQ*10*Meca.M[2].MM_TOUR)/(Meca.M[2].VIT_SS_ACCEL*Meca.M[2].PAS_TOUR)));
		VYD=(long int)(((Interface.FREQ*10*Meca.M[3].MM_TOUR)/(Meca.M[3].VIT_SS_ACCEL*Meca.M[3].PAS_TOUR)));
		VZ =(long int)(((Interface.FREQ*10*Meca.M[4].MM_TOUR)/(Meca.M[4].VIT_SS_ACCEL*Meca.M[4].PAS_TOUR)));
		for (i=0;i!=Data_Cut_Nbr;i++)
		{
			Data_Cut_Int[i].XG=(long int)((Data_Cut[i].XG*Meca.M[0].PAS_TOUR)/Meca.M[0].MM_TOUR)-OLD_XG;
			Data_Cut_Int[i].YG=(long int)((Data_Cut[i].YG*Meca.M[1].PAS_TOUR)/Meca.M[1].MM_TOUR)-OLD_YG;
			Data_Cut_Int[i].XD=(long int)((Data_Cut[i].XD*Meca.M[2].PAS_TOUR)/Meca.M[2].MM_TOUR)-OLD_XD;
			Data_Cut_Int[i].YD=(long int)((Data_Cut[i].YD*Meca.M[3].PAS_TOUR)/Meca.M[3].MM_TOUR)-OLD_YD;
			Data_Cut_Int[i].Z =(long int)((Data_Cut[i].Z *Meca.M[4].PAS_TOUR)/Meca.M[4].MM_TOUR)-OLD_Z;
			OLD_XG+=Data_Cut_Int[i].XG;
			OLD_YG+=Data_Cut_Int[i].YG;
			OLD_XD+=Data_Cut_Int[i].XD;
			OLD_YD+=Data_Cut_Int[i].YD;
			OLD_Z +=Data_Cut_Int[i].Z;
			if(OLD_PWM != (Data_Cut[i].PWM*256)/100)
				OLD_PWM=Data_Cut_Int[i].PWM=(Data_Cut[i].PWM*255)/100;
			else
				Data_Cut_Int[i].PWM=-1;
			Data_Cut_Int[i].TIME=(long int)(Data_Cut[i].TIME*Interface.FREQ+0.5);
			if(Data_Cut_Int[i].XG!=0)
				if(10*Data_Cut_Int[i].TIME/abs(Data_Cut_Int[i].XG) < VXG)
				{
					sprintf_s(Text,50,"Seg=%d,VXG=%f>VXGMax=%f",i,(float)((abs(Data_Cut_Int[i].XG)*Interface.FREQ*Meca.M[0].MM_TOUR)/((float)Data_Cut_Int[i].TIME*Meca.M[0].PAS_TOUR)),Meca.M[0].VIT_SS_ACCEL);
					MessageBox(NULL,Text,"Warning",MB_OK);
					ret=0;
				}
			if(Data_Cut_Int[i].YG!=0)
				if(10*Data_Cut_Int[i].TIME/abs(Data_Cut_Int[i].YG) < VYG)
				{
					sprintf_s(Text,50,"Seg=%d,VYG=%f>VYGMax=%f",i,(float)((abs(Data_Cut_Int[i].YG)*Interface.FREQ*Meca.M[1].MM_TOUR)/((float)Data_Cut_Int[i].TIME*Meca.M[1].PAS_TOUR)),Meca.M[1].VIT_SS_ACCEL);
					MessageBox(NULL,Text,"Warning",MB_OK);
					ret=0;
				}
			if(Data_Cut_Int[i].XD!=0)
				if(10*Data_Cut_Int[i].TIME/abs(Data_Cut_Int[i].XD) < VXD)
				{
					sprintf_s(Text,99,"Seg=%d,VXD=%f>VXDMax=%f",i,(float)((abs(Data_Cut_Int[i].XD)*Interface.FREQ*Meca.M[2].MM_TOUR)/((float)Data_Cut_Int[i].TIME*Meca.M[2].PAS_TOUR)),Meca.M[2].VIT_SS_ACCEL);
					MessageBox(NULL,Text,"Warning",MB_OK);
					ret=0;
				}
			if(Data_Cut_Int[i].YD!=0)
				if(10*Data_Cut_Int[i].TIME/abs(Data_Cut_Int[i].YD) < VYD)
				{
					sprintf_s(Text,50,"Seg=%d,VYD=%f>VYDMax=%f",i,(float)((abs(Data_Cut_Int[i].YD)*Interface.FREQ*Meca.M[3].MM_TOUR)/((float)Data_Cut_Int[i].TIME*Meca.M[3].PAS_TOUR)),Meca.M[3].VIT_SS_ACCEL);
					MessageBox(NULL,Text,"Warning",MB_OK);
					ret=0;
				}
			if(Data_Cut_Int[i].Z!=0)
				if(10*Data_Cut_Int[i].TIME/abs(Data_Cut_Int[i].Z) < VZ)
				{
					sprintf_s(Text,50,"Seg=%d,VZ=%f>VZMax=%f",i,(float)((abs(Data_Cut_Int[i].Z)*Interface.FREQ*Meca.M[4].MM_TOUR)/((float)Data_Cut_Int[i].TIME*Meca.M[4].PAS_TOUR)),Meca.M[4].VIT_SS_ACCEL);
					MessageBox(NULL,Text,"Warning",MB_OK);
					ret=0;
				}
		}
	}
	return ret;
}

VOID LoadCutFile(CUT *Data)
{
    HANDLE hFile;
    BOOL bVal = FALSE;
    LPSTR pszText,pszText1,pszText2;
    DWORD dwFileSize;
    DWORD dwRead;
	char Text[100];
	float Temps=0;
	int i,PWM;

	hFile = CreateFile(szCutFileName, GENERIC_READ, FILE_SHARE_READ, NULL,
        OPEN_EXISTING, 0, NULL);
    if(hFile != INVALID_HANDLE_VALUE)
    {
        dwFileSize = GetFileSize(hFile, NULL);
        if(dwFileSize != 0xFFFFFFFF)
        {
            pszText = LPSTR(GlobalAlloc(GPTR, dwFileSize + 1));
            if(pszText != NULL)
            {
                if(ReadFile(hFile, pszText, dwFileSize, &dwRead, NULL))
                {
                    pszText[dwFileSize] = 0;
					pszText1=pszText;
					Data_Cut_Nbr=0;
					Xmax=Ymax=0;
					PWMmax=0;
					if(Data_Cut != NULL)
						GlobalFree(Data_Cut);
					Data_Cut=(CUT*)GlobalAlloc(GPTR, (CountStrLines(pszText1)+2)*sizeof(CUT));
					if(Data_Cut != NULL)
					{
						pszText2=strrchr(szCutFileName,'\\');
						if(pszText2!=NULL)
							pszText2++;
						else
							pszText2=szCutFileName;
						SetWindowText(GetDlgItem(hWndCut,IDC_FICHIER),pszText2);
						PWM=0;
						while(*pszText1!='\0')
						{
							Data_Cut[Data_Cut_Nbr].XG=0;
							Data_Cut[Data_Cut_Nbr].YG=0;
							Data_Cut[Data_Cut_Nbr].XD=0;
							Data_Cut[Data_Cut_Nbr].YD=0;
							Data_Cut[Data_Cut_Nbr].Z=0;
							Data_Cut[Data_Cut_Nbr].TIME=0;
							Data_Cut[Data_Cut_Nbr].PWM=PWM;
							bVal=FALSE;
							while(*pszText1!='\n' && *pszText1!='\0')
							{
								switch(*pszText1)
								{
									case 'N':
										strncpy_s(Data_Cut_Name,pszText1+2,strcspn(pszText1+2,"\r\n"));
										pszText1+=strcspn(pszText1,"\r\n");
										while(*pszText1==' '||*pszText1==','||*pszText1=='\t'||*pszText1=='\r')
											*pszText1++;
										break;
									case 'H':
										Data_Cut[Data_Cut_Nbr].PWM=PWM=(int)strtod(pszText1+3,&pszText2);
										if(Data_Cut[Data_Cut_Nbr].PWM>PWMmax)
											PWMmax=Data_Cut[Data_Cut_Nbr].PWM;
										pszText1=pszText2;
										while(*pszText1==' '||*pszText1==','||*pszText1=='\t'||*pszText1=='\r')
											*pszText1++;
										break;
									case 'T':
										Data_Cut[Data_Cut_Nbr].TIME=(float)strtod(pszText1+2,&pszText2);
										pszText1=pszText2;
										while(*pszText1==' '||*pszText1==','||*pszText1=='\t'||*pszText1=='\r')
											*pszText1++;
										bVal=TRUE;
										break;
									case 'X':
										if(*(pszText1+1)=='G')
										{
											Data_Cut[Data_Cut_Nbr].XG=(float)strtod(pszText1+3,&pszText2);
											if(Data_Cut[Data_Cut_Nbr].XG>Xmax)
												Xmax=Data_Cut[Data_Cut_Nbr].XG;
										}
										else
										{
											if(*(pszText1+1)=='D')
											{
												Data_Cut[Data_Cut_Nbr].XD=(float)strtod(pszText1+3,&pszText2);
												if(Data_Cut[Data_Cut_Nbr].XD>Xmax)
													Xmax=Data_Cut[Data_Cut_Nbr].XD;
											}
											else
												break;
										}
										pszText1=pszText2;
										while(*pszText1==' '||*pszText1==','||*pszText1=='\t'||*pszText1=='\r')
											*pszText1++;
										bVal=TRUE;
										break;
									case 'Y':
										if(*(pszText1+1)=='G')
										{
											Data_Cut[Data_Cut_Nbr].YG=(float)strtod(pszText1+3,&pszText2);
											if(Data_Cut[Data_Cut_Nbr].YG>Ymax)
												Ymax=Data_Cut[Data_Cut_Nbr].YG;
										}
										else
										{
											if(*(pszText1+1)=='D')
											{
												Data_Cut[Data_Cut_Nbr].YD=(float)strtod(pszText1+3,&pszText2);
												if(Data_Cut[Data_Cut_Nbr].YD>Ymax)
													Ymax=Data_Cut[Data_Cut_Nbr].YD;
											}
											else
												break;
										}
										pszText1=pszText2;
										while(*pszText1==' '||*pszText1==','||*pszText1=='\t'||*pszText1=='\r')
											*pszText1++;
										bVal=TRUE;
										break;
									case 'Z':
										Data_Cut[Data_Cut_Nbr].Z=(float)strtod(pszText1+3,&pszText2);
										pszText1=pszText2;
										while(*pszText1==' '||*pszText1==','||*pszText1=='\t'||*pszText1=='\r')
											*pszText1++;
										bVal=TRUE;
										break;
									default:
										while(*pszText1!=0&&*pszText1!='\n')
											pszText1++;
										break;
								}
							}
							if(*pszText1=='\n')
								*pszText1++;
							if(bVal==TRUE)
							{
								Temps+=Data_Cut[Data_Cut_Nbr].TIME;
								Data_Cut_Nbr++;
							}
						}
						sprintf_s(Text,10,"%d",Data_Cut_Nbr);
						SetWindowText(GetDlgItem(hWndCut,IDC_CUT_SEGMENTS),Text);
						i=(int)(Temps/60);
						sprintf_s(Text,90,"%dm%.0fs",i,Temps-(float)i*60);
						SetWindowText(GetDlgItem(hWndCut,IDC_CUT_TOTAL_TIME),Text);
						sprintf_s(Text,90,"%.2fmm",Xmax);
						SetWindowText(GetDlgItem(hWndCut,IDC_CUT_XMAX),Text);
						sprintf_s(Text,90,"%.2fmm",Ymax);
						SetWindowText(GetDlgItem(hWndCut,IDC_CUT_YMAX),Text);
						sprintf_s(Text,90,"%d%%",PWMmax);
						SetWindowText(GetDlgItem(hWndCut,IDC_CUT_PWMMAX),Text);
						Poly_Cut_Nbr=Data_Cut_Nbr;
					}
				}
                GlobalFree(pszText);
            }
        }
        CloseHandle(hFile);
    }
}

int CountStrLines(LPCTSTR string)
{
	int res=0;
	while(*string!=0)
	{
		if(*string=='\n')
		{
			res++;
			if(*(string+1) == '/')
				res--;
		}
		string++;
	}
	return res;
}

void ReadIni()
{
	char Text[100],Text1[100],MECA[100];
	int i;

	//Interface
	GetPrivateProfileString("INTERFACE","M_ON","0.50",(LPSTR)&Text,99,".\\IPL5X.ini");
	Interface.M_ON=(unsigned int)((float)atof(Text)*1000);
	GetPrivateProfileString("INTERFACE","M_OFF","1.00",(LPSTR)&Text,99,".\\IPL5X.ini");
	Interface.M_OFF=(unsigned int)((float)atof(Text)*1000);
	Interface.FREQ=      GetPrivateProfileInt("INTERFACE","FREQ",50000,".\\IPL5X.ini");
	Interface.ACCEL_TYPE=GetPrivateProfileInt("INTERFACE","ACCEL_TYPE",6,".\\IPL5X.ini");
	// Poly
	GetPrivateProfileString("POLY","SIZE_X","300.00",(LPSTR)&Text,99,".\\IPL5X.ini");
	Poly.SizeX=(float)atof(Text);
	GetPrivateProfileString("POLY","SIZE_Y","100.00",(LPSTR)&Text,99,".\\IPL5X.ini");
	Poly.SizeY=(float)atof(Text);
	GetPrivateProfileString("POLY","SIZE_Z","50.00",(LPSTR)&Text,99,".\\IPL5X.ini");
	Poly.SizeZ=(float)atof(Text);
	GetPrivateProfileString("POLY","POS_X","5.00",(LPSTR)&Text,99,".\\IPL5X.ini");
	Poly.PosX=(float)atof(Text);
	GetPrivateProfileString("POLY","POS_Y","0.00",(LPSTR)&Text,99,".\\IPL5X.ini");
	Poly.PosY=(float)atof(Text);
	GetPrivateProfileString("POLY","POS_Z","300.00",(LPSTR)&Text,99,".\\IPL5X.ini");
	Poly.PosZ=(float)atof(Text);
	//Meca
	GetPrivateProfileString("MECA","DEFAULT","MECA1",(LPSTR)&MECA,99,".\\IPL5X.ini");
	GetPrivateProfileString("MECA",MECA,"Fil chaud",(LPSTR)&Meca.NAME,99,".\\IPL5X.ini");
	GetPrivateProfileString(MECA,"SIZE_X","1000",(LPSTR)&Text,99,".\\IPL5X.ini");
	Meca.SIZE_X=(float)atof(Text);
	GetPrivateProfileString(MECA,"SIZE_Y","500",(LPSTR)&Text,99,".\\IPL5X.ini");
	Meca.SIZE_Y=(float)atof(Text);
	GetPrivateProfileString(MECA,"SIZE_Z","800",(LPSTR)&Text,99,".\\IPL5X.ini");
	Meca.SIZE_Z=(float)atof(Text);
	for(i=0;i!=5;i++)
	{
		sprintf_s(Text1,20,"M%d_PAS_TOUR",i+1);
		Meca.M[i].PAS_TOUR=GetPrivateProfileInt(MECA,Text1,400,".\\IPL5X.ini");
		sprintf_s(Text1,20,"M%d_MM_TOUR",i+1);
		GetPrivateProfileString(MECA,Text1,"1.25",(LPSTR)&Text,99,".\\IPL5X.ini");
		Meca.M[i].MM_TOUR=(float)atof(Text);
		sprintf_s(Text1,20,"M%d_PAS_PERDU",i+1);
		Meca.M[i].PAS_PERDU=GetPrivateProfileInt(MECA,Text1,0,".\\IPL5X.ini");
		sprintf_s(Text1,20,"M%d_VIT_SS_ACCEL",i+1);
		GetPrivateProfileString(MECA,Text1,"5.70",(LPSTR)&Text,99,".\\IPL5X.ini");
		Meca.M[i].VIT_SS_ACCEL=(float)atof(Text);
		sprintf_s(Text1,20,"M%d_VIT_AV_ACCEL",i+1);
		GetPrivateProfileString(MECA,Text1,"7.00",(LPSTR)&Text,99,".\\IPL5X.ini");
		Meca.M[i].VIT_AV_ACCEL=(float)atof(Text);
		sprintf_s(Text1,20,"M%d_STEP",i+1);
		Meca.M[i].STEP=GetPrivateProfileInt(MECA,Text1,i*2+1,".\\IPL5X.ini");
		sprintf_s(Text1,20,"M%d_DIR" ,i+1);
		Meca.M[i].DIR= GetPrivateProfileInt(MECA,Text1,i*2+2,".\\IPL5X.ini");
	}
	GetPrivateProfileString(MECA,"M1_NAME","XG",(LPSTR)&Meca.M[0].NAME,9,".\\IPL5X.ini");
	GetPrivateProfileString(MECA,"M2_NAME","YG",(LPSTR)&Meca.M[1].NAME,9,".\\IPL5X.ini");
	GetPrivateProfileString(MECA,"M3_NAME","XD",(LPSTR)&Meca.M[2].NAME,9,".\\IPL5X.ini");
	GetPrivateProfileString(MECA,"M4_NAME","YD",(LPSTR)&Meca.M[3].NAME,9,".\\IPL5X.ini");
	GetPrivateProfileString(MECA,"M5_NAME"," ",(LPSTR)&Meca.M[4].NAME,9,".\\IPL5X.ini");
}

