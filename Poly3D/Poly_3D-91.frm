VERSION 5.00
Begin VB.Form FrmBloc 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Dimensions du bloc (en mm)"
   ClientHeight    =   6000
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   3900
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6000
   ScaleWidth      =   3900
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.PictureBox PB_HELP 
      BackColor       =   &H80000007&
      Height          =   495
      Left            =   3360
      ScaleHeight     =   435
      ScaleWidth      =   435
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   4080
      Width           =   495
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   2655
      LargeChange     =   10
      Left            =   3000
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   3240
      Width           =   255
   End
   Begin VB.TextBox TBVal 
      Height          =   330
      Index           =   6
      Left            =   1920
      TabIndex        =   12
      Top             =   5490
      Width           =   855
   End
   Begin VB.TextBox TBVal 
      Height          =   330
      Index           =   5
      Left            =   1920
      TabIndex        =   11
      Top             =   5040
      Width           =   855
   End
   Begin VB.TextBox TBVal 
      Height          =   330
      Index           =   4
      Left            =   1920
      TabIndex        =   10
      Top             =   4590
      Width           =   855
   End
   Begin VB.TextBox TBVal 
      Height          =   330
      Index           =   3
      Left            =   1920
      TabIndex        =   9
      Top             =   4140
      Width           =   855
   End
   Begin VB.TextBox TBVal 
      Height          =   330
      Index           =   2
      Left            =   1920
      TabIndex        =   8
      Top             =   3690
      Width           =   855
   End
   Begin VB.TextBox TBVal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "#�##0,0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1036
         SubFormatType   =   1
      EndProperty
      Height          =   330
      Index           =   1
      Left            =   1920
      TabIndex        =   7
      Top             =   3240
      Width           =   855
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      Enabled         =   0   'False
      Height          =   3060
      Left            =   0
      ScaleHeight     =   3000
      ScaleWidth      =   3825
      TabIndex        =   0
      Top             =   0
      Width           =   3885
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "dX bord de fuite :"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   90
      TabIndex        =   6
      Top             =   4635
      Width           =   1725
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "dX bord d'attaque :"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   90
      TabIndex        =   5
      Top             =   4140
      Width           =   1725
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Longueur :"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   360
      TabIndex        =   4
      Top             =   5535
      Width           =   1440
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Epaisseur :"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   360
      TabIndex        =   3
      Top             =   5040
      Width           =   1440
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Saumon :"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   780
      TabIndex        =   2
      Top             =   3660
      Width           =   1020
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Emplanture :"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   675
      TabIndex        =   1
      Top             =   3285
      Width           =   1125
   End
End
Attribute VB_Name = "FrmBloc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim CouleurSelectionne As Long
Dim CouleurLignePleine As Long
Dim CouleurLignePointille As Long
Dim CosA As Double, CosB As Double, SinA As Double, SinB As Double
Dim SymboleDecimal As String

Dim oToolTip As CDKSToolTip 'Objet tooltip pour faire des zolis tooltip !

Const MaxX = 1000
Const maxY = 600
Const MaxZ = 200

Dim XE, XS, DXA, DXF, y, Z, Hdessin, Vdessin As Single

Dim iTB As Integer      'Indice de la textBox qui a le focus

Private Sub Form_Activate()
    TBVal(iTB).SetFocus
End Sub

Private Sub Form_Load()
    
    Call ChargeTrad(Me)
    CouleurSelectionne = RGB(255, 209, 218)
    CouleurLignePleine = RGB(0, 102, 0)
    CouleurLignePointille = RGB(51, 51, 255)
    SymboleDecimal = Format(0, ".") 'Pour que �a marche chez les ricains...
    
    'on cr�e un nouvel objet tooltip
    Set oToolTip = New CDKSToolTip
    oToolTip.BackColor = RGB(127, 255, 255)
    Let oToolTip.Title = "Bloc - Aide"
    Let oToolTip.TipText = "Les valeurs peuvent �tre saisies directement au clavier."
    Let oToolTip.TipText = oToolTip.TipText & vbCrLf & "Les chiffres du haut du clavier peuvent �tre utilis�s directement (cas des portables)."
    Let oToolTip.TipText = oToolTip.TipText & vbCrLf & "Pour valider une saisie, il faut utiliser la touche <Entr�e> ou la touche <Tab>."
    Let oToolTip.TipText = oToolTip.TipText & vbCrLf & "L'ascenseur permet de modifier par pas de 1 ou de 0.1 la valeur s�lectionn�e. La vue 3D est mise � jour automatiquement."
    Let oToolTip.WaitTime = CLng(200)
    oToolTip.ForeColor = RGB(0, 0, 0)
    oToolTip.IconStyle = ToolTipIconInfo
    oToolTip.IsCentered = True
    oToolTip.Style = ToolTipBalloon
    oToolTip.CreateToolTip (PB_HELP.hwnd)
    PB_HELP.ToolTipText = ""
    
    CosA = Cos(0.5) 'Angle des ar�tes de droite
    SinA = Sin(0.5)
    CosB = Cos(0.6) 'Angle des ar�tes de gauche
    SinB = Sin(0.6)
    
    XE = 200
    XS = 160
    DXA = 10
    DXF = 30
    y = 40
    Z = 600
    Hdessin = 0
    Vdessin = 0
    
    Call RemplissageLabels(1)
    
    VScroll1.Min = -50
    VScroll1.Max = 50
    VScroll1.Value = 0
    VScroll1.LargeChange = 10
    VScroll1.SmallChange = 1
    
    Call Affiche
    
    iTB = 1
End Sub

Private Sub Affiche()
    Dim XV3, YV3, XV4, YV4, XV5, YV5, XV6, YV6 As Single
    Dim XO, YO, XV1, YV1, XV2, YV2 As Single
    Dim CoeffHorizontal As Double, CoeffVertical As Double

    Picture1.Cls

    XV1 = Z * CosA 'Vecteur du bord du bloc parall�le � Z
    YV1 = -Z * SinA
    
    XV2 = -DXF * CosB 'Vecteur de la fl�che au bord de fuite
    YV2 = -DXF * SinB

    XV3 = -XS * CosB  'Vecteur du saumon
    YV3 = -XS * SinB

    XV4 = -XE * CosB  'Vecteur du saumon
    YV4 = -XE * SinB

    XV5 = 0 'Vecteur de l'�paisseur
    YV5 = -y

    XV6 = -DXA * CosB 'Vecteur de la fl�che au bord d'attaque
    YV6 = -DXA * SinB

    'dimensions du trac�

    If DXF >= 0 And DXA >= 0 Then
        Hdessin = Abs(XV4) + Abs(XV1)
        Vdessin = Abs(YV1) + Abs(YV2) + Abs(YV3) + Abs(YV5) + Abs(YV6)
    End If

    If DXF >= 0 And DXA < 0 Then
        Hdessin = Abs(XV4) + Abs(XV1) + Abs(XV6)
        Vdessin = Abs(YV1) + Abs(YV2) + Abs(YV3) + Abs(YV5) 'fl�che n�gative au bord d'attaque
    End If

    If DXF < 0 And DXA >= 0 Then  'fl�che n�gative au bord de fuite
        Hdessin = Abs(XV4) + Abs(XV1) + Abs(XV2)
        Vdessin = Abs(YV1) + Abs(YV3) + Abs(YV5) + Abs(YV6)
    End If
    
    'todo : cas o� DXF < 0 and DXA < 0

    'Ajustement � la zone de dessin
    CoeffHorizontal = (Picture1.ScaleWidth - 300) / Hdessin
    CoeffVertical = (Picture1.ScaleHeight - 300) / Vdessin
    XV1 = XV1 * CoeffHorizontal
    YV1 = YV1 * CoeffVertical
    XV2 = XV2 * CoeffHorizontal
    YV2 = YV2 * CoeffVertical
    XV3 = XV3 * CoeffHorizontal
    YV3 = YV3 * CoeffVertical
    XV4 = XV4 * CoeffHorizontal
    YV4 = YV4 * CoeffVertical
    XV5 = XV5 * CoeffHorizontal
    YV5 = YV5 * CoeffVertical
    XV6 = XV6 * CoeffHorizontal
    YV6 = YV6 * CoeffVertical

    If DXF >= 0 And DXA >= 0 Then
        XO = -XV4 + 150 'Emplacement du point le plus bas (origine en haut � gauche)
        YO = Picture1.ScaleHeight - 150
    End If
    If DXF >= 0 And DXA < 0 Then
        XO = Abs(XV4) + Abs(XV6) + 150
        YO = Picture1.ScaleHeight - 150
    End If
    If DXF < 0 And DXA >= 0 Then
        XO = Abs(XV4) + 150
        YO = Picture1.ScaleHeight - 150 - Abs(YV2)
    End If

    'Affichage des lignes en pointill�s

    Picture1.ForeColor = CouleurLignePointille
    Picture1.DrawStyle = vbDash
    Picture1.DrawWidth = 1
    If DXF >= 0 And DXA >= 0 Then
        'bas bf
        Picture1.Line (XO, YO)-(XO + XV1, YO + YV1)
        'bas fl�che bf
        Picture1.Line (XO + XV1, YO + YV1)-(XO + XV1 + XV2, YO + YV1 + YV2)
        'haut bf
        Picture1.Line (XO + XV5, YO + YV5)-(XO + XV1 + XV5, YO + YV1 + YV5)
        'haut fl�che bf
        Picture1.Line (XO + XV1 + XV5, YO + YV1 + YV5)-(XO + XV1 + XV2 + XV5, YO + YV1 + YV2 + YV5)
        'ar�te verticale
        Picture1.Line (XO + XV1, YO + YV1)-(XO + XV1 + XV5, YO + YV1 + YV5)
        'haut ba
        Picture1.Line (XO + XV5 + XV4, YO + YV5 + YV4)-(XO + XV1 + XV5 + XV4, YO + YV1 + YV5 + YV4)
        'haut fl�che ba
        Picture1.Line (XO + XV1 + XV5 + XV4, YO + YV1 + YV5 + YV4)-(XO + XV1 + XV2 + XV5 + XV3, YO + YV1 + YV2 + YV5 + YV3)
    End If
    If DXF >= 0 And DXA < 0 Then
        'bas bf
        Picture1.Line (XO, YO)-(XO + XV1, YO + YV1)
        'bas fl�che bf
        Picture1.Line (XO + XV1, YO + YV1)-(XO + XV1 + XV2, YO + YV1 + YV2)
        'haut bf
        Picture1.Line (XO + XV5, YO + YV5)-(XO + XV1 + XV5, YO + YV1 + YV5)
        'haut fl�che bf
        Picture1.Line (XO + XV1 + XV5, YO + YV1 + YV5)-(XO + XV1 + XV2 + XV5, YO + YV1 + YV2 + YV5)
        'ar�te verticale
        Picture1.Line (XO + XV1, YO + YV1)-(XO + XV1 + XV5, YO + YV1 + YV5)
        'haut ba
        Picture1.Line (XO + XV5 + XV4 - XV6, YO + YV5 + YV4 - YV6)-(XO + XV1 + XV5 + XV4 - XV6, YO + YV1 + YV5 + YV4 - YV6)
        'haut fl�che ba
        Picture1.Line (XO + XV5 + XV4, YO + YV5 + YV4)-(XO + XV2 + XV5 + XV3, YO + YV2 + YV5 + YV3)
        'ar�te verticale
        Picture1.Line (XO + XV4 - XV6, YO + YV4 - YV6)-(XO + XV4 - XV6 + XV5, YO + YV4 - YV6 + YV5)
        'bas fl�che ba
        Picture1.Line (XO + XV4, YO + YV4)-(XO + XV2 + XV3, YO + YV2 + YV3)
    End If
    If DXF < 0 And DXA >= 0 Then
        'bas bf
        Picture1.Line (XO + XV2, YO + YV2)-(XO + XV2 + XV1, YO + YV2 + YV1)
        'bas fl�che bf
        Picture1.Line (XO, YO)-(XO + XV2, YO + YV2)
        'haut bf
        Picture1.Line (XO + XV2 + XV5, YO + YV2 + YV5)-(XO + XV2 + XV1 + XV5, YO + YV2 + YV1 + YV5)
        'haut fl�che bf
        Picture1.Line (XO + XV5, YO + YV5)-(XO + XV2 + XV5, YO + YV2 + YV5)
        'ar�te verticale
        Picture1.Line (XO + XV2, YO + YV2)-(XO + XV2 + XV5, YO + YV2 + YV5)
        'haut ba
        Picture1.Line (XO + XV5 + XV4, YO + YV5 + YV4)-(XO + XV1 + XV5 + XV4, YO + YV1 + YV5 + YV4)
        'haut fl�che ba
        Picture1.Line (XO + XV1 + XV5 + XV4, YO + YV1 + YV5 + YV4)-(XO + XV1 + XV2 + XV5 + XV3, YO + YV1 + YV2 + YV5 + YV3)
    End If

    'todo : cas o� DXF < 0 and DXA < 0

    'Affichage des lignes pleines

    Picture1.DrawStyle = vbSolid
    Picture1.DrawWidth = 2
    Picture1.ForeColor = CouleurLignePleine
    'bord de fuite
    Picture1.Line (XO, YO)-(XO + XV1 + XV2, YO + YV1 + YV2), RGB(0, 102, 0)
    Picture1.Line (XO, YO)-(XO + XV5, YO + YV5)
    Picture1.Line (XO + XV5, YO + YV5)-(XO + XV1 + XV2 + XV5, YO + YV1 + YV2 + YV5)
    Picture1.Line (XO + XV1 + XV2, YO + YV1 + YV2)-(XO + XV1 + XV2 + XV5, YO + YV1 + YV2 + YV5)
    'saumon
    Picture1.Line (XO + XV1 + XV2 + XV5, YO + YV1 + YV2 + YV5)-(XO + XV1 + XV2 + XV5 + XV3, YO + YV1 + YV2 + YV5 + YV3)
    'emplanture
    Picture1.Line (XO, YO)-(XO + XV4, YO + YV4)
    Picture1.Line (XO + XV5, YO + YV5)-(XO + XV4 + XV5, YO + YV4 + YV5)
    Picture1.Line (XO + XV4, YO + YV4)-(XO + XV4 + XV5, YO + YV4 + YV5)
    'bord d'attaque
    Picture1.Line (XO + XV4 + XV5, YO + YV4 + YV5)-(XO + XV1 + XV2 + XV5 + XV3, YO + YV1 + YV2 + YV5 + YV3)
End Sub

Private Sub RemplissageLabels(Num)   'Num = n� du Label qui a le focus
    TBVal(1).Text = Format(XE, "#0.0")
    TBVal(2).Text = Format(XS, "#0.0")
    TBVal(3).Text = Format(DXA, "#0.0")
    TBVal(4).Text = Format(DXF, "#0.0")
    TBVal(5).Text = Format(y, "#0.0")
    TBVal(6).Text = Format(Z, "#0.0")
End Sub

Private Sub Form_Unload(Cancel As Integer)
    oToolTip.DestroyToolTip
End Sub

Private Sub TBVal_GotFocus(Index As Integer)
    'Pour s�lectionner TOUT le texte de la TextBox
    TBVal(Index).SelStart = 0
    TBVal(Index).SelLength = Len(TBVal(Index).Text)
    iTB = Index
End Sub

Private Sub TBVal_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim i As Integer

    'Trancodage des touches du haut du clavier pour utilisation facile avec un portable
    Select Case KeyAscii
        Case Asc("�")
            KeyAscii = Asc("0")
        Case Asc("&")
            KeyAscii = Asc("1")
        Case Asc("�")
            KeyAscii = Asc("2")
        Case Asc("""")
            KeyAscii = Asc("3")
        Case Asc("'")
            KeyAscii = Asc("4")
        Case Asc("(")
            KeyAscii = Asc("5")
        Case Asc("-")
            KeyAscii = Asc("6")
        Case Asc("�")
            KeyAscii = Asc("7")
        Case Asc("_")
            KeyAscii = Asc("8")
        Case Asc("�")
            KeyAscii = Asc("9")
        Case Asc(".")
            KeyAscii = Asc(SymboleDecimal)
    End Select
    
    If (KeyAscii < Asc("0") Or KeyAscii > Asc("9")) And _
        KeyAscii <> 13 And KeyAscii <> Asc(SymboleDecimal) And _
        KeyAscii <> 8 Then
        Beep
        KeyAscii = 0
    End If
    
    If KeyAscii = 13 Then   'Touche <Entr�e>
        If Index < TBVal.UBound Then
            SendKeys "{TAB}"
        Else
            For i = 1 To TBVal.Count - 1    'Pour "remonter" au premier TextBox
                SendKeys "+{TAB}"
            Next i
        End If
    End If
End Sub

Private Sub TBVal_Validate(Index As Integer, Cancel As Boolean)
    'Index : 1 = Emplanture
    '        2 = Saumon
    '        3 = BA
    '        4 = BF
    '        5 = Epaisseur
    '        6 = longueur
    Dim strError As String
    Dim wXE, wXS, wDXA, wDXF, wY, wZ As Single
    
    wXE = XE
    wXS = XS
    wDXA = DXA
    wDXF = DXF
    wY = y
    wZ = Z
    
    Select Case Index
        Case 1
            XE = Format(Val(Replace(TBVal(1).Text, SymboleDecimal, ".")), "0.0")
        Case 2
            XS = Format(Val(Replace(TBVal(2).Text, SymboleDecimal, ".")), "0.0")
        Case 3
            DXA = Format(Val(Replace(TBVal(3).Text, SymboleDecimal, ".")), "0.0")
        Case 4
            DXF = Format(Val(Replace(TBVal(4).Text, SymboleDecimal, ".")), "0.0")
        Case 5
            y = Format(Val(Replace(TBVal(5).Text, SymboleDecimal, ".")), "0.0")
        Case 6
            Z = Format(Val(Replace(TBVal(6).Text, SymboleDecimal, ".")), "0.0")
    End Select
    
    'Contr�les
    strError = ""
    'If XE < XS Then
    '    strError = "L'emplanture ne peut pas �tre plus petite que le saumon"
    'End If
    If XE <= 0 Then
        strError = "L'emplanture doit �tre plus grande que 0"
    End If
    If XS <= 0 Then
        strError = "Le saumon doit �tre plus grand que 0"
    End If
    If y <= 0 Then
        strError = "L'�paisseur doit �tre plus grande que 0"
    End If
    
    If Index = 1 Or Index = 2 Or Index = 4 Then
        DXA = XE - XS - DXF
    Else
        DXF = XE - XS - DXA
    End If
    
    If strError <> "" Then
        MsgBox strError, vbCritical + vbOKOnly, App.Title
        Cancel = True
        XE = wXE
        XS = wXS
        DXA = wDXA
        DXF = wDXF
        y = wY
        Z = wZ
        TBVal(Index).SelStart = 0
        TBVal(Index).SelLength = Len(TBVal(Index).Text)
    Else
        Call Affiche
    End If
    Call RemplissageLabels(Index)
End Sub

Private Sub VScroll1_Change()
    Dim wCancel As Boolean
    TBVal(iTB).Text = Format(Val(Replace(TBVal(iTB).Text, SymboleDecimal, ".")) - VScroll1.Value / 10, "0.0")
    VScroll1.Value = 0
    Call TBVal_Validate(iTB, wCancel)
    TBVal(iTB).SetFocus
End Sub
