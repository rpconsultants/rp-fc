Attribute VB_Name = "Module_INI"
Option Explicit

'Module de gestion du fichier INI

Function ReadINI(monItem As String, maValeur As String) As String
    Dim stBuf As String, lgBuf As Long, lgRep As Long
    Dim i As Integer, j As Integer
    Dim monEnreg As String

    'On commence par lire dans le .INI
    stBuf = Space$(255)
    lgBuf = 255
    lgRep = GetPrivateProfileString(App.Title, monItem, maValeur, stBuf, lgBuf, App.Path & "\" & App.Title & ".ini")
    ReadINI = Replace(Left(stBuf, lgRep), "&&", Chr(9))
End Function

Function ReadINI_Trad(maLangue As String, monFormulaire As String, monItem As String) As String
    Dim stBuf As String, lgBuf As Long, lgRep As Long
    Dim i As Integer, j As Integer
    Dim monEnreg As String
    Dim maValeur As String
    
    If monItem = "" Then
        ReadINI_Trad = "___"
        Exit Function
    End If
    
    'On commence par lire dans le .INI de la langue
    stBuf = Space$(255)
    lgBuf = 255
    maValeur = "$$"
    lgRep = GetPrivateProfileString(monFormulaire, monItem, maValeur, stBuf, lgBuf, App.Path & "\" & App.Title & maLangue & ".ini")
    If Left(stBuf, 2) = "$$" And maLangue <> "en" Then
        ReadINI_Trad = ReadINI_Trad("en", monFormulaire, monItem)
        Exit Function
    End If
    If Left(stBuf, 2) = "$$" Then
        ReadINI_Trad = monItem
        Exit Function
    End If
    ReadINI_Trad = Left(stBuf, lgRep)
End Function
