VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDecoupe 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   9840
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   15060
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9840
   ScaleWidth      =   15060
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.CheckBox CB_Points 
      Height          =   495
      Left            =   960
      Picture         =   "frmDecoupe.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   23
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   4080
      Width           =   735
   End
   Begin VB.CheckBox CB_Num 
      Height          =   495
      Left            =   1680
      Picture         =   "frmDecoupe.frx":03D6
      Style           =   1  'Graphical
      TabIndex        =   22
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   4080
      Width           =   615
   End
   Begin VB.PictureBox PB 
      AutoRedraw      =   -1  'True
      Height          =   4335
      Index           =   2
      Left            =   3840
      ScaleHeight     =   4275
      ScaleWidth      =   10995
      TabIndex        =   20
      ToolTipText     =   "0"
      Top             =   5160
      Width           =   11055
   End
   Begin VB.PictureBox PB 
      AutoRedraw      =   -1  'True
      Height          =   4335
      Index           =   1
      Left            =   3840
      ScaleHeight     =   4275
      ScaleWidth      =   10995
      TabIndex        =   18
      ToolTipText     =   "0"
      Top             =   240
      Width           =   11055
   End
   Begin MSComctlLib.StatusBar SB 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   17
      Top             =   9585
      Width           =   15060
      _ExtentX        =   26564
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Btn_Calcul 
      BackColor       =   &H000080FF&
      Caption         =   "Calcul"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   15
      Tag             =   "9"
      ToolTipText     =   "0"
      Top             =   3240
      Width           =   1455
   End
   Begin VB.TextBox TB_LARGEUR 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      TabIndex        =   14
      Text            =   "Largeur"
      Top             =   1560
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.ComboBox Combo_Table 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   0
      Sorted          =   -1  'True
      TabIndex        =   13
      ToolTipText     =   "0"
      Top             =   0
      Width           =   3600
   End
   Begin VB.CommandButton Btn_Decoupe 
      BackColor       =   &H000000FF&
      Caption         =   "Decoupe"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   12
      Tag             =   "10"
      ToolTipText     =   "0"
      Top             =   3240
      Width           =   1455
   End
   Begin VB.TextBox TB_Vitesse 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2160
      TabIndex        =   10
      ToolTipText     =   "0"
      Top             =   2760
      Width           =   1395
   End
   Begin VB.TextBox TB_Offset_Y 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2160
      TabIndex        =   8
      ToolTipText     =   "0"
      Top             =   2400
      Width           =   1395
   End
   Begin VB.TextBox TB_Offset_X 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2160
      TabIndex        =   6
      ToolTipText     =   "0"
      Top             =   2040
      Width           =   1395
   End
   Begin VB.TextBox TB_Epaisseur_Bloc 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2160
      TabIndex        =   4
      ToolTipText     =   "0"
      Top             =   1200
      Width           =   1395
   End
   Begin VB.TextBox TB_Offset_Droit 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2160
      TabIndex        =   2
      ToolTipText     =   "0"
      Top             =   840
      Width           =   1395
   End
   Begin VB.TextBox TB_Offset_Gauche 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   2160
      TabIndex        =   0
      ToolTipText     =   "0"
      Top             =   480
      Width           =   1395
   End
   Begin VB.Label Label1 
      Caption         =   "Saumon"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Index           =   1
      Left            =   3840
      TabIndex        =   21
      Tag             =   "12"
      ToolTipText     =   "0"
      Top             =   4800
      Width           =   3855
   End
   Begin VB.Label Label1 
      Caption         =   "Emplanture"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Index           =   0
      Left            =   3840
      TabIndex        =   19
      Tag             =   "11"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   3855
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      Caption         =   "Largeur"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   0
      TabIndex        =   16
      Tag             =   "2"
      ToolTipText     =   "0"
      Top             =   1560
      Width           =   2115
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      Caption         =   "Vitesse en mm/s:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   0
      TabIndex        =   11
      Tag             =   "6"
      ToolTipText     =   "0"
      Top             =   2760
      Width           =   2115
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      Caption         =   "Offset Y en mm:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   0
      TabIndex        =   9
      Tag             =   "7"
      ToolTipText     =   "0"
      Top             =   2400
      Width           =   2115
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      Caption         =   "Offset X en mm:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   3
      Left            =   0
      TabIndex        =   7
      Tag             =   "5"
      ToolTipText     =   "0"
      Top             =   1920
      Width           =   2115
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      Caption         =   "Epaisseur du bloc en mm:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   0
      TabIndex        =   5
      Tag             =   "3"
      ToolTipText     =   "0"
      Top             =   1200
      Width           =   2115
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      Caption         =   "Offset droit en mm:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   0
      TabIndex        =   3
      Tag             =   "4"
      ToolTipText     =   "0"
      Top             =   840
      Width           =   2115
   End
   Begin VB.Label Label 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Offset gauche en mm:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   0
      TabIndex        =   1
      Tag             =   "2"
      ToolTipText     =   "0"
      Top             =   480
      Width           =   2115
   End
End
Attribute VB_Name = "frmDecoupe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim i As Long
Dim k As Integer

Dim Largeur(EMPLANTURE To SAUMON) As Long
Dim Hauteur(EMPLANTURE To SAUMON) As Long
Dim Gauche(EMPLANTURE To SAUMON) As Long
Dim Haut(EMPLANTURE To SAUMON) As Long

Dim bPoints As Boolean
Dim bNum As Boolean
Dim ListeDecalage() As ClsListeChainee

Private Sub Btn_Calcul_Click()
    Dim Course_X As Double
    Dim Course_Y As Double
    
    'Calcul d�calage
    ListeDecalage() = Calcul_Decalage_V3(gListePointSynchro, 3, "DEPRON")
    
    Call Dessin
    
    'Calcul de la trajectoire des chariots
    Tab_Chariot = Calcul_Trajectoire(ListeDecalage, CDbl(TB_Offset_Gauche), _
        CDbl(TB_Offset_Droit), CDbl(TB_Epaisseur_Bloc), CDbl(TB_Vitesse), Combo_Table)
    
    'Contr�le de la trajectoire des chariots
    Course_X = ReadINI_Machine("table_" & Combo_Table, "TB_SAV_16", 0)
    Course_Y = ReadINI_Machine("table_" & Combo_Table, "TB_SAV_17", 0)
    Call Ctrl_Trajectoire(Tab_Chariot, Course_X, Course_Y, CDbl(TB_Offset_X), CDbl(TB_Offset_Y), CDbl(TB_Vitesse))
    
    Btn_Decoupe.Enabled = True
    'frmMain.Form_Resize
    Call Form_Resize
End Sub

Private Sub Btn_Decoupe_Click()
    'Lancement d�coupe
    Btn_Decoupe.Enabled = False
    Call Envoi_Decoupe(Tab_Chariot, CDbl(TB_Offset_X), CDbl(TB_Offset_Y), CDbl(TB_Vitesse), maClasseHID, Me)
    Btn_Decoupe.Enabled = True
End Sub

Private Sub CB_Points_Click()
    bPoints = (CB_Points.value = vbChecked)
    Call Dessin
End Sub

Private Sub CB_Num_Click()
    bNum = (CB_Num.value = vbChecked)
    Call Dessin
End Sub

Private Sub Combo_Table_Change()
    TB_LARGEUR = ReadINI_Machine("table_" & Combo_Table, "TB_SAV_23", 0)
End Sub

Private Sub Form_Load()
    TB_Offset_Gauche = Format$(ReadINI(Me.Name & "Offset_Gauche", 200), "#0.0")
    TB_Offset_Droit = Format$(ReadINI(Me.Name & "Offset_Droit", 300), "#0.0")
    TB_Epaisseur_Bloc = Format$(ReadINI(Me.Name & "Epaisseur_Bloc", 100), "#0.0")
    TB_Offset_X = Format$(ReadINI(Me.Name & "Offset_X", 0), "#0.0")
    TB_Offset_Y = Format$(ReadINI(Me.Name & "Offset_Y", 0), "#0.0")
    TB_Vitesse = Format$(ReadINI(Me.Name & "Vitesse", 3), "#0.0")
    
    Dim Section() As String
    Dim i As Integer
    Dim maTable As String
    
    'Lecture table par d�faut
    gstrTable = ReadINI("TableDefaut", "MM2001")
    
    Set gFenetreHID = frmDecoupe
    ChargeTrad Me

    Combo_Table.Clear
    Call ListeSectionIni(App.Title & ".ini", Section)  '-- Param�tre Section pass� ByRef
    For i = LBound(Section) To UBound(Section)
        maTable = Section(i)
        If Left$(maTable, 6) = "table_" Then
            maTable = Mid$(maTable, 7, Len(maTable) - 6)
            Combo_Table.AddItem maTable
        End If
    Next
    On Error Resume Next        'Lancement sans .ini
    For i = 0 To Combo_Table.ListCount
        If Combo_Table.List(i) = gstrTable Then
            Combo_Table.ListIndex = i
        End If
    Next i
    
    ReDim ListeDecalage(EMPLANTURE To SAUMON) As ClsListeChainee
    ListeDecalage(EMPLANTURE) = New ClsListeChainee
    ListeDecalage(SAUMON) = New ClsListeChainee
    
    Call Dessin
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call WriteINI(Me.Name & "Offset_Gauche", TB_Offset_Gauche)
    Call WriteINI(Me.Name & "Offset_Droit", TB_Offset_Droit)
    Call WriteINI(Me.Name & "Epaisseur_Bloc", TB_Epaisseur_Bloc)
    Call WriteINI(Me.Name & "Offset_X", TB_Offset_X)
    Call WriteINI(Me.Name & "Offset_Y", TB_Offset_Y)
    Call WriteINI(Me.Name & "TB_Vitesse", TB_Vitesse)
End Sub

Private Sub Form_Resize()
    For k = EMPLANTURE To SAUMON
        Largeur(k) = PB(k).Width
        Hauteur(k) = PB(k).Height
        Gauche(k) = PB(k).Left
        Haut(k) = PB(k).Top
        Largeur(k) = PB(k).Width
        Hauteur(k) = PB(k).Height
        Gauche(k) = PB(k).Left
        Haut(k) = PB(k).Top
    Next k
    Call Dessin
End Sub

Private Sub TB_LARGEUR_Change()
    'On fait changer l'offset droit
    TB_Offset_Droit = TB_LARGEUR - TB_Offset_Gauche - TB_Epaisseur_Bloc
End Sub

Private Sub TB_Offset_Gauche_Change()
    Call WriteINI(Me.Name & "_Offset_Gauche", Val(TB_Offset_Gauche))
    'On fait changer l'offset droit
    On Error Resume Next
    TB_Offset_Droit = TB_LARGEUR - TB_Offset_Gauche - TB_Epaisseur_Bloc
End Sub

Private Sub TB_Offset_Droit_Change()
    Call WriteINI(Me.Name & "_Offset_Droit", Val(TB_Offset_Droit))
    'On fait changer l'offset gauche
    On Error Resume Next
    TB_Offset_Gauche = TB_LARGEUR - TB_Offset_Droit - TB_Epaisseur_Bloc
End Sub

Private Sub TB_Epaisseur_Bloc_Change()
    Call WriteINI(Me.Name & "_Epaisseur_Bloc", Val(TB_Epaisseur_Bloc))
    On Error Resume Next
    TB_Offset_Droit = TB_LARGEUR - TB_Offset_Gauche - TB_Epaisseur_Bloc
End Sub

Private Sub TB_Offset_X_Change()
    Call WriteINI(Me.Name & "_Offset_X", Val(TB_Offset_X))
End Sub

Private Sub TB_Offset_Y_Change()
    Call WriteINI(Me.Name & "_Offset_Y", Val(TB_Offset_Y))
End Sub

Private Sub TB_Vitesse_Change()
    Call WriteINI(Me.Name & "_Vitesse", Val(TB_Vitesse))
End Sub

Private Sub TB_Offset_Gauche_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Offset_Droit_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Epaisseur_Bloc_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Offset_X_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Offset_Y_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Vitesse_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Offset_Gauche_LostFocus()
    If Val(TB_Offset_Gauche) = 0 Then TB_Offset_Gauche = Format(0#, "#0.0")
    TB_Offset_Gauche = Round(TB_Offset_Gauche, 1)
End Sub

Private Sub TB_Offset_Droit_LostFocus()
    If Val(TB_Offset_Droit) = 0 Then TB_Offset_Droit = Format(0#, "#0.0")
    TB_Offset_Droit = Round(TB_Offset_Droit, 1)
End Sub

Private Sub TB_Epaisseur_Bloc_LostFocus()
    If Val(TB_Epaisseur_Bloc) = 0 Then TB_Epaisseur_Bloc = Format(0#, "#0.0")
    TB_Epaisseur_Bloc = Round(TB_Epaisseur_Bloc, 1)
End Sub

Private Sub TB_Offset_X_LostFocus()
    If Val(TB_Offset_X) = 0 Then TB_Offset_X = Format(0#, "#0.0")
    TB_Offset_X = Round(TB_Offset_X, 1)
End Sub

Private Sub TB_Offset_Y_LostFocus()
    If Val(TB_Offset_Y) = 0 Then TB_Offset_Y = Format(0#, "#0.0")
    TB_Offset_Y = Round(TB_Offset_Y, 1)
End Sub

Private Sub TB_Vitesse_LostFocus()
    If Val(TB_Vitesse) = 0 Then TB_Vitesse = Format(0#, "#0.0")
    TB_Vitesse = Round(TB_Vitesse, 1)
End Sub

Sub Dessin()
    'Dessin pour l'emplanture et le saumon,
    'pour le profil initial et le profil d�cal�
    Dim P1 As Point
    Dim P2 As Point
    Dim x1 As Double, y1 As Double, x2 As Double, y2 As Double
    Dim Rapport(EMPLANTURE To SAUMON) As Double
    Dim DeltaEpsilon As Double
    Dim DeltaX As Single
    Dim DeltaY As Single
    'Pour les fl�ches
    Dim PDepart As Point
    Dim pFleche As Point
    Dim bDecalage As Boolean    'D�calage calcul� ?
    
    DeltaEpsilon = 0.05
    
    x1 = Min(gListePointSynchro(EMPLANTURE).minX, gListePointSynchro(SAUMON).minX)
    x2 = Max(gListePointSynchro(EMPLANTURE).MaxX, gListePointSynchro(SAUMON).MaxX)
    y1 = Min(gListePointSynchro(EMPLANTURE).minY, gListePointSynchro(SAUMON).minY)
    y2 = Max(gListePointSynchro(EMPLANTURE).maxY, gListePointSynchro(SAUMON).maxY)
    On Error GoTo DecalageVide
    bDecalage = False
    bDecalage = Not ListeDecalage(EMPLANTURE).IsEmpty
    If bDecalage Then
        x1 = Min(x1, ListeDecalage(EMPLANTURE).minX)
        x1 = Min(x1, ListeDecalage(SAUMON).minX)
        x2 = Max(x2, ListeDecalage(EMPLANTURE).MaxX)
        x2 = Max(x2, ListeDecalage(SAUMON).MaxX)
        y1 = Min(y1, ListeDecalage(EMPLANTURE).minY)
        y1 = Min(y1, ListeDecalage(SAUMON).minY)
        y2 = Max(y2, ListeDecalage(EMPLANTURE).maxY)
        y2 = Max(y2, ListeDecalage(SAUMON).maxY)
    End If
DecalageVide:
    
    x1 = x1 - DeltaEpsilon * (x2 - x1)
    y1 = y1 - DeltaEpsilon * (y2 - y1)
    x2 = x2 + DeltaEpsilon * (x2 - x1)
    y2 = y2 + DeltaEpsilon * (y2 - y1)
    
    'Dessin profil initial
    For k = EMPLANTURE To SAUMON
        If gListePointSynchro(k).count <> 0 Then
            'Dimensionnement et placement des dessins
            Call PB_Dimension(PB(k), x1, y1, x2, y2, Largeur(k), Hauteur(k), Haut(k), Gauche(k))
            gListePointSynchro(k).MoveFirst
            P1 = gListePointSynchro(k).CurrentPoint
            For i = 1 To gListePointSynchro(k).count - 1
                gListePointSynchro(k).MoveNext
                P2 = gListePointSynchro(k).CurrentPoint
                PB(k).Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k)
                P1 = P2
            Next i
        End If
    Next k
   
   'Dessin profil d�cal�
    If bDecalage Then
        For k = EMPLANTURE To SAUMON
            ListeDecalage(k).MoveFirst
            P1 = ListeDecalage(k).CurrentPoint
            For i = 2 To ListeDecalage(k).count - 1
                ListeDecalage(k).MoveNext
                P2 = ListeDecalage(k).CurrentPoint
                    
                If i = 2 Then
                    PDepart = P1
                    pFleche = P2
                End If
                If PDepart.x = pFleche.x And _
                    PDepart.y = pFleche.y Then
                    pFleche = P1
                Else
                    'Trac� des fl�ches de sens
                    Call PB_Fleche(PB(k), PDepart, pFleche, CouleurFleche)
                    PB(k).DrawWidth = 1
                End If
                If i = Round(ListeDecalage(k).count / 2, 0) Then
                    PDepart = P1
                    pFleche = P2
                End If
'                PB(k).Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k)
                PB(k).Line (P1.x, P1.y)-(P2.x, P2.y), vbBlack
                If bPoints Or bNum Then
                    Call PB_Cercle(PB(k), P2, Couleur(k), Couleur(3 - k), _
                        False, False, False, bNum, IIf(bNum, i, 0))
                End If
                Debug.Print "P1 : " & P1.x & " " & P1.y & "       P2 : " & P2.x & " " & P2.y
                P1 = P2
            Next i
        Next k
    End If
End Sub
