Attribute VB_Name = "Module_PB"
Option Explicit
Dim AncienP As Point

Public Sub PB_Cercle(PB As PictureBox, P As Point, Couleur As Long, _
    CouleurInverse As Long, bSynchro As Boolean, bSel As Boolean, _
    bTemp As Boolean, Optional bNum As Boolean = False, Optional n As Long = 0)
    
    Dim maCouleur As Long
    Dim Rayon As Single
    Dim lg As Single
    Dim monTexte As String
    
    Rayon = Sqr((PB.ScaleWidth * PB.ScaleWidth) + (PB.ScaleHeight * PB.ScaleHeight)) / 250
    PB.DrawWidth = 1
    
    If bSel Or bSynchro Then
        maCouleur = CouleurInverse
    Else
        maCouleur = Couleur
    End If
    If bTemp Then
        maCouleur = RGB(0, 0, 0)
    End If
    If bSynchro Then
        lg = 3 * Rayon
        'On fait une croix
        PB.Line (P.x - lg, P.y)-(P.x + lg, P.y)
        PB.Line (P.x, P.y - lg)-(P.x, P.y + lg)
    End If
    
    If bNum Then
        monTexte = Format(n, "#")
        If n = 9 Then
            n = n
        End If
        If n < 10 Then
            monTexte = " " & monTexte & " "
        End If
        Rayon = (Max(PB.TextWidth(monTexte), PB.TextHeight(monTexte)) / 2) * 1.4
        If Calcul_Distance(AncienP, P) > Rayon * 1.5 Or bSel Then
            AncienP = P
            'Et on inscrit le n�
            PB.ForeColor = CouleurInverse
            PB.FontTransparent = False
            PB.CurrentX = P.x - PB.TextWidth(monTexte) / 2
            PB.CurrentY = P.y - PB.TextHeight(monTexte) / 2
            PB.FillStyle = vbFSSolid
            PB.Circle (P.x, P.y), Abs(Rayon), Couleur
            PB.FillStyle = vbFSTransparent
            PB.CurrentX = P.x - PB.TextWidth(monTexte) / 2
            PB.CurrentY = P.y - PB.TextHeight(monTexte) / 2
            PB.Print monTexte
'            PB.DrawStyle = vbSolid
            Exit Sub
        End If
    Else
        PB.Circle (P.x, P.y), Rayon, maCouleur
    End If

    If bSel Then
        Rayon = Rayon * 2#
        PB.Circle (P.x, P.y), Rayon, CouleurInverse
    End If
End Sub

Public Sub PB_Carre(PB As PictureBox, PDepart As Point, Couleur As Long)
    Dim RayonSequence As Single
    Dim PPrime As Point
    Dim Pseconde As Point
    Dim Aod As Double
    Dim wForecolor As Long
    
    wForecolor = PB.ForeColor
    PB.ForeColor = Couleur
    PB.DrawWidth = 1
    RayonSequence = Sqr((PB.ScaleWidth * PB.ScaleWidth) + (PB.ScaleHeight * PB.ScaleHeight)) / 75
    RayonSequence = Round(RayonSequence / 2#)
    PPrime = PDepart
    PPrime.x = PDepart.x - RayonSequence / 2
    PPrime.y = PDepart.y - RayonSequence / 2
    Pseconde = PPrime
    Pseconde.x = PPrime.x + RayonSequence
    PB.Line (PPrime.x, PPrime.y)-(Pseconde.x, Pseconde.y)
    PPrime = Pseconde
    PPrime.y = Pseconde.y + RayonSequence
    PB.Line (PPrime.x, PPrime.y)-(Pseconde.x, Pseconde.y)
    Pseconde = PPrime
    Pseconde.x = PPrime.x - RayonSequence
    PB.Line (PPrime.x, PPrime.y)-(Pseconde.x, Pseconde.y)
    PPrime = Pseconde
    PPrime.y = Pseconde.y - RayonSequence
    PB.Line (PPrime.x, PPrime.y)-(Pseconde.x, Pseconde.y)
    PB.ForeColor = wForecolor
End Sub

Public Sub PB_Dimension(PB As PictureBox, _
        x1 As Single, y1 As Single, x2 As Single, y2 As Single, _
        Largeur As Long, Hauteur As Long, Haut As Long, GAUCHE As Long)
    'Dimensionne une PictureBox en fonction des rapports largeur/hauteur de la zone o� l'on va la placer
    '    et du dessin que l'on veut afficher
    
    Dim Re As Double, Rd As Double
    
    'Calcul du rapport largeur/hauteur des dessins :
    Rd = Abs((x2 - x1) / (y2 - y1))
    'Calcul du rapport largeur/hauteur de la PictureBox :
    Re = Largeur / Hauteur
    
    'Dimension de la PictureBox
    If Rd > Re Then
        PB.Width = Largeur
        PB.Height = PB.Width / Abs(Rd)
        'Centrage
        PB.Left = GAUCHE
        PB.Top = Haut + (Hauteur - PB.Height) / 2
    Else
        PB.Height = Hauteur
        PB.Width = PB.Height * Abs(Rd)
        'Centrage
        PB.Left = GAUCHE + (Largeur - PB.Width) / 2
        PB.Top = Haut
    End If
        
    PB.Picture = LoadPicture   'Effacement de la PictureBox
    PB.FillColor = RGB(255, 255, 255)
    PB.FillStyle = vbFSTransparent
    PB.DrawStyle = vbSolid
    PB.DrawWidth = 1
    PB.ForeColor = RGB(255, 0, 255)
    PB.Scale (x1, y2)-(x2, y1)
End Sub

Public Sub PB_Fleche(PB As PictureBox, PDepart As Point, pFleche As Point, Couleur As Long)
    Dim RayonSequence As Single
    Dim PPrime As Point
    Dim PTierce As Point
    Dim Aod As Double
    
    PB.DrawWidth = 2
    RayonSequence = Sqr((PB.ScaleWidth * PB.ScaleWidth) + (PB.ScaleHeight * PB.ScaleHeight)) / 75
    'Calcul du point de la pointe de la fl�che
    Call Calcul_Inter_Droite_Cercle(PDepart, pFleche, PDepart, RayonSequence * 2, PPrime, PTierce)
    'Choix du point : le plus proche du point d'arriv�e
    If Calcul_Distance(PTierce, pFleche) < Calcul_Distance(PPrime, pFleche) Then
        PPrime = PTierce
    End If
    
    PB.Line (PDepart.x, PDepart.y)-(PPrime.x, PPrime.y), Couleur
    
    'On trace maintenant les deux traits de la pointe de la fl�che :
    Call Calcul_Ang_Segment(PDepart, PPrime, Aod)
    PTierce.x = PPrime.x + Cos(Aod + 3 * PI / 4) * RayonSequence
    PTierce.y = PPrime.y + Sin(Aod + 3 * PI / 4) * RayonSequence
    PB.Line (PPrime.x, PPrime.y)-(PTierce.x, PTierce.y), Couleur
    
    PTierce.x = PPrime.x + Cos(Aod - 3 * PI / 4) * RayonSequence
    PTierce.y = PPrime.y + Sin(Aod - 3 * PI / 4) * RayonSequence
    PB.Line (PPrime.x, PPrime.y)-(PTierce.x, PTierce.y), Couleur
    PB.DrawWidth = 1
End Sub

Public Sub PB_Profil_Tab(PB() As PictureBox, _
    TabPoints() As PointDecoupe, _
    Couleur() As Long, _
    CouleurFleche As Long, _
    points As Byte) '0=aucun point, 1=tous les points, 10=tous les 10 points
    'Dessin d'un profil contenu dans un tableau
    
    Dim i As Long
    Dim k As Integer
    Dim P1 As Point, P2 As Point
    Dim PDepart As Point, pFleche As Point
    
    For k = EMPLANTURE To SAUMON
        PB(k).DrawStyle = vbDot
        i = 1
        P1 = TabPoints(i).P(k)
        PDepart = P1
        Do
            i = i + 1
            P2 = TabPoints(i).P(k)
            If i = 2 Then
                pFleche = P2
            End If
            If PDepart.x = pFleche.x And _
                PDepart.y = pFleche.y Then
                pFleche = P1
            Else
                'Trac� des fl�ches de sens
                Call PB_Fleche(PB(k), PDepart, pFleche, CouleurFleche)
            End If
            If i = UBound(TabPoints) / 2 Then
                PDepart = P1
                pFleche = P1
            End If
            If points = 1 Or (points = 10 And i Mod 10 = 0) Then
                Call PB_Cercle(PB(k), P1, Couleur(k), Couleur(3 - k), _
                    False, _
                    False, _
                    False)
            End If
              
            'PB(k).Line (P1.x, P1.y)-(P2.x, P2.y), couleur(k)
            PB(k).Line (P1.x, P1.y)-(P2.x, P2.y), RGB(0, 0, 0)
            P1 = P2
             
        Loop While i < UBound(TabPoints)
        If points = 1 Or (points = 10 And i Mod 10 = 0) Then
            Call PB_Cercle(PB(k), P2, Couleur(k), Couleur(3 - k), _
                False, _
                False, _
                False)
        End If
    Next k
End Sub

Public Sub PB_Profil_LC(PB() As PictureBox, _
    LC() As ClsListeChainee, _
    Couleur() As Long, _
    CouleurFleche As Long, _
    points As Byte) '0=aucun point, 1=tous les points, 10=tous les 10 points
    'Dessin d'un profil contenu dans une liste cha�n�e
    
    Dim i As Long
    Dim k As Integer
    Dim P1 As Point, P2 As Point
    Dim PDepart As Point, pFleche As Point
    
    For k = EMPLANTURE To SAUMON
        With LC(k)
            PB(k).DrawStyle = vbNormal
            .MoveFirst
            P1 = .CurrentPoint
            PDepart = P1
            i = 1
            While .MoveNext
                P2 = .CurrentPoint
                If i = 1 Then pFleche = P2
                i = i + 1
                If PDepart.x = pFleche.x And _
                    PDepart.y = pFleche.y Then
                    pFleche = P1
                Else
                    'Trac� des fl�ches de sens
                    Call PB_Fleche(PB(k), PDepart, pFleche, CouleurFleche)
                End If
                If i = Int(.count / 2) Then
                    PDepart = P1
                    pFleche = P1
                End If
                If points = 1 Or (points = 10 And i Mod 10 = 0) Then
                    Call PB_Cercle(PB(k), P1, Couleur(k), Couleur(3 - k), _
                        False, _
                        False, _
                        False)
                End If
            
                PB(k).Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k)
                P1 = P2
            Wend
            If points = 1 Or (points = 10 And points Mod 10 = 0) Then
                Call PB_Cercle(PB(k), P2, Couleur(k), Couleur(3 - k), _
                    False, _
                    False, _
                    False)
            End If
        End With
    Next k
End Sub

Public Sub PB_Line(PB As PictureBox, P1 As Point, P2 As Point, Couleur As Long)
    PB.Line (P1.x, P1.y)-(P2.x, P2.y), Couleur
End Sub

Public Sub PB_Rectangle(PB As PictureBox, P1 As Point, P2 As Point, Couleur As Long, Optional Rempl As Boolean = False)
    If Rempl Then
        PB.Line (P1.x, P1.y)-(P2.x, P2.y), Couleur, BF
    Else
        PB.Line (P1.x, P1.y)-(P2.x, P2.y), Couleur, B
    End If
End Sub

Public Sub PB_DessinCoteV(PB As PictureBox, P1 As Point, P2 As Point)
    Dim P3 As Point
    
    PB.DrawStyle = vbSolid
    'Trait de cote
    Call PB_Line(PB, P1, P2, vbBlack)
    'Fl�che du bas
    P3.x = P1.x - 50
    P3.y = P1.y + 120
    Call PB_Line(PB, P1, P3, vbBlack)
    P3.x = P1.x + 50
    Call PB_Line(PB, P1, P3, vbBlack)
    
    'Fl�che du haut
    P3.x = P2.x - 50
    P3.y = P2.y - 120
    Call PB_Line(PB, P2, P3, vbBlack)
    P3.x = P2.x + 50
    Call PB_Line(PB, P2, P3, vbBlack)
End Sub

Public Sub PB_DessinCoteH(PB As PictureBox, P1 As Point, P2 As Point)
    Dim P3 As Point
    
    PB.DrawStyle = vbSolid
    'Trait de cote
    Call PB_Line(PB, P1, P2, vbBlack)
    'Fl�che de droite
    P3.x = P1.x + 120
    P3.y = P1.y + 50
    Call PB_Line(PB, P1, P3, vbBlack)
    P3.y = P1.y - 50
    Call PB_Line(PB, P1, P3, vbBlack)
    
    'Fl�che de gauche
    P3.x = P2.x - 120
    P3.y = P2.y + 50
    Call PB_Line(PB, P2, P3, vbBlack)
    P3.y = P2.y - 50
    Call PB_Line(PB, P2, P3, vbBlack)
End Sub

