VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "RP-FC"
   ClientHeight    =   3210
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   7365
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3210
   ScaleWidth      =   7365
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.Timer Timer_Refresh 
      Interval        =   100
      Left            =   2640
      Top             =   2760
   End
   Begin VB.Frame Frame_Decoupe 
      BackColor       =   &H00C0C0FF&
      Caption         =   "D�coupe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   3720
      TabIndex        =   2
      Tag             =   "2001"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   3615
      Begin VB.CommandButton CB_Tables 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Tables"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   1800
         Picture         =   "frmMain.frx":0ECA
         Style           =   1  'Graphical
         TabIndex        =   9
         Tag             =   "2016"
         ToolTipText     =   "tt2016"
         Top             =   1560
         Width           =   1665
      End
      Begin VB.CommandButton CB_Materiaux 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Mat�riaux"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   120
         Picture         =   "frmMain.frx":26E8
         Style           =   1  'Graphical
         TabIndex        =   8
         Tag             =   "2015"
         ToolTipText     =   "tt2015"
         Top             =   1560
         Width           =   1665
      End
      Begin VB.CommandButton CB_Guillotine 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Guillotine"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   1800
         Picture         =   "frmMain.frx":3F06
         Style           =   1  'Graphical
         TabIndex        =   7
         Tag             =   "2014"
         ToolTipText     =   "tt2014"
         Top             =   240
         Width           =   1665
      End
      Begin VB.CommandButton CB_Decoupe 
         BackColor       =   &H00C0C0FF&
         Caption         =   "D�coupe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   120
         Picture         =   "frmMain.frx":5724
         Style           =   1  'Graphical
         TabIndex        =   6
         Tag             =   "2013"
         ToolTipText     =   "tt2013"
         Top             =   240
         Width           =   1665
      End
   End
   Begin VB.Frame Frame_Projet 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Projet"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   0
      TabIndex        =   1
      Tag             =   "2000"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   3615
      Begin VB.CommandButton CB_Sauver 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Sauver projet"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   1800
         Picture         =   "frmMain.frx":6F42
         Style           =   1  'Graphical
         TabIndex        =   10
         Tag             =   "2017"
         ToolTipText     =   "tt2017"
         Top             =   1560
         Width           =   1665
      End
      Begin VB.CommandButton CB_Nouveau 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Nouveau"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   120
         Picture         =   "frmMain.frx":8760
         Style           =   1  'Graphical
         TabIndex        =   3
         Tag             =   "2010"
         ToolTipText     =   "tt2010"
         Top             =   240
         Width           =   1635
      End
      Begin VB.CommandButton CB_Modifier 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Modifier projet"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   120
         Picture         =   "frmMain.frx":9F7E
         Style           =   1  'Graphical
         TabIndex        =   5
         Tag             =   "2012"
         ToolTipText     =   "tt2012"
         Top             =   1560
         Width           =   1665
      End
      Begin VB.CommandButton CB_Ouvrir 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Ouvrir existant"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   1800
         Picture         =   "frmMain.frx":B79C
         Style           =   1  'Graphical
         TabIndex        =   4
         Tag             =   "2011"
         ToolTipText     =   "tt2011"
         Top             =   240
         Width           =   1665
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   2940
      Width           =   7365
      _ExtentX        =   12991
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Text            =   "IPL5X non connect�e"
            TextSave        =   "IPL5X non connect�e"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "25/03/2014"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            TextSave        =   "18:45"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "1000"
      Begin VB.Menu mnuFileNew 
         Caption         =   "1001"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "1006"
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "1003"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuFileSaveAs 
         Caption         =   "1003_1"
      End
      Begin VB.Menu mnuFileCut 
         Caption         =   "1005"
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMRU 
         Caption         =   "1004"
         Index           =   0
      End
      Begin VB.Menu mnuFileBar5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "1012"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "10011"
      Begin VB.Menu mnuFileProfil 
         Caption         =   "1002"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "1025"
      Begin VB.Menu mnuToolsTables 
         Caption         =   "1025_1"
      End
      Begin VB.Menu mnuToolsMats 
         Caption         =   "1025_2"
      End
      Begin VB.Menu mnuToolsLang 
         Caption         =   "1025_3"
      End
   End
   Begin VB.Menu mnuOutils 
      Caption         =   "1026"
      Begin VB.Menu mnuOutilsIPL5X 
         Caption         =   "1026_1"
      End
      Begin VB.Menu mnuOutilsMM2001 
         Caption         =   "1026_2"
      End
      Begin VB.Menu mnuOutilsGuillotine 
         Caption         =   "1026_3"
      End
      Begin VB.Menu mnuOutilsDefaut 
         Caption         =   "1026_4"
      End
   End
   Begin VB.Menu mnuHelpAbout 
      Caption         =   "1030"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
Private Declare Function GetActiveWindow Lib "user32" () As Long
Dim MonFichier As String

Private Sub CB_Decoupe_Click()
    Timer_Refresh.Enabled = False
    Call mnuFileCut_Click
End Sub

Private Sub CB_Guillotine_Click()
    Timer_Refresh.Enabled = False
    Call mnuOutilsGuillotine_Click
End Sub

Private Sub CB_Materiaux_Click()
    Timer_Refresh.Enabled = False
    Call mnuToolsMats_Click
End Sub

Private Sub CB_Modifier_Click()
    Call frmImporter.Form_Resize
    frmImporter.Show
End Sub

Private Sub CB_Nouveau_Click()
    Call mnuFileNew_Click
End Sub

Private Sub CB_Ouvrir_Click()
    If bSaveFC Then
        If MsgBox(ReadINI_Trad(gLangue, "erreurs", 206), vbCritical + vbYesNo) = vbYes Then
            Call CB_Sauver_Click
        End If
    End If
    Fichier = ""
    Call mnuFileOpen_Click
End Sub

Private Sub CB_Sauver_Click()
    Call mnuFileSaveAs_Click
End Sub

Private Sub CB_Tables_Click()
    Timer_Refresh.Enabled = False
    Call mnuToolsTables_Click
End Sub

Private Sub Form_Load()
    Dim k As Integer
    Dim i As Long
    Dim bPrem As Boolean
    Dim Index As Integer
    
    On Error Resume Next
    With Me
        Left = ReadINI_Num(Me.Name & "_Left", Me.Left)
        Top = ReadINI_Num(Me.Name & "_Top", Me.Top)
    End With
    On Error GoTo 0
    
    ChargeTrad Me
    Set gFenetreHID = frmMain
    Set maClasseHID = New ClsCdeIPL5X
    
    mnuFileCut.Enabled = False
    Me.CB_Decoupe.Enabled = False
    
    bMRU = False
    
    'Changer l'"AutoSize" de la StatusBar.
    For i = 1 To sbStatusBar.Panels.count
        With sbStatusBar.Panels(i)
            .AutoSize = sbrContents
            .Alignment = sbrCenter
            .Width = 500
        End With
    Next i
    
    gbModif = False
    
    'Chargement des donn�es
    MRUCount = NoMRUs
   
    ' Recherche des MRUs
    Call LectureMRU(Me)
    
    frmImporter.Hide
    FrmPilManClavier.Show
    bPrem = ReadINI_Num(Me.Name & "_Prem", True)
    If bPrem Then
        frmParamMachine.Show vbModeless, Me
        Call WriteINI(Me.Name & "_Prem", False)
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim monFich As String
    
    If bSaveFC Then
        If MsgBox(ReadINI_Trad(gLangue, "erreurs", 206), vbCritical + vbYesNo) = vbYes Then
            Call CB_Sauver_Click
        End If
    End If
   
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
    End If

    Set maClasseHID = Nothing
    Call EcritureMRU(Me)
    Unload frmImporter
    Unload FrmPilManClavier
    
    If gbInit Then
        'R�initialisation des valeurs par d�faut
        'Suppression du .ini
        If MsgBox(ReadINI_Trad(gLangue, "erreurs", 205), vbCritical + vbYesNo) = vbYes Then
            monFich = App.Path & "\" & "RP-FC.inisav"
            If Dir(monFich) <> "" Then
                Kill (monFich)
            End If
            Name App.Path & "\" & "RP-FC.ini" As monFich
        End If
    Else
        'Sauvegarde du .ini dans "Mes documents"
        Call FileCopy(App.Path & "\" & "RP-FC.ini", SpecFolder(CSIDL_PERSONAL) & "\" & "\" & "RP-FC.inisav")
        'Enregistrement des valeurs derni�rement utilis�es
        Call WriteINI("Offset_Gauche", Str(wOffset_Gauche))
        Call WriteINI("Offset_Droit", Str(wOffset_Droit))
        Call WriteINI("Offset_X", Str(wOffset_X))
        Call WriteINI("Offset_Y", Str(wOffset_Y))
        Call WriteINI("Vitesse", Str(wVitesse))
        Call WriteINI("RayonCompense", Str(wRayonCompense))
        Call WriteINI("Option_GD", Str(wOption_GD))
        Call WriteINI("Epaisseur", Str(wEpaisseur))
        Call WriteINI("LongueurBloc", Str(wLongueurBloc))
    End If
End Sub

Private Sub mnuFileCut_Click()
    MDIFrmDecoupe.Show vbModeless, Me
End Sub

Private Sub mnuFileOpen_Click()
    Dim InitialDir As String
    Dim monEnreg As String
    Dim monTab() As String
    
    'Ouvrir un projet
    If Left$(Fichier, 3) = "MRU" Then    'On est pass� par un MRU
       MonFichier = Right$(Fichier, Len(Fichier) - 3)
    Else
        MonFichier = ReadINI("FichierFC", "")
        ' et r�cup�ration du chemin
        InitialDir = GetPathName(MonFichier)
    
        monEnreg = "Fichiers projet (*.FC)" & Chr(0) & "*.FC" & Chr(0)
        monEnreg = monEnreg & "Fichiers export GMFC (*.txt)" & Chr(0) & "*.txt" & Chr(0)
        monEnreg = monEnreg & "Fichiers de Complexes (*.cpx)" & Chr(0) & "*.cpx" & Chr(0)
        MonFichier = RechercheFichier(Me, MonFichier, monEnreg)
        If Len(MonFichier) = 0 Then
            Exit Sub
        End If
        If UCase$(GetExtension(MonFichier)) <> "FC" And _
            UCase$(GetExtension(MonFichier)) <> "TXT" And _
            UCase$(GetExtension(MonFichier)) <> "CPX" Then
            Exit Sub
        End If
    End If
    
    Call UnixToDos(MonFichier)  'Conversion 0A -> 0D0A
    
    If UCase$(GetExtension(MonFichier)) = "FC" Then
        Open MonFichier For Input As #1
        Input #1, monEnreg
            If UCase$(Left(monEnreg, 6)) <> "ECORDE" Then
                Close #1
                Exit Sub
            End If
        Close #1
        Call OuvrirFC(MonFichier)
    ElseIf UCase$(GetExtension(MonFichier)) = "TXT" Then    'Fichier txt export de GMFC
        Call OuvrirGMFC(MonFichier)
        Else
            Call OuvrirCPX(MonFichier)
        End If
    
    'Mise � jour des MRUs
    Call AddMRU(MonFichier, Me)
End Sub

Public Sub OuvrirFC(MonFichier As String)
    Dim k As Integer
    Dim monEnreg As String
    Dim i As Long, ii As Long, j As Long
    Dim EouS As Integer
    Dim strX As String
    Dim strY As String
    Dim monCode As String
    Dim Temp As String
    Dim monElem As TypeNoeud
    Dim wDecalage As Single
        
    wComm = ""
    wDecalage = 0#
    For k = EMPLANTURE To SAUMON
        Set gListePointSynchro(k) = New ClsListeChainee
    Next k
    Call WriteINI("FichierFC", MonFichier)
    Open MonFichier For Input As #1
    Input #1, monEnreg
    While Not EOF(1)
        'On cherche le "="
        i = InStr(monEnreg, "=")
        If Left$(monEnreg, 11) = "Commentaire" Then
            monCode = "Commentaire"
        Else
            EouS = IIf(Left$(monEnreg, 1) = "E", 1, 2)
            monCode = Mid$(monEnreg, 2, i - 2)
        End If
        Temp = Replace(Right$(monEnreg, Len(monEnreg) - i), ".", Format$(0#, "#.#"))
        Select Case monCode
            'Corde
            Case "Corde"
                wCorde(EouS) = CDbl(Temp)
            'Coeff hauteur
            Case "xHauteur"
                wdimMult(EouS) = CDbl(Temp)
            'Miroir Vertical
            Case "MiroirVertical"
                bMiroirVertical(EouS) = IIf(Right$(monEnreg, Len(monEnreg) - i) = "true", True, False)
            'Miroir Horizontal
            Case "MiroirHorizontal"
                bMiroirHorizontal(EouS) = IIf(Right$(monEnreg, Len(monEnreg) - i) = "true", True, False)
            'Fleche BA
            Case "FlecheBA"
                wFleche_BA = CDbl(Temp)
            'Fleche BF
            Case "FlecheBF"
                wFleche_BF = CDbl(Temp)
            'Marge BA
            Case "MargeBA"
                wMargeBA(EouS) = CDbl(Temp)
            'Marge BF
            Case "MargeBF"
                wMargeBF(EouS) = CDbl(Temp)
            'Angle atteint (vrillage)
            Case "Vrillage"
                wAngle_Atteint(EouS) = CDbl(Temp)
            'Epaisseur
                Case "Epaisseur"
                wEpaisseur = CDbl(Temp)
            'Longueur bloc
                Case "LongueurBloc"
                wLongueurBloc = CDbl(Temp)
            'Envergure
                Case "Envergure"
                wEnvergure = CDbl(Temp)
            'MDessous
                Case "MDessous"
                wDessous(EouS) = CDbl(Temp)
            'Decalage       'Pr�sent uniquement dans les anciennes versions
                Case "Decalage"
                wDecalage = CDbl(Temp)
            'Commentaire
                Case "Commentaire"
                wComm = wComm & Temp & vbCrLf
            'Fichier
                Case "Fichier"
                wFichier(EouS) = Temp
            'Offset_Gauche
                Case "Offset_Gauche"
                wOffset_Gauche = Temp
            'Offset_X
                Case "Offset_X"
                wOffset_X = Temp
            'Offset_Y
                Case "Offset_Y"
                wOffset_Y = Temp
            'Vitesse
                Case "Vitesse"
                wVitesse = Temp
            'Compensation
                Case "RayonCompense"
                wRayonCompense = Temp
            'Bloc
            Case "Bloc0", "Bloc1", "Bloc2", "Bloc3", "Bloc4"
                ii = Val(Right(monCode, 1))
                strX = Right$(monEnreg, Len(monEnreg) - i)
                j = 1
                While Mid$(strX, j, 1) <> ";"
                    j = j + 1
                Wend
                strY = Right$(monEnreg, Len(strX) - j)
                strX = Left$(strX, j - 1)
                tabBloc(EouS, ii).x = CDbl(Replace(strX, ".", Format(0#, "#.#")))
                tabBloc(EouS, ii).y = CDbl(Replace(strY, ".", Format(0#, "#.#")))
                
            Case "in"
                GoTo Fermeture
            'Coordonn�es
            Case Else
                strX = Right$(monEnreg, Len(monEnreg) - i)
                j = 1
                While Mid$(strX, j, 1) <> ";"
                    j = j + 1
                Wend
                strY = Right$(monEnreg, Len(strX) - j)
                strX = Left$(strX, j - 1)
                monElem.x = CDbl(Replace(strX, ".", Format(0#, "#.#")))
                monElem.y = CDbl(Replace(strY, ".", Format(0#, "#.#")))
                gListePointSynchro(EouS).Insert monElem
                gListePointSynchro(EouS).MoveNext
        End Select
        Do
            Input #1, monEnreg
        Loop While Len(monEnreg) = 0
    Wend
    'On enl�ve le vbcrlf de la fin du commentaire
    If Len(wComm) > 2 Then
        wComm = Left$(wComm, Len(wComm) - 2)
    End If
    wDessous(SAUMON) = wDessous(SAUMON) + wDecalage
    
    'On recale en bas
    For k = EMPLANTURE To SAUMON
        gListePointSynchro(k).Decal 0#, -gListePointSynchro(k).minY
    Next k

Fermeture:
    Close #1
    If gListePointSynchro(EMPLANTURE).count = 0 Or _
        gListePointSynchro(SAUMON).count = 0 Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 111), vbCritical
            bProfilCharge = False
            Exit Sub
    End If
        
    bProfilCharge = False
    For k = EMPLANTURE To SAUMON
        wH(k) = gListePointSynchro(k).maxY - gListePointSynchro(k).minY
        frmImporter.TB_H(k) = Round(wH(k), 2)
        frmImporter.TB_DimMod(k) = wH(k) * wdimMult(k)
        nbPoints(k) = gListePointSynchro(k).count
    Next k
    Call frmImporter.RemplLV
    frmImporter.TB_Comm = wComm
    
'    'Sortie fichier des coordonn�es
'    Open "RP-FC_Lecture.txt" For Output As #1
'    Dim Dist As Single
'    For k = EMPLANTURE To SAUMON
'        Print #1, IIf(k = EMPLANTURE, "Emplanture", "Saumon")
'        Print #1, "x", "y", "distance"
'        i = 0
'        Dist = 0#
'        gListePointSynchro(k).MoveFirst
'        Do
'            If i > 0 Then
'                Dist = Calcul_Distance(gListePointSynchro(k).PreviousPoint, gListePointSynchro(k).CurrentPoint)
'            End If
'            Print #1, gListePointSynchro(k).CurrentPoint.x, gListePointSynchro(k).CurrentPoint.y, Dist
'            i = i + 1
'        Loop Until Not gListePointSynchro(k).MoveNext
'    Next k
'    Close #1


    bProfilCharge = True
    Me.Caption = "RP-FC <-> " & GetFileName(MonFichier)
    Call frmImporter.Form_Resize
    
    If Len(wComm) > 2 Then      'Il y a un commentaire !
        MsgBox wComm
    End If
    
    frmImporter.TabStrip1.Tabs(1).Selected = True
    Call frmImporter.CalculZoom
    frmImporter.Show

    For k = EMPLANTURE To SAUMON
        frmImporter.CB_Save_Profil_Orig(k).Enabled = True
        frmImporter.CB_Save_Profil_Deform(k).Enabled = True
        frmImporter.CB_Identique(k).Enabled = True
    Next k
    frmImporter.CB_FC_COMP(1).Enabled = True
    frmImporter.CB_FC_COMP(2).Enabled = True
    frmImporter.CB_Nettoyage.Enabled = True
End Sub

Public Sub OuvrirCPX(MonFichier As String)
    Dim k As Integer
    Dim monEnreg As String
    Dim i As Long, ii As Long, j As Long
    Dim EouS As Integer
    Dim strX As String
    Dim strY As String
    Dim monCode As String
    Dim Temp As String
    Dim monElem As TypeNoeud
    Dim wDecalage As Single
    Dim Pmin(EMPLANTURE To SAUMON) As Point
    Dim Pmax(EMPLANTURE To SAUMON) As Point
    Dim bDeb As Boolean
        
    wComm = ""
    wDecalage = 0#
    
    For k = EMPLANTURE To SAUMON
        Set gListePointSynchro(k) = New ClsListeChainee
    Next k
    Call WriteINI("FichierCPX", MonFichier)
    Open MonFichier For Input As #1
    While Not EOF(1)
        Do
            Input #1, monEnreg
        Loop While Len(monEnreg) = 0
        If Left$(monEnreg, 1) = "[" Then
            If monEnreg = "[Emplanture]" Then EouS = EMPLANTURE
            If monEnreg = "[Saumon]" Then EouS = SAUMON
            bDeb = True
        Else
            
        'On cherche le "="
        i = InStr(monEnreg, "=")
        monCode = Left$(monEnreg, i - 1)
        Temp = Replace(Right$(monEnreg, Len(monEnreg) - i), ".", Format$(0#, "#.#"))
        Select Case monCode
            'Fichier
            Case "NomFichier"
                wFichier(EouS) = Temp
            'Envergure
                Case "Ecartement"
                wEnvergure = CDbl(Temp)
            'Commentaire
                Case "Commentaire"
                wComm = wComm & Temp & vbCrLf
            'Coordonn�es
            Case Else
                If IsNumeric(monCode) Then
                    'Suppression du dernier ":" de la fin utilis� pour les points remarquables
                    j = InStrRev(Temp, ":")
                    monElem.bSynchro = (Val(Right$(Temp, 1)) >= 4)
                    Temp = Left$(Temp, j - 1)
                    
                    j = InStr(Temp, ":")
                    strX = Left$(Temp, j - 1)
                    strY = Right$(Temp, Len(Temp) - j)
                    monElem.x = CDbl(Replace(strX, ".", Format(0#, "#.#")))
                    monElem.y = CDbl(Replace(strY, ".", Format(0#, "#.#")))
                    If bDeb Then
                        bDeb = Not bDeb
                        Pmin(EouS).x = monElem.x
                        Pmin(EouS).y = monElem.y
                        Pmax(EouS).x = monElem.x
                        Pmax(EouS).y = monElem.y
                    Else
                        Pmin(EouS).x = Min(Pmin(EouS).x, monElem.x)
                        Pmin(EouS).y = Min(Pmin(EouS).y, monElem.y)
                        Pmax(EouS).x = Max(Pmax(EouS).x, monElem.x)
                        Pmax(EouS).y = Max(Pmax(EouS).y, monElem.y)
                    End If
                    gListePointSynchro(EouS).Insert monElem
                    gListePointSynchro(EouS).MoveNext
                End If
        End Select
        End If
    Wend
    'On enl�ve le vbcrlf de la fin du commentaire
    If Len(wComm) > 2 Then
        wComm = Left$(wComm, Len(wComm) - 2)
    End If
    
    For k = EMPLANTURE To SAUMON
        'Corde
        wCorde(k) = Pmax(k).x - Pmin(k).x
        'Coeff hauteur
        wdimMult(k) = 1#
        'Miroir Vertical
        bMiroirVertical(k) = False
        'Miroir Horizontal
        bMiroirHorizontal(k) = False
        'Fleche BA
        wFleche_BA = Pmin(SAUMON).x - Pmin(EMPLANTURE).x
        'Fleche BF
        wFleche_BF = Pmax(EMPLANTURE).x - Pmax(SAUMON).x
        'Marge BA
        wMargeBA(k) = 5#
        'Marge BF
        wMargeBF(k) = 5#
        'Angle atteint (vrillage)
        wAngle_Atteint(EouS) = 0#
        'Epaisseur
        wEpaisseur = 100#
        'MDessous
'        wDessous(EouS) = 5#
        wDessous(k) = Pmin(k).y
    Next k
Fermeture:
    Close #1
    If gListePointSynchro(EMPLANTURE).count = 0 Or _
        gListePointSynchro(SAUMON).count = 0 Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 111), vbCritical
            bProfilCharge = False
            Exit Sub
    End If
        
    bProfilCharge = False
    For k = EMPLANTURE To SAUMON
        wH(k) = gListePointSynchro(k).maxY - gListePointSynchro(k).minY
        frmImporter.TB_H(k) = Round(wH(k), 2)
        frmImporter.TB_DimMod(k) = Round(wH(k) * wdimMult(k), 2)
        nbPoints(k) = gListePointSynchro(k).count
    Next k
    Call frmImporter.RemplLV
    frmImporter.TB_Comm = wComm
    
    bProfilCharge = True
    Me.Caption = "RP-FC <-> " & GetFileName(MonFichier)
    Call frmImporter.Form_Resize
    
    frmImporter.TabStrip1.Tabs(1).Selected = True
    Call frmImporter.CalculZoom
    frmImporter.Show
    For k = EMPLANTURE To SAUMON
        frmImporter.CB_Save_Profil_Orig(k).Enabled = True
        frmImporter.CB_Save_Profil_Deform(k).Enabled = True
        frmImporter.CB_FC_COMP(1).Enabled = True
        frmImporter.CB_FC_COMP(2).Enabled = True
        frmImporter.CB_Nettoyage.Enabled = True
    Next k
End Sub

Public Sub OuvrirGMFC(MonFichier As String)
    Dim k As Integer
    Dim monEnreg As String
    Dim i As Long, ii As Long, j As Long
    Dim maVal As Long
    Dim EouS As Integer
    Dim strX As String
    Dim strY As String
    Dim monCode As String
    Dim Temp As String
    Dim monElem As TypeNoeud
    Dim f As Integer
    Dim buffer As String
    Dim t() As String
    Dim TempHaut As Single
        
    wComm = ""
    For k = EMPLANTURE To SAUMON
        Set gListePointSynchro(k) = New ClsListeChainee
    Next k
    Call WriteINI("FichierFC", Replace(MonFichier, ".txt", ".fc"))
    
    'Ouverture du fichier en 'Binary'
    f = FreeFile
    Open MonFichier For Binary As #f
        ' pr�allocation d'un buffer � la taille du fichier
        buffer = Space$(LOF(f))
        ' lecture compl�te du fichier
        Get #f, , buffer
    Close #f
    buffer = Replace(Replace(buffer, vbCrLf, vbLf), vbCr & vbCr, vbCr)
    t = Split(buffer, vbLf)
    
    EouS = 0
    f = 0
    Do
        If Left$(t(f), 7) = "Fleche:" Then
            wFleche_BA = RechercheNum(t(f))
        End If
        If Left$(t(f), 15) = "Demi envergure:" Then
            wEnvergure = RechercheNum(t(f))
        End If
        If Left$(t(f), 9) = "Marge BA:" Then
            If EouS = TOUT Then
                wMargeBA(EMPLANTURE) = RechercheNum(t(f))
                wMargeBA(SAUMON) = RechercheNum(t(f))
            Else
                wMargeBA(EouS) = RechercheNum(t(f))
            End If
        End If
        If Left$(t(f), 9) = "Marge BF:" Then
            If EouS = TOUT Then
                wMargeBF(EMPLANTURE) = RechercheNum(t(f))
                wMargeBF(SAUMON) = RechercheNum(t(f))
            Else
                wMargeBF(EouS) = RechercheNum(t(f))
            End If
        End If
        If Left$(t(f), 16) = "Hauteur de base:" Then
            TempHaut = RechercheNum(t(f))
            If Abs(TempHaut) > 1000# Then
                MsgBox ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 115) & " " & t(f), vbCritical + vbOKOnly
            End If

            If EouS = TOUT Then
                wDessous(EMPLANTURE) = TempHaut
                wDessous(SAUMON) = TempHaut
            Else
                wDessous(EouS) = TempHaut
            End If
        End If
            
        If Left$(t(f), 13) = "Commentaires:" Then
            If Len(t(f)) > 13 Then
                wComm = Right$(t(f), Len(t(f)) - 14) & vbCrLf
            End If
            For f = f + 1 To UBound(t)
                wComm = wComm & t(f) & vbCrLf
            Next f
            Exit Do
        End If
        If Left$(t(f), 29) = "Emplanture/Saumon identiques:" Then
            If Mid$(t(f), 31) = "O" Then EouS = TOUT
        End If
        If EouS <> TOUT And Left$(t(f), 11) = "Emplanture:" Then
            EouS = EMPLANTURE
        End If
        If EouS <> TOUT And Left$(t(f), 7) = "Saumon:" Then
            EouS = SAUMON
        End If
        If Left$(t(f), 4) = "Nom:" Then
            If EouS = TOUT Then
                wFichier(EMPLANTURE) = Right$(t(f), Len(t(f)) - 5)
                wFichier(SAUMON) = Right$(t(f), Len(t(f)) - 5)
            Else
                wFichier(EouS) = Right$(t(f), Len(t(f)) - 5)
            End If
        End If
        If Left$(t(f), 6) = "Corde:" Then
            If EouS = TOUT Then
                wCorde(EMPLANTURE) = RechercheNum(t(f))
                wCorde(SAUMON) = RechercheNum(t(f))
            Else
                wCorde(EouS) = RechercheNum(t(f))
            End If
        End If
        If Left$(t(f), 16) = "Hauteur de base:" Then
            If EouS = TOUT Then
                wDessous(EMPLANTURE) = RechercheNum(t(f))
                wDessous(SAUMON) = RechercheNum(t(f))
            Else
                wDessous(EouS) = RechercheNum(t(f))
            End If
        End If
        If Left$(t(f), 9) = "Extrados:" Then
'Debug.Print "Extrados"
            maVal = RechercheNum(t(f))
            For f = f + 1 To f + maVal
                'Recherche d'un espace dans la ligne
                j = InStr(t(f), " ")
                strX = Left$(t(f), j - 1)
                strY = Right$(t(f), Len(t(f)) - j)
'Debug.Print strX, strY
                monElem.x = CDbl(Replace(strX, ".", Format(0#, "#.#")))
                monElem.y = CDbl(Replace(strY, ".", Format(0#, "#.#")))
                If EouS = TOUT Then
                    gListePointSynchro(EMPLANTURE).Insert monElem
                    gListePointSynchro(EMPLANTURE).MoveNext
                    gListePointSynchro(SAUMON).Insert monElem
                    gListePointSynchro(SAUMON).MoveNext
                Else
                    gListePointSynchro(EouS).Insert monElem
                    gListePointSynchro(EouS).MoveNext
                End If
            Next f
            If EouS = TOUT Then
                gListePointSynchro(EMPLANTURE).Inversion
                gListePointSynchro(SAUMON).Inversion
            Else
                gListePointSynchro(EouS).Inversion
            End If
        End If

        If Left$(t(f), 9) = "Intrados:" Then
'Debug.Print "Intrados"
            maVal = RechercheNum(t(f))
            For f = f + 1 To f + maVal
                'Recherche d'un espace dans la ligne
                j = InStr(t(f), " ")
                strX = Left$(t(f), j - 1)
                strY = Right$(t(f), Len(t(f)) - j)
'Debug.Print strX, strY
                monElem.x = CDbl(Replace(strX, ".", Format(0#, "#.#")))
                monElem.y = CDbl(Replace(strY, ".", Format(0#, "#.#")))
                If EouS = TOUT Then
                    gListePointSynchro(EMPLANTURE).Insert monElem
                    gListePointSynchro(EMPLANTURE).MoveNext
                    gListePointSynchro(SAUMON).Insert monElem
                    gListePointSynchro(SAUMON).MoveNext
                Else
                    gListePointSynchro(EouS).Insert monElem
                    gListePointSynchro(EouS).MoveNext
                End If
            Next f
        End If
        
        f = f + 1
    Loop While f <= UBound(t)
    'On enl�ve le vbcrlf de la fin
    If Len(wComm) > 2 Then
        wComm = Left$(wComm, Len(wComm) - 2)
    End If
    
    If gListePointSynchro(EMPLANTURE).count = 0 Or _
        gListePointSynchro(SAUMON).count = 0 Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 111), vbCritical
            bProfilCharge = False
            Exit Sub
    End If
    
    'Il faut recalculer les coordonn�es � la bonne corde !
    For k = EMPLANTURE To SAUMON
        Call gListePointSynchro(k).ModifCorde(wCorde(k))
    Next k
    
    bProfilCharge = False
    For k = EMPLANTURE To SAUMON
        wH(k) = gListePointSynchro(k).maxY - gListePointSynchro(k).minY
        frmImporter.TB_H(k) = Round(wH(k), 2)
        frmImporter.TB_DimMod(k) = wH(k) * wdimMult(k)
        nbPoints(k) = gListePointSynchro(k).count
    Next k
    Call frmImporter.RemplLV
    frmImporter.TB_Comm = wComm

    bProfilCharge = True
    Me.Caption = "RP-FC <-> " & GetFileName(MonFichier)
    Call frmImporter.Form_Resize
    
    frmImporter.TabStrip1.Tabs(1).Selected = True
    Call frmImporter.CalculZoom
    frmImporter.Show
    For k = EMPLANTURE To SAUMON
        frmImporter.CB_Save_Profil_Orig(k).Enabled = True
        frmImporter.CB_Save_Profil_Deform(k).Enabled = True
        frmImporter.CB_FC_COMP(1).Enabled = True
        frmImporter.CB_FC_COMP(2).Enabled = True
        frmImporter.CB_Nettoyage.Enabled = True
    Next k
End Sub

Private Sub mnuFileSave_Click()
    If Me.Caption = "RP-FC" Then
        Call mnuFileSaveAs_Click
    Else
        Call SauverFC
    End If
End Sub

Private Sub mnuFileSaveAs_Click()
    If Not bProfilCharge Then Exit Sub
    If MonFichier = "" Then MonFichier = "New.fc"
    MonFichier = Replace(MonFichier, "." & GetExtension(MonFichier), ".fc")
    
    MonFichier = RechercheFichier(Me, MonFichier, "Fichiers projet FC" & Chr(0) & "*.FC" & Chr(0), True)
    If Len(MonFichier) = 0 Then
        Exit Sub
    End If
    If GetExtension(MonFichier) = "" Then MonFichier = MonFichier & ".fc"
    Call WriteINI("FichierFC", MonFichier)
    Call SauverFC
    Me.Caption = "RP-FC <-> " & GetFileName(MonFichier)
    bSaveFC = False
End Sub
Private Sub SauverFC()
    Dim i As Long
    Dim k As Integer
    Dim monEnreg As String
    Dim InitialDir As String
    Dim EouS As String * 1
    Dim monTab() As String
    
    If GetExtension(MonFichier) = "" Then MonFichier = MonFichier & ".fc"
    Call WriteINI("FichierFC", MonFichier)
    Open MonFichier For Output As #1    'On efface tout
    Close #1
        
    Open MonFichier For Append As #1
   
    For k = EMPLANTURE To SAUMON
        EouS = IIf(k = EMPLANTURE, "E", "S")
        
        '1�) Ecriture des valeurs
        'Corde
        monEnreg = EouS & "Corde=" & wCorde(k)
        Call MEFEnreg(monEnreg)
        'Coeff hauteur
        monEnreg = EouS & "xHauteur=" & wdimMult(k)
        Call MEFEnreg(monEnreg)
        'Miroir Vertical
        monEnreg = EouS & "MiroirVertical=" & IIf(bMiroirVertical(k), "true", "false")
        Call MEFEnreg(monEnreg)
        'Miroir Horizontal
        monEnreg = EouS & "MiroirHorizontal=" & IIf(bMiroirHorizontal(k), "true", "false")
        Call MEFEnreg(monEnreg)
        'Fleche BA
        monEnreg = EouS & "FlecheBA=" & wFleche_BA
        Call MEFEnreg(monEnreg)
        'Fleche BF
        monEnreg = EouS & "FlecheBF=" & wFleche_BF
        Call MEFEnreg(monEnreg)
        'Marge BA
        monEnreg = EouS & "MargeBA=" & wMargeBA(k)
        Call MEFEnreg(monEnreg)
        'Marge BF
        monEnreg = EouS & "MargeBF=" & wMargeBF(k)
        Call MEFEnreg(monEnreg)
        'Angle atteint (vrillage)
        monEnreg = EouS & "Vrillage=" & wAngle_Atteint(k)
        Call MEFEnreg(monEnreg)
        'Epaisseur
        monEnreg = EouS & "Epaisseur=" & wEpaisseur
        Call MEFEnreg(monEnreg)
        'Longueur bloc
        monEnreg = EouS & "LongueurBloc=" & wLongueurBloc
        Call MEFEnreg(monEnreg)
        'Envergure
        monEnreg = EouS & "Envergure=" & wEnvergure
        Call MEFEnreg(monEnreg)
        'MDessous
        monEnreg = EouS & "MDessous=" & wDessous(k)
        Call MEFEnreg(monEnreg)
        'Nom du fichier de profil
        monEnreg = EouS & "Fichier=" & wFichier(k)
        Call MEFEnreg(monEnreg)
        'Offset_Gauche
            monEnreg = EouS & "Offset_Gauche=" & wOffset_Gauche
            Call MEFEnreg(monEnreg)
        'Offset_X
            monEnreg = EouS & "Offset_X=" & wOffset_X
            Call MEFEnreg(monEnreg)
        'Offset_Y
            monEnreg = EouS & "Offset_Y=" & wOffset_Y
            Call MEFEnreg(monEnreg)
        'Vitesse
            monEnreg = EouS & "Vitesse=" & wVitesse
            Call MEFEnreg(monEnreg)
        'Compensation
            monEnreg = EouS & "RayonCompense=" & wRayonCompense
            Call MEFEnreg(monEnreg)
        'Bloc
        For i = 0 To 4
            monEnreg = EouS & "Bloc" & i & "=" & _
                tabBloc(k, i).x & ";" & _
                tabBloc(k, i).y
            Call MEFEnreg(monEnreg)
        Next i
            
        '2�) Copie des coordonn�es
        i = 1
        gListePointSynchro(k).MoveFirst
        Do
            monEnreg = EouS & Format$(i, "0000") & "=" & _
                       CDbl(gListePointSynchro(k).CurrentPoint.x) & ";" & _
                       CDbl(gListePointSynchro(k).CurrentPoint.y)
            Call MEFEnreg(monEnreg)
            i = i + 1
        Loop While gListePointSynchro(k).MoveNext
    Next k
    'todo
    'Sauvegarde des param�tres de d�coupe
    
    'Commentaire
    monTab = Split(wComm, vbCrLf)
    For i = LBound(monTab) To UBound(monTab)
        monEnreg = "Commentaire" & i & "=" & Replace(monTab(i), vbCr, "")
        Call MEFEnreg(monEnreg)
    Next i

    monEnreg = "Fin="
    Call MEFEnreg(monEnreg)

    Close #1
    'Mise � jour des MRUs
    Call AddMRU(MonFichier, Me)
End Sub

Private Sub mnuMRU_Click(Index As Integer)
    MonFichier = mnuMRU(Index).Caption
    On Error Resume Next
    If Not FileExists(MonFichier) Then
        Call DeleteMRU(MonFichier, Me)
    Else
        Fichier = "MRU" & MonFichier
        Call mnuFileOpen_Click
'        Call OuvrirFC(MonFichier)
    End If
End Sub

Private Sub mnuOutilsDefaut_Click()
    gbInit = True
    Unload Me
End Sub

Private Sub mnuOutilsGuillotine_Click()
    frmGuillotine.Show vbModal, Me
End Sub

Private Sub mnuOutilsIPL5X_Click()
    If Not maClasseHID.IsAvailable Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 999), vbCritical + vbOKOnly
        Exit Sub
    End If
    frmMain.Timer_Refresh.Enabled = False
    frmTestIPL.Show vbModal, Me
    frmMain.Timer_Refresh.Enabled = True
End Sub

Private Sub mnuOutilsMM2001_Click()
    frmMM2001.Show vbModal, Me
End Sub

Private Sub mnuToolsLang_Click()
    FrmParamAppli.Show vbModal, Me
End Sub

Private Sub mnuToolsMats_Click()
    frmParamMateriau.Show vbModal, Me
End Sub

Private Sub mnuToolsTables_Click()
    frmParamMachine.Show vbModeless, Me
End Sub

Private Sub mnuHelpAbout_Click()
    frmAbout.Show vbModal, Me
End Sub

Private Sub mnuFileExit_Click()
    'd�charge la feuille
    Unload frmImporter
    Unload Me
End Sub

Private Sub mnuFileProfil_Click()
    Call frmImporter.ValeurDefaut
    Call frmImporter.Form_Resize
    frmImporter.TabStrip1.Tabs(1).Selected = True
    frmImporter.Show
End Sub

Private Sub mnuFileNew_Click()
    Dim k As Integer
    
    Me.Caption = "RP-FC"
    bProfilCharge = False
    For k = EMPLANTURE To SAUMON
        nbPoints(k) = 0
        frmImporter.PB_Synchro(k).Picture = LoadPicture
        frmImporter.PB_Profil1(k).Picture = LoadPicture
        frmImporter.PB_Vue_Dessus.Picture = LoadPicture
        frmImporter.PB_ProfilCote.Picture = LoadPicture
        frmImporter.PB_Bloc(k).Picture = LoadPicture
        frmImporter.LV_Synchro(k).ListItems.Clear
    Next k
    Call mnuFileProfil_Click
    mnuFileCut.Enabled = False
    CB_Decoupe.Enabled = False
End Sub

Private Sub Timer_Refresh_Timer()
    'Il faut invalider le timer d�s que l'on clique sur autre chose
    If maClasseHID.IsAvailable Then
        sbStatusBar.Panels(1) = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 77)
    Else
        sbStatusBar.Panels(1) = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 78)
    End If
End Sub
