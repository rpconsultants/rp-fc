Attribute VB_Name = "Module_MRU"
Option Explicit

'Constantes et variables pour les MRU
Global Const NoMRUs = -1     'Indique qu'il n'y a pas actuellement de MRUs

Global MRUCount As Long
Global bMRU As Boolean         'Pour savoir si on ouvre un MRU

Sub LectureMRU(f As Form)
    Dim i As Long
    Dim maChaine As String    'Nom du MRU lu
   
    MRUCount = 0
   
    For i = 0 To 8
        If i >= f.mnuMRU.count Then
            Load f.mnuMRU(i)
        End If

        f.mnuMRU(i).Visible = False
        maChaine = ReadINI("MRUFiles" & Trim$(CStr(i + 1)), "")
        If maChaine <> "" Then
            f.mnuMRU(i).Visible = True
            f.mnuMRU(i).Caption = maChaine
            MRUCount = MRUCount + 1
        End If
    Next i
    
    If MRUCount > 0 Then
        Call RechercheDoublonMRU(f)
    End If
End Sub

Sub RechercheDoublonMRU(f As Form)
'Recherche doublon et suppression �ventuelle du second
    Dim i As Long
    Dim j As Long
    Dim k As Long
    ReDim tabMRU(0 To MRUCount) As String
    
    On Error Resume Next
   
    For i = 0 To MRUCount - 1   'Pour chaque entr�e du menu MRU
        'Recherche en table
        For j = 0 To MRUCount - 1
            If f.mnuMRU(i).Caption = tabMRU(j) Then
                'Doublon
                f.mnuMRU(i).Visible = False
                For k = i To MRUCount - 2
                    f.mnuMRU(i).Caption = f.mnuMRU(i + 1).Caption
                Next k
                f.mnuMRU(k).Visible = False
                MRUCount = MRUCount - 1
            End If
        Next j
    Next i
End Sub

Sub AddMRU(monMRU As String, f As Form)
    Dim i As Long
    Dim MaxMRU As Long              'Compte le nombre de MRUs m�moris�s

    ' Lecture de la valeur "MaxMRU" : Nombre de MRU m�moris�s
    MaxMRU = ReadINI("gMaxMRU", "10")
    
    Call DeleteMRU(monMRU, f)

    'D�calage � partir de l'avant dernier
    For i = MRUCount - 1 To 1 Step -1
        f.mnuMRU(i).Caption = f.mnuMRU(i - 1).Caption
    Next i
    f.mnuMRU(0).Caption = monMRU
    
    If MRUCount < MaxMRU Then
        MRUCount = MRUCount + 1
    End If
    
    Call RechercheDoublonMRU(f)
    
    For i = 0 To MRUCount - 1
        f.mnuMRU(i).Visible = (i < MaxMRU)
    Next i

End Sub

Sub DeleteMRU(monMRU As String, f As Form)
    Dim i As Long
    Dim k As Long
    Dim Indice As Long
        
    'Recherche du MRU
    Indice = -1
    For i = 0 To MRUCount - 1
        If f.mnuMRU(i).Caption = monMRU Then
            Indice = i
        End If
    Next i
    
    If Indice = -1 Then
        Exit Sub
    End If
    
    For k = Indice To MRUCount - 2
        f.mnuMRU(k).Caption = f.mnuMRU(k + 1).Caption
    Next k
    If k < MRUCount - 1 Then
        f.mnuMRU(k).Visible = False
    Else
        f.mnuMRU(MRUCount - 1).Visible = False
    End If
    MRUCount = MRUCount - 1
End Sub

Sub EcritureMRU(f As Form)
    Dim i As Long
   
    For i = 0 To 8
    ' Ecriture de la MRU list dans le .ini
        If f.mnuMRU(i).Visible Then
            Call WriteINI("MRUFiles" & Trim$(CStr(i + 1)), f.mnuMRU(i).Caption)
        Else
            Call DeleteINI("MRUFiles" & Trim$(CStr(i + 1)))
        End If
    Next i
End Sub


