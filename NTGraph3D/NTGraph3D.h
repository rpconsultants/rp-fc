/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Tue Jul 29 13:35:43 2003
 */
/* Compiler settings for E:\Microsoft Visual Studio\MyProjects\NTGraph3D_ATL\NTGraph3D_ATL\NTGraph3D_src\NTGraph3D.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __NTGraph3D_h__
#define __NTGraph3D_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IGraph3D_FWD_DEFINED__
#define __IGraph3D_FWD_DEFINED__
typedef interface IGraph3D IGraph3D;
#endif 	/* __IGraph3D_FWD_DEFINED__ */


#ifndef __NTGraph3D_FWD_DEFINED__
#define __NTGraph3D_FWD_DEFINED__

#ifdef __cplusplus
typedef class NTGraph3D NTGraph3D;
#else
typedef struct NTGraph3D NTGraph3D;
#endif /* __cplusplus */

#endif 	/* __NTGraph3D_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IGraph3D_INTERFACE_DEFINED__
#define __IGraph3D_INTERFACE_DEFINED__

/* interface IGraph3D */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IGraph3D;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("74881C3E-3B4A-4134-9069-1B6E6B2E886F")
    IGraph3D : public IDispatch
    {
    public:
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_BackColor( 
            /* [in] */ OLE_COLOR clr) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_BackColor( 
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pclr) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_BorderStyle( 
            /* [in] */ long style) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_BorderStyle( 
            /* [retval][out] */ long __RPC_FAR *pstyle) = 0;
        
        virtual /* [id][propputref] */ HRESULT STDMETHODCALLTYPE putref_Font( 
            /* [in] */ IFontDisp __RPC_FAR *pFont) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Font( 
            /* [in] */ IFontDisp __RPC_FAR *pFont) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Font( 
            /* [retval][out] */ IFontDisp __RPC_FAR *__RPC_FAR *ppFont) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Caption( 
            /* [in] */ BSTR strCaption) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Caption( 
            /* [retval][out] */ BSTR __RPC_FAR *pstrCaption) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_BorderVisible( 
            /* [in] */ VARIANT_BOOL vbool) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_BorderVisible( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbool) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Appearance( 
            /* [in] */ short appearance) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Appearance( 
            /* [retval][out] */ short __RPC_FAR *pappearance) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CaptionColor( 
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CaptionColor( 
            /* [in] */ OLE_COLOR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetRange( 
            double xmin,
            double xmax,
            double ymin,
            double ymax,
            double zmin,
            double zmax) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AutoRange( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowPropertyPages( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TrackMode( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TrackMode( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Projection( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Projection( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XLabel( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_XLabel( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YLabel( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_YLabel( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ZLabel( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ZLabel( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XGridNumber( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_XGridNumber( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YGridNumber( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_YGridNumber( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ZGridNumber( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ZGridNumber( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XGridColor( 
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_XGridColor( 
            /* [in] */ OLE_COLOR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YGridColor( 
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_YGridColor( 
            /* [in] */ OLE_COLOR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ZGridColor( 
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ZGridColor( 
            /* [in] */ OLE_COLOR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddElement( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteElement( 
            short ElementID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearGraph( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementLineColor( 
            short ElementID,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementLineColor( 
            short ElementID,
            /* [in] */ OLE_COLOR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementPointColor( 
            short ElementID,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementPointColor( 
            short ElementID,
            /* [in] */ OLE_COLOR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementLineWidth( 
            short ElementID,
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementLineWidth( 
            short ElementID,
            /* [in] */ float newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementPointSize( 
            short ElementID,
            /* [retval][out] */ float __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementPointSize( 
            short ElementID,
            /* [in] */ float newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementType( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementType( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PlotXYZ( 
            double x,
            double y,
            double z,
            short ElementID) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementShow( 
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementShow( 
            short ElementID,
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementSurfaceFill( 
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementSurfaceFill( 
            short ElementID,
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementSurfaceFlat( 
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementSurfaceFlat( 
            short ElementID,
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementLight( 
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementLight( 
            short ElementID,
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementLightingAmbient( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementLightingAmbient( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementLightingDiffuse( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementLightingDiffuse( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementLightingSpecular( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementLightingSpecular( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementMaterialAmbient( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementMaterialAmbient( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementMaterialDiffuse( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementMaterialDiffuse( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementMaterialSpecular( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementMaterialSpecular( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementMaterialShinines( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementMaterialShinines( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementMaterialEmission( 
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementMaterialEmission( 
            short ElementID,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLightCoordinates( 
            short ElementID,
            float x,
            float y,
            float z) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CopyToClipboard( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Lighting( 
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Lighting( 
            short ElementID,
            /* [in] */ BOOL newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGraph3DVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IGraph3D __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IGraph3D __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IGraph3D __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_BackColor )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ OLE_COLOR clr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BackColor )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pclr);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_BorderStyle )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ long style);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BorderStyle )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pstyle);
        
        /* [id][propputref] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *putref_Font )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ IFontDisp __RPC_FAR *pFont);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Font )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ IFontDisp __RPC_FAR *pFont);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Font )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ IFontDisp __RPC_FAR *__RPC_FAR *ppFont);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Caption )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ BSTR strCaption);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Caption )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pstrCaption);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_BorderVisible )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ VARIANT_BOOL vbool);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BorderVisible )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbool);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Appearance )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ short appearance);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Appearance )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pappearance);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CaptionColor )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_CaptionColor )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ OLE_COLOR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetRange )( 
            IGraph3D __RPC_FAR * This,
            double xmin,
            double xmax,
            double ymin,
            double ymax,
            double zmin,
            double zmax);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AutoRange )( 
            IGraph3D __RPC_FAR * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ShowPropertyPages )( 
            IGraph3D __RPC_FAR * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TrackMode )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TrackMode )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Projection )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Projection )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_XLabel )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_XLabel )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_YLabel )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_YLabel )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ZLabel )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ZLabel )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_XGridNumber )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_XGridNumber )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_YGridNumber )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_YGridNumber )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ZGridNumber )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ZGridNumber )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_XGridColor )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_XGridColor )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ OLE_COLOR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_YGridColor )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_YGridColor )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ OLE_COLOR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ZGridColor )( 
            IGraph3D __RPC_FAR * This,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ZGridColor )( 
            IGraph3D __RPC_FAR * This,
            /* [in] */ OLE_COLOR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AddElement )( 
            IGraph3D __RPC_FAR * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DeleteElement )( 
            IGraph3D __RPC_FAR * This,
            short ElementID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ClearGraph )( 
            IGraph3D __RPC_FAR * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementLineColor )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementLineColor )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ OLE_COLOR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementPointColor )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementPointColor )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ OLE_COLOR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementLineWidth )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementLineWidth )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ float newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementPointSize )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ float __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementPointSize )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ float newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementType )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementType )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *PlotXYZ )( 
            IGraph3D __RPC_FAR * This,
            double x,
            double y,
            double z,
            short ElementID);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementShow )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementShow )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementSurfaceFill )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementSurfaceFill )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementSurfaceFlat )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementSurfaceFlat )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementLight )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementLight )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementLightingAmbient )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementLightingAmbient )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementLightingDiffuse )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementLightingDiffuse )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementLightingSpecular )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementLightingSpecular )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementMaterialAmbient )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementMaterialAmbient )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementMaterialDiffuse )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementMaterialDiffuse )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementMaterialSpecular )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementMaterialSpecular )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementMaterialShinines )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementMaterialShinines )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ElementMaterialEmission )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ElementMaterialEmission )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetLightCoordinates )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            float x,
            float y,
            float z);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CopyToClipboard )( 
            IGraph3D __RPC_FAR * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Lighting )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [retval][out] */ BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Lighting )( 
            IGraph3D __RPC_FAR * This,
            short ElementID,
            /* [in] */ BOOL newVal);
        
        END_INTERFACE
    } IGraph3DVtbl;

    interface IGraph3D
    {
        CONST_VTBL struct IGraph3DVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGraph3D_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IGraph3D_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IGraph3D_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IGraph3D_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IGraph3D_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IGraph3D_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IGraph3D_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IGraph3D_put_BackColor(This,clr)	\
    (This)->lpVtbl -> put_BackColor(This,clr)

#define IGraph3D_get_BackColor(This,pclr)	\
    (This)->lpVtbl -> get_BackColor(This,pclr)

#define IGraph3D_put_BorderStyle(This,style)	\
    (This)->lpVtbl -> put_BorderStyle(This,style)

#define IGraph3D_get_BorderStyle(This,pstyle)	\
    (This)->lpVtbl -> get_BorderStyle(This,pstyle)

#define IGraph3D_putref_Font(This,pFont)	\
    (This)->lpVtbl -> putref_Font(This,pFont)

#define IGraph3D_put_Font(This,pFont)	\
    (This)->lpVtbl -> put_Font(This,pFont)

#define IGraph3D_get_Font(This,ppFont)	\
    (This)->lpVtbl -> get_Font(This,ppFont)

#define IGraph3D_put_Caption(This,strCaption)	\
    (This)->lpVtbl -> put_Caption(This,strCaption)

#define IGraph3D_get_Caption(This,pstrCaption)	\
    (This)->lpVtbl -> get_Caption(This,pstrCaption)

#define IGraph3D_put_BorderVisible(This,vbool)	\
    (This)->lpVtbl -> put_BorderVisible(This,vbool)

#define IGraph3D_get_BorderVisible(This,pbool)	\
    (This)->lpVtbl -> get_BorderVisible(This,pbool)

#define IGraph3D_put_Appearance(This,appearance)	\
    (This)->lpVtbl -> put_Appearance(This,appearance)

#define IGraph3D_get_Appearance(This,pappearance)	\
    (This)->lpVtbl -> get_Appearance(This,pappearance)

#define IGraph3D_get_CaptionColor(This,pVal)	\
    (This)->lpVtbl -> get_CaptionColor(This,pVal)

#define IGraph3D_put_CaptionColor(This,newVal)	\
    (This)->lpVtbl -> put_CaptionColor(This,newVal)

#define IGraph3D_SetRange(This,xmin,xmax,ymin,ymax,zmin,zmax)	\
    (This)->lpVtbl -> SetRange(This,xmin,xmax,ymin,ymax,zmin,zmax)

#define IGraph3D_AutoRange(This)	\
    (This)->lpVtbl -> AutoRange(This)

#define IGraph3D_ShowPropertyPages(This)	\
    (This)->lpVtbl -> ShowPropertyPages(This)

#define IGraph3D_get_TrackMode(This,pVal)	\
    (This)->lpVtbl -> get_TrackMode(This,pVal)

#define IGraph3D_put_TrackMode(This,newVal)	\
    (This)->lpVtbl -> put_TrackMode(This,newVal)

#define IGraph3D_get_Projection(This,pVal)	\
    (This)->lpVtbl -> get_Projection(This,pVal)

#define IGraph3D_put_Projection(This,newVal)	\
    (This)->lpVtbl -> put_Projection(This,newVal)

#define IGraph3D_get_XLabel(This,pVal)	\
    (This)->lpVtbl -> get_XLabel(This,pVal)

#define IGraph3D_put_XLabel(This,newVal)	\
    (This)->lpVtbl -> put_XLabel(This,newVal)

#define IGraph3D_get_YLabel(This,pVal)	\
    (This)->lpVtbl -> get_YLabel(This,pVal)

#define IGraph3D_put_YLabel(This,newVal)	\
    (This)->lpVtbl -> put_YLabel(This,newVal)

#define IGraph3D_get_ZLabel(This,pVal)	\
    (This)->lpVtbl -> get_ZLabel(This,pVal)

#define IGraph3D_put_ZLabel(This,newVal)	\
    (This)->lpVtbl -> put_ZLabel(This,newVal)

#define IGraph3D_get_XGridNumber(This,pVal)	\
    (This)->lpVtbl -> get_XGridNumber(This,pVal)

#define IGraph3D_put_XGridNumber(This,newVal)	\
    (This)->lpVtbl -> put_XGridNumber(This,newVal)

#define IGraph3D_get_YGridNumber(This,pVal)	\
    (This)->lpVtbl -> get_YGridNumber(This,pVal)

#define IGraph3D_put_YGridNumber(This,newVal)	\
    (This)->lpVtbl -> put_YGridNumber(This,newVal)

#define IGraph3D_get_ZGridNumber(This,pVal)	\
    (This)->lpVtbl -> get_ZGridNumber(This,pVal)

#define IGraph3D_put_ZGridNumber(This,newVal)	\
    (This)->lpVtbl -> put_ZGridNumber(This,newVal)

#define IGraph3D_get_XGridColor(This,pVal)	\
    (This)->lpVtbl -> get_XGridColor(This,pVal)

#define IGraph3D_put_XGridColor(This,newVal)	\
    (This)->lpVtbl -> put_XGridColor(This,newVal)

#define IGraph3D_get_YGridColor(This,pVal)	\
    (This)->lpVtbl -> get_YGridColor(This,pVal)

#define IGraph3D_put_YGridColor(This,newVal)	\
    (This)->lpVtbl -> put_YGridColor(This,newVal)

#define IGraph3D_get_ZGridColor(This,pVal)	\
    (This)->lpVtbl -> get_ZGridColor(This,pVal)

#define IGraph3D_put_ZGridColor(This,newVal)	\
    (This)->lpVtbl -> put_ZGridColor(This,newVal)

#define IGraph3D_AddElement(This)	\
    (This)->lpVtbl -> AddElement(This)

#define IGraph3D_DeleteElement(This,ElementID)	\
    (This)->lpVtbl -> DeleteElement(This,ElementID)

#define IGraph3D_ClearGraph(This)	\
    (This)->lpVtbl -> ClearGraph(This)

#define IGraph3D_get_ElementLineColor(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementLineColor(This,ElementID,pVal)

#define IGraph3D_put_ElementLineColor(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementLineColor(This,ElementID,newVal)

#define IGraph3D_get_ElementPointColor(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementPointColor(This,ElementID,pVal)

#define IGraph3D_put_ElementPointColor(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementPointColor(This,ElementID,newVal)

#define IGraph3D_get_ElementLineWidth(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementLineWidth(This,ElementID,pVal)

#define IGraph3D_put_ElementLineWidth(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementLineWidth(This,ElementID,newVal)

#define IGraph3D_get_ElementPointSize(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementPointSize(This,ElementID,pVal)

#define IGraph3D_put_ElementPointSize(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementPointSize(This,ElementID,newVal)

#define IGraph3D_get_ElementType(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementType(This,ElementID,pVal)

#define IGraph3D_put_ElementType(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementType(This,ElementID,newVal)

#define IGraph3D_PlotXYZ(This,x,y,z,ElementID)	\
    (This)->lpVtbl -> PlotXYZ(This,x,y,z,ElementID)

#define IGraph3D_get_ElementShow(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementShow(This,ElementID,pVal)

#define IGraph3D_put_ElementShow(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementShow(This,ElementID,newVal)

#define IGraph3D_get_ElementSurfaceFill(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementSurfaceFill(This,ElementID,pVal)

#define IGraph3D_put_ElementSurfaceFill(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementSurfaceFill(This,ElementID,newVal)

#define IGraph3D_get_ElementSurfaceFlat(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementSurfaceFlat(This,ElementID,pVal)

#define IGraph3D_put_ElementSurfaceFlat(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementSurfaceFlat(This,ElementID,newVal)

#define IGraph3D_get_ElementLight(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementLight(This,ElementID,pVal)

#define IGraph3D_put_ElementLight(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementLight(This,ElementID,newVal)

#define IGraph3D_get_ElementLightingAmbient(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementLightingAmbient(This,ElementID,pVal)

#define IGraph3D_put_ElementLightingAmbient(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementLightingAmbient(This,ElementID,newVal)

#define IGraph3D_get_ElementLightingDiffuse(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementLightingDiffuse(This,ElementID,pVal)

#define IGraph3D_put_ElementLightingDiffuse(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementLightingDiffuse(This,ElementID,newVal)

#define IGraph3D_get_ElementLightingSpecular(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementLightingSpecular(This,ElementID,pVal)

#define IGraph3D_put_ElementLightingSpecular(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementLightingSpecular(This,ElementID,newVal)

#define IGraph3D_get_ElementMaterialAmbient(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementMaterialAmbient(This,ElementID,pVal)

#define IGraph3D_put_ElementMaterialAmbient(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementMaterialAmbient(This,ElementID,newVal)

#define IGraph3D_get_ElementMaterialDiffuse(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementMaterialDiffuse(This,ElementID,pVal)

#define IGraph3D_put_ElementMaterialDiffuse(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementMaterialDiffuse(This,ElementID,newVal)

#define IGraph3D_get_ElementMaterialSpecular(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementMaterialSpecular(This,ElementID,pVal)

#define IGraph3D_put_ElementMaterialSpecular(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementMaterialSpecular(This,ElementID,newVal)

#define IGraph3D_get_ElementMaterialShinines(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementMaterialShinines(This,ElementID,pVal)

#define IGraph3D_put_ElementMaterialShinines(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementMaterialShinines(This,ElementID,newVal)

#define IGraph3D_get_ElementMaterialEmission(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_ElementMaterialEmission(This,ElementID,pVal)

#define IGraph3D_put_ElementMaterialEmission(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_ElementMaterialEmission(This,ElementID,newVal)

#define IGraph3D_SetLightCoordinates(This,ElementID,x,y,z)	\
    (This)->lpVtbl -> SetLightCoordinates(This,ElementID,x,y,z)

#define IGraph3D_CopyToClipboard(This)	\
    (This)->lpVtbl -> CopyToClipboard(This)

#define IGraph3D_get_Lighting(This,ElementID,pVal)	\
    (This)->lpVtbl -> get_Lighting(This,ElementID,pVal)

#define IGraph3D_put_Lighting(This,ElementID,newVal)	\
    (This)->lpVtbl -> put_Lighting(This,ElementID,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_BackColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ OLE_COLOR clr);


void __RPC_STUB IGraph3D_put_BackColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_BackColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ OLE_COLOR __RPC_FAR *pclr);


void __RPC_STUB IGraph3D_get_BackColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_BorderStyle_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ long style);


void __RPC_STUB IGraph3D_put_BorderStyle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_BorderStyle_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pstyle);


void __RPC_STUB IGraph3D_get_BorderStyle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propputref] */ HRESULT STDMETHODCALLTYPE IGraph3D_putref_Font_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ IFontDisp __RPC_FAR *pFont);


void __RPC_STUB IGraph3D_putref_Font_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_Font_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ IFontDisp __RPC_FAR *pFont);


void __RPC_STUB IGraph3D_put_Font_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_Font_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ IFontDisp __RPC_FAR *__RPC_FAR *ppFont);


void __RPC_STUB IGraph3D_get_Font_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_Caption_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ BSTR strCaption);


void __RPC_STUB IGraph3D_put_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_Caption_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pstrCaption);


void __RPC_STUB IGraph3D_get_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_BorderVisible_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ VARIANT_BOOL vbool);


void __RPC_STUB IGraph3D_put_BorderVisible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_BorderVisible_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbool);


void __RPC_STUB IGraph3D_get_BorderVisible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_Appearance_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ short appearance);


void __RPC_STUB IGraph3D_put_Appearance_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_Appearance_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pappearance);


void __RPC_STUB IGraph3D_get_Appearance_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_CaptionColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_CaptionColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_CaptionColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ OLE_COLOR newVal);


void __RPC_STUB IGraph3D_put_CaptionColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_SetRange_Proxy( 
    IGraph3D __RPC_FAR * This,
    double xmin,
    double xmax,
    double ymin,
    double ymax,
    double zmin,
    double zmax);


void __RPC_STUB IGraph3D_SetRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_AutoRange_Proxy( 
    IGraph3D __RPC_FAR * This);


void __RPC_STUB IGraph3D_AutoRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_ShowPropertyPages_Proxy( 
    IGraph3D __RPC_FAR * This);


void __RPC_STUB IGraph3D_ShowPropertyPages_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_TrackMode_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_TrackMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_TrackMode_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_TrackMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_Projection_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_Projection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_Projection_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_Projection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_XLabel_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_XLabel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_XLabel_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IGraph3D_put_XLabel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_YLabel_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_YLabel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_YLabel_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IGraph3D_put_YLabel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ZLabel_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ZLabel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ZLabel_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IGraph3D_put_ZLabel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_XGridNumber_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_XGridNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_XGridNumber_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_XGridNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_YGridNumber_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_YGridNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_YGridNumber_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_YGridNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ZGridNumber_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ZGridNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ZGridNumber_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ZGridNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_XGridColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_XGridColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_XGridColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ OLE_COLOR newVal);


void __RPC_STUB IGraph3D_put_XGridColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_YGridColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_YGridColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_YGridColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ OLE_COLOR newVal);


void __RPC_STUB IGraph3D_put_YGridColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ZGridColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ZGridColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ZGridColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    /* [in] */ OLE_COLOR newVal);


void __RPC_STUB IGraph3D_put_ZGridColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_AddElement_Proxy( 
    IGraph3D __RPC_FAR * This);


void __RPC_STUB IGraph3D_AddElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_DeleteElement_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID);


void __RPC_STUB IGraph3D_DeleteElement_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_ClearGraph_Proxy( 
    IGraph3D __RPC_FAR * This);


void __RPC_STUB IGraph3D_ClearGraph_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementLineColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementLineColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementLineColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ OLE_COLOR newVal);


void __RPC_STUB IGraph3D_put_ElementLineColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementPointColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ OLE_COLOR __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementPointColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementPointColor_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ OLE_COLOR newVal);


void __RPC_STUB IGraph3D_put_ElementPointColor_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementLineWidth_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementLineWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementLineWidth_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ float newVal);


void __RPC_STUB IGraph3D_put_ElementLineWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementPointSize_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ float __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementPointSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementPointSize_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ float newVal);


void __RPC_STUB IGraph3D_put_ElementPointSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementType_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementType_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_PlotXYZ_Proxy( 
    IGraph3D __RPC_FAR * This,
    double x,
    double y,
    double z,
    short ElementID);


void __RPC_STUB IGraph3D_PlotXYZ_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementShow_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ BOOL __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementShow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementShow_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ BOOL newVal);


void __RPC_STUB IGraph3D_put_ElementShow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementSurfaceFill_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ BOOL __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementSurfaceFill_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementSurfaceFill_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ BOOL newVal);


void __RPC_STUB IGraph3D_put_ElementSurfaceFill_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementSurfaceFlat_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ BOOL __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementSurfaceFlat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementSurfaceFlat_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ BOOL newVal);


void __RPC_STUB IGraph3D_put_ElementSurfaceFlat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementLight_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ BOOL __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementLight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementLight_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ BOOL newVal);


void __RPC_STUB IGraph3D_put_ElementLight_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementLightingAmbient_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementLightingAmbient_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementLightingAmbient_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementLightingAmbient_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementLightingDiffuse_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementLightingDiffuse_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementLightingDiffuse_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementLightingDiffuse_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementLightingSpecular_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementLightingSpecular_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementLightingSpecular_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementLightingSpecular_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementMaterialAmbient_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementMaterialAmbient_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementMaterialAmbient_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementMaterialAmbient_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementMaterialDiffuse_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementMaterialDiffuse_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementMaterialDiffuse_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementMaterialDiffuse_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementMaterialSpecular_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementMaterialSpecular_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementMaterialSpecular_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementMaterialSpecular_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementMaterialShinines_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementMaterialShinines_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementMaterialShinines_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementMaterialShinines_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_ElementMaterialEmission_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_ElementMaterialEmission_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_ElementMaterialEmission_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ short newVal);


void __RPC_STUB IGraph3D_put_ElementMaterialEmission_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_SetLightCoordinates_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    float x,
    float y,
    float z);


void __RPC_STUB IGraph3D_SetLightCoordinates_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IGraph3D_CopyToClipboard_Proxy( 
    IGraph3D __RPC_FAR * This);


void __RPC_STUB IGraph3D_CopyToClipboard_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IGraph3D_get_Lighting_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [retval][out] */ BOOL __RPC_FAR *pVal);


void __RPC_STUB IGraph3D_get_Lighting_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IGraph3D_put_Lighting_Proxy( 
    IGraph3D __RPC_FAR * This,
    short ElementID,
    /* [in] */ BOOL newVal);


void __RPC_STUB IGraph3D_put_Lighting_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IGraph3D_INTERFACE_DEFINED__ */



#ifndef __NTGRAPH3DLib_LIBRARY_DEFINED__
#define __NTGRAPH3DLib_LIBRARY_DEFINED__

/* library NTGRAPH3DLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_NTGRAPH3DLib;

EXTERN_C const CLSID CLSID_NTGraph3D;

#ifdef __cplusplus

class DECLSPEC_UUID("DF3EB502-551C-4A90-B564-6681A6962F58")
NTGraph3D;
#endif
#endif /* __NTGRAPH3DLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
