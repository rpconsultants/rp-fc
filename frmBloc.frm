VERSION 5.00
Begin VB.Form frmBloc 
   AutoRedraw      =   -1  'True
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Position dans le bloc"
   ClientHeight    =   9480
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   13470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9480
   ScaleWidth      =   13470
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdInit_5 
      Height          =   405
      Left            =   60
      Picture         =   "frmBloc.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   74
      Top             =   1680
      Width           =   1350
   End
   Begin VB.Frame FrameDim 
      Caption         =   "Dimensions du bloc"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4785
      Left            =   60
      TabIndex        =   55
      Top             =   2160
      Width           =   3340
      Begin VB.PictureBox PB_3D 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   2580
         Left            =   60
         ScaleHeight     =   2580
         ScaleWidth      =   3225
         TabIndex        =   68
         Top             =   2130
         Width           =   3225
      End
      Begin VB.Label lblLong 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1420
         TabIndex        =   67
         Top             =   1850
         Width           =   1770
      End
      Begin VB.Label lblEp 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1420
         TabIndex        =   66
         Top             =   1550
         Width           =   1770
      End
      Begin VB.Label lblFBF 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1420
         TabIndex        =   65
         Top             =   1250
         Width           =   1770
      End
      Begin VB.Label lblFBA 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1420
         TabIndex        =   64
         Top             =   950
         Width           =   1770
      End
      Begin VB.Label lbl 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   1420
         TabIndex        =   63
         Top             =   650
         Width           =   1770
      End
      Begin VB.Label lbl 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   1420
         TabIndex        =   62
         Top             =   350
         Width           =   1770
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Saumon :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   61
         Top             =   650
         Width           =   1200
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Epaisseur :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   60
         Top             =   1550
         Width           =   1200
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Longueur :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   59
         Top             =   1850
         Width           =   1200
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fl�che BA :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   58
         Top             =   950
         Width           =   1200
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Fl�che BF :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   57
         Top             =   1250
         Width           =   1200
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Emplanture :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   56
         Top             =   350
         Width           =   1200
      End
   End
   Begin VB.CheckBox chkProp 
      Caption         =   "Marges BA et BF du saumon proportionnelles � celles de l'emplanture"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3510
      TabIndex        =   41
      Top             =   3855
      Width           =   6945
   End
   Begin VB.Frame FrameDepla 
      Caption         =   "D�placement"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1995
      Left            =   1440
      TabIndex        =   22
      Top             =   60
      Width           =   1950
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   4
         Left            =   1455
         Picture         =   "frmBloc.frx":0395
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   570
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   5
         Left            =   1455
         Picture         =   "frmBloc.frx":06ED
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   900
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   6
         Left            =   1455
         Picture         =   "frmBloc.frx":0A42
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   1230
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   12
         Left            =   135
         Picture         =   "frmBloc.frx":0D8F
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   570
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   11
         Left            =   135
         Picture         =   "frmBloc.frx":10E5
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   900
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   10
         Left            =   135
         Picture         =   "frmBloc.frx":143B
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   1230
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   7
         Left            =   1125
         Picture         =   "frmBloc.frx":1788
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   1560
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   8
         Left            =   795
         Picture         =   "frmBloc.frx":1AD5
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   1560
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   3
         Left            =   1125
         Picture         =   "frmBloc.frx":1E2B
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   240
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   2
         Left            =   795
         Picture         =   "frmBloc.frx":2178
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   240
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   9
         Left            =   465
         Picture         =   "frmBloc.frx":24CB
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   1560
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   1
         Left            =   465
         Picture         =   "frmBloc.frx":2825
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   240
         Width           =   330
      End
   End
   Begin VB.Frame FrameEp 
      Caption         =   "Epaisseur"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   60
      TabIndex        =   20
      Top             =   60
      Width           =   1350
      Begin VB.CommandButton cmdEp 
         Height          =   270
         Index           =   6
         Left            =   840
         Picture         =   "frmBloc.frx":2B80
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   1155
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   300
         Index           =   5
         Left            =   555
         Picture         =   "frmBloc.frx":2ECD
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   1155
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   270
         Index           =   3
         Left            =   840
         Picture         =   "frmBloc.frx":3223
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   885
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   300
         Index           =   2
         Left            =   555
         Picture         =   "frmBloc.frx":3570
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   855
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   330
         Index           =   4
         Left            =   270
         Picture         =   "frmBloc.frx":38C3
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   1155
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   330
         Index           =   1
         Left            =   270
         Picture         =   "frmBloc.frx":3C1D
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   825
         Width           =   280
      End
      Begin VB.TextBox TxtEpaisseur 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   255
         TabIndex        =   21
         Top             =   360
         Width           =   840
      End
   End
   Begin VB.Frame Frame_Bloc 
      Caption         =   "Position de la d�coupe au saumon (mm)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3750
      Index           =   2
      Left            =   3420
      TabIndex        =   9
      Top             =   4185
      Width           =   9885
      Begin VB.CommandButton cmdMBA 
         Height          =   270
         Index           =   8
         Left            =   2820
         Picture         =   "frmBloc.frx":3F78
         Style           =   1  'Graphical
         TabIndex        =   73
         Top             =   3390
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   300
         Index           =   7
         Left            =   2535
         Picture         =   "frmBloc.frx":42C7
         Style           =   1  'Graphical
         TabIndex        =   72
         Top             =   3390
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   270
         Index           =   6
         Left            =   2820
         Picture         =   "frmBloc.frx":461D
         Style           =   1  'Graphical
         TabIndex        =   71
         Top             =   3120
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   300
         Index           =   5
         Left            =   2535
         Picture         =   "frmBloc.frx":496E
         Style           =   1  'Graphical
         TabIndex        =   70
         Top             =   3090
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   270
         Index           =   8
         Left            =   7755
         Picture         =   "frmBloc.frx":4CC5
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   3360
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   300
         Index           =   7
         Left            =   7470
         Picture         =   "frmBloc.frx":5016
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   3360
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   270
         Index           =   6
         Left            =   7755
         Picture         =   "frmBloc.frx":536C
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   3090
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   300
         Index           =   5
         Left            =   7470
         Picture         =   "frmBloc.frx":56BD
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   3060
         Width           =   280
      End
      Begin VB.CheckBox Check 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Index           =   2
         Left            =   8685
         Picture         =   "frmBloc.frx":5A14
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   3075
         Width           =   1080
      End
      Begin VB.TextBox TextMDessus 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   7860
         TabIndex        =   17
         Top             =   405
         Width           =   825
      End
      Begin VB.TextBox TextMDessous 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   7860
         TabIndex        =   16
         Top             =   2070
         Width           =   825
      End
      Begin VB.TextBox TextBFH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   8925
         TabIndex        =   15
         Top             =   750
         Width           =   825
      End
      Begin VB.TextBox TextBFB 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   8925
         TabIndex        =   14
         Top             =   1710
         Width           =   825
      End
      Begin VB.TextBox TextMargeBA 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   1500
         TabIndex        =   13
         Top             =   3300
         Width           =   825
      End
      Begin VB.TextBox TextMargeBF 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   6435
         TabIndex        =   12
         Top             =   3300
         Width           =   825
      End
      Begin VB.TextBox TextBAB 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   135
         TabIndex        =   11
         Top             =   1650
         Width           =   825
      End
      Begin VB.TextBox TextBAH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   135
         TabIndex        =   10
         Top             =   630
         Width           =   825
      End
      Begin VB.PictureBox PB_Bloc 
         AutoRedraw      =   -1  'True
         Enabled         =   0   'False
         Height          =   3405
         Index           =   2
         Left            =   60
         ScaleHeight     =   3345
         ScaleWidth      =   9690
         TabIndex        =   69
         Top             =   270
         Width           =   9750
      End
   End
   Begin VB.Frame Frame_Bloc 
      Caption         =   "Position de la d�coupe � l'emplanture (mm)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3750
      Index           =   1
      Left            =   3480
      TabIndex        =   0
      Top             =   0
      Width           =   9885
      Begin VB.CommandButton cmdMBA 
         Height          =   270
         Index           =   4
         Left            =   2805
         Picture         =   "frmBloc.frx":5E21
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   3375
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   300
         Index           =   3
         Left            =   2520
         Picture         =   "frmBloc.frx":6170
         Style           =   1  'Graphical
         TabIndex        =   49
         Top             =   3375
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   270
         Index           =   2
         Left            =   2805
         Picture         =   "frmBloc.frx":64C6
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   3105
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   300
         Index           =   1
         Left            =   2520
         Picture         =   "frmBloc.frx":6817
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   3075
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   300
         Index           =   1
         Left            =   7455
         Picture         =   "frmBloc.frx":6B6E
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   3060
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   270
         Index           =   2
         Left            =   7740
         Picture         =   "frmBloc.frx":6EC5
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   3090
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   300
         Index           =   3
         Left            =   7455
         Picture         =   "frmBloc.frx":7216
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   3360
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   270
         Index           =   4
         Left            =   7740
         Picture         =   "frmBloc.frx":756C
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   3360
         Width           =   280
      End
      Begin VB.CheckBox Check 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Index           =   1
         Left            =   8685
         Picture         =   "frmBloc.frx":78BD
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   3075
         Width           =   1080
      End
      Begin VB.TextBox TextBAH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   135
         TabIndex        =   8
         Top             =   630
         Width           =   825
      End
      Begin VB.TextBox TextBAB 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   135
         TabIndex        =   7
         Top             =   1650
         Width           =   825
      End
      Begin VB.TextBox TextMargeBF 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   6435
         TabIndex        =   2
         Top             =   3300
         Width           =   825
      End
      Begin VB.TextBox TextMargeBA 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   1500
         TabIndex        =   1
         Top             =   3285
         Width           =   825
      End
      Begin VB.TextBox TextBFB 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   8925
         TabIndex        =   5
         Top             =   1710
         Width           =   825
      End
      Begin VB.TextBox TextBFH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   8925
         TabIndex        =   6
         Top             =   750
         Width           =   825
      End
      Begin VB.TextBox TextMDessous 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   7860
         TabIndex        =   4
         Top             =   2070
         Width           =   825
      End
      Begin VB.TextBox TextMDessus 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   7860
         TabIndex        =   3
         Top             =   405
         Width           =   825
      End
      Begin VB.PictureBox PB_Bloc 
         AutoRedraw      =   -1  'True
         Enabled         =   0   'False
         Height          =   3405
         Index           =   1
         Left            =   120
         ScaleHeight     =   3345
         ScaleWidth      =   9690
         TabIndex        =   46
         Top             =   240
         Width           =   9750
      End
   End
End
Attribute VB_Name = "frmBloc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

