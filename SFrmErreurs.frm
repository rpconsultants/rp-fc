VERSION 5.00
Begin VB.Form SFrmErreurs 
   Caption         =   "Erreurs de trajectoire"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   Tag             =   "1"
   Begin VB.TextBox TB_Erreur 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3135
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Tag             =   "2"
      Text            =   "SFrmErreurs.frx":0000
      ToolTipText     =   "0"
      Top             =   0
      Width           =   4695
   End
End
Attribute VB_Name = "SFrmErreurs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Set gFenetreHID = MDIFrmDecoupe
    ChargeTrad Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Cancel = False
    Select Case UnloadMode
        Case vbFormControlMenu
            Me.WindowState = vbMinimized
            Cancel = True
    End Select
End Sub

Private Sub Form_Resize()
    TB_Erreur.Move 0, 0, Me.ScaleWidth, Me.ScaleHeight
End Sub
