Attribute VB_Name = "Module_Langue"
Option Explicit

Private Declare Function GetSystemDefaultLangID Lib "kernel32" () As Long

Function TrouverLangue() As String
    'Trouver la langue de la machine
    Dim Machaine As String
    
    Machaine = Right(Hex(GetSystemDefaultLangID()), 4)
    Select Case Machaine
        Case "0403"
            TrouverLangue = "ca"
        Case "0407"
            TrouverLangue = "de"
        Case "040A"
            TrouverLangue = "es"
        Case "040C", "080C"
            TrouverLangue = "fr"
        Case "0410"
            TrouverLangue = "it"
        Case Else
            TrouverLangue = "en"
    End Select
End Function


