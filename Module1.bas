Attribute VB_Name = "Module1"
Option Explicit

'Constantes IPL5X
Global Const FastStop = &H0
Global Const SlowStop = &H1
Global Const ResumeStop = &H2
Global Const StatusStop = &H3

Global Const FdcOn = &H0
Global Const FdcOff = &H1
Global Const FdcRead = &H2

Global Const EMPLANTURE = 1
Global Const SAUMON = 2
Global Const TOUT = 3

Global Const E = 1              'Indices pour les donn�es zoom de chaque fen�tre
Global Const s = 2
Global Const f = 3
Global Const D = 4
Global Const IM = 5

Global Const GAUCHE = 0
Global Const DROITE = 1

Global gLangue As String        'Langue utilis�e pour les traductions
Global gMaxMRU As Long          'Compte le nombre de MRUs m�moris�s

Global gTabFlash() As Byte      'Pour �crire dans la Flash
Global gFlash    As Byte        'Mode Flash

Global Const Marge = 180
 
Public fMainForm As frmMain
Public gbModif As Boolean
Public gProjet As String
Public gPause As Boolean        'Arr�t des d�placements IPL5x
Global gTouches As Boolean          'Utilisation des touches IPL5X
Global Fichier As String
Global bAfficheNum As Boolean
Global bProfil(EMPLANTURE To SAUMON) As Boolean
Global bProfilCharge As Boolean
Global gSegment As Long         'N� du segment pour stocker dans le tableau
Global DeplEnCours As Boolean
Global bSaveFC As Boolean
Global bDessinDEcoupe As Boolean

Global wDelta_Gauche As Single
Global wDelta_Droit As Single
Global wOption_GD As Integer

Global gbOrientationInv As Boolean      'Orientation de la table invers�e

Global bMemoBloc As Boolean             'M�morisation des dimensions du bloc
Global gwLargeurEmplanture As Single
Global gwLargeurSaumon     As Single
Global gwFlecheBA          As Single
Global gwFlecheBF          As Single
Global gwEpaisseur         As Single
Global gwLongueur          As Single

Global maClasseHID As ClsCdeIPL5X

Global allowedKeys As String
Global SepDecimal As String

' vendor et product IDs
Global Const VendorID = &H4D8             ' VendorID de l'interface PIC18F4550
Global Const ProductID = &HAA             ' ProductID de l'interface PIC18F4550
Global gFenetreHID As Form
Global gstrTable As String
Global gstrMat As String
Global gOptionLPT As Integer        'Choix du port (parall�le ou USB)
Public gExtension As String
Public iDessin As Long              'Pour l'affichage de la d�coupe en cours
Global iSegmentTraite As Long
Global gbInit As Boolean

Type Col_Sep
    red As Integer
    green As Integer
    blue As Integer
End Type

Public Type Point
    x As Single                   'Abcisse
    y As Single                   'Ordonn�e
End Type

Public Type TypeNoeud
    x As Single             'Abcisse du point
    y As Single             'Ordonn�e du point
    bTemp As Boolean        'Ce point est-il temporaire (utilis� pour la synchro)
    bSynchro As Boolean     'Ce point est-il un point de synchro (utilis� pour la synchro)
    Dist As Single
    Vitesse As Single       'Vitesse de ce segment (calcul�)
    Decalage As Single      'D�calage de ce segment (calcul�)
    iOrigine As Long        'Indice du tableau de coordonn�es qui a g�n�r� ce noeud
End Type

Public Type TypeDecal
    XOrigEmplanture As Single               'Abcisse origine emplanture
    YOrigEmplanture As Single               'Ordonn�e origine emplanture
    XOrigSaumon As Single                   'Abcisse origine saumon
    YOrigSaumon As Single                   'Ordonn�e origine saumon
    XDecalEmplanture As Single              'Abcisse d�cal�e emplanture
    YDecalEmplanture As Single              'Ordonn�e d�cal�e emplanture
    XDecalSaumon As Single                  'Abcisse d�cal�e saumon
    YDecalSaumon As Single                  'Ordonn�e d�cal�e saumon
    Vitesse_Temps As Single                 'Vitesse du segment le plus �lev� (aller)
                                            'Temps de parcours en s (retour)
End Type

Global Pmin(EMPLANTURE To TOUT) As Point, Pmax(EMPLANTURE To TOUT) As Point

Type PointPLT
    x As Single                 'Abcisse
    y As Single                 'Ordonn�e
    Cde As String * 2           'Commande (PU ou PD)
    numSequence As String * 2   'N� de s�quence de la d�coupe
End Type

Private Type PointI
    x As Single                   'Abcisse
    y As Single                   'Ordonn�e
    i As Long                     'Indice du point de gTabCoord apr�s lequel doit se trouver ce point
End Type

Type PointDecoupe
    P(EMPLANTURE To SAUMON) As Point
End Type

Type ElemDecoupe
    P_Decal(EMPLANTURE To SAUMON) As Point  'Points d�cal�s
    P_abs(EMPLANTURE To SAUMON) As Point    'Points chariots
    X_pas(EMPLANTURE To SAUMON) As Long
    Y_pas(EMPLANTURE To SAUMON) As Long
    Temps As Single
    Nbr_Pulse As Long
    iOrigine As Long
End Type

Global monTableau() As PointPLT
Global monTableau1() As Point
Global Pointsplt() As PointPLT    'Emplanture
Global PointsPLTDecal(EMPLANTURE To SAUMON, 0 To 0) As PointPLT
Global gTabCoord() As PointDecoupe
Global TabSynchro() As Boolean
Global nbPoints(EMPLANTURE To SAUMON) As Long
Global monTableauLecture() As PointPLT
Global Couleur(EMPLANTURE To SAUMON) As Long
Global CouleurFleche As Long
Global TabSynchroPt() As PointI
Global gListePointSynchro(EMPLANTURE To SAUMON) As ClsListeChainee
Global bMiroirHorizontal(EMPLANTURE To SAUMON) As Boolean
Global bMiroirVertical(EMPLANTURE To SAUMON) As Boolean
Global wCorde(EMPLANTURE To SAUMON) As Single
Global wdimMult(EMPLANTURE To SAUMON) As Single
Global wH(EMPLANTURE To SAUMON) As Single
Global wFleche_BA As Single
Global wFleche_BF As Single
Global wMargeBA(EMPLANTURE To SAUMON) As Single
Global wMargeBF(EMPLANTURE To SAUMON) As Single
'Global mDessous As Single
Global MDessus(EMPLANTURE To SAUMON) As Single
Global wDessous(EMPLANTURE To SAUMON) As Single
Global wFichier(EMPLANTURE To SAUMON) As String

Global Tab_Chariot() As ElemDecoupe
Global ListeDecoupe(EMPLANTURE To SAUMON) As ClsListeChainee
Global tabBloc(EMPLANTURE To SAUMON, 0 To 4) As Point   'Coordonn�es du bloc (E et S)
Global tabBlocInv(EMPLANTURE To SAUMON, 0 To 4) As Point   'Coordonn�es du bloc (E et S) en miroir

'Param�tres de d�coupe
Global wOffset_Gauche As Single
Global wOffset_Droit As Single
Global wVitesse As Double
Global wOffset_X As Single
Global wOffset_Y As Single
Global wRayonCompense As Boolean

'Dimensions du bloc
Global wLongueurBloc As Single                  'Z
Global wEpaisseur As Single                     'Y
Global wLargeur(EMPLANTURE To SAUMON) As Single 'X
Global wAngle_Atteint(EMPLANTURE To SAUMON) As Single 'Vrillage
Global wEnvergure As Single
'Global wDecalage As Single
Global wLargeurTable As Single
Global wComm As String

Global PI As Double 'PI est calcul� au moment du d�marrage du programme

Declare Function LireFichier Lib "CNCTools.dll" (Tableau() As PointPLT, NomFic As String, _
    xMin As Single, xMax As Single, Ymin As Single, Ymax As Single, Comment As String) As Long
Declare Function EcrireFichier Lib "CNCTools.dll" (Tableau() As PointPLT, NomFic As String, Comment As String) As Long
Declare Function LissageSequence Lib "CNCTools.dll" (Tableau() As PointPLT) As Long
Declare Function NettoyageFichier Lib "CNCTools.dll" (Tableau() As PointPLT, EPSILON As Single) _
    As Long 'Renvoie le nombre de points
Declare Function NettoyageSynchro Lib "CNCTools.dll" (Tableau() As Point, EPSILON As Single) As Long
Declare Function DecalageV4 Lib "CNCTools.dll" (Tableau() As TypeDecal, Sens As Integer, _
    VitesseMatiere As Single, DVH As Single, SH1 As Single, SH2 As Single) As Long
Declare Function Version Lib "CNCTools.dll" () As Long

Declare Function SetParent Lib "user32" _
  (ByVal hWndChild As Long, _
   ByVal hWndNewParent As Long) As Long

Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Declare Function SetCursorPos Lib "user32" ( _
   ByVal x As Long, ByVal y As Long) As Long
   
Declare Function GetTickCount Lib "kernel32" () As Long

Declare Function GetSystemMetrics Lib "user32" _
(ByVal nIndex As Long) As Long

Private Declare Function VarPtrArray Lib "msvbvm60.dll" Alias "VarPtr" (Ptr() As Any) As Long

Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" ( _
    ByVal lpAppName As String, _
    ByVal lpReturnedString As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) _
    As Long

Declare Function GetPrivateProfileSectionNames Lib "kernel32.dll" Alias "GetPrivateProfileSectionNamesA" ( _
    ByVal lpszReturnBuffer As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) _
    As Long

Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
    ByVal AppName$, _
    ByVal KeyName$, _
    ByVal keydefault$, _
    ByVal FileName$)

Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
    ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, _
    ByVal lpDefault As String, _
    ByVal lpReturnedString As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) As Long

Private Type OPENFILENAME
    lStructSize As Long
    hwndOwner As Long
    hInstance As Long
    lpstrFilter As String
    lpstrCustomFilter As String
    nMaxCustFilter As Long
    nFilterIndex As Long
    lpstrFile As String
    nMaxFile As Long
    lpstrFileTitle As String
    nMaxFileTitle As Long
    lpstrInitialDir As String
    lpstrTitle As String
    flags As Long
    nFileOffset As Integer
    nFileExtension As Integer
    lpstrDefExt As String
    lCustData As Long
    lpfnHook As Long
    lpTemplateName As String
End Type

Private Enum OFNFlagsEnum
    OFN_ALLOWMULTISELECT = &H200
    OFN_CREATEPROMPT = &H2000
    OFN_ENABLEHOOK = &H20
    OFN_ENABLETEMPLATE = &H40
    OFN_ENABLETEMPLATEHANDLE = &H80
    OFN_EXPLORER = &H80000
    OFN_EXTENSIONDIFFERENT = &H400
    OFN_FILEMUSTEXIST = &H1000
    OFN_HIDEREADONLY = &H4
    OFN_LONGNAMES = &H200000
    OFN_NOCHANGEDIR = &H8
    OFN_NODEREFERENCELINKS = &H100000
    OFN_NOLONGNAMES = &H40000
    OFN_NONETWORKBUTTON = &H20000
    OFN_NOREADONLYRETURN = &H8000
    OFN_NOTESTFILECREATE = &H10000
    OFN_NOVALIDATE = &H100
    OFN_OVERWRITEPROMPT = &H2
    OFN_PATHMUSTEXIST = &H800
    OFN_READONLY = &H1
    OFN_SHAREAWARE = &H4000
    OFN_SHAREFALLTHROUGH = 2
    OFN_SHARENOWARN = 1
    OFN_SHAREWARN = 0
    OFN_SHOWHELP = &H10
End Enum

Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal _
 hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cX As _
Long, ByVal cY As Long, ByVal wFlags As Long) As Long

Public Const CSIDL_PERSONAL As Long = &H5
Const CSIDL_DESKTOPDIRECTORY = &H10
Public Const MAX_PATH = 256&
Const NOERROR = 0

Private Declare Function GetOpenFileName Lib "comdlg32.dll" Alias _
"GetOpenFileNameA" (pOpenfilename As OPENFILENAME) As Long

Private Declare Function GetSaveFileName Lib "comdlg32.dll" Alias _
    "GetSaveFileNameA" (pOpenfilename As OPENFILENAME) As Long

Private Declare Function SHGetSpecialFolderLocation _
    Lib "shell32" (ByVal hWnd As Long, _
    ByVal nFolder As Long, ppidl As Long) As Long

Private Declare Function SHGetPathFromIDList _
    Lib "shell32" Alias "SHGetPathFromIDListA" _
    (ByVal Pidl As Long, ByVal pszPath As String) As Long

Private Declare Sub CoTaskMemFree Lib "ole32" (ByVal pvoid As Long)

Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Const HKEY_CLASSES_ROOT = &H80000000
Const HKEY_LOCAL_MACHINE = &H80000002
Declare Function RegCreateKey Lib "advapi32.dll" Alias "RegCreateKeyA" (ByVal hKey&, ByVal lpszSubKey$, lphKey&) As Long
Declare Function RegSetValue Lib "advapi32.dll" Alias "RegSetValueA" (ByVal hKey&, ByVal lpszSubKey$, ByVal fdwType&, ByVal lpszValue$, ByVal dwLength&) As Long
Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long

Public Const REG_SZ = 1

Sub Main()
    Dim MonFichier As String
    Dim monEnreg As String
    Dim NumVersion() As String
    
    'On va chercher la langue
    gLangue = ReadINI("Language", TrouverLangue)
    
    'S'il existe d�j� une instance de l'application, on affiche un message
    'd'erreur et le programme se termine
    If App.PrevInstance Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 31), vbExclamation, "Erreur"
        Exit Sub
    End If
    
    'Test de connexion Internet
    If Internet_Connected Then
        monEnreg = OuvrirURL("http://82.228.30.185/5XProject/files/RP-FC/ver.txt")
        If monEnreg <> "" And InStr(monEnreg, "not found") = 0 And InStr(monEnreg, "RP-FC") <> 0 Then
            NumVersion = Split(monEnreg, ".")
            If NumVersion(0) > App.Major Or NumVersion(1) > App.Minor Or NumVersion(2) > App.Revision Then
                If MsgBox(ReadINI_Trad(gLangue, "erreurs", 113), vbExclamation + vbYesNo) = vbYes Then
                    Shell "explorer http://5xproject.dyndns.org/", vbNormalFocus
                End If
            End If
        End If
    End If
    
    gbInit = False  'A t'on demand� la r�initialisation ?
    
    PI = Atn(1) * 4
    SepDecimal = Format$(0, ".")
    allowedKeys = "0123456789-.," & Chr(8)
    ReDim Pointsplt(EMPLANTURE To SAUMON, 0 To 0)
    'Lecture table par d�faut
    gstrTable = ReadINI("TableDefaut", "MM2001")
    wLargeurTable = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_23", 600)
    wDelta_Gauche = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_24", 0#)
    wDelta_Droit = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_25", 0#)
    gbOrientationInv = (ReadINI_Machine("table_" & gstrTable, "CB_SAV_13", 0) = "1")
    
    'Lecture de la valeur "CouleurEmplanture" : Couleur du trac� de l'emplanture
    Couleur(EMPLANTURE) = ReadINI_Num("CouleurEmplanture", RGB(255, 0, 0))
    'Lecture de la valeur "CouleurSaumon" : Couleur du trac� du saumon
    Couleur(SAUMON) = ReadINI_Num("CouleurSaumon", RGB(0, 0, 255))
    'Lecture de la valeur "CouleurFleche" : Couleur des fl�ches de sens
    CouleurFleche = ReadINI_Num("CouleurFleche", RGB(127, 0, 127))
    
    'Lecture des valeurs derni�rement utilis�es
    wOffset_Gauche = ReadINI_Num("Offset_Gauche", 200)
    wOffset_Droit = ReadINI_Num("Offset_Droit", 300)
    wOffset_X = ReadINI_Num("Offset_X", 20)
    wOffset_Y = ReadINI_Num("Offset_Y", 0)
    wVitesse = Min(Calcul_Fil_VitMax(gstrMat), CSng(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 0)))
    wVitesse = Min(wVitesse, ReadINI_Num("Vitesse", 0.5))
    wVitesse = Round(wVitesse, 1)
    wRayonCompense = ReadINI_Num("RayonCompense", -1)
    wOption_GD = ReadINI_Num("Option_GD", GAUCHE)
    wEpaisseur = ReadINI_Num("Epaisseur", 50#)
    wLongueurBloc = ReadINI_Num("LongueurBloc", 600#)
    
    bMemoBloc = False           'M�morisation des dimensions du bloc
    
    'Recherche si le RP-FC.ini existe (1�re utilisation)
    If Dir(App.Path & "\" & App.Title & ".ini") = "" Then
        'Le .ini n'existe pas, on va le cr�er
        Call CreatINI
    End If
    Call CreatAssoc("RP-FC", ".fc", ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 310), "IconeRPFC.ico")
    If Version < 62 Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 70), vbCritical
    End If

    If Len(Dir(App.Path & "\vcredist_x86.exe")) <> 0 Then
        ShellWait ("vcredist_x86.exe")
        Kill App.Path & "\vcredist_x86.exe"
    End If
    bSaveFC = False
    
    frmMain.Show
    If Command$ <> "" Then  'On r�cup�re le nom du fichier en ligne de commande
        MonFichier = Replace(Command$, """", "")
        If UCase$(GetExtension(MonFichier)) <> "FC" Then
            Exit Sub
        End If
        Call UnixToDos(MonFichier)  'Conversion 0A -> 0D0A
        Open MonFichier For Input As #1
        Input #1, monEnreg
            If UCase$(Left(monEnreg, 6)) <> "ECORDE" Then
                Close #1
                Exit Sub
            End If
        Close #1
        frmMain.OuvrirFC Replace(Command$, """", "")
    End If
End Sub

Sub ChargeTrad(Frm As Form)
    On Error Resume Next

    Dim ctl As Control
    Dim Obj As Object
    Dim fnt As Object
    Dim sCtlType As String
    Dim maLangue As String
    Dim i As Integer
    Dim maTrad As String

    'On va chercher dans le fichier INI en fonction de la langue :
    ' Code ISO 639-1
    ' fr : Fran�ais
    ' en : Anglais
    ' de : Allemand
    ' es : Espagnol
    ' it : Italien
    ' pt : Portugais
    'etc...
    
    'On va chercher la langue
    maLangue = gLangue

    'D�finit la l�gende de la feuille
    Frm.Caption = ReadINI_Trad(maLangue, Frm.Name, Frm.Tag)

    'D�finit les l�gendes des contr�les en utilisant la
    'propri�t� caption pour les �l�ments de menu et la propri�t� Tag
    'pour tous les autres contr�les
    For Each ctl In Frm.Controls
        If ctl.Tag <> "0" Or ctl.ToolTipText <> "0" Then
            sCtlType = TypeName(ctl)
            If sCtlType = "Label" Then
                ctl.Caption = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag)
                If ctl.ToolTipText = "0" Then
                    ctl.ToolTipText = ""
                Else
                    ctl.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, ctl.ToolTipText)
                End If
            ElseIf sCtlType = "Menu" Then
                ctl.Caption = ReadINI_Trad(maLangue, Frm.Name, ctl.Caption)
            ElseIf sCtlType = "TabStrip" Then
                For Each Obj In ctl.Tabs
                    If Obj.Tag <> "0" Then
                        Obj.Caption = ReadINI_Trad(maLangue, Frm.Name, Obj.Tag)
                    End If
                    If maTrad <> "___" Then
                        Obj.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, "tt" & Obj.ToolTipText)
                    End If
                Next
            ElseIf sCtlType = "SSTab" Then
                'Pour les TabCaption
                For i = 0 To ctl.Tabs - 1
                    ctl.TabCaption(i) = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                Next i
            ElseIf sCtlType = "Toolbar" Then
                For Each Obj In ctl.buttons
                    maTrad = ReadINI_Trad(maLangue, Frm.Name, Obj.ToolTipText)
                    If maTrad <> "___" Then
                        Obj.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, "tt" & Obj.ToolTipText)
                    End If
                Next
            ElseIf sCtlType = "StatusBar" Then
                For i = 1 To ctl.Panels.count
                    ctl.Panels(i) = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                Next i
            ElseIf sCtlType = "ListView" Then
                For Each Obj In ctl.ColumnHeaders
                    Obj.Text = ReadINI_Trad(maLangue, Frm.Name, Obj.Tag)
                Next
            ElseIf sCtlType = "ListBox" Then
                If ctl.Tag <> "0" Then
                    i = 1
                    maTrad = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                    While maTrad <> ctl.Tag & "_" & i
                        ctl.AddItem maTrad
                        i = i + 1
                        maTrad = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                    Wend
                End If
            ElseIf sCtlType = "ComboBox" Then
                If ctl.Tag <> "0" Then
                    i = 1
                    maTrad = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                    While maTrad <> ctl.Tag & "_" & i
                        ctl.AddItem maTrad
                        i = i + 1
                        maTrad = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                    Wend
                End If
                If ctl.ToolTipText = "0" Then
                    ctl.ToolTipText = ""
                Else
                    ctl.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, ctl.ToolTipText)
                End If
            Else
                If ctl.Tag <> "0" Then
                    ctl.Caption = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag)
                End If
                If ctl.ToolTipText = "0" Then
                    ctl.ToolTipText = ""
                Else
                    ctl.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, ctl.ToolTipText)
                End If
            End If
        End If
        If ctl.ToolTipText = "0" Then
            ctl.ToolTipText = ""
        End If
    Next
End Sub

Public Function GetPathName(ByVal strPath) As String

    Dim intLenPath As Integer
    Dim intSlash As Integer
     
    On Error GoTo GetPathName_Err
     
    intLenPath = Len(strPath)
     
    If intLenPath > 0 Then
        strPath = StrReverse(strPath)
        intSlash = InStr(1, strPath, "\", vbTextCompare)
        If intSlash > 0 Then
            strPath = Right$(strPath, intLenPath - (intSlash - 1))
            strPath = StrReverse(strPath)
            GetPathName = strPath
        ElseIf intSlash = 0 Then
            'No slash found, may not be a valid path name.
            GetPathName = vbNullString
        End If
    ElseIf intLenPath = 0 Then
        GetPathName = vbNullString
    End If
     
GetPathName_Exit:
    Exit Function
     
GetPathName_Err:
    GetPathName = vbNullString
    Resume GetPathName_Exit
End Function

Public Function GetFileName(ByVal strFileName) As String

    Dim intLen As Integer
    Dim intSlash As Integer
     
    On Error GoTo GetFileName_Err
     
    intLen = Len(strFileName)
     
    If intLen > 0 Then
        strFileName = StrReverse(strFileName)
        intSlash = InStr(1, strFileName, "\", vbTextCompare)
        If intSlash > 0 Then
            strFileName = Left$(strFileName, intSlash - 1)
            strFileName = StrReverse(strFileName)
            GetFileName = strFileName
        ElseIf intSlash = 0 Then
            'No slash found, may not be a valid path name.
            GetFileName = StrReverse(strFileName)
        End If
    ElseIf intLen = 0 Then
        GetFileName = vbNullString
    End If
     
GetFileName_Exit:
    Exit Function
     
GetFileName_Err:
    GetFileName = vbNullString
    Resume GetFileName_Exit
End Function

Public Function GetFileNameWithoutExtension(ByVal strFileName) As String

    Dim intLen As Integer
    Dim intPoint As Integer
     
    On Error GoTo GetFileNamewithoutextension_Err
    
    strFileName = GetFileName(strFileName)
     
    intLen = Len(strFileName)
     
    If intLen > 0 Then
        strFileName = StrReverse(strFileName)
        intPoint = InStr(1, strFileName, ".", vbTextCompare)
        If intPoint > 0 Then
            strFileName = Right$(strFileName, intLen - intPoint)
            strFileName = StrReverse(strFileName)
            GetFileNameWithoutExtension = strFileName
        ElseIf intPoint = 0 Then
            'No slash found, may not be a valid path name.
            GetFileNameWithoutExtension = vbNullString
        End If
    ElseIf intLen = 0 Then
        GetFileNameWithoutExtension = vbNullString
    End If
     
GetFileNameWithoutExtension_Exit:
    Exit Function
     
GetFileNamewithoutextension_Err:
    GetFileNameWithoutExtension = vbNullString
    Resume GetFileNameWithoutExtension_Exit
End Function

Function RechercheFichier(strform As Form, InitialDir As String, sFilter As String, _
        Optional bSave As Boolean = False) As String
    Dim OpenFile As OPENFILENAME
    Dim lReturn As Long
    Dim stBuf As String, lgBuf As Long, lgRep As Long
    
    OpenFile.lStructSize = Len(OpenFile)
    OpenFile.hwndOwner = strform.hWnd
    
    OpenFile.lpstrFilter = sFilter
    OpenFile.nFilterIndex = 1
    OpenFile.lpstrFile = String(257, 0)
    OpenFile.nMaxFile = Len(OpenFile.lpstrFile) - 1
    OpenFile.lpstrFileTitle = OpenFile.lpstrFile
    OpenFile.nMaxFileTitle = OpenFile.nMaxFile
    If Len(Trim(InitialDir)) = 0 Then
        OpenFile.lpstrInitialDir = SpecFolder(CSIDL_DESKTOPDIRECTORY) & "\"
    Else
        OpenFile.lpstrInitialDir = InitialDir
        'If bSave Then OpenFile.lpstrFile = GetFileName(InitialDir)
        If bSave Then OpenFile.lpstrFile = Left$(InitialDir & String$(1024, vbNullChar), 1024)

    End If
    If bSave Then
        OpenFile.lpstrTitle = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 320)   'Enregistrer sous
    Else
        OpenFile.lpstrTitle = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 321)   'S�lection d'un fichier de dessin
    End If
    
    OpenFile.flags = 0
    If bSave Then
        OpenFile.flags = cdlOFNOverwritePrompt
        lReturn = GetSaveFileName(OpenFile)
    Else
        lReturn = GetOpenFileName(OpenFile)
    End If
    If lReturn = 0 Then
        'MsgBox "Aucun fichier s�lectionn� !", vbInformation, _
        '  "S�lection du fichier"
        RechercheFichier = ""
    Else
        RechercheFichier = Trim(Left(OpenFile.lpstrFile, InStr(1, OpenFile.lpstrFile, vbNullChar) - 1))
    End If
End Function

Function RechercheFichierHex(strform As Form, InitialDir As String) As String
    Dim OpenFile As OPENFILENAME
    Dim lReturn As Long
    Dim sFilter As String
    Dim stBuf As String, lgBuf As Long, lgRep As Long
    
    OpenFile.lStructSize = Len(OpenFile)
    OpenFile.hwndOwner = strform.hWnd
    
    sFilter = "Fichiers hex" & Chr(0) & "*.hex" & Chr(0)
        
    OpenFile.lpstrFilter = sFilter
    OpenFile.nFilterIndex = 1
    OpenFile.lpstrFile = String(257, 0)
    OpenFile.nMaxFile = Len(OpenFile.lpstrFile) - 1
    OpenFile.lpstrFileTitle = OpenFile.lpstrFile
    OpenFile.nMaxFileTitle = OpenFile.nMaxFile
    If Len(Trim(InitialDir)) = 0 Then
        OpenFile.lpstrInitialDir = SpecFolder(CSIDL_DESKTOPDIRECTORY) & "\"
    Else
        OpenFile.lpstrInitialDir = InitialDir
    End If
    OpenFile.lpstrTitle = "S�lection du fichier hex"
    OpenFile.flags = 0
    lReturn = GetOpenFileName(OpenFile)
        If lReturn = 0 Then
            MsgBox "Aucun fichier s�lectionn� !", vbInformation, _
              "S�lection du fichier"
            RechercheFichierHex = ""
         Else
            RechercheFichierHex = Trim(Left(OpenFile.lpstrFile, InStr(1, OpenFile.lpstrFile, vbNullChar) - 1))
         End If
End Function

Public Function SpecFolder(ByVal lngFolder As Long) As String
Dim lngPidlFound As Long
Dim lngFolderFound As Long
Dim lngPidl As Long
Dim strPath As String

strPath = Space(MAX_PATH)

lngPidlFound = SHGetSpecialFolderLocation(0, lngFolder, lngPidl)
If lngPidlFound = NOERROR Then
    lngFolderFound = SHGetPathFromIDList(lngPidl, strPath)
    If lngFolderFound Then
        SpecFolder = Left$(strPath, _
            InStr(1, strPath, vbNullChar) - 1)
    End If
End If
CoTaskMemFree lngPidl
End Function

Public Function GetExtension(ByVal Fichier) As String
    Dim PosPoint As Integer
    Dim i As Integer
    
    PosPoint = -1
    
    For i = Len(Fichier) To 2 Step -1
        If Mid$(Fichier, i, 1) = "." Then
            PosPoint = i
            Exit For
        End If
    Next i
    If PosPoint = -1 Then
        GetExtension = ""
    Else
        GetExtension = Mid$(Fichier, PosPoint + 1, Len(Fichier) - PosPoint)
    End If
End Function

Function OuvrirPLT(nomFichier As String) As Boolean
    Dim i As Long, n As Long, j As Long
    Dim x As Single, y As Single, Rayon As Single
    Dim angle As Single, AngleDep As Double, monAngle As Double
    Dim szBuf1 As String, szBufTemp As String
    Dim nbCar As Long
    Dim iPV As Long
    Dim iDeb As Long
    Dim Cde As String * 2
    Dim PointCentre As Point
    Dim PointDepart As Point
    Dim monPoint As Point

    Dim RetVal As Long
    
    If nomFichier = "" Then
        OuvrirPLT = False
        Exit Function
    End If
    
    Open nomFichier For Input Access Read As #1
    nbCar = LOF(1)
    szBuf1 = Input(nbCar, 1)
    szBuf1 = Replace(szBuf1, "PUPDPA", "PD")
    szBuf1 = Replace(szBuf1, "PDPA", ";PD")
    szBuf1 = Replace(szBuf1, "PUPA", ";PU")
    'szBuf1 = Replace(szBuf1, "PA", ";PD")
    szBuf1 = Replace(szBuf1, "PDAA", ";AA")
    szBuf1 = Replace(szBuf1, "PDCI", ";CI")
    
    iPV = 1
    iDeb = 1
    
    iPV = InStr(iDeb, szBuf1, ";")
    
    n = -1
    
    While iPV <> 0
        szBufTemp = Mid$(szBuf1, iDeb, iPV - iDeb)
        Cde = UCase$(Left$(szBufTemp, 2))
        If Cde = "PU" Or Cde = "PD" Then
            'On cherche l'espace
            i = InStr(szBufTemp, " ")
            'ou la virgule
            If i = 0 Then i = InStr(szBufTemp, ",")
            If i <> 0 Then
                n = n + 1
                ReDim Preserve monTableauLecture(0 To n)
                x = Val(Mid$(szBufTemp, 3, i - 3))
                y = Val(Mid$(szBufTemp, i + 1, Len(szBufTemp)))
                monTableauLecture(n).x = x
                monTableauLecture(n).y = y
            End If
        End If
        If Cde = "CI" Then
            'C'est un cercle, on cherche le rayon
            Rayon = Val(Mid$(szBufTemp, 3, Len(szBufTemp) - 2))
            If Rayon <> 0 Then
                PointCentre.x = monTableauLecture(n - 1).x
                PointCentre.y = monTableauLecture(n - 1).y
                n = n - 1 'le centre ne doit pas �tre trac� !
                For j = 0 To 360
                    If n > UBound(monTableauLecture) Then
                        ReDim Preserve monTableauLecture(0 To n)
                    End If
                    x = Cos(j * PI / 180) * Rayon + PointCentre.x
                    y = Sin(j * PI / 180) * Rayon + PointCentre.y
                    monTableauLecture(n).x = x
                    monTableauLecture(n).y = y
                    n = n + 1
                Next j
            End If
        End If
        If Cde = "AA" Then
            'C'est un arc, on cherche le rayon
            'On cherche l'espace
            i = InStr(szBufTemp, " ")
            'ou la virgule
            If i = 0 Then i = InStr(szBufTemp, ",")
            Rayon = 0
            If i <> 0 Then
                x = Val(Mid$(szBufTemp, 3, i - 3))
                szBufTemp = Right$(szBufTemp, Len(szBufTemp) - i)
                j = InStr(szBufTemp, " ")
                If j = 0 Then j = InStr(szBufTemp, ",")
                y = Val(Mid$(szBufTemp, 1, j))
                angle = Val(Mid$(szBufTemp, j + 1, Len(szBufTemp) - j))
                angle = angle * PI / 180
            End If
            'Calcul du rayon
            PointDepart.x = monTableauLecture(n - 1).x
            PointDepart.y = monTableauLecture(n - 1).y
            PointCentre.x = x
            PointCentre.y = y
            Rayon = Calcul_Distance(PointCentre, PointDepart)
            'Calcul de l'angle de d�part
            Call Calcul_Ang_Segment(PointCentre, PointDepart, AngleDep)
            If angle > 0 Then
                For monAngle = AngleDep To AngleDep + angle Step 2 * PI / 360
                    If n > UBound(monTableauLecture) Then
                        ReDim Preserve monTableauLecture(0 To n)
                    End If
                    x = Cos(monAngle) * Rayon + PointCentre.x
                    y = Sin(monAngle) * Rayon + PointCentre.y
                    monTableauLecture(n).x = x
                    monTableauLecture(n).y = y
                    n = n + 1
                Next monAngle
            Else
                For monAngle = AngleDep To AngleDep + angle Step -2 * PI / 360
                    If n > UBound(monTableauLecture) Then
                        ReDim Preserve monTableauLecture(0 To n)
                    End If
                    x = Cos(monAngle) * Rayon + PointCentre.x
                    y = Sin(monAngle) * Rayon + PointCentre.y
                    monTableauLecture(n).x = x
                    monTableauLecture(n).y = y
                    n = n + 1
                Next monAngle
            End If
        End If
        
        iDeb = iPV + 1
        'On saute �ventuellement le CRLF
        If Mid$(szBuf1, iDeb, 1) = vbCr Then iDeb = iDeb + 1
        If Mid$(szBuf1, iDeb, 1) = vbLf Then iDeb = iDeb + 1

        iPV = InStr(iDeb, szBuf1, ";")

    Wend
    Close #1
    
    'Call Supprime_Doublons
        
    'Call CalculMaxTableau(False)
End Function

Function OuvrirDAT(nomFichier As String) As Boolean
    Dim i As Long, n As Long, j As Long
    Dim szBuf1 As String, szBufTemp As String
    Dim szBufx As String
    Dim szBufy As String
    Dim nbCar As Long
    Dim iPV As Long
    Dim iDeb As Long
    Dim Cde As String * 2

    Dim RetVal As Long
    
    If nomFichier = "" Then
        OuvrirDAT = False
        Exit Function
    End If
    
    'Calcul des coordonn�es min et max
    If Not CalculMaxDAT(nomFichier) Then
        Exit Function
    End If
    
    Open nomFichier For Input Access Read As #1
    nbCar = LOF(1)
    szBuf1 = Input(nbCar, 1) & vbCrLf
    
    iPV = 1
    iDeb = 1
    
    iPV = InStr(iDeb, szBuf1, vbCrLf)
    
    n = 0
    
    While iPV <> 0
        szBufTemp = Mid$(szBuf1, iDeb, iPV - iDeb)
        'On cherche l'espace
        i = InStr(szBufTemp, " ")
        If i <> 0 Then
            szBufx = Left$(szBufTemp, i)
            szBufy = Right$(szBufTemp, Len(szBufTemp) - i)
            If InStr(szBufx, ".") <> 0 And _
                InStr(szBufy, ".") <> 0 Then
                ReDim Preserve monTableauLecture(0 To n)
                monTableauLecture(n).x = Val(szBufx)
                monTableauLecture(n).y = Val(szBufy)
                n = n + 1
            End If
        End If
        
        iDeb = iPV + 1

        iPV = InStr(iDeb, szBuf1, vbCrLf)

    Wend
    Close #1
    
    'Call Remplissage_TabDecal
End Function

Function OuvrirRPFC(nomFichier As String) As Boolean
    Dim i As Long, nbCar As Long
    Dim monEnreg As String, szBuf1 As String
    Dim RapportH As Double
    Dim RapportV As Double
    
    If nomFichier = "" Then
        OuvrirRPFC = False
        Exit Function
    End If
    
    RapportH = 1
    RapportV = 1
    
    'Calcul des coordonn�es min et max
    If Not CalculMaxRPFC(nomFichier) Then
        Exit Function
    End If
    
    monEnreg = Space$(255)

    Open nomFichier For Input Access Read As #1
    
    i = 0
    
    While Not EOF(1)
        Line Input #1, monEnreg
        If Left$(monEnreg, 2) <> "$$" Then
            monEnreg = Replace(monEnreg, ",", ".")
            monTableauLecture(i).x = Val(Mid$(monEnreg, 1, 15))
            monTableauLecture(i).y = Val(Mid$(monEnreg, 16, 15))
            i = i + 1
        End If
    Wend
    
    Close #1
    gProjet = "Chargt"
    'Call Chargt_Configuration
    'Call Chargt_Config_Fraisage
    'Call Chargt_Config_Interface
    'Call Chargt_Couleurs
    'Call Chargt_Marges
    'Call Chargt_Optimisation
    'Call Chargt_Outil
    'Call Chargt_Rapports
    
    Call Remplissage_TabDecal
End Function

Function Calcul_Distance(P1 As Point, P2 As Point) As Double
    Dim InterX As Double
    Dim InterY As Double
    
    InterX = P2.x - P1.x
    InterY = P2.y - P1.y
    
    Calcul_Distance = Math.Sqr(InterX * InterX + InterY * InterY)
End Function

Function Calcul_Distance_Carre(P1 As Point, P2 As Point) As Double
    Dim InterX As Double
    Dim InterY As Double
    
    InterX = P2.x - P1.x
    InterY = P2.y - P1.y
    
    Calcul_Distance_Carre = InterX * InterX + InterY * InterY
End Function

Function Calcul_Vitesse(P1 As Point, P2 As Point, Vx As Single, Vy As Single) As Single
    'Calcul de la vitesse r�elle en fonction des vitesses Vx et Vy
    'Trajet : de P1 � P2
    Calcul_Vitesse = Calcul_Distance(P1, P2) / Max(Abs(P2.x - P1.x) / Vx, Abs(P2.y - P1.y) / Vy)
End Function

Function Calcul_VitesseX(P1 As Point, P2 As Point, Vitesse As Single) As Single
    Dim Temps As Single
    'Calcul de la vitesse sur X en fonction de la vitesse sur le segment P1 - P2
    Temps = Calcul_Distance(P1, P2) / Vitesse
    If Temps = 0# Then
        Calcul_VitesseX = 0
    Else
        Calcul_VitesseX = Abs(P2.x - P1.x) / Temps
    End If
End Function

Function Calcul_VitesseY(P1 As Point, P2 As Point, Vitesse As Single) As Single
    Dim Temps As Single
    'Calcul de la vitesse sur Y en fonction de la vitesse sur le segment P1 - P2
    Temps = Calcul_Distance(P1, P2) / Vitesse
    If Temps = 0# Then
        Calcul_VitesseY = 0
    Else
        Calcul_VitesseY = Abs(P2.y - P1.y) / Temps
    End If
End Function

Function Calcul_Vitesse_TempsX(P1 As Point, P2 As Point, Temps As Single) As Single
    'Calcul de la vitesse sur X en fonction du temps sur le segment P1 - P2
    Calcul_Vitesse_TempsX = Abs(P2.x - P1.x) / Temps
End Function

Function Calcul_Vitesse_TempsY(P1 As Point, P2 As Point, Temps As Single) As Single
    'Calcul de la vitesse sur Y en fonction du temps sur le segment P1 - P2
    Calcul_Vitesse_TempsY = Abs(P2.y - P1.y) / Temps
End Function

Function Calcul_Temps_Points(P1 As Point, P2 As Point, Vitesse As Single) As Single
    'Calcul du temps pour le segment P1 - P2 en fonction de la vitesse
    Calcul_Temps_Points = Calcul_Distance(P1, P2) / Vitesse
End Function

Function Calcul_Temps_Distance(Dist As Single, Vitesse As Single) As Single
    'Calcul du temps pour la distance Dist en fonction de la vitesse
    Calcul_Temps_Distance = Abs(Dist / Vitesse)
End Function

Function ArcSin(x As Double) As Double
    On Error Resume Next
    If x = 1 Then
        ArcSin = PI / 2
    ElseIf x = -1 Then
        ArcSin = -PI / 2
    Else
        ArcSin = Math.Atn(x / Sqr(-x * x + 1))
    End If
    ArcSin = ArcSin * 180 / PI
End Function
Sub Calcul_Ang_Segment(P1 As Point, P2 As Point, ByRef Aod As Double)
    'Calcul de l'angle d'un segment par rapport � l'horizontale
    
    If P2.x > P1.x Then Aod = Math.Atn((P1.y - P2.y) / (P1.x - P2.x))
    If P2.x < P1.x Then Aod = Math.Atn((P1.y - P2.y) / (P1.x - P2.x)) + PI
    If P2.x = P1.x Then
        If P2.y > P1.y Then Aod = PI / 2
        If P2.y < P1.y Then Aod = 3 * PI / 2
    End If
    If Aod < 0 Then Aod = Aod + 2 * PI
End Sub

'Sub Supprime_Doublons()
'    'Tassement du tableau : suppression des points similaires cons�cutifs
'    Dim j As Long, n As Long
'    Dim k As Integer
'    Dim EPSILON As Single
'
'    Call CalculMaxTableau(False)
'
'    For k = EMPLANTURE To SAUMON
'        j = 0
'        EPSILON = (pMax(k).x - pMin(k).x) / 100000
'        For n = 1 To UBound(Pointsplt)
'            If Not (Abs(Pointsplt(k, n).x - Pointsplt(k, j).x) < EPSILON _
'                And Abs(Pointsplt(k, n).y - Pointsplt(k, j).y) < EPSILON) _
'                Then
'                j = j + 1
'            End If
'            Pointsplt(k, j) = Pointsplt(k, n)
'        Next n
'        nbPoints(k) = j + 1
'        ReDim Preserve Pointsplt(EMPLANTURE To SAUMON, 0 To nbPoints(k) - 1)
'    Next k
'End Sub

Sub Supprime_Doublons(k As Integer)
    'Tassement du tableau : suppression des points similaires cons�cutifs
    Dim j As Long, n As Long
    Dim nb As Long

    nb = 0

    j = 0
    For n = 1 To nbPoints(k) - 1
        If Not (monTableau(k, n).x = monTableau(k, j).x _
            And monTableau(k, n).y = monTableau(k, j).y) Then
            j = j + 1
        Else
            nb = nb + 1
        End If
        monTableau(k, j) = monTableau(k, n)
    Next n
    nbPoints(k) = nbPoints(k) - nb
End Sub

Sub CalculMaxTableau(bCalculDecalage As Boolean)
    Dim i As Long, j As Long
    Dim k As Integer
    
    For k = EMPLANTURE To SAUMON
        Pmin(k) = gTabCoord(0).P(k)
        Pmax(k) = Pmin(k)
    Next k
    
    'Trac� initial
    For k = EMPLANTURE To SAUMON
        For i = 1 To UBound(Pointsplt)
            Pmin(k).x = Min(Pmin(k).x, gTabCoord(i).P(k).x)
            Pmin(k).y = Min(Pmin(k).y, gTabCoord(i).P(k).y)
            Pmax(k).x = Max(Pmax(k).x, gTabCoord(i).P(k).x)
            Pmax(k).y = Max(Pmax(k).y, gTabCoord(i).P(k).y)
        Next i
    Next k
    Pmin(TOUT).x = Min(Pmin(EMPLANTURE).x, Pmin(SAUMON).x)
    Pmin(TOUT).y = Min(Pmin(EMPLANTURE).y, Pmin(SAUMON).y)
    Pmax(TOUT).x = Max(Pmax(EMPLANTURE).x, Pmax(SAUMON).x)
    Pmax(TOUT).y = Max(Pmax(EMPLANTURE).y, Pmax(SAUMON).y)
    
    'If bCalculDecalage Then
    '    'Trac� d�cal�
    '    For i = 0 To UBound(gTabDecal)
    '        For j = 0 To UBound(gTabDecal(i).TabDecalPLT)
    '            For k = EMPLANTURE To SAUMON
    '                pMin(k).X = Min(pMin(k).X, PointsPLT(k, i).P.X)
    '                pMin(k).Y = Min(pMin(k).Y, PointsPLT(k, i).P.Y)
    '                pMax(k).X = Max(pMax(k).X, PointsPLT(k, i).P.X)
    '                pMax(k).Y = Max(pMax(k).Y, PointsPLT(k, i).P.Y)
    '            Next k
    '        Next j
    '    Next i
    '    pMin(TOUT).X = Min(pMin(EMPLANTURE).X, pMin(SAUMON).X)
    '    pMin(TOUT).Y = Min(pMin(EMPLANTURE).Y, pMin(SAUMON).Y)
    '    pMax(TOUT).X = Max(pMax(EMPLANTURE).X, pMax(SAUMON).X)
    '    pMax(TOUT).Y = Max(pMax(EMPLANTURE).Y, pMax(SAUMON).Y)
    'End If
End Sub

Function CalculMaxDAT(nomFichier As String) As Boolean
    'Lecture du fichier pour trouver les dimensions horizontales et verticales et le nombre de points
    'Parcours du .DAT
    Dim x As Double, y As Double
    Dim xAncien As Double, yAncien As Double
    Dim bPrem As Boolean
    Dim szBuf1 As String
    Dim szBufTemp As String
    Dim szBufx As String
    Dim szBufy As String
    Dim i As Integer
    Dim nbCar As Long
    Dim iPV As Long
    Dim iDeb As Long
    Dim Cde As String * 2
    Dim CdeAncien As String * 2
    Dim xMin As Double
    Dim xMax As Double
    Dim Ymin As Double
    Dim Ymax As Double
    
    bPrem = True
        
    Open nomFichier For Input Access Read As #1
    nbCar = LOF(1)
    szBuf1 = Input(nbCar, 1)
    
    iPV = 1
    iDeb = 1
    iPV = InStr(iDeb, szBuf1, vbCrLf)

    While iPV <> 0
        szBufTemp = Mid$(szBuf1, iDeb, iPV - iDeb)
        'On cherche l'espace
        i = InStr(szBufTemp, " ")
        If i <> 0 Then
            szBufx = Left$(szBufTemp, i)
            szBufy = Right$(szBufTemp, Len(szBufTemp) - i)
            If InStr(szBufx, ".") <> 0 And _
                InStr(szBufy, ".") <> 0 Then
                x = Val(szBufx)
                y = Val(szBufy)
            
                If x < xMin Or bPrem Then
                    xMin = x
                End If
                If x > xMax Or bPrem Then
                    xMax = x
                End If
                If y < Ymin Or bPrem Then
                    Ymin = y
                End If
                If y > Ymax Or bPrem Then
                    Ymax = y
                End If
                bPrem = False
                CdeAncien = Cde
                xAncien = x
                yAncien = y
            End If
        End If
        
        iDeb = iPV + 1
        iPV = InStr(iDeb, szBuf1, vbCrLf)

    Wend
    Close #1
    Pmin(EMPLANTURE).x = xMin
    Pmin(EMPLANTURE).y = Ymin
    Pmax(EMPLANTURE).x = xMax
    Pmax(EMPLANTURE).y = Ymax
    If xMin = xMax Or Ymin = Ymax Then
        MsgBox "Fichier inexploitable !", vbCritical + vbOKOnly
        CalculMaxDAT = False
        Exit Function
    End If
    If xMax = xMin And Ymax = Ymin Then
        MsgBox "Fichier vide !", vbCritical + vbOKOnly
        CalculMaxDAT = False
        Exit Function
    End If
    CalculMaxDAT = True
End Function

Sub Remplissage_TabDecal()
    'Dim xMinDecal As Double
    'Dim yMinDecal As Double
    '
    ''remplissage du tableau TabDecal
    'Dim i As Long, n As Long
    'Dim numSequence As String * 2
    '
    'numSequence = "-1"
    'n = 0
    ''calcul du nombre de s�quences diff�rentes
    'For i = 0 To UBound(PointsPLT)
    '    If PointsPLT(i).numSequence <> numSequence Then
    '        n = n + 1
    '        numSequence = PointsPLT(i).numSequence
    '    End If
    'Next i
    '
    'If n = 0 Then
    '    Exit Sub
    'End If
    '
    'ReDim gTabDecal(0 To n - 1)
    ''Remplissage des num�ros de s�quence dans le tableau
    'numSequence = "-1"
    'n = 0
    'For i = 0 To UBound(PointsPLT)
    '    If PointsPLT(i).numSequence <> numSequence Then
    '        gTabDecal(n).numSequence = PointsPLT(i).numSequence
    '        ReDim gTabDecal(n).TabDecalPLT(0 To 0)
    '        n = n + 1
    '        numSequence = PointsPLT(i).numSequence
    '    End If
    'Next i
    '
    ''Parcours de gTabDecal pour trouver le Xmin et le Ymin
    'xMinDecal = Min(gTabDecal(0).TabDecalPLT(EMPLANTURE, 0).P.X, gTabDecal(0).TabDecalPLT(EMPLANTURE, 0).P.X)
    'yMinDecal = Min(gTabDecal(0).TabDecalPLT(EMPLANTURE, 0).P.Y, gTabDecal(0).TabDecalPLT(EMPLANTURE, 0).P.Y)
    'For i = 0 To UBound(gTabDecal)
    '    For n = 0 To UBound(gTabDecal(i).TabDecalPLT)
    '        xMinDecal = Min(xMinDecal, gTabDecal(i).TabDecalPLT(EMPLANTURE, n).P.X)
    '        xMinDecal = Min(xMinDecal, gTabDecal(i).TabDecalPLT(SAUMON, n).P.X)
    '        yMinDecal = Min(yMinDecal, gTabDecal(i).TabDecalPLT(EMPLANTURE, n).P.Y)
    '        yMinDecal = Min(yMinDecal, gTabDecal(i).TabDecalPLT(SAUMON, n).P.Y)
    '    Next n
    'Next i
    'For i = 0 To UBound(PointsPLT)
    '    xMinDecal = Min(xMinDecal, PointsPLT(EMPLANTURE, i).P.X)
    '    xMinDecal = Min(xMinDecal, PointsPLT(SAUMON, i).P.X)
    '    yMinDecal = Min(yMinDecal, PointsPLT(EMPLANTURE, i).P.Y)
    '    yMinDecal = Min(yMinDecal, PointsPLT(SAUMON, i).P.Y)
    'Next i
    'For i = 0 To UBound(gTabDecal)
    '    For n = 0 To UBound(gTabDecal(i).TabDecalPLT)
    '        gTabDecal(i).TabDecalPLT(EMPLANTURE, n).P.X = gTabDecal(i).TabDecalPLT(EMPLANTURE, n).P.X - xMinDecal
    '        gTabDecal(i).TabDecalPLT(SAUMON, n).P.X = gTabDecal(i).TabDecalPLT(SAUMON, n).P.X - xMinDecal
    '        gTabDecal(i).TabDecalPLT(EMPLANTURE, n).P.Y = gTabDecal(i).TabDecalPLT(EMPLANTURE, n).P.Y - yMinDecal
    '        gTabDecal(i).TabDecalPLT(SAUMON, n).P.Y = gTabDecal(i).TabDecalPLT(SAUMON, n).P.Y - yMinDecal
    '    Next n
    'Next i
    'For i = 0 To UBound(PointsPLT)
    '    PointsPLT(EMPLANTURE, i).P.X = PointsPLT(EMPLANTURE, i).P.X - xMinDecal
    '    PointsPLT(SAUMON, i).P.X = PointsPLT(SAUMON, i).P.X - xMinDecal
    '    PointsPLT(EMPLANTURE, i).P.Y = PointsPLT(EMPLANTURE, i).P.Y - yMinDecal
    '    PointsPLT(SAUMON, i).P.Y = PointsPLT(SAUMON, i).P.Y - yMinDecal
    'Next i
End Sub

Function CalculMaxRPFC(nomFichier As String) As Boolean
    'Lecture du fichier pour trouver les dimensions horizontales et verticales et le nombre de points
    'Parcours du .RPFC
    Dim x As Double, y As Double
    Dim bPrem As Boolean
    Dim monEnreg As String
    Dim xMin As Double
    Dim xMax As Double
    Dim Ymin As Double
    Dim Ymax As Double
    
    bPrem = True
    monEnreg = Space$(255)
        
    Open nomFichier For Input Access Read As #1
    
    While Not EOF(1)
        Line Input #1, monEnreg
        If Left$(monEnreg, 2) <> "$$" Then
            monEnreg = Replace(monEnreg, ",", ".")
            x = Val(Mid$(monEnreg, 1, 15))
            y = Val(Mid$(monEnreg, 16, 15))
            If x < xMin Or bPrem Then
                xMin = x
            End If
            If x > xMax Or bPrem Then
                xMax = x
            End If
            If y < Ymin Or bPrem Then
                Ymin = y
            End If
            If y > Ymax Or bPrem Then
                Ymax = y
            End If
            bPrem = False
        End If
    Wend
    
    Close #1
    Pmin(EMPLANTURE).x = xMin
    Pmin(EMPLANTURE).y = Ymin
    Pmax(EMPLANTURE).x = xMax
    Pmax(EMPLANTURE).y = Ymax
    If xMin = xMax Or Ymin = Ymax Then
        MsgBox "Fichier inexploitable !", vbCritical + vbOKOnly
        CalculMaxRPFC = False
        Exit Function
    End If
    If xMax = xMin And Ymax = Ymin Then
        MsgBox "Fichier vide !", vbCritical + vbOKOnly
        CalculMaxRPFC = False
        Exit Function
    End If
    CalculMaxRPFC = True
End Function

Function Increment_Sequence(Num As String) As String
    'On commence par " A" jusqu'� " Z", puis "AA" jusqu'� "AZ", etc juqu'� "ZZ"
    Dim Car As String * 1
    
    If Len(Num) = 0 Then
        Increment_Sequence = " A"
        Exit Function
    End If
        
    If Len(Num) = 1 Then Num = Space$(1) & Num
    Car = Right$(Num, 1)
    If Car < "Z" Then
        Increment_Sequence = Left(Num, 1) & Chr(Asc(Car) + 1)
    Else
        If Left$(Num, 1) = Space$(1) Then
            Increment_Sequence = "AA"
        Else
            Increment_Sequence = Chr(Asc(Left$(Num, 1)) + 1) & "A"
        End If
    End If
End Function

Function Recherche_Debut_Sequence(numSequence As String) As Long
    ''Recherche du d�but de la s�quence :
    '
    'Dim i As Long
    '
    'For i = 0 To UBound(PointsPLT)
    '    If PointsPLT(i).numSequence = numSequence Then
    '        Recherche_Debut_Sequence = i
    '        Exit Function
    '    End If
    'Next i
    'Recherche_Debut_Sequence = -1
End Function

Function Recherche_Nombre_Sequence(debut As Long, minX As Single, maxX As Single, minY As Single, maxY As Single) As Long
    ''Recherche du nombre de points de la s�quence
    '
    'Dim i As Long, numSequence As String * 2
    '
    'If UBound(PointsPLT) = 0 Then
    '    Exit Function
    'End If
    '
    'numSequence = PointsPLT(debut).numSequence
    'minX = Min(PointsPLT(EMPLANTURE, debut).P.X, PointsPLT(SAUMON, debut).P.X)
    'MAXX = Max(PointsPLT(EMPLANTURE, debut).P.X, PointsPLT(SAUMON, debut).P.X)
    'minY = Min(PointsPLT(EMPLANTURE, debut).P.Y, PointsPLT(SAUMON, debut).P.Y)
    'maxY = Max(PointsPLT(EMPLANTURE, debut).P.Y, PointsPLT(SAUMON, debut).P.Y)
    'For i = debut To UBound(PointsPLT)
    '    If PointsPLT(i).numSequence <> numSequence Then
    '        Recherche_Nombre_Sequence = i - debut
    '        Exit Function
    '    End If
    '    minX = Min(minX, PointsPLT(EMPLANTURE, i).P.X)
    '    minX = Min(minX, PointsPLT(SAUMON, i).P.X)
    '    MAXX = Max(MAXX, PointsPLT(EMPLANTURE, i).P.X)
    '    MAXX = Max(MAXX, PointsPLT(SAUMON, i).P.X)
    '    minY = Min(minY, PointsPLT(EMPLANTURE, i).P.Y)
    '    minY = Min(minY, PointsPLT(SAUMON, i).P.Y)
    '    maxY = Max(maxY, PointsPLT(EMPLANTURE, i).P.Y)
    '    maxY = Max(maxY, PointsPLT(SAUMON, i).P.Y)
    'Next i
    'Recherche_Nombre_Sequence = i - debut
End Function

Sub Supprime_Sequence(maSequence As String, EouS As Integer)
    'Suppression d'une s�quence et tassement du tableau
    Dim deb As Long, nb As Long, i As Long
    Dim minX As Single, maxX As Single, minY As Single, maxY As Single
    
    'todo
    'If UBound(PointsPLT(EouS)) = 0 Then
    '    Exit Sub
    'End If
    
    deb = Recherche_Debut_Sequence(maSequence)
    nb = Recherche_Nombre_Sequence(deb, minX, maxX, minY, maxY)
    'todo
    'For i = deb To UBound(PointsPLT(EouS)) - nb
    '    PointsPLT(EouS, i) = PointsPLT(EouS, i + nb)
    'Next i
    nbPoints(EouS) = nbPoints(EouS) - nb
    ReDim Preserve Pointsplt(EouS, nbPoints(EouS))
End Sub

Function PLTtoAPI(P As Point, PB As PictureBox) As Point
    Dim i As Integer
    'todo
    i = 0
    Select Case PB.Parent.Name
        Case "SFrmEmplanture"
            i = E
        Case "SFrmSaumon"
            i = s
        Case "SFrmFace"
            i = f
        Case "SFrmDessus"
            i = D
        Case "frmImporter"
            i = IM
    End Select
    
    If i = 0 Then
        PLTtoAPI.x = P.x
        PLTtoAPI.y = P.y
        Exit Function
    End If
    PLTtoAPI.x = (P.x - PB.Parent.XcentreI) * PB.Parent.CoeffInit + PB.Parent.X0
    If (i = f Or i = D) Then
    Else
        If gbOrientationInv Then PLTtoAPI.x = PB.ScaleWidth - PLTtoAPI.x
    End If
'    If gbOrientationInv Then PLTtoAPI.x = PB.ScaleWidth - PLTtoAPI.x
    PLTtoAPI.y = PB.Height - ((P.y - PB.Parent.YcentreI) * PB.Parent.CoeffInit + PB.Parent.Y0)
End Function

Function DG(P1 As Point, P2 As Point, P3 As Point) As String
    'D�termination du "c�t�" du point P3 par rapport � P1-P2
    
    'Si P1 et P2 sont confondus, renvoie space$(1)
    If P1.x = P2.x And P1.y = P2.y Then
        DG = Space$(1)
        Exit Function
    End If
    
    Dim da As Double, db As Double, dc As Double, dc3 As Double
    'Vecteur orthogonal :
    da = P1.y - P2.y
    db = P2.x - P1.x
    dc = da * P1.x + db * P1.y
    dc3 = da * P3.x + db * P3.y
    If dc3 > dc Then
        DG = "G"    'P3 est � gauche de P1-P2
    Else
        If dc3 < dc Then
            DG = "D"    'P3 est � droite de P1-P2
        Else
            DG = "C"    'P3 est sur P1-P2
        End If
    End If
End Function

Function Min(ByRef A As Variant, ByRef B As Variant)
    If A < B Then
        Min = A
    Else
        Min = B
    End If
End Function

Function Max(ByRef A As Variant, ByRef B As Variant)
    If A > B Then
        Max = A
    Else
        Max = B
    End If
End Function

Sub Swap(ByRef A As Variant, ByRef B As Variant)
    Dim c As Variant
    c = A
    A = B
    B = c
End Sub

Function CtrlKeyPress(KeyAscii As Integer) As Integer
    If Chr(KeyAscii) = "." Or Chr(KeyAscii) = "," Then
        KeyAscii = Asc(SepDecimal)
    End If
    If InStr(allowedKeys, Chr(KeyAscii)) = 0 Then KeyAscii = 0
    CtrlKeyPress = KeyAscii
End Function

Function Calcul_Inter_Droite_Cercle(PA As Point, PB As Point, PC As Point, _
    Rayon As Double, ByRef P1 As Point, ByRef P2 As Point) As Boolean
    'Renvoie 'true' s'il y a intersection
    'Renvoie P1 et P2, les points d'intersection

    'La droite passe par les deux points PA et PB
    'Le cercle est d�fini par son centre PC et son rayon rayon
    Dim alpha As Double
    Dim beta As Double
    Dim gamma As Double
    Dim Delta As Double
    Dim u1 As Double, u2 As Double
    
    alpha = (PB.x - PA.x) ^ 2 + (PB.y - PA.y) ^ 2
    beta = 2 * ((PB.x - PA.x) * (PA.x - PC.x) + (PB.y - PA.y) * (PA.y - PC.y))
    gamma = PA.x ^ 2 + PA.y ^ 2 + PC.x ^ 2 + PC.y ^ 2 - 2 * (PA.x * PC.x + PA.y * PC.y) - Rayon ^ 2
    Delta = beta ^ 2 - 4 * alpha * gamma
    
    If Delta <= 0 Then
        Calcul_Inter_Droite_Cercle = False
        Exit Function
    End If
    
    Calcul_Inter_Droite_Cercle = True
    
    u1 = (-beta + Math.Sqr(Delta)) / 2 / alpha
    u2 = (-beta - Math.Sqr(Delta)) / 2 / alpha
    
    P1.x = PA.x + u1 * (PB.x - PA.x)
    P1.y = PA.y + u1 * (PB.y - PA.y)
    
    P2.x = PA.x + u2 * (PB.x - PA.x)
    P2.y = PA.y + u2 * (PB.y - PA.y)
End Function

Sub Chargt_Materiau(maCombo As Control)
    Dim Section() As String
    Dim i As Integer
    Dim monMat As String
    
    'Lecture mat�riau par d�faut
    gstrMat = ReadINI("MatDefaut", "______")
    
    maCombo.Clear
    Call ListeSectionIni(App.Title & ".ini", Section)  '-- Param�tre Section pass� ByRef
    For i = LBound(Section) To UBound(Section)
        monMat = Section(i)
        If Left$(monMat, 4) = "mat_" And Len(Trim(monMat)) > 4 Then
            monMat = Mid$(monMat, 5, Len(monMat) - 4)
            maCombo.AddItem monMat
        End If
    Next
    On Error Resume Next        'Lancement sans .ini
    For i = 0 To maCombo.ListCount
        If maCombo.List(i) = gstrMat Then
            maCombo.ListIndex = i
        End If
    Next i
End Sub

Function Calcul_Fil(ByVal Materiau As String, ByVal Vitesse_Consigne As Double, _
    ByRef CH As Double, ByRef CB As Double, _
    ByRef VH As Double, ByRef VB As Double, SH1 As Single, SH2 As Single) As Single
    
    'D�termination de la chauffe correspondant � la vitesse de consigne
    CH = ReadINI_Machine_Num("mat_" & Materiau, "CH", 90)
    CB = ReadINI_Machine_Num("mat_" & Materiau, "CB", 20)
    VH = CDbl(ReadINI_Machine_Num("mat_" & Materiau, "VH", 6))
    VB = CDbl(ReadINI_Machine_Num("mat_" & Materiau, "VB", 2))
    If VH = VB Then VH = VB + 0.01
    Calcul_Fil = (Vitesse_Consigne * (CH - CB) / (VH - VB) + (VH * CB - VB * CH) / (VH - VB))
    Calcul_Fil = Min(Calcul_Fil, 100#)
    
    'Pour cette vitesse et cette chauffe qui se trouvent sur la droite de chauffe id�ale,
    '       Le rayonnement est �gal � SH1/2
    SH1 = CDbl(ReadINI_Machine_Num("mat_" & Materiau, "SH1", 0))
    SH2 = CDbl(ReadINI_Machine_Num("mat_" & Materiau, "SH2", 0))
End Function

Function Calcul_Fil_VitMax(ByVal Materiau As String) As Single
    Dim CH As Double, CB As Double
    Dim VH As Double, VB As Double
    Dim ChauffeMax As Double
    'Renvoie la vitesse max pour ce mat�riau
    
    'Il faut tenir compte de la chauffe maximum
    'Lecture de la chauffe maximum
    ChauffeMax = Val(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_28", 0))
    CH = ReadINI_Machine_Num("mat_" & Materiau, "CH", 90)
    CB = ReadINI_Machine_Num("mat_" & Materiau, "CB", 20)
    VH = CDbl(ReadINI_Machine_Num("mat_" & Materiau, "VH", 6))
    VB = CDbl(ReadINI_Machine_Num("mat_" & Materiau, "VB", 2))
    If CH = CB Then CH = CB + 0.01
'    Calcul_Fil_VitMax = 100# * (VH - VB) / (CH - CB) + (VB * CH - VH * CB) / (CH - CB)
    Calcul_Fil_VitMax = ChauffeMax * (VH - VB) / (CH - CB) + (VB * CH - VH * CB) / (CH - CB)
End Function

Sub CalculMinMaxPointPLT(monTableau() As PointPLT)
    Dim i As Long
    Dim k As Integer
    
    For k = EMPLANTURE To SAUMON
        Pmin(k).x = monTableau(k, 0).x
        Pmin(k).y = monTableau(k, 0).y
        Pmax(k) = Pmin(k)
        For i = 1 To nbPoints(k) - 1
            Pmin(k).x = Min(Pmin(k).x, monTableau(k, i).x)
            Pmin(k).y = Min(Pmin(k).y, monTableau(k, i).y)
            Pmax(k).x = Max(Pmax(k).x, monTableau(k, i).x)
            Pmax(k).y = Max(Pmax(k).y, monTableau(k, i).y)
        Next i
    Next k
    Pmin(TOUT).x = Min(Pmin(EMPLANTURE).x, Pmin(SAUMON).x)
    Pmin(TOUT).y = Min(Pmin(EMPLANTURE).y, Pmin(SAUMON).y)
    Pmax(TOUT).x = Max(Pmax(EMPLANTURE).x, Pmax(SAUMON).x)
    Pmax(TOUT).y = Max(Pmax(EMPLANTURE).y, Pmax(SAUMON).y)
End Sub

Sub Nettoyage(Pointsplt() As PointPLT, Dist As Single)
'Tassement du tableau : suppression des points "presque" align�s
    Dim i As Long, j As Long, k As Long
    Dim lined As Long
    Dim Nbr_Out As Long
    Dim A As Single, B As Single
    Dim d2 As Single, d1 As Single
    Dim EPSILON As Single
    
    EPSILON = Dist * Dist  'On �l�ve au carr� pour aller plus vite pour les comparaisons

    i = 0
    Nbr_Out = 1

    Do
        j = 1
        Do
            lined = 0
            j = j + 1
            For k = 1 To j - 1
                If Pointsplt(k).Cde <> Pointsplt(k - 1).Cde Then Exit For
                'Est-ce que tous les points interm�diaires sont align�s ?
                A = Pointsplt(i + j).x - Pointsplt(i).x
                B = Pointsplt(i + j).y - Pointsplt(i).y
                d2 = A * A + B * B
    
                If d2 < EPSILON Then
                    ' Les points i and i+j sont trop pr�s
                    d1 = (Pointsplt(i + k).x - Pointsplt(i).x) * (Pointsplt(i + k).x - Pointsplt(i).x) + _
                        (Pointsplt(i + k).y - Pointsplt(i).y) * (Pointsplt(i + k).y - Pointsplt(i).y)
    
                    If d1 < EPSILON Then
                        lined = lined + 1 ' Le point i+k est aussi trop pr�s
                    Else
                        Exit For  ' Le point i+k n'est align�, pas la peine de continuer !
                    End If
                Else
                    d1 = ((Pointsplt(i + k).x - Pointsplt(i).x) * A + _
                        (Pointsplt(i + k).y - Pointsplt(i).y) * B) / d2
    
                    If d1 < 0# Then
                        d1 = 0#  'La r�f�rence est le point i
                    Else
                        If d1 > 1# Then d1 = 1#  ' La r�f�rence est le point i+j
                    End If
    
                    A = Pointsplt(i).x + d1 * A - Pointsplt(i + k).x
                    B = Pointsplt(i).y + d1 * B - Pointsplt(i + k).y
                    If A * A + B * B < EPSILON Then
                        lined = lined + 1 'point align�
                    Else
                        Exit For   'Le point i+k n'est pas align�, pas besoin de continuer
                    End If
                End If
            Next k
        Loop While lined = j - 1 And i + j + 1 <= UBound(Pointsplt) 'Boucle jusqu'� ce qu'un point ne soit pas align�
    
        i = i + j - 1
        Pointsplt(Nbr_Out) = Pointsplt(i)
        Nbr_Out = Nbr_Out + 1
    Loop While (i <= UBound(Pointsplt) - 2)
    
    'Dernier point
    Pointsplt(Nbr_Out) = Pointsplt(UBound(Pointsplt))
    
    'Redimensionnement et r�affichage
    ReDim Preserve Pointsplt(0 To Nbr_Out)
End Sub

Function Resolution(maTable As String) As Single
    'Calcul de la r�solution de la machine
    Dim X_mmTour As Single, X_PasTour As Single
    Dim Y_mmTour As Single, Y_PasTour As Single
    Dim maRes As Integer

    X_mmTour = ReadINI_Machine_Num("table_" & maTable, "TB_SAV_0", 1)
    Y_mmTour = ReadINI_Machine_Num("table_" & maTable, "TB_SAV_1", 1)
    X_PasTour = ReadINI_Machine_Num("table_" & maTable, "TB_SAV_3", 100)
    Y_PasTour = ReadINI_Machine_Num("table_" & maTable, "TB_SAV_4", 100)
    maRes = Val(ReadINI_Machine_Num("table_" & maTable, "Combo_SAV_12", 1))
    X_PasTour = X_PasTour * 2 ^ maRes
    maRes = Val(ReadINI_Machine_Num("table_" & maTable, "Combo_SAV_13", 1))
    Y_PasTour = Y_PasTour * 2 ^ maRes

    Resolution = Min(X_mmTour / X_PasTour, Y_mmTour / Y_PasTour)
End Function

Function RechercheNum(t As String) As Double
    Dim i As Integer
    Dim monTexte As String
    Dim c As String * 1
    
    monTexte = ""
    For i = 1 To Len(t)
        c = Mid$(t, i, 1)
        If c >= "0" And c <= "9" Or c = "." Or c = "," Or c = "-" Then
            monTexte = monTexte & c
        End If
    Next i
    
    RechercheNum = CDbl(Replace(monTexte, ".", Format(0#, "#.#")))
End Function

Sub MEFEnreg(E As String)
    E = Replace(E, ",", ".")
    Print #1, E
End Sub

Sub CreatAssoc(NomLogiciel As String, Extension As String, TypeFichier As String, NomIcone As String)
    Dim NomClef As String
    Dim ValeurClef As String
    Dim RetVal As Long
    Dim ClefHandle As Long
        
    NomClef = NomLogiciel
    RetVal = RegOpenKey(HKEY_CLASSES_ROOT, NomClef, ClefHandle)
    If RetVal = 0 Then Exit Sub

    NomClef = "Software\" & NomLogiciel ' ATTENTION: il y a un "backslash" entre "Software" et Nomlogiciel (inverse de "/", mais �a ne s'affiche pas sur ce site !?)
    ValeurClef = App.Path
    RetVal = RegCreateKey(HKEY_LOCAL_MACHINE, NomClef, ClefHandle)
    RetVal = RegSetValue(ClefHandle, "", REG_SZ, ValeurClef, 0&)

    NomClef = NomLogiciel
    ValeurClef = TypeFichier
    RetVal = RegCreateKey(HKEY_CLASSES_ROOT, NomClef, ClefHandle)
    RetVal = RegSetValue(ClefHandle, "", REG_SZ, ValeurClef, 0&)

    NomClef = Extension
    ValeurClef = NomLogiciel
    RetVal = RegCreateKey(HKEY_CLASSES_ROOT, NomClef, ClefHandle)
    RetVal = RegSetValue(ClefHandle, "", REG_SZ, ValeurClef, 0&)

    NomClef = NomLogiciel
    ValeurClef = """" & App.Path & "\" & App.EXEName & """" & " ""%1"""
    RetVal = RegCreateKey(HKEY_CLASSES_ROOT, NomClef, ClefHandle)
    RetVal = RegSetValue(ClefHandle, "shell\open\command", REG_SZ, ValeurClef, MAX_PATH)
    'ValeurClef = App.Path & NomIcone & ",0"
    'RetVal = RegSetValue(ClefHandle, "DefaultIcon", REG_SZ, ValeurClef, MAX_PATH)
End Sub

Public Function IsRunning(Pgm As String) As Boolean
Dim Test As Boolean
Dim Liste As Object, Element As Object
    'Pgm = "msimn.exe"
    Test = False
    Set Liste = GetObject("winmgmts:").InstancesOf("Win32_Process")

    For Each Element In Liste
        If Trim$(LCase$(Element.Name)) = Trim$(LCase$(Pgm)) Then
            Test = True: Exit For
        End If
    Next
    IsRunning = Test
End Function

Public Sub UnixToDos(MonFichier As String)
'Traitement du format Unix : 0A au lieu de 0D0A
    Dim f As Integer
    Dim monEnreg As String
    Dim monTab() As String

    f = FreeFile
    Open MonFichier For Binary As f
    ' pr�allocation d'un buffer � la taille du fichier
    monEnreg = Space$(LOF(f))
    ' lecture compl�te du fichier
    Get f, , monEnreg
    Close f
    monTab = Split(monEnreg, vbLf)
    If Right(monTab(0), 1) <> Chr(13) Then
        monEnreg = Join(monTab, Chr(13) & Chr(10))
        Open MonFichier For Binary As f
        Put f, , monEnreg
        Close f
    End If
End Sub

Function GetInverseColor(ByVal vbCol As Long) As Long
Dim colDecompose As Col_Sep

    colDecompose = SepareColor(vbCol)
    GetInverseColor = RGB(255 - colDecompose.red, 255 - colDecompose.green, 255 - colDecompose.blue)
End Function
 
Function SepareColor(ByVal ColRGB As Long) As Col_Sep
    With SepareColor
        .red = Int(ColRGB And &HFF)
        .green = Int((ColRGB And &H100FF00) / &H100)
        .blue = Int((ColRGB And &HFF0000) / &H10000)
    End With
End Function

Function FileExists(FileName As String) As Boolean
    'Retourne "Vrai" si le fichier existe
    On Error GoTo ErrorHandler
    ' V�rifie que ce n'est pas un dossier avec le code attribut
    FileExists = (GetAttr(FileName) And vbDirectory) = 0
ErrorHandler:
    ' Si erreur, la fonction retoune "Faux"
End Function

'Pour d�terminer si un tableau est initialis�
Function IsArrayNullChariot(ByRef aArray() As ElemDecoupe) As Boolean
    Dim lVarPtr As Long, lRet As Long
    lVarPtr = VarPtrArray(aArray)
    Call CopyMemory(lRet, ByVal lVarPtr, 4&)
    IsArrayNullChariot = (lRet = 0)
End Function


