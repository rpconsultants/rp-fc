VERSION 5.00
Begin VB.Form frmSynchroOptions 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Synchronisation"
   ClientHeight    =   2325
   ClientLeft      =   2760
   ClientTop       =   3630
   ClientWidth     =   4695
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2325
   ScaleWidth      =   4695
   Tag             =   "1"
   Begin VB.Frame Frame1 
      Caption         =   "Points cl�s"
      Height          =   2340
      Left            =   0
      TabIndex        =   0
      Tag             =   "10"
      ToolTipText     =   "0"
      Top             =   -30
      Width           =   4695
      Begin VB.TextBox TB_Angle 
         Alignment       =   2  'Center
         Height          =   375
         Left            =   3720
         TabIndex        =   6
         Tag             =   "0"
         Text            =   "30.0"
         ToolTipText     =   "0"
         Top             =   1800
         Width           =   855
      End
      Begin VB.CheckBox Check 
         Caption         =   "Changement d'angle sup�rieur � :"
         Height          =   375
         Index           =   5
         Left            =   105
         TabIndex        =   5
         Tag             =   "16"
         ToolTipText     =   "0"
         Top             =   1800
         Width           =   3495
      End
      Begin VB.CheckBox Check 
         Caption         =   "Changements de direction sur Y"
         Height          =   375
         Index           =   4
         Left            =   105
         TabIndex        =   4
         Tag             =   "15"
         ToolTipText     =   "0"
         Top             =   1365
         Width           =   3975
      End
      Begin VB.CheckBox Check 
         Caption         =   "Changements de direction sur X"
         Height          =   375
         Index           =   3
         Left            =   105
         TabIndex        =   3
         Tag             =   "14"
         ToolTipText     =   "0"
         Top             =   990
         Width           =   3975
      End
      Begin VB.CheckBox Check 
         Caption         =   "Intrados et extrados"
         Height          =   375
         Index           =   2
         Left            =   105
         TabIndex        =   2
         Tag             =   "13"
         ToolTipText     =   "0"
         Top             =   645
         Width           =   3975
      End
      Begin VB.CheckBox Check 
         Caption         =   "BA et BF"
         Height          =   375
         Index           =   1
         Left            =   105
         TabIndex        =   1
         Tag             =   "12"
         ToolTipText     =   "0"
         Top             =   285
         Width           =   3975
      End
   End
End
Attribute VB_Name = "frmSynchroOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Check_Click(Index As Integer)
    'Recherche automatique des points cl�s
    Dim i As Long
    Dim iGauche As Long
    Dim iDroit As Long
    Dim PGauche As Point
    Dim PDroit As Point
    Dim iHaut As Long
    Dim iBas As Long
    Dim PHaut As Point
    Dim PBas As Point
    Dim P1 As Point, P2 As Point
    Dim DirPlus As Boolean
    Dim angle As Double
    Dim monAngleCourant As Double
    Dim monAngleAncien As Double
    Dim k As Integer
    
    If Not bProfilCharge Then Exit Sub
    If Check(5).value = vbChecked And Not IsNumeric(TB_Angle.Text) Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 7), vbQuestion + vbYesNo, App.Title
        Exit Sub
    End If
    
    For k = EMPLANTURE To SAUMON
        gListePointSynchro(k).RAZPtCles
    Next k
    Call frmImporter.RemplLV
    
    For k = EMPLANTURE To SAUMON
        'Premier point
        frmImporter.LV_Synchro(k).ListItems(1).Checked = True
        'Dernier point
        frmImporter.LV_Synchro(k).ListItems(frmImporter.LV_Synchro(k).ListItems.count).Checked = True
    Next k
    
    If Check(1).value = vbChecked Then 'BA et BF
        For k = EMPLANTURE To SAUMON
            iGauche = 1
            iDroit = 1
            gListePointSynchro(k).MoveFirst
            i = 1
            PGauche = gListePointSynchro(k).CurrentPoint
            PDroit = PGauche
            While gListePointSynchro(k).MoveNext
                i = i + 1
                If gListePointSynchro(k).CurrentPoint.x < PGauche.x Then
                    iGauche = i
                    PGauche = gListePointSynchro(k).CurrentPoint
                ElseIf gListePointSynchro(k).CurrentPoint.x > PDroit.x Then
                    iDroit = i
                    PDroit = gListePointSynchro(k).CurrentPoint
                End If
            Wend
            'Point le plus � gauche = synchro
            frmImporter.LV_Synchro(k).ListItems(iGauche).Checked = True
            'Point le plus � droite = synchro
            frmImporter.LV_Synchro(k).ListItems(iDroit).Checked = True
        Next k
    End If
    
    If Check(2).value = vbChecked Then 'Intrados et extrados
        For k = EMPLANTURE To SAUMON
            iHaut = 1
            iBas = 1
            gListePointSynchro(k).MoveFirst
            i = 1
            PHaut = gListePointSynchro(k).CurrentPoint
            PBas = PHaut
            While gListePointSynchro(k).MoveNext
                i = i + 1
                If gListePointSynchro(k).CurrentPoint.y < PBas.y Then
                    iBas = i
                    PBas = gListePointSynchro(k).CurrentPoint
                ElseIf gListePointSynchro(k).CurrentPoint.y > PHaut.y Then
                    iHaut = i
                    PHaut = gListePointSynchro(k).CurrentPoint
                End If
            Wend
            'Point le plus en haut = synchro
            frmImporter.LV_Synchro(k).ListItems(iHaut).Checked = True
            'Point le plus en bas = synchro
            frmImporter.LV_Synchro(k).ListItems(iBas).Checked = True
        Next k
    End If
    
    If Check(3).value = vbChecked Then 'Changement de direction sur X
        For k = EMPLANTURE To SAUMON
            gListePointSynchro(k).MoveFirst
            P1 = gListePointSynchro(k).CurrentPoint
            gListePointSynchro(k).MoveNext
            P2 = gListePointSynchro(k).CurrentPoint
            DirPlus = IIf(P2.x > P1.x, True, False)
            i = 2
            While gListePointSynchro(k).MoveNext
                i = i + 1
                If (gListePointSynchro(k).CurrentPoint.x > P2.x) <> DirPlus Then
                    frmImporter.LV_Synchro(k).ListItems(i - 1).Checked = True
                    DirPlus = Not DirPlus
                End If
                P2 = gListePointSynchro(k).CurrentPoint
            Wend
        Next k
    End If
    
    If Check(4).value = vbChecked Then 'Changement de direction sur Y
        For k = EMPLANTURE To SAUMON
            gListePointSynchro(k).MoveFirst
            P1 = gListePointSynchro(k).CurrentPoint
            gListePointSynchro(k).MoveNext
            P2 = gListePointSynchro(k).CurrentPoint
            DirPlus = IIf(P2.y > P1.y, True, False)
            i = 2
            While gListePointSynchro(k).MoveNext
                i = i + 1
                If (gListePointSynchro(k).CurrentPoint.y > P2.y) <> DirPlus Then
                    frmImporter.LV_Synchro(k).ListItems(i - 1).Checked = True
                    DirPlus = Not DirPlus
                End If
                P2 = gListePointSynchro(k).CurrentPoint
            Wend
        Next k
    End If
    
    If Check(5).value = vbChecked Then 'Changement d'angle sup�rieur � TB_ANGLE
        angle = Sin(CDbl(TB_Angle) * PI / 180)
        For k = EMPLANTURE To SAUMON
            gListePointSynchro(k).MoveFirst
            P1 = gListePointSynchro(k).CurrentPoint
            gListePointSynchro(k).MoveNext
            P2 = gListePointSynchro(k).CurrentPoint
            Call Calcul_Ang_Segment(P1, P2, monAngleAncien)
            i = 2
            While gListePointSynchro(k).MoveNext
                i = i + 1
                Call Calcul_Ang_Segment(gListePointSynchro(k).CurrentPoint, P2, monAngleCourant)
                
                If Abs(Sin(monAngleCourant - monAngleAncien)) > angle Then
                    frmImporter.LV_Synchro(k).ListItems(i - 1).Checked = True
                End If
                monAngleAncien = monAngleCourant
                P2 = gListePointSynchro(k).CurrentPoint
            Wend
        Next k
    End If
    
    'Mise � jour de la liste des points
    For k = EMPLANTURE To SAUMON
        For i = 1 To frmImporter.LV_Synchro(k).ListItems.count
            gListePointSynchro(k).Synchro i, (frmImporter.LV_Synchro(k).ListItems(i).Checked = True)
        Next i
        frmImporter.Lbl_PtsSynchro(k).Caption = ReadINI_Trad(gLangue, "erreurs", 71) & _
            gListePointSynchro(k).NbPtsSynchro
    Next k
    
    Call frmImporter.Form_Resize
End Sub

Private Sub Form_Activate()
    Dim r As Long
    Const HWND_TOPMOST = -1
    Const HWND_NOTOPMOST = -2
    Const SWP_NOMOVE = &H2
    Const SWP_NOSIZE = &H1

    r = SetWindowPos(Me.hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
End Sub

Private Sub Form_Load()
    ChargeTrad Me
    TB_Angle = Format$(30#, "0.0")
End Sub

Private Sub TB_Angle_Validate(Cancel As Boolean)
    If Not IsNumeric(TB_Angle.Text) Then
        Cancel = True
        Exit Sub
    End If
    If CDbl(TB_Angle) > 0 Then
        Call Check_Click(1)
        Call WriteINI(Me.Name & "_AngleSynchro", Val(TB_Angle))
    End If
End Sub

Private Sub TB_Angle_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Angle_Validate(False)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Angle_LostFocus()
    If Not IsNumeric(TB_Angle) Then Exit Sub
    If CDbl(TB_Angle) = 0 Then
        TB_Angle = Format(0#, "#0.0")
    Else
        TB_Angle = Format$(TB_Angle, "#0.0")
    End If
End Sub

