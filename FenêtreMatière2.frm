VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   8910
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13380
   LinkTopic       =   "Form1"
   ScaleHeight     =   8910
   ScaleWidth      =   13380
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame5 
      Caption         =   "Aide"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1725
      Left            =   11385
      TabIndex        =   93
      Top             =   2745
      Width           =   1875
      Begin VB.CommandButton Command14 
         Caption         =   "Pas � pas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   150
         TabIndex        =   96
         Top             =   1185
         Width           =   1560
      End
      Begin VB.CommandButton Command10 
         Caption         =   "Saign�es"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   150
         TabIndex        =   95
         Top             =   735
         Width           =   1560
      End
      Begin VB.CommandButton Command12 
         Caption         =   "Chauffe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   150
         TabIndex        =   94
         Top             =   285
         Width           =   1560
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Rayonnements"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Left            =   9585
      TabIndex        =   37
      Top             =   6600
      Width           =   3660
      Begin VB.TextBox Text13 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2115
         TabIndex        =   73
         Text            =   "100"
         Top             =   390
         Width           =   465
      End
      Begin VB.TextBox Text17 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2115
         TabIndex        =   72
         Text            =   "100"
         Top             =   1245
         Width           =   465
      End
      Begin VB.Label Label26 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Demi-vitesse DVH"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   105
         TabIndex        =   80
         Top             =   840
         Width           =   1785
      End
      Begin VB.Label Label28 
         Alignment       =   2  'Center
         Caption         =   "----"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   2130
         TabIndex        =   79
         Top             =   840
         Width           =   540
      End
      Begin VB.Label Label35 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2775
         TabIndex        =   78
         Top             =   840
         Width           =   660
      End
      Begin VB.Label Label17 
         Alignment       =   2  'Center
         BackColor       =   &H0000C0C0&
         Caption         =   "Saign�e SH1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   105
         TabIndex        =   77
         Top             =   405
         Width           =   1785
      End
      Begin VB.Label Label32 
         Alignment       =   2  'Center
         BackColor       =   &H0000C0C0&
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2775
         TabIndex        =   76
         Top             =   390
         Width           =   660
      End
      Begin VB.Label Label37 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         Caption         =   "Saign�e SH2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   105
         TabIndex        =   75
         Top             =   1260
         Width           =   1785
      End
      Begin VB.Label Label39 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2775
         TabIndex        =   74
         Top             =   1245
         Width           =   660
      End
      Begin VB.Line Line6 
         X1              =   2010
         X2              =   2010
         Y1              =   300
         Y2              =   1965
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Droite de chauffe id�ale"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Left            =   5790
      TabIndex        =   30
      Top             =   6600
      Width           =   3660
      Begin VB.TextBox Text19 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2190
         TabIndex        =   82
         Text            =   "100"
         Top             =   1635
         Width           =   465
      End
      Begin VB.TextBox Text20 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2115
         TabIndex        =   81
         Text            =   "99.99"
         Top             =   1200
         Width           =   630
      End
      Begin VB.TextBox Text14 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2190
         TabIndex        =   34
         Text            =   "100"
         Top             =   810
         Width           =   465
      End
      Begin VB.TextBox Text12 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2115
         TabIndex        =   33
         Text            =   "99.99"
         Top             =   375
         Width           =   630
      End
      Begin VB.Label Label47 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2835
         TabIndex        =   86
         Top             =   1635
         Width           =   690
      End
      Begin VB.Label Label48 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2850
         TabIndex        =   85
         Top             =   1230
         Width           =   675
      End
      Begin VB.Label Label52 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "Chauffe CB"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   180
         TabIndex        =   84
         Top             =   1665
         Width           =   1560
      End
      Begin VB.Label Label53 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         Caption         =   "Vitesse VB"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   165
         TabIndex        =   83
         Top             =   1245
         Width           =   1590
      End
      Begin VB.Label Label30 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2835
         TabIndex        =   36
         Top             =   810
         Width           =   690
      End
      Begin VB.Label Label29 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C000&
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2850
         TabIndex        =   35
         Top             =   405
         Width           =   675
      End
      Begin VB.Line Line3 
         X1              =   2010
         X2              =   2010
         Y1              =   300
         Y2              =   2040
      End
      Begin VB.Label Label31 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         Caption         =   "Chauffe CH"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   180
         TabIndex        =   32
         Top             =   840
         Width           =   1560
      End
      Begin VB.Label Label25 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C000&
         Caption         =   "Vitesse VH"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   165
         TabIndex        =   31
         Top             =   420
         Width           =   1590
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7935
      Left            =   75
      TabIndex        =   4
      Top             =   900
      Width           =   5565
      _ExtentX        =   9816
      _ExtentY        =   13996
      _Version        =   393216
      TabHeight       =   882
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Blocs"
      TabPicture(0)   =   "Fen�treMati�re2.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Image1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label18"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label19"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label20"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label21"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label22"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label23"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label24"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label33"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label34"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label54"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label55"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label56"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Label57"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Label43"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Command4"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Check3"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Text8"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Check5"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Text9"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Text10"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Text11"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "Text15"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Check6"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "Command5"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).ControlCount=   25
      TabCaption(1)   =   "Tranches"
      TabPicture(1)   =   "Fen�treMati�re2.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame18"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame3"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Test"
      TabPicture(2)   =   "Fen�treMati�re2.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label51"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Image4"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "Image8"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "Label15"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "Label16"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "Label45"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "Label46"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "Label49"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "Label50"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).Control(9)=   "Image9"
      Tab(2).Control(9).Enabled=   0   'False
      Tab(2).Control(10)=   "Text18"
      Tab(2).Control(10).Enabled=   0   'False
      Tab(2).Control(11)=   "Text7"
      Tab(2).Control(11).Enabled=   0   'False
      Tab(2).Control(12)=   "Text23"
      Tab(2).Control(12).Enabled=   0   'False
      Tab(2).Control(13)=   "Command17"
      Tab(2).Control(13).Enabled=   0   'False
      Tab(2).ControlCount=   14
      Begin VB.CommandButton Command17 
         Caption         =   "Lancer la d�coupe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   -72090
         TabIndex        =   119
         Top             =   6915
         Width           =   2085
      End
      Begin VB.TextBox Text23 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -73770
         TabIndex        =   116
         Top             =   6990
         Width           =   645
      End
      Begin VB.TextBox Text7 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -73740
         TabIndex        =   115
         Top             =   5190
         Width           =   615
      End
      Begin VB.TextBox Text18 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -72045
         TabIndex        =   113
         Top             =   5190
         Width           =   645
      End
      Begin VB.Frame Frame3 
         Caption         =   "Arr�t d'urgence"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   840
         Left            =   -74865
         TabIndex        =   108
         Top             =   6975
         Width           =   5310
         Begin VB.CommandButton Command16 
            Caption         =   "STOP"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Left            =   2445
            TabIndex        =   109
            Top             =   285
            Width           =   2505
         End
      End
      Begin VB.CommandButton Command5 
         Caption         =   "ARRET D'URGENCE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1170
         Left            =   3825
         TabIndex        =   58
         Top             =   6585
         Width           =   1530
      End
      Begin VB.CheckBox Check6 
         Caption         =   "Manuel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2820
         TabIndex        =   52
         Top             =   4140
         Width           =   1560
      End
      Begin VB.TextBox Text15 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3885
         TabIndex        =   50
         Top             =   3540
         Width           =   645
      End
      Begin VB.TextBox Text11 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1770
         TabIndex        =   48
         Top             =   3540
         Width           =   645
      End
      Begin VB.TextBox Text10 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2955
         TabIndex        =   45
         Top             =   1590
         Width           =   750
      End
      Begin VB.TextBox Text9 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3630
         TabIndex        =   42
         Top             =   2685
         Width           =   720
      End
      Begin VB.CheckBox Check5 
         Caption         =   "Vitesse maxi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3750
         TabIndex        =   41
         Top             =   3045
         Width           =   1560
      End
      Begin VB.TextBox Text8 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1590
         TabIndex        =   38
         Top             =   2700
         Width           =   705
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Vitesse maxi"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1695
         TabIndex        =   29
         Top             =   3060
         Width           =   1560
      End
      Begin VB.Frame Frame18 
         Caption         =   "Tranches"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2835
         Left            =   -74880
         TabIndex        =   13
         Top             =   3930
         Width           =   5325
         Begin VB.TextBox Text16 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4110
            TabIndex        =   88
            Top             =   450
            Width           =   510
         End
         Begin VB.CommandButton Command9 
            BackColor       =   &H00E0E0E0&
            Height          =   900
            Left            =   3990
            Picture         =   "Fen�treMati�re2.frx":0054
            Style           =   1  'Graphical
            TabIndex        =   87
            Top             =   870
            Width           =   1035
         End
         Begin VB.TextBox Text6 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1185
            TabIndex        =   26
            Top             =   825
            Width           =   645
         End
         Begin VB.CommandButton Command2 
            BackColor       =   &H00E0E0E0&
            Height          =   750
            Left            =   2745
            Picture         =   "Fen�treMati�re2.frx":12F6
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   1365
            Width           =   1035
         End
         Begin VB.TextBox Text2 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   900
            TabIndex        =   18
            Top             =   1410
            Width           =   750
         End
         Begin VB.TextBox Text3 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1185
            TabIndex        =   17
            Top             =   435
            Width           =   645
         End
         Begin VB.TextBox Text5 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   900
            TabIndex        =   16
            Top             =   1875
            Width           =   750
         End
         Begin VB.CommandButton Command1 
            BackColor       =   &H00E0E0E0&
            Height          =   600
            Left            =   2745
            Picture         =   "Fen�treMati�re2.frx":22F0
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   435
            Width           =   1035
         End
         Begin VB.CheckBox Check4 
            Caption         =   "Pause apr�s x"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   465
            TabIndex        =   14
            Top             =   2310
            Width           =   1785
         End
         Begin VB.Label Label41 
            BackStyle       =   0  'Transparent
            Caption         =   "� y"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4740
            TabIndex        =   89
            Top             =   465
            Width           =   360
         End
         Begin VB.Image Image6 
            Height          =   2400
            Left            =   3975
            Picture         =   "Fen�treMati�re2.frx":3086
            Top             =   210
            Width           =   1050
         End
         Begin VB.Image Image3 
            Height          =   2400
            Left            =   2730
            Picture         =   "Fen�treMati�re2.frx":6950
            Top             =   210
            Width           =   1050
         End
         Begin VB.Label Label13 
            Alignment       =   1  'Right Justify
            Caption         =   "Chauffe :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   225
            TabIndex        =   28
            Top             =   855
            Width           =   885
         End
         Begin VB.Label Label12 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2025
            TabIndex        =   27
            Top             =   855
            Width           =   375
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "x ="
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   300
            TabIndex        =   25
            Top             =   1440
            Width           =   465
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Vitesse :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   225
            TabIndex        =   24
            Top             =   465
            Width           =   885
         End
         Begin VB.Label Label7 
            Caption         =   "mm/s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1950
            TabIndex        =   23
            Top             =   480
            Width           =   555
         End
         Begin VB.Label Label8 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   1740
            TabIndex        =   22
            Top             =   1455
            Width           =   420
         End
         Begin VB.Label Label9 
            Alignment       =   1  'Right Justify
            Caption         =   "y ="
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   300
            TabIndex        =   21
            Top             =   1845
            Width           =   450
         End
         Begin VB.Label Label10 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1740
            TabIndex        =   20
            Top             =   1890
            Width           =   390
         End
         Begin VB.Line Line1 
            X1              =   180
            X2              =   2385
            Y1              =   1260
            Y2              =   1260
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Mont�e / Descente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3210
         Left            =   -74880
         TabIndex        =   6
         Top             =   615
         Width           =   5325
         Begin VB.CommandButton Command15 
            BackColor       =   &H00E0E0E0&
            Height          =   960
            Left            =   2655
            Picture         =   "Fen�treMati�re2.frx":A21A
            Style           =   1  'Graphical
            TabIndex        =   105
            Top             =   1920
            Width           =   840
         End
         Begin VB.TextBox Text22 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3570
            TabIndex        =   104
            Top             =   2055
            Width           =   660
         End
         Begin VB.CheckBox Check9 
            Caption         =   "Origine"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4095
            TabIndex        =   103
            Top             =   2490
            Width           =   1110
         End
         Begin VB.TextBox Text21 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3585
            TabIndex        =   99
            Top             =   345
            Width           =   645
         End
         Begin VB.CheckBox Check8 
            Caption         =   "Vitesse maxi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3270
            TabIndex        =   98
            Top             =   750
            Width           =   1620
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Vitesse maxi"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   720
            TabIndex        =   91
            Top             =   765
            Width           =   1620
         End
         Begin VB.TextBox Text1 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1185
            TabIndex        =   9
            Top             =   1965
            Width           =   660
         End
         Begin VB.TextBox Text4 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1020
            TabIndex        =   8
            Top             =   330
            Width           =   645
         End
         Begin VB.CommandButton Command3 
            BackColor       =   &H00E0E0E0&
            Height          =   1365
            Left            =   210
            Picture         =   "Fen�treMati�re2.frx":B4EC
            Style           =   1  'Graphical
            TabIndex        =   7
            Top             =   1455
            Width           =   900
         End
         Begin VB.Line Line2 
            X1              =   2460
            X2              =   2460
            Y1              =   240
            Y2              =   3090
         End
         Begin VB.Image Image7 
            Height          =   1920
            Left            =   2550
            Picture         =   "Fen�treMati�re2.frx":CEF6
            Top             =   1155
            Width           =   1050
         End
         Begin VB.Label Label65 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   4335
            TabIndex        =   107
            Top             =   2085
            Width           =   360
         End
         Begin VB.Label Label64 
            Alignment       =   1  'Right Justify
            Caption         =   "ou"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   3525
            TabIndex        =   106
            Top             =   2505
            Width           =   435
         End
         Begin VB.Label Label63 
            Caption         =   "mm/s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   4380
            TabIndex        =   102
            Top             =   405
            Width           =   645
         End
         Begin VB.Label Label60 
            Alignment       =   1  'Right Justify
            Caption         =   "Vitesse :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2610
            TabIndex        =   101
            Top             =   390
            Width           =   885
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "ou"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   2745
            TabIndex        =   100
            Top             =   765
            Width           =   360
         End
         Begin VB.Label Label11 
            Alignment       =   1  'Right Justify
            Caption         =   "ou"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   195
            TabIndex        =   92
            Top             =   780
            Width           =   360
         End
         Begin VB.Image Image2 
            Height          =   1920
            Left            =   135
            Picture         =   "Fen�treMati�re2.frx":FD40
            Top             =   1155
            Width           =   1050
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            Caption         =   "Vitesse :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   45
            TabIndex        =   12
            Top             =   375
            Width           =   885
         End
         Begin VB.Label Label5 
            Caption         =   "mm/s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1815
            TabIndex        =   11
            Top             =   390
            Width           =   645
         End
         Begin VB.Label Label6 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1950
            TabIndex        =   10
            Top             =   1995
            Width           =   360
         End
      End
      Begin VB.CommandButton Command4 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1785
         Left            =   3825
         Picture         =   "Fen�treMati�re2.frx":12B8A
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   4695
         Width           =   1530
      End
      Begin VB.Image Image9 
         Height          =   1785
         Left            =   -71415
         Picture         =   "Fen�treMati�re2.frx":15248
         Top             =   2595
         Width           =   1500
      End
      Begin VB.Label Label50 
         Alignment       =   1  'Right Justify
         Caption         =   "Vitesse :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -74745
         TabIndex        =   118
         Top             =   7035
         Width           =   885
      End
      Begin VB.Label Label49 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -72975
         TabIndex        =   117
         Top             =   7050
         Width           =   645
      End
      Begin VB.Label Label46 
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -73080
         TabIndex        =   114
         Top             =   5250
         Width           =   360
      End
      Begin VB.Label Label45 
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -71355
         TabIndex        =   112
         Top             =   5250
         Width           =   360
      End
      Begin VB.Label Label16 
         BackColor       =   &H80000000&
         Caption         =   "Indiquer la distance entre le chariot et l'emplanture et la distance entre l'emplanture et le saumon :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   -74835
         TabIndex        =   111
         Top             =   4545
         Width           =   5205
      End
      Begin VB.Label Label15 
         Caption         =   "L'emplanture et le saumon d�coup�s doivent parfaitement s'emboiter :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1125
         Left            =   -71730
         TabIndex        =   110
         Top             =   1485
         Width           =   2130
      End
      Begin VB.Image Image8 
         Height          =   1365
         Left            =   -74340
         Picture         =   "Fen�treMati�re2.frx":1DA7A
         Top             =   5430
         Width           =   4110
      End
      Begin VB.Image Image4 
         Height          =   2610
         Left            =   -74850
         Picture         =   "Fen�treMati�re2.frx":1E3AC
         Top             =   1665
         Width           =   3000
      End
      Begin VB.Label Label51 
         BackColor       =   &H80000000&
         Caption         =   $"Fen�treMati�re2.frx":21749
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   -74865
         TabIndex        =   97
         Top             =   645
         Width           =   5190
      End
      Begin VB.Label Label43 
         BackColor       =   &H80000000&
         Caption         =   "L'outil ci-dessous est une guillotine verticale pour pr�parer vos blocs de mati�re pour les tests."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   540
         Left            =   180
         TabIndex        =   90
         Top             =   735
         Width           =   5160
      End
      Begin VB.Label Label57 
         Alignment       =   2  'Center
         Caption         =   "Chauffe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   240
         TabIndex        =   57
         Top             =   3840
         Width           =   930
      End
      Begin VB.Label Label56 
         Alignment       =   2  'Center
         Caption         =   "Vitesse"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   240
         TabIndex        =   56
         Top             =   2910
         Width           =   930
      End
      Begin VB.Label Label55 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Descente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3750
         TabIndex        =   55
         Top             =   2175
         Width           =   1035
      End
      Begin VB.Label Label54 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         Caption         =   "Mont�e"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         TabIndex        =   54
         Top             =   2190
         Width           =   1035
      End
      Begin VB.Label Label34 
         Caption         =   "ou"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2430
         TabIndex        =   53
         Top             =   4155
         Width           =   465
      End
      Begin VB.Label Label33 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4635
         TabIndex        =   51
         Top             =   3570
         Width           =   270
      End
      Begin VB.Label Label24 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   2565
         TabIndex        =   49
         Top             =   3585
         Width           =   270
      End
      Begin VB.Label Label23 
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   3765
         TabIndex        =   47
         Top             =   1650
         Width           =   465
      End
      Begin VB.Label Label22 
         Alignment       =   1  'Right Justify
         Caption         =   "Y ="
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2295
         TabIndex        =   46
         Top             =   1665
         Width           =   540
      End
      Begin VB.Label Label21 
         Caption         =   "ou"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3345
         TabIndex        =   44
         Top             =   3060
         Width           =   465
      End
      Begin VB.Label Label20 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4500
         TabIndex        =   43
         Top             =   2745
         Width           =   645
      End
      Begin VB.Label Label19 
         Caption         =   "ou"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1290
         TabIndex        =   40
         Top             =   3075
         Width           =   465
      End
      Begin VB.Label Label18 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2445
         TabIndex        =   39
         Top             =   2760
         Width           =   645
      End
      Begin VB.Image Image1 
         Height          =   3165
         Left            =   105
         Picture         =   "Fen�treMati�re2.frx":217F4
         Top             =   1455
         Width           =   5325
      End
   End
   Begin VB.Frame Frame20 
      Caption         =   "Mati�re"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   13185
      Begin VB.CommandButton Command8 
         Caption         =   "Supprimer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   8340
         TabIndex        =   61
         Top             =   255
         Width           =   1425
      End
      Begin VB.CommandButton Command7 
         Caption         =   "Renommer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6900
         TabIndex        =   60
         Top             =   255
         Width           =   1425
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Copier"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5460
         TabIndex        =   59
         Top             =   255
         Width           =   1425
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   180
         TabIndex        =   3
         Text            =   "Combo1"
         Top             =   270
         Width           =   3465
      End
      Begin VB.CommandButton Command11 
         Caption         =   "Base de donn�es"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   10560
         TabIndex        =   2
         Top             =   255
         Width           =   2355
      End
      Begin VB.CommandButton Command13 
         Caption         =   "Ajouter"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4020
         TabIndex        =   1
         Top             =   255
         Width           =   1425
      End
   End
   Begin VB.Label Label62 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "SH2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   8955
      TabIndex        =   71
      Top             =   1455
      Width           =   645
   End
   Begin VB.Label Label61 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "SH1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   10875
      TabIndex        =   70
      Top             =   1680
      Width           =   645
   End
   Begin VB.Label Label40 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FF8080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "VB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   7365
      TabIndex        =   69
      Top             =   5850
      Width           =   495
   End
   Begin VB.Label Label38 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   5760
      TabIndex        =   68
      Top             =   4545
      Width           =   495
   End
   Begin VB.Label Label27 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000080FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CH"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   5775
      TabIndex        =   67
      Top             =   1755
      Width           =   495
   End
   Begin VB.Label Label59 
      Alignment       =   2  'Center
      Caption         =   "Saign�e (mm)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6735
      TabIndex        =   66
      Top             =   2160
      Width           =   1110
   End
   Begin VB.Label Label58 
      Caption         =   "Chauffe (%)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6645
      TabIndex        =   65
      Top             =   1170
      Width           =   1020
   End
   Begin VB.Label Label44 
      Caption         =   "Vitesse (mm/s)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10950
      TabIndex        =   64
      Top             =   5115
      Width           =   1020
   End
   Begin VB.Label Label42 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "VH"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   10320
      TabIndex        =   63
      Top             =   5850
      Width           =   495
   End
   Begin VB.Label Label36 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFF80&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "DVH"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   8280
      TabIndex        =   62
      Top             =   5850
      Width           =   645
   End
   Begin VB.Image Image5 
      Height          =   4935
      Left            =   6225
      Picture         =   "Fen�treMati�re2.frx":21E39
      Top             =   1020
      Width           =   5415
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
