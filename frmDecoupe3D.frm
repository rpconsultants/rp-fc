VERSION 5.00
Begin VB.Form frmDecoupe3D 
   Caption         =   "Form1"
   ClientHeight    =   7200
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11925
   LinkTopic       =   "Form1"
   ScaleHeight     =   7200
   ScaleWidth      =   11925
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox PB1 
      Height          =   6255
      Left            =   480
      ScaleHeight     =   6195
      ScaleWidth      =   10875
      TabIndex        =   1
      Top             =   360
      Width           =   10935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1215
   End
End
Attribute VB_Name = "frmDecoupe3D"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim DX As New DirectX7
Dim DDRAW As DirectDraw7
Dim Primary As DirectDrawSurface7
Dim BackBuffer As DirectDrawSurface7
Dim Clipper As DirectDrawClipper
Dim D3D As Direct3D7
Dim D3Ddevice As Direct3DDevice7

Dim SrcRect As RECT
Dim DestRect As RECT

Dim viewport(0) As D3DRECT
Dim SurfDesc As DDSURFACEDESC2
Dim VPDesc As D3DVIEWPORT7

Dim Vertex() As D3DVERTEX
Dim Vertex2() As D3DVERTEX

Dim Material As D3DMATERIAL7

Dim MatWorld As D3DMATRIX
Dim MatProj As D3DMATRIX
Dim Matview As D3DMATRIX
Dim MatRotX As D3DMATRIX
Dim MatRotY As D3DMATRIX

Dim EndNow As Boolean
Dim MouseFlag As Integer
Dim AncienX As Integer, AncienY As Integer
Dim AngleX As Integer, AngleY As Integer

Function DirectDrawInit() As Long
    Set DDRAW = DX.DirectDrawCreate("")
    DDRAW.SetCooperativeLevel hwnd, DDSCL_NORMAL
    SurfDesc.lFlags = DDSD_CAPS
    SurfDesc.ddsCaps.lCaps = DDSCAPS_PRIMARYSURFACE
    Set Primary = DDRAW.CreateSurface(SurfDesc)
    SurfDesc.lFlags = DDSD_HEIGHT Or DDSD_WIDTH Or DDSD_CAPS
    SurfDesc.ddsCaps.lCaps = DDSCAPS_OFFSCREENPLAIN Or DDSCAPS_3DDEVICE
    DX.GetWindowRect hwnd, DestRect
    SurfDesc.lWidth = DestRect.right - DestRect.left
    SurfDesc.lHeight = DestRect.bottom - DestRect.top
    Set BackBuffer = DDRAW.CreateSurface(SurfDesc)
    
    With SrcRect
        .left = 0
        .top = 0
        .bottom = SurfDesc.lHeight
        .right = SurfDesc.lWidth
    End With
    
    Set Clipper = DDRAW.CreateClipper(0)
    'Clipper.SetHWnd hwnd
    Clipper.SetHWnd PB1.hwnd
    Primary.SetClipper Clipper
    
    DirectDrawInit = err.Number
End Function

Function Direct3DInit() As Long
    Dim DEnum As Direct3DEnumDevices
    Dim Guid As String

    Set D3D = DDRAW.GetDirect3D
    
    Set DEnum = D3D.GetDevicesEnum
    Guid = DEnum.GetGuid(DEnum.GetCount)

    Set D3Ddevice = D3D.CreateDevice(Guid, BackBuffer)
    VPDesc.lWidth = DestRect.right - DestRect.left
    VPDesc.lHeight = DestRect.bottom - DestRect.top
    VPDesc.minz = 0
    VPDesc.maxz = 1
    D3Ddevice.SetViewport VPDesc
    
    With viewport(0)
        .x1 = 0
        .y1 = 0
        .x2 = VPDesc.lWidth
        .y2 = VPDesc.lHeight
    End With
    
    D3Ddevice.SetRenderState D3DRENDERSTATE_AMBIENT, &HFFFFFFFF
    D3Ddevice.SetRenderState D3DRENDERSTATE_SRCBLEND, D3DBLEND_ONE
    D3Ddevice.SetRenderState D3DRENDERSTATE_DESTBLEND, D3DBLEND_ONE
    D3Ddevice.SetRenderState D3DRENDERSTATE_DITHERENABLE, True
    D3Ddevice.SetRenderState D3DRENDERSTATE_SPECULARENABLE, False
    D3Ddevice.SetTextureStageState 0, D3DTSS_COLORARG1, D3DTA_TEXTURE
    D3Ddevice.SetTextureStageState 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE
    D3Ddevice.SetTextureStageState 0, D3DTSS_COLOROP, D3DTOP_MODULATE
    D3Ddevice.SetTextureStageState 0, D3DTSS_MINFILTER, D3DTFN_LINEAR
    D3Ddevice.SetTextureStageState 0, D3DTSS_MAGFILTER, D3DTFG_LINEAR

    'D3Ddevice.SetRenderState D3DRENDERSTATE_AMBIENT, DX.CreateColorRGBA(1, 1, 1, 1)
    'D3Ddevice.SetRenderState D3DRENDERSTATE_AMBIENT, DX.CreateColorRGBA(0.5, 0.5, 0.5, 1)
    D3Ddevice.SetRenderState D3DRENDERSTATE_CULLMODE, D3DCULL_NONE
    
    Material.Ambient.r = 1
    Material.Ambient.g = 0
    Material.Ambient.B = 0
    D3Ddevice.SetMaterial Material
    
    DX.IdentityMatrix MatWorld
    D3Ddevice.SetTransform D3DTRANSFORMSTATE_WORLD, MatWorld
    DX.IdentityMatrix Matview
    DX.ViewMatrix Matview, MakeVector(0, 0, -3), MakeVector(0, 0, 0), MakeVector(0, 1, 0), 0
    D3Ddevice.SetTransform D3DTRANSFORMSTATE_VIEW, Matview
    DX.IdentityMatrix MatProj
    DX.ProjectionMatrix MatProj, 1, 1000, 3.14 / 2#
    D3Ddevice.SetTransform D3DTRANSFORMSTATE_PROJECTION, MatProj
    
    Direct3DInit = err.Number
End Function

Function MakeVector(x As Single, y As Single, z As Single) As D3DVECTOR
    With MakeVector
        .x = x
        .y = y
        .z = z
    End With
End Function

Sub CreateTriangle()
    ReDim Vertex(0 To 4)
    DX.CreateD3DVertex 0, 0, 0, 0, 0, 0, 0, 0, Vertex(0)
    DX.CreateD3DVertex 0, 1, 0, 0, 0, 0, 0, 0, Vertex(1)
    DX.CreateD3DVertex 1, 1, 0, 0, 0, 0, 0, 0, Vertex(2)
    DX.CreateD3DVertex 1, 0, 0, 0, 0, 0, 0, 0, Vertex(3)
    DX.CreateD3DVertex 0, 0, 0, 0, 0, 0, 0, 0, Vertex(4)
    
    ReDim Vertex2(0 To 4)
    DX.CreateD3DVertex 0, 0, 2, 0, 0, 0, 0, 0, Vertex2(0)
    DX.CreateD3DVertex 0, 1, 2, 0, 0, 0, 0, 0, Vertex2(1)
    DX.CreateD3DVertex 1, 1, 2, 0, 0, 0, 0, 0, Vertex2(2)
    DX.CreateD3DVertex 1, 0, 2, 0, 0, 0, 0, 0, Vertex2(3)
    DX.CreateD3DVertex 0, 0, 2, 0, 0, 0, 0, 0, Vertex2(4)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then EndNow = True
End Sub

Private Sub Form_Load()
    'Show
    
    MouseFlag = 0
    AncienX = 0
    AncienY = 0

    If DirectDrawInit() <> DD_OK Then Unload Me
    If Direct3DInit() <> DD_OK Then Unload Me
    CreateTriangle
    
    D3Ddevice.Clear 1, viewport(), D3DCLEAR_TARGET, vbWhite, 0, 0
    D3Ddevice.BeginScene
    'D3Ddevice.DrawPrimitive D3DPT_TRIANGLELIST, D3DFVF_VERTEX, Vertex(0), UBound(Vertex) + 1, D3DDP_DEFAULT
    D3Ddevice.DrawPrimitive D3DPT_LINESTRIP, D3DFVF_VERTEX, Vertex(0), UBound(Vertex) + 1, D3DDP_DEFAULT
    D3Ddevice.DrawPrimitive D3DPT_LINESTRIP, D3DFVF_VERTEX, Vertex2(0), UBound(Vertex2) + 1, D3DDP_DEFAULT
    D3Ddevice.EndScene
            
    'DX.RotateYMatrix MatRotX, 0.1
    'D3Ddevice.SetTransform D3DTRANSFORMSTATE_WORLD, MatRotX

    'DX.GetWindowRect hwnd, DestRect
    DX.GetWindowRect PB1.hwnd, DestRect
    
    Show
    DoEvents
    Primary.Blt DestRect, BackBuffer, SrcRect, DDBLT_WAIT
    
'    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EndNow = True
End Sub

Private Sub PB1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button <> vbLeftButton Then Exit Sub
    
    If MouseFlag = 0 Then
        AncienX = x
        AncienY = y
        MouseFlag = 1
    Else
        AngleX = (AngleX + AncienX - x) Mod 1024
        AngleY = (AngleY + AncienY - y) Mod 1024
        AncienX = x
        AncienY = y
        'Clear the viewport
        D3Ddevice.Clear 1, viewport(), D3DCLEAR_TARGET, RGB(200, 200, 200), 0, 0
        
        'Begin the scene
        D3Ddevice.BeginScene
        'D3Ddevice.DrawPrimitive D3DPT_TRIANGLELIST, D3DFVF_VERTEX, Vertex(0), UBound(Vertex) + 1, D3DDP_DEFAULT
        Material.Ambient.r = 1
        Material.Ambient.g = 0
        Material.Ambient.B = 0
        D3Ddevice.SetMaterial Material
        D3Ddevice.DrawPrimitive D3DPT_LINESTRIP, D3DFVF_VERTEX, Vertex(0), UBound(Vertex) + 1, D3DDP_DEFAULT
        Material.Ambient.r = 0
        Material.Ambient.g = 0
        Material.Ambient.B = 1
        D3Ddevice.SetMaterial Material
        D3Ddevice.DrawPrimitive D3DPT_LINESTRIP, D3DFVF_VERTEX, Vertex2(0), UBound(Vertex2) + 1, D3DDP_DEFAULT
            
        'End the scene
        D3Ddevice.EndScene
        'Rotate the matrix X
        DX.RotateXMatrix MatRotX, (AngleY * 2 * PI) / 1024#
        'Set the new world transform matrix
        D3Ddevice.SetTransform D3DTRANSFORMSTATE_WORLD, MatRotX
        
        'Rotate the matrix Y
        DX.RotateYMatrix MatRotY, (AngleX * 2 * PI) / 1024#
        'Set the new world transform matrix
        D3Ddevice.MultiplyTransform D3DTRANSFORMSTATE_WORLD, MatRotY
        
        BackBuffer.DrawText 0, 10, "Coucou", False
        'Copy the backbuffer to the screen
        'DX.GetWindowRect hwnd, DestRect
        Primary.Blt DestRect, BackBuffer, SrcRect, DDBLT_WAIT
    End If
    frmDecoupe3D.Caption = "X= " & AngleX & "    Y= " & AngleY
End Sub
