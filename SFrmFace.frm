VERSION 5.00
Begin VB.Form SFrmFace 
   AutoRedraw      =   -1  'True
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Face"
   ClientHeight    =   4080
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   8310
   ControlBox      =   0   'False
   Icon            =   "SFrmFace.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4080
   ScaleWidth      =   8310
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PB 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   3675
      Left            =   60
      ScaleHeight     =   3675
      ScaleWidth      =   7995
      TabIndex        =   0
      Top             =   60
      Width           =   7995
      Begin VB.ComboBox Combo_Zoom 
         Height          =   315
         ItemData        =   "SFrmFace.frx":058A
         Left            =   0
         List            =   "SFrmFace.frx":058C
         TabIndex        =   1
         Tag             =   "0"
         Text            =   "Zoom"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   1335
      End
   End
End
Attribute VB_Name = "SFrmFace"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public X0 As Single, Y0 As Single
Dim X1Zoom As Single, Y1Zoom As Single  'Cadre de zoom
Dim X2Zoom As Single, Y2Zoom As Single
Public XcentreI As Single, YcentreI As Single
Dim XcentreZ As Single, YcentreZ As Single
Dim MemoShift As Integer
Dim X1Pan As Single, Y1Pan As Single 'Origine du d�placement de la figure
Dim MemoX1Pan As Single, MemoY1Pan As Single
Public CoeffInit As Single 'facteur d'adaptation � l'affichage total
Public MemoCoeffInit As Single 'M�morisation pour retour � zoom 1

Private Sub Form_KeyPress(KeyAscii As Integer)
    gPause = True
End Sub

Private Sub Form_Load()
    'Liste de zoom
    Combo_Zoom.AddItem "+"
    Combo_Zoom.AddItem " "
    Combo_Zoom.AddItem "-"
    Combo_Zoom.ListIndex = 1
    Combo_Zoom.Left = -10000
    
    Call InitVariable
End Sub

Public Sub InitVariable()
    Dim xMin As Single
    Dim xMax As Single
    Dim Ymin As Single
    Dim Ymax As Single
    
    'Dimensions du dessin pour Face
    xMin = 0
    Ymin = -10
    xMax = wLargeurTable 'Largeur de la table
    Ymax = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_17", 400)    'Course en Y
    XcentreI = (xMax + xMin) / 2
    YcentreI = (Ymax + Ymin) / 2
    MemoCoeffInit = 1#
    CoeffInit = 1#
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Cancel = False
    Select Case UnloadMode
        Case vbFormControlMenu
            Me.WindowState = vbMinimized
            Cancel = True
    End Select
End Sub

Public Sub Form_Resize()
    Dim monRapport As Double
    Dim xMin As Single, xMax As Single, Ymin As Single, Ymax As Single
    
    With PB
        .Top = 0
        .Left = 0
        .Width = Me.ScaleWidth
        .Height = Me.ScaleHeight
        X0 = .Width / 2  'coordonn�es du centre de la fen�tre
        Y0 = .Height / 2
    End With

    'Dimensions du dessin
    xMin = 0
    Ymin = -10
    xMax = wLargeurTable 'Largeur de la table
    Ymax = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_17", 400)    'Course en Y

    If MemoCoeffInit = 0# Then
        MemoCoeffInit = 1#
        CoeffInit = 1#
    End If
    monRapport = CoeffInit / MemoCoeffInit
    If ((PB.Width - 2 * Marge) / (xMax - xMin)) < ((PB.Height - 2 * Marge) / (Ymax - Ymin)) Then
        CoeffInit = (PB.Width - 2 * Marge) / (xMax - xMin)
    Else
        CoeffInit = (PB.Height - 2 * Marge) / (Ymax - Ymin)
    End If
    MemoCoeffInit = CoeffInit
    CoeffInit = CoeffInit * monRapport
    If Me.WindowState <> vbMinimized Then
        Call MDIFrmDecoupe.PublicDessinFace
    End If
End Sub

Private Sub PB_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbMiddleButton Then
        Combo_Zoom.SetFocus
        Exit Sub
    End If
    
    '****************************************************
    '**** Gestion du ZOOM et DEPLACEMENT � la SOURIS ****
    '****************************************************
    MemoShift = 0
    If Button = vbLeftButton And (Shift And vbShiftMask) <> vbShiftMask And MDIFrmDecoupe.TB.buttons.Item("mnu_ZoomSelection").value = tbrPressed Then
        'Bouton gauche, pas de Shift et bouton "S�lection" appuy� : zoom par s�lection
        MemoShift = 1     'au cas o� la touche serait rel�ch�e
        X1Zoom = x
        Y1Zoom = y
        PB.MousePointer = vbSizePointer  'curseur 4 fl�ches
        PB.DrawStyle = vbDot      'trait petits pointill�s
        PB.AutoRedraw = False     'lors du d�placement, on va dessiner le rectangle sur le plan temporaire
                                  'pour pouvoir l'effacer au fur et � mesure sans effacer la figure
        Exit Sub
    End If
    
    If Button = vbRightButton And (Shift And vbShiftMask) <> vbShiftMask Then
        'bouton droit, pas de shift : d�placement
        MemoShift = 2
        X1Pan = x
        Y1Pan = y
        MemoX1Pan = x 'm�morisation du d�but du d�placement pour affectation � XcentreI et YcentreI
        MemoY1Pan = y '  � la fin du d�placement
        PB.MousePointer = vbCustom  'Curseur personnalis� (main 16x16 sur icone 32x32)
        PB.MouseIcon = MDIFrmDecoupe.ImageMain.Picture
        Exit Sub
    End If
End Sub

Private Sub Combo_Zoom_Click()
    If Combo_Zoom.ListIndex = 0 Then
        CoeffInit = CoeffInit * 1.1
    End If
    If Combo_Zoom.ListIndex = 2 Then
        CoeffInit = CoeffInit / 1.1
    End If
    Combo_Zoom.ListIndex = 1
    Call MDIFrmDecoupe.PublicDessinFace
End Sub

Private Sub PB_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    'Pendant le d�placement du curseur, on trace le rectangle de zoom
    '   ou on d�place la figure
    Select Case MemoShift
    Case 1  'Bouton gauche, pas de Shift
        If (Button = vbLeftButton And MDIFrmDecoupe.TB.buttons.Item("mnu_ZoomSelection").value = tbrPressed) Then  'zoom par s�lection
            PB.Cls
            PB.Line (X1Zoom, Y1Zoom)-(x, y), vbBlack, B   'trac� du rectangle de s�lection
        End If
    Case 2   'Bouton droit, pas de Shift : d�placement
        XcentreZ = X0 - (x - X1Pan)
        YcentreZ = Y0 - (y - Y1Pan)
        XcentreI = (XcentreZ - X0) / CoeffInit + XcentreI
        YcentreI = (PB.Height - (YcentreZ + Y0)) / CoeffInit + YcentreI
        Call MDIFrmDecoupe.PublicDessinFace

        X1Pan = x 'm�morisation de l'emplacement courant comme nouvelle origine de la translation
        Y1Pan = y
    End Select
End Sub

Private Sub PB_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'Lorsqu'on relache le bouton lors d'une op�ration de d�placement
   
    Select Case MemoShift
    Case 1  'Bouton gauche, pas de Shift
        '*** rel�chement du bouton dans le cas du zoom ***
        If (Button = vbLeftButton And MDIFrmDecoupe.TB.buttons.Item("mnu_ZoomSelection").value = tbrPressed) Then     'zoom par s�lection
            'au rel�chement, on efface et on passe sur le plan permanent
            PB.MousePointer = vbNormal 'curseur fl�che
            PB.Cls
            PB.DrawStyle = vbSolid  'traits continus
            PB.AutoRedraw = True 'on repasse sur le plan permanent
            On Error GoTo Annuler  'En cas de division par z�ro si pas de d�placement de la souris
            'On va � l'envers pour retrouver les limites de la fen�tre affich�e sur le profil initial
            X1Zoom = (X1Zoom - X0) / CoeffInit + XcentreI
            Y1Zoom = (PB.Height - Y1Zoom - Y0) / CoeffInit + YcentreI
            X2Zoom = (x - X0) / CoeffInit + XcentreI
            Y2Zoom = (PB.Height - y - Y0) / CoeffInit + YcentreI
            'on calcule le centre de la zone � afficher et son coeff d'ajustement � la fen�tre
            XcentreI = (X1Zoom + X2Zoom) / 2
            YcentreI = (Y1Zoom + Y2Zoom) / 2
            If (PB.Width - 2 * Marge) / Abs(X1Zoom - X2Zoom) < (PB.Height - 2 * Marge) / Abs(Y1Zoom - Y2Zoom) Then
                CoeffInit = (PB.Width - 2 * Marge) / Abs(X1Zoom - X2Zoom)
            Else
                CoeffInit = (PB.Height - 2 * Marge) / Abs(Y1Zoom - Y2Zoom)
            End If
            MDIFrmDecoupe.TB.buttons.Item("mnu_ZoomSelection").value = tbrUnpressed
            End If
      
    Case 2   'Bouton droit, pas de Shift : d�placement
            '*** rel�chement du bouton dans le cas du d�placement de la figure ***
            PB.MousePointer = vbDefault
    End Select
        Call MDIFrmDecoupe.PublicDessinFace
    MemoShift = 0  'reset de la variable pour �v�nement mousemove
Annuler:
    Exit Sub
End Sub

