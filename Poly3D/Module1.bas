Attribute VB_Name = "Module1"
Option Explicit

Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" ( _
    ByVal lpAppName As String, _
    ByVal lpReturnedString As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) _
    As Long

Declare Function GetPrivateProfileSectionNames Lib "kernel32.dll" Alias "GetPrivateProfileSectionNamesA" ( _
    ByVal lpszReturnBuffer As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) _
    As Long

Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
    ByVal AppName$, _
    ByVal KeyName$, _
    ByVal keydefault$, _
    ByVal FileName$)

Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
    ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, _
    ByVal lpDefault As String, _
    ByVal lpReturnedString As String, _
    ByVal nSize As Long, _
    ByVal lpFileName As String) As Long


Sub ChargeTrad(Frm As Form)
    On Error Resume Next

    Dim ctl As Control
    Dim Obj As Object
    Dim fnt As Object
    Dim sCtlType As String
    Dim maLangue As String
    Dim i As Integer
    Dim maTrad As String

    'On va chercher dans le fichier INI en fonction de la langue :
    ' Code ISO 639-1
    ' fr : Fran�ais
    ' en : Anglais
    ' de : Allemand
    ' es : Espagnol
    ' it : Italien
    ' pt : Portugais
    'etc...
    
    'On va chercher la langue
    maLangue = ReadINI("Language", "en")

    'D�finit la l�gende de la feuille
    Frm.Caption = ReadINI_Trad(maLangue, Frm.Name, Frm.Tag)

    'D�finit les l�gendes des contr�les en utilisant la
    'propri�t� caption pour les �l�ments de menu et la propri�t� Tag
    'pour tous les autres contr�les
    For Each ctl In Frm.Controls
        If ctl.Tag <> "0" Or ctl.ToolTipText <> "0" Then
            sCtlType = TypeName(ctl)
            If sCtlType = "Label" Then
                ctl.Caption = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag)
                If ctl.ToolTipText = "0" Then
                    ctl.ToolTipText = ""
                Else
                    ctl.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, ctl.ToolTipText)
                End If
            ElseIf sCtlType = "Menu" Then
                ctl.Caption = ReadINI_Trad(maLangue, Frm.Name, ctl.Caption)
            ElseIf sCtlType = "TabStrip" Then
                For Each Obj In ctl.Tabs
                    If Obj.Tag <> "0" Then
                        Obj.Caption = ReadINI_Trad(maLangue, Frm.Name, Obj.Tag)
                    End If
                    If maTrad <> "___" Then
                        Obj.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, "tt" & Obj.ToolTipText)
                    End If
                Next
            ElseIf sCtlType = "SSTab" Then
                'Pour les TabCaption
                For i = 0 To ctl.Tabs - 1
                    ctl.TabCaption(i) = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                Next i
            ElseIf sCtlType = "Toolbar" Then
                For Each Obj In ctl.Buttons
                    maTrad = ReadINI_Trad(maLangue, Frm.Name, Obj.ToolTipText)
                    If maTrad <> "___" Then
                        Obj.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, "tt" & Obj.ToolTipText)
                    End If
                Next
            ElseIf sCtlType = "ListView" Then
                For Each Obj In ctl.ColumnHeaders
                    Obj.Text = ReadINI_Trad(maLangue, Frm.Name, Obj.Tag)
                Next
            ElseIf sCtlType = "ListBox" Then
                If ctl.Tag <> "0" Then
                    i = 1
                    maTrad = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                    While maTrad <> ctl.Tag & "_" & i
                        ctl.AddItem maTrad
                        i = i + 1
                        maTrad = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                    Wend
                End If
            ElseIf sCtlType = "ComboBox" Then
                If ctl.Tag <> "0" Then
                    If ctl.Tag = "45" Then
                        ctl.Tag = ctl.Tag
                    End If
                    i = 1
                    maTrad = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                    While maTrad <> ctl.Tag & "_" & i
                        ctl.AddItem maTrad
                        i = i + 1
                        maTrad = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag & "_" & i)
                    Wend
                End If
            Else
                If ctl.Tag <> "0" Then
                    ctl.Caption = ReadINI_Trad(maLangue, Frm.Name, ctl.Tag)
                End If
                If ctl.ToolTipText = "0" Then
                    ctl.ToolTipText = ""
                Else
                    ctl.ToolTipText = ReadINI_Trad(maLangue, Frm.Name, ctl.ToolTipText)
                End If
            End If
        End If
        If ctl.ToolTipText = "0" Then
            ctl.ToolTipText = ""
        End If
    Next
End Sub

