#include "stdafx.h"

// Global Variables:
HWND extern hStatus;
HANDLE HID_handle= INVALID_HANDLE_VALUE;
HANDLE HID_write_handle= INVALID_HANDLE_VALUE;
HANDLE HID_EventObject;
OVERLAPPED HIDOverlapped;
char HIDDevicePathName[256]="";
GUID hidGUID;
DWORD extern HID_vendorID, HID_productID;
int extern HID_input_len, HID_output_len;

// Forward declarations for HID:
LRESULT		Main_OnDeviceChange(WPARAM wParam, LPARAM lParam);
BOOL		DeviceNameMatch(LPARAM lParam);
void		RegisterForDeviceNotifications(HWND);
VOID		DisconnectHID();
BOOL		ConnectHID(DWORD, DWORD, DWORD);
BOOL		WriteHID(const BYTE *bytes, int nBuffLen);
int			ReadHID(BYTE *bytes, int nBuffLen);

BOOL ConnectHID(DWORD vendorID, DWORD productID, DWORD versionNumber)
{
    HDEVINFO					hardwareDeviceInfoSet;
    SP_DEVICE_INTERFACE_DATA	deviceInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA	deviceDetail;
    DWORD result;
    DWORD index = 0;
	BOOL LastDevice = FALSE;
    ULONG requiredSize;
    HIDD_ATTRIBUTES deviceAttributes;
	PHIDP_PREPARSED_DATA PreparsedData;
	HIDP_CAPS Capabilities;

	DisconnectHID();
	
    //Get a list of devices matching the criteria (hid interface, present)
    hardwareDeviceInfoSet = SetupDiGetClassDevs (&hidGUID,
                                                 NULL, // Define no enumerator (global)
                                                 NULL, // Define no
                                                 (DIGCF_PRESENT | // Only Devices present
                                                 DIGCF_DEVICEINTERFACE)); // Function class devices.
    deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
	
	//Step through the available devices looking for the one we want. 
	//Quit on detecting the desired device or checking all available devices without success.
	
	do
	{
		/*
		API function: SetupDiEnumDeviceInterfaces
		On return, MyDeviceInterfaceData contains the handle to a
		SP_DEVICE_INTERFACE_DATA structure for a detected device.
		Requires:
		The DeviceInfoSet returned in SetupDiGetClassDevs.
		The HidGuid returned in GetHidGuid.
		An index to specify a device.
		*/
	    result = SetupDiEnumDeviceInterfaces (hardwareDeviceInfoSet,
												NULL, //infoData,
												&hidGUID, //interfaceClassGuid,
												index, 
												&deviceInterfaceData);
		if (result == FALSE)
		{
		    /* Failed to get a device - possibly the index is larger than the number of devices */
			LastDevice = TRUE;
		}
		else
		{
			/*
			API function: SetupDiGetDeviceInterfaceDetail
			Returns: an SP_DEVICE_INTERFACE_DETAIL_DATA structure
			containing information about a device.
			To retrieve the information, call this function twice.
			The first time returns the size of the structure in Length.
			The second time returns a pointer to the data in DeviceInfoSet.
			Requires:
			A DeviceInfoSet returned by SetupDiGetClassDevs
			The SP_DEVICE_INTERFACE_DATA structure returned by SetupDiEnumDeviceInterfaces.
			
			The final parameter is an optional pointer to an SP_DEV_INFO_DATA structure.
			This application doesn't retrieve or use the structure.			
			If retrieving the structure, set 
			MyDeviceInfoData.cbSize = length of MyDeviceInfoData.
			and pass the structure's address.
			*/
		    //Get the details with null values to get the required size of the buffer
			SetupDiGetDeviceInterfaceDetail (hardwareDeviceInfoSet,
                                     &deviceInterfaceData,
                                     NULL, //interfaceDetail,
                                     0, //interfaceDetailSize,
                                     &requiredSize,
                                     0); //infoData))

			//Allocate the buffer
			deviceDetail = (PSP_INTERFACE_DEVICE_DETAIL_DATA)malloc(requiredSize);
			//Set cbSize in the detailData structure.
			deviceDetail->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

			//Call the function again, this time passing it the returned buffer size.
		    if (SetupDiGetDeviceInterfaceDetail (hardwareDeviceInfoSet,
				&deviceInterfaceData,
				deviceDetail,
				requiredSize,
				&requiredSize,
				NULL)) 
			{
			    //Open file on the device
				HID_handle = CreateFile (deviceDetail->DevicePath,
					GENERIC_READ | GENERIC_WRITE,
					FILE_SHARE_READ | FILE_SHARE_WRITE,
					(LPSECURITY_ATTRIBUTES)NULL,        // no SECURITY_ATTRIBUTES structure
					OPEN_EXISTING, // No special create flags
					FILE_FLAG_OVERLAPPED, 
					NULL);       // No template file
				if(HID_handle != INVALID_HANDLE_VALUE)
				{
					/* API function: HidD_GetAttributes
					Requests information from the device.
					Requires: the handle returned by CreateFile.
					Returns: a HIDD_ATTRIBUTES structure containing
					the Vendor ID, Product ID, and Product Version Number.
					Use this information to decide if the detected device is
					the one we're looking for. */
					//Set the Size to the number of bytes in the structure.
					deviceAttributes.Size = sizeof(deviceAttributes);
					if (HidD_GetAttributes (HID_handle, &deviceAttributes))
					{
				        if ((vendorID == 0 || deviceAttributes.VendorID == vendorID) &&
							(productID == 0 || deviceAttributes.ProductID == productID) &&
							(versionNumber == 0 || deviceAttributes.VersionNumber == versionNumber))
						{
							HID_EventObject = CreateEvent(NULL, TRUE, TRUE, "");
							//Set the members of the overlapped structure.
							HIDOverlapped.Offset = 0;
							HIDOverlapped.OffsetHigh = 0;
							HIDOverlapped.hEvent = HID_EventObject;
							strcpy_s(HIDDevicePathName,deviceDetail->DevicePath);
							HID_write_handle = CreateFile (deviceDetail->DevicePath,
								GENERIC_WRITE,
								FILE_SHARE_READ | FILE_SHARE_WRITE,
								(LPSECURITY_ATTRIBUTES)NULL,        // no SECURITY_ATTRIBUTES structure
								OPEN_EXISTING, // No special create flags
								0, 
								NULL);       // No template file
							HidD_GetPreparsedData(HID_handle, &PreparsedData);
							HidP_GetCaps(PreparsedData,&Capabilities);
							HID_input_len=Capabilities.InputReportByteLength;
							HID_output_len=Capabilities.OutputReportByteLength;
							HidD_FreePreparsedData(PreparsedData);
							SendMessage(hStatus, SB_SETTEXT, 1, (LPARAM)"IPL5X: Connected");
						}
						else
						{
							//IDs don't match.
							CloseHandle(HID_handle);
							HID_handle = INVALID_HANDLE_VALUE;
						}
					}
				}
			}
			free (deviceDetail);
		}
		index++;
	} //do
	while ((LastDevice == FALSE) && (HID_handle == INVALID_HANDLE_VALUE));
	SetupDiDestroyDeviceInfoList (hardwareDeviceInfoSet);
	if(HID_handle != INVALID_HANDLE_VALUE)
		return true;
    return false;
}

BOOL WriteHID(const BYTE *bytes, int nBuffLen) 
{
	unsigned long BytesWritten;
	return WriteFile(HID_write_handle, (LPCVOID)bytes, nBuffLen, &BytesWritten, NULL);
}

int ReadHID(BYTE *bytes, int nBuffLen)
{
	unsigned long BytesRead = 0;
	ReadFile(HID_handle, (LPVOID)bytes, nBuffLen, &BytesRead, &HIDOverlapped);
	DWORD Result = WaitForSingleObject(HID_EventObject, 500);
	if (Result == WAIT_OBJECT_0) {		
		ResetEvent(HID_EventObject);
		return BytesRead;
	}
	if(Result == WAIT_TIMEOUT) {
		Result = CancelIo(HID_handle);			
	}

	ResetEvent(HID_EventObject);
	return 0;
}

void RegisterForDeviceNotifications(HWND hwnd)
{
	// Request to receive messages when a device is attached or removed.
	// Also see WM_DEVICECHANGE in BEGIN_MESSAGE_MAP(CUsbhidiocDlg, CDialog).
	DEV_BROADCAST_DEVICEINTERFACE DevBroadcastDeviceInterface;
	HDEVNOTIFY DeviceNotificationHandle;

	DevBroadcastDeviceInterface.dbcc_size = sizeof(DevBroadcastDeviceInterface);
	DevBroadcastDeviceInterface.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	DevBroadcastDeviceInterface.dbcc_classguid = hidGUID;
	DeviceNotificationHandle = RegisterDeviceNotification(hwnd, &DevBroadcastDeviceInterface, DEVICE_NOTIFY_WINDOW_HANDLE);
}

LRESULT Main_OnDeviceChange(WPARAM wParam, LPARAM lParam)  
{
	//DisplayData("Device change detected.");
	PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)lParam;
	switch(wParam) 
		{
		// Find out if a device has been attached or removed.
		// If yes, see if the name matches the device path name of the device we want to access.
		case DBT_DEVICEARRIVAL:
			if(HID_handle == INVALID_HANDLE_VALUE)
					ConnectHID(HID_vendorID, HID_productID,0);
			return TRUE; 
		case DBT_DEVICEREMOVECOMPLETE:
			if (DeviceNameMatch(lParam))
			{
				DisconnectHID();
			}
			return TRUE; 
		default:
			return TRUE; 
		} 
}

BOOL DeviceNameMatch(LPARAM lParam)
{
	// Compare the device path name of a device recently attached or removed 
	// with the device path name of the device we want to communicate with.
	PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)lParam;
	if (lpdb->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE) 
	{
		PDEV_BROADCAST_DEVICEINTERFACE lpdbi = (PDEV_BROADCAST_DEVICEINTERFACE)lParam;
		//The dbch_devicetype parameter indicates that the event applies to a device interface.
		//So the structure in LParam is actually a DEV_BROADCAST_INTERFACE structure, 
		//which begins with a DEV_BROADCAST_HDR.
		//The dbcc_name parameter of DevBroadcastDeviceInterface contains the device name. 
		//Compare the name of the newly attached device with the name of the device 
		//the application is accessing (myDevicePathName).
		if (_stricmp(lpdbi->dbcc_name,HIDDevicePathName) == 0)
            return true; 			//The name matches.
		else
            return false;           //It's a different device.
	}
		else
			return false;			//It's a different device.
}

VOID DisconnectHID()
{
	if (HID_EventObject != INVALID_HANDLE_VALUE)
		CloseHandle(HID_EventObject);
	HID_EventObject = INVALID_HANDLE_VALUE;
	
	if (HID_handle != INVALID_HANDLE_VALUE)
		CloseHandle(HID_handle);
	if (HID_write_handle != INVALID_HANDLE_VALUE)
		CloseHandle(HID_write_handle);
	HID_handle = INVALID_HANDLE_VALUE;
	HID_write_handle = INVALID_HANDLE_VALUE;
	SendMessage(hStatus, SB_SETTEXT, 1, (LPARAM)"IPL5X: Not connected");
}

