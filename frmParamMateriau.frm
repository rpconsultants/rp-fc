VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmParamMateriau 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   10080
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   13260
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10080
   ScaleWidth      =   13260
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.Frame Frame_Calculette 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Calculette"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Left            =   9240
      TabIndex        =   93
      Tag             =   "401"
      ToolTipText     =   "0"
      Top             =   3960
      Visible         =   0   'False
      Width           =   3255
      Begin VB.CommandButton Cmd_Copier_SH 
         Caption         =   "Copier vers SH2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   1680
         TabIndex        =   105
         Tag             =   "409"
         ToolTipText     =   "0"
         Top             =   2400
         Width           =   1455
      End
      Begin VB.CommandButton Cmd_Copier_SH 
         Caption         =   "Copier vers SH1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   120
         TabIndex        =   104
         Tag             =   "408"
         ToolTipText     =   "0"
         Top             =   2400
         Width           =   1455
      End
      Begin VB.TextBox TB_Rayonnement 
         Alignment       =   2  'Center
         BackColor       =   &H00C0FFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2280
         TabIndex        =   102
         Tag             =   "0"
         Text            =   "10"
         ToolTipText     =   "0"
         Top             =   1920
         Width           =   630
      End
      Begin VB.TextBox TB_Nbr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         TabIndex        =   100
         Tag             =   "0"
         Text            =   "10"
         ToolTipText     =   "0"
         Top             =   1080
         Width           =   630
      End
      Begin VB.TextBox TB_EP_Apr�s 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         TabIndex        =   97
         Tag             =   "0"
         Text            =   "10"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   630
      End
      Begin VB.TextBox TB_EP_Avant 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         TabIndex        =   94
         Tag             =   "0"
         Text            =   "10"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   630
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Rayonnement :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   360
         TabIndex        =   103
         Tag             =   "407"
         ToolTipText     =   "0"
         Top             =   1920
         Width           =   1905
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Caption         =   "Nombre de coupes :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   120
         TabIndex        =   101
         Tag             =   "406"
         ToolTipText     =   "0"
         Top             =   1110
         Width           =   1905
      End
      Begin VB.Label Label7 
         BackColor       =   &H00C0C0C0&
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   2760
         TabIndex        =   99
         Tag             =   "405"
         ToolTipText     =   "0"
         Top             =   765
         Width           =   420
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Caption         =   "Epaisseur apr�s :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   120
         TabIndex        =   98
         Tag             =   "404"
         ToolTipText     =   "0"
         Top             =   750
         Width           =   1905
      End
      Begin VB.Label Label4 
         BackColor       =   &H00C0C0C0&
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   2760
         TabIndex        =   96
         Tag             =   "403"
         ToolTipText     =   "0"
         Top             =   405
         Width           =   420
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Caption         =   "Epaisseur avant :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   120
         TabIndex        =   95
         Tag             =   "402"
         ToolTipText     =   "0"
         Top             =   390
         Width           =   1905
      End
   End
   Begin VB.Frame Frame_Vitesse 
      Caption         =   "Vitesse"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   0
      TabIndex        =   85
      Tag             =   "100"
      ToolTipText     =   "0"
      Top             =   840
      Width           =   5655
      Begin VB.TextBox TB_Vitesse 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         TabIndex        =   89
         Tag             =   "0"
         Text            =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   1215
      End
      Begin VB.OptionButton Option_Vitesse 
         Caption         =   "choisie"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   88
         Tag             =   "108"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   3975
      End
      Begin VB.OptionButton Option_Vitesse 
         Caption         =   "maxi sans acc�l�ration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   87
         Tag             =   "106"
         ToolTipText     =   "0"
         Top             =   600
         Value           =   -1  'True
         Width           =   3975
      End
      Begin VB.OptionButton Option_Vitesse 
         Caption         =   "maxi avec acc�l�ration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   86
         Tag             =   "105"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   3975
      End
   End
   Begin MSComctlLib.StatusBar SB 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   84
      Top             =   9825
      Width           =   13260
      _ExtentX        =   23389
      _ExtentY        =   450
      Style           =   1
      SimpleText      =   "..."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton Cmd_Stop 
      BackColor       =   &H000000FF&
      Cancel          =   -1  'True
      Caption         =   "STOP"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   79
      Tag             =   "221"
      ToolTipText     =   "0"
      Top             =   9480
      Width           =   5685
   End
   Begin VB.Frame Frame_Chauffe 
      Caption         =   "Chauffe"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   0
      TabIndex        =   74
      Tag             =   "111"
      ToolTipText     =   "0"
      Top             =   8520
      Width           =   5685
      Begin VB.HScrollBar HScroll_Chauffe 
         Height          =   255
         LargeChange     =   10
         Left            =   840
         Max             =   100
         TabIndex        =   75
         Top             =   600
         Width           =   4170
      End
      Begin VB.Label Label_Chauffe 
         Alignment       =   2  'Center
         Caption         =   "Label_Chauffe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2640
         TabIndex        =   78
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label_PWM_AUTO 
         Alignment       =   2  'Center
         Caption         =   "Auto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3840
         TabIndex        =   77
         Tag             =   "112"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label_PWM_MANUAL 
         Alignment       =   2  'Center
         Caption         =   "Manuel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   840
         TabIndex        =   76
         Tag             =   "113"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame_Aide 
      Caption         =   "Aide"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   11280
      TabIndex        =   52
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   3120
      Width           =   1875
      Begin VB.CommandButton Cmd_Aide_Saignees 
         Caption         =   "Saign�es"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   150
         TabIndex        =   54
         Tag             =   "302"
         ToolTipText     =   "0"
         Top             =   735
         Width           =   1560
      End
      Begin VB.CommandButton Cmd_Aide_Chauffe 
         Caption         =   "Chauffe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   150
         TabIndex        =   53
         Tag             =   "301"
         ToolTipText     =   "0"
         Top             =   285
         Width           =   1560
      End
   End
   Begin VB.Frame Frame_Rayonnement 
      Caption         =   "Rayonnements"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Left            =   9585
      TabIndex        =   26
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   7440
      Width           =   3660
      Begin VB.CommandButton Cmd_Calculette 
         Height          =   615
         Left            =   3240
         Picture         =   "frmParamMateriau.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   92
         Tag             =   "0"
         ToolTipText     =   "tt400"
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox TB_Vitesse_DVH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2115
         TabIndex        =   91
         Tag             =   "0"
         Text            =   "6.00"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   465
      End
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   2115
         TabIndex        =   22
         Tag             =   "0"
         Text            =   "1.10"
         ToolTipText     =   "0"
         Top             =   390
         Width           =   465
      End
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   6
         Left            =   2115
         TabIndex        =   23
         Tag             =   "0"
         Text            =   "2.25"
         ToolTipText     =   "0"
         Top             =   1245
         Width           =   465
      End
      Begin VB.Label Label26 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Demi-vitesse DVH"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   105
         TabIndex        =   45
         Tag             =   "256"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   1785
      End
      Begin VB.Label Label35 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2520
         TabIndex        =   44
         Tag             =   "258"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   660
      End
      Begin VB.Label Label17 
         Alignment       =   2  'Center
         BackColor       =   &H0000C0C0&
         Caption         =   "Saign�e SH1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   105
         TabIndex        =   43
         Tag             =   "255"
         ToolTipText     =   "0"
         Top             =   405
         Width           =   1785
      End
      Begin VB.Label Label32 
         Alignment       =   2  'Center
         BackColor       =   &H0000C0C0&
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2520
         TabIndex        =   42
         Tag             =   "259"
         ToolTipText     =   "0"
         Top             =   390
         Width           =   660
      End
      Begin VB.Label Label37 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         Caption         =   "Saign�e SH2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   105
         TabIndex        =   41
         Tag             =   "257"
         ToolTipText     =   "0"
         Top             =   1260
         Width           =   1785
      End
      Begin VB.Label Label39 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2520
         TabIndex        =   40
         Tag             =   "259"
         ToolTipText     =   "0"
         Top             =   1245
         Width           =   660
      End
      Begin VB.Line Line6 
         Tag             =   "0"
         X1              =   2010
         X2              =   2010
         Y1              =   300
         Y2              =   1965
      End
   End
   Begin VB.Frame Frame_Droite 
      Caption         =   "Droite de chauffe id�ale"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Left            =   5760
      TabIndex        =   19
      Tag             =   "250"
      ToolTipText     =   "0"
      Top             =   7440
      Width           =   3660
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   2190
         TabIndex        =   83
         Tag             =   "0"
         Text            =   "6.00"
         ToolTipText     =   "0"
         Top             =   375
         Width           =   630
      End
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   2190
         TabIndex        =   82
         Tag             =   "0"
         Text            =   "100"
         ToolTipText     =   "0"
         Top             =   810
         Width           =   630
      End
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   2190
         TabIndex        =   81
         Tag             =   "0"
         Text            =   "2.00"
         ToolTipText     =   "0"
         Top             =   1200
         Width           =   630
      End
      Begin VB.TextBox TB_SAV 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   2190
         TabIndex        =   80
         Tag             =   "0"
         Text            =   "40"
         ToolTipText     =   "0"
         Top             =   1635
         Width           =   630
      End
      Begin VB.Label Label_Chauffe_CB 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2835
         TabIndex        =   49
         Tag             =   "260"
         ToolTipText     =   "tt260"
         Top             =   1635
         Width           =   690
      End
      Begin VB.Label Label_Vitesse_VB 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2850
         TabIndex        =   48
         Tag             =   "258"
         ToolTipText     =   "tt258"
         Top             =   1230
         Width           =   675
      End
      Begin VB.Label Label52 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "Chauffe CB"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   180
         TabIndex        =   47
         Tag             =   "254"
         ToolTipText     =   "0"
         Top             =   1665
         Width           =   1560
      End
      Begin VB.Label Label53 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         Caption         =   "Vitesse VB"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   165
         TabIndex        =   46
         Tag             =   "253"
         ToolTipText     =   "0"
         Top             =   1245
         Width           =   1590
      End
      Begin VB.Label Label_Chauffe_CH 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2835
         TabIndex        =   25
         Tag             =   "260"
         ToolTipText     =   "tt260"
         Top             =   810
         Width           =   690
      End
      Begin VB.Label Label_Vitesse_VH 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C000&
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2850
         TabIndex        =   24
         Tag             =   "258"
         ToolTipText     =   "tt258"
         Top             =   405
         Width           =   675
      End
      Begin VB.Line Line3 
         Tag             =   "0"
         X1              =   2010
         X2              =   2010
         Y1              =   300
         Y2              =   2040
      End
      Begin VB.Label Label31 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         Caption         =   "Chauffe CH"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   180
         TabIndex        =   21
         Tag             =   "252"
         ToolTipText     =   "0"
         Top             =   840
         Width           =   1560
      End
      Begin VB.Label Label25 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C000&
         Caption         =   "Vitesse VH"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   165
         TabIndex        =   20
         Tag             =   "251"
         ToolTipText     =   "0"
         Top             =   420
         Width           =   1590
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6255
      Left            =   0
      TabIndex        =   3
      Tag             =   "101"
      ToolTipText     =   "0"
      Top             =   2280
      Width           =   5685
      _ExtentX        =   10028
      _ExtentY        =   11033
      _Version        =   393216
      Tab             =   1
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Blocs"
      TabPicture(0)   =   "frmParamMateriau.frx":067A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "TB_TEMP(0)"
      Tab(0).Control(1)=   "Cmd_Bloc(0)"
      Tab(0).Control(2)=   "Cmd_Bloc(1)"
      Tab(0).Control(3)=   "Line2(4)"
      Tab(0).Control(4)=   "Line1(5)"
      Tab(0).Control(5)=   "Line1(0)"
      Tab(0).Control(6)=   "Line2(2)"
      Tab(0).Control(7)=   "Line2(3)"
      Tab(0).Control(8)=   "Label20"
      Tab(0).Control(9)=   "Label19"
      Tab(0).Control(10)=   "Label43"
      Tab(0).ControlCount=   11
      TabCaption(1)   =   "Tranches"
      TabPicture(1)   =   "frmParamMateriau.frx":0696
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame_Tranches"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame_Mont�e"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Test"
      TabPicture(2)   =   "frmParamMateriau.frx":06B2
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Image4"
      Tab(2).Control(1)=   "Image8"
      Tab(2).Control(2)=   "Label15"
      Tab(2).Control(3)=   "Label16"
      Tab(2).Control(4)=   "Label45"
      Tab(2).Control(5)=   "Label46"
      Tab(2).Control(6)=   "Image9"
      Tab(2).Control(7)=   "Label51"
      Tab(2).Control(8)=   "TB_TEMP(11)"
      Tab(2).Control(9)=   "TB_TEMP(10)"
      Tab(2).Control(10)=   "Cmd_Decoupe_Test"
      Tab(2).Control(11)=   "Timer_Test"
      Tab(2).ControlCount=   12
      Begin VB.Timer Timer_Test 
         Interval        =   50
         Left            =   -70680
         Top             =   3960
      End
      Begin VB.Frame Frame_Mont�e 
         Caption         =   "Mont�e / Descente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2370
         Left            =   120
         TabIndex        =   65
         Tag             =   "202"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   5445
         Begin VB.CommandButton Cmd_Mont�e_Descente 
            BackColor       =   &H00E0E0E0&
            Height          =   1365
            Index           =   0
            Left            =   210
            Picture         =   "frmParamMateriau.frx":06CE
            Style           =   1  'Graphical
            TabIndex        =   70
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   615
            Width           =   900
         End
         Begin VB.TextBox TB_TEMP 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   1185
            TabIndex        =   69
            Tag             =   "0"
            Text            =   "200"
            ToolTipText     =   "0"
            Top             =   1125
            Width           =   660
         End
         Begin VB.CheckBox CB_TEMP 
            Caption         =   "Origine"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   4095
            TabIndex        =   68
            Tag             =   "208"
            ToolTipText     =   "0"
            Top             =   1650
            Width           =   1110
         End
         Begin VB.TextBox TB_TEMP 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   5
            Left            =   3570
            TabIndex        =   67
            Tag             =   "0"
            Text            =   "100"
            ToolTipText     =   "0"
            Top             =   1125
            Width           =   660
         End
         Begin VB.CommandButton Cmd_Mont�e_Descente 
            BackColor       =   &H00E0E0E0&
            Height          =   960
            Index           =   1
            Left            =   2655
            Picture         =   "frmParamMateriau.frx":20D8
            Style           =   1  'Graphical
            TabIndex        =   66
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   1080
            Width           =   840
         End
         Begin VB.Label Label6 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1950
            TabIndex        =   73
            Tag             =   "207"
            Top             =   1155
            Width           =   360
         End
         Begin VB.Image Image2 
            Height          =   1920
            Left            =   135
            Picture         =   "frmParamMateriau.frx":33AA
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   315
            Width           =   1050
         End
         Begin VB.Label Label64 
            Alignment       =   1  'Right Justify
            Caption         =   "ou"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   3525
            TabIndex        =   72
            Tag             =   "205"
            ToolTipText     =   "0"
            Top             =   1665
            Width           =   435
         End
         Begin VB.Label Label65 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   4335
            TabIndex        =   71
            Tag             =   "207"
            Top             =   1245
            Width           =   360
         End
         Begin VB.Image Image7 
            Height          =   1920
            Left            =   2550
            Picture         =   "frmParamMateriau.frx":61F4
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   315
            Width           =   1050
         End
         Begin VB.Line Line2 
            Index           =   1
            Tag             =   "0"
            X1              =   2460
            X2              =   2460
            Y1              =   360
            Y2              =   2250
         End
      End
      Begin VB.TextBox TB_TEMP 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   -72720
         TabIndex        =   62
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   750
      End
      Begin VB.CommandButton Cmd_Bloc 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1785
         Index           =   0
         Left            =   -74640
         Picture         =   "frmParamMateriau.frx":903E
         Style           =   1  'Graphical
         TabIndex        =   61
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1560
         Width           =   2010
      End
      Begin VB.CommandButton Cmd_Bloc 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1785
         Index           =   1
         Left            =   -71880
         Picture         =   "frmParamMateriau.frx":9627
         Style           =   1  'Graphical
         TabIndex        =   60
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1560
         Width           =   2010
      End
      Begin VB.CommandButton Cmd_Decoupe_Test 
         Caption         =   "Lancer la d�coupe"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   -70800
         TabIndex        =   13
         Tag             =   "237"
         ToolTipText     =   "0"
         Top             =   5280
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.TextBox TB_TEMP 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         Left            =   -74340
         TabIndex        =   11
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4470
         Width           =   615
      End
      Begin VB.TextBox TB_TEMP 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   11
         Left            =   -72645
         TabIndex        =   12
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4470
         Width           =   645
      End
      Begin VB.Frame Frame_Tranches 
         Caption         =   "Tranches"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2595
         Left            =   120
         TabIndex        =   14
         Tag             =   "210"
         ToolTipText     =   "0"
         Top             =   2760
         Width           =   5445
         Begin VB.TextBox TB_TEMP 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   13
            Left            =   3960
            TabIndex        =   9
            Tag             =   "0"
            Text            =   "10"
            ToolTipText     =   "0"
            Top             =   450
            Width           =   510
         End
         Begin VB.CommandButton Cmd_XTranche 
            BackColor       =   &H00E0E0E0&
            Height          =   900
            Left            =   3990
            Picture         =   "frmParamMateriau.frx":9C0D
            Style           =   1  'Graphical
            TabIndex        =   10
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   870
            Width           =   1035
         End
         Begin VB.CommandButton Cmd_1Tranche 
            BackColor       =   &H00E0E0E0&
            Height          =   750
            Index           =   1
            Left            =   2760
            Picture         =   "frmParamMateriau.frx":AEAF
            Style           =   1  'Graphical
            TabIndex        =   8
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   1365
            Width           =   1035
         End
         Begin VB.TextBox TB_TEMP 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   8
            Left            =   900
            TabIndex        =   4
            Tag             =   "0"
            Text            =   "10"
            ToolTipText     =   "0"
            Top             =   1050
            Width           =   750
         End
         Begin VB.TextBox TB_TEMP 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   9
            Left            =   900
            TabIndex        =   5
            Tag             =   "0"
            Text            =   "100"
            ToolTipText     =   "0"
            Top             =   1515
            Width           =   750
         End
         Begin VB.CommandButton Cmd_1Tranche 
            BackColor       =   &H00E0E0E0&
            Height          =   600
            Index           =   0
            Left            =   2745
            Picture         =   "frmParamMateriau.frx":BEA9
            Style           =   1  'Graphical
            TabIndex        =   7
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   435
            Width           =   1035
         End
         Begin VB.CheckBox CB_TEMP 
            Caption         =   "Pause apr�s x"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   5
            Left            =   105
            TabIndex        =   6
            Tag             =   "218"
            ToolTipText     =   "0"
            Top             =   2160
            Width           =   2625
         End
         Begin VB.Label Label41 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "AR"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4500
            TabIndex        =   50
            Tag             =   "109"
            ToolTipText     =   "0"
            Top             =   465
            Width           =   720
         End
         Begin VB.Image Image6 
            Height          =   2400
            Left            =   3960
            Picture         =   "frmParamMateriau.frx":CC3F
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   120
            Width           =   1050
         End
         Begin VB.Image Image3 
            Height          =   2400
            Left            =   2730
            Picture         =   "frmParamMateriau.frx":10509
            Tag             =   "0"
            ToolTipText     =   "0"
            Top             =   120
            Width           =   1050
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "x ="
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   300
            TabIndex        =   18
            Tag             =   "215"
            ToolTipText     =   "0"
            Top             =   1080
            Width           =   465
         End
         Begin VB.Label Label8 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   1740
            TabIndex        =   17
            Tag             =   "217"
            ToolTipText     =   "0"
            Top             =   1095
            Width           =   420
         End
         Begin VB.Label Label9 
            Alignment       =   1  'Right Justify
            Caption         =   "y ="
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   300
            TabIndex        =   16
            Tag             =   "216"
            ToolTipText     =   "0"
            Top             =   1485
            Width           =   450
         End
         Begin VB.Label Label10 
            Caption         =   "mm"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1740
            TabIndex        =   15
            Tag             =   "217"
            ToolTipText     =   "0"
            Top             =   1530
            Width           =   390
         End
         Begin VB.Line Line1 
            Index           =   1
            Tag             =   "0"
            X1              =   180
            X2              =   2385
            Y1              =   900
            Y2              =   900
         End
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Explications"
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   120
         TabIndex        =   90
         Tag             =   "225"
         ToolTipText     =   "0"
         Top             =   5400
         Width           =   5445
      End
      Begin VB.Label Label51 
         BackColor       =   &H80000000&
         Caption         =   $"frmParamMateriau.frx":13DD3
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   -74880
         TabIndex        =   55
         Tag             =   "231"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   5190
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00800000&
         BorderWidth     =   2
         Index           =   4
         X1              =   -72240
         X2              =   -72240
         Y1              =   1440
         Y2              =   3480
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00800000&
         BorderWidth     =   2
         Index           =   5
         X1              =   -74880
         X2              =   -69480
         Y1              =   3480
         Y2              =   3480
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00800000&
         BorderWidth     =   2
         Index           =   0
         X1              =   -74880
         X2              =   -69480
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00800000&
         BorderWidth     =   2
         Index           =   2
         X1              =   -69480
         X2              =   -69480
         Y1              =   1440
         Y2              =   3480
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00800000&
         BorderWidth     =   2
         Index           =   3
         X1              =   -74880
         X2              =   -74880
         Y1              =   1440
         Y2              =   3480
      End
      Begin VB.Label Label20 
         Alignment       =   2  'Center
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -71880
         TabIndex        =   64
         Tag             =   "104"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   495
      End
      Begin VB.Label Label19 
         Alignment       =   2  'Center
         Caption         =   "Y ="
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -73200
         TabIndex        =   63
         Tag             =   "103"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   495
      End
      Begin VB.Image Image9 
         Height          =   1740
         Left            =   -71415
         Picture         =   "frmParamMateriau.frx":13E7E
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   2040
         Width           =   1500
      End
      Begin VB.Label Label46 
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -73080
         TabIndex        =   59
         Tag             =   "234"
         Top             =   5250
         Width           =   360
      End
      Begin VB.Label Label45 
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -71355
         TabIndex        =   58
         Tag             =   "234"
         Top             =   5250
         Width           =   360
      End
      Begin VB.Label Label16 
         BackColor       =   &H80000000&
         Caption         =   "Indiquer la distance entre le chariot et l'emplanture et la distance entre l'emplanture et le saumon :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   -74835
         TabIndex        =   57
         Tag             =   "233"
         ToolTipText     =   "0"
         Top             =   3840
         Width           =   5205
      End
      Begin VB.Label Label15 
         Caption         =   "L'emplanture et le saumon d�coup�s doivent parfaitement s'emboiter :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1005
         Left            =   -71730
         TabIndex        =   56
         Tag             =   "232"
         ToolTipText     =   "0"
         Top             =   1080
         Width           =   2130
      End
      Begin VB.Image Image8 
         Height          =   1365
         Left            =   -74940
         Picture         =   "frmParamMateriau.frx":1C6B0
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   4680
         Width           =   4110
      End
      Begin VB.Image Image4 
         Height          =   2610
         Left            =   -74850
         Picture         =   "frmParamMateriau.frx":1CFE2
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1080
         Width           =   3000
      End
      Begin VB.Label Label43 
         BackColor       =   &H80000000&
         Caption         =   "L'outil ci-dessous est une guillotine verticale pour pr�parer vos blocs de mati�re pour les tests."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   540
         Left            =   -74820
         TabIndex        =   51
         Tag             =   "102"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   5160
      End
   End
   Begin VB.Frame Frame_Matiere 
      Caption         =   "Mati�re"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   780
      Left            =   0
      TabIndex        =   0
      Tag             =   "2"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   13185
      Begin VB.CommandButton cmdGestion 
         Caption         =   "Supprimer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   8340
         TabIndex        =   29
         Tag             =   "6"
         ToolTipText     =   "0"
         Top             =   255
         Width           =   1425
      End
      Begin VB.CommandButton cmdGestion 
         Caption         =   "Renommer"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   6900
         TabIndex        =   28
         Tag             =   "5"
         ToolTipText     =   "0"
         Top             =   255
         Width           =   1425
      End
      Begin VB.CommandButton cmdGestion 
         Caption         =   "Copier"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   5460
         TabIndex        =   27
         Tag             =   "4"
         ToolTipText     =   "0"
         Top             =   255
         Width           =   1425
      End
      Begin VB.ComboBox Combo_Mat 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   180
         TabIndex        =   2
         Tag             =   "1"
         ToolTipText     =   "0"
         Top             =   270
         Width           =   3465
      End
      Begin VB.CommandButton Cmd_BD 
         Caption         =   "Base de donn�es"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   10560
         TabIndex        =   1
         Tag             =   "7"
         ToolTipText     =   "0"
         Top             =   255
         Width           =   2355
      End
   End
   Begin VB.Label LblDessin 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "SH2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Index           =   6
      Left            =   8955
      TabIndex        =   39
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   1815
      Width           =   645
   End
   Begin VB.Label LblDessin 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "SH1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Index           =   5
      Left            =   10875
      TabIndex        =   38
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   2040
      Width           =   645
   End
   Begin VB.Label LblDessin 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FF8080&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "VB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Index           =   3
      Left            =   7365
      TabIndex        =   37
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   6210
      Width           =   495
   End
   Begin VB.Label LblDessin 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Index           =   4
      Left            =   5760
      TabIndex        =   36
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   4905
      Width           =   495
   End
   Begin VB.Label LblDessin 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000080FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CH"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Index           =   2
      Left            =   5775
      TabIndex        =   35
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   2115
      Width           =   495
   End
   Begin VB.Label Label59 
      Alignment       =   2  'Center
      Caption         =   "Saign�e (mm)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6735
      TabIndex        =   34
      Tag             =   "242"
      ToolTipText     =   "0"
      Top             =   2520
      Width           =   1110
   End
   Begin VB.Label Label58 
      Caption         =   "Chauffe (%)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6645
      TabIndex        =   33
      Tag             =   "241"
      ToolTipText     =   "0"
      Top             =   1530
      Width           =   1020
   End
   Begin VB.Label Label44 
      Caption         =   "Vitesse (mm/s)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10950
      TabIndex        =   32
      Tag             =   "243"
      ToolTipText     =   "0"
      Top             =   5475
      Width           =   1020
   End
   Begin VB.Label LblDessin 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "VH"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Index           =   1
      Left            =   10320
      TabIndex        =   31
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   6210
      Width           =   495
   End
   Begin VB.Label Label36 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFF80&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "DVH"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   8280
      TabIndex        =   30
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   6210
      Width           =   645
   End
   Begin VB.Image Image5 
      Height          =   4935
      Left            =   6225
      Picture         =   "frmParamMateriau.frx":2037F
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   1380
      Width           =   5415
   End
End
Attribute VB_Name = "frmParamMateriau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim AncienMat As String
Dim Vitesse As Single
Dim bTempo As Boolean   'Doit-on appliquer la tempo de la chauffe ?
'Sert pour le calcul de la chauffe
Dim CH As Double, CB As Double, VH As Double, VB As Double
Dim SH1 As Single, SH2 As Single
Dim chauffe As Double
Dim monTemps As Long        'Sert pour la temporisation
Dim bInfo                   'Dans le timer, doit-on envoyer la demande d'info ?

Private Sub Cmd_1Tranche_Click(Index As Integer)
    Dim x As Single
    Dim y As Single
    Dim Tps As Single
    
    x = CDbl(TB_TEMP(8))
    y = CDbl(TB_TEMP(9))
    
    If y = 0# Or x = 0# Or Vitesse = 0# Or Not maClasseHID.IsAvailable Then
        Beep
        Exit Sub
    End If
    
    gPause = False
    DeplEnCours = True
    Call Cmd_Enable(False)
    
    bInfo = False
    If Label_PWM_AUTO.Visible Then   'Chauffe auto
        chauffe = Min(Calcul_Fil(gstrMat, Vitesse, CH, CB, VH, VB, SH1, SH2), 100)
        bTempo = True
    Else
        chauffe = Val(Label_Chauffe)
        bTempo = False
    End If
    Label_Chauffe = chauffe
    HScroll_Chauffe.value = chauffe
    
    'Envoi consigne de chauffe
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrire(Round(chauffe * 255 / 100, 0), bTempo)

    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 303)
    ReDim Tab_Chariot(1 To 1)
    If Index = 1 Then   'Descente avant tranche
        Tab_Chariot(1).Temps = Calcul_Temps_Distance(y, Vitesse)
        Tab_Chariot(1).P_abs(EMPLANTURE).x = 0#
        Tab_Chariot(1).P_abs(EMPLANTURE).y = -y
        Tab_Chariot(1).P_abs(SAUMON).x = 0#
        Tab_Chariot(1).P_abs(SAUMON).y = -y
'        Call maClasseHID.CommandeDATAmm(0#, -y, 0#, -y, Vitesse, Tps)
        ReDim Preserve Tab_Chariot(1 To 2)
    End If
    Tab_Chariot(UBound(Tab_Chariot)).Temps = Calcul_Temps_Distance(x, Vitesse)
    Tab_Chariot(UBound(Tab_Chariot)).P_abs(EMPLANTURE).x = x
    Tab_Chariot(UBound(Tab_Chariot)).P_abs(EMPLANTURE).y = 0#
    Tab_Chariot(UBound(Tab_Chariot)).P_abs(SAUMON).x = x
    Tab_Chariot(UBound(Tab_Chariot)).P_abs(SAUMON).y = 0#
    Call Envoi_Decoupe(Tab_Chariot, Me)
    
    bTempo = False
    If CB_TEMP(5).value = vbChecked Then 'Pause apr�s trajet x
        'Pause de 3 secondes
        'On lance le timer
        SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 302)
        monTemps = Hour(Now) * 3600 + Minute(Now) * 60 + Second(Now)
        While (Hour(Now) * 3600 + Minute(Now) * 60 + Second(Now)) < monTemps + 3
            Call Timer_Test_Timer
            DoEvents
        Wend
        bTempo = True
    End If

    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 303)
    'Envoi consigne de chauffe
    Call maClasseHID.ChauffeEcrire(Round(chauffe * 255 / 100, 0), bTempo)
    ReDim Preserve Tab_Chariot(1 To 2)
    Tab_Chariot(1).Temps = Calcul_Temps_Distance(y, Vitesse)
    Tab_Chariot(1).P_abs(EMPLANTURE).x = 0#
    Tab_Chariot(1).P_abs(EMPLANTURE).y = -y
    Tab_Chariot(1).P_abs(SAUMON).x = 0#
    Tab_Chariot(1).P_abs(SAUMON).y = -y
    Tab_Chariot(2).Temps = Calcul_Temps_Distance(x, Vitesse)
    Tab_Chariot(2).P_abs(EMPLANTURE).x = -x
    Tab_Chariot(2).P_abs(EMPLANTURE).y = 0#
    Tab_Chariot(2).P_abs(SAUMON).x = -x
    Tab_Chariot(2).P_abs(SAUMON).y = 0#
    Call Envoi_Decoupe(Tab_Chariot, Me)
    
    'Envoi consigne de chauffe = 0
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrire(0, bTempo)
    
    Label_Chauffe = 0
    HScroll_Chauffe.value = 0
    Call Cmd_Enable(True)
    bInfo = True
    DeplEnCours = False
    SB.SimpleText = "..."
End Sub

Private Sub Cmd_BD_Click()
    Call Sav_Mat(AncienMat)
    frmBD.Show vbModal
End Sub

Private Sub Cmd_Bloc_Click(Index As Integer)
    Dim y As Single
    Dim Tps As Single
    
    y = CDbl(TB_TEMP(0)) * IIf(Index = 0, 1, -1)
    
    If y = 0# Or Vitesse = 0# Or Not maClasseHID.IsAvailable Then
        Beep
        Exit Sub
    End If
    
    gPause = False
    DeplEnCours = True
    bInfo = False
    Call Cmd_Enable(False)
    
    If Label_PWM_AUTO.Visible Then   'Chauffe auto
        chauffe = Min(Calcul_Fil(gstrMat, Vitesse, CH, CB, VH, VB, SH1, SH2), 100)
        bTempo = True
    Else
        chauffe = Val(Label_Chauffe)
        bTempo = False
    End If
    Label_Chauffe = chauffe
    HScroll_Chauffe.value = chauffe
    
    'Envoi consigne de chauffe
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrire(Round(chauffe * 255 / 100, 0), bTempo)

    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 303)
    ReDim Tab_Chariot(1 To 1)
    Tab_Chariot(1).Temps = Calcul_Temps_Distance(y, Vitesse)
    Tab_Chariot(1).P_abs(EMPLANTURE).x = 0#
    Tab_Chariot(1).P_abs(EMPLANTURE).y = y
    Tab_Chariot(1).P_abs(SAUMON).x = 0#
    Tab_Chariot(1).P_abs(SAUMON).y = y
    Call Envoi_Decoupe(Tab_Chariot, Me)
    
    'Envoi consigne de chauffe
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrire(0, bTempo)
    
    Call Cmd_Enable(True)
    Label_Chauffe = 0
    HScroll_Chauffe.value = 0
    bInfo = True
    DeplEnCours = False
    SB.SimpleText = "..."
End Sub

'Private Sub Cmd_Decoupe_Test_Click()
'    Dim k As Integer
'    Dim Tab_Ls() As ElemDecoupe
'    Dim Course_X As Single
'    Dim Course_Y As Single
'    Dim Tps As Double
'    Dim Offset_Droit As Single
'    Dim monElem As TypeNoeud
'
'    If Vitesse = 0# Or Not maClasseHID.IsAvailable Then
'        Beep
'        Exit Sub
'    End If
'
'    bInfo = False
'    Call Cmd_Enable(False)
'    Label_Chauffe = 0
'    HScroll_Chauffe.value = 0
'    If Label_PWM_AUTO.Visible Then   'Chauffe auto
'        chauffe = Min(Calcul_Fil(gstrMat, Vitesse, CH, CB, VH, VB, SH1, SH2), 100)
'    Else
'        chauffe = Val(Label_Chauffe)
'    End If
'
'    'Listes cha�n�es des points
'    k = EMPLANTURE
'    Set gListePointSynchro(k) = New ClsListeChainee
'    monElem.x = 90
'    monElem.y = 0
'    monElem.Vitesse = Vitesse
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 90
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 19.125
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 78.75
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 67.5
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 19.125
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 56.25
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 45
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 19.125
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 33.75
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 22.5
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 19.125
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 11.25
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 0
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 0
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 11.25
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -19.125
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 22.5
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 33.75
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -19.125
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 45
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 56.25
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -19.125
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 67.5
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 78.75
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -19.125
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 90
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -7.875
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 0
'    gListePointSynchro(k).Insert monElem
'
'    k = SAUMON
'    Set gListePointSynchro(k) = New ClsListeChainee
'    monElem.x = 50
'    monElem.y = 0
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 4.375
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 10.625
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 16.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 43.75
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 37.5
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 10.625
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 4.375
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 31.25
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 25
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 10.625
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 16.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 18.75
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 12.5
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 10.625
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 4.375
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 6.25
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 0
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 0
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -4.375
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 6.25
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 12.5
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -10.625
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -16.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 18.75
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 25
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -10.625
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -4.375
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 31.25
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 37.5
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -10.625
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -16.875
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 43.75
'    gListePointSynchro(k).Insert monElem
'    monElem.x = 50
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -10.625
'    gListePointSynchro(k).Insert monElem
'    monElem.y = -4.375
'    gListePointSynchro(k).Insert monElem
'    monElem.y = 0
'    gListePointSynchro(k).Insert monElem
'
'    'Calcul de la trajectoire des chariots
'    Offset_Droit = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_23", 0) - _
'        CDbl(TB_TEMP(10)) - CDbl(TB_TEMP(11))
'    Tab_Ls = Calcul_Trajectoire(gListePointSynchro, CDbl(TB_TEMP(10)), _
'        Offset_Droit, CDbl(TB_TEMP(11)), Vitesse, gstrTable, Gauche)
'    'Contr�le de la trajectoire des chariots
'    Course_X = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_16", 0)
'    Course_Y = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_17", 0)
'    'Call Ctrl_Trajectoire(Tab_Ls, Course_X, Course_Y, 0#, 19.125, Vitesse)
'    'Lancement d�coupe
'    bInfo = False
'    Call Envoi_Decoupe(Tab_Ls, Me)
'
'    Label_Chauffe = 0
'    HScroll_Chauffe.value = 0
'    Call Cmd_Enable(True)
'    bInfo = True
'End Sub

Private Sub Cmd_Calculette_Click()
    Frame_Calculette.Visible = True
    TB_Nbr = TB_TEMP(13) * 2
End Sub

Private Sub Cmd_Copier_SH_Click(Index As Integer)
    Frame_Calculette.Visible = False
    If Index = 1 Then
        TB_SAV(5) = TB_Rayonnement
    Else
        TB_SAV(6) = TB_Rayonnement
    End If
End Sub

Private Sub Cmd_Mont�e_Descente_Click(Index As Integer)
    Dim y As Single
    Dim Tps As Single
    
'    y = CDbl(TB_TEMP(3)) * IIf(Index = 0, 1, -1)
    If Index = 0 Then
        y = CDbl(TB_TEMP(3))
    Else
        y = -CDbl(TB_TEMP(5))
    End If
    
    If y = 0# Or Vitesse = 0# Or Not maClasseHID.IsAvailable Then
        Beep
        Exit Sub
    End If
        
    Call Cmd_Enable(False)
    DeplEnCours = True
    
    bInfo = False
    gPause = False
    If Label_PWM_AUTO.Visible Then   'Chauffe auto
        chauffe = Min(Calcul_Fil(gstrMat, Vitesse, CH, CB, VH, VB, SH1, SH2), 100)
        bTempo = True
    Else
        chauffe = Val(Label_Chauffe)
        bTempo = False
    End If
    Label_Chauffe = chauffe
    HScroll_Chauffe.value = chauffe

    'Envoi consigne de chauffe
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrire(Round(chauffe * 255 / 100, 0), bTempo)

    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 303)
    ReDim Tab_Chariot(1 To 1)
    Tab_Chariot(1).Temps = Calcul_Temps_Distance(y, Vitesse)
    Tab_Chariot(1).P_abs(EMPLANTURE).x = 0#
    Tab_Chariot(1).P_abs(EMPLANTURE).y = y
    Tab_Chariot(1).P_abs(SAUMON).x = 0#
    Tab_Chariot(1).P_abs(SAUMON).y = y
    Call Envoi_Decoupe(Tab_Chariot, Me)
    
    'Envoi consigne de chauffe
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrire(0, bTempo)
    
    Label_Chauffe = 0
    HScroll_Chauffe.value = 0
    Call Cmd_Enable(True)
    bInfo = True
    DeplEnCours = False
    SB.SimpleText = "..."
End Sub

Private Sub Cmd_Stop_Click()
    gPause = True
    Call maClasseHID.ChauffeEcrire(0, False)
    gPause = True
    Call Cmd_Enable(True)
End Sub

Private Sub Cmd_XTranche_Click()
    Dim x As Single, y As Single
    Dim Tps As Double
    Dim maVitMaxY As Single
    
    If Val(TB_TEMP(13)) = 0 Or Not maClasseHID.IsAvailable Then
        Beep
        Exit Sub
    End If
    x = CDbl(TB_TEMP(8))
    y = CDbl(TB_TEMP(9))
    chauffe = Val(Label_Chauffe)
    
    If y = 0# Or x = 0# Or Vitesse = 0# Then
        Beep
        Exit Sub
    End If
    
    maVitMaxY = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 3)
    
    gPause = False
    DeplEnCours = True
    bInfo = False
    Call Cmd_Enable(False)
    If Label_PWM_AUTO.Visible Then   'Chauffe auto
        chauffe = Min(Calcul_Fil(gstrMat, Vitesse, CH, CB, VH, VB, SH1, SH2), 100)
        bTempo = True
    Else
        chauffe = Val(Label_Chauffe)
        bTempo = False
    End If
    Label_Chauffe = chauffe
    HScroll_Chauffe.value = chauffe
    'Envoi consigne de chauffe
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrire(Round(chauffe * 255 / 100, 0), bTempo)

    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 303)
    
    While Val(TB_TEMP(13)) <> 0
        TB_TEMP(13) = Val(TB_TEMP(13)) - 1
        ReDim Preserve Tab_Chariot(1 To 2)
        Tab_Chariot(1).Temps = Calcul_Temps_Distance(x, Vitesse)
        Tab_Chariot(1).P_abs(EMPLANTURE).x = x
        Tab_Chariot(1).P_abs(EMPLANTURE).y = 0#
        Tab_Chariot(1).P_abs(SAUMON).x = x
        Tab_Chariot(1).P_abs(SAUMON).y = 0#
        
        Tab_Chariot(2).Temps = Calcul_Temps_Distance(y, maVitMaxY)
        Tab_Chariot(2).P_abs(EMPLANTURE).x = 0#
        Tab_Chariot(2).P_abs(EMPLANTURE).y = -y
        Tab_Chariot(2).P_abs(SAUMON).x = 0#
        Tab_Chariot(2).P_abs(SAUMON).y = -y
        'Envoi consigne de chauffe
        Call maClasseHID.ChauffeEcrire(Round(chauffe * 255 / 100, 0), False)
        Call Envoi_Decoupe(Tab_Chariot, Me)
        
        bTempo = False
        If CB_TEMP(5).value = vbChecked Then 'Pause apr�s trajet x
            'Pause de 3 secondes
            'On lance le timer
            SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 302)
            monTemps = Hour(Now) * 3600 + Minute(Now) * 60 + Second(Now)
            While (Hour(Now) * 3600 + Minute(Now) * 60 + Second(Now)) < monTemps + 3
                Call Timer_Test_Timer
                DoEvents
            Wend
            bTempo = True
        End If
        
        'Envoi consigne de chauffe
        Call maClasseHID.ChauffeEcrire(Round(chauffe * 255 / 100, 0), bTempo)
        Tab_Chariot(1).Temps = Calcul_Temps_Distance(x, Vitesse)
        Tab_Chariot(1).P_abs(EMPLANTURE).x = -x
        Tab_Chariot(1).P_abs(EMPLANTURE).y = 0#
        Tab_Chariot(1).P_abs(SAUMON).x = -x
        Tab_Chariot(1).P_abs(SAUMON).y = 0#
        
        ' l'�l�ment 2 est d�j� rempli !
        Call Envoi_Decoupe(Tab_Chariot, Me)
    Wend

    Label_Chauffe = 0
    HScroll_Chauffe.value = 0
    'Envoi consigne de chauffe = 0
    SB.SimpleText = ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 304)
    Call maClasseHID.ChauffeEcrire(0, bTempo)
    Call Cmd_Enable(True)
    bInfo = True
    DeplEnCours = False
    SB.SimpleText = "..."
End Sub

Private Sub cmdGestion_Click(Index As Integer)
    Dim NouveauNom As String
    Dim NomINI As String
    Dim Section() As String
    
    If Index = 1 Or Index = 2 Then       'Copier ou renommer
        NouveauNom = SaisieNom
        If NouveauNom = "" Then Exit Sub
        If NouveauNom = Combo_Mat.Text Then Exit Sub
    End If
    
    NomINI = App.Path & "\" & App.Title & ".ini"
    Select Case Index
        Case 1  'Copier
            Sav_Mat (Combo_Mat.Text)
            Call CopySectionIni("mat_" & Combo_Mat.Text, "mat_" & NouveauNom, NomINI)
            Call WriteINI("MatDefaut", NouveauNom)
        Case 2  'Renommer
            Sav_Mat (Combo_Mat.Text)
            Call RenameSectionIni("mat_" & Combo_Mat.Text, "mat_" & NouveauNom, NomINI)
            Call WriteINI("MatDefaut", NouveauNom)
        Case 3  'Supprimer
            If MsgBox(ReadINI_Trad(gLangue, "erreurs", 51) & " " & _
                Combo_Mat.Text & " ?", vbCritical + vbQuestion + vbOKCancel, App.Title) = vbOK Then
                Call DeleteSectionIni("mat_" & Combo_Mat.Text, NomINI)
                AncienMat = ""
                Combo_Mat.ListIndex = 0
            End If
    End Select
    
    Call Chargt_Materiau(Combo_Mat)
End Sub

Private Sub Combo_Mat_Click()
    'Sauvegarde de l'ancien mat�riau
    If AncienMat <> "" Then
        If Not Sav_Mat(AncienMat) Then Exit Sub
    End If
    'Chargement du nouveau
    Call Chargt_Mat
    Me.Caption = Combo_Mat.Text
    AncienMat = Combo_Mat.Text
    gstrMat = Combo_Mat.Text
    'Ecriture mat�riau par d�faut
    Call WriteINI("MatDefaut", gstrMat)
End Sub

Public Sub Form_Load()
    ChargeTrad Me
    Me.Left = ReadINI_Num(Me.Name & "_Left", Me.Left)
    Me.Top = ReadINI_Num(Me.Name & "_Top", Me.Top)
    Dim i As Integer
    
    Set gFenetreHID = frmParamMateriau
    
    AncienMat = ""
    SSTab1.Tab = 1
    Call Chargt_Materiau(Combo_Mat)
    Call Chargt_Mat
    
    On Error Resume Next
    For i = TB_TEMP.LBound To TB_TEMP.UBound
        TB_TEMP(i) = ReadINI_Num("TB_TEMP_" & i, 0)
    Next i
    On Error GoTo 0
    
    'Lecture de la table en cours dans l'interface
    gstrTable = ReadINI("TableDefaut", "MM2001")
    maClasseHID.TableVerif
    
'    'Chauffe
'    chauffe = maClasseHID.ChauffeLire
'    Me.HScroll_Chauffe.value = Asc(Mid$(chauffe, 3, 1))

    Option_Vitesse_Click (1)   'Maxi sans acc�l�ration
    
    bInfo = True
End Sub

Private Function SaisieNom() As String
    Dim Machaine As String
    SaisieNom = "NewMaterial_"
    While Len(SaisieNom) = 0 Or Len(SaisieNom) > 32 Or SaisieNom = "NewMaterial_"
        If SaisieNom <> "NewMaterial_" Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 54), vbCritical
        End If
        Machaine = InputBox(ReadINI_Trad(gLangue, "erreurs", 52), Combo_Mat.Text, SaisieNom)
        If Machaine = "" Then Exit Function
        SaisieNom = Machaine
    Wend
End Function

Private Function Sav_Mat(gstrMat As String) As Boolean
    Dim ctl As Control
    Dim monNom As String
    
    On Error Resume Next
   
    For Each ctl In Me.Controls
        If ctl.Name = "TB_SAV" Then   'TextBox
            Select Case ctl.Index
                Case 1
                    monNom = "VH"
                Case 2
                    monNom = "CH"
                Case 3
                    monNom = "VB"
                Case 4
                    monNom = "CB"
                Case 5
                    monNom = "SH1"
                Case 6
                    monNom = "SH2"
            End Select
            Call WriteINI_Machine("mat_" & gstrMat, monNom, CDbl(ctl.Text))
        End If
        If ctl.Name = "Combo_SAV" Then 'ComboBox
            Call WriteINI_Machine("mat_" & gstrMat, ctl.Name & "_" & ctl.Index, ctl.ListIndex)
        End If
        If ctl.Name = "CB_SAV" Then   'CheckButton
            Call WriteINI_Machine("mat_" & gstrMat, ctl.Name & "_" & ctl.Index, ctl.value)
        End If
        If ctl.Name = "Option_SAV" Then   'Boutons radio
            Call WriteINI_Machine("mat_" & gstrMat, ctl.Name & "_" & ctl.Index, IIf(ctl.value, "1", "0"))
        End If
    Next ctl
    'Nom du mat�riau
    Call WriteINI_Machine("mat_" & gstrMat, "COMBO_MAT", gstrMat)

    Sav_Mat = True
End Function

Private Sub Chargt_Mat()
    Dim ctl As Control
    Dim monNom As String
    
    For Each ctl In Me.Controls
        If ctl.Name = "TB_SAV" Then   'TextBox
            Select Case ctl.Index
                Case 1
                    monNom = "VH"
                Case 2
                    monNom = "CH"
                Case 3
                    monNom = "VB"
                Case 4
                    monNom = "CB"
                Case 5
                    monNom = "SH1"
                Case 6
                    monNom = "SH2"
            End Select
            ctl.Text = ReadINI_Machine_Num("mat_" & Combo_Mat, monNom, ctl.Text)
        End If
        If ctl.Name = "Combo_SAV" Then 'ComboBox
            ctl.ListIndex = ReadINI_Machine("mat_" & Combo_Mat, ctl.Name & "_" & ctl.Index, 0)
        End If
        If ctl.Name = "CB_SAV" Then   'CheckButton
            ctl.value = ReadINI_Machine("mat_" & Combo_Mat, ctl.Name & "_" & ctl.Index, ctl.value)
        End If
        If ctl.Name = "Option_SAV" Then   'Boutons radio
            ctl.value = (ReadINI_Machine("mat_" & Combo_Mat, ctl.Name & "_" & ctl.Index, 0) = "1")
        End If
    Next ctl
End Sub

Private Sub Form_QueryUnload(cancel As Integer, UnloadMode As Integer)
    gPause = True
    cancel = Not Sav_Mat(AncienMat)
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
    End If
    If DeplEnCours Then Beep
    cancel = DeplEnCours
End Sub

Private Sub Form_Unload(cancel As Integer)
'    maClasseHID.EnvoiUSB    'RAZ du buffer
    frmMain.Timer_Refresh.Enabled = True
End Sub

Private Sub Label_Chauffe_CB_Click()
    TB_SAV(4) = Label_Chauffe
End Sub

Private Sub Label_Chauffe_CH_Click()
    TB_SAV(2) = Label_Chauffe
End Sub

Private Sub Label_Vitesse_VB_Click()
    TB_SAV(3) = TB_Vitesse
End Sub

Private Sub Label_Vitesse_VH_Click()
    TB_SAV(1) = TB_Vitesse
End Sub

Private Sub Option_Vitesse_Click(Index As Integer)
    TB_Vitesse.Enabled = (Index = 2)
    Select Case Index
        Case 0
            TB_Vitesse = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_15", 3)
        Case 1
            TB_Vitesse = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_13", 3)
        Case 2
    End Select
    Vitesse = CDbl(TB_Vitesse)
    If Index <> 2 Then  'Si l'index est � 2, on passera par TB_Vitesse_Change
        chauffe = Min(Calcul_Fil(gstrMat, Vitesse, CH, CB, VH, VB, SH1, SH2), 100)
        Label_Chauffe = chauffe
    End If
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    Frame_Droite.Enabled = (SSTab1.Tab = 1)
    Frame_Rayonnement.Enabled = (SSTab1.Tab = 1)
End Sub

Private Sub TB_EP_Apr�s_Change()
    If TB_EP_Apr�s = "" Then Exit Sub
    If Not IsNumeric(TB_EP_Apr�s) Then Exit Sub
    If Val(TB_Nbr) = 0 Then Exit Sub
    TB_Rayonnement = Format$((CSng(TB_EP_Avant.Text) - CSng(TB_EP_Apr�s.Text)) / CSng(TB_Nbr.Text), "0.00")
End Sub

Private Sub TB_EP_Apr�s_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_EP_Avant_Change()
    If TB_EP_Avant = "" Then Exit Sub
    If Not IsNumeric(TB_EP_Avant) Then Exit Sub
    If Val(TB_Nbr) = 0 Then Exit Sub
    TB_Rayonnement = Format$((CSng(TB_EP_Avant.Text) - CSng(TB_EP_Apr�s.Text)) / CSng(TB_Nbr.Text), "0.00")
End Sub

Private Sub TB_EP_Avant_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Nbr_Change()
    If TB_Nbr = "" Then Exit Sub
    If Not IsNumeric(TB_Nbr) Then Exit Sub
    If Val(TB_Nbr) = 0 Then Exit Sub
    TB_Rayonnement = Format$((CSng(TB_EP_Avant.Text) - CSng(TB_EP_Apr�s.Text)) / CSng(TB_Nbr.Text), "0.00")
End Sub

Private Sub TB_Nbr_KeyPress(KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_SAV_Change(Index As Integer)
    If Index <> 1 Then Exit Sub
    If TB_SAV(1) = "" Then Exit Sub
    If Not IsNumeric(TB_SAV(1)) Then Exit Sub
    TB_Vitesse_DVH = Format$(CDbl(TB_SAV(1).Text) / 2#, "0.00")
End Sub

Private Sub TB_SAV_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_SAV_Validate(Index As Integer, cancel As Boolean)
    If Index = 3 Or Index = 3 Then
        If CDbl(TB_SAV(3).Text) = CDbl(TB_SAV(1).Text) Then
             MsgBox ReadINI_Trad(gLangue, "erreurs", 301), vbCritical, App.Title
             cancel = True
        End If
    End If
    If Index = 4 Then
        If CDbl(TB_SAV(4).Text) = CDbl(TB_SAV(2).Text) Then
             MsgBox ReadINI_Trad(gLangue, "erreurs", 301), vbCritical, App.Title
             cancel = True
        End If
    End If
End Sub

Private Sub TB_TEMP_Change(Index As Integer)
    If Index = 3 Or Index = 4 Or Index = 10 Then
        If Val(TB_TEMP(Index)) > 100 Then
            Beep
        End If
    End If
End Sub

Private Sub TB_TEMP_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_TEMP_Validate(Index, False)
        Exit Sub
    End If
End Sub

Private Sub TB_TEMP_Validate(Index As Integer, cancel As Boolean)
    If TB_TEMP(Index).Text = "" Then Exit Sub
    If Not IsNumeric(TB_TEMP(Index).Text) Then Exit Sub
    TB_TEMP(Index).Text = CDbl(TB_TEMP(Index))
    Select Case Index
        Case 3, 4, 10, 11
            TB_TEMP(Index) = Format$(Round(TB_TEMP(Index), 1), "#0.0")
        Case 13
            TB_TEMP(Index) = Format$(Round(TB_TEMP(Index), 0), "#0")
         Case Else
            TB_TEMP(Index) = Format$(Round(TB_TEMP(Index), 2), "#0.00")
    End Select
    Call WriteINI("TB_TEMP_" & Index, TB_TEMP(Index).Text)
End Sub

Private Sub TB_Vitesse_Change()
    If TB_Vitesse = "" Then Exit Sub
    If Not IsNumeric(TB_Vitesse) Then Exit Sub
    Vitesse = CDbl(TB_Vitesse)
End Sub

Private Sub TB_Vitesse_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Vitesse_Validate(False)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Vitesse_Validate(cancel As Boolean)
    cancel = True
    If TB_Vitesse = "" Then Exit Sub
    If Not IsNumeric(TB_Vitesse) Then Exit Sub
    
    cancel = False
    Vitesse = CDbl(TB_Vitesse)
    chauffe = Min(Calcul_Fil(gstrMat, Vitesse, CH, CB, VH, VB, SH1, SH2), 100)
    Label_Chauffe = chauffe
End Sub

Private Sub Timer_Test_Timer()
    If Not maClasseHID.IsAvailable Then Exit Sub
    Call maClasseHID.DemandeInformation
    
    If maClasseHID.PWM_ON Then  'PWM ON
        Label_PWM_AUTO.BackColor = RGB(200, 0, 0)
        Label_PWM_MANUAL.BackColor = RGB(200, 0, 0)
        Label_Chauffe.BackColor = RGB(200, 0, 0)
        Frame_Chauffe.BackColor = RGB(200, 0, 0)
    Else
        Label_PWM_AUTO.BackColor = &H8000000F
        Label_PWM_MANUAL.BackColor = &H8000000F
        Label_Chauffe.BackColor = &H8000000F
        Frame_Chauffe.BackColor = &H8000000F
    End If
    
    If maClasseHID.PWM_Manual Then
        Label_PWM_MANUAL.Visible = True
        Label_PWM_AUTO.Visible = False
        Label_Chauffe.Visible = True
        Label_Chauffe = maClasseHID.PWMM
        HScroll_Chauffe.Visible = True
        HScroll_Chauffe.value = maClasseHID.PWMM
    Else
        Label_PWM_AUTO.Visible = True
        Label_PWM_MANUAL.Visible = False
        Label_Chauffe.Visible = True
        Label_Chauffe = maClasseHID.PWMA
        HScroll_Chauffe.Visible = True
        HScroll_Chauffe.value = maClasseHID.PWMA
    End If
End Sub

Sub Cmd_Enable(bEnable As Boolean)
    Dim Ctrl As Control
    For Each Ctrl In Controls
        If Left(Ctrl.Name, 4) = "Cmd_" And Ctrl.Name <> "Cmd_Stop" Then
            Ctrl.Enabled = bEnable
        End If
    Next
End Sub
