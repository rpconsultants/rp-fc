Attribute VB_Name = "Module_Shell"
Option Explicit

Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function SetEnvironmentVariable Lib "kernel32" Alias "SetEnvironmentVariableA" (ByVal lpName As String, ByVal lpValue As String) As Long

Public Function ShellWait(ByVal vCommand As String) As Boolean

    Dim mProcess As Long
    Dim mRet As Long
    Dim mPID As Long
    Dim bRet As Boolean
       
    On Error GoTo ShellWait_Error

    mPID = Shell(vCommand, vbMinimizedNoFocus)
    mProcess = OpenProcess(&H1F0FFF, 0, mPID)
    Do
        mRet = WaitForSingleObject(mProcess, 1000)
        If mRet <> &H102& Then
            bRet = True
            Exit Do
        End If
        DoEvents
    Loop
    CloseHandle mProcess
ShellWait_OK:
    On Error GoTo 0
    Exit Function

ShellWait_Error:
    bRet = False
    Resume ShellWait_OK
End Function



