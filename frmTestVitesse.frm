VERSION 5.00
Begin VB.Form frmTestVitesse 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Outil test vitesse"
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4380
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4440
   ScaleWidth      =   4380
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.Timer Timer_Test 
      Interval        =   50
      Left            =   2760
      Top             =   3960
   End
   Begin VB.Frame Frame1 
      Caption         =   "Param�tres"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   0
      TabIndex        =   6
      Tag             =   "30"
      Top             =   2280
      Width           =   3375
      Begin VB.TextBox TB_Param 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   1845
         TabIndex        =   16
         Text            =   "20"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   705
      End
      Begin VB.TextBox TB_Param 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   1845
         TabIndex        =   13
         Text            =   "1"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   705
      End
      Begin VB.TextBox TB_Param 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1845
         TabIndex        =   10
         Text            =   "20"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   705
      End
      Begin VB.TextBox TB_Param 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1845
         TabIndex        =   7
         Text            =   "4"
         ToolTipText     =   "0"
         Top             =   210
         Width           =   705
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Distance � parcourir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Tag             =   "34"
         ToolTipText     =   "0"
         Top             =   990
         Width           =   1650
      End
      Begin VB.Label Label10 
         Caption         =   "mm"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2685
         TabIndex        =   17
         Tag             =   "35"
         ToolTipText     =   "0"
         Top             =   990
         Width           =   645
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Incr�ment"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Tag             =   "33"
         ToolTipText     =   "0"
         Top             =   750
         Width           =   1650
      End
      Begin VB.Label Label8 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2685
         TabIndex        =   14
         Tag             =   "26"
         ToolTipText     =   "0"
         Top             =   750
         Width           =   645
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Vitesse d'arriv�e"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Tag             =   "32"
         ToolTipText     =   "0"
         Top             =   510
         Width           =   1650
      End
      Begin VB.Label Label6 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2685
         TabIndex        =   11
         Tag             =   "26"
         ToolTipText     =   "0"
         Top             =   510
         Width           =   645
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Vitesse de d�part"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Tag             =   "31"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1650
      End
      Begin VB.Label Label4 
         Caption         =   "mm/s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2685
         TabIndex        =   8
         Tag             =   "26"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   645
      End
   End
   Begin VB.CommandButton Cmd_Stop 
      Caption         =   "STOP"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   0
      TabIndex        =   5
      Tag             =   "41"
      ToolTipText     =   "0"
      Top             =   3720
      Width           =   1365
   End
   Begin VB.CommandButton Cmd_Go 
      Caption         =   "GO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   3000
      TabIndex        =   1
      Tag             =   "40"
      ToolTipText     =   "0"
      Top             =   3720
      Width           =   1365
   End
   Begin VB.Frame Frame_Axe 
      Caption         =   "Axe test�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Tag             =   "20"
      Top             =   1650
      Width           =   4335
      Begin VB.CheckBox CB_Axe 
         Caption         =   "Z"
         Height          =   255
         Index           =   4
         Left            =   3480
         TabIndex        =   23
         Tag             =   "27"
         ToolTipText     =   "0"
         Top             =   360
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CheckBox CB_Axe 
         Caption         =   "YD"
         Height          =   255
         Index           =   3
         Left            =   2640
         TabIndex        =   22
         Tag             =   "24"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   735
      End
      Begin VB.CheckBox CB_Axe 
         Caption         =   "YG"
         Height          =   255
         Index           =   2
         Left            =   1800
         TabIndex        =   21
         Tag             =   "23"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   735
      End
      Begin VB.CheckBox CB_Axe 
         Caption         =   "XD"
         Height          =   255
         Index           =   1
         Left            =   960
         TabIndex        =   20
         Tag             =   "22"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   735
      End
      Begin VB.CheckBox CB_Axe 
         Caption         =   "XG"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   19
         Tag             =   "21"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Label Label_Vitesse 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1920
      TabIndex        =   4
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   4080
      Width           =   660
   End
   Begin VB.Label Label_Aide 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Aide"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   75
      TabIndex        =   3
      Tag             =   "10"
      Top             =   105
      Width           =   4290
   End
   Begin VB.Label Label_VitesseActuelle 
      Alignment       =   1  'Right Justify
      Caption         =   "Vitesse actuelle"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1440
      TabIndex        =   2
      Tag             =   "30"
      ToolTipText     =   "0"
      Top             =   3720
      Width           =   1545
   End
End
Attribute VB_Name = "frmTestVitesse"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim maVitesse As Double


