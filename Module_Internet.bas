Attribute VB_Name = "Module_Internet"
Option Explicit

Dim hSession As Long
Dim hUrlFile As Long
Const INTERNET_OPEN_TYPE_PRECONFIG = 0
Const INTERNET_FLAG_RELOAD = &H80000000
 
Private Declare Function InternetGetConnectedState Lib "wininet" (ByRef dwFlags As Long, _
    ByVal dwReserved As Long) As Long       'Etat de connexion � Internet
Private Declare Function InternetOpen Lib "wininet.dll" _
     Alias "InternetOpenA" _
     (ByVal lpszAgent As String, _
        ByVal dwAccessType As Long, _
        ByVal lpszProxyName As String, _
        ByVal lpszProxyBypass As String, _
        ByVal dwFlags As Long) _
        As Long
Private Declare Function InternetCloseHandle Lib "wininet.dll" _
    (ByVal hInet As Long) As Long
Private Declare Function InternetOpenUrl Lib "wininet.dll" _
     Alias "InternetOpenUrlA" _
     (ByVal hInternetSession As Long, _
        ByVal lpszUrl As String, _
        ByVal lpszHeaders As String, _
        ByVal dwHeadersLength As Long, _
        ByVal dwFlags As Long, _
        ByVal dwContext As Long) _
        As Long
Private Declare Function InternetReadFile Lib "wininet.dll" _
     (ByVal hFile As Long, _
        ByVal lpBuffer As String, _
        ByVal dwNumberOfBytesToRead As Long, _
        lNumberOfBytesRead As Long) _
        As Long
Private Declare Function DeleteUrlCacheEntry Lib "wininet.dll" _
     Alias "DeleteUrlCacheEntryA" _
     (ByVal lpszUrlName As String) _
        As Integer

Public Function Internet_Connected() As Boolean
    Dim dwFlags As Long

    Internet_Connected = InternetGetConnectedState(dwFlags, 0&)
End Function

Public Function OuvrirURL(sURL As String) As String
    Dim buffer As String * 256
    Dim Info As String
    Dim NombreOctets As Long
    Dim ValRet As Long
    Dim r As Integer
 
    On Error GoTo Err_Lecture
 
    DeleteUrlCacheEntry sURL
 
    hSession = InternetOpen(App.Title, _
        INTERNET_OPEN_TYPE_PRECONFIG, _
        vbNullString, _
        vbNullString, _
        0)
    
    hUrlFile = InternetOpenUrl(hSession, _
        sURL, _
        vbNullString, _
        0, _
        INTERNET_FLAG_RELOAD, _
        0)
        
    ValRet = InternetReadFile(hUrlFile, _
        buffer, _
        Len(buffer), _
        NombreOctets)
        
    InternetCloseHandle (hUrlFile)
    InternetCloseHandle (hSession)
    If NombreOctets = 0 Then
        OuvrirURL = ""
        Exit Function
    End If
        
    Info = Left$(buffer, NombreOctets)
    
    DeleteUrlCacheEntry sURL
    
    OuvrirURL = Info
    Exit Function
 
Err_Lecture:
    OuvrirURL = ""
    Screen.MousePointer = vbNormal
    Exit Function
 
End Function
