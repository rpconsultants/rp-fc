VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsListNode"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_Noeud As TypeNoeud
Private m_Next As clsListNode

'Donn�es correspondant au noeud
Friend Function GetData() As TypeNoeud
    GetData = m_Noeud
End Function

Friend Function GetPoint() As Point
    Dim monPoint As Point
    monPoint.x = m_Noeud.x
    monPoint.y = m_Noeud.y
    GetPoint = monPoint
End Function

Friend Function GetDist() As Single
    GetDist = m_Noeud.Dist
End Function

Friend Function GetOrigine() As Long
    GetOrigine = m_Noeud.iOrigine
End Function

Friend Sub LetData(x As TypeNoeud)
    m_Noeud = x
End Sub

Friend Sub LetbSynchro(bSynchro As Boolean)
    m_Noeud.bSynchro = bSynchro
End Sub

Friend Sub LetDist(Dist As Single)
    m_Noeud.Dist = Dist
End Sub

Friend Sub LetVitesse(Vitesse As Single)
    m_Noeud.Vitesse = Vitesse
End Sub

Friend Sub LetTemp(Temp As Boolean)
    m_Noeud.bTemp = Temp
End Sub

Friend Sub LetX(x As Single)
    m_Noeud.x = x
End Sub

Friend Sub LetY(y As Single)
    m_Noeud.y = y
End Sub

Friend Sub LetDecalage(Decalage As Single)
    m_Noeud.Decalage = Decalage
End Sub

Friend Sub LetOrigine(Origine As Long)
    m_Noeud.iOrigine = Origine
End Sub

'Noeud suivant dans la liste
Public Property Get NextNode() As clsListNode
    Set NextNode = m_Next
End Property

Public Property Set NextNode(value As clsListNode)
    Set m_Next = value
End Property



