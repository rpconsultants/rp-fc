Attribute VB_Name = "Module_INI"
Option Explicit

'Module de gestion du fichier INI

Function ReadINI_Num(monItem As String, maValeur As String) As String
    ReadINI_Num = ReadINI(monItem, maValeur)
    
    If Format$(0#, ".") <> "." Then
        ReadINI_Num = Replace(ReadINI_Num, ".", Format$(0#, "."))
    End If
End Function

Function ReadINI(monItem As String, maValeur As String) As String
    Dim stBuf As String, lgBuf As Long, lgRep As Long
    Dim i As Integer, j As Integer
    Dim monEnreg As String
    
    stBuf = Space$(255)
    lgBuf = 255
    lgRep = GetPrivateProfileString(App.Title, monItem, maValeur, stBuf, lgBuf, App.Path & "\" & App.Title & ".ini")
    ReadINI = Replace(Left(stBuf, lgRep), "&&", Chr(9))
End Function

Function WriteINI(monItem As String, maValeur As String)
    maValeur = Replace(maValeur, Chr(9), "&&")
    
    WritePrivateProfileString App.Title, monItem, maValeur, App.Path & "\" & App.Title & ".ini"
End Function

Function ReadINI_Machine(maSection As String, monItem As String, maValeur As String) As String
    Dim stBuf As String, lgBuf As Long, lgRep As Long
    
    stBuf = Space$(255)
    lgBuf = 255
    lgRep = GetPrivateProfileString(maSection, monItem, maValeur, stBuf, lgBuf, App.Path & "\" & App.Title & ".ini")
    ReadINI_Machine = Replace(Left(stBuf, lgRep), "&&", Chr(9))
End Function

Function ReadINI_Machine_Num(maSection As String, monItem As String, maValeur As String) As String
    ReadINI_Machine_Num = ReadINI_Machine(maSection, monItem, maValeur)
    
    If Format$(0#, ".") = "," Then
        ReadINI_Machine_Num = Replace(ReadINI_Machine_Num, ".", ",")
    End If
    If Format$(0#, ".") = "." Then
        ReadINI_Machine_Num = Replace(ReadINI_Machine_Num, ",", ".")
    End If
End Function

Function WriteINI_Machine(maSection As String, monItem As String, maValeur As String)
    maValeur = Replace(maValeur, Chr(9), "&&")
    
    WritePrivateProfileString maSection, monItem, maValeur, App.Path & "\" & App.Title & ".ini"
End Function

'Fonction pour supprimer une valeur dans un fichier INI
Function DeleteINI(monItem As String)
    WritePrivateProfileString App.Title, monItem, vbNullString, App.Path & "\" & App.Title & ".ini"
End Function

'Fonction pour remplacer une valeur dans un fichier
Private Sub ReplaceLineFromFileByPattern(ByVal FileName As String, _
                                       ByVal pattern As String, _
                                       ByVal NewValue As String)
    
    Dim f As Integer, errCode As Integer, errString As String
    Dim buffer As String
    Dim t() As String
    Dim i As Long
    Dim nbOcc As Long
    Dim bTrouve As Boolean
    
    buffer = ReadFileToBuffer(FileName, errCode, errString)
    t() = Split(buffer, vbCrLf)
    
    bTrouve = False
        
    f = FreeFile
    Open FileName For Output As #f
        For i = 0 To UBound(t()) - 1
            If InStr(t(i), pattern) > 0 Then
                ' Si on trouve le pattern
                bTrouve = True
                nbOcc = nbOcc + 1
                If (nbOcc = 1) Then
                    ' On remplace toute la ligne
                    Print #f, NewValue
                Else
                    Print #f, t(i)
                End If
            Else
                Print #f, t(i)
            End If
        Next i
        If Not bTrouve Then
            Print #f, NewValue
        End If
    Close #f
    
End Sub

' Cette fonction lit le contenu du fichier szFileName et retourne
' ce contenu. En cas d'erreur, elle retourne une cha�ne vide et
' renseigne le code d'erreur et la description de l'erreur
'
Private Function ReadFileToBuffer(ByVal szFileName As String, _
                                ByRef errCode As Integer, _
                                ByRef errString As String) As String
    Dim f As Integer
    Dim buffer As String

    ' trappe les erreurs
    On Error GoTo ReadFileToBuffer_ERR

    ' Ouverture du fichier en 'Binary'
    f = FreeFile
    Open szFileName For Binary As #f
        ' pr�allocation d'un buffer � la taille du fichier
        buffer = Space$(LOF(f))
        ' lecture compl�te du fichier
        Get #f, , buffer
    Close #f
    ReadFileToBuffer = buffer
ReadFileToBuffer_END:
    Exit Function
    
ReadFileToBuffer_ERR:
    ' Gestion d'erreur
    ReadFileToBuffer = ""
    errCode = err.Number
    errString = err.Description
    Resume ReadFileToBuffer_END
End Function

Function ReadINI_Trad(maLangue As String, monFormulaire As String, monItem As String) As String
    Dim stBuf As String, lgBuf As Long, lgRep As Long
    Dim i As Integer, j As Integer
    Dim monEnreg As String
    Dim maValeur As String
    
    If monItem = "" Then
        ReadINI_Trad = "___"
        Exit Function
    End If
    
    'On commence par lire dans le .INI de la langue
    stBuf = Space$(255)
    lgBuf = 255
    maValeur = "$$"
    lgRep = GetPrivateProfileString(monFormulaire, monItem, maValeur, stBuf, lgBuf, App.Path & "\" & App.Title & maLangue & ".trad")
    If Left(stBuf, 2) = "$$" And maLangue <> "en" Then
        ReadINI_Trad = ReadINI_Trad("en", monFormulaire, monItem)
        Exit Function
    End If
    If Left(stBuf, 2) = "$$" Then
        ReadINI_Trad = monItem
        Exit Function
    End If
    ReadINI_Trad = Left(stBuf, lgRep)
End Function

Function ListeSectionIni(ByVal Path As String, Section() As String)
    Dim strReturn As String
    strReturn = String(8192, 0)
    
    'Param�tre lpszReturnBuffer: adresse d'un tampon qui va recevoir la ou les sections du fichier .ini.
    '(Chaque nom de section est termin� par un caract�re null (Chr(0)=vbNullChar), le dernier est suivi d'un second caract�re null.
   
    GetPrivateProfileSectionNames strReturn, Len(strReturn), App.Path & "\" & Path
   
    Section = Split(Left(strReturn, InStr(1, strReturn, vbNullChar & vbNullChar) - 1), vbNullChar)
End Function

Sub RenameSectionIni(ByVal strSection As String, ByVal strNewSection As String, ByVal strfullpath As String)
    'Renomme une section d'un fichier .ini
    'Copie vers la nouvelle
    Call CopySectionIni(strSection, strNewSection, strfullpath)
    'supprime l'ancienne
    Call DeleteSectionIni(strSection, strfullpath)
End Sub

Sub CopySectionIni(ByVal strSection As String, ByVal strNewSection As String, ByVal strfullpath As String)
    'Renomme une section d'un fichier .ini
    Dim KeyAndVal() As String, Key_Val() As String, strBuffer As String
    Dim intx As Integer
    
    Let strBuffer$ = String$(8192, Chr$(0&))
    Call GetPrivateProfileSection(strSection, strBuffer, Len(strBuffer), strfullpath)
    KeyAndVal = Split(strBuffer, vbNullChar)
    For intx = LBound(KeyAndVal) To UBound(KeyAndVal)
        Key_Val = Split(KeyAndVal(intx), "=")
        If UBound(Key_Val) = -1 Then Exit For
        Call WritePrivateProfileString(strNewSection, UCase$(Key_Val(0)), Key_Val(1), strfullpath)
    Next

    Erase KeyAndVal
    Erase Key_Val
End Sub

Sub DeleteSectionIni(ByVal strSection As String, ByVal strfullpath As String)
    'Supprime une section
    Call WritePrivateProfileString(strSection, vbNullString, vbNullString, strfullpath)
End Sub

Sub CreatINI()
    Dim szFileName As String
    Dim f As Integer
    
    szFileName = App.Path & "\" & App.Title & ".ini"
    
    Dim buffer As String

    ' Ouverture du fichier
    f = FreeFile
    Open szFileName For Output As #f
    
        Print #f, "[RP-FC]"
        Print #f, "TableDefaut=MM2001"
        Print #f, "MatDefaut=DEPRON"

        Print #f, "[table_MM2001]"
        Print #f, "COMBO_SAV_0=0"
        Print #f, "COMBO_SAV_1=2"
        Print #f, "COMBO_SAV_2=3"
        Print #f, "COMBO_SAV_3=0"
        Print #f, "COMBO_SAV_4=1"
        Print #f, "COMBO_SAV_5=6"
        Print #f, "COMBO_SAV_6=7"
        Print #f, "COMBO_SAV_7=4"
        Print #f, "COMBO_SAV_8=5"
        Print #f, "COMBO_SAV_9=8"
        Print #f, "COMBO_SAV_10=8"
        Print #f, "COMBO_SAV_11=4"
        Print #f, "COMBO_SAV_12=1"
        Print #f, "COMBO_SAV_13=1"
        Print #f, "COMBO_SAV_14=0"
        
        Print #f, "CB_SAV_0=0"
        Print #f, "CB_SAV_1=0"
        Print #f, "CB_SAV_2=0"
        Print #f, "CB_SAV_3=0"
        Print #f, "CB_SAV_4=0"
        Print #f, "CB_SAV_5=0"
        Print #f, "CB_SAV_6=0"
        Print #f, "CB_SAV_7=0"
        Print #f, "CB_SAV_8=0"
        Print #f, "CB_SAV_9=0"
        Print #f, "CB_SAV_10=0"
        Print #f, "CB_SAV_11=0"
        Print #f, "CB_SAV_12=0"
        Print #f, "CB_SAV_13=0"
        Print #f, "CB_SAV_103=1"
        Print #f, "CB_SAV_107=1"
        
        Print #f, "TB_SAV_0=1,00"
        Print #f, "TB_SAV_1=1,00"
        Print #f, "TB_SAV_2=0,00"
        Print #f, "TB_SAV_3=100,0"
        Print #f, "TB_SAV_4=100,0"
        Print #f, "TB_SAV_5=100,0"
        Print #f, "TB_SAV_6=3,6"
        Print #f, "TB_SAV_7=3,6"
        Print #f, "TB_SAV_8=3,6"
        Print #f, "TB_SAV_9=0"
        Print #f, "TB_SAV_10=0"
        Print #f, "TB_SAV_11=0"
        Print #f, "TB_SAV_12=6,0"
        Print #f, "TB_SAV_13=6,0"
        Print #f, "TB_SAV_14=10,0"
        Print #f, "TB_SAV_15=10,0"
        Print #f, "TB_SAV_16=600,0"
        Print #f, "TB_SAV_17=400,0"
        Print #f, "TB_SAV_18=1200,0"
        Print #f, "TB_SAV_19=2,0"
        Print #f, "TB_SAV_20=2,0"
        Print #f, "TB_SAV_21=0,0"
        Print #f, "TB_SAV_22=0,0"
        Print #f, "TB_SAV_23=610,0"
        Print #f, "TB_SAV_24=10,0"
        Print #f, "TB_SAV_25=10,0"
        Print #f, "TB_SAV_26=0,0"
        Print #f, "TB_SAV_27=0,0"
        Print #f, "TB_SAV_28=100"
        Print #f, "TB_SAV_30=30"
        Print #f, "TB_SAV_31=0"
        Print #f, "TB_SAV_32=0"
        Print #f, "TB_SAV_34=50"
        Print #f, "TB_SAV_35=60"
        
        Print #f, "OPTION_SAV_0=0"
        Print #f, "OPTION_SAV_1=0"
        Print #f, "OPTION_SAV_2=0"
        Print #f, "OPTION_SAV_3=0"
        Print #f, "OPTION_SAV_4=0"
        Print #f, "OPTION_SAV_5=0"
        Print #f, "OPTION_SAV_6=0"
        Print #f, "OPTION_SAV_7=1"
        Print #f, "OPTION_SAV_8=0"
        
        Print #f, "Combo_Table=MM2001"
        Print #f, ""
        
        Print #f, "[mat_DEPRON]"
        Print #f, "CH=2"
        Print #f, "CB=1"
        Print #f, "VH=20"
        Print #f, "VB=2"
        Print #f, "SH1=1"
        Print #f, "SH2=2"
        Print #f, "Combo_Mat=DEPRON"
    Close #f
End Sub
