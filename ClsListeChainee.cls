VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsListeChainee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_Head As clsListNode
Private m_Previous As clsListNode
Private m_Current As clsListNode
Private m_Count As Long
Private m_MinX As Single
Private m_MinY As Single
Private m_MaxX As Single
Private m_MaxY As Single
Private m_bMinMax As Boolean

Friend Sub Insert(monNoeud As TypeNoeud)
    Dim LinkSave As clsListNode
    
    'Si aucune donn�e n'est d�j� pr�sente, cr�e une t�te de liste
    If (m_Count = 0) Then
        Set m_Head = New clsListNode
        m_Head.LetData monNoeud
        Set m_Current = m_Head
    Else
        'Si l'utilisateur veut changer la t�te de liste
        If m_Current Is m_Head Then
            'Cr�e le nouveau noeud et effectue le chainage
            Set LinkSave = m_Head
            Set m_Head = New clsListNode
            m_Head.LetData monNoeud
            Set m_Head.NextNode = LinkSave
            Set m_Current = m_Head
            
        'Si on est pas en dehors de la liste
        ElseIf Not m_Previous Is Nothing Then
            'Cr�e le nouveau noeud et effectue le chainage
            Set m_Previous.NextNode = New clsListNode
            m_Previous.NextNode.LetData monNoeud
            Set m_Previous.NextNode.NextNode = m_Current
            Set m_Current = m_Previous.NextNode
        End If
    End If
    
    m_Count = m_Count + 1
End Sub

Friend Sub InsertAfter(monNoeud As TypeNoeud)
    Dim LinkSave As clsListNode
    
    'Si aucune donn�e n'est d�j� pr�sente, cr�e une t�te de liste
    If (m_Count = 0) Then
        Set m_Head = New clsListNode
        m_Head.LetData monNoeud
        Set m_Current = m_Head
    Else
       'Cr�e le nouveau noeud et effectue le chainage
       Set m_Current.NextNode = New clsListNode
       m_Current.NextNode.LetData monNoeud
       Call MoveNext
    End If
    
    m_Count = m_Count + 1
End Sub

Public Function Remove() As Boolean
    'S'il existe des noeuds
    If m_Count <> 0 Then
        'Si l'utilisateur d�sire supprimer la t�te, celle-ci devient le second �l�ment
        If m_Current Is m_Head Then
            Set m_Head = m_Head.NextNode
            Set m_Current = m_Head
        'Si l'utilisateur tente de supprimer un autre noeud, effectue le cha�nage appropri�
        ElseIf Not m_Current Is Nothing Then
            Set m_Previous.NextNode = m_Current.NextNode
            Set m_Current = m_Previous.NextNode
            'On tente de supprimer le dernier noeud
            If (m_Current Is Nothing) Then
                Remove = False
                Exit Function
             End If
        End If
        
        Remove = False
        m_Count = m_Count - 1
    End If
End Function

'D�placement l'�l�ment courant en d�but de liste
Public Sub MoveFirst()
    Set m_Current = m_Head
    Set m_Previous = Nothing
End Sub

'D�placement l'�l�ment courant vers le suivant
Public Function MoveNext() As Boolean
    MoveNext = False
    
    If Not m_Current Is Nothing Then
        Set m_Previous = m_Current
        Set m_Current = m_Current.NextNode
    'Si aucun �l�ment n'est s�lectionn�, revient au d�but
    ElseIf m_Previous Is Nothing And m_Current Is Nothing Then
        MoveFirst
    End If
    
    MoveNext = Not (m_Current Is Nothing)
End Function

Public Sub MoveLast()
    While (MoveNext)
    Wend
End Sub

Public Function MoveIndex(Index As Long) As Boolean
    MoveIndex = False
    If Index > m_Count Then Exit Function
    Dim i As Long
    
    i = 1
    MoveFirst
    
    While (i < Index)
        i = i + 1
        MoveNext
    Wend
    MoveIndex = True
End Function

Public Property Get count() As Long
    count = m_Count
End Property

Public Property Get IsEmpty() As Boolean
    IsEmpty = (m_Count = 0)
End Property

Public Function CurrentX() As Single
    If Not m_Current Is Nothing Then
        CurrentX = m_Current.GetData.x
    End If
End Function

Public Function CurrentY() As Single
    If Not m_Current Is Nothing Then
        CurrentY = m_Current.GetData.y
    End If
End Function

Public Function CurrentbTemp() As Boolean
    If Not m_Current Is Nothing Then
        CurrentbTemp = m_Current.GetData.bTemp
    End If
End Function

Public Function CurrentbSynchro() As Boolean
    If Not m_Current Is Nothing Then
        CurrentbSynchro = m_Current.GetData.bSynchro
    End If
End Function

Public Function CurrentDist() As Single
    If Not m_Current Is Nothing Then
        CurrentDist = m_Current.GetData.Dist
    End If
End Function

Public Function CurrentVitesse() As Single
    If Not m_Current Is Nothing Then
        CurrentVitesse = m_Current.GetData.Vitesse
    End If
End Function

Public Function CurrentDecalage() As Single
    If Not m_Current Is Nothing Then
        CurrentDecalage = m_Current.GetData.Decalage
    End If
End Function

Public Function PreviousDecalage() As Single
    If Not m_Previous Is Nothing Then
        PreviousDecalage = m_Previous.GetData.Decalage
    End If
End Function

Public Function maxX() As Single
    If Not m_bMinMax Then MinMax
    maxX = m_MaxX
End Function

Public Function minX() As Single
    If Not m_bMinMax Then MinMax
    minX = m_MinX
End Function

Public Function maxY() As Single
    If Not m_bMinMax Then MinMax
    maxY = m_MaxY
End Function

Public Function minY() As Single
    If Not m_bMinMax Then MinMax
    minY = m_MinY
End Function

Friend Function CurrentPoint() As Point
    If Not m_Current Is Nothing Then
        CurrentPoint = m_Current.GetPoint
    End If
End Function

Friend Function CurrentOrigine() As Long
    If Not m_Current Is Nothing Then
        CurrentOrigine = m_Current.GetOrigine
    End If
End Function

Friend Function PreviousPoint() As Point
    If Not m_Previous Is Nothing Then
        PreviousPoint = m_Previous.GetPoint
    End If
End Function

Friend Function NextPoint() As Point
    If Not m_Current.NextNode Is Nothing Then
        NextPoint = m_Current.NextNode.GetPoint
    End If
End Function

Private Sub MinMax()
    Dim memo_Current As clsListNode
    
    Set memo_Current = m_Current
    
    Call MoveFirst
    m_MinX = m_Current.GetData.x
    m_MaxX = m_Current.GetData.x
    m_MinY = m_Current.GetData.y
    m_MaxY = m_Current.GetData.y
    Do
        If m_Current.GetData.x < m_MinX Then
            m_MinX = m_Current.GetData.x
        ElseIf m_Current.GetData.x > m_MaxX Then
            m_MaxX = m_Current.GetData.x
        End If
        If m_Current.GetData.y < m_MinY Then
            m_MinY = m_Current.GetData.y
        ElseIf m_Current.GetData.y > m_MaxY Then
            m_MaxY = m_Current.GetData.y
        End If
    Loop While MoveNext
    Set m_Current = memo_Current
    m_bMinMax = True
End Sub

Public Sub SupprPtsInutiles()
    'Suppression des points en double et des points align�s
    Dim i As Long
    Dim j As Long
    Dim k As Integer
    Dim P1 As clsListNode, P2 As clsListNode, P3 As clsListNode
    
    Call MoveFirst
    Set P1 = m_Current
    Call MoveNext
    Set P2 = m_Current
    Call MoveNext
    
    'Suppression des points align�s
    Do
        Set P3 = m_Current
        If EstSurLigne(P1.GetPoint, P2.GetPoint, P3.GetPoint) Then
            'On en fait un point en double
            Call P2.LetData(P1.GetData)
        End If
        Set P1 = P2
        Set P2 = P3
    Loop While MoveNext
    
    'Suppression des points en double
    Call MoveFirst
    Set P1 = m_Current
    If Not MoveNext Then Exit Sub
    Do
        Do While m_Current.GetPoint.x = P1.GetPoint.x And _
            m_Current.GetPoint.y = P1.GetPoint.y
            If Not Remove Then Exit Do
        Loop
        Set P1 = m_Current
    Loop While MoveNext
End Sub

Private Function EstSurLigne(P1 As Point, P2 As Point, P3 As Point) As Boolean
    'Ces trois points sont-ils align�s ?
    ' Algo de Caroline
    Const SINGLE_MAX = 3.4E+38

    Dim A As Single, B As Single
    Dim P1_SAV As Point
    Dim P2_SAV As Point
    Dim P3_SAV As Point
    
    P1_SAV = P1
    P2_SAV = P2
    P3_SAV = P3
    
    P2.x = P2.x - P1.x
    P3.x = P3.x - P1.x
    P2.y = P2.y - P1.y
    P3.y = P3.y - P1.y

    If P2.x = 0 Then
        A = SINGLE_MAX
    Else
        A = P2.y / P2.x
    End If
        
    If P3.x = 0 Then
        B = SINGLE_MAX
    Else
        B = P3.y / P3.x
    End If

    EstSurLigne = (A = B)
    P1 = P1_SAV
    P2 = P2_SAV
    P3 = P3_SAV
    
End Function

Public Sub RAZPtCles()
    Call MoveFirst
    Do
        If m_Current.GetData.bTemp Then
            Call Remove
        Else
            If m_Current.GetData.bSynchro Then
                m_Current.LetbSynchro (False)
            End If
        End If
    Loop While MoveNext
End Sub

Public Sub Synchro(Num As Long, bSynchro As Boolean)
    Dim i As Long
    
    Call MoveFirst
    i = 1
    While i < Num
        MoveNext
        i = i + 1
    Wend
    m_Current.LetbSynchro bSynchro
End Sub

Public Function NbPtsSynchro() As Long
    Dim i As Long
    
    Call MoveFirst
    i = 0
    Do
        If m_Current.GetData.bSynchro Then i = i + 1
    Loop While MoveNext
    NbPtsSynchro = i
End Function

Public Sub ChangeOrigine(Num As Long)
    If Num > m_Count Then Exit Sub
    
    'On boucle le dernier sur le premier
    MoveIndex m_Count
    Set m_Current.NextNode = m_Head
    
    MoveIndex Num - 1
    'On fait pointer le d�but de la liste sur le Num
    Set m_Head = m_Current.NextNode
    'Le Num-1 est la fin de la liste
    'Set m_Current.NextNode = Nothing
    Set m_Current.NextNode = New clsListNode
    Set m_Current = m_Current.NextNode
    m_Current.LetData m_Head.GetData
    
    Set m_Current.NextNode = Nothing
    m_Count = m_Count + 1
    
    'On fait le m�nage
    'Suppression des points en double
    Dim P1 As clsListNode
    Call MoveFirst
    Set P1 = m_Current
    Call MoveNext
    Do
        While m_Current.GetPoint.x = P1.GetPoint.x And _
            m_Current.GetPoint.y = P1.GetPoint.y
            Remove
        Wend
        Set P1 = m_Current
    Loop While MoveNext
End Sub

Public Sub Dist(Dist As Single)
    m_Current.LetDist Dist
End Sub

Public Sub Decalage(Decalage As Single)
    m_Current.LetDecalage Decalage
End Sub

Public Sub Vitesse(Vitesse As Single)
    m_Current.LetVitesse Vitesse
End Sub

Public Sub Temp(Temp As Boolean)
    m_Current.LetTemp Temp
End Sub

Private Sub Class_Initialize()
    m_bMinMax = False
    m_Count = 0
End Sub

Public Sub Inversion()
    Dim i As Long
    Dim monTableau() As TypeNoeud
    ReDim monTableau(1 To m_Count)

    Call MoveFirst
    For i = 1 To m_Count
        monTableau(i).bSynchro = m_Current.GetData.bSynchro
        monTableau(i).bTemp = m_Current.GetData.bTemp
        monTableau(i).Decalage = m_Current.GetData.Decalage
        monTableau(i).Dist = m_Current.GetData.Dist
        monTableau(i).Vitesse = m_Current.GetData.Vitesse
        monTableau(i).x = m_Current.GetData.x
        monTableau(i).y = m_Current.GetData.y
        Call MoveNext
    Next i

    Call MoveFirst
    For i = m_Count To 1 Step -1
        m_Current.LetData monTableau(i)
        Call MoveNext
    Next i
End Sub

Public Sub ModifCorde(Corde As Single)
    Dim Rapport As Double
    
    Rapport = (Corde / (maxX - minX))
    
    MoveFirst
    Do
        m_Current.LetX CurrentX * Rapport
        m_Current.LetY CurrentY * Rapport
    Loop Until Not MoveNext
    
    MoveFirst 'Pour ne pas pointer sur rien
    m_bMinMax = False
End Sub

Public Sub Decal(x As Single, y As Single)
    MoveFirst
    Do
        m_Current.LetX CurrentX + x
        m_Current.LetY CurrentY + y
    Loop Until Not MoveNext
    
    MoveFirst 'Pour ne pas pointer sur rien
    m_bMinMax = False
End Sub

Public Sub DecalEpsilon()
    Dim x As Single, y As Single
    MoveFirst
    x = CurrentX
    y = CurrentY
    MoveNext
    Do
        If CurrentX = x Then m_Current.LetX CurrentX + 0.01
        If CurrentY = y Then m_Current.LetY CurrentY + 0.01
        x = CurrentX
        y = CurrentY
    Loop Until Not MoveNext
    
    MoveFirst 'Pour ne pas pointer sur rien
    m_bMinMax = False
End Sub
Public Sub MiroirH(minX As Single, maxX As Single)
    MoveFirst
    Do
        m_Current.LetX maxX - CurrentX + minX
    Loop Until Not MoveNext
    
    MoveFirst 'Pour ne pas pointer sur rien
End Sub

Public Sub MiroirV(minY As Single, maxY As Single)
    MoveFirst
    Do
        m_Current.LetY maxY - CurrentY + minY
    Loop Until Not MoveNext
    
    MoveFirst 'Pour ne pas pointer sur rien
End Sub

Public Sub Imprime()
    Dim i As Long
    MoveFirst
    i = 1
    Do
        Debug.Print i, "X=" & CurrentX, "Y=" & CurrentY, "Vitesse=" & CurrentVitesse
        i = i + 1
    Loop Until Not MoveNext
    
    MoveFirst 'Pour ne pas pointer sur rien
End Sub


