VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMM2001 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "MM2001"
   ClientHeight    =   3030
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   2565
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   2565
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.Frame Frame_MM2001 
      Caption         =   "MM2001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Left            =   0
      TabIndex        =   0
      Tag             =   "64"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   2535
      Begin MSComctlLib.ProgressBar PB_MM2001 
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   2400
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
      End
      Begin VB.CommandButton Cmd_Verif 
         Caption         =   "V�rification firmware"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Tag             =   "67"
         ToolTipText     =   "0"
         Top             =   1800
         Width           =   2295
      End
      Begin VB.CommandButton Cmd_Update 
         Caption         =   "MAJ firmware"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Tag             =   "66"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   2295
      End
      Begin VB.CommandButton Cmd_Version 
         Caption         =   "Version PIC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Tag             =   "65"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label LB_Version 
         Alignment       =   2  'Center
         Caption         =   "Version"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Visible         =   0   'False
         Width           =   2295
      End
   End
End
Attribute VB_Name = "frmMM2001"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim bMAJ As Boolean         'Une MAJ MM2001 est en cours

Private Sub Cmd_Update_Click()
    Dim i As Integer
    Dim FF As Integer
    Dim monEnreg As String
    Dim Nbr As Byte
    Dim NbrTot As Integer   'Nombre d'octets � traiter
    Dim NbrCur As Integer   'Nombre d'octets trait�
    Dim Offset As Integer
    Dim LBA As Long
    Dim Ret As String
    Dim Adr_MM2001 As Long
    Dim Str_Hex As String
    Dim strDiff1 As String, strDiff2 As String
    
    MsgBox ReadINI_Trad(gLangue, "erreurs", 66), vbCritical

    strDiff1 = ReadINI_Trad(gLangue, "erreurs", 63)
    strDiff2 = ReadINI_Trad(gLangue, "erreurs", 64)
    
    'Recherche fichier .hex
    Fichier = RechercheFichierHex(Me, App.Path)
    If Len(Fichier) = 0 Then
        Exit Sub
    End If
    
    bMAJ = True
    
    'Calcul du nombre d'octets � mettre � jour
    FF = FreeFile
    Open Fichier For Input Access Read As FF
    NbrTot = 0
    While Not EOF(FF)
        Line Input #FF, monEnreg
        If Mid$(monEnreg, 8, 2) = "00" Then
            NbrTot = NbrTot + str2Byte(Mid$(monEnreg, 2, 2))
        End If
    Wend
    Close FF
    
    Set gFenetreHID = frmMM2001
    maClasseHID.MM2001_LVP (False)  'Exit mode LVP
    maClasseHID.MM2001_LVP (True)   'Enter mode LVP
    PB_MM2001.Top = Cmd_Update.Top + Cmd_Update.Height + 50
    PB_MM2001.Visible = True
    maClasseHID.MM2001_Erase_Program    'Bulk erase program memory
    Sleep 1000
    maClasseHID.MM2001_Erase_Data    'Bulk erase program memory
    
    'Lecture s�quentielle du fichier
    NbrCur = 0
    FF = FreeFile
    Open Fichier For Input Access Read As FF
    While Not EOF(FF)
        Line Input #FF, monEnreg
        'D�codage de la ligne
        Nbr = str2Byte(Mid$(monEnreg, 2, 2))
        Offset = str2Offset(Mid$(monEnreg, 4, 4))
        
        If Left$(monEnreg, 1) <> ":" Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 62), vbCritical
            GoTo Cmd_Update_Fin
        End If
        Select Case Mid$(monEnreg, 8, 2)
            Case "00"
                If Mid$(monEnreg, 4, 4) = "400E" Then
                    'Configuration word
                    Call maClasseHID.MM2001_Send_CW(str2Mot(Mid$(monEnreg, 10, 4)))
                Else
                    'Ligne de donn�es
                    For i = 0 To Nbr / 2 - 1
                        NbrCur = NbrCur + 2
                        PB_MM2001.value = NbrCur / NbrTot * 100
                        DoEvents
                        Adr_MM2001 = (LBA * 256 + Offset) / 2 + i   'Adresse dans le PIC
                        Str_Hex = Mid$(monEnreg, i * 4 + 10, 4)     'String dans la ligne du .hex
                        If Not maClasseHID.MM2001_Send_Data(Adr_MM2001, str2Mot(Str_Hex)) Then
                            MsgBox strDiff1 & Adr_MM2001 & " MM2001 : " & strDiff2 & "   " & Str_Hex & " ==="
                        End If
                    Next i
                End If
            Case "01"
                'Ligne de fin
                MsgBox ReadINI_Trad(gLangue, "erreurs", 68), vbExclamation
                GoTo Cmd_Update_Fin
            Case "02"
                'SBA : Segment Base Adress
            Case "03"
                'Execution Start Adress (CS+IP)
            Case "04"
                'LBA : Linear Base Adress
                LBA = str2Int(Mid$(monEnreg, 10, 4))
                'Le PIC est en 8 bits, donc sera toujours � 0
            Case "05"
                'Execution Start Adress (EIP)
        End Select
    Wend
Cmd_Update_Fin:
    Close FF
    
    maClasseHID.MM2001_LVP (False)   'Exit mode LVP
    PB_MM2001.Visible = False
    
    bMAJ = False
    
    'Verif
    Cmd_Verif_Click
End Sub

Private Sub Cmd_Verif_Click()
    Dim i As Integer
    Dim FF As Integer
    Dim monEnreg As String
    Dim Nbr As Byte
    Dim NbrTot As Integer   'Nombre d'octets � traiter
    Dim NbrCur As Integer   'Nombre d'octets trait�
    Dim Offset As Integer
    Dim LBA As Long
    Dim Ret As Integer
    Dim Adr_MM2001 As Long
    Dim Str_Hex As String
    Dim strDiff1 As String, strDiff2 As String
    Dim NbrErr As Integer   'Nombre d'erreurs
    Dim strErr As String    'Liste des erreurs
    
    strDiff1 = ReadINI_Trad(gLangue, "erreurs", 63)
    strDiff2 = ReadINI_Trad(gLangue, "erreurs", 64)
    
    If Fichier = "" Then
        'Recherche fichier .hex
        Fichier = RechercheFichierHex(Me, App.Path)
        If Len(Fichier) = 0 Then
            Exit Sub
        End If
        MsgBox ReadINI_Trad(gLangue, "erreurs", 66), vbInformation
    End If
    
    bMAJ = True
    
    'Calcul du nombre d'octets � v�rifier
    FF = FreeFile
    Open Fichier For Input Access Read As FF
    NbrTot = 0
    NbrErr = 0
    strErr = ""
    While Not EOF(FF)
        Line Input #FF, monEnreg
        If Mid$(monEnreg, 8, 2) = "00" Then
            NbrTot = NbrTot + str2Byte(Mid$(monEnreg, 2, 2))
        End If
    Wend
    Close FF
    
    Set gFenetreHID = frmMM2001
    maClasseHID.MM2001_LVP (False)  'Exit mode LVP
    maClasseHID.MM2001_LVP (True)   'Enter mode LVP
    PB_MM2001.Top = Cmd_Verif.Top + Cmd_Verif.Height + 50
    PB_MM2001.Visible = True
    
    'Lecture s�quentielle du fichier
    FF = FreeFile
    Open Fichier For Input Access Read As FF
    While Not EOF(FF)
        Line Input #FF, monEnreg
        'D�codage de la ligne
        Nbr = str2Byte(Mid$(monEnreg, 2, 2))
        Offset = str2Offset(Mid$(monEnreg, 4, 4))
        
        If Left$(monEnreg, 1) <> ":" Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 62), vbCritical
            GoTo Cmd_Verif_Fin
        End If
        Select Case Mid$(monEnreg, 8, 2)
            Case "00"
                'Ligne de donn�es
                If Mid$(monEnreg, 4, 4) = "400E" Then
                    'Configuration word
                    Str_Hex = Mid$(monEnreg, 10, 4) 'String dans la ligne du .hex
                    Ret = maClasseHID.MM2001_Read_CW
                    If Ret <> str2Int(Str_Hex) Then
                        strErr = strErr & strDiff1 & Adr_MM2001 & " MM2001 : " & Ret & "   " & strDiff2 & "   " & Str_Hex
                        strErr = strErr & vbCrLf
                        NbrErr = NbrErr + 1
                        If NbrErr > 10 Then
                            MsgBox strErr, vbCritical
                            GoTo Cmd_Verif_Fin
                        End If
                    End If
                Else
                    For i = 0 To Nbr / 2 - 1
                        NbrCur = NbrCur + 2
                        PB_MM2001.value = NbrCur / NbrTot * 100
                        DoEvents
                        Adr_MM2001 = (LBA * 256 + Offset) / 2 + i 'Adresse dans le PIC
                        Str_Hex = Mid$(monEnreg, i * 4 + 10, 4) 'String dans la ligne du .hex
                        Ret = maClasseHID.MM2001_Read_Data(Adr_MM2001)
                        If Ret <> str2Int(Str_Hex) Then
                            strErr = strErr & strDiff1 & Adr_MM2001 & " MM2001 : " & Ret & "   " & strDiff2 & "   " & Str_Hex
                            strErr = strErr & vbCrLf
                            NbrErr = NbrErr + 1
                            If NbrErr > 16 Then
                                MsgBox strErr, vbCritical
                                GoTo Cmd_Verif_Fin
                            End If
                        End If
                    Next i
                End If
            Case "01"
                'Ligne de fin
                MsgBox ReadINI_Trad(gLangue, "erreurs", 65), vbExclamation
                GoTo Cmd_Verif_Fin
            Case "02"
                'SBA : Segment Base Adress
            Case "03"
                'Execution Start Adress (CS+IP)
            Case "04"
                'LBA : Linear Base Adress
                LBA = str2Int(Mid$(monEnreg, 10, 4))
                'Le PIC est en 8 bits, donc sera toujours � 0
            Case "05"
                'Execution Start Adress (EIP)
        End Select
    Wend
Cmd_Verif_Fin:
    Close FF
    bMAJ = False
    maClasseHID.MM2001_LVP (False)   'Exit mode LVP
    MsgBox ReadINI_Trad(gLangue, "erreurs", 67), vbInformation
    PB_MM2001.Visible = False
End Sub

Private Sub Cmd_Version_Click()
    Set gFenetreHID = frmMM2001
    LB_Version.Visible = True
    LB_Version.Caption = maClasseHID.MM2001_Version
End Sub

Private Sub Form_Load()
    Me.Left = ReadINI_Num(Me.Name & "_Left", 0)
    Me.Top = ReadINI_Num(Me.Name & "_Top", 0)
    PB_MM2001.Visible = False
    Call ChargeTrad(Me)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If bMAJ Then
        Cancel = True
        Exit Sub
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
    End If
End Sub
