VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmBD 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Base de donn�es mat�riaux"
   ClientHeight    =   7350
   ClientLeft      =   150
   ClientTop       =   420
   ClientWidth     =   8940
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7350
   ScaleWidth      =   8940
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.TextBox txtSaisie 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   6360
      TabIndex        =   3
      ToolTipText     =   "0"
      Top             =   6960
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton Btn_Annuler 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5640
      TabIndex        =   2
      Tag             =   "5"
      ToolTipText     =   "tt5"
      Top             =   6960
      Width           =   1215
   End
   Begin VB.CommandButton Btn_OK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6840
      TabIndex        =   1
      Tag             =   "6"
      ToolTipText     =   "tt6"
      Top             =   6960
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   6495
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "0"
      Top             =   0
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   11456
      _Version        =   393216
      Cols            =   8
      AllowBigSelection=   0   'False
      ScrollBars      =   2
      AllowUserResizing=   3
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00C0E0FF&
      Caption         =   "Entr�e pour Valider une cellule"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   4680
      TabIndex        =   7
      Tag             =   "8"
      ToolTipText     =   "0"
      Top             =   6480
      Width           =   3855
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0E0FF&
      Caption         =   "Clic droit pour supprimer/ins�rer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   0
      TabIndex        =   6
      Tag             =   "4"
      ToolTipText     =   "0"
      Top             =   6960
      Width           =   4575
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0E0FF&
      Caption         =   "CTRL+C / CTRL+V pour copier/coller"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   0
      TabIndex        =   5
      Tag             =   "3"
      ToolTipText     =   "0"
      Top             =   6720
      Width           =   4575
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0E0FF&
      Caption         =   "F2 ou clic pour modifier une cellule"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   4
      Tag             =   "2"
      ToolTipText     =   "0"
      Top             =   6480
      Width           =   4575
   End
   Begin VB.Menu mnu_DB 
      Caption         =   "Base de donn�es"
      Visible         =   0   'False
      Begin VB.Menu mnu_DB_Insert 
         Caption         =   "Ins�rer"
      End
      Begin VB.Menu mnu_DB_Delete 
         Caption         =   "Supprimer"
      End
   End
End
Attribute VB_Name = "frmBD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const Blanc = &H80000005
Private Const SAUMON = &HC0E0FF

Private Type Size
        cX As Long
        cY As Long
    End Type

Private Declare Function GetTextExtentPoint32 Lib "gdi32" Alias "GetTextExtentPoint32A" _
        (ByVal hdc As Long, ByVal lpsz As String, _
        ByVal cbString As Long, lpSize As Size) As Long
        
Dim maRowSelect As Integer 'Ligne s�lectionn�e par le clic droit

Private Sub Btn_Annuler_Click()
    Unload Me
End Sub

Private Sub Btn_OK_Click()
    Dim intCol As Integer, intRow As Integer ' Indices de ligne et colonne de W
    Dim monMat As String                      ' La ligne � �crire dans le fichier
    Dim bErr As Boolean
    
    'Contr�le des donn�es
    bErr = False
    With MSFlexGrid1
        ' Pour chaque ligne
        For intRow = .FixedRows To .Rows - 2
            .Row = intRow
            
            'VH
            .Col = 2
            If CDbl(.Text) <= 0 Then
                bErr = True
                Exit For
            End If
            'CH
            .Col = 3
            If CDbl(.Text) <= 0 Then
                bErr = True
                Exit For
            End If
            'VB
            .Col = 4
            If CDbl(.Text) <= 0 Then
                bErr = True
                Exit For
            End If
            'CB
            .Col = 5
            If CDbl(.Text) <= 0 Then
                bErr = True
                Exit For
            End If
            'SH1
            .Col = 6
            If CDbl(.Text) <= 0 Then
                bErr = True
                Exit For
            End If
            'SH2
            .Col = 7
            If CDbl(.Text) <= 0 Then
                bErr = True
                Exit For
            End If
        Next intRow
    End With
    
    If bErr Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 55), vbCritical, App.Title
        MSFlexGrid1.SetFocus
        Exit Sub
    End If
    'Suppression de TOUTES les sections [mat_...]
    Dim Section() As String
    Dim i As Integer
    
    Call ListeSectionIni(App.Title & ".ini", Section)  '-- Param�tre Section pass� ByRef
    For i = LBound(Section) To UBound(Section)
        monMat = Section(i)
        If Left$(monMat, 4) = "mat_" Then
            Call DeleteSectionIni(monMat, App.Path & "\" & App.Title & ".ini")
        End If
    Next

    'Ecriture dans le .ini
    With MSFlexGrid1
        ' Pour chaque ligne
        For intRow = .FixedRows To .Rows - 2
            monMat = .TextMatrix(intRow, 1)
            ' Pour chaque colonne
            For intCol = .FixedCols To .Cols - 1
                ' Ajoute la valeur de la cellule
                Call WriteINI_Machine("mat_" & monMat, .TextMatrix(0, intCol), .TextMatrix(intRow, intCol))
            Next intCol
        Next intRow
    End With
    
    Unload Me
    'Rechargement de la listbox de la fen�tre principale
    frmParamMateriau.Form_Load
End Sub

Private Sub Form_Load()
    Dim idxRow As Long, idxCol As Long, lngMax As Long
    
    ChargeTrad Me
    Me.Left = ReadINI_Num(Me.Name & "_Left", Me.Left)
    Me.Top = ReadINI_Num(Me.Name & "_Top", Me.Top)

    
    MSFlexGrid1.Row = 0
    MSFlexGrid1.Col = 0
    MSFlexGrid1.RowHeight(0) = MSFlexGrid1.RowHeight(0) + MSFlexGrid1.RowHeight(0)
    MSFlexGrid1.TextArray(faIndex(MSFlexGrid1.Row, MSFlexGrid1.Col)) = "   "
    MSFlexGrid1.Col = 1
    MSFlexGrid1.TextArray(faIndex(MSFlexGrid1.Row, MSFlexGrid1.Col)) = Space$(20) & "Mat�riau" & Space$(20)
    MSFlexGrid1.Col = 2
    MSFlexGrid1.TextArray(faIndex(MSFlexGrid1.Row, MSFlexGrid1.Col)) = Space$(5) & "VH" & Space$(5)
    MSFlexGrid1.Col = 3
    MSFlexGrid1.TextArray(faIndex(MSFlexGrid1.Row, MSFlexGrid1.Col)) = Space$(5) & "CH" & Space$(5)
    MSFlexGrid1.Col = 4
    MSFlexGrid1.TextArray(faIndex(MSFlexGrid1.Row, MSFlexGrid1.Col)) = Space$(5) & "VB" & Space$(5)
    MSFlexGrid1.Col = 5
    MSFlexGrid1.TextArray(faIndex(MSFlexGrid1.Row, MSFlexGrid1.Col)) = Space$(5) & "CB" & Space$(5)
    MSFlexGrid1.Col = 6
    MSFlexGrid1.TextArray(faIndex(MSFlexGrid1.Row, MSFlexGrid1.Col)) = Space$(5) & "SH1" & Space$(4)
    MSFlexGrid1.Col = 7
    MSFlexGrid1.TextArray(faIndex(MSFlexGrid1.Row, MSFlexGrid1.Col)) = Space$(5) & "SH2" & Space$(4)
    
    For idxCol = 0 To MSFlexGrid1.Cols - 1
        MSFlexGrid1.Col = idxCol
        MSFlexGrid1.CellAlignment = flexAlignCenterTop
        MSFlexGrid1.WordWrap = True
    Next idxCol
    
    'Dimensionnement des colonnes sur les textes de la premi�re ligne
    Dim texte As String, taille As Size
    With MSFlexGrid1
        ' Parcours des colonnes
        For idxCol = 0 To .Cols - 1
            texte = .TextMatrix(0, idxCol)
            ' Met la taille du texte de la cellule en pixels dans taille
            GetTextExtentPoint32 hdc, texte, Len(texte), taille
            ' Met lngMax en twips en ajoutant 10 pixels pour les marges
            lngMax = (taille.cX + 10) * Screen.TwipsPerPixelX
            ' Applique la largeur de colonne si besoin
            'If lngMax > .ColWidth(idxCol) Then .ColWidth(idxCol) = lngMax
            .ColWidth(idxCol) = lngMax
        Next
    End With
                                
    'Chargement des donn�es
    Dim Section() As String
    Dim monTexte As String
    Dim monMat As String
    Dim i As Integer
    Dim j As Integer
    
    j = 1
    
    Call ListeSectionIni(App.Title & ".ini", Section)  '-- Param�tre Section pass� ByRef
    For i = LBound(Section) To UBound(Section)
        monMat = Section(i)
        If Left$(monMat, 4) = "mat_" Then
            monMat = Mid$(monMat, 5, Len(monMat) - 4)
            monTexte = j & Chr(9) & monMat
            monTexte = monTexte & Chr(9) & ReadINI_Machine_Num("mat_" & monMat, "VH", "0")
            monTexte = monTexte & Chr(9) & ReadINI_Machine_Num("mat_" & monMat, "CH", "0")
            monTexte = monTexte & Chr(9) & ReadINI_Machine_Num("mat_" & monMat, "VB", "0")
            monTexte = monTexte & Chr(9) & ReadINI_Machine_Num("mat_" & monMat, "CB", "0")
            monTexte = monTexte & Chr(9) & ReadINI_Machine_Num("mat_" & monMat, "SH1", "0")
            monTexte = monTexte & Chr(9) & ReadINI_Machine_Num("mat_" & monMat, "SH2", "0")
            MSFlexGrid1.AddItem monTexte, j
                    
            MSFlexGrid1.Row = j
            For idxCol = 0 To MSFlexGrid1.Cols - 1
                MSFlexGrid1.Col = idxCol
                MSFlexGrid1.CellAlignment = flexAlignCenterTop
            Next idxCol

            j = j + 1
        End If
    Next
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Me.WindowState <> vbMinimized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
    End If
End Sub

Private Sub mnu_DB_Delete_Click()
    Dim idxCol As Integer
    Dim idxRow As Integer

    MSFlexGrid1.RemoveItem maRowSelect
    'Il faut "d�caler" les lignes suivantes
    '   C'est � dire d�cr�menter les n� de ligne
    For idxRow = maRowSelect To MSFlexGrid1.Rows - 2
        MSFlexGrid1.Col = 0
        MSFlexGrid1.Row = idxRow
        MSFlexGrid1.Text = Val(MSFlexGrid1.Text) - 1
    Next idxRow
End Sub

Private Sub mnu_DB_Insert_Click()
    Dim idxCol As Integer
    Dim idxRow As Integer
    
    MSFlexGrid1.AddItem "", maRowSelect
    MSFlexGrid1.Col = 0
    MSFlexGrid1.Row = maRowSelect
    MSFlexGrid1.Text = maRowSelect
    For idxCol = 0 To MSFlexGrid1.Cols - 1
        MSFlexGrid1.Col = idxCol
        MSFlexGrid1.CellAlignment = flexAlignCenterTop
    Next idxCol
    
    'Il faut "d�caler" les lignes suivantes
    '   C'est � dire incr�menter les n� de ligne
    For idxRow = maRowSelect + 1 To MSFlexGrid1.Rows - 2
        MSFlexGrid1.Col = 0
        MSFlexGrid1.Row = idxRow
        MSFlexGrid1.Text = Val(MSFlexGrid1.Text) + 1
    Next idxRow
    
    MSFlexGrid1.Row = maRowSelect
    MSFlexGrid1.Col = 1
End Sub

Private Sub MSFlexGrid1_EnterCell()
    If MSFlexGrid1.Row <> 0 Then
        MSFlexGrid1.CellBackColor = SAUMON
    End If
End Sub

Private Sub MSFlexGrid1_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim Col_Max As Integer
    Dim Ligne_Max As Integer
    Dim n_Col As Integer
    Dim n_Ligne As Integer
    Dim sCellule As String
    Dim sCol As String
    Dim sLigne As String
    Dim i As Integer
    Select Case KeyCode
        Case 9
            'Si SHIFT+TAB
            If Shift = 1 Then
                'On revient a la cellule precedente
                Col_Max = MSFlexGrid1.FixedCols
                Ligne_Max = MSFlexGrid1.FixedRows
                n_Col = MSFlexGrid1.Col
                n_Ligne = MSFlexGrid1.Row
                'En v�rifiant de ne pas s'arreter sur une colonne masqu�e
                Do
                    If n_Col = Col_Max Then
                        If n_Ligne = Ligne_Max Then Exit Sub
                        MSFlexGrid1.Row = MSFlexGrid1.Row - 1
                        MSFlexGrid1.Col = MSFlexGrid1.Cols - 1
                    Else
                        MSFlexGrid1.Col = MSFlexGrid1.Col - 1
                    End If
                    n_Col = MSFlexGrid1.Col
                    n_Ligne = MSFlexGrid1.Row
                Loop Until MSFlexGrid1.ColWidth(MSFlexGrid1.Col) > 0
            Else
                'On passe a la cellule d'apr�s
                Col_Max = MSFlexGrid1.Cols - 1
                Ligne_Max = MSFlexGrid1.Rows - 1
                n_Col = MSFlexGrid1.Col
                n_Ligne = MSFlexGrid1.Row
                'En v�rifiant de ne pas s'arreter sur une colonne masqu�e
                Do
                    If n_Col = Col_Max Then
                        If n_Ligne = Ligne_Max Then MSFlexGrid1.Rows = MSFlexGrid1.Rows + 1
                        MSFlexGrid1.Row = MSFlexGrid1.Row + 1
                        MSFlexGrid1.Col = MSFlexGrid1.FixedCols
                    Else
                        MSFlexGrid1.Col = MSFlexGrid1.Col + 1
                    End If
                    n_Col = MSFlexGrid1.Col
                    n_Ligne = MSFlexGrid1.Row
                Loop Until MSFlexGrid1.ColWidth(MSFlexGrid1.Col) > 0
            End If
        Case 46   'Touche SUPPR
            MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, MSFlexGrid1.Col) = ""
        Case vbKeyF2
            'Pour le contenu de la cellule
            'On place la textbox sur la cellule pour pouvoir modifier le text
            txtSaisie.Visible = True
            txtSaisie.Left = MSFlexGrid1.CellLeft
            txtSaisie.Top = MSFlexGrid1.CellTop
            txtSaisie.Height = MSFlexGrid1.CellHeight
            txtSaisie.Width = MSFlexGrid1.CellWidth
            'txtSaisie.Alignment = MSFlexGrid1.CellAlignment
            txtSaisie.Alignment = vbCenter
            txtSaisie.Text = MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, MSFlexGrid1.Col)
            txtSaisie.SelStart = Len(txtSaisie.Text)
            txtSaisie.SetFocus
        Case Else
            
    End Select
End Sub

Private Sub MSFlexGrid1_KeyPress(KeyAscii As Integer)
    Dim Col_Max As Integer
    Dim Ligne_Max As Integer
    Dim n_Col As Integer
    Dim n_Ligne As Integer
    'On r�cup�re la touche qui a �t� press�e
    Select Case KeyAscii
        Case 3
            'Fonction COPIER
            Call Clipboard.SetText(MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, MSFlexGrid1.Col))
        Case 8          'Si {BACKSPACE}
            'On place la textbox VIDE sur la cellule pour pouvoir saisir le texte
            txtSaisie.Visible = True
            txtSaisie.Left = MSFlexGrid1.CellLeft
            txtSaisie.Top = MSFlexGrid1.CellTop
            txtSaisie.Height = MSFlexGrid1.CellHeight
            txtSaisie.Width = MSFlexGrid1.CellWidth
            txtSaisie.Alignment = vbCenter
            txtSaisie.SelStart = 1
            txtSaisie.SetFocus
        Case 9
            'TAB ou SHIFT+TAB
            'D�j� g�r� par KeyDown donc on fait rien
        Case 13      'Si {ENTRER}
            'On passe a la cellule d'apr�s
            Col_Max = MSFlexGrid1.Cols - 1
            Ligne_Max = MSFlexGrid1.Rows - 1
            n_Col = MSFlexGrid1.Col
            n_Ligne = MSFlexGrid1.Row
            'En v�rifiant de ne pas s'arreter sur une colonne masqu�e
            Do
                If n_Col = Col_Max Then
                    If n_Ligne = Ligne_Max Then MSFlexGrid1.Rows = MSFlexGrid1.Rows + 1
                    MSFlexGrid1.Row = MSFlexGrid1.Row + 1
                    MSFlexGrid1.Col = MSFlexGrid1.FixedCols
                Else
                    MSFlexGrid1.Col = MSFlexGrid1.Col + 1
                End If
                n_Col = MSFlexGrid1.Col
                n_Ligne = MSFlexGrid1.Row
            Loop Until MSFlexGrid1.ColWidth(MSFlexGrid1.Col) > 0
        Case 22
            'Fonction COLLER
            MSFlexGrid1.Text = Clipboard.GetText
            'msflexgrid1.TextMatrix(msflexgrid1.Row, msflexgrid1.Col) = Clipboard.GetText
        Case 27         'Si {ECHAP}
            'On ne fait rien
        Case Else       'Pour les autres touches
            'On place la textbox sur la cellule pour pouvoir saisir le texte
            txtSaisie.Visible = True
            txtSaisie.Left = MSFlexGrid1.CellLeft
            txtSaisie.Top = MSFlexGrid1.CellTop
            txtSaisie.Height = MSFlexGrid1.CellHeight
            txtSaisie.Width = MSFlexGrid1.CellWidth
            'txtSaisie.Alignment = MSFlexGrid1.CellAlignment
            txtSaisie.Alignment = vbCenter
            txtSaisie.Text = Chr$(KeyAscii)
            txtSaisie.SelStart = 1
            txtSaisie.SetFocus
    End Select
End Sub

' Cette fonction permet de traiter les donn�es plus facilement
Function faIndex(Row As Integer, Col As Integer) As Long
    faIndex = Row * MSFlexGrid1.Cols + Col
End Function

Private Sub MSFlexGrid1_LeaveCell()
    MSFlexGrid1.CellBackColor = Blanc
End Sub

Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton Then
        'Menu contextuel : Ins�rer ou Supprimer
        If MSFlexGrid1.MouseRow > 0 And MSFlexGrid1.MouseRow < MSFlexGrid1.Rows - 1 Then
            maRowSelect = MSFlexGrid1.MouseRow
            PopupMenu mnu_DB
        End If
    End If
End Sub

Private Sub MSFlexGrid1_SelChange()
    Dim maRow As Integer
    Dim maCol As Integer
    Dim idxCol As Integer
    
    maRow = MSFlexGrid1.Row
    maCol = MSFlexGrid1.Col
    If MSFlexGrid1.RowSel = MSFlexGrid1.Rows - 1 Then
        'On utilise la derni�re ligne disponible, il faut donc en afficher une nouvelle
        MSFlexGrid1.AddItem "", MSFlexGrid1.Rows
        MSFlexGrid1.Col = 0
        MSFlexGrid1.Text = maRow
        For idxCol = 0 To MSFlexGrid1.Cols - 1
            MSFlexGrid1.Col = idxCol
            MSFlexGrid1.CellAlignment = flexAlignCenterTop
        Next idxCol

        MSFlexGrid1.RowSel = maRow
        MSFlexGrid1.ColSel = maCol
        
    End If
End Sub

' Exporter le contenu d'une msflexgrid dans un fichier :
'Cette fonction exporte le contenu de la msflexgrid pass�e en param�tre dans le fichier strFileName.
'Les colonnes sont s�par�es par le caract�re pass� dans le troisi�me param�tre.
 Function ExportGridToFile(Mygrid As MSFlexGrid, ByVal strFileName As String, _
    Optional ByVal strSep As String = vbTab) As Boolean
    Dim intFreeFile As Integer               ' Num�ro du fichier
    Dim intCol As Integer, intRow As Integer ' Indices de ligne et colonne de W
    Dim ligne As String                      ' La ligne � �crire dans le fichier
    ' Gestion des �ventuelles erreurs
    On Error GoTo ExportGridToFile_ERR
    ' Prend le prochain num�ro de fichier
    intFreeFile = FreeFile
    ' Ouvre le fichier en bloquant son acc�s aux autres applications
    Open strFileName For Output Access Write Lock Read Write As #intFreeFile
    With Mygrid
        ' Pour chaque ligne
        For intRow = .FixedRows To .Rows - 1
            ligne = ""
            ' Pour chaque colonne
            For intCol = .FixedCols To .Cols - 1
                ' Ajoute la valeur de la cellule
                ligne = ligne & .TextMatrix(intRow, intCol) & strSep
            Next intCol
            ' Enl�ve le s�parateur final
            If strSep <> "" Then ligne = Left(ligne, Len(ligne) - 1)
            Print #intFreeFile, ligne
        Next intRow
    End With
    ' Valide le bon fonctionnement de la fonction
    ExportGridToFile = True
ExportGridToFile_FIN:
    Close #intFreeFile + 1
    Exit Function
ExportGridToFile_ERR:
    ExportGridToFile = False
    Resume ExportGridToFile_FIN
End Function

Private Sub txtSaisie_KeyPress(KeyAscii As Integer)
   'On r�cup�re les touches saisies sur la textbox
    Select Case KeyAscii
        Case 13     'Si {ENTRER}
            'On place le text dans la cellule qui a appel� la textbox
            MSFlexGrid1.TextMatrix(MSFlexGrid1.Row, MSFlexGrid1.Col) = Replace(txtSaisie.Text, ".", ",")
            txtSaisie.Text = ""
            txtSaisie.Visible = False
            Call MSFlexGrid1_KeyPress(13)
        Case 27     'Si {ECHAP}
            'On revient � la cellule sans placer le texte
            txtSaisie.Text = ""
            txtSaisie.Visible = False
    End Select
End Sub
