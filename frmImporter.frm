VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmImporter 
   Caption         =   "Cr�er une d�coupe"
   ClientHeight    =   10950
   ClientLeft      =   225
   ClientTop       =   555
   ClientWidth     =   20250
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   10950
   ScaleWidth      =   20250
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Tag             =   "1"
   Begin VB.ComboBox Combo_Zoom 
      Height          =   315
      Index           =   2
      ItemData        =   "frmImporter.frx":0000
      Left            =   8520
      List            =   "frmImporter.frx":0002
      TabIndex        =   186
      Tag             =   "0"
      Text            =   "Zoom"
      ToolTipText     =   "0"
      Top             =   360
      Width           =   1335
   End
   Begin VB.ComboBox Combo_Zoom 
      Height          =   315
      Index           =   1
      ItemData        =   "frmImporter.frx":0004
      Left            =   8520
      List            =   "frmImporter.frx":0006
      TabIndex        =   185
      Tag             =   "0"
      Text            =   "Zoom"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   1335
   End
   Begin VB.Frame Frame_5 
      Caption         =   "Commentaires"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   18000
      TabIndex        =   170
      Tag             =   "93"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   2295
      Begin VB.TextBox TB_Comm 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   171
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame Frame_4 
      Caption         =   "Position de la d�coupe � l'emplanture (mm)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3750
      Index           =   1
      Left            =   3600
      TabIndex        =   123
      Tag             =   "70"
      ToolTipText     =   "0"
      Top             =   720
      Width           =   9885
      Begin VB.CommandButton cmdMBA 
         Height          =   270
         Index           =   4
         Left            =   2805
         Picture         =   "frmImporter.frx":0008
         Style           =   1  'Graphical
         TabIndex        =   140
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3375
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   300
         Index           =   3
         Left            =   2520
         Picture         =   "frmImporter.frx":0357
         Style           =   1  'Graphical
         TabIndex        =   139
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3375
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   270
         Index           =   2
         Left            =   2805
         Picture         =   "frmImporter.frx":06AD
         Style           =   1  'Graphical
         TabIndex        =   138
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3105
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   300
         Index           =   1
         Left            =   2520
         Picture         =   "frmImporter.frx":09FE
         Style           =   1  'Graphical
         TabIndex        =   137
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3075
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   300
         Index           =   1
         Left            =   7455
         Picture         =   "frmImporter.frx":0D55
         Style           =   1  'Graphical
         TabIndex        =   136
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3060
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   270
         Index           =   2
         Left            =   7740
         Picture         =   "frmImporter.frx":10AC
         Style           =   1  'Graphical
         TabIndex        =   135
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3090
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   300
         Index           =   3
         Left            =   7455
         Picture         =   "frmImporter.frx":13FD
         Style           =   1  'Graphical
         TabIndex        =   134
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3360
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   270
         Index           =   4
         Left            =   7740
         Picture         =   "frmImporter.frx":1753
         Style           =   1  'Graphical
         TabIndex        =   133
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3360
         Width           =   280
      End
      Begin VB.CheckBox Check2Cotes 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Index           =   1
         Left            =   8685
         Picture         =   "frmImporter.frx":1AA4
         Style           =   1  'Graphical
         TabIndex        =   132
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3075
         Width           =   1080
      End
      Begin VB.TextBox TB_BAH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   135
         TabIndex        =   131
         ToolTipText     =   "0"
         Top             =   630
         Width           =   825
      End
      Begin VB.TextBox TB_BAB 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   135
         TabIndex        =   130
         ToolTipText     =   "0"
         Top             =   1650
         Width           =   825
      End
      Begin VB.TextBox TB_MargeBF 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   6435
         TabIndex        =   129
         ToolTipText     =   "0"
         Top             =   3300
         Width           =   825
      End
      Begin VB.TextBox TB_MargeBA 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   1500
         TabIndex        =   128
         ToolTipText     =   "0"
         Top             =   3285
         Width           =   825
      End
      Begin VB.TextBox TB_BFB 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   8925
         TabIndex        =   127
         ToolTipText     =   "0"
         Top             =   1710
         Width           =   825
      End
      Begin VB.TextBox TB_BFH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   8925
         TabIndex        =   126
         ToolTipText     =   "0"
         Top             =   750
         Width           =   825
      End
      Begin VB.TextBox TB_MDessous 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   7860
         TabIndex        =   125
         ToolTipText     =   "0"
         Top             =   2070
         Width           =   825
      End
      Begin VB.TextBox TB_MDessus 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   7860
         TabIndex        =   124
         ToolTipText     =   "0"
         Top             =   405
         Width           =   825
      End
      Begin VB.PictureBox PB_Bloc 
         AutoRedraw      =   -1  'True
         Enabled         =   0   'False
         Height          =   3405
         Index           =   1
         Left            =   60
         ScaleHeight     =   3345
         ScaleWidth      =   9690
         TabIndex        =   141
         Top             =   240
         Width           =   9750
      End
   End
   Begin VB.Frame Frame_4 
      Caption         =   "Position de la d�coupe au saumon (mm)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3750
      Index           =   2
      Left            =   3600
      TabIndex        =   142
      Tag             =   "80"
      ToolTipText     =   "0"
      Top             =   4560
      Width           =   9885
      Begin VB.CommandButton cmdMBA 
         Height          =   270
         Index           =   8
         Left            =   2820
         Picture         =   "frmImporter.frx":1EA4
         Style           =   1  'Graphical
         TabIndex        =   159
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3390
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   300
         Index           =   7
         Left            =   2520
         Picture         =   "frmImporter.frx":21F3
         Style           =   1  'Graphical
         TabIndex        =   158
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3390
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   270
         Index           =   6
         Left            =   2820
         Picture         =   "frmImporter.frx":2549
         Style           =   1  'Graphical
         TabIndex        =   157
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3120
         Width           =   280
      End
      Begin VB.CommandButton cmdMBA 
         Height          =   300
         Index           =   5
         Left            =   2535
         Picture         =   "frmImporter.frx":289A
         Style           =   1  'Graphical
         TabIndex        =   156
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3090
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   270
         Index           =   8
         Left            =   7755
         Picture         =   "frmImporter.frx":2BF1
         Style           =   1  'Graphical
         TabIndex        =   155
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3360
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   300
         Index           =   7
         Left            =   7470
         Picture         =   "frmImporter.frx":2F42
         Style           =   1  'Graphical
         TabIndex        =   154
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3360
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   270
         Index           =   6
         Left            =   7755
         Picture         =   "frmImporter.frx":3298
         Style           =   1  'Graphical
         TabIndex        =   153
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3090
         Width           =   280
      End
      Begin VB.CommandButton cmdMBF 
         Height          =   300
         Index           =   5
         Left            =   7470
         Picture         =   "frmImporter.frx":35E9
         Style           =   1  'Graphical
         TabIndex        =   152
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3060
         Width           =   280
      End
      Begin VB.CheckBox Check2Cotes 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   555
         Index           =   2
         Left            =   8685
         Picture         =   "frmImporter.frx":3940
         Style           =   1  'Graphical
         TabIndex        =   151
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   3075
         Width           =   1080
      End
      Begin VB.TextBox TB_MDessus 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   7860
         TabIndex        =   150
         ToolTipText     =   "0"
         Top             =   405
         Width           =   825
      End
      Begin VB.TextBox TB_MDessous 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   7860
         TabIndex        =   149
         ToolTipText     =   "0"
         Top             =   2070
         Width           =   825
      End
      Begin VB.TextBox TB_BFH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   8925
         TabIndex        =   148
         ToolTipText     =   "0"
         Top             =   750
         Width           =   825
      End
      Begin VB.TextBox TB_BFB 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   8925
         TabIndex        =   147
         ToolTipText     =   "0"
         Top             =   1710
         Width           =   825
      End
      Begin VB.TextBox TB_MargeBA 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   1500
         TabIndex        =   146
         ToolTipText     =   "0"
         Top             =   3300
         Width           =   825
      End
      Begin VB.TextBox TB_MargeBF 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   6435
         TabIndex        =   145
         ToolTipText     =   "0"
         Top             =   3300
         Width           =   825
      End
      Begin VB.TextBox TB_BAB 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   135
         TabIndex        =   144
         ToolTipText     =   "0"
         Top             =   1650
         Width           =   825
      End
      Begin VB.TextBox TB_BAH 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   135
         TabIndex        =   143
         ToolTipText     =   "0"
         Top             =   630
         Width           =   825
      End
      Begin VB.PictureBox PB_Bloc 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   3405
         Index           =   2
         Left            =   60
         ScaleHeight     =   3405
         ScaleWidth      =   9750
         TabIndex        =   160
         Top             =   240
         Width           =   9750
      End
   End
   Begin VB.Frame Frame_47 
      BorderStyle     =   0  'None
      Height          =   1215
      Left            =   120
      TabIndex        =   121
      ToolTipText     =   "0"
      Top             =   7560
      Width           =   3255
      Begin VB.CommandButton CmdRectangulaire 
         Caption         =   "Bloc rectangulaire"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   167
         Tag             =   "92"
         ToolTipText     =   "0"
         Top             =   720
         Width           =   3255
      End
      Begin VB.CheckBox chkProp 
         Caption         =   "Marges BA et BF du saumon proportionnelles � celles de l'emplanture"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   0
         TabIndex        =   122
         Tag             =   "90"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   3225
      End
   End
   Begin VB.Frame Frame_46 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   120
      TabIndex        =   119
      ToolTipText     =   "0"
      Top             =   2160
      Width           =   1335
      Begin VB.CommandButton cmdInit_5 
         Height          =   390
         Left            =   0
         Picture         =   "frmImporter.frx":3D4D
         Style           =   1  'Graphical
         TabIndex        =   120
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   1335
      End
   End
   Begin VB.Frame Frame_44 
      Caption         =   "Dimensions du bloc"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4830
      Left            =   120
      TabIndex        =   105
      Tag             =   "62"
      ToolTipText     =   "0"
      Top             =   2640
      Width           =   3340
      Begin VB.PictureBox PB_3D 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   2580
         Left            =   60
         ScaleHeight     =   2580
         ScaleWidth      =   3225
         TabIndex        =   106
         Top             =   2130
         Width           =   3225
      End
      Begin VB.Label lblLong 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1420
         TabIndex        =   118
         ToolTipText     =   "0"
         Top             =   1850
         Width           =   1770
      End
      Begin VB.Label lblEp 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1425
         TabIndex        =   117
         ToolTipText     =   "0"
         Top             =   1560
         Width           =   1770
      End
      Begin VB.Label lblFBF 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1420
         TabIndex        =   116
         ToolTipText     =   "0"
         Top             =   1250
         Width           =   1770
      End
      Begin VB.Label lblFBA 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1440
         TabIndex        =   115
         ToolTipText     =   "0"
         Top             =   945
         Width           =   1770
      End
      Begin VB.Label lbl 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   1420
         TabIndex        =   114
         ToolTipText     =   "0"
         Top             =   650
         Width           =   1770
      End
      Begin VB.Label lbl 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   1420
         TabIndex        =   113
         ToolTipText     =   "0"
         Top             =   350
         Width           =   1770
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Saumon :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   112
         Tag             =   "64"
         ToolTipText     =   "0"
         Top             =   650
         Width           =   1200
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Epaisseur :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   111
         Tag             =   "67"
         ToolTipText     =   "0"
         Top             =   1550
         Width           =   1200
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Longueur :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   110
         Tag             =   "68"
         ToolTipText     =   "0"
         Top             =   1850
         Width           =   1200
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fl�che BA :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   109
         Tag             =   "65"
         ToolTipText     =   "0"
         Top             =   950
         Width           =   1200
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Fl�che BF :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   108
         Tag             =   "66"
         ToolTipText     =   "0"
         Top             =   1250
         Width           =   1200
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Emplanture :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   107
         Tag             =   "63"
         ToolTipText     =   "0"
         Top             =   350
         Width           =   1200
      End
   End
   Begin VB.Frame Frame_42 
      Caption         =   "D�placement"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1995
      Left            =   1560
      TabIndex        =   92
      Tag             =   "61"
      ToolTipText     =   "0"
      Top             =   600
      Width           =   1950
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   4
         Left            =   1455
         Picture         =   "frmImporter.frx":40E2
         Style           =   1  'Graphical
         TabIndex        =   104
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   570
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   5
         Left            =   1455
         Picture         =   "frmImporter.frx":443A
         Style           =   1  'Graphical
         TabIndex        =   103
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   900
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   6
         Left            =   1455
         Picture         =   "frmImporter.frx":478F
         Style           =   1  'Graphical
         TabIndex        =   102
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1230
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   12
         Left            =   135
         Picture         =   "frmImporter.frx":4ADC
         Style           =   1  'Graphical
         TabIndex        =   101
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   570
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   11
         Left            =   135
         Picture         =   "frmImporter.frx":4E32
         Style           =   1  'Graphical
         TabIndex        =   100
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   900
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   10
         Left            =   135
         Picture         =   "frmImporter.frx":5188
         Style           =   1  'Graphical
         TabIndex        =   99
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1230
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   7
         Left            =   1125
         Picture         =   "frmImporter.frx":54D5
         Style           =   1  'Graphical
         TabIndex        =   98
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1560
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   8
         Left            =   795
         Picture         =   "frmImporter.frx":5822
         Style           =   1  'Graphical
         TabIndex        =   97
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1560
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   3
         Left            =   1125
         Picture         =   "frmImporter.frx":5B78
         Style           =   1  'Graphical
         TabIndex        =   96
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   2
         Left            =   795
         Picture         =   "frmImporter.frx":5EC5
         Style           =   1  'Graphical
         TabIndex        =   95
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   9
         Left            =   465
         Picture         =   "frmImporter.frx":6218
         Style           =   1  'Graphical
         TabIndex        =   94
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1560
         Width           =   330
      End
      Begin VB.CommandButton cmdDep 
         Height          =   330
         Index           =   1
         Left            =   465
         Picture         =   "frmImporter.frx":6572
         Style           =   1  'Graphical
         TabIndex        =   93
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   330
      End
   End
   Begin VB.Frame Frame_41 
      Caption         =   "Epaisseur"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   120
      TabIndex        =   84
      Tag             =   "60"
      ToolTipText     =   "0"
      Top             =   600
      Width           =   1350
      Begin VB.CommandButton cmdEp 
         Height          =   270
         Index           =   6
         Left            =   840
         Picture         =   "frmImporter.frx":68CD
         Style           =   1  'Graphical
         TabIndex        =   91
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1155
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   300
         Index           =   5
         Left            =   555
         Picture         =   "frmImporter.frx":6C1A
         Style           =   1  'Graphical
         TabIndex        =   90
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1155
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   270
         Index           =   3
         Left            =   840
         Picture         =   "frmImporter.frx":6F70
         Style           =   1  'Graphical
         TabIndex        =   89
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   885
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   300
         Index           =   2
         Left            =   555
         Picture         =   "frmImporter.frx":72BD
         Style           =   1  'Graphical
         TabIndex        =   88
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   855
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   330
         Index           =   4
         Left            =   270
         Picture         =   "frmImporter.frx":7610
         Style           =   1  'Graphical
         TabIndex        =   87
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1155
         Width           =   280
      End
      Begin VB.CommandButton cmdEp 
         Height          =   330
         Index           =   1
         Left            =   270
         Picture         =   "frmImporter.frx":796A
         Style           =   1  'Graphical
         TabIndex        =   86
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   825
         Width           =   280
      End
      Begin VB.TextBox TB_Epaisseur 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   255
         TabIndex        =   85
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   840
      End
   End
   Begin VB.Frame Frame_45 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   120
      TabIndex        =   82
      ToolTipText     =   "0"
      Top             =   120
      Width           =   10095
      Begin VB.Label Label_Aide_4 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Vous pouvez r�gler ici la position de la d�coupe dans le bloc"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   0
         TabIndex        =   83
         Tag             =   "91"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   9330
      End
   End
   Begin VB.Frame Frame_3 
      Caption         =   "Vue de c�t�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3375
      Index           =   2
      Left            =   24480
      TabIndex        =   71
      Tag             =   "46"
      ToolTipText     =   "0"
      Top             =   7920
      Width           =   13305
      Begin VB.TextBox TB_Diedre 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   12000
         TabIndex        =   79
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   2280
         Width           =   1155
      End
      Begin VB.CommandButton CB_Pos 
         Height          =   500
         Index           =   1
         Left            =   11040
         Picture         =   "frmImporter.frx":7CC5
         Style           =   1  'Graphical
         TabIndex        =   78
         Tag             =   "0"
         ToolTipText     =   "tt41"
         Top             =   480
         Width           =   840
      End
      Begin VB.CommandButton CB_Pos 
         Height          =   500
         Index           =   2
         Left            =   11040
         Picture         =   "frmImporter.frx":84CF
         Style           =   1  'Graphical
         TabIndex        =   77
         Tag             =   "0"
         ToolTipText     =   "tt42"
         Top             =   975
         Width           =   840
      End
      Begin VB.CommandButton CB_Pos 
         Height          =   500
         Index           =   4
         Left            =   11040
         Picture         =   "frmImporter.frx":8BAD
         Style           =   1  'Graphical
         TabIndex        =   76
         Tag             =   "0"
         ToolTipText     =   "tt44"
         Top             =   1965
         Width           =   840
      End
      Begin VB.CommandButton CB_Pos 
         Height          =   500
         Index           =   5
         Left            =   11040
         Picture         =   "frmImporter.frx":928B
         Style           =   1  'Graphical
         TabIndex        =   75
         Tag             =   "0"
         ToolTipText     =   "tt45"
         Top             =   2460
         Width           =   840
      End
      Begin VB.CommandButton CB_Pos 
         Height          =   500
         Index           =   3
         Left            =   11040
         Picture         =   "frmImporter.frx":9A95
         Style           =   1  'Graphical
         TabIndex        =   74
         Tag             =   "0"
         ToolTipText     =   "tt43"
         Top             =   1470
         Width           =   840
      End
      Begin VB.PictureBox PB_ProfilCote 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   2955
         Left            =   120
         ScaleHeight     =   2955
         ScaleWidth      =   7935
         TabIndex        =   73
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   7935
      End
      Begin VB.TextBox TB_Decalage 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   12000
         TabIndex        =   72
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   1320
         Width           =   1155
      End
      Begin VB.Label Label3_4 
         Alignment       =   2  'Center
         Caption         =   "Di�dre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   12000
         TabIndex        =   81
         Tag             =   "48"
         ToolTipText     =   "0"
         Top             =   1920
         Width           =   1155
      End
      Begin VB.Label Label3_3 
         Alignment       =   2  'Center
         Caption         =   "D�calage vertical du saumon en mm :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   12000
         TabIndex        =   80
         Tag             =   "47"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1155
      End
   End
   Begin VB.Frame Frame_3 
      Caption         =   "Vue du dessus"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Index           =   1
      Left            =   24480
      TabIndex        =   59
      Tag             =   "42"
      ToolTipText     =   "0"
      Top             =   4200
      Width           =   13305
      Begin VB.CommandButton CB_Fleche 
         Caption         =   "BA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   10920
         TabIndex        =   66
         Tag             =   "51"
         ToolTipText     =   "tt51"
         Top             =   720
         Width           =   735
      End
      Begin VB.PictureBox PB_Vue_Dessus 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   2655
         Left            =   120
         ScaleHeight     =   2655
         ScaleWidth      =   7935
         TabIndex        =   65
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   7935
      End
      Begin VB.TextBox TB_Fleche_BA 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   11760
         TabIndex        =   64
         ToolTipText     =   "0"
         Top             =   1080
         Width           =   1395
      End
      Begin VB.TextBox TB_Fleche_BF 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   11760
         TabIndex        =   63
         ToolTipText     =   "0"
         Top             =   2910
         Width           =   1395
      End
      Begin VB.TextBox TB_Envergure 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5220
         TabIndex        =   62
         ToolTipText     =   "0"
         Top             =   225
         Width           =   780
      End
      Begin VB.CommandButton CB_Fleche 
         Caption         =   "Centre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   10920
         TabIndex        =   61
         Tag             =   "52"
         ToolTipText     =   "tt52"
         Top             =   1560
         Width           =   735
      End
      Begin VB.CommandButton CB_Fleche 
         Caption         =   "BF"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   3
         Left            =   10920
         TabIndex        =   60
         Tag             =   "53"
         ToolTipText     =   "tt53"
         Top             =   2400
         Width           =   735
      End
      Begin VB.Label LabelErrEnvergure 
         Caption         =   "LabelErrEnvergure"
         Height          =   255
         Left            =   6240
         TabIndex        =   70
         Tag             =   "49"
         Top             =   240
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label Label3_1 
         Alignment       =   2  'Center
         Caption         =   "Fl�che au bord d'attaque en mm:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   11760
         TabIndex        =   69
         Tag             =   "44"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1395
      End
      Begin VB.Label Label3_2 
         Alignment       =   2  'Center
         Caption         =   "Fl�che au bord de fuite en mm :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   11760
         TabIndex        =   68
         Tag             =   "45"
         ToolTipText     =   "0"
         Top             =   2160
         Width           =   1395
      End
      Begin VB.Label Label28 
         Alignment       =   1  'Right Justify
         Caption         =   "Distance entre l'emplanture et le saumon (mm) :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   570
         TabIndex        =   67
         Tag             =   "43"
         Top             =   285
         Width           =   4545
      End
   End
   Begin VB.Frame Frame_31 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   24960
      TabIndex        =   57
      ToolTipText     =   "0"
      Top             =   3720
      Width           =   10455
      Begin VB.Label Label_Aide_3 
         BackColor       =   &H00E0E0E0&
         Caption         =   $"frmImporter.frx":A173
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   0
         TabIndex        =   58
         Tag             =   "41"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   9450
      End
   End
   Begin VB.Frame Frame_2 
      Caption         =   "Saumon"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4125
      Index           =   2
      Left            =   19560
      TabIndex        =   40
      Tag             =   "17"
      ToolTipText     =   "0"
      Top             =   7560
      Width           =   4575
      Begin VB.CommandButton CB_Save_Profil_Deform 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   2760
         Picture         =   "frmImporter.frx":A23F
         Style           =   1  'Graphical
         TabIndex        =   174
         Tag             =   "0"
         ToolTipText     =   "tt20"
         Top             =   360
         Width           =   375
      End
      Begin VB.Frame Frame_25 
         Caption         =   "Dimension horizontale"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   2
         Left            =   120
         TabIndex        =   163
         Tag             =   "22"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   2535
         Begin VB.TextBox TB_Corde 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   2
            Left            =   1080
            TabIndex        =   164
            ToolTipText     =   "0"
            Top             =   240
            Width           =   855
         End
      End
      Begin VB.Frame Frame_21 
         Caption         =   "Dimension verticale"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   2
         Left            =   120
         TabIndex        =   50
         Tag             =   "23"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   3135
         Begin VB.TextBox TB_DimMult 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   2
            Left            =   2640
            TabIndex        =   53
            Text            =   "1"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   375
         End
         Begin VB.TextBox TB_DimMod 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   2
            Left            =   960
            TabIndex        =   52
            ToolTipText     =   "0"
            Top             =   720
            Width           =   855
         End
         Begin VB.TextBox TB_H 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   285
            Index           =   2
            Left            =   960
            TabIndex        =   51
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label_22 
            Alignment       =   1  'Right Justify
            Caption         =   "�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   2160
            TabIndex        =   56
            Tag             =   "26"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   285
         End
         Begin VB.Label Label_23 
            Alignment       =   1  'Right Justify
            Caption         =   "Modifi�e :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   45
            TabIndex        =   55
            Tag             =   "25"
            Top             =   720
            Width           =   855
         End
         Begin VB.Label Label_21 
            Alignment       =   1  'Right Justify
            Caption         =   "Initiale :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   165
            TabIndex        =   54
            Tag             =   "24"
            Top             =   330
            Width           =   735
         End
      End
      Begin VB.PictureBox PB_Profil1 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   3375
         Index           =   2
         Left            =   3330
         ScaleHeight     =   3375
         ScaleWidth      =   1290
         TabIndex        =   49
         ToolTipText     =   "0"
         Top             =   360
         Width           =   1290
      End
      Begin VB.Frame Frame_22 
         Caption         =   "Rotation"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   2
         Left            =   120
         TabIndex        =   44
         Tag             =   "27"
         ToolTipText     =   "0"
         Top             =   3000
         Width           =   3135
         Begin VB.HScrollBar HScroll_Rot 
            Height          =   255
            Index           =   2
            LargeChange     =   5
            Left            =   120
            Max             =   180
            Min             =   -180
            TabIndex        =   46
            Top             =   240
            Width           =   2895
         End
         Begin VB.TextBox TB_Angle_Atteint 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   2
            Left            =   1440
            TabIndex        =   45
            Tag             =   "31"
            ToolTipText     =   "tt31"
            Top             =   600
            Width           =   735
         End
         Begin VB.Label Label_25 
            Caption         =   "degr�s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   2280
            TabIndex        =   48
            Tag             =   "28"
            Top             =   600
            Width           =   750
         End
         Begin VB.Label Label_24 
            Caption         =   "Angle atteint :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   120
            TabIndex        =   47
            Tag             =   "31"
            ToolTipText     =   "0"
            Top             =   600
            Width           =   1290
         End
      End
      Begin VB.Frame Frame_24 
         Caption         =   "Sym�trie"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   2
         Left            =   120
         TabIndex        =   41
         Tag             =   "32"
         ToolTipText     =   "0"
         Top             =   2160
         Width           =   3135
         Begin VB.CommandButton CB_Miroir_Vertical 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   2
            Left            =   2280
            Picture         =   "frmImporter.frx":A389
            Style           =   1  'Graphical
            TabIndex        =   43
            Tag             =   "0"
            ToolTipText     =   "tt33"
            Top             =   240
            Width           =   465
         End
         Begin VB.CommandButton CB_Miroir_Horizontal 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   2
            Left            =   1200
            Picture         =   "frmImporter.frx":AC53
            Style           =   1  'Graphical
            TabIndex        =   42
            Tag             =   "0"
            ToolTipText     =   "tt32"
            Top             =   240
            Width           =   465
         End
      End
   End
   Begin VB.Frame Frame_2 
      Caption         =   "Emplanture"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4125
      Index           =   1
      Left            =   19560
      TabIndex        =   23
      Tag             =   "13"
      ToolTipText     =   "0"
      Top             =   3360
      Width           =   4575
      Begin VB.CommandButton CB_Save_Profil_Deform 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   2760
         Picture         =   "frmImporter.frx":B51D
         Style           =   1  'Graphical
         TabIndex        =   173
         Tag             =   "0"
         ToolTipText     =   "tt20"
         Top             =   480
         Width           =   375
      End
      Begin VB.Frame Frame_25 
         Caption         =   "Dimension horizontale"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   1
         Left            =   120
         TabIndex        =   161
         Tag             =   "22"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   2535
         Begin VB.TextBox TB_Corde 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   1080
            TabIndex        =   162
            ToolTipText     =   "0"
            Top             =   240
            Width           =   855
         End
      End
      Begin VB.Frame Frame_22 
         Caption         =   "Rotation"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   1
         Left            =   120
         TabIndex        =   35
         Tag             =   "27"
         ToolTipText     =   "0"
         Top             =   3000
         Width           =   3135
         Begin VB.HScrollBar HScroll_Rot 
            Height          =   255
            Index           =   1
            LargeChange     =   5
            Left            =   120
            Max             =   180
            Min             =   -180
            TabIndex        =   37
            Top             =   240
            Width           =   2895
         End
         Begin VB.TextBox TB_Angle_Atteint 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   1
            Left            =   1425
            TabIndex        =   36
            Tag             =   "31"
            ToolTipText     =   "tt31"
            Top             =   570
            Width           =   735
         End
         Begin VB.Label Label_24 
            Caption         =   "Angle atteint :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   120
            TabIndex        =   39
            Tag             =   "31"
            ToolTipText     =   "0"
            Top             =   570
            Width           =   1290
         End
         Begin VB.Label Label_25 
            Caption         =   "degr�s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   2265
            TabIndex        =   38
            Tag             =   "28"
            Top             =   585
            Width           =   750
         End
      End
      Begin VB.Frame Frame_21 
         Caption         =   "Dimension verticale"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Index           =   1
         Left            =   120
         TabIndex        =   28
         Tag             =   "23"
         ToolTipText     =   "0"
         Top             =   960
         Width           =   3135
         Begin VB.TextBox TB_H 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   285
            Index           =   1
            Left            =   1080
            TabIndex        =   31
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox TB_DimMod 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   1080
            TabIndex        =   30
            ToolTipText     =   "0"
            Top             =   720
            Width           =   855
         End
         Begin VB.TextBox TB_DimMult 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   1
            Left            =   2640
            TabIndex        =   29
            Text            =   "1"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   375
         End
         Begin VB.Label Label_21 
            Alignment       =   1  'Right Justify
            Caption         =   "Initiale :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   45
            TabIndex        =   34
            Tag             =   "24"
            Top             =   330
            Width           =   930
         End
         Begin VB.Label Label_23 
            Alignment       =   1  'Right Justify
            Caption         =   "Modifi�e :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   45
            TabIndex        =   33
            Tag             =   "25"
            Top             =   720
            Width           =   930
         End
         Begin VB.Label Label_22 
            Alignment       =   1  'Right Justify
            Caption         =   "�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   2160
            TabIndex        =   32
            Tag             =   "26"
            ToolTipText     =   "0"
            Top             =   360
            Width           =   285
         End
      End
      Begin VB.PictureBox PB_Profil1 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         Height          =   3510
         Index           =   1
         Left            =   3330
         ScaleHeight     =   3510
         ScaleWidth      =   690
         TabIndex        =   27
         ToolTipText     =   "0"
         Top             =   180
         Width           =   690
      End
      Begin VB.Frame Frame_24 
         Caption         =   "Sym�trie"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Index           =   1
         Left            =   120
         TabIndex        =   24
         Tag             =   "32"
         ToolTipText     =   "0"
         Top             =   2160
         Width           =   3135
         Begin VB.CommandButton CB_Miroir_Horizontal 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   1
            Left            =   1200
            Picture         =   "frmImporter.frx":B667
            Style           =   1  'Graphical
            TabIndex        =   26
            Tag             =   "0"
            ToolTipText     =   "tt32"
            Top             =   240
            Width           =   465
         End
         Begin VB.CommandButton CB_Miroir_Vertical 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   1
            Left            =   2280
            Picture         =   "frmImporter.frx":BF31
            Style           =   1  'Graphical
            TabIndex        =   25
            Tag             =   "0"
            ToolTipText     =   "tt33"
            Top             =   240
            Width           =   465
         End
      End
   End
   Begin VB.Frame Frame_23 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   20040
      TabIndex        =   21
      ToolTipText     =   "0"
      Top             =   2640
      Width           =   9495
      Begin VB.Label Label_Aide_2 
         BackColor       =   &H00E0E0E0&
         Caption         =   $"frmImporter.frx":C7FB
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   0
         TabIndex        =   22
         Tag             =   "21"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   9330
      End
   End
   Begin VB.Frame Frame_1 
      Caption         =   "Saumon"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3210
      Index           =   2
      Left            =   16200
      TabIndex        =   16
      Tag             =   "17"
      ToolTipText     =   "0"
      Top             =   5400
      Width           =   3480
      Begin VB.CheckBox CB_Identique 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   1080
         Picture         =   "frmImporter.frx":C890
         Style           =   1  'Graphical
         TabIndex        =   188
         Tag             =   "0"
         ToolTipText     =   "tt4"
         Top             =   600
         Width           =   600
      End
      Begin VB.CheckBox CB_ZoomSelection 
         Height          =   495
         Index           =   2
         Left            =   1680
         Picture         =   "frmImporter.frx":CC90
         Style           =   1  'Graphical
         TabIndex        =   184
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton CB_Zoom1 
         Height          =   495
         Index           =   2
         Left            =   2160
         Picture         =   "frmImporter.frx":D342
         Style           =   1  'Graphical
         TabIndex        =   182
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton CB_ZoomPlus 
         Height          =   495
         Index           =   2
         Left            =   600
         Picture         =   "frmImporter.frx":D9F4
         Style           =   1  'Graphical
         TabIndex        =   181
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton CB_ZoomMoins 
         Height          =   495
         Index           =   2
         Left            =   120
         Picture         =   "frmImporter.frx":E0A6
         Style           =   1  'Graphical
         TabIndex        =   180
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton CB_Save_Profil_Orig 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   1920
         Picture         =   "frmImporter.frx":E758
         Style           =   1  'Graphical
         TabIndex        =   169
         Tag             =   "0"
         ToolTipText     =   "tt20"
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton CB_Inversion 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   2280
         Picture         =   "frmImporter.frx":E8A2
         Style           =   1  'Graphical
         TabIndex        =   19
         Tag             =   "0"
         ToolTipText     =   "tt18"
         Top             =   240
         Width           =   375
      End
      Begin VB.PictureBox PB_Synchro 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3300
         Index           =   2
         Left            =   2880
         ScaleHeight     =   3300
         ScaleWidth      =   7365
         TabIndex        =   18
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   7365
      End
      Begin VB.CommandButton CB_Importer 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   120
         Picture         =   "frmImporter.frx":E9EC
         Style           =   1  'Graphical
         TabIndex        =   17
         Tag             =   "0"
         ToolTipText     =   "tt14"
         Top             =   240
         Width           =   375
      End
      Begin MSComctlLib.ListView LV_Synchro 
         Height          =   1575
         Index           =   2
         Left            =   120
         TabIndex        =   20
         Top             =   1080
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   2778
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "111"
            Text            =   "__"
            Object.Width           =   441
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Object.Tag             =   "112"
            Text            =   "Pt"
            Object.Width           =   794
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "113"
            Text            =   "X"
            Object.Width           =   1341
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "114"
            Text            =   "Y"
            Object.Width           =   1341
         EndProperty
      End
      Begin VB.Label Lbl_PtsSynchro 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   480
         TabIndex        =   166
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Frame Frame_1 
      Caption         =   "Emplanture"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2970
      Index           =   1
      Left            =   16200
      TabIndex        =   11
      Tag             =   "13"
      ToolTipText     =   "0"
      Top             =   2400
      Width           =   3000
      Begin VB.CheckBox CB_Identique 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   1080
         Picture         =   "frmImporter.frx":EB36
         Style           =   1  'Graphical
         TabIndex        =   187
         Tag             =   "0"
         ToolTipText     =   "tt3"
         Top             =   600
         Width           =   600
      End
      Begin VB.CheckBox CB_ZoomSelection 
         Height          =   495
         Index           =   1
         Left            =   1680
         Picture         =   "frmImporter.frx":EF36
         Style           =   1  'Graphical
         TabIndex        =   183
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton CB_Zoom1 
         Height          =   495
         Index           =   1
         Left            =   2160
         Picture         =   "frmImporter.frx":F5E8
         Style           =   1  'Graphical
         TabIndex        =   179
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton CB_ZoomPlus 
         Height          =   495
         Index           =   1
         Left            =   600
         Picture         =   "frmImporter.frx":FC9A
         Style           =   1  'Graphical
         TabIndex        =   178
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton CB_ZoomMoins 
         Height          =   495
         Index           =   1
         Left            =   120
         Picture         =   "frmImporter.frx":1034C
         Style           =   1  'Graphical
         TabIndex        =   177
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   600
         Width           =   495
      End
      Begin VB.CommandButton CB_Save_Profil_Orig 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   1920
         Picture         =   "frmImporter.frx":109FE
         Style           =   1  'Graphical
         TabIndex        =   168
         Tag             =   "0"
         ToolTipText     =   "tt20"
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton CB_Inversion 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   2280
         Picture         =   "frmImporter.frx":10B48
         Style           =   1  'Graphical
         TabIndex        =   14
         Tag             =   "0"
         ToolTipText     =   "tt18"
         Top             =   240
         Width           =   375
      End
      Begin VB.PictureBox PB_Synchro 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3300
         Index           =   1
         Left            =   2880
         ScaleHeight     =   3300
         ScaleWidth      =   7365
         TabIndex        =   13
         Tag             =   "0"
         ToolTipText     =   "tt67"
         Top             =   180
         Width           =   7365
      End
      Begin VB.CommandButton CB_Importer 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   120
         Picture         =   "frmImporter.frx":10C92
         Style           =   1  'Graphical
         TabIndex        =   12
         Tag             =   "0"
         ToolTipText     =   "tt14"
         Top             =   240
         Width           =   375
      End
      Begin MSComctlLib.ListView LV_Synchro 
         Height          =   1455
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   1080
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   2566
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   0
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Tag             =   "111"
            Text            =   "__"
            Object.Width           =   441
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Object.Tag             =   "112"
            Text            =   "Pt"
            Object.Width           =   794
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Object.Tag             =   "113"
            Text            =   "X"
            Object.Width           =   1341
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Object.Tag             =   "114"
            Text            =   "Y"
            Object.Width           =   1341
         EndProperty
      End
      Begin VB.Label Lbl_PtsSynchro 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   480
         TabIndex        =   165
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.Frame Frame_12 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   16080
      TabIndex        =   4
      ToolTipText     =   "0"
      Top             =   1920
      Width           =   10335
      Begin VB.CommandButton CB_FC_COMP 
         Caption         =   "<-"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   8040
         TabIndex        =   176
         Tag             =   "0"
         ToolTipText     =   "tt107"
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton CB_FC_COMP 
         Caption         =   "->"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   8040
         TabIndex        =   175
         Tag             =   "0"
         ToolTipText     =   "tt108"
         Top             =   0
         Width           =   375
      End
      Begin VB.CommandButton CB_Nettoyage 
         Enabled         =   0   'False
         Height          =   495
         Left            =   6960
         Picture         =   "frmImporter.frx":10DDC
         Style           =   1  'Graphical
         TabIndex        =   172
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   615
      End
      Begin VB.CheckBox CB_Num 
         Height          =   495
         Left            =   5760
         Picture         =   "frmImporter.frx":114C6
         Style           =   1  'Graphical
         TabIndex        =   10
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   615
      End
      Begin VB.CheckBox CB_Points 
         Height          =   495
         Left            =   6240
         Picture         =   "frmImporter.frx":118BA
         Style           =   1  'Graphical
         TabIndex        =   9
         Tag             =   "0"
         ToolTipText     =   "0"
         Top             =   0
         Width           =   735
      End
      Begin VB.CommandButton RAZButton 
         Caption         =   "RAZ"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4500
         TabIndex        =   8
         Tag             =   "104"
         ToolTipText     =   "tt104"
         Top             =   0
         Width           =   855
      End
      Begin VB.CommandButton SynchroButton 
         Caption         =   "Synchronisation"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2580
         TabIndex        =   7
         Tag             =   "103"
         ToolTipText     =   "tt103"
         Top             =   0
         Width           =   1920
      End
      Begin VB.PictureBox PB_Help 
         Height          =   330
         Left            =   5400
         Picture         =   "frmImporter.frx":11C90
         ScaleHeight     =   270
         ScaleWidth      =   255
         TabIndex        =   6
         ToolTipText     =   "tt60"
         Top             =   0
         Width           =   315
      End
      Begin VB.CommandButton CB_Synchro 
         Caption         =   "Points de synchro auto."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -30
         TabIndex        =   5
         Tag             =   "102"
         ToolTipText     =   "tt102"
         Top             =   0
         Width           =   2610
      End
      Begin VB.Image Image2 
         Height          =   480
         Left            =   8400
         Picture         =   "frmImporter.frx":11DDA
         Top             =   0
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   7560
         Picture         =   "frmImporter.frx":126A4
         Top             =   0
         Width           =   480
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   1455
      Left            =   10800
      TabIndex        =   3
      Top             =   120
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   2566
      TabWidthStyle   =   2
      MultiRow        =   -1  'True
      TabFixedWidth   =   1411
      TabMinWidth     =   0
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   5
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Profils/Cordes/Sens"
            Object.Tag             =   "2_0"
            Object.ToolTipText     =   "2_0"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "D�formation/Rotation"
            Object.Tag             =   "2_1"
            Object.ToolTipText     =   "2_1"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Largeur/Positions"
            Object.Tag             =   "2_2"
            Object.ToolTipText     =   "2_2"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Bloc"
            Object.Tag             =   "2_3"
            Object.ToolTipText     =   "2_3"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Commentaires"
            Object.Tag             =   "2_4"
            Object.ToolTipText     =   "2_4"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.StatusBar SB1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   10695
      Width           =   20250
      _ExtentX        =   35719
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   9
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel9 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton CB_Defaut 
      BackColor       =   &H80000007&
      Caption         =   "Valeurs par d�faut"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   10200
      TabIndex        =   1
      Tag             =   "16"
      ToolTipText     =   "16"
      Top             =   8880
      Width           =   1815
   End
   Begin VB.CommandButton CB_OK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   12000
      TabIndex        =   0
      Tag             =   "990"
      ToolTipText     =   "tt990"
      Top             =   8880
      Width           =   1455
   End
   Begin VB.Image ImageMain 
      Height          =   480
      Left            =   0
      Picture         =   "frmImporter.frx":13BBE
      Tag             =   "0"
      ToolTipText     =   "0"
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Menu mnu_ctx 
      Caption         =   "1001"
      Visible         =   0   'False
      Begin VB.Menu mnuCtx_PtCle 
         Caption         =   "1002"
      End
      Begin VB.Menu mnuCtx_Origine 
         Caption         =   "1003"
      End
      Begin VB.Menu mnuCtx_Suppression 
         Caption         =   "1004"
      End
   End
End
Attribute VB_Name = "frmImporter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim bSav As Boolean
Dim i As Long
Dim k As Integer
Dim Largeur(EMPLANTURE To SAUMON) As Long
Dim Hauteur(EMPLANTURE To SAUMON) As Long
Dim Largeur1(EMPLANTURE To SAUMON) As Long
Dim Hauteur1(EMPLANTURE To SAUMON) As Long
Dim Gauche1(EMPLANTURE To SAUMON) As Long
Dim Haut1(EMPLANTURE To SAUMON) As Long
Dim Largeur2 As Long
Dim Hauteur2 As Long
Dim Gauche2 As Long
Dim Haut2 As Long
Dim bPoints As Boolean
Dim bNum As Boolean
'Dim Zoom As Integer
Dim bModif As Boolean
Dim monIndex As Integer
Dim bLancementComplexes As Boolean      'Faut-il lancer Complexes ?

'Gestion du zoom dans l'onglet 0
Dim X0(EMPLANTURE To SAUMON) As Single, Y0(EMPLANTURE To SAUMON) As Single
Dim XcentreI(EMPLANTURE To SAUMON) As Single, YcentreI(EMPLANTURE To SAUMON) As Single
Dim CoeffInit(EMPLANTURE To SAUMON) As Single 'facteur d'adaptation � l'affichage total
Dim X1Zoom(EMPLANTURE To SAUMON) As Single, Y1Zoom(EMPLANTURE To SAUMON) As Single  'Cadre de zoom
Dim X2Zoom(EMPLANTURE To SAUMON) As Single, Y2Zoom(EMPLANTURE To SAUMON) As Single
Dim XcentreZ(EMPLANTURE To SAUMON) As Single, YcentreZ(EMPLANTURE To SAUMON) As Single
Dim MemoShift(EMPLANTURE To SAUMON) As Integer
Dim X1Pan(EMPLANTURE To SAUMON) As Single, Y1Pan(EMPLANTURE To SAUMON) As Single 'Origine du d�placement de la figure
Dim MemoX1Pan(EMPLANTURE To SAUMON) As Single, MemoY1Pan(EMPLANTURE To SAUMON) As Single
Dim MemoCoeffInit(EMPLANTURE To SAUMON) As Single 'M�morisation pour retour � zoom 1

'Onglet Bloc
'Affichage des deux c�t�s dans l'onglet bloc
Dim DeuxCotes(EMPLANTURE To SAUMON) As Single

Dim x2 As Single, x1 As Single, y2 As Single, y1 As Single

Dim P_mini(EMPLANTURE To SAUMON) As Point
Dim P_maxi(EMPLANTURE To SAUMON) As Point

Dim decalageX As Single, decalageY As Single, limiteX, LargeurZone, HauteurZone   As Single
Dim CentrageX As Single, CentrageY As Single
Dim limiteY As Single
Dim largeurReel As Single, hauteurReel As Single
Dim Rlargeur As Double, Rhauteur As Double, Rapport As Double

Dim P_BA(EMPLANTURE To SAUMON) As Point
Dim P_BF(EMPLANTURE To SAUMON) As Point
Dim P_sup(EMPLANTURE To SAUMON) As Point
Dim P_inf(EMPLANTURE To SAUMON) As Point

Dim DeltaEpsilon As Double

Private Sub CB_Identique_Click(Index As Integer)
    'Recopie de saumon � emplanture ou inversement
    'Index : 1 = Emplanture, 2 = Saumon
    Dim monElem As TypeNoeud
    
    Set gListePointSynchro(Index) = New ClsListeChainee
    
    gListePointSynchro(3 - Index).MoveFirst
    Do
        monElem.bSynchro = gListePointSynchro(3 - Index).CurrentbSynchro
        monElem.bTemp = gListePointSynchro(3 - Index).CurrentbTemp
        monElem.Decalage = gListePointSynchro(3 - Index).CurrentDecalage
        monElem.Dist = gListePointSynchro(3 - Index).CurrentDist
        monElem.iOrigine = gListePointSynchro(3 - Index).CurrentOrigine
        monElem.x = gListePointSynchro(3 - Index).CurrentX
        monElem.y = gListePointSynchro(3 - Index).CurrentY
        monElem.Vitesse = gListePointSynchro(3 - Index).CurrentVitesse
        
        gListePointSynchro(Index).InsertAfter monElem
    Loop While gListePointSynchro(3 - Index).MoveNext

    gListePointSynchro(Index).MoveFirst 'Pour ne pas pointer sur rien

    nbPoints(Index) = nbPoints(3 - Index)
    wCorde(Index) = wCorde(3 - Index)
    TB_Corde(Index) = TB_Corde(3 - Index)
    wH(Index) = wH(3 - Index)
    TB_H(Index) = TB_H(3 - Index)
    TB_DimMod(Index) = TB_DimMod(3 - Index)
    Call RemplLV
    Call CalculZoom
End Sub

Private Sub CB_Nettoyage_Click()
'O� sont les donn�es  : gListePointSynchro(k)
    Dim monTableau1() As Point
    Dim monTableau2() As PointPLT
    Dim nb As Long
    Dim k As Integer
    Dim i As Long
    Dim monElem As TypeNoeud
    
    If gListePointSynchro(EMPLANTURE).count = gListePointSynchro(SAUMON).count Then
        k = MsgBox(ReadINI_Trad(ReadINI("Language", "en"), "erreurs", 114), vbQuestion + vbYesNoCancel)
    Else
        k = vbNo
    End If
    
    Select Case k
        Case vbNo   'Nettoyage sans synchro
            'Suppression des points en double et des points align�s
            For k = EMPLANTURE To SAUMON
                ReDim monTableau2(0 To gListePointSynchro(k).count - 1)
                nb = 0
                gListePointSynchro(k).MoveFirst
                Do
                    monTableau2(nb).x = gListePointSynchro(k).CurrentPoint.x
                    monTableau2(nb).y = gListePointSynchro(k).CurrentPoint.y
                    nb = nb + 1
                Loop While gListePointSynchro(k).MoveNext
                
'                nb = NettoyageFichier(monTableau2(), Resolution(gstrTable))
                nb = NettoyageFichier(monTableau2(), 0.1)
                
                Set gListePointSynchro(k) = New ClsListeChainee
                For i = 0 To UBound(monTableau2)
                    monElem.x = monTableau2(i).x
                    monElem.y = monTableau2(i).y
                    gListePointSynchro(k).Insert monElem
                    gListePointSynchro(k).MoveNext
                Next i
                gListePointSynchro(k).MoveFirst 'Pour ne pas pointer sur rien
                nbPoints(k) = gListePointSynchro(k).count
            Next k
            Call RAZButton_Click
        Case vbYes  'Nettoyage avec synchro
            ReDim monTableau1(EMPLANTURE To SAUMON, 0 To nbPoints(EMPLANTURE) - 1)
            'Parcours de gListePointSynchro(k)
            For k = EMPLANTURE To SAUMON
                nb = 0
                gListePointSynchro(k).MoveFirst
                Do
                    monTableau1(k, nb) = gListePointSynchro(k).CurrentPoint
                    nb = nb + 1
                Loop While gListePointSynchro(k).MoveNext
            Next k
            'Appel au nettoyage synchro :
            nb = NettoyageSynchro(monTableau1(), Resolution(gstrTable))
'            nb = NettoyageFichier(monTableau2(), 0.1)
            If nb <> 0 Then
                ReDim Preserve monTableau1(1 To 2, 0 To nb - 1)
            End If
        
            For k = EMPLANTURE To SAUMON
                Set gListePointSynchro(k) = New ClsListeChainee
                For i = 0 To UBound(monTableau1, 2)
                    monElem.x = monTableau1(k, i).x
                    monElem.y = monTableau1(k, i).y
                    gListePointSynchro(k).Insert monElem
                    gListePointSynchro(k).MoveNext
                Next i
                gListePointSynchro(k).MoveFirst 'Pour ne pas pointer sur rien
                nbPoints(k) = gListePointSynchro(k).count
            Next k
            Call RemplLV
            bProfilCharge = True
            Call Form_Resize
    End Select
End Sub

Private Sub CB_FC_Comp_Click(Index As Integer)
    Dim Tableau() As PointPLT
    
    'Faut-il lancer Complexes ?
    If Not IsRunning("Complexes.exe") Then
        'Recherche si pr�sent
        If Dir(App.Path & "/Complexes.exe") <> "" Then
            Shell App.Path & "/Complexes.exe"
        Else
            MsgBox ReadINI_Trad(gLangue, "erreurs", 112), vbExclamation, "Erreur"
        End If
    End If

    'R�cup�ration ou cr�ation des fichiers temporaires pour Complexes
    Dim k As Integer
    Dim monTab() As String
    Dim Comment As String
    Dim xMin, xMax, Ymin, Ymax As Single
    Dim TempPoints As Long
    Dim MonFichier As String
    Dim maCorde As Single
    Dim monEnreg As String
    Dim monMilieu(EMPLANTURE To SAUMON) As Single
    
    If Index = 1 Then   'RP-FC vers Complexes
        'Cr�ation des fichiers
        For k = EMPLANTURE To SAUMON
            MonFichier = App.Path & "\Temp" & IIf(k = EMPLANTURE, "E", "S") & ".dat"
            monEnreg = IIf(k = EMPLANTURE, "E", "S")
            monEnreg = monEnreg & ";" & wCorde(k)
            monEnreg = monEnreg & ";" & wFleche_BA
            monEnreg = monEnreg & ";" & wDessous(SAUMON) - wDessous(EMPLANTURE)
            monEnreg = monEnreg & ";" & wEnvergure
            monEnreg = monEnreg & ";" & wFichier(k)
        
            ReDim Tableau(0 To gListePointSynchro(k).count - 1)
            gListePointSynchro(k).MoveFirst
            i = 0
            Do
                Tableau(i).x = gListePointSynchro(k).CurrentPoint.x
                Tableau(i).y = gListePointSynchro(k).CurrentPoint.y
                i = i + 1
            Loop Until Not gListePointSynchro(k).MoveNext
        
            TempPoints = EcrireFichier(Tableau, MonFichier & vbNullChar, monEnreg)
        Next k
        Exit Sub
    End If
    
    'Complexes vers FC : r�cup�ration des fichiers
    If Dir(App.Path & "\TempE.dat", vbHidden) = "" Or _
        Dir(App.Path & "\TempS.dat", vbHidden) = "" Then Exit Sub
    For k = EMPLANTURE To SAUMON
        MonFichier = App.Path & "\Temp" & IIf(k = EMPLANTURE, "E", "S") & ".dat"
        Comment = Space$(260)
        ReDim monTableauLecture(0 To 0)
        TempPoints = LireFichier(monTableauLecture(), MonFichier & vbNullChar, xMin, xMax, Ymin, Ymax, Comment)
        monMilieu(k) = (Ymax - Ymin) / 2#
    
        If TempPoints < 2 Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", -TempPoints), vbCritical
            Exit Sub
        End If
        
        'Contr�le : pas d'aller-retour !
        Dim Aod1 As Double, Aod2 As Double
        Dim P1 As Point, P2 As Point, P3 As Point
        P1.x = monTableauLecture(0).x
        P1.y = monTableauLecture(0).y
        P2.x = monTableauLecture(1).x
        P2.y = monTableauLecture(1).y
        Call Calcul_Ang_Segment(P1, P2, Aod1)
        For i = 2 To TempPoints - 1
            P3.x = monTableauLecture(i).x
            P3.y = monTableauLecture(i).y
            Call Calcul_Ang_Segment(P2, P3, Aod2)
        
            If Abs(Abs(Aod1 - Aod2) - PI) < 0.000001 Then
                MsgBox ReadINI_Trad(gLangue, "erreurs", 200) & " " & i & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 201), vbCritical
            End If
            Aod1 = Aod2
            P1 = P2
            P2 = P3
        Next i
        
        monTab = Split(Trim$(Comment), ";")
        
        maCorde = Replace(monTab(1), ".", Format$(0, "#.#"))
        wDessous(IIf(monTab(0) = "E", EMPLANTURE, SAUMON)) = Replace(monTab(3), ".", Format$(0, "#.#"))
    
        Call RecopieVersListe(monTableauLecture, gListePointSynchro(k), maCorde)
        nbPoints(k) = gListePointSynchro(k).count
        wCorde(k) = maCorde
        wMargeBA(k) = 5#
        wMargeBF(k) = 5#
        wH(k) = gListePointSynchro(k).maxY - gListePointSynchro(k).minY
        TB_H(k) = Round(wH(k), 2)
        TB_DimMod(k) = TB_H(k)
        wFichier(k) = monTab(5)
        Kill MonFichier
    Next k
    
    wFleche_BA = Replace(monTab(2), ".", Format$(0, "#.#"))
    wDessous(EMPLANTURE) = 0#
    wDessous(SAUMON) = Replace(monTab(3), ".", Format$(0, "#.#"))
    wEnvergure = Replace(monTab(4), ".", Format$(0, "#.#"))
    
    bProfilCharge = False
    
    Call RemplLV
    bProfilCharge = True
    Call WriteINI("FichierFC", MonFichier)
    Call Form_Resize
End Sub

Private Sub CB_Defaut_Click()
    Call ValeurDefaut
    Call Form_Resize
End Sub

Private Sub CB_Fleche_Click(Index As Integer)
    bSav = bProfilCharge
    bProfilCharge = False
    Select Case Index
        Case 1          'Alignement au BA
            TB_Fleche_BA = 0
            TB_Fleche_BF = Round((P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x) - TB_Fleche_BA - _
                (P_maxi(SAUMON).x - P_mini(SAUMON).x), 1)
        Case 2          'Alignement centr�
            TB_Fleche_BA = Round(((P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x) - _
                         (P_maxi(SAUMON).x - P_mini(SAUMON).x)) / 2, 1)
            TB_Fleche_BF = Round(TB_Corde(EMPLANTURE) - TB_Fleche_BA - TB_Corde(SAUMON), 1)
        Case 3          'Alignement au BF
            TB_Fleche_BA = Round((P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x) - TB_Fleche_BF - _
                (P_maxi(SAUMON).x - P_mini(SAUMON).x), 1)
            TB_Fleche_BF = 0
    End Select
    wFleche_BA = TB_Fleche_BA
    wFleche_BF = TB_Fleche_BF
    bProfilCharge = bSav
    Call Form_Resize
End Sub

Private Sub CB_Importer_Click(Index As Integer)
    If Importer_Fichier(Index) Then
        Call CalculZoom
    End If
End Sub

Public Sub CalculZoom()
    Dim x1 As Single, y1 As Single, x2 As Single, y2 As Single

    For k = EMPLANTURE To SAUMON
        x1 = gListePointSynchro(k).minX
        x2 = gListePointSynchro(k).maxX
        y1 = gListePointSynchro(k).minY
        y2 = gListePointSynchro(k).maxY
        x1 = x1 - DeltaEpsilon * (x2 - x1)
        y1 = y1 - DeltaEpsilon * (y2 - y1)
        x2 = x2 + DeltaEpsilon * (x2 - x1)
        y2 = y2 + DeltaEpsilon * (y2 - y1)
        XcentreI(k) = (x2 + x1) / 2
        YcentreI(k) = (y2 + y1) / 2
        MemoCoeffInit(k) = 1#
        CoeffInit(k) = 1#
    Next k
    Call Form_Resize
End Sub

Private Sub CB_Inversion_Click(Index As Integer)
    If Not bProfilCharge Then Exit Sub
    gListePointSynchro(Index).Inversion
    Call RemplLV
    Call Form_Resize
End Sub

Private Sub CB_Miroir_Horizontal_Click(Index As Integer)
    bMiroirHorizontal(Index) = Not bMiroirHorizontal(Index)
    Call Form_Resize
End Sub

Private Sub CB_Miroir_Vertical_Click(Index As Integer)
    bMiroirVertical(Index) = Not bMiroirVertical(Index)
    Call Form_Resize
End Sub

Private Sub CB_OK_Click()
    Dim bSynchro As Boolean
    
    If Val(lblLong.Caption) <> 0# Then
        bMemoBloc = True
        gwLargeurEmplanture = Val(lbl(EMPLANTURE).Caption)
        gwLargeurSaumon = Val(lbl(SAUMON).Caption)
        gwFlecheBA = Val(lblFBA.Caption)
        gwFlecheBF = Val(lblFBF.Caption)
        gwEpaisseur = Val(lblEp.Caption)
        gwLongueur = Val(lblLong.Caption)
    End If
    
    If Not bProfilCharge Then
        Me.Hide
        Exit Sub
    End If
    
    bSynchro = False
    If nbPoints(EMPLANTURE) <> nbPoints(SAUMON) Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 60), vbCritical
        bSynchro = True
    Else
        'If wFichier(EMPLANTURE) <> wFichier(SAUMON) Then
        '    MsgBox ReadINI_Trad(glangue, "erreurs", 61), vbCritical
        '    bSynchro = True
        'End If
    End If
    
    wEnvergure = CDbl(Replace(TB_Envergure, ".", Format$(0#, ".")))
    wComm = TB_Comm
    For k = EMPLANTURE To SAUMON
        wDessous(k) = CDbl(Replace(TB_MDessous(k), ".", Format$(0#, ".")))
    Next k
    
    Call Form_Resize            'Pour forcer le recalcul de Tableau1
    Me.Hide
    
    bSaveFC = True
    
    frmMain.mnuFileCut.Enabled = Not bSynchro
    frmMain.CB_Decoupe.Enabled = Not bSynchro
End Sub

Private Sub CB_Points_Click()
    bPoints = (CB_Points.value = vbChecked)
    Call Dessine_Onglet0
End Sub

Private Sub CB_Num_Click()
    bNum = (CB_Num.value = vbChecked)
    Call Dessine_Onglet0
End Sub

Private Sub CB_Pos_Click(Index As Integer)
    Dim YE As Single
    Dim YS As Single
    Dim PMini(EMPLANTURE To SAUMON) As Point, PMaxi(EMPLANTURE To SAUMON) As Point
    Dim k As Integer
    'Repositionnement des deux profils
    '1 : Haut
    '2 : sur BA
    '3 : Centre
    '4 : sur BF
    '5 : Bas
    
    For k = EMPLANTURE To SAUMON
        PMini(k) = monTableau1(k, 0)
        PMaxi(k) = PMini(k)
        For i = 1 To nbPoints(k) - 1
            PMini(k).x = Min(PMini(k).x, monTableau1(k, i).x)
            PMini(k).y = Min(PMini(k).y, monTableau1(k, i).y)
            PMaxi(k).x = Max(PMaxi(k).x, monTableau1(k, i).x)
            PMaxi(k).y = Max(PMaxi(k).y, monTableau1(k, i).y)
        Next i
    Next k
'    Debug.Print PMaxi(EMPLANTURE).y, PMini(EMPLANTURE).y, PMaxi(SAUMON).y, PMini(SAUMON).y, wDessous(SAUMON)
    Select Case Index
        Case 1      'Haut
            wDessous(SAUMON) = wDessous(SAUMON) + PMaxi(EMPLANTURE).y - PMaxi(SAUMON).y
        Case 2      'sur BA
            'Recherche du Y du BA de l'emplanture
            For i = 0 To nbPoints(EMPLANTURE) - 1
                If monTableau1(EMPLANTURE, i).x = PMini(EMPLANTURE).x Then
                    YE = monTableau1(EMPLANTURE, i).y
                    Exit For
                End If
            Next i
            'Recherche du Y du BA du saumon
            For i = 0 To nbPoints(SAUMON) - 1
                If monTableau1(SAUMON, i).x = PMini(SAUMON).x Then
                    YS = monTableau1(SAUMON, i).y
                    Exit For
                End If
            Next i
            wDessous(SAUMON) = wDessous(SAUMON) + (YE - YS)
        Case 3      'Centre
            wDessous(SAUMON) = wDessous(SAUMON) + (PMaxi(EMPLANTURE).y + PMini(EMPLANTURE).y) / 2 - _
                            (PMaxi(SAUMON).y + PMini(SAUMON).y) / 2
        Case 4      'sur BF
            'Recherche du Y du BF de l'emplanture
            For i = 0 To nbPoints(EMPLANTURE) - 1
                If monTableau1(EMPLANTURE, i).x = PMaxi(EMPLANTURE).x Then
                    YE = monTableau1(EMPLANTURE, i).y
                    Exit For
                End If
            Next i
            'Recherche du Y du BF du saumon
            For i = 0 To nbPoints(SAUMON) - 1
                If monTableau1(SAUMON, i).x = PMaxi(SAUMON).x Then
                    YS = monTableau1(SAUMON, i).y
                    Exit For
                End If
            Next i
            wDessous(SAUMON) = wDessous(SAUMON) + (YE - YS)
        Case 5      'Bas
            wDessous(SAUMON) = wDessous(SAUMON) + PMini(EMPLANTURE).y - PMini(SAUMON).y
    End Select
    Call Form_Resize
End Sub

Private Sub CB_Save_Profil_Deform_Click(Index As Integer)
    'Sauvegarde des coordonn�es (DAT, DXF) apr�s d�formation
    Call Exporter_Fichier(Index, False)
End Sub

Private Sub CB_Save_Profil_Orig_Click(Index As Integer)
    'Sauvegarde des coordonn�es (DAT, DXF) d'origine
    Call Exporter_Fichier(Index, True)
End Sub

Private Sub CB_Synchro_Click()
    frmSynchroOptions.Show vbModal
    SynchroButton.Enabled = True
End Sub

Private Sub CB_Zoom1_Click(Index As Integer)
    Dim x1 As Single, y1 As Single, x2 As Single, y2 As Single
    
    x1 = gListePointSynchro(Index).minX
    x2 = gListePointSynchro(Index).maxX
    y1 = gListePointSynchro(Index).minY
    y2 = gListePointSynchro(Index).maxY
    x1 = x1 - DeltaEpsilon * (x2 - x1)
    y1 = y1 - DeltaEpsilon * (y2 - y1)
    x2 = x2 + DeltaEpsilon * (x2 - x1)
    y2 = y2 + DeltaEpsilon * (y2 - y1)
    XcentreI(Index) = (x2 + x1) / 2
    YcentreI(Index) = (y2 + y1) / 2
    CoeffInit(Index) = MemoCoeffInit(Index)
    Call Form_Resize
End Sub

Private Sub CB_ZoomMoins_Click(Index As Integer)
    CoeffInit(Index) = CoeffInit(Index) * 0.9
    Call Form_Resize
End Sub

Private Sub CB_ZoomPlus_Click(Index As Integer)
    CoeffInit(Index) = CoeffInit(Index) / 0.9
    Call Form_Resize
End Sub

Private Sub CB_ZoomSelection_Click(Index As Integer)
'    If Not CB_ZoomSelection(Index).value Then
'        CB_ZoomSelection(Index).value = True
'    Else
'        CB_ZoomSelection(Index).value = False
'    End If
End Sub

Private Sub Check2Cotes_Click(Index As Integer)
    DeuxCotes(Index) = Not DeuxCotes(Index)
    Call Form_Resize
End Sub

Private Sub CmdRectangulaire_Click()
    Dim xMin As Single, xMax As Single
    Dim k As Integer
    'Pour g�n�rer un bloc rectangulaire
    'Nouvelle proc�dure : on se cale sur le plus grand c�t�
    'sens horloge : 0           1
    '               3           2
    If tabBloc(EMPLANTURE, 0).x < tabBloc(SAUMON, 0).x Then
        wMargeBA(SAUMON) = wMargeBA(SAUMON) + tabBloc(SAUMON, 0).x - tabBloc(EMPLANTURE, 0).x
        tabBloc(SAUMON, 0).x = tabBloc(EMPLANTURE, 0).x
    Else
        wMargeBA(EMPLANTURE) = wMargeBA(EMPLANTURE) + tabBloc(EMPLANTURE, 0).x - tabBloc(SAUMON, 0).x
        tabBloc(EMPLANTURE, 0).x = tabBloc(SAUMON, 0).x
    End If
    If tabBloc(EMPLANTURE, 1).x < tabBloc(SAUMON, 1).x Then
        wMargeBF(EMPLANTURE) = wMargeBF(EMPLANTURE) + tabBloc(SAUMON, 1).x - tabBloc(EMPLANTURE, 1).x
        tabBloc(EMPLANTURE, 1).x = tabBloc(SAUMON, 1).x
    Else
        wMargeBF(SAUMON) = wMargeBF(SAUMON) + tabBloc(EMPLANTURE, 1).x - tabBloc(SAUMON, 1).x
        tabBloc(SAUMON, 1).x = tabBloc(EMPLANTURE, 0).x
    End If
    
    For k = EMPLANTURE To SAUMON
        tabBloc(k, 2) = tabBloc(k, 1)
        tabBloc(k, 3) = tabBloc(k, 0)
        tabBloc(k, 4) = tabBloc(k, 0)
    Next k
    
    Call Form_Resize
End Sub

Private Sub Combo_Zoom_Click(Index As Integer)
    If Combo_Zoom(Index).ListIndex = 0 Then
        CoeffInit(Index) = CoeffInit(Index) * 1.1
    End If
    If Combo_Zoom(Index).ListIndex = 2 Then
        CoeffInit(Index) = CoeffInit(Index) / 1.1
    End If
    If Combo_Zoom(Index).ListIndex = 1 Then Exit Sub
    Combo_Zoom(Index).ListIndex = 1
    Call Dessine_Onglet0
End Sub

Private Sub Form_Load()
'    Call SubClass(hwnd)
    
    ptLimitMinSize = GetPOINTAPI(780, 700)
    bLimitMinSize = True
    bLimitMaxSize = False
    bUseMaximizedPos = False
    bUseMaximizedSize = False
    
    On Error Resume Next
    Me.Move ReadINI_Num(Me.Name & "_Left", 0), _
            ReadINI_Num(Me.Name & "_Top", 0), _
            ReadINI_Num(Me.Name & "_Width", 13000), _
            ReadINI_Num(Me.Name & "_Height", 10600)
    On Error GoTo 0
    If ReadINI_Num(Me.Name & "_Maximized", 0) = -1 Then
        Me.WindowState = vbMaximized
    End If
    
    ChargeTrad Me
    bProfilCharge = False
    bLancementComplexes = True
    
    ReDim Preserve Pointsplt(EMPLANTURE To SAUMON, 0)
    'Positionnement sur le premier onglet
    TabStrip1.Tabs(1).Selected = True
    bModif = True
    
    For k = EMPLANTURE To SAUMON
        CB_Identique(k).Enabled = False
        Largeur1(k) = PB_Profil1(k).Width
        Hauteur1(k) = PB_Profil1(k).Height
        Gauche1(k) = PB_Profil1(k).Left
        Haut1(k) = PB_Profil1(k).Top
        TB_Corde(k) = 100#
        Frame_1(k).ForeColor = Couleur(k)
        TB_Corde(k).ForeColor = Couleur(k)
        Frame_24(k).ForeColor = Couleur(k)
        Frame_25(k).ForeColor = Couleur(k)
        Frame_2(k).ForeColor = Couleur(k)
        Frame_21(k).ForeColor = Couleur(k)
        Frame_22(k).ForeColor = Couleur(k)
        Label_21(k).ForeColor = Couleur(k)
        Label_22(k).ForeColor = Couleur(k)
        Label_23(k).ForeColor = Couleur(k)
        Label_24(k).ForeColor = Couleur(k)
        Label_25(k).ForeColor = Couleur(k)
        Lbl_PtsSynchro(k).ForeColor = Couleur(k)
        Lbl_PtsSynchro(k).Caption = ReadINI_Trad(gLangue, "erreurs", 71) & " 0"

        LV_Synchro(k).ForeColor = Couleur(k)
        PB_Synchro(k).FillColor = Frame_1(k).BackColor
        'Liste de zoom pour Onglet0
        Combo_Zoom(k).AddItem "+"
        Combo_Zoom(k).AddItem " "
        Combo_Zoom(k).AddItem "-"
        Combo_Zoom(k).ListIndex = 1
        Combo_Zoom(k).Left = -10000
    Next k
    Largeur2 = PB_ProfilCote.Width
    Hauteur2 = PB_ProfilCote.Height
    Gauche2 = PB_ProfilCote.Left
    Haut2 = PB_ProfilCote.Top
    
    ' Changer l'"AutoSize" de la StatusBar.
    For k = 1 To SB1.Panels.count
        With SB1.Panels(k)
            .AutoSize = sbrContents
            .Alignment = sbrCenter
            .Width = 500
            .Text = ""
        End With
    Next k
    
    Call ValeurDefaut
    
    'Onglet Bloc
    For k = EMPLANTURE To SAUMON
        DeuxCotes(k) = False  'initialisation des boutons
        Check2Cotes(k).value = False
        Frame_4(k).ForeColor = Couleur(k)
    Next k
End Sub

Private Sub Dessine_Onglet0()
    Dim P1 As Point
    Dim P2 As Point
    Dim x1 As Single, y1 As Single, x2 As Single, y2 As Single
    Dim Rapport(EMPLANTURE To SAUMON) As Double
    Dim DeltaEpsilon As Double
    'Pour les fl�ches
    Dim PDepart As Point
    Dim pFleche As Point
    
    Dim Re As Double, Rd As Double
    Dim x As Double
    Dim y As Double
    Dim PC As Double
    Dim NumSynchro As Long

    If Not bProfilCharge Then Exit Sub
    
    DeltaEpsilon = 0.05
    
    For k = EMPLANTURE To SAUMON
        PB_Synchro(k).Cls
        PB_Synchro(k).AutoRedraw = True
    
        x1 = gListePointSynchro(k).minX
        x2 = gListePointSynchro(k).maxX
        y1 = gListePointSynchro(k).minY
        y2 = gListePointSynchro(k).maxY
        x1 = x1 - DeltaEpsilon * (x2 - x1)
        y1 = y1 - DeltaEpsilon * (y2 - y1)
        x2 = x2 + DeltaEpsilon * (x2 - x1)
        y2 = y2 + DeltaEpsilon * (y2 - y1)
    Next k
    
    For k = EMPLANTURE To SAUMON        'Dessin des lignes
        gListePointSynchro(k).MoveFirst
        P1 = PLTtoAPI(gListePointSynchro(k).CurrentPoint, PB_Synchro(k), k)
        i = 1
        While gListePointSynchro(k).MoveNext
            P2 = PLTtoAPI(gListePointSynchro(k).CurrentPoint, PB_Synchro(k), k)
            PB_Synchro(k).Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k)
            P1 = P2
        Wend
    Next k
    
    For k = EMPLANTURE To SAUMON    'Dessin des cercles
        gListePointSynchro(k).MoveFirst
        i = 1
        Do
            If bPoints Or bNum Or LV_Synchro(k).ListItems(i).Selected Then
                Call PB_Cercle(PB_Synchro(k), _
                    PLTtoAPI(gListePointSynchro(k).CurrentPoint, PB_Synchro(k), k), _
                    Couleur(k), _
                    Couleur(3 - k), _
                    False, _
                    LV_Synchro(k).ListItems(i).Selected, _
                    gListePointSynchro(k).CurrentbTemp, _
                    bNum, _
                    i)
            End If
            i = i + 1
        Loop Until Not gListePointSynchro(k).MoveNext
    Next k
    
    For k = EMPLANTURE To SAUMON        'Dessin des points de synchro
        gListePointSynchro(k).MoveFirst
        NumSynchro = 0
        Do
            If gListePointSynchro(k).CurrentbSynchro Then
                NumSynchro = NumSynchro + 1
                Call PB_Cercle(PB_Synchro(k), _
                    PLTtoAPI(gListePointSynchro(k).CurrentPoint, PB_Synchro(k), k), _
                    Couleur(k), _
                    Couleur(3 - k), _
                    gListePointSynchro(k).CurrentbSynchro, _
                    False, _
                    False, _
                    bNum, _
                    NumSynchro)
            End If
        Loop Until Not gListePointSynchro(k).MoveNext
    Next k
    
    For k = EMPLANTURE To SAUMON        'Dessin des fl�ches
        gListePointSynchro(k).MoveFirst
        P1 = PLTtoAPI(gListePointSynchro(k).CurrentPoint, PB_Synchro(k), k)
        PDepart = P1
        i = 1
        While gListePointSynchro(k).MoveNext
            i = i + 1
            P2 = PLTtoAPI(gListePointSynchro(k).CurrentPoint, PB_Synchro(k), k)
            If i = 2 Then
                pFleche = P2
            End If
            If PDepart.x = pFleche.x And _
                PDepart.y = pFleche.y Then
                pFleche = P1
            Else
                'Trac� des fl�ches de sens
                Call PB_Fleche(PB_Synchro(k), PDepart, pFleche, CouleurFleche)
            End If
            If i = Round(gListePointSynchro(k).count / 2, 0) Then
                PDepart = P1
                pFleche = P1
            End If
            P1 = P2
        Wend
    Next k
End Sub

Private Sub Dessine_Onglet1()
    Dim P1 As Point
    Dim P2 As Point
    Dim x1 As Single, y1 As Single, x2 As Single, y2 As Single
    Dim DeltaEpsilon As Double
    Dim DeltaX As Single
    Dim DeltaY As Single
    
    If Not bProfilCharge Then Exit Sub
    
    DeltaEpsilon = 0.05
    
    x1 = Min(P_mini(EMPLANTURE).x, P_mini(SAUMON).x)
    x2 = Max(P_maxi(EMPLANTURE).x, P_maxi(SAUMON).x)
    y1 = Min(P_mini(EMPLANTURE).y, P_mini(SAUMON).y)
    y2 = Max(P_maxi(EMPLANTURE).y, P_maxi(SAUMON).y)
    DeltaX = DeltaEpsilon * (x2 - x1)
    DeltaY = DeltaEpsilon * (y2 - y1)
    x1 = x1 - DeltaX
    y1 = y1 - DeltaY
    x2 = x2 + DeltaX
    y2 = y2 + DeltaY
    
    'Pour le centrage des dessins, il faut calculer DeltaX etDeltaY :
    ' D�calage entre le milieu du dessin de l'emplanture et le milieu du dessin du saumon
    DeltaX = (P_maxi(SAUMON).x + P_mini(SAUMON).x) / 2 - _
            (P_maxi(EMPLANTURE).x + P_mini(EMPLANTURE).x) / 2
    DeltaY = (P_maxi(SAUMON).y + P_mini(SAUMON).y) / 2 - _
            (P_maxi(EMPLANTURE).y + P_mini(EMPLANTURE).y) / 2
            
    'Dessin proprement dit
    For k = EMPLANTURE To SAUMON
        'Dimensionnement et placement des dessins
        Call PB_Dimension(PB_Profil1(k), x1, y1, x2, y2, Largeur1(k), Hauteur1(k), Haut1(k), Gauche1(k))
        For i = 1 To gListePointSynchro(k).count - 1
            P1 = monTableau1(k, i - 1)
            P2 = monTableau1(k, i)
            
            If DeltaX < 0 And k = SAUMON Then
                P1.x = P1.x - DeltaX
                P2.x = P2.x - DeltaX
            End If
            If DeltaX > 0 And k = EMPLANTURE Then
                P1.x = P1.x + DeltaX
                P2.x = P2.x + DeltaX
            End If
            If DeltaY < 0 And k = SAUMON Then
                P1.y = P1.y
                P2.y = P2.y
            End If
            If DeltaY > 0 And k = EMPLANTURE Then
                P1.y = P1.y
                P2.y = P2.y
            End If
            PB_Profil1(k).Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k)
        Next i
    Next k
End Sub

Private Sub Dessine_Onglet2()
    Dim P1 As Point
    Dim P2 As Point
    Dim P3 As Point
    Dim P4 As Point
    Dim x1 As Single, y1 As Single, x2 As Single, y2 As Single
    Dim CouleurLignes As Long
    Dim DeltaEpsilon As Double
    Dim DeltaX As Single
    Dim DeltaY As Single
    Dim maxX As Integer
    Dim bNegatif As Boolean
    Dim monDecalage As Double
    
    If Not bProfilCharge Then Exit Sub
    
    DeltaEpsilon = 0.05
    
    'Recalcul de Min et max
    Call MaxiMini_profils

    x1 = Min(P_mini(EMPLANTURE).x, P_mini(SAUMON).x)
    x2 = Max(P_maxi(EMPLANTURE).x, P_maxi(SAUMON).x)
    y1 = Min(P_mini(EMPLANTURE).y, P_mini(SAUMON).y)
    y2 = Max(P_maxi(EMPLANTURE).y, P_maxi(SAUMON).y)
    DeltaX = DeltaEpsilon * (x2 - x1)
    DeltaY = DeltaEpsilon * (y2 - y1)
    x1 = x1 - DeltaX
    y1 = y1 - DeltaY
    x2 = x2 + DeltaX
    y2 = y2 + DeltaY
    
    'Dimensionnement et placement des dessins
    Call PB_Dimension(PB_ProfilCote, x1, y1, x2, y2, Largeur2, Hauteur2, Haut2, Gauche2)
    
    'Ligne d'axe
    PB_ProfilCote.DrawStyle = vbDot
    PB_ProfilCote.Line (x1, (P_mini(EMPLANTURE).y + P_maxi(EMPLANTURE).y) / 2)- _
        (x2, (P_mini(EMPLANTURE).y + P_maxi(EMPLANTURE).y) / 2), RGB(0, 0, 0)
    
    'Dessin proprement dit
    PB_ProfilCote.DrawStyle = vbSolid
    For k = EMPLANTURE To SAUMON
        For i = 1 To nbPoints(k) - 1
            P1 = monTableau1(k, i - 1)
            P2 = monTableau1(k, i)
            PB_ProfilCote.Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(k)
        Next i
    Next k
    
    '''''''''''''''''
    ' Vue du dessus '
    '''''''''''''''''
    
    'Largeur de la fen�tre : MAXX de la table + 2 x DeltaEpsilon
    'Lecture de la valeur "MAXX" : Couleur du trac� du saumon
    DeltaEpsilon = 0.05
    maxX = Val(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_16", 800))
    x1 = 0
    x2 = maxX + 2 * maxX * DeltaEpsilon

    y2 = Min(P_mini(EMPLANTURE).x, P_mini(SAUMON).x)
    y1 = Max(P_maxi(EMPLANTURE).x, P_maxi(SAUMON).x)
    DeltaEpsilon = DeltaEpsilon * (y2 - y1)
    y2 = y2 + DeltaEpsilon
    y1 = y1 - DeltaEpsilon
    
    PB_Vue_Dessus.Picture = LoadPicture   'Effacement de la PictureBox
    PB_Vue_Dessus.FillColor = RGB(255, 255, 255)
    PB_Vue_Dessus.FillStyle = vbFSTransparent
    PB_Vue_Dessus.ForeColor = RGB(255, 0, 255)
    PB_Vue_Dessus.ScaleMode = vbPixels
    PB_Vue_Dessus.ScaleLeft = x1
    PB_Vue_Dessus.ScaleWidth = x2 - x1
    PB_Vue_Dessus.ScaleTop = y2
    PB_Vue_Dessus.ScaleHeight = y1 - y2
    
    P1.x = (maxX - Val(TB_Envergure)) / 2
    P1.y = P_maxi(EMPLANTURE).x
    
    P2.x = P1.x
    P2.y = P_mini(EMPLANTURE).x
    
    P3.x = (maxX - Val(TB_Envergure)) / 2 + Val(TB_Envergure)
    P3.y = P_maxi(SAUMON).x
    
    P4.x = P3.x
    P4.y = P_mini(SAUMON).x
    
    'Trac� des lignes
    'Lecture de la valeur "CouleurLignes" : Couleur du trac� des lignes
    CouleurLignes = Val(ReadINI_Num("CouleurLignes", RGB(255, 0, 255)))
    PB_Vue_Dessus.DrawWidth = 1
    PB_Vue_Dessus.DrawStyle = vbNormal
    For i = 0 To Min(nbPoints(EMPLANTURE), nbPoints(SAUMON)) - 1
        PB_Vue_Dessus.Line (P1.x, monTableau1(EMPLANTURE, i).x)-(P3.x, monTableau1(SAUMON, i).x), CouleurLignes
    Next i
    
    'Ligne de cote de l'emplanture
    PB_Vue_Dessus.DrawWidth = 1
    PB_Vue_Dessus.DrawStyle = vbNormal
    PB_Vue_Dessus.Line (P1.x, y1)-(P1.x, y2), RGB(0, 0, 0)
    
    'Ligne de l'emplanture
    PB_Vue_Dessus.DrawWidth = 2
    PB_Vue_Dessus.Line (P1.x, P1.y)-(P2.x, P2.y), Couleur(EMPLANTURE)
    
    'Ligne de cote du  saumon
    PB_Vue_Dessus.DrawWidth = 1
    PB_Vue_Dessus.DrawStyle = vbNormal
    PB_Vue_Dessus.Line (P3.x, y1)-(P4.x, y2), RGB(0, 0, 0)
    
    'Ligne du saumon
    PB_Vue_Dessus.DrawWidth = 2
    PB_Vue_Dessus.DrawStyle = vbSolid
    PB_Vue_Dessus.Line (P3.x, P3.y)-(P4.x, P4.y), Couleur(SAUMON)
    
    'Bord de fuite
    PB_Vue_Dessus.DrawWidth = 2
    PB_Vue_Dessus.DrawStyle = vbSolid
    PB_Vue_Dessus.Line (P1.x, P1.y)-(P3.x, P3.y), RGB(0, 0, 0)
    
    'Bord d'attaque
    PB_Vue_Dessus.DrawWidth = 2
    PB_Vue_Dessus.Line (P2.x, P2.y)-(P4.x, P4.y), RGB(0, 0, 0)
    
    'Lignes de cote horizontales
    PB_Vue_Dessus.DrawWidth = 1
    PB_Vue_Dessus.DrawStyle = vbDot
    PB_Vue_Dessus.Line (x2, P1.y)-(P1.x, P1.y), RGB(0, 0, 0)
    PB_Vue_Dessus.Line (x2, P2.y)-(P2.x, P2.y), RGB(0, 0, 0)
    If P3.y <> P1.y Then
        PB_Vue_Dessus.Line (x2, P3.y)-(P3.x, P3.y), RGB(0, 0, 0)
    End If
    If P4.y <> P2.y Then
        PB_Vue_Dessus.Line (x2, P4.y)-(P4.x, P4.y), RGB(0, 0, 0)
    End If
End Sub

Private Sub Dessine_Onglet3()
    'Onglet Bloc
    Dim j As Integer
    Dim P1 As Point, P2 As Point, PDeb As Point
    Dim yRef As Single
    Dim yRef1 As Single, yRef2 As Single, yRef3 As Single, yRef4 As Single, yRef5 As Single
    
    If Me.ScaleHeight = 0 Then Exit Sub
    
    Call MaxiMini_profils 'on calcule les coordonn�es de tous les points extr�mes du profil
    
    'Calcul des coordonn�es du bloc
    For k = EMPLANTURE To SAUMON
        'sens horloge : 0           1
        '               3           2
        tabBloc(k, 0).x = P_mini(k).x - wMargeBA(k)
        tabBloc(k, 1).x = P_maxi(k).x + wMargeBF(k)
        tabBloc(k, 2).x = tabBloc(k, 1).x
        tabBloc(k, 3).x = tabBloc(k, 0).x
        
        tabBloc(k, 0).y = wEpaisseur
        tabBloc(k, 1).y = tabBloc(k, 0).y
        tabBloc(k, 2).y = 0
        tabBloc(k, 3).y = tabBloc(k, 2).y
        
        tabBloc(k, 4) = tabBloc(k, 0)
    Next k
    
    Call MaxiMini_tout
    Call Alignement(x1, y1)
    Call MaxiMini_profils 'on calcule les coordonn�es de tous les points extr�mes du profil
    
    lblFBA.Caption = Str(Round(tabBloc(SAUMON, 0).x - tabBloc(EMPLANTURE, 0).x, 1)) + "  mm"
    lblFBF.Caption = Str(Round(tabBloc(EMPLANTURE, 1).x - tabBloc(SAUMON, 1).x, 1)) + "  mm"
    lblEp.Caption = Str(wEpaisseur) + "  mm"
    lblLong.Caption = Str(wEnvergure) + "  mm"
    
    For k = EMPLANTURE To SAUMON
        'Dimensionnement des PictureBoxes
        PB_Bloc(k).Width = Frame_4(k).Width - 135 'Red�finition zone de dessin
        PB_Bloc(k).Height = Frame_4(k).Height - 350
        'Positionnement des contr�les
        TB_BFH(k).Left = PB_Bloc(k).Width - 870
        TB_BFB(k).Left = TB_BFH(k).Left
        TB_MDessus(k).Left = TB_BFH(k).Left - 1065
        TB_MDessous(k).Left = TB_BFH(k).Left - 1065
        Check2Cotes(k).Left = PB_Bloc(k).Width - 1060
        Check2Cotes(k).Top = PB_Bloc(k).Height - 325
    Next k
    
    '-----d�finition de la zone de trac� ----------------
    decalageX = TB_BAH(EMPLANTURE).Left + TB_BAH(EMPLANTURE).Width 'd�calage de l'origine suivant X
    decalageX = decalageX + PB_Bloc(EMPLANTURE).ScaleWidth * 0.075
    decalageY = 50 'd�calage de l'origine suivant Y
    limiteX = TB_MDessus(EMPLANTURE).Left 'limite suivant X
    limiteX = limiteX - PB_Bloc(EMPLANTURE).ScaleWidth * 0.075
    'Renaud : ajuster ci-dessous pour monter ou descendre la ligne de base
    TB_MargeBA(EMPLANTURE).Top = PB_Bloc(EMPLANTURE).Height - 120
    limiteY = TB_MargeBA(EMPLANTURE).Top - 650 - 200 'limite suivant Y, 200 pour les segments
    
    'dimensions de la d�coupe
    largeurReel = x2 - x1
    hauteurReel = y2 - y1

    'dimensions de la zone de trac�
    LargeurZone = limiteX - decalageX
    HauteurZone = limiteY - decalageY
      
    'mise � l'�chelle
    Rlargeur = LargeurZone / largeurReel
    Rhauteur = HauteurZone / hauteurReel
    If Rlargeur > Rhauteur Then
        Rapport = Rhauteur
        CentrageX = (LargeurZone - (x2 - x1) * Rapport) / 2
        CentrageY = 0
    Else
        Rapport = Rlargeur
        CentrageX = 0
        CentrageY = (HauteurZone - (y2 - y1) * Rapport) / 2
    End If
    
    For k = EMPLANTURE To SAUMON
        TB_MargeBA(k).Top = PB_Bloc(k).Height - 120
        TB_MargeBF(k).Top = TB_MargeBA(k).Top
        TB_MargeBA(k).Left = (M_X(P_BA(k).x) + M_X(tabBloc(k, 0).x)) / 2 - TB_MargeBA(k).Width / 2
        If TB_MargeBA(k).Left + 2200 > (PB_Bloc(k).Left + PB_Bloc(k).Width) / 2 Then
            TB_MargeBA(k).Left = (PB_Bloc(k).Left + PB_Bloc(k).Width) / 2 - 2200
        End If
        TB_MargeBF(k).Left = (M_X(P_BF(k).x) + M_X(tabBloc(k, 1).x)) / 2 - TB_MargeBF(k).Width / 2
        If TB_MargeBF(k).Left - 200 < (PB_Bloc(k).Left + PB_Bloc(k).Width) / 2 Then
            TB_MargeBF(k).Left = (PB_Bloc(k).Left + PB_Bloc(k).Width) / 2 + 200
        End If
        
        'Remplissage des TextBoxes
        TB_BAB(k).Text = Str(Round(P_BA(k).y, 2))
        TB_BAH(k).Text = Str(Round(wEpaisseur - P_BA(k).y, 2))
        TB_BFB(k).Text = Str(Round(P_BF(k).y, 2))
        TB_BFH(k).Text = Str(Round(wEpaisseur - P_BF(k).y, 2))
        TB_MDessous(k).Text = Str(Round(wDessous(k), 2))
        TB_MDessus(k).Text = Str(Round(wEpaisseur - P_sup(k).y, 2))
        
        'Largeur emplanture et saumon
        lbl(k).Caption = Str(wMargeBA(k) + P_maxi(k).x - P_mini(k).x + wMargeBF(k)) + "  mm"
        wLargeur(k) = wMargeBA(k) + P_maxi(k).x - P_mini(k).x + wMargeBF(k)
        
        'Positionnement des contr�les
        j = IIf(k = SAUMON, 4, 0)
        
        cmdMBA(1 + j).Left = TB_MargeBA(k).Left + TB_MargeBA(k).Width + 184
        cmdMBA(2 + j).Left = cmdMBA(1 + j).Left + cmdMBA(1 + j).Width
        cmdMBA(3 + j).Left = cmdMBA(1 + j).Left
        cmdMBA(4 + j).Left = cmdMBA(2 + j).Left
        cmdMBA(1 + j).Top = Frame_4(k).Height - 692
        cmdMBA(2 + j).Top = cmdMBA(1 + j).Top + 30
        cmdMBA(3 + j).Top = cmdMBA(1 + j).Top + cmdMBA(1 + j).Height
        cmdMBA(4 + j).Top = cmdMBA(2 + j).Top + cmdMBA(2 + j).Height
        
        cmdMBF(1 + j).Left = TB_MargeBF(k).Left + TB_MargeBF(k).Width + 184
        cmdMBF(1 + j).Top = Frame_4(k).Height - 692
        cmdMBF(2 + j).Left = cmdMBF(1 + j).Left + 296
        cmdMBF(2 + j).Top = cmdMBF(1 + j).Top + 30
        cmdMBF(3 + j).Left = TB_MargeBF(k).Left + TB_MargeBF(k).Width + 184
        cmdMBF(3 + j).Top = cmdMBF(1 + j).Top + 300
        cmdMBF(4 + j).Left = cmdMBF(1 + j).Left + 296
        cmdMBF(4 + j).Top = cmdMBF(1 + j).Top + 300
        
        '------- param�tres de trac�-----------------
        PB_Bloc(k).Cls
        PB_Bloc(k).DrawStyle = vbSolid  ' lignes pleines
        PB_Bloc(k).DrawWidth = 1        ' �paisseur 1 pixel
        PB_Bloc(k).ForeColor = vbBlack   'couleur
  
        If DeuxCotes(k) = True Then     'gestion de la vue du 2�me profil
            P1.x = M_X(monTableau1(3 - k, 0).x)
            P1.y = M_Y(monTableau1(3 - k, 0).y)
            PDeb = P1
            For i = 1 To UBound(monTableau1, 2)
                P2.x = M_X(monTableau1(3 - k, i).x)
                P2.y = M_Y(monTableau1(3 - k, i).y)
                Call PB_Line(PB_Bloc(k), P1, P2, Couleur(3 - k))
                P1 = P2
            Next i
            PB_Bloc(k).Circle (PDeb.x, PDeb.y), 50, Couleur(3 - k)
        End If
        
        P1.x = M_X(monTableau1(k, 0).x)
        P1.y = M_Y(monTableau1(k, 0).y)
        PDeb = P1
        For i = 1 To UBound(monTableau1, 2)         'trac� des profils
            P2.x = M_X(monTableau1(k, i).x)
            P2.y = M_Y(monTableau1(k, i).y)
            Call PB_Line(PB_Bloc(k), P1, P2, Couleur(k))
            P1 = P2
        Next i
        PB_Bloc(k).Circle (PDeb.x, PDeb.y), 50, Couleur(k)
        
        '---------fl�ches de cotation vue de c�t�-----------------------
        ' Dessin des lignes dessus - dessous des cases de valeurs
        ' Emplanture Placement vertical du bord d'attaque
        'trait m�dian et jonction avec le BA en deux segments
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_BAH(k).Left - 100
        P1.y = M_Y(P_BA(k).y)
        If P1.y + 200 + TB_BAB(k).Height > M_Y(tabBloc(k, 2).y) Then
            P1.y = M_Y(tabBloc(k, 2).y) - 200 - TB_BAB(k).Height
        End If
        yRef = P1.y
        P2.x = P1.x + TB_BAH(k).Width + 200
        P2.y = P1.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P1.x = M_X(tabBloc(k, 0).x)
        P1.y = M_Y(P_BA(k).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        P2.y = P1.y
        P2.x = M_X(P_BA(k).x)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)

        ' trait sup�rieur et jonction avec l'angle du bloc
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_BAH(k).Left - 100
        P1.y = yRef - 100 - TB_BAH(k).Height
        P2.x = P1.x + TB_BAH(k).Width + 100
        P2.y = P1.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P1.x = M_X(tabBloc(k, 0).x)
        P1.y = M_Y(tabBloc(k, 0).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        'trait inf�rieur et jonction avec l'angle du bloc
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_BAB(k).Left - 100
        P1.y = yRef + 100 + TB_BAB(k).Height
        P2.x = P1.x + TB_BAB(k).Width + 100
        P2.y = P1.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P1.x = M_X(tabBloc(k, 3).x)
        P1.y = M_Y(tabBloc(k, 3).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        'fl�ches en haut
        P1.x = TB_BAH(k).Left + TB_BAH(k).Width
        P1.y = yRef - 100 - TB_BAH(k).Height
        P2.x = P1.x
        P2.y = yRef
        Call PB_DessinCoteV(PB_Bloc(k), P1, P2)
        'fl�ches en bas
        P1 = P2
        P2.y = yRef + 100 + TB_BAH(k).Height
        Call PB_DessinCoteV(PB_Bloc(k), P1, P2)
        TB_BAH(k).Top = yRef + 200 - TB_BAH(k).Height
        TB_BAB(k).Top = yRef + 300

        ' Bord de fuite
        'ligne la plus basse et jonction angle du bloc en bas
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MDessous(k).Left - 200
        P1.y = M_Y(tabBloc(k, 2).y)
        P2.x = TB_BFB(k).Left + TB_BFB(k).Width
        P2.y = P1.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P1.x = M_X(tabBloc(k, 2).x)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        yRef1 = P1.y
        TB_MDessous(k).Top = yRef1 - TB_MDessous(k).Height + 200

        'ligne marge basse et jonction point bas du profil
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MDessous(k).Left - 200
        P1.y = yRef1 - TB_MDessous(k).Height - 100
        yRef2 = P1.y
        P2.x = TB_MDessous(k).Left + TB_MDessous(k).Width
        P2.y = P1.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P2.x = M_X(tabBloc(k, 1).x)
        P2.y = M_Y(wDessous(k))
        P2.y = M_Y(P_inf(k).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        P1 = P2
        P1.x = M_X(P_inf(k).x)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        
        'ligne m�diane et traits de jonction BF
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MDessus(k).Left - 200
        P1.y = yRef2 - TB_BFB(k).Height - 100
        yRef3 = P1.y
        P2.x = TB_BFH(k).Left + TB_BFH(k).Width
        P2.y = P1.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P2.x = M_X(tabBloc(k, 1).x)
        P2.y = M_Y(P_BF(k).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        P1.x = M_X(P_BF(k).x)
        P1.y = M_Y(P_BF(k).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        TB_BFB(k).Top = yRef2 + 100
        
        'trait de jonction marge sup�rieure
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MDessus(k).Left - 200
        P1.y = yRef3 - TB_BFH(k).Height - 100
        yRef4 = P1.y
        P2.x = TB_MDessus(k).Left + TB_MDessus(k).Width
        P2.y = P1.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P2.x = M_X(tabBloc(k, 1).x)
        P2.y = M_Y(P_sup(k).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        P1.x = M_X(P_sup(k).x)
        P1.y = P2.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        TB_MDessus(k).Top = yRef4 - 200
        
        ' trait sup�rieur et jonction avec l'angle du bloc
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MDessus(k).Left - 200
        P1.y = yRef4 - TB_MDessus(k).Height - 200
        yRef5 = P1.y
        P2.x = TB_BFH(k).Left + TB_BFH(k).Width
        P2.y = P1.y
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P2.x = M_X(tabBloc(k, 1).x)
        P2.y = M_Y(tabBloc(k, 1).y)
        Call PB_Line(PB_Bloc(k), P2, P1, vbBlack)
        TB_BFH(k).Top = yRef5 + 600
        
        'fl�ches
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MDessus(k).Left - 200
        P1.y = yRef1
        P2.x = P1.x
        P2.y = yRef2
        Call PB_DessinCoteV(PB_Bloc(k), P2, P1)
        
        P1.y = yRef4
        P2.y = yRef5
        Call PB_DessinCoteV(PB_Bloc(k), P2, P1)

        P1.x = TB_BFH(k).Left - 200
        P2.x = P1.x
        
        P1.y = yRef1
        P2.y = yRef3
        Call PB_DessinCoteV(PB_Bloc(k), P2, P1)
        
        P1.y = yRef3
        P2.y = yRef5
        Call PB_DessinCoteV(PB_Bloc(k), P2, P1)
        
        'case du bas BA et jonctions
        P1.x = TB_MargeBA(k).Left - 190
        P1.y = TB_MargeBA(k).Top + 50
        P2.x = TB_MargeBA(k).Left - 190
        P2.y = TB_MargeBA(k).Top - 650
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P1.x = M_X(tabBloc(k, 3).x)
        P1.y = M_Y(tabBloc(k, 3).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MargeBA(k).Left + TB_MargeBA(k).Width + 10
        P1.y = TB_MargeBA(k).Top + 50
        P2.x = TB_MargeBA(k).Left + TB_MargeBA(k).Width + 10
        P2.y = TB_MargeBA(k).Top - 650
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P1.x = M_X(P_BA(k).x)
        P1.y = M_Y(tabBloc(k, 3).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        P2.x = M_X(P_BA(k).x)
        P2.y = M_Y(P_BA(k).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        'fl�ches
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MargeBA(k).Left - 190
        P1.y = TB_MargeBA(k).Top - 480
        P2.x = TB_MargeBA(k).Left + TB_MargeBA(k).Width + 10
        P2.y = P1.y
        Call PB_DessinCoteH(PB_Bloc(k), P1, P2)
              
        'case du bas BF et jonctions
        P1.x = TB_MargeBF(k).Left - 190
        P1.y = TB_MargeBF(k).Top + 50
        P2.x = TB_MargeBF(k).Left - 190
        P2.y = TB_MargeBF(k).Top - 650
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P1.x = M_X(P_BF(k).x)
        P1.y = M_Y(tabBloc(k, 2).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        P2 = P1
        P2.y = M_Y(P_BF(k).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MargeBF(k).Left + TB_MargeBF(k).Width + 10
        P1.y = TB_MargeBF(k).Top + 50
        P2.x = TB_MargeBF(k).Left + TB_MargeBF(k).Width + 10
        P2.y = TB_MargeBF(k).Top - 650
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        PB_Bloc(k).DrawStyle = vbDot
        P1.x = M_X(tabBloc(k, 2).x)
        P1.y = M_Y(tabBloc(k, 2).y)
        Call PB_Line(PB_Bloc(k), P1, P2, vbBlack)
        'fl�ches
        PB_Bloc(k).DrawStyle = vbSolid
        P1.x = TB_MargeBF(k).Left - 190
        P1.y = TB_MargeBF(k).Top - 480
        P2.x = TB_MargeBF(k).Left + TB_MargeBF(k).Width + 10
        P2.y = TB_MargeBF(k).Top - 480
        Call PB_DessinCoteH(PB_Bloc(k), P1, P2)
    
        If DeuxCotes(k) = True Then     'gestion de la vue du 2�me bloc
            P1.x = M_X(tabBloc(3 - k, 0).x)
            P1.y = M_Y(tabBloc(3 - k, 0).y)
            For i = 1 To 4
                P2.x = M_X(tabBloc(3 - k, i).x)
                P2.y = M_Y(tabBloc(3 - k, i).y)
                Call PB_Line(PB_Bloc(k), P1, P2, Couleur(3 - k))
                P1 = P2
            Next i
        End If
        'Trac� du bloc
        P1.x = M_X(tabBloc(k, 0).x)
        P1.y = M_Y(tabBloc(k, 0).y)
        
        For i = 1 To 4
            P2.x = M_X(tabBloc(k, i).x)
            P2.y = M_Y(tabBloc(k, i).y)
            Call PB_Line(PB_Bloc(k), P1, P2, Couleur(k))
            P1 = P2
        Next i
    Next k
    'Renaud : Ici pour dessiner la table (ou pour appeler la fonction qui va dessiner la table)
    'Genre : Call DessinTable(PB_Bloc(k), P1) : P1 te donne le point en bas � gauche du bloc
    
    Call Affiche3D
End Sub

Private Sub Charge_Dessin()
    'Constitution du tableau des points du dessin
    If UBound(Pointsplt) = 0 Then Exit Sub
    
    ReDim Preserve monTableau(0 To UBound(Pointsplt))
    For i = 0 To UBound(Pointsplt)
        monTableau(i) = Pointsplt(i)
    Next i
End Sub

Private Function Importer_Fichier(EouS As Integer) As Boolean
    Dim InitialDir As String
    Dim j As Long
    Dim P1 As Point, P2 As Point, P3 As Point
    Dim Rapport As Double
    Dim iOrigine As Long
    Dim PTemp As Point
    Dim TempPoints As Long
    Dim TempTab As PointPLT
    Dim xMin, xMax, Ymin, Ymax As Single
    Dim x_Dim As Single
    Dim maCorde As Single
    Dim bProfilFerme
    Dim maSequence As String * 2
    
    ' Lecture de la valeur "Fichier" : Nom du fichier
    Fichier = ReadINI("Fichier", "")
    ' et r�cup�ration du chemin
    InitialDir = GetPathName(Fichier)
        
    Fichier = RechercheFichier(Me, InitialDir, "Fichiers dessin" & Chr(0) & "*.DAT; *.PLT; *.PRN; *.HPGL; *.NC; *.DXF; *.DRD" & Chr(0))
    If Len(Fichier) = 0 Then
        Importer_Fichier = False
        Exit Function
    End If
    Call WriteINI("Fichier", Fichier)
    
    ReDim monTableauLecture(0 To 0)
    
    wFichier(EouS) = GetFileName(Fichier)
    If EouS = EMPLANTURE And nbPoints(SAUMON) = 0 Then
        wFichier(SAUMON) = GetFileName(Fichier)
        EouS = TOUT
    End If
    If EouS = SAUMON And nbPoints(EMPLANTURE) = 0 Then
        wFichier(EMPLANTURE) = GetFileName(Fichier)
        EouS = TOUT
    End If

    wComm = Space$(260)
    TempPoints = LireFichier(monTableauLecture(), Fichier & vbNullChar, xMin, xMax, Ymin, Ymax, wComm)
    
    If TempPoints < 2 Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", -TempPoints), vbCritical
        Importer_Fichier = False
        Exit Function
    End If
    
    'Contr�le : pas d'aller-retour !
    Dim Aod1 As Double, Aod2 As Double
    P1.x = monTableauLecture(0).x
    P1.y = monTableauLecture(0).y
    P2.x = monTableauLecture(1).x
    P2.y = monTableauLecture(1).y
    Call Calcul_Ang_Segment(P1, P2, Aod1)
    For i = 2 To TempPoints - 1
        P3.x = monTableauLecture(i).x
        P3.y = monTableauLecture(i).y
        Call Calcul_Ang_Segment(P2, P3, Aod2)
        
Debug.Print "i : " & i & "    Aod1 : " & Aod1 & "    Aod2 = " & Aod2
        If Abs(Abs(Aod1 - Aod2) - PI) < 0.000001 Then
            MsgBox ReadINI_Trad(gLangue, "erreurs", 200) & " " & i & vbCrLf & ReadINI_Trad(gLangue, "erreurs", 201), vbCritical
        End If
        Aod1 = Aod2
        P1 = P2
        P2 = P3
    Next i
    
    i = LBound(monTableauLecture)
    maSequence = monTableauLecture(i).numSequence
    Do
        i = i + 1
        If monTableauLecture(i).numSequence <> maSequence Then
            If MsgBox(ReadINI_Trad(gLangue, "erreurs", 110), vbCritical + vbYesNo) <> vbYes Then
                Exit Function
            Else
                'Call SoudeSequences
                Exit Do
            End If
        End If
    Loop While i < UBound(monTableauLecture)
    
    'Longueur de la table
    x_Dim = ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_16", 600)
    
    If MsgBox(ReadINI_Trad(gLangue, "erreurs", 116), vbQuestion + vbYesNo) = vbYes Then
        If UCase$(GetExtension(Fichier)) <> "DAT" Then
            'Mise � l'�chelle pour passer le nettoyage
            Rapport = x_Dim / (xMax - xMin)
        
            For i = 0 To UBound(monTableauLecture)
                monTableauLecture(i).x = monTableauLecture(i).x - xMin
                monTableauLecture(i).y = monTableauLecture(i).y - Ymin
                monTableauLecture(i).x = monTableauLecture(i).x * Rapport
                monTableauLecture(i).y = monTableauLecture(i).y * Rapport
            Next i
        
            Call Nettoyage(monTableauLecture(), Resolution(gstrTable))
        End If
    End If
    
    'Redimensionnement � la bonne corde
    If (UCase$(GetExtension(Fichier))) = "PLT" Or _
        (UCase$(GetExtension(Fichier))) = "DXF" Then
        maCorde = (xMax - xMin)
    Else
        maCorde = CDbl(TB_Corde(IIf(EouS = 2, SAUMON, EMPLANTURE)))
    End If
    
    If maCorde > x_Dim Then
        If MsgBox(ReadINI_Trad(gLangue, "erreurs", 120), vbQuestion + vbYesNo) = vbYes Then
            maCorde = Min(maCorde, x_Dim)
        End If
    End If
    
    If EouS = EMPLANTURE Or EouS = TOUT Then
        Call RecopieVersListe(monTableauLecture, gListePointSynchro(EMPLANTURE), maCorde)
        nbPoints(EMPLANTURE) = gListePointSynchro(EMPLANTURE).count
        wCorde(EMPLANTURE) = maCorde
        TB_Corde(EMPLANTURE) = maCorde
        wH(EMPLANTURE) = gListePointSynchro(EMPLANTURE).maxY - gListePointSynchro(EMPLANTURE).minY
        TB_H(EMPLANTURE) = Round(wH(EMPLANTURE), 2)
        TB_DimMod(EMPLANTURE) = TB_H(EMPLANTURE)
        If UCase$(GetExtension(Fichier)) <> "DAT" Then
            Call gListePointSynchro(EMPLANTURE).SupprPtsInutiles
        End If
        CB_Save_Profil_Orig(EMPLANTURE).Enabled = True
        CB_Save_Profil_Deform(EMPLANTURE).Enabled = True
        CB_FC_COMP(1).Enabled = True
        CB_FC_COMP(2).Enabled = True
        CB_Nettoyage.Enabled = True
    End If
    If EouS = SAUMON Or EouS = TOUT Then
        Call RecopieVersListe(monTableauLecture, gListePointSynchro(SAUMON), maCorde)
        nbPoints(SAUMON) = gListePointSynchro(SAUMON).count
        wCorde(SAUMON) = maCorde
        TB_Corde(SAUMON) = maCorde
        wH(SAUMON) = gListePointSynchro(SAUMON).maxY - gListePointSynchro(SAUMON).minY
        TB_H(SAUMON) = Round(wH(SAUMON), 2)
        TB_DimMod(SAUMON) = TB_H(SAUMON)
        If UCase$(GetExtension(Fichier)) <> "DAT" Then
            Call gListePointSynchro(SAUMON).SupprPtsInutiles
        End If
        CB_Save_Profil_Orig(SAUMON).Enabled = True
        CB_Save_Profil_Deform(SAUMON).Enabled = True
        CB_FC_COMP(1).Enabled = True
        CB_FC_COMP(2).Enabled = True
        CB_Nettoyage.Enabled = True
    End If
    TB_Fleche_BA = 0
    
    bProfilCharge = False
    
    Call RemplLV
    
    bProfilCharge = True
    For k = EMPLANTURE To SAUMON
        CB_Identique(k).Enabled = True
    Next k
    Importer_Fichier = True
End Function

Private Sub Exporter_Fichier(EouS As Integer, Orig_Deform As Boolean)   'Orig_Deform = true : Profil d'origine
    Dim InitialDir As String
    Dim i As Long
    Dim TempPoints As Long
    Dim Tableau() As PointPLT
    Dim monTexte As String
    
    ' Lecture de la valeur "Fichier" : Nom du fichier
    Fichier = ReadINI("Fichier", "")
    ' et r�cup�ration du chemin
    InitialDir = GetPathName(Fichier)
    
    monTexte = ReadINI_Trad(gLangue, "erreurs", 73) & Chr(0) & "*.DAT; *.DXF" & Chr(0)
    Fichier = RechercheFichier(Me, Fichier, monTexte, True)
    If Len(Fichier) = 0 Then
        Exit Sub
    End If
    If UCase$(GetExtension(Fichier)) <> "DAT" And UCase$(GetExtension(Fichier)) <> "DXF" Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 73), vbCritical + vbOKOnly
        Exit Sub
    End If
    Call WriteINI("Fichier", Fichier)
    
    If Orig_Deform Then
        ReDim Tableau(0 To gListePointSynchro(EouS).count - 1)
        gListePointSynchro(EouS).MoveFirst
        i = 0
        Do
            Tableau(i).x = gListePointSynchro(EouS).CurrentPoint.x
            Tableau(i).y = gListePointSynchro(EouS).CurrentPoint.y
            i = i + 1
        Loop Until Not gListePointSynchro(EouS).MoveNext
    Else
        ReDim Tableau(0 To nbPoints(EouS) - 1)
        For i = LBound(monTableau1, 2) To nbPoints(EouS) - 1
            Tableau(i).x = monTableau1(EouS, i).x
            Tableau(i).y = monTableau1(EouS, i).y
        Next i
    End If
    
    wComm = Left$(GetFileName(Fichier) & " - " & ReadINI_Trad(gLangue, "erreurs", 75) & " " & wCorde(EouS) & " mm" & Space$(260), 260)
    TempPoints = EcrireFichier(Tableau, Fichier & vbNullChar, wComm)
End Sub

Public Sub Form_Resize()
    Dim X1Zoom As Double
    Dim X2Zoom As Double
    Dim Y1Zoom As Double
    Dim Y2Zoom As Double
    Dim Re As Double, Rd As Double
    Dim PC As Double
    Dim bTemp As Boolean
    Dim TempYS As Single, TempYE As Single
    Dim monRapport As Double
    Dim x1 As Single, x2 As Single, y1 As Single, y2 As Single

    On Error Resume Next
    
    If Me.WindowState <> vbMinimized And Me.WindowState <> vbMaximized Then
        Call WriteINI(Me.Name & "_Left", Me.Left)
        Call WriteINI(Me.Name & "_Top", Me.Top)
        Call WriteINI(Me.Name & "_Width", Me.Width)
        Call WriteINI(Me.Name & "_Height", Me.Height)
    End If
    
    With TabStrip1
        .Left = 0
        .Top = 0
        .Width = Me.ScaleWidth
        .Height = Me.ScaleHeight - CB_OK.Height - SB1.Height
        .TabWidthStyle = tabFixed
        .TabFixedWidth = (.Width - 100) / .Tabs.count
        .TabFixedHeight = 500
    End With
    
    With Frame_12
        .Top = TabStrip1.TabFixedHeight + 50
        .Left = 50
        .Width = TabStrip1.Width - 2 * .Left
    End With
    
    With Frame_23
        .Top = TabStrip1.TabFixedHeight + 50
        .Left = 50
        .Width = TabStrip1.Width - 2 * .Left
        Label_Aide_2.Left = .Left
        Label_Aide_2.Width = .Width
    End With
    
    Frame_31.Top = TabStrip1.TabFixedHeight + 50
    Frame_31.Left = 50
    Frame_31.Width = TabStrip1.Width - 2 * Frame_31.Left
    Label_Aide_3.Left = Frame_31.Left
    Label_Aide_3.Width = Frame_31.Width
    
    Frame_45.Top = TabStrip1.TabFixedHeight + 50
    Frame_45.Left = 50
    Frame_45.Width = TabStrip1.Width - 2 * Frame_45.Left
    Label_Aide_4.Left = Frame_45.Left
    Label_Aide_4.Width = Frame_45.Width
    Frame_41.Top = Frame_45.Top + Frame_45.Height
    Frame_42.Top = Frame_41.Top
    Frame_42.Left = Frame_41.Left + Frame_41.Width
    Frame_44.Left = Frame_41.Left
    Frame_44.Top = Frame_42.Top + Frame_42.Height
    Frame_46.Top = Frame_41.Top + Frame_41.Height
    Frame_47.Top = Frame_44.Top + Frame_44.Height
    
    Frame_5.Top = TabStrip1.TabFixedHeight + 50
    Frame_5.Left = 50
    Frame_5.Height = TabStrip1.Height - TabStrip1.TabFixedHeight - 100
    Frame_5.Width = TabStrip1.Width - 100
    TB_Comm.Width = Frame_5.Width - 200
    TB_Comm.Height = Frame_5.Height - 300
    
    For k = EMPLANTURE To SAUMON
        'Recup des valeurs stock�es
        bTemp = bProfilCharge
        bProfilCharge = False
        TB_Corde(k) = wCorde(k)
        TB_DimMult(k) = wdimMult(k)
        TB_MargeBA(k) = Round(wMargeBA(k), 2)
        TB_MargeBF(k) = Round(wMargeBF(k), 2)
        TB_Angle_Atteint(k) = wAngle_Atteint(k)
        bProfilCharge = bTemp

        'Onglet Synchro
        PB_Synchro(k).Picture = LoadPicture   'Effacement de la PictureBox
    
        Frame_1(k).Left = 120
        Frame_1(k).Width = TabStrip1.Width - Frame_1(k).Left * 2
        Frame_1(k).Height = (TabStrip1.Height - TabStrip1.TabFixedHeight - Frame_12.Height - 100) / 2
        LV_Synchro(k).Height = Frame_1(k).Height - LV_Synchro(k).Top - 100

        Largeur(k) = Frame_1(k).Width - LV_Synchro(k).Width - LV_Synchro(k).Left - 100
        If Largeur(k) < 0 Then Exit Sub
        Hauteur(k) = Frame_1(k).Height - 250
        If Hauteur(k) < 0 Then Exit Sub
        PB_Synchro(k).Left = LV_Synchro(k).Width + LV_Synchro(k).Left
        PB_Synchro(k).Width = Largeur(k)
        PB_Synchro(k).Height = Hauteur(k)
        
        Frame_2(k).Left = 0
        Frame_2(k).Height = (TabStrip1.Height - TabStrip1.TabFixedHeight - Frame_23.Height - 100) / 2
        Frame_2(k).Width = TabStrip1.Width - Frame_2(k).Left * 2
        Frame_3(k).Left = 0
        Frame_3(k).Height = (TabStrip1.Height - TabStrip1.TabFixedHeight - Frame_31.Height - 100) / 2
        Frame_3(k).Width = TabStrip1.Width - 250
        Frame_4(k).Left = Frame_42.Left + Frame_42.Width + 50
        Frame_4(k).Height = (TabStrip1.Height - TabStrip1.TabFixedHeight - Frame_45.Height - 100) / 2
        Frame_4(k).Width = TabStrip1.Width - Frame_44.Width - 250
        
        'Dimensions du dessin
        x1 = gListePointSynchro(k).minX
        x2 = gListePointSynchro(k).maxX
        y1 = gListePointSynchro(k).minY
        y2 = gListePointSynchro(k).maxY
        x1 = x1 - DeltaEpsilon * (x2 - x1)
        y1 = y1 - DeltaEpsilon * (y2 - y1)
        x2 = x2 + DeltaEpsilon * (x2 - x1)
        y2 = y2 + DeltaEpsilon * (y2 - y1)
        X0(k) = PB_Synchro(k).Width / 2  'coordonn�es du centre de la fen�tre
        Y0(k) = PB_Synchro(k).Height / 2

        monRapport = CoeffInit(k) / MemoCoeffInit(k)
        If ((PB_Synchro(k).Width - 2 * Marge) / (x2 - x1)) < ((PB_Synchro(k).Height - 2 * Marge) / (y2 - y1)) Then
            CoeffInit(k) = (PB_Synchro(k).Width - 2 * Marge) / (x2 - x1)
        Else
            CoeffInit(k) = (PB_Synchro(k).Height - 2 * Marge) / (y2 - y1)
        End If
        MemoCoeffInit(k) = CoeffInit(k)
        CoeffInit(k) = CoeffInit(k) * monRapport

    Next k
    
    Frame_1(EMPLANTURE).Top = Frame_12.Top + Frame_12.Height
    Frame_1(SAUMON).Top = Frame_1(EMPLANTURE).Top + Frame_1(EMPLANTURE).Height

    Frame_2(EMPLANTURE).Top = Frame_23.Top + Frame_23.Height
    Frame_2(SAUMON).Top = Frame_2(EMPLANTURE).Top + Frame_2(EMPLANTURE).Height

    Frame_3(EMPLANTURE).Top = Frame_31.Top + Frame_31.Height
    Frame_3(SAUMON).Top = Frame_3(EMPLANTURE).Top + Frame_3(EMPLANTURE).Height

    Frame_4(EMPLANTURE).Top = Frame_45.Top + Frame_45.Height
    Frame_4(SAUMON).Top = Frame_4(EMPLANTURE).Top + Frame_4(EMPLANTURE).Height
    Label3_1.Left = Frame_3(SAUMON).Width - Label3_1.Width - 50
    Label3_2.Left = Frame_3(SAUMON).Width - Label3_2.Width - 50
    Label3_3.Left = Frame_3(SAUMON).Width - Label3_3.Width - 50
    Label3_4.Left = Frame_3(SAUMON).Width - Label3_4.Width - 50
    TB_Fleche_BA.Left = Frame_3(SAUMON).Width - TB_Fleche_BA.Width - 50
    TB_Fleche_BF.Left = Frame_3(SAUMON).Width - TB_Fleche_BF.Width - 50
    TB_Decalage.Left = Frame_3(SAUMON).Width - TB_Decalage.Width - 50
    TB_Diedre.Left = Frame_3(SAUMON).Width - TB_Diedre.Width - 50

    For k = CB_Fleche.LBound To CB_Fleche.UBound
        CB_Fleche(k).Left = Frame_3(SAUMON).Width - Label3_1.Width - CB_Fleche(k).Width - 100
    Next k
    For k = CB_Pos.LBound To CB_Pos.UBound
        CB_Pos(k).Left = Frame_3(EMPLANTURE).Width - Label3_3.Width - CB_Pos(k).Width - 100
    Next k
    For k = EMPLANTURE To SAUMON
        Largeur1(k) = Frame_2(k).Width - Frame_22(k).Width - 550
        Hauteur1(k) = Frame_2(k).Height - 250
    Next k
    Largeur2 = CB_Pos(1).Left - 500
    Hauteur2 = Frame_3(2).Height - 350
    PB_Vue_Dessus.Width = CB_Fleche(1).Left - 500
    PB_Vue_Dessus.Height = Frame_3(1).Height - TB_Decalage.Height - 300
    CB_OK.Left = Me.ScaleWidth - CB_OK.Width
    CB_Defaut.Left = CB_OK.Left - CB_Defaut.Width
    CB_OK.Top = Me.ScaleHeight - CB_OK.Height - SB1.Height
    CB_Defaut.Top = CB_OK.Top

    If bProfilCharge Then
        Call Dessine_Onglet0

        Call RecopieVersTableau1
        Call Miroir
        Call Rotation
        Call MaxiMini_profils
        
        Call Dessine_Onglet1
        
        'D�calages en X et en Y
        Call Zero
        Call Decalage(wDessous(EMPLANTURE), wDessous(SAUMON), CDbl(TB_Fleche_BA) + P_mini(EMPLANTURE).x - P_mini(SAUMON).x)
        Call Dessine_Onglet2
        
        TB_Fleche_BA = wFleche_BA
        TB_Fleche_BF = wFleche_BF
        TB_Epaisseur = Format(Round(wEpaisseur, 1), "#.0")
        TB_Envergure = wEnvergure
        TB_Decalage = Round(wDessous(SAUMON) - wDessous(EMPLANTURE), 1)
        TempYS = (P_maxi(SAUMON).y + P_mini(SAUMON).y) / 2
        TempYE = (P_maxi(EMPLANTURE).y + P_mini(EMPLANTURE).y) / 2
        TB_Diedre = Round(ArcSin((TempYS - TempYE) / TB_Envergure), 1) 'todo
        Call Dessine_Onglet3
        
        For k = EMPLANTURE To SAUMON
            'Nom des fichiers
            SB1.Panels((k - 1) * 5 + 1).Text = wFichier(k)
            'Nombre de points
            SB1.Panels((k - 1) * 5 + 2).Text = gListePointSynchro(k).count & " pts"
            'X
            SB1.Panels((k - 1) * 5 + 3).Text = "X:" & TB_Corde(k)
            'Y
            SB1.Panels((k - 1) * 5 + 4).Text = "Y:" & TB_DimMod(k)
        Next k
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim bSynchro As Boolean
    
    Call WriteINI(Me.Name & "_Maximized", IIf(Me.WindowState = vbMaximized, -1, 0))
    
    If Not bProfilCharge Then
        Me.Hide
        Exit Sub
    End If
    
    bSynchro = False
    If nbPoints(EMPLANTURE) <> nbPoints(SAUMON) Then
        MsgBox ReadINI_Trad(gLangue, "erreurs", 60), vbCritical
        bSynchro = True
    Else
        'If wFichier(EMPLANTURE) <> wFichier(SAUMON) Then
        '    MsgBox ReadINI_Trad(glangue, "erreurs", 61), vbCritical
        '    bSynchro = True
        'End If
    End If
    
    wEnvergure = CDbl(Replace(TB_Envergure, ".", Format$(0#, ".")))
    wComm = TB_Comm
    For k = EMPLANTURE To SAUMON
        wDessous(k) = CDbl(Replace(TB_MDessous(k), ".", Format$(0#, ".")))
    Next k
    
    Call Form_Resize            'Pour forcer le recalcul de Tableau1
    Me.Hide
    
    bSaveFC = True
    
    frmMain.mnuFileCut.Enabled = Not bSynchro
    frmMain.CB_Decoupe.Enabled = Not bSynchro
End Sub


Private Sub HScroll_Rot_Change(Index As Integer)
    wAngle_Atteint(Index) = HScroll_Rot(Index).value
    Call Form_Resize
End Sub

Private Sub HScroll_Rot_Scroll(Index As Integer)
    wAngle_Atteint(Index) = HScroll_Rot(Index).value
    Call Form_Resize
End Sub

Private Sub Lbl_PtsSynchro_Change(Index As Integer)
    If gListePointSynchro(EMPLANTURE) Is Nothing Then
        SynchroButton.Enabled = False
        Exit Sub
    End If
    SynchroButton.Enabled = (Lbl_PtsSynchro(EMPLANTURE) = Lbl_PtsSynchro(SAUMON) And _
            gListePointSynchro(EMPLANTURE).NbPtsSynchro >= 2)
End Sub

Private Sub LV_Synchro_ItemCheck(Index As Integer, ByVal Item As MSComctlLib.ListItem)
    gListePointSynchro(Index).Synchro Item.Index, Item.Checked
    Lbl_PtsSynchro(Index).Caption = ReadINI_Trad(gLangue, "erreurs", 71) & _
        gListePointSynchro(Index).NbPtsSynchro
    Call Form_Resize
End Sub

Private Sub LV_Synchro_ItemClick(Index As Integer, ByVal Item As MSComctlLib.ListItem)
    Dim kBis As Integer
    'Synchro avec l'autre ListView
    kBis = 3 - Index
    On Error Resume Next    'Si les listes n'ont pas le m�me nombre de points
    LV_Synchro(kBis).ListItems(Item.Index).EnsureVisible
    Set LV_Synchro(kBis).SelectedItem = LV_Synchro(kBis).ListItems(Item.Index)
    
    'On clique sur un point, on affiche une croix sur le graphique
    Call Form_Resize
End Sub

Private Sub LV_Synchro_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton Then
        monIndex = Index
        PopupMenu mnu_ctx
    End If
End Sub

Private Sub mnuCtx_Origine_Click()
    gListePointSynchro(monIndex).ChangeOrigine LV_Synchro(monIndex).SelectedItem.Index
    Call RemplLV
    Call Form_Resize
End Sub

Private Sub mnuCtx_PtCle_Click()
    'Mise � jour de gListePointSynchro
    'Trouver quel point est s�lectionn� dans la ListView
    i = LV_Synchro(monIndex).SelectedItem.Index
    LV_Synchro(monIndex).ListItems(i).Checked = Not LV_Synchro(monIndex).ListItems(i).Checked
    gListePointSynchro(monIndex).Synchro i, LV_Synchro(monIndex).SelectedItem.Checked
    
    Lbl_PtsSynchro(monIndex).Caption = ReadINI_Trad(gLangue, "erreurs", 71) & _
        gListePointSynchro(monIndex).NbPtsSynchro

    Call Form_Resize
End Sub

Private Sub mnuCtx_Suppression_Click()
    'Mise � jour de gListePointSynchro
    'Trouver quel point est s�lectionn� dans la ListView
    i = LV_Synchro(monIndex).SelectedItem.Index
    gListePointSynchro(monIndex).MoveIndex i
    gListePointSynchro(monIndex).Remove
    nbPoints(monIndex) = gListePointSynchro(monIndex).count
    Lbl_PtsSynchro(monIndex).Caption = ReadINI_Trad(gLangue, "erreurs", 71) & _
        gListePointSynchro(monIndex).NbPtsSynchro
    Call RemplLV
    Call Form_Resize
End Sub

Private Sub PB_Synchro_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbMiddleButton Then
        Combo_Zoom(Index).SetFocus
        Exit Sub
    End If
    '****************************************************
    '**** Gestion du ZOOM et DEPLACEMENT � la SOURIS ****
    '****************************************************
    MemoShift(Index) = 0
    If Button = vbLeftButton And (Shift And vbShiftMask) <> vbShiftMask And Me.CB_ZoomSelection(Index).value = tbrPressed Then
        'Bouton gauche, pas de Shift et bouton "S�lection" appuy� : zoom par s�lection
        MemoShift(Index) = 1     'au cas o� la touche serait rel�ch�e
        X1Zoom(Index) = x
        Y1Zoom(Index) = y
        PB_Synchro(Index).MousePointer = vbSizePointer  'curseur 4 fl�ches
        PB_Synchro(Index).DrawStyle = vbDot      'trait petits pointill�s
        PB_Synchro(Index).AutoRedraw = False     'lors du d�placement, on va dessiner le rectangle sur le plan temporaire
                                  'pour pouvoir l'effacer au fur et � mesure sans effacer la figure
        Exit Sub
    End If
    
    If Button = vbRightButton And (Shift And vbShiftMask) <> vbShiftMask Then
        'bouton droit, pas de shift : d�placement
        MemoShift(Index) = 2
        X1Pan(Index) = x
        Y1Pan(Index) = y
        MemoX1Pan(Index) = x 'm�morisation du d�but du d�placement pour affectation � XcentreI et YcentreI
        MemoY1Pan(Index) = y '  � la fin du d�placement
        PB_Synchro(Index).MousePointer = vbCustom  'Curseur personnalis� (main 16x16 sur icone 32x32)
        PB_Synchro(Index).MouseIcon = Me.ImageMain.Picture
        Exit Sub
    End If
    
    'Recherche si on a cliqu� sur un point
    Dim MinDist As Double
    Dim maDist As Double
    Dim maDistMini As Double
    Dim P As Point
    Dim minI As Long
    
    If Not bProfilCharge Then Exit Sub
    
    P.x = x
    P.y = y
    P = APItoPLT(P, PB_Synchro(Index), Index)
    
    maDistMini = Sqr((PB_Synchro(Index).ScaleWidth * PB_Synchro(Index).ScaleWidth) + _
        (PB_Synchro(Index).ScaleHeight + PB_Synchro(Index).ScaleHeight)) / 150
    
    gListePointSynchro(Index).MoveFirst
    minI = 1
    i = 1
    MinDist = Calcul_Distance(P, gListePointSynchro(Index).CurrentPoint)
    
    While gListePointSynchro(Index).MoveNext
        i = i + 1
        maDist = Calcul_Distance(P, gListePointSynchro(Index).CurrentPoint)
        If maDist < MinDist Then
            MinDist = maDist
            minI = i
        End If
    Wend
    
    If MinDist > maDistMini Then Exit Sub
    
    Set LV_Synchro(Index).SelectedItem = LV_Synchro(Index).ListItems(minI)
    LV_Synchro(Index).ListItems(minI).EnsureVisible
    
    If Button = vbRightButton Then
        monIndex = Index
        PopupMenu mnu_ctx
    End If

    Call Form_Resize
End Sub

Private Sub PB_Synchro_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    'Pendant le d�placement du curseur, on trace le rectangle de zoom
    '   ou on d�place la figure
    Select Case MemoShift(Index)
    Case 1  'Bouton gauche et zoom par s�lection
        If (Button = vbLeftButton And CB_ZoomSelection(Index) = tbrPressed) Then  'zoom par s�lection
            PB_Synchro(Index).Cls
            PB_Synchro(Index).Line (X1Zoom(Index), Y1Zoom(Index))-(x, y), vbBlack, B   'trac� du rectangle de s�lection
        End If
    Case 2   'Bouton droit, pas de Shift : d�placement
        XcentreZ(Index) = X0(Index) - (x - X1Pan(Index))
        YcentreZ(Index) = Y0(Index) - (y - Y1Pan(Index))
        XcentreI(Index) = XcentreI(Index) + (XcentreZ(Index) - X0(Index)) / CoeffInit(Index)
        YcentreI(Index) = YcentreI(Index) + (PB_Synchro(Index).Height - (YcentreZ(Index) + Y0(Index))) / CoeffInit(Index)
        Call Dessine_Onglet0

        X1Pan(Index) = x 'm�morisation de l'emplacement courant comme nouvelle origine de la translation
        Y1Pan(Index) = y
    End Select
    
    'Recherche si on est pr�s d'un point pour afficher son n�
    Dim MinDist As Double
    Dim maDist As Double
    Dim maDistMini As Double
    Dim P As Point
    Dim minI As Long
    
    If Not bProfilCharge Then Exit Sub
    
    maDistMini = Sqr((PB_Synchro(Index).ScaleWidth * PB_Synchro(Index).ScaleWidth) + (PB_Synchro(Index).ScaleHeight * PB_Synchro(Index).ScaleHeight)) / 150
    
    P.x = x
    P.y = y
    gListePointSynchro(Index).MoveFirst
    MinDist = Calcul_Distance(P, gListePointSynchro(Index).CurrentPoint)
    minI = 1
    i = 1
    
    While gListePointSynchro(Index).MoveNext
        i = i + 1
        maDist = Calcul_Distance(P, gListePointSynchro(Index).CurrentPoint)
        If maDist < MinDist Then
            MinDist = maDist
            minI = i
        End If
    Wend
    If MinDist <= maDistMini Then
        PB_Synchro(Index).ToolTipText = minI
    Else
        PB_Synchro(Index).ToolTipText = ""
    End If
End Sub

Private Sub PB_Synchro_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'Lorsqu'on rel�che le bouton lors d'une op�ration de d�placement
   
    Select Case MemoShift(Index)
    Case 1  'Bouton gauche et zoom par s�lection
        '*** rel�chement du bouton dans le cas du zoom ***
        If (Button = vbLeftButton And Me.CB_ZoomSelection(Index) = tbrPressed) Then     'zoom par s�lection
            'au rel�chement, on efface et on passe sur le plan permanent
            PB_Synchro(Index).MousePointer = vbNormal 'curseur fl�che
            PB_Synchro(Index).Cls
            PB_Synchro(Index).DrawStyle = vbSolid  'traits continus
            PB_Synchro(Index).AutoRedraw = True 'on repasse sur le plan permanent
            On Error GoTo Annuler  'En cas de division par z�ro si pas de d�placement de la souris
            'On va � l'envers pour retrouver les limites de la fen�tre affich�e sur le profil initial
            X1Zoom(Index) = (X1Zoom(Index) - X0(Index)) / CoeffInit(Index) + XcentreI(Index)
            Y1Zoom(Index) = (PB_Synchro(Index).Height - Y1Zoom(Index) - Y0(Index)) / CoeffInit(Index) + YcentreI(Index)
            X2Zoom(Index) = (x - X0(Index)) / CoeffInit(Index) + XcentreI(Index)
            Y2Zoom(Index) = (PB_Synchro(Index).Height - y - Y0(Index)) / CoeffInit(Index) + YcentreI(Index)
            'on calcule le centre de la zone � afficher et son coeff d'ajustement � la fen�tre
            XcentreI(Index) = (X1Zoom(Index) + X2Zoom(Index)) / 2
            YcentreI(Index) = (Y1Zoom(Index) + Y2Zoom(Index)) / 2
            If (PB_Synchro(Index).Width - 2 * Marge) / Abs(X1Zoom(Index) - X2Zoom(Index)) < (PB_Synchro(Index).Height - 2 * Marge) / Abs(Y1Zoom(Index) - Y2Zoom(Index)) Then
                CoeffInit(Index) = (PB_Synchro(Index).Width - 2 * Marge) / Abs(X1Zoom(Index) - X2Zoom(Index))
            Else
                CoeffInit(Index) = (PB_Synchro(Index).Height - 2 * Marge) / Abs(Y1Zoom(Index) - Y2Zoom(Index))
            End If
            Me.CB_ZoomSelection(Index) = tbrUnpressed
            End If
      
    Case 2   'Bouton droit, pas de Shift : d�placement
            '*** rel�chement du bouton dans le cas du d�placement de la figure ***
            PB_Synchro(Index).MousePointer = vbDefault
    End Select
        Call Dessine_Onglet0
    MemoShift(Index) = 0  'reset de la variable pour �v�nement mousemove
Annuler:
    Exit Sub
End Sub

Private Sub RAZButton_Click()
    'RAZ des synchro
    For k = EMPLANTURE To SAUMON
        If Not (gListePointSynchro(k) Is Nothing) Then
            gListePointSynchro(k).RAZPtCles
            nbPoints(k) = gListePointSynchro(k).count
        Else
            nbPoints(k) = 0
        End If
        Lbl_PtsSynchro(k).Caption = ReadINI_Trad(gLangue, "erreurs", 71) & " 0"
    Next k
    
    Call RemplLV
    Call Form_Resize
    SynchroButton.Enabled = True
End Sub

Private Sub SynchroButton_Click()
    Dim j As Long
    Dim iDeb(EMPLANTURE To SAUMON) As Long
    Dim iFin(EMPLANTURE To SAUMON) As Long
    Dim iSegment As Long
    Dim monK As Integer
    Dim NbPtCles(EMPLANTURE To SAUMON) As Long
    Dim P As Point
    Dim P1 As Point
    Dim P2 As Point
    Dim PRet1 As Point
    Dim PRet2 As Point
    Dim maDist As Double
    Dim monRayon As Double
    Dim Rapport(EMPLANTURE To SAUMON) As Double
    Dim EPSILON(EMPLANTURE To SAUMON) As Double
    Dim monNoeud As TypeNoeud
    Dim InterDist As Single
    Dim Dist(EMPLANTURE To SAUMON) As Double
    Dim Point1(EMPLANTURE To SAUMON) As Point
    Dim Point2(EMPLANTURE To SAUMON) As Point
    
    'Recherche des points cl�s dans les listes cha�n�es
    'On doit avoir le m�me nombre (>=2) de points cl�s sur chaque profil
    For k = EMPLANTURE To SAUMON
        NbPtCles(k) = 0
        gListePointSynchro(k).MoveFirst
        Do
            NbPtCles(k) = NbPtCles(k) + IIf(gListePointSynchro(k).CurrentbSynchro, 1, 0)
        Loop While gListePointSynchro(k).MoveNext
        
        'Calcul de l'epsilon
        EPSILON(k) = (gListePointSynchro(k).maxX - gListePointSynchro(k).minX) / 1000#
        iFin(k) = 1
    Next k
    
    If NbPtCles(EMPLANTURE) <> NbPtCles(SAUMON) Then    'Nbres de points cl�s diff�rents
        MsgBox ReadINI_Trad(gLangue, "erreurs", 5), vbCritical
        Exit Sub
    End If
    If NbPtCles(EMPLANTURE) < 2 Then                    'Nbre de points cl�s < 2
        MsgBox ReadINI_Trad(gLangue, "erreurs", 6), vbCritical
        Exit Sub
    End If
    
    For iSegment = 1 To NbPtCles(EMPLANTURE) - 1 'Boucle des segments (tron�ons)
        'On calcule les distances (longueur du tron�on emplanture et du tron�on saumon)
        For k = EMPLANTURE To SAUMON    'Boucle des profils
            iDeb(k) = iFin(k)
            InterDist = 0#
            gListePointSynchro(k).MoveIndex iDeb(k)
            gListePointSynchro(k).Dist 0#
            P1 = gListePointSynchro(k).CurrentPoint
            Do
                gListePointSynchro(k).MoveNext
                iFin(k) = iFin(k) + 1
                P2 = gListePointSynchro(k).CurrentPoint
                InterDist = Calcul_Distance(P1, P2) + InterDist
                gListePointSynchro(k).Dist InterDist
                P1 = P2
            Loop While Not gListePointSynchro(k).CurrentbSynchro
            Dist(k) = InterDist
        Next k
        Rapport(EMPLANTURE) = Dist(EMPLANTURE) / Dist(SAUMON)
        Rapport(SAUMON) = 1# / Rapport(EMPLANTURE)
        
        'Parcours des deux tron�ons de l'emplanture et du saumon
        For k = EMPLANTURE To SAUMON    'Boucle des profils
            gListePointSynchro(k).MoveIndex iDeb(k) + 1
        Next k
        While Not (gListePointSynchro(EMPLANTURE).CurrentbSynchro And _
                   gListePointSynchro(SAUMON).CurrentbSynchro)
            Dist(EMPLANTURE) = gListePointSynchro(EMPLANTURE).CurrentDist
            Dist(SAUMON) = gListePointSynchro(SAUMON).CurrentDist
            If Abs(Dist(EMPLANTURE) / Rapport(EMPLANTURE) - Dist(SAUMON)) > EPSILON(SAUMON) Then
                If Dist(EMPLANTURE) / Rapport(EMPLANTURE) < Dist(SAUMON) Then
                    monK = SAUMON       'On cr�e dans le saumon
                    monRayon = Dist(EMPLANTURE) / Rapport(EMPLANTURE) - Dist(SAUMON)
                Else
                    monK = EMPLANTURE   'On cr�e dans l'emplanture
                    monRayon = Dist(SAUMON) / Rapport(SAUMON) - Dist(EMPLANTURE)
                End If
                
                P1 = gListePointSynchro(monK).PreviousPoint
                P2 = gListePointSynchro(monK).CurrentPoint
                'Le point P est situ� entre les points P1 et P2
                
                Call Calcul_Inter_Droite_Cercle(P1, P2, P2, monRayon, PRet1, PRet2)
                'Il faut ensuite trouver le point compris entre p1 et p2
                If Calcul_Distance(P1, PRet1) + Calcul_Distance(P2, PRet1) < _
                    Calcul_Distance(P1, PRet2) + Calcul_Distance(P2, PRet2) Then
                    P = PRet1
                Else
                    P = PRet2
                End If
                monNoeud.x = P.x
                monNoeud.y = P.y
                monNoeud.bSynchro = False
                monNoeud.bTemp = True
                gListePointSynchro(monK).Insert monNoeud
                iFin(monK) = iFin(monK) + 1
            End If      ' < EPSILON
            For k = EMPLANTURE To SAUMON
                If Not gListePointSynchro(k).CurrentbSynchro Then
                    gListePointSynchro(k).MoveNext
                End If
            Next k
        Wend
    Next iSegment
    
    For k = EMPLANTURE To SAUMON    'Boucle des profils
        nbPoints(k) = gListePointSynchro(k).count
    Next k
    
    Call RemplLV
    Call Form_Resize
    SynchroButton.Enabled = False
End Sub

Private Sub TabStrip1_Click()
    Dim Ctrl As Control
    For Each Ctrl In Controls
        If UCase$(Left(Ctrl.Name, 5)) = "FRAME" Then
            Ctrl.Visible = (Val(Mid$(Ctrl.Name, 7, 1)) = TabStrip1.SelectedItem.Index)
        End If
    Next
End Sub

Private Sub TB_Angle_Atteint_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Angle_Atteint_LostFocus(Index)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Angle_Atteint_LostFocus(Index As Integer)
    If Not bProfilCharge Then Exit Sub
    If TB_Angle_Atteint(Index) = "" Then TB_Angle_Atteint(Index) = 0#
    If Not IsNumeric(TB_Angle_Atteint(Index)) Then TB_Angle_Atteint(Index) = 0#
    If Val(TB_Angle_Atteint(Index)) = 0 Then TB_Angle_Atteint(Index) = Format(0#, "#0.0")
    wAngle_Atteint(Index) = CDbl(TB_Angle_Atteint(Index))
    bSav = bProfilCharge
    bProfilCharge = False
    TB_Angle_Atteint(Index) = Round(TB_Angle_Atteint(Index), 1)
    HScroll_Rot(Index).value = TB_Angle_Atteint(Index)
    bProfilCharge = bSav
    Call Form_Resize
End Sub

Private Sub TB_Corde_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Corde_LostFocus(Index)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Corde_LostFocus(Index As Integer)
    If Not bProfilCharge Then Exit Sub
    If Val(TB_Corde(Index)) = 0 Then TB_Corde(Index) = Format(1#, "#0.0")
    If TB_Corde(Index) = "" Then TB_Corde(Index) = 0#
    If Not IsNumeric(TB_Corde(Index)) Then TB_Corde(Index) = 0#
    wCorde(Index) = CDbl(TB_Corde(Index))
    If wCorde(Index) = 0# Then Exit Sub
    bSav = bProfilCharge
    bProfilCharge = False
    wCorde(Index) = CDbl(TB_Corde(Index))
    
    gListePointSynchro(Index).ModifCorde (wCorde(Index))

    Call RecopieVersTableau1
    Call MaxiMini_profils
    wFleche_BA = Round(((P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x) - (P_maxi(SAUMON).x - P_mini(SAUMON).x)) / 2, 1)
    TB_Fleche_BA = wFleche_BA
    
    wH(Index) = gListePointSynchro(Index).maxY - gListePointSynchro(Index).minY
    TB_H(Index) = Round(wH(Index), 2)
    TB_DimMod(Index) = TB_H(Index)
    bProfilCharge = bSav
    
    Call Form_Resize
End Sub

Private Sub TB_Epaisseur_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Epaisseur_LostFocus
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Epaisseur_LostFocus()
    If Not bProfilCharge Then Exit Sub
    
    If Not IsNumeric(TB_Epaisseur) Then
        wEpaisseur = 0#
    Else
        wEpaisseur = CDbl(TB_Epaisseur)
    End If
    TB_Epaisseur = Format$(wEpaisseur, "#0.0")
    Call Form_Resize
End Sub

Private Sub Recalcul_Hauteur()
    Dim j As Integer
    'Recalcul des coordonn�es en cas de changement de hauteur
    Dim Rapport As Double
    
    For j = EMPLANTURE To SAUMON
        Rapport = Val(TB_DimMod(j)) / (Pmax(j).y - Pmin(j).y)
        For i = 0 To nbPoints(j) - 1
            monTableau(j, i).y = Round(monTableau(j, i).y * Rapport, 4)
        Next i
    Next j
    Call CalculMinMaxPointPLT(monTableau)
End Sub

Private Sub CalculCordeHauteurFleche()
    bSav = bProfilCharge
    bProfilCharge = False
    TB_Fleche_BA = TB_Corde(EMPLANTURE) - TB_Corde(SAUMON)
    TB_Fleche_BF = "0"
    bProfilCharge = bSav
End Sub

Private Sub TB_Decalage_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Decalage_LostFocus
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Decalage_LostFocus()
    If Val(TB_Decalage) = 0 Then TB_Decalage = Format(0#, "#0.0")
    If TB_Decalage = "" Or Not bProfilCharge Then Exit Sub
    If Not IsNumeric(TB_Decalage) Then
        wDessous(SAUMON) = wDessous(EMPLANTURE)
    Else
        wDessous(SAUMON) = wDessous(EMPLANTURE) + CSng(TB_Decalage)
    End If
    bSav = bProfilCharge
    bProfilCharge = False
    TB_Decalage = Format$(TB_Decalage, "#0.0")
    bProfilCharge = bSav
    Call Form_Resize
End Sub

Private Sub TB_Diedre_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Diedre_LostFocus
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Diedre_LostFocus()
    If CDbl(TB_Diedre) = 0 Then TB_Diedre = Format(0#, "#0.0")
    If TB_Diedre = "" Then TB_Diedre = 0#
    If Not IsNumeric(TB_Diedre) Then TB_Diedre = 0#
    If Not bProfilCharge Then Exit Sub
    TB_Diedre = Format$(TB_Diedre, "#0.0")
    bSav = bProfilCharge
    bProfilCharge = False
    wDessous(SAUMON) = wDessous(SAUMON) + (P_maxi(EMPLANTURE).y + P_mini(EMPLANTURE).y) / 2 - _
        (P_maxi(SAUMON).y + P_mini(SAUMON).y) / 2
    bProfilCharge = bSav
    Call Form_Resize
End Sub

Private Sub TB_DimMod_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_DimMod_LostFocus(Index)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_DimMod_LostFocus(Index As Integer)
    If Not bProfilCharge Then Exit Sub
    If Not IsNumeric(TB_DimMod(Index)) Then TB_DimMod(Index) = 0#
    If CDbl(TB_DimMod(Index)) = 0 Then TB_DimMod(Index) = Val(TB_H(Index))
    TB_DimMod(Index) = Round(TB_DimMod(Index), 2)
    bSav = bProfilCharge
    bProfilCharge = False
    wdimMult(Index) = TB_DimMod(Index) / wH(Index)
    TB_DimMult(Index) = Round(wdimMult(Index), 2)
    bProfilCharge = bSav
    Call Form_Resize
End Sub

Private Sub TB_DimMult_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_DimMult_LostFocus(Index)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_DimMult_LostFocus(Index As Integer)
    If Not bProfilCharge Then Exit Sub
    If TB_DimMult(Index) = "" Then TB_DimMult(Index) = 1#
    If Not IsNumeric(TB_DimMult(Index)) Then TB_DimMult(Index) = 1#
    wdimMult(Index) = CDbl(TB_DimMult(Index))
    TB_DimMult(Index) = Round(wdimMult(Index), 2)
    If wdimMult(Index) <> 0 And bProfilCharge Then
        TB_DimMod(Index) = Round(wH(Index) * wdimMult(Index), 2)
    End If
    Call Form_Resize
End Sub

Private Sub TB_Envergure_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Envergure_LostFocus
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Envergure_LostFocus()
    If Not bProfilCharge Then Exit Sub
    If TB_Envergure = "" Then Exit Sub
    If Not IsNumeric(TB_Envergure) Then Exit Sub
    If CDbl(TB_Envergure) = 0 Then Exit Sub
    If CDbl(TB_Envergure) > CDbl(ReadINI_Machine_Num("table_" & gstrTable, "TB_SAV_23", 600)) Then
        MsgBox LabelErrEnvergure, vbCritical
        TB_Envergure = Round(wEnvergure, 1)
        Exit Sub
    End If
    wEnvergure = CDbl(TB_Envergure)
    TB_Envergure = Round(TB_Envergure, 1)
    Call Form_Resize
End Sub

Private Sub TB_Fleche_BA_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Fleche_BA_LostFocus
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Fleche_BA_LostFocus()
    If TB_Fleche_BA = "" Or Not bProfilCharge Then Exit Sub
    If Not IsNumeric(TB_Fleche_BA) Then
        Exit Sub
    End If
    wFleche_BA = CDbl(TB_Fleche_BA)
    bSav = bProfilCharge
    bProfilCharge = False
    wFleche_BF = Round((P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x) - TB_Fleche_BA - _
        (P_maxi(SAUMON).x - P_mini(SAUMON).x), 1)
    TB_Fleche_BF = Format(wFleche_BF, "#.0")
    bProfilCharge = bSav
    Call Form_Resize
End Sub

Private Sub TB_Fleche_BF_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_Fleche_BF_LostFocus
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_Fleche_BF_LostFocus()
    If TB_Fleche_BF = "" Or Not bProfilCharge Then Exit Sub
    If Not IsNumeric(TB_Fleche_BF) Then
        Exit Sub
    End If
    wFleche_BF = CDbl(TB_Fleche_BF)
    bSav = bProfilCharge
    bProfilCharge = False
    wFleche_BA = Round((P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x) - TB_Fleche_BF - _
        (P_maxi(SAUMON).x - P_mini(SAUMON).x), 1)
    TB_Fleche_BA = Format(wFleche_BA, "#.0")
    bProfilCharge = bSav
    Call Form_Resize
End Sub

Function InverseX(x As Single, EouS As Integer) As Single
    'Inversion de l'abcisse d'un point (sert pour le miroir horizontal)
    InverseX = P_maxi(EouS).x - x + P_mini(EouS).x
End Function

Function InverseY(y As Single, EouS As Integer) As Single
    'Inversion de l'ordonn�e d'un point (sert pour le miroir horizontal)
    InverseY = P_maxi(EouS).y - y + P_mini(EouS).y
End Function

Public Sub ValeurDefaut()
    For k = EMPLANTURE To SAUMON
        wCorde(k) = Format(100, "#.0")
        HScroll_Rot(k) = 0
        bMiroirHorizontal(k) = False
        bMiroirVertical(k) = False
        bProfil(k) = False
        TB_Angle_Atteint(k) = 0
        wdimMult(k) = 1
        wDessous(k) = 5#
        wMargeBA(k) = 5#
        wMargeBF(k) = 5#
    Next k
    
    wFleche_BA = 0
    wFleche_BF = 0
    wEnvergure = 600
    wComm = ""
End Sub

Private Function RecopieVersTableau1()
    ReDim monTableau1(1 To 2, 0 To Max(gListePointSynchro(EMPLANTURE).count, gListePointSynchro(SAUMON).count) - 1)
    'On recopie gListePointSynchro vers monTableau1 en redimensionnant au passage et en
    'calculant les min et max
    For k = EMPLANTURE To SAUMON
        gListePointSynchro(k).MoveFirst
        P_mini(k).x = monTableau1(k, 0).x
        P_mini(k).y = monTableau1(k, 0).y
        P_maxi(k).x = monTableau1(k, 0).x
        P_maxi(k).y = monTableau1(k, 0).y
        
        i = 0
        Do
            monTableau1(k, i).x = gListePointSynchro(k).CurrentPoint.x
            monTableau1(k, i).y = gListePointSynchro(k).CurrentPoint.y * wdimMult(k)

            P_maxi(k).x = Max(P_maxi(k).x, monTableau1(k, i).x)
            P_mini(k).x = Min(P_mini(k).x, monTableau1(k, i).x)
            P_maxi(k).y = Max(P_maxi(k).y, monTableau1(k, i).y)
            P_mini(k).y = Min(P_mini(k).y, monTableau1(k, i).y)

            i = i + 1
        Loop While gListePointSynchro(k).MoveNext
        nbPoints(k) = gListePointSynchro(k).count
    Next k
End Function

Private Function RecopieVersListe(Tableau() As PointPLT, Liste As ClsListeChainee, Corde As Single)
    Dim xMax As Single, xMin As Single
    Dim Rapport As Single
    Dim monElem As TypeNoeud
    
    xMax = Tableau(0).x
    xMin = xMax
    For i = 1 To UBound(Tableau)
        xMax = Max(xMax, Tableau(i).x)
        xMin = Min(xMin, Tableau(i).x)
    Next i
    Rapport = (Corde / (xMax - xMin))
    
    'On recopie vers la liste cha�n�e
    monElem.bSynchro = False
    monElem.bTemp = False
    
    Set Liste = New ClsListeChainee
        
    For i = 0 To UBound(Tableau)
        monElem.x = Tableau(i).x * Rapport
        monElem.y = Tableau(i).y * Rapport
        Liste.Insert monElem
        Liste.MoveNext
    Next i
    
    Liste.MoveFirst 'Pour ne pas pointer sur rien
End Function

Private Function Miroir()
    For k = EMPLANTURE To SAUMON
        If bMiroirHorizontal(k) Or bMiroirVertical(k) Then
            For i = 0 To nbPoints(k) - 1
                If bMiroirHorizontal(k) Then
                    monTableau1(k, i).x = InverseX(monTableau1(k, i).x, k)
                End If
                If bMiroirVertical(k) Then
                    monTableau1(k, i).y = InverseY(monTableau1(k, i).y, k)
                End If
            Next i
        End If
    Next k
End Function

Private Function Rotation()
    'Rotation
    Dim PointCentre As Point
    Dim dRayon As Double
    Dim dAnglePoint As Double
    Dim dAngle As Double
    Dim monAngle As Double
    Dim P As Point
    
    For k = EMPLANTURE To SAUMON
        PointCentre.x = (P_maxi(k).x + P_mini(k).x) / 2
        PointCentre.y = (P_maxi(k).y + P_mini(k).y) / 2

        monAngle = -TB_Angle_Atteint(k) * PI / 180#
        If monAngle <> 0# Then
            For i = 0 To nbPoints(k) - 1
                P = monTableau1(k, i)
                dRayon = Calcul_Distance(P, PointCentre)
                Call Calcul_Ang_Segment(PointCentre, P, dAnglePoint)
                If dRayon <> 0 Then
                    dAngle = dAnglePoint + monAngle
                    monTableau1(k, i).x = dRayon * Cos(dAngle) + PointCentre.x
                    monTableau1(k, i).y = dRayon * Sin(dAngle) + PointCentre.y
                End If
            Next i
        End If
    Next k
End Function

Private Function Zero()
    'On ram�ne � 0,0
    For k = EMPLANTURE To SAUMON
        If P_mini(k).x <> 0 Or P_mini(k).y <> 0 Then
            For i = 0 To nbPoints(k) - 1
                monTableau1(k, i).x = monTableau1(k, i).x - P_mini(k).x
                monTableau1(k, i).y = monTableau1(k, i).y - P_mini(k).y
            Next i
        End If
        P_maxi(k).x = P_maxi(k).x - P_mini(k).x
        P_mini(k).x = 0
        P_maxi(k).y = P_maxi(k).y - P_mini(k).y
        P_mini(k).y = 0
    Next k
End Function

Private Function Decalage(E_DecalY As Single, S_DecalY As Single, DecalX As Single)
    If E_DecalY = 0# And S_DecalY = 0# And DecalX = 0# Then Exit Function
    'D�calages en X et en Y
    
    For i = LBound(monTableau1, 2) To UBound(monTableau1, 2)
        monTableau1(SAUMON, i).x = monTableau1(SAUMON, i).x + DecalX
        monTableau1(SAUMON, i).y = monTableau1(SAUMON, i).y + S_DecalY
        monTableau1(EMPLANTURE, i).y = monTableau1(EMPLANTURE, i).y + E_DecalY
    Next i
End Function

Private Sub cmdDep_Click(Index As Integer)
    'Commande de d�placement dans le bloc
    Dim maValeur As Single
    
    Select Case Index
        Case 1
            maValeur = 10#
        Case 2
            maValeur = 1#
        Case 3
            maValeur = 0.1
        Case 4
            maValeur = 10#
        Case 5
            maValeur = 1#
        Case 6
            maValeur = 0.1
        Case 7
            maValeur = -0.1
        Case 8
            maValeur = -1#
        Case 9
            maValeur = -10#
        Case 10
            maValeur = -0.1
        Case 11
            maValeur = -1#
        Case 12
            maValeur = -10#
    End Select
    Select Case Index
        Case 1 To 3, 7 To 9 'D�placement vertical
            If maValeur < 0 Then
                If Abs(maValeur) > wDessous(EMPLANTURE) Or Abs(maValeur) > wDessous(SAUMON) Then
                    Exit Sub
                End If
            End If
            For k = EMPLANTURE To SAUMON
                wDessous(k) = wDessous(k) + maValeur
            Next k
        Case 4 To 6, 10 To 12 'D�placement horizontal
            For k = EMPLANTURE To SAUMON
                wMargeBA(k) = wMargeBA(k) + maValeur
                TB_MargeBA(k).Text = Str(wMargeBA(k))
                wMargeBF(k) = wMargeBF(k) - maValeur
                TB_MargeBF(k).Text = Str(wMargeBF(k))
            Next k
    End Select
    Call Form_Resize
End Sub

Private Sub cmdEp_Click(Index As Integer)
    'Commandes Epaisseur
    Dim maValeur As Single
    
    Select Case Index
        Case 1
            maValeur = 10#
        Case 2
            maValeur = 1#
        Case 3
            maValeur = 0.1
        Case 4
            maValeur = -10#
        Case 5
            maValeur = -1#
        Case 6
            maValeur = -0.1
    End Select
    wEpaisseur = wEpaisseur + maValeur
    If wEpaisseur < 0 Then
        Beep
        wEpaisseur = 0
    End If
    TB_Epaisseur.Text = Format$(Round(wEpaisseur, 1), "#.0")
    Call Form_Resize
End Sub

Private Sub cmdMBA_Click(Index As Integer)
    'Commande Marge au Bord d'Attaque
    Dim maValeur As Single
    
    Select Case Index Mod 4
        Case 1
            maValeur = 1#
        Case 2
            maValeur = 0.1
        Case 3
            maValeur = -1#
        Case 0
            maValeur = -0.1
    End Select
    k = EMPLANTURE
    If Index > 4 Then k = SAUMON
    wMargeBA(k) = wMargeBA(k) + maValeur
    TB_MargeBA(k).Text = Str(wMargeBA(k))
    Call Form_Resize
End Sub

Private Sub cmdMBF_Click(Index As Integer)
    'Commande marge au Bord de Fuite
    Dim maValeur As Single
    
    Select Case Index Mod 4
        Case 1
            maValeur = 1#
        Case 2
            maValeur = 0.1
        Case 3
            maValeur = -1#
        Case 0
            maValeur = -0.1
    End Select
    k = EMPLANTURE
    If Index > 4 Then k = SAUMON
    wMargeBF(k) = wMargeBF(k) + maValeur
    TB_MargeBF(k).Text = Str(wMargeBF(k))
    Call Form_Resize
End Sub

Private Sub cmdInit_5_Click()
    'Initialisation : 5 mm partout
    chkProp.value = 0
    
    If wDessous(EMPLANTURE) < wDessous(SAUMON) Then
        wDessous(SAUMON) = wDessous(SAUMON) - (wDessous(EMPLANTURE) - 5#)
        wDessous(EMPLANTURE) = 5#
    Else
        wDessous(EMPLANTURE) = wDessous(EMPLANTURE) - (wDessous(SAUMON) - 5#)
        wDessous(SAUMON) = 5#
    End If
    For k = EMPLANTURE To SAUMON
        wMargeBA(k) = 5#
        wMargeBF(k) = 5#
        TB_MDessous(k).Text = Str(wDessous(k))
    Next k
  
    Call Form_Resize
End Sub

Private Sub chkProp_Click()
    Dim j As Integer
    If chkProp.value = vbChecked Then
        wMargeBA(SAUMON) = (P_maxi(SAUMON).x - P_mini(SAUMON).x) * wMargeBA(EMPLANTURE) / (P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x)
        wMargeBF(SAUMON) = (P_maxi(SAUMON).x - P_mini(SAUMON).x) * wMargeBF(EMPLANTURE) / (P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x)
        TB_MargeBA(SAUMON).Text = Str(wMargeBA(SAUMON))
        TB_MargeBF(SAUMON).Text = Str(wMargeBF(SAUMON))
        TB_MargeBA(SAUMON).Enabled = False
        TB_MargeBF(SAUMON).Enabled = False
        For j = 5 To 8
            cmdMBA(j).Enabled = False
            cmdMBF(j).Enabled = False
        Next j
    Else
        TB_MargeBA(SAUMON).Enabled = True
        TB_MargeBF(SAUMON).Enabled = True
        For j = 5 To 8
            cmdMBA(j).Enabled = True
            cmdMBF(j).Enabled = True
        Next j
  End If
  Call Form_Resize
End Sub

Private Sub TB_MargeBA_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_MargeBA_LostFocus(Index)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_MargeBA_LostFocus(Index As Integer)
    If Not bProfilCharge Then Exit Sub
    If Val(TB_MargeBA(Index)) = 0 Then TB_MargeBA(Index) = Format(0#, "#0.00")
    TB_MargeBA(Index) = Format$(Val(TB_MargeBA(Index)), "#0.00")
    If TB_MargeBA(Index) = "" Then TB_MargeBA(Index) = 0#
    If Not IsNumeric(TB_MargeBA(Index)) Then
        Exit Sub
    End If
    wMargeBA(Index) = CDbl(TB_MargeBA(Index))
    Call Form_Resize
End Sub

Private Sub TB_MargeBF_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call TB_MargeBf_LostFocus(Index)
        Exit Sub
    End If
    KeyAscii = CtrlKeyPress(KeyAscii)
End Sub

Private Sub TB_MargeBf_LostFocus(Index As Integer)
    If Not bProfilCharge Then Exit Sub
    If Val(TB_MargeBF(Index)) = 0 Then TB_MargeBF(Index) = Format(0#, "#0.00")
    TB_MargeBF(Index) = Format$(Val(TB_MargeBF(Index)), "#0.00")
    If TB_MargeBF(Index) = "" Then TB_MargeBF(Index) = 0#
    If Not IsNumeric(TB_MargeBF(Index)) Then
        Exit Sub
    End If
    wMargeBF(Index) = CDbl(TB_MargeBF(Index))
    Call Form_Resize
End Sub

Private Sub MaxiMini_tout()
    x1 = Min(P_mini(EMPLANTURE).x, P_mini(SAUMON).x)
    x2 = Max(P_maxi(EMPLANTURE).x, P_maxi(SAUMON).x)
    y1 = Min(P_mini(EMPLANTURE).y, P_mini(SAUMON).y)
    y2 = Max(P_maxi(EMPLANTURE).y, P_maxi(SAUMON).y)
    For k = EMPLANTURE To SAUMON
        x1 = Min(x1, tabBloc(k, 0).x)
        x2 = Max(x2, tabBloc(k, 1).x)
        y1 = Min(y1, tabBloc(k, 2).y)
        y2 = Max(y2, tabBloc(k, 1).y)
    Next k
End Sub
Private Sub MaxiMini_profils()
    For k = EMPLANTURE To SAUMON
        P_mini(k) = monTableau1(k, 0)
        P_maxi(k) = monTableau1(k, 0)
        P_BA(k) = monTableau1(k, 0)
        P_BF(k) = monTableau1(k, 0)
        P_sup(k) = monTableau1(k, 0)
        P_inf(k) = monTableau1(k, 0)
        For i = 1 To UBound(monTableau1, 2)
            If monTableau1(k, i).x > P_BF(k).x Then
                P_BF(k) = monTableau1(k, i)
            End If
            If monTableau1(k, i).x < P_BA(k).x Then
                P_BA(k) = monTableau1(k, i)
            End If
            If monTableau1(k, i).y > P_sup(k).y Then
                P_sup(k) = monTableau1(k, i)
            End If
            If monTableau1(k, i).y < P_inf(k).y Then
                P_inf(k) = monTableau1(k, i)
            End If
            If monTableau1(k, i).x > P_maxi(k).x Then P_maxi(k).x = monTableau1(k, i).x
            If monTableau1(k, i).x < P_mini(k).x Then P_mini(k).x = monTableau1(k, i).x
            If monTableau1(k, i).y > P_maxi(k).y Then P_maxi(k).y = monTableau1(k, i).y
            If monTableau1(k, i).y < P_mini(k).y Then P_mini(k).y = monTableau1(k, i).y
        Next i
    Next k
End Sub

Private Sub Alignement(x1 As Single, y1 As Single)
    If x1 = 0# And y1 = 0# Then Exit Sub

    For k = EMPLANTURE To SAUMON
        For i = 0 To UBound(monTableau1, 2)
            monTableau1(k, i).x = monTableau1(k, i).x - x1
            monTableau1(k, i).y = monTableau1(k, i).y - y1
        Next i
        For i = 0 To UBound(tabBloc, 2)
            tabBloc(k, i).x = tabBloc(k, i).x - x1
            tabBloc(k, i).y = tabBloc(k, i).y - y1
        Next i
    Next k
End Sub

Private Sub Affiche3D()
    'Variables pour l'affichage 3D
    Dim XE As Single, XS As Single, DXA As Single, DXF As Single, y As Single, z As Single
    Dim Hdessin As Single, Vdessin As Single, CoeffHorizontal As Single
    Dim CoeffVertical As Single, Increment As Single
    Dim A As Single, B As Single, XO As Single, YO As Single, XV1 As Single, YV1 As Single, XV2 As Single
    Dim YV2 As Single, XV3 As Single, YV3 As Single, XV4 As Single, YV4 As Single
    Dim XP1 As Single, XP2 As Single, XP3 As Single, XP4 As Single, XPmin As Single, XPmax As Single
    Dim VB(1 To 3, 1 To 2) As Single
    Dim CouleurBloc As Long
    
    'Pour l'instant on travaille sur le bloc rectangulaire qui contient le bloc avec fl�ches BA/BF
    'On d�termine vue du dessus les dimensions de ce bloc rectangulaire :
    XP1 = 0
    XP2 = wMargeBA(EMPLANTURE) + (P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x) + wMargeBF(EMPLANTURE)
    XP3 = tabBloc(SAUMON, 0).x - tabBloc(EMPLANTURE, 0).x
    XP4 = tabBloc(SAUMON, 0).x - tabBloc(EMPLANTURE, 0).x + wMargeBA(SAUMON) + (P_maxi(SAUMON).x - P_mini(SAUMON).x) + wMargeBF(SAUMON)
    
    XPmin = Min(XP1, XP3)
    XPmax = Max(XP2, XP4)
        
    CouleurBloc = RGB(0, 102, 0)
    z = wEnvergure
    y = wEpaisseur
    A = 0.5   'Angle des ar�tes de droite
    B = 0.6   'Angle des ar�tes de gauche
    
    VB(1, 1) = z * Cos(A) 'Vecteur du bord du bloc parall�le � Z
    VB(1, 2) = -z * Sin(A)
    
    VB(2, 1) = -(XPmax - XPmin) * Cos(B) 'Vecteur du bord parall�le � X
    VB(2, 2) = -(XPmax - XPmin) * Sin(B)
    
    VB(3, 1) = 0 'vecteur de l'�paisseur
    VB(3, 2) = -y
        
    'dimensions du trac�
    Hdessin = Abs(VB(1, 1)) + Abs(VB(2, 1))
    Vdessin = Abs(VB(3, 2)) + Abs(VB(1, 2)) + Abs(VB(2, 2))
    
    'Ajustement � la zone de dessin
    CoeffHorizontal = (PB_3D.ScaleWidth - 200) / Hdessin
    CoeffVertical = (PB_3D.ScaleHeight - 200) / Vdessin
    For i = 1 To 3
      VB(i, 1) = VB(i, 1) * CoeffHorizontal
      VB(i, 2) = VB(i, 2) * CoeffVertical
    Next i
    
    'D�finition du vecteur qui am�ne au point de d�but du trac� (par en bas)
    XO = Abs(VB(2, 1)) + 100
    YO = PB_3D.ScaleHeight - 100
    
    PB_3D.Cls

    'Affichage du bloc en lignes pointill�s
    
    PB_3D.ForeColor = RGB(51, 51, 255)
    PB_3D.ForeColor = vbBlack
    PB_3D.DrawStyle = 1
    PB_3D.DrawWidth = 1
    
    For i = 1 To 3
      PB_3D.Line (XO, YO)-(XO + VB(i, 1), YO + VB(i, 2))
    Next i
    For i = 1 To 2
      PB_3D.Line (XO + VB(3, 1), YO + VB(3, 2))-(XO + VB(3, 1) + VB(i, 1), YO + VB(3, 2) + VB(i, 2))
    Next i
    PB_3D.Line (XO + VB(1, 1), YO + VB(1, 2))-(XO + VB(1, 1) + VB(3, 1), YO + VB(1, 2) + VB(3, 2))
    PB_3D.Line (XO + VB(1, 1) + VB(3, 1), YO + VB(1, 2) + VB(3, 2))-(XO + VB(1, 1) + VB(3, 1) + VB(2, 1), YO + VB(1, 2) + VB(3, 2) + VB(2, 2))
    PB_3D.Line (XO + VB(2, 1), YO + VB(2, 2))-(XO + VB(2, 1) + VB(3, 1), YO + VB(2, 2) + VB(3, 2))
    PB_3D.Line (XO + VB(2, 1) + VB(3, 1), YO + VB(2, 2) + VB(3, 2))-(XO + VB(1, 1) + VB(3, 1) + VB(2, 1), YO + VB(1, 2) + VB(3, 2) + VB(2, 2))

    DXA = tabBloc(SAUMON, 0).x - tabBloc(EMPLANTURE, 0).x
    DXF = tabBloc(EMPLANTURE, 1).x - tabBloc(SAUMON, 1).x
    XE = wMargeBA(EMPLANTURE) + (P_maxi(EMPLANTURE).x - P_mini(EMPLANTURE).x) + wMargeBF(EMPLANTURE)
    XS = wMargeBA(SAUMON) + (P_maxi(SAUMON).x - P_mini(SAUMON).x) + wMargeBF(SAUMON)
    
    XV2 = -XE * Cos(B) * CoeffHorizontal 'Vecteur de l'emplanture
    YV2 = -XE * Sin(B) * CoeffVertical
  
    XV3 = -DXF * Cos(B) * CoeffHorizontal 'Vecteur de la fl�che au bord de fuite
    YV3 = -DXF * Sin(B) * CoeffVertical
    
    XV1 = -XS * Cos(B) * CoeffHorizontal 'Vecteur du saumon
    YV1 = -XS * Sin(B) * CoeffVertical
    
    XV4 = DXA * Cos(B) * CoeffHorizontal 'Vecteur de la fl�che au bord d'attaque
    YV4 = DXA * Sin(B) * CoeffVertical

    'Affichage du bloc en lignes pleines
    If DXF > 0 Then  ' il faut d�finir le point de d�part des vecteurs en fonction de la fl�che BF saumon
      XO = Abs(VB(2, 1)) + 100
      YO = PB_3D.ScaleHeight - 100
    Else
      XO = Abs(VB(2, 1)) - Abs(XV3) + 100
      YO = PB_3D.ScaleHeight - Abs(XV3) - 100
    End If
    
    PB_3D.DrawStyle = 0
    PB_3D.DrawWidth = 2
    PB_3D.ForeColor = CouleurBloc
    'bords de fuite
    PB_3D.Line (XO, YO)-(XO + VB(1, 1) + XV3, YO + VB(1, 2) + YV3)
    PB_3D.Line (XO, YO + VB(3, 2))-(XO + VB(1, 1) + XV3, YO + VB(3, 2) + VB(1, 2) + YV3)
    PB_3D.ForeColor = Couleur(EMPLANTURE)
    PB_3D.Line (XO, YO)-(XO, YO + VB(3, 2)) 'segment vertical en bas
    PB_3D.ForeColor = Couleur(SAUMON)
    PB_3D.Line (XO + VB(1, 1) + XV3, YO + VB(1, 2) + YV3)-(XO + VB(1, 1) + XV3, YO + VB(1, 2) + YV3 + VB(3, 2))
    'saumon
    PB_3D.Line (XO + VB(1, 1) + XV3, YO + VB(1, 2) + YV3 + VB(3, 2))-(XO + VB(1, 1) + XV3 + XV1, YO + VB(1, 2) + YV3 + VB(3, 2) + YV1)
    'emplanture
    PB_3D.ForeColor = Couleur(EMPLANTURE)
    PB_3D.Line (XO, YO)-(XO + XV2, YO + YV2)
    PB_3D.Line (XO, YO + VB(3, 2))-(XO + XV2, YO + YV2 + VB(3, 2))
    PB_3D.Line (XO + XV2, YO + YV2)-(XO + XV2, YO + YV2 + VB(3, 2))
    'bord d'attaque
    PB_3D.ForeColor = CouleurBloc
    PB_3D.Line (XO + XV2, YO + YV2 + VB(3, 2))-(XO + XV2 + VB(1, 1) + XV4, YO + YV2 + VB(3, 2) + VB(1, 2) + YV4)
End Sub

Function M_X(x As Single) As Single
    M_X = x * Rapport + decalageX + CentrageX
End Function

Function M_Y(y As Single) As Single
    M_Y = limiteY - y * Rapport
End Function

Public Sub RemplLV()
    Dim mItem As Variant
    
    For k = EMPLANTURE To SAUMON
        LV_Synchro(k).ListItems.Clear
        If Not (gListePointSynchro(k) Is Nothing) Then
            gListePointSynchro(k).MoveFirst
            For i = 1 To gListePointSynchro(k).count
                LV_Synchro(k).ListItems.Add
                LV_Synchro(k).ListItems(i).SubItems(1) = Format$(i, "###")
                LV_Synchro(k).ListItems(i).SubItems(2) = Format$(gListePointSynchro(k).CurrentX, "0.000")
                LV_Synchro(k).ListItems(i).SubItems(3) = Format$(gListePointSynchro(k).CurrentY, "0.000")
                LV_Synchro(k).ListItems(i).Checked = gListePointSynchro(k).CurrentbSynchro
                If gListePointSynchro(k).CurrentbTemp Then
                    Set mItem = LV_Synchro(k).ListItems(i)
                    mItem.Bold = True
                    mItem.ListSubItems(1).ForeColor = &H0
                    mItem.ListSubItems(2).ForeColor = &H0
                    mItem.ListSubItems(3).ForeColor = &H0
                End If
    
                gListePointSynchro(k).MoveNext
            Next i
        End If
    Next k
End Sub

Private Function PLTtoAPI(P As Point, PB As PictureBox, k As Integer) As Point
    PLTtoAPI.x = (P.x - XcentreI(k)) * CoeffInit(k) + X0(k)
    PLTtoAPI.y = PB.Height - ((P.y - YcentreI(k)) * CoeffInit(k) + Y0(k))
End Function

Private Function APItoPLT(P As Point, PB As PictureBox, k As Integer) As Point
    APItoPLT.x = (P.x - X0(k)) / CoeffInit(k) + XcentreI(k)
    APItoPLT.y = (PB.Height - P.y - Y0(k)) / CoeffInit(k) + YcentreI(k)
End Function

