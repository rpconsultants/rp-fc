/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Tue Jul 29 13:35:43 2003
 */
/* Compiler settings for E:\Microsoft Visual Studio\MyProjects\NTGraph3D_ATL\NTGraph3D_ATL\NTGraph3D_src\NTGraph3D.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IGraph3D = {0x74881C3E,0x3B4A,0x4134,{0x90,0x69,0x1B,0x6E,0x6B,0x2E,0x88,0x6F}};


const IID LIBID_NTGRAPH3DLib = {0x7A99C13D,0x22DF,0x400C,{0xAB,0x37,0x48,0xC3,0x2A,0x89,0x7E,0xFB}};


const CLSID CLSID_NTGraph3D = {0xDF3EB502,0x551C,0x4A90,{0xB5,0x64,0x66,0x81,0xA6,0x96,0x2F,0x58}};


#ifdef __cplusplus
}
#endif

