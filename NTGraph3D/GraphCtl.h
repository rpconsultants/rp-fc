// GraphCtl.h : Declaration of the CGraphCtl
//
// Autor : Nikolai Teofilov  nteofilov@yahoo.de
//			
// Comment	: Use with your own risk !
//
// Copyright (c) 2003.

#ifndef __GRAPHCTL_H_
#define __GRAPHCTL_H_

#include "resource.h"       // main symbols
#include <atlctl.h>

#ifndef MAX
#define MAX 1
#endif

#ifndef MIN
#define MIN 0
#endif

class CPoint3D
{
public:
   float x, y, z;
   CPoint3D () { x=y=z=0; }
   CPoint3D (float c1, float c2, float c3)
   {
      x = c1;      y = c2;      z = c3;
   }
   CPoint3D& operator=(const CPoint3D& pt)
   {
      x = pt.x;   z = pt.z;   y = pt.y;
      return *this;
   }
   CPoint3D (const CPoint3D& pt)
   {
      *this = pt;
   }
   void Translate(double cx, double cy, double cz)
   {
	   x+=cx;
	   y+=cy;
	   z+=cz;
   }
};

using namespace std;
// Define a template class for a vector of 3D Points.
typedef vector<CPoint3D> POINTVECTOR;


// Enum to describe control's line style constants
enum LineType {Lines, Points, LinePoint, Surface};

//////////////////////////////////////////
// Declaration of the CGraphElement class.
// See NTGraphCtl.cpp for implementation.
class CElement 
{
public:
	CElement()
	{
		 min=max=CPoint3D(0,0,0);
		 bIsPlotAvailable = FALSE ;
		 m_bShow = TRUE;
		 m_bFill = TRUE;
		 m_bFlat = FALSE;
		 m_bLights = FALSE;
		 
		 ////// Initial lighting params //////
		 m_LightParam[0] = 40;	// X position
	 	 m_LightParam[1] = 100;	// Y position
		 m_LightParam[2] = 80;	// Z position
		 m_LightParam[3] = 80;	// Ambient light
		 m_LightParam[4] = 50;	// Diffuse light
		 m_LightParam[5] = 50;	// Specular light
		 m_LightParam[6] = 50;	// Ambient material
		 m_LightParam[7] = 50;	// Diffuse material
		 m_LightParam[8] = 40;	// Specular material
		 m_LightParam[9] = 70;	// Shininess material
		 m_LightParam[10] = 50;	// Emission material
		 
		 m_LineColor = m_PointColor = RGB(100,100,100);
		 m_nType  = LinePoint;
		 m_LineWidth = 0.0f;
		 m_PointSize = 0.0f;
	}

	BOOL bIsPlotAvailable;
	BOOL m_bShow;
	BOOL m_bFill;
	BOOL m_bFlat;
	BOOL m_bLights;
	int	m_LightParam[11];	// Graphics dimension (along X-axis)
	COLORREF m_LineColor;
	COLORREF m_PointColor;
	LineType m_nType;
	GLfloat	 m_LineWidth;
	GLfloat  m_PointSize;

	CPoint3D min,max;

	POINTVECTOR m_PointList;
};


// Define a template class for a vector of Elements.
typedef vector<CElement> ELEMENTVECTOR;

/////////////////////////////////////////////////////////////////////////////
// CGraphCtl
class ATL_NO_VTABLE CGraphCtl : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CStockPropImpl<CGraphCtl, IGraph3D, &IID_IGraph3D, &LIBID_NTGRAPH3DLib>,
	public CComControl<CGraphCtl>,
	public IPersistStreamInitImpl<CGraphCtl>,
	public IOleControlImpl<CGraphCtl>,
	public IOleObjectImpl<CGraphCtl>,
	public IOleInPlaceActiveObjectImpl<CGraphCtl>,
	public IViewObjectExImpl<CGraphCtl>,
	public IOleInPlaceObjectWindowlessImpl<CGraphCtl>,
	public IPersistStorageImpl<CGraphCtl>,
	public ISpecifyPropertyPagesImpl<CGraphCtl>,
	public IQuickActivateImpl<CGraphCtl>,
	public IDataObjectImpl<CGraphCtl>,
	public IProvideClassInfo2Impl<&CLSID_NTGraph3D, NULL, &LIBID_NTGRAPH3DLib>,
	public CComCoClass<CGraphCtl, &CLSID_NTGraph3D>
{

   // Enum to describe tracking mode state, border and projection style constants.
   enum TrackModeState {None, Zoom, Rotate, Pan};   
   enum ProjectionStyle {Perspective, Orthographic};
   enum Borderstyle {none = 0, bump = 1, etched = 2, raised = 3};


public:
	CGraphCtl()
	{
		m_bMouseCaptured = FALSE;
		m_bstrCaption = _T("NTGraph3D");
		m_bWindowOnly = TRUE;
		m_wAngleY = -45.0f;
		m_wAngleX = 30.0f;
		m_wAngleZ = 0.0f;
		m_pPal = NULL;
		m_hPal = NULL;
		m_hrc = NULL;

		bIsPlotAvailable= FALSE;

		m_nTrackState = None;

		m_bstrXLabel = "X";
		m_bstrYLabel = "Y";
		m_bstrZLabel = "Z";

		m_clrBackColor = RGB(255,255,255);
		m_clrXGridColor = RGB(255,0,0);
		m_clrYGridColor = RGB(0,255,0);
		m_clrZGridColor = RGB(0,0,255);
		m_clrCaptionColor = RGB(128,128,255);

		m_nProjection = Perspective; //set perspective projection

		m_nGridX = 2;
		m_nGridY = 2;
		m_nGridZ = 2;		

		SetRange(0,1,0,1,0,1);
		
		dAutoRangeX[MIN] = 0;
		dAutoRangeX[MAX] = 0;
		dAutoRangeY[MIN] = 0;
		dAutoRangeY[MAX] = 0;
		dAutoRangeZ[MIN] = 0;
		dAutoRangeZ[MAX] = 0;

	}

	~CGraphCtl()
	{
		if (m_pPal)
		{
			delete[] m_pPal;
			m_pPal = NULL;
		}
		if (m_hPal)
			DeleteObject(m_hPal);

		// Delete Element List
		if(!m_ElementList.empty())
				m_ElementList.clear ();
	}


	void SetBkColor(COLORREF rgb)
	{
		float r=float(GetRValue(rgb))/255;
		float g=float(GetGValue(rgb))/255;
		float b=float(GetBValue(rgb))/255;
		glClearColor(r,g,b,10.0f);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void SetColor(COLORREF rgb)
	{
		float r=float(GetRValue(rgb))/255;
		float g=float(GetGValue(rgb))/255;
		float b=float(GetBValue(rgb))/255;
		glColor3f(r,g,b);
	}

	void SetLight (CElement *pElement);


DECLARE_REGISTRY_RESOURCEID(IDR_GRAPHCTL)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CGraphCtl)
	COM_INTERFACE_ENTRY(IGraph3D)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IViewObjectEx)
	COM_INTERFACE_ENTRY(IViewObject2)
	COM_INTERFACE_ENTRY(IViewObject)
	COM_INTERFACE_ENTRY(IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceObject)
	COM_INTERFACE_ENTRY2(IOleWindow, IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceActiveObject)
	COM_INTERFACE_ENTRY(IOleControl)
	COM_INTERFACE_ENTRY(IOleObject)
	COM_INTERFACE_ENTRY(IPersistStreamInit)
	COM_INTERFACE_ENTRY2(IPersist, IPersistStreamInit)
	COM_INTERFACE_ENTRY(ISpecifyPropertyPages)
	COM_INTERFACE_ENTRY(IQuickActivate)
	COM_INTERFACE_ENTRY(IPersistStorage)
	COM_INTERFACE_ENTRY(IDataObject)
	COM_INTERFACE_ENTRY(IProvideClassInfo)
	COM_INTERFACE_ENTRY(IProvideClassInfo2)
END_COM_MAP()

BEGIN_PROP_MAP(CGraphCtl)
	PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
	PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
	PROP_ENTRY("Appearance", DISPID_APPEARANCE, CLSID_NULL)
	PROP_ENTRY("BackColor", DISPID_BACKCOLOR, CLSID_StockColorPage)
	PROP_ENTRY("BorderStyle", DISPID_BORDERSTYLE, CLSID_NULL)
	PROP_ENTRY("BorderVisible", DISPID_BORDERVISIBLE, CLSID_NULL)
	PROP_ENTRY("Caption", DISPID_CAPTION, CLSID_NULL)
	PROP_ENTRY("Font", DISPID_FONT, CLSID_StockFontPage)
	// Example entries
	// PROP_ENTRY("Property Description", dispid, clsid)
	// PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()

BEGIN_MSG_MAP(CGraphCtl)
	CHAIN_MSG_MAP(CComControl<CGraphCtl>)
	DEFAULT_REFLECTION_HANDLER()
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUP)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
END_MSG_MAP()
// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);



// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

// IGraph3D
public:
	STDMETHOD(get_Lighting)(short ElementID, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Lighting)(short ElementID, /*[in]*/ BOOL newVal);
	STDMETHOD(CopyToClipboard)();
	STDMETHOD(SetLightCoordinates)(short ElementID, float x, float y, float z);
	STDMETHOD(get_ElementMaterialEmission)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementMaterialEmission)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementMaterialShinines)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementMaterialShinines)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementMaterialSpecular)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementMaterialSpecular)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementMaterialDiffuse)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementMaterialDiffuse)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementMaterialAmbient)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementMaterialAmbient)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementLightingSpecular)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementLightingSpecular)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementLightingDiffuse)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementLightingDiffuse)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementLightingAmbient)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementLightingAmbient)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementLight)(short ElementID, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ElementLight)(short ElementID, /*[in]*/ BOOL newVal);
	STDMETHOD(get_ElementSurfaceFlat)(short ElementID, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ElementSurfaceFlat)(short ElementID, /*[in]*/ BOOL newVal);
	STDMETHOD(get_ElementSurfaceFill)(short ElementID, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ElementSurfaceFill)(short ElementID, /*[in]*/ BOOL newVal);
	STDMETHOD(get_ElementShow)(short ElementID, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ElementShow)(short ElementID, /*[in]*/ BOOL newVal);
	STDMETHOD(PlotXYZ)(double x, double y, double z, short ElementID);
	STDMETHOD(get_ElementType)(short ElementID, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_ElementType)(short ElementID, /*[in]*/ short newVal);
	STDMETHOD(get_ElementPointSize)(short ElementID, /*[out, retval]*/ float *pVal);
	STDMETHOD(put_ElementPointSize)(short ElementID, /*[in]*/ float newVal);
	STDMETHOD(get_ElementLineWidth)(short ElementID, /*[out, retval]*/ float *pVal);
	STDMETHOD(put_ElementLineWidth)(short ElementID, /*[in]*/ float newVal);
	STDMETHOD(get_ElementPointColor)(short ElementID, /*[out, retval]*/ OLE_COLOR *pVal);
	STDMETHOD(put_ElementPointColor)(short ElementID, /*[in]*/ OLE_COLOR newVal);
	STDMETHOD(get_ElementLineColor)(short ElementID, /*[out, retval]*/ OLE_COLOR *pVal);
	STDMETHOD(put_ElementLineColor)(short ElementID, /*[in]*/ OLE_COLOR newVal);
	STDMETHOD(ClearGraph)();
	STDMETHOD(DeleteElement)(short ElementID);
	STDMETHOD(AddElement)();
	STDMETHOD(get_ZGridColor)(/*[out, retval]*/ OLE_COLOR *pVal);
	STDMETHOD(put_ZGridColor)(/*[in]*/ OLE_COLOR newVal);
	STDMETHOD(get_YGridColor)(/*[out, retval]*/ OLE_COLOR *pVal);
	STDMETHOD(put_YGridColor)(/*[in]*/ OLE_COLOR newVal);
	STDMETHOD(get_XGridColor)(/*[out, retval]*/ OLE_COLOR *pVal);
	STDMETHOD(put_XGridColor)(/*[in]*/ OLE_COLOR newVal);
	STDMETHOD(get_ZGridNumber)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_ZGridNumber)(/*[in]*/ short newVal);
	STDMETHOD(get_YGridNumber)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_YGridNumber)(/*[in]*/ short newVal);
	STDMETHOD(get_XGridNumber)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_XGridNumber)(/*[in]*/ short newVal);
	STDMETHOD(get_ZLabel)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ZLabel)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_YLabel)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_YLabel)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_XLabel)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_XLabel)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Projection)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Projection)(/*[in]*/ short newVal);
	STDMETHOD(get_TrackMode)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_TrackMode)(/*[in]*/ short newVal);
	STDMETHOD(ShowPropertyPages)();
	STDMETHOD(AutoRange)();
	STDMETHOD(SetRange)(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);
	STDMETHOD(get_CaptionColor)(/*[out, retval]*/ OLE_COLOR *pVal);
	STDMETHOD(put_CaptionColor)(/*[in]*/ OLE_COLOR newVal);

	short m_nAppearance;
	LONG m_nBorderStyle;
	BOOL m_bBorderVisible;
	CComPtr<IFontDisp> m_pFont;

	OLE_COLOR m_clrBackColor;
	OLE_COLOR m_clrCaptionColor;
	OLE_COLOR m_clrXGridColor;
	OLE_COLOR m_clrYGridColor;
	OLE_COLOR m_clrZGridColor;

	CComBSTR m_bstrCaption; 
	CComBSTR m_bstrXLabel;
    CComBSTR m_bstrYLabel;
    CComBSTR m_bstrZLabel;

	long m_nProjection;
	short m_nTrackState;

	float m_fRadius;
	BOOL m_bMouseCaptured;
	int m_xPos;
	int m_yPos;
	GLfloat  m_wAngleY;
	GLfloat  m_wAngleX;
	GLfloat  m_wAngleZ;

	HGLRC m_hrc;
	HPALETTE m_hPal;
	LOGPALETTE *m_pPal;

	BOOL bIsPlotAvailable;

	int m_nGridX; 	// XGridNumber					
	int m_nGridY;	// YGridNumber
	int m_nGridZ;   // ZGridNumber

	double dRangeX[2];
	double dRangeY[2];
	double dRangeZ[2];

	double dAutoRangeX[2];
	double dAutoRangeY[2];
	double dAutoRangeZ[2];

	ELEMENTVECTOR m_ElementList; // Element List

	void DrawGraph(HDC hdc, RECT rc);
    void DrawBorder(HDC hdc, RECT rc);	
	void DrawAxis();
	void DrawAxisBox();
	void DrawAxisGrid();
	void DrawAxisLabels(HDC hdc);
	void PlotElement();
	void DrawGraphTitle(HDC hdc, RECT rc);
	void DoPan(int ptX, int ptY);
	void PrintText(HDC hdc, CPoint3D pt, const char *);
	BOOL PtInRange(CPoint3D *pt);
	BOOL PtInAxisBox(CPoint3D* pt);
	CPoint3D Corrdinate(CPoint3D *point);

	BOOL bSetupPixelFormat(HDC hdc);
	void CreateContext(HDC hdc, RECT& rc);
	void CreateRGBPalette(HDC hdc);
	unsigned char ComponentFromIndex(int i, UINT nbits, UINT shift);

	STDMETHODIMP put_Caption(BSTR pCaption)
	{
		USES_CONVERSION;
		ATLTRACE(_T("INTGraph3D::put_Caption\n"));
		m_bstrCaption = pCaption;
		return S_OK;
	}
	STDMETHODIMP get_Caption(BSTR* ppCaption)
	{
		ATLTRACE(_T("INTGraph3D::get_Caption\n"));
		*ppCaption = m_bstrCaption.Copy();
		return S_OK;
	}
	HRESULT OnDraw(ATL_DRAWINFO& di);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonUP(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		::wglMakeCurrent(NULL,  NULL);
		if (m_hrc)
		{
			::wglDeleteContext(m_hrc);
			m_hrc = NULL;
		}
		HDC hdc = GetDC();
		RECT rc;
		GetClientRect(&rc);
		CreateContext(hdc, rc);
		return 0;
	}
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}
	STDMETHOD(GetColorSet)(DWORD dwDrawAspect,LONG lindex, void* pvAspect, DVTARGETDEVICE* ptd, HDC hicTargetDev, LOGPALETTE** ppColorSet)
	{
		ATLTRACE(_T("GetColorSet\n"));
		if (ppColorSet == NULL)
			return E_POINTER;
		HRESULT hr = S_FALSE;
		*ppColorSet = NULL;
		if (m_pPal != NULL)
		{
			int nSize = sizeof(LOGPALETTE) + m_pPal->palNumEntries * sizeof(PALETTEENTRY);
			*ppColorSet = (PLOGPALETTE) CoTaskMemAlloc(nSize);
			if (*ppColorSet == NULL)
				hr = E_OUTOFMEMORY;
			else
			{
				memcpy(*ppColorSet, m_pPal, nSize);
				hr = S_OK;
			}
		}
		return hr;
	}

};

#endif //__GRAPHCTL_H_
