VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmParamLangue 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Param�tres"
   ClientHeight    =   4005
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6015
   HelpContextID   =   12
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   6015
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Tag             =   "1"
   Begin VB.CommandButton Btn_OK 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4560
      TabIndex        =   14
      Tag             =   "99"
      ToolTipText     =   "0"
      Top             =   3600
      Width           =   1455
   End
   Begin VB.CommandButton Btn_Annuler 
      Caption         =   "Annuler"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   13
      Tag             =   "98"
      ToolTipText     =   "0"
      Top             =   3600
      Width           =   1455
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3615
      Left            =   0
      TabIndex        =   0
      Tag             =   "2"
      ToolTipText     =   "0"
      Top             =   0
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   6376
      _Version        =   393216
      Tab             =   2
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Couleurs"
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame_Couleur"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Divers"
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame_Affichage"
      Tab(1).Control(1)=   "Frame17"
      Tab(1).Control(2)=   "Frame3"
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Langue"
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Label1"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "LB_Langues"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      Begin VB.ListBox LB_Langues 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2700
         Left            =   1320
         Sorted          =   -1  'True
         TabIndex        =   30
         Tag             =   "0"
         ToolTipText     =   "tt71"
         Top             =   840
         Width           =   3375
      End
      Begin VB.Frame Frame_Affichage 
         Caption         =   "Affichages divers"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   1695
         Left            =   -74880
         TabIndex        =   22
         Tag             =   "50"
         ToolTipText     =   "0"
         Top             =   420
         Width           =   5775
         Begin VB.CheckBox CB_Confirm_Suppr 
            Height          =   255
            Left            =   5160
            TabIndex        =   25
            ToolTipText     =   "Si cette case est coch�e, le d�tail des calculs de d�calage est affich�. Cette option ralentit le traitement !"
            Top             =   720
            Width           =   135
         End
         Begin VB.HScrollBar HScroll_TimerInterval 
            Height          =   255
            LargeChange     =   100
            Left            =   120
            Max             =   5
            Min             =   275
            SmallChange     =   10
            TabIndex        =   24
            Tag             =   "0"
            Top             =   1320
            Value           =   200
            Width           =   5535
         End
         Begin VB.CheckBox CB_Affichage_Calcul 
            Height          =   255
            Left            =   5160
            TabIndex        =   23
            ToolTipText     =   "Si cette case est coch�e, le d�tail des calculs de d�calage est affich�. Cette option ralentit le traitement !"
            Top             =   360
            Width           =   135
         End
         Begin VB.Label Label9 
            Caption         =   "Confirmation lors de la suppression de points ?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   28
            Tag             =   "52"
            ToolTipText     =   "0"
            Top             =   720
            Width           =   4335
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            Caption         =   "Vitesse de r�p�tition pour les fl�ches"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   27
            Tag             =   "53"
            ToolTipText     =   "0"
            Top             =   1080
            Width           =   5535
         End
         Begin VB.Label Label6 
            Caption         =   "Affichage de la progression du calcul du d�calage. Cet affichage augmente le temps d'ex�cution !"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   120
            TabIndex        =   26
            Tag             =   "51"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   4815
         End
      End
      Begin VB.Frame Frame17 
         Caption         =   "Derniers fichiers utilis�s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   735
         Left            =   -74880
         TabIndex        =   19
         Tag             =   "55"
         ToolTipText     =   "0"
         Top             =   2100
         Width           =   5775
         Begin VB.VScrollBar VScroll_MRU 
            Height          =   375
            Left            =   2880
            Max             =   0
            Min             =   9
            TabIndex        =   21
            Tag             =   "0"
            Top             =   240
            Width           =   255
         End
         Begin VB.TextBox TB_MRU 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2160
            TabIndex        =   20
            Tag             =   "0"
            Text            =   "4"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   735
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Driver GMFC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   735
         Left            =   -74880
         TabIndex        =   17
         Tag             =   "60"
         ToolTipText     =   "0"
         Top             =   2820
         Width           =   5775
         Begin VB.CheckBox CB_Endis 
            Alignment       =   1  'Right Justify
            Caption         =   "Activation/d�sactivation automatique"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   960
            TabIndex        =   18
            Tag             =   "61"
            ToolTipText     =   "tt61"
            Top             =   240
            Width           =   3855
         End
      End
      Begin VB.Frame Frame_Couleur 
         Caption         =   "Couleurs"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   3015
         Left            =   -74880
         TabIndex        =   1
         Tag             =   "5"
         ToolTipText     =   "0"
         Top             =   360
         Width           =   4695
         Begin VB.CommandButton Btn_Couleur 
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   5
            Left            =   3480
            TabIndex        =   15
            Tag             =   "0"
            ToolTipText     =   "tt13"
            Top             =   2040
            Width           =   255
         End
         Begin VB.CommandButton Btn_Couleur 
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   4
            Left            =   3480
            TabIndex        =   7
            Tag             =   "0"
            ToolTipText     =   "tt13"
            Top             =   1680
            Width           =   255
         End
         Begin VB.CommandButton Btn_Couleur 
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   3480
            TabIndex        =   6
            Tag             =   "0"
            ToolTipText     =   "tt13"
            Top             =   1320
            Width           =   255
         End
         Begin VB.CommandButton Btn_Couleur 
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   3480
            TabIndex        =   5
            Tag             =   "0"
            ToolTipText     =   "tt13"
            Top             =   960
            Width           =   255
         End
         Begin VB.CommandButton Btn_Couleur 
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   3480
            TabIndex        =   4
            Tag             =   "0"
            ToolTipText     =   "tt13"
            Top             =   600
            Width           =   255
         End
         Begin VB.CommandButton Btn_Couleur 
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   3480
            Style           =   1  'Graphical
            TabIndex        =   3
            Tag             =   "0"
            ToolTipText     =   "tt13"
            Top             =   240
            Width           =   255
         End
         Begin VB.CommandButton Btn_Valeurs_Defaut 
            Caption         =   "Valeurs des couleurs par d�faut"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   2
            Tag             =   "12"
            ToolTipText     =   "Revenir aux valeurs par d�faut pour les couleurs"
            Top             =   2520
            Width           =   4455
         End
         Begin VB.Label LB_Couleur 
            Alignment       =   2  'Center
            Caption         =   "Point d'attache"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   5
            Left            =   120
            TabIndex        =   16
            Tag             =   "11"
            ToolTipText     =   "0"
            Top             =   2040
            Width           =   3375
         End
         Begin VB.Label LB_Couleur 
            Alignment       =   2  'Center
            Caption         =   "N� des s�quences"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   120
            TabIndex        =   12
            Tag             =   "7"
            ToolTipText     =   "0"
            Top             =   600
            Width           =   3375
         End
         Begin VB.Label LB_Couleur 
            Alignment       =   2  'Center
            Caption         =   "N� des points"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   11
            Tag             =   "6"
            ToolTipText     =   "0"
            Top             =   240
            Width           =   3375
         End
         Begin VB.Label LB_Couleur 
            Alignment       =   2  'Center
            Caption         =   "Trac� s�lectionn�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   120
            TabIndex        =   10
            Tag             =   "9"
            ToolTipText     =   "0"
            Top             =   1320
            Width           =   3375
         End
         Begin VB.Label LB_Couleur 
            Alignment       =   2  'Center
            Caption         =   "Trac� initial"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   120
            TabIndex        =   9
            Tag             =   "8"
            ToolTipText     =   "0"
            Top             =   960
            Width           =   3375
         End
         Begin VB.Label LB_Couleur 
            Alignment       =   2  'Center
            Caption         =   "Trajet de l'outil"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   4
            Left            =   120
            TabIndex        =   8
            Tag             =   "10"
            ToolTipText     =   "0"
            Top             =   1680
            Width           =   3375
         End
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Vous pouvez choisir parmi les langues suivantes :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   29
         Tag             =   "70"
         ToolTipText     =   "0"
         Top             =   480
         Width           =   5775
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "FrmParamLangue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Btn_Annuler_Click()
    F_Representation.Form_Resize
    Call Chargt_Couleurs
    Call Chargt_Optimisation
    Unload Me
End Sub

Private Sub Btn_Couleur_Click(Index As Integer)
    On Error GoTo ErrorH

    CommonDialog1.ShowColor

    'Apr�s "OK"
    LB_Couleur(Index).ForeColor = CommonDialog1.Color
    gCouleurNumero = LB_Couleur(0).ForeColor
    gCouleurSequence = LB_Couleur(1).ForeColor
    gCouleurInitial = LB_Couleur(2).ForeColor
    gCouleurSelectionne = LB_Couleur(3).ForeColor
    gCouleurDecalage = LB_Couleur(4).ForeColor
    gCouleurEncoche = LB_Couleur(5).ForeColor
    Call F_Representation.Form_Resize
    Exit Sub

ErrorH:
    Common_Error 'Appel du S/P Common_Error...
    Exit Sub
End Sub

Private Sub Btn_OK_Click()
    Call Sauve_Couleurs
    Call Sauve_Optimisation
    Unload Me
End Sub

Private Sub Common_Error()
'---------------------------------
'Catch des erreurs de CommonDialog
'---------------------------------

If err.Number = 32755 Then  'Bouton "Cancel"
    'Ne rien faire
    'puisqu'il n'y a rien � faire...
End If

End Sub

Private Sub Btn_Valeurs_Defaut_Click()
    'On supprime les valeurs concernant les couleurs
    Call WriteINI("CouleurNumero", RGB(0, 0, 0))
    Call WriteINI("CouleurSequence", RGB(0, 0, 255))
    Call WriteINI("CouleurSelectionne", RGB(255, 0, 0))
    Call WriteINI("CouleurDeselectionne", RGB(100, 0, 0))
    Call WriteINI("CouleurDecalage", RGB(0, 255, 0))
    Call WriteINI("CouleurEncoche", RGB(100, 0, 100))
    'Il faut redessiner le formulaire
    Form_Load
End Sub

Private Sub CB_Endis_Click()
    gEnDis = CB_Endis
End Sub

Private Sub Form_Load()
    Dim MonDossier As String
    Dim MonFichier As String
    Dim MonCodeLangue As String
    Dim Machaine As String
    
    Call ChargeTrad(Me)
    CB_Affichage_Calcul.value = IIf(gAffichageCalcul, vbChecked, vbUnchecked)
    CB_Confirm_Suppr.value = IIf(gConfirmSuppr, vbChecked, vbUnchecked)
    HScroll_TimerInterval = gTimerInterval
    VScroll_MRU.value = gMaxMRU
    CB_Endis.value = IIf(gEnDis, vbChecked, vbUnchecked)
    
    Call Chargt_Couleurs
    
    LB_Couleur(0).ForeColor = gCouleurNumero
    LB_Couleur(1).ForeColor = gCouleurSequence
    LB_Couleur(2).ForeColor = gCouleurInitial
    LB_Couleur(3).ForeColor = gCouleurSelectionne
    LB_Couleur(4).ForeColor = gCouleurDecalage
    LB_Couleur(5).ForeColor = gCouleurEncoche
    
    'Remplissage du tableau des langues
    MonDossier = App.Path & "\" & App.EXEName & "??.trad"
    MonFichier = Dir(MonDossier, vbNormal)
    Do While MonFichier <> ""
        If Len(GetFichierSansExtension(MonFichier)) = Len(App.EXEName) + 3 Then '3 = langue + point
            MonCodeLangue = Left$(Right$(MonFichier, 7), 2)
            Machaine = MonCodeLangue & vbTab
            Select Case MonCodeLangue
                ' fr : Fran�ais
                Case "fr"
                    Machaine = Machaine & "Fran�ais"
                ' en : Anglais
                Case "en"
                    Machaine = Machaine & "English"
                ' de : Allemand
                Case "de"
                    Machaine = Machaine & "Deutsch"
                ' es : Espagnol
                Case "es"
                    Machaine = Machaine & "Espanol"
                ' ca : Catalan
                Case "ca"
                    Machaine = Machaine & "Catalan"
                ' it : Italien
                Case "it"
                    Machaine = Machaine & "Italiano"
                ' pt : Portugais
                Case "pt"
                    Machaine = Machaine & "Portugues"
                ' de : Allemand
                Case "de"
                    Machaine = Machaine & "Deutsch"
            End Select
            Me.LB_Langues.AddItem Machaine
        End If
        MonFichier = Dir()
    Loop

    SSTab1.Tab = 1
End Sub

Private Sub LB_Langues_Click()
    gLangue = Left(LB_Langues.List(LB_Langues.ListIndex), 2)
    Call WriteINI("Language", gLangue)
    Call ChargeTrad(Me)
End Sub

Private Sub VScroll_MRU_Change()
    If VScroll_MRU > 9 Then
        Exit Sub
    End If
    gMaxMRU = VScroll_MRU
    TB_MRU = gMaxMRU
End Sub
