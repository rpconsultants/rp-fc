
NTGraph3Dps.dll: dlldata.obj NTGraph3D_p.obj NTGraph3D_i.obj
	link /dll /out:NTGraph3Dps.dll /def:NTGraph3Dps.def /entry:DllMain dlldata.obj NTGraph3D_p.obj NTGraph3D_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del NTGraph3Dps.dll
	@del NTGraph3Dps.lib
	@del NTGraph3Dps.exp
	@del dlldata.obj
	@del NTGraph3D_p.obj
	@del NTGraph3D_i.obj
